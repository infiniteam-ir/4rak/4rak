<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Css and Bootstrap-->
    <link href="{{asset('login-files/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/w3s.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit.min.js')}}" rel="stylesheet">
    <link href="{{asset('login-files/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <!--Private for This page-->
    <link href="{{asset('login-files/login.css')}}" rel="stylesheet">

    <title> تغییر شماره موبایل | چارک</title>
</head>
<body class="container-fluid w-100 font-iran-sans">
<div class="row">


    <div class="col-12 col-md-4 bg-light text-center justify-content-center shadowRight">
        <div class="rightSection">
            <div class="mx-auto">
                <a href="{{route('about')}}">
                    <img class="img-fluid " style="max-width: 150px;max-height: 150px"
                         src="{{asset('img/4rak-logo.png')}}">
                </a>
                <div>
                    <label class="label mb-3">لطفا شماره جدید را وارد کنید. </label>
                    <form method="POST" action="{{route('update_mobile')}}" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label class="text-muted">شماره فعلی:</label>
                            <div class="input-group">
                                <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                    <div class="input-group-text border-0 row ml-1">
                                        <i class="fas fa-mobile-alt col-2"></i>
                                        <input disabled name="current-mobile" type="text"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->mobile}}"
                                               class="disabled form-control text-center border-0 col-10">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="text-muted">شماره جدید:</label>
                            <div class="input-group">
                                <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                    <div class="input-group-text border-0 row ml-1">
                                        <i class="fas fa-mobile-alt col-2"></i>
                                        <input name="mobile" placeholder="09130000000" type="text"
                                               class="form-control text-center border-0 col-10">
                                    </div>
                                </div>
                            </div>
                            @if($errors->any())
                                <ul class="rounded p-2 text-danger">
                                    @foreach($errors->all() as $error)
                                        <li class="my-1">{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-success w-25 px-5s">ثبت</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-8 d-none d-md-flex bgLeft">

    </div>
</div>
</body>
<!-- jQuery -->
<script src="{{asset('login-files/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('login-files/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('login-files/uikit.min.js')}}"></script>
<script src="{{asset('login-files/uikit-icons.min.js')}}"></script>
</html>
