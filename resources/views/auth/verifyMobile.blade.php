
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Css and Bootstrap-->
    <link href="{{asset('login-files/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/w3s.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit.min.js')}}" rel="stylesheet">
    <link href="{{asset('login-files/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <!--Private for This page-->
    <link href="{{asset('login-files/login.css')}}" rel="stylesheet">

    <title>  تایید شماره موبایل | چارک</title>
</head>
<body class="container-fluid w-100 font-iran-sans">
<div class="row">


    <div class="col-12 col-md-4 bg-light text-center justify-content-center shadowRight">
        <div class="rightSection">
            <div class="mx-auto">
                <a href="{{route('about')}}">
                    <img class="img-fluid " style="max-width: 150px;max-height: 150px" src="{{asset('img/4rak-logo.png')}}">
                </a>
                <h6>تایید شماره موبایل</h6>
              <div>
                  <form method="POST" action="{{route('verifyMobile')}}">
                      @csrf
                      <div class="form-group text-center justify-content-center">
                          <div class="input-group text-center justify-content-center">
                              <label class="label-success mx-3 text-center" for="">کد تایید برای شماره {{\Illuminate\Support\Facades\Auth::user()->mobile}} ارسال شد.</label>
                             <span class="mb-3">لطفا کد را در کادر زیر وارد کنید. </span>
                              <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                  <div class="input-group-text border-0 row ml-1">
                                      <i class="fas fa-mobile-alt col-2"></i>
                                      <input name="code" type="text" placeholder="کد تایید"
                                             class="form-control text-center border-0 col-10">
                                  </div>
                              </div>
                          </div>
                          @if(Session::has('error'))
                              <div class="alert alert-danger mt-2">
                                  <h4 class="text-danger">
                                      {{Session::pull('error')}}
                                  </h4>
                              </div>
                          @endif
                      </div>
                      <div class="row p-0 pr-5 mx-auto input-group my-4">
                          <a class="text-muted" href="{{route('change_mobile')}}">تغییر شماره موبایل</a>
                      </div>
                      <button type="submit" class="btn btn-success w-50  px-5s">تایید</button>
                  </form>
              </div>

            </div>
        </div>
    </div>
    <div class="col-md-8 d-none d-md-flex bgLeft">

    </div>
</div>
</body>
<!-- jQuery -->
<script src="{{asset('login-files/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('login-files/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('login-files/uikit.min.js')}}"></script>
<script src="{{asset('login-files/uikit-icons.min.js')}}"></script>
</html>
