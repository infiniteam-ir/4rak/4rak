
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Css and Bootstrap-->
    <link href="{{asset('login-files/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/w3s.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit.min.js')}}" rel="stylesheet">
    <link href="{{asset('login-files/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <!--Private for This page-->
    <link href="{{asset('login-files/login.css')}}" rel="stylesheet">

    <title> ثبت نام | چارک</title>
</head>
<body class="container-fluid w-100 font-iran-sans">
<div class="row">
    <div class="col-12 col-md-4 bg-light text-center justify-content-center shadowRight" style="overflow-y: auto!important;">
        <div class="rightSection">
            <div class="mx-auto">
                <a href="{{route('about')}}">
                    <img class="img-fluid " style="max-width: 150px;max-height: 150px" src="{{asset('img/4rak-logo.png')}}">
                </a>
                <ul class="nav nav-tabs border-0 justify-content-center mt-0 mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  outlineRemove "  href="{{route('login')}}" >ورود</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active outlineRemove "  href="{{route('register')}}">ثبت نام</a>
                    </li>
                </ul>
               <div>
                   <form method="POST" action="{{route('register')}}">
                       @csrf
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-user col-2"></i>
                                       <input name="name" type="text" placeholder="نام " class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('name') <small class="text-danger ">{{$message}}</small> @enderror--}}
                       </div>
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-user col-2"></i>
                                       <input name="family" type="text" placeholder="نام خانوادگی"
                                              class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('family') <small class="text-danger">{{$message}}</small> @enderror--}}
                       </div>
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-mobile-alt col-2"></i>
                                       <input name="mobile" type="text" placeholder="موبایل"
                                              class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('mobile') <small class="text-danger">{{$message}}</small> @enderror--}}

                       </div>
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-mail-bulk col-2"></i>
                                       <input name="email" autocomplete="off" type="text" placeholder="ایمیل "
                                              class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('email') <small class="text-danger">{{$message}}</small> @enderror--}}

                       </div>
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-key col-2"></i>
                                       <input name="password" type="password" placeholder="کلمه عبور "
                                              class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('password') <small class="text-danger">{{$message}}</small> @enderror--}}

                       </div>
                       <div class="form-group">
                           <div class="input-group">
                               <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                   <div class="input-group-text border-0 row ml-1">
                                       <i class="fas fa-key col-2"></i>
                                       <input name="password_confirmation" type="password" placeholder="تکرار کلمه عبور "
                                              class="form-control text-center border-0 col-10">
                                   </div>
                               </div>
                           </div>
{{--                           @error('password_confirmation') <small class="text-danger">{{$message}}</small> @enderror--}}

                       </div>
                       <div class="row mb-3">
                           <button type="reset" class="btn btn-danger float-right col-4 mx-3">انصراف</button>
                           <button type="submit" class="btn btn-info float-left col-4 mr-5">ثبت نام</button>
                       </div>
                   </form>
                   @if($errors->any())
                       <div class="row d-md-none w-100 h-100 d-flex  align-items-center">
                           <div class="col-12  text-danger ">
                               <ul class="rounded ">
                                   @foreach($errors->all() as $error)
                                       <li class="my-1">{{$error}}</li>
                                   @endforeach
                               </ul>
                           </div>
                       </div>
                   @endif
               </div>

            </div>
        </div>
    </div>
    <div class="col-md-8 d-none d-md-flex bgLeft">
        @if($errors->any())
        <div class="row w-100 h-100 d-flex  align-items-center">
            <div class="col-7 bg text-white ">
                    <ul class="rounded p-5" style="background-color: rgba(226,49,49,0.8)">
                        @foreach($errors->all() as $error)
                            <li class="my-1">{{$error}}</li>
                        @endforeach
                    </ul>
            </div>
        </div>
        @endif
    </div>

</div>
</body>
<!-- jQuery -->
<script src="{{asset('login-files/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('login-files/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('login-files/uikit.min.js')}}"></script>
<script src="{{asset('login-files/uikit-icons.min.js')}}"></script>
</html>
