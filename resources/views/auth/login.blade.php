
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Css and Bootstrap-->
    <link href="{{asset('login-files/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/w3s.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('login-files/uikit.min.js')}}" rel="stylesheet">
    <link href="{{asset('login-files/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <!--Private for This page-->
    <link href="{{asset('login-files/login.css')}}" rel="stylesheet">

    <title> ورود | چارک </title>
</head>
<body class="container-fluid w-100 font-iran-sans">
<div class="row">
    <div class="col-12  col-md-12 col-lg-4 bg-light text-center justify-content-center shadowRight">
        <div class="rightSection">
            <div class=" mx-lg-auto">
                <a href="{{route('about')}}">
                    <img class="img-fluid " style="max-width: 150px;max-height: 150px" src="{{asset('img/4rak-logo.png')}}">
                </a>
                <ul class="nav nav-tabs border-0 justify-content-center mt-0 mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active outlineRemove "  href="{{route('login')}}" >ورود</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link outlineRemove "  href="{{route('register')}}">ثبت نام</a>
                    </li>
                </ul>

              <div class="w-100">
                  <form method="POST" action="{{route('login')}}">
                      @csrf
                      <div class="form-group">
                          <div class="input-group">
                              <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                  <div class="input-group-text border-0 row ml-1">
                                      <i class="fas fa-user col-2"></i>
                                      <input name="mobile" type="text" placeholder="موبایل"
                                             class="form-control text-center border-0 col-10">
                                  </div>
                              </div>
                          </div>
                          @error('mobile')
                          <span class="text-danger mt-3">{{$message}}</span>
                          @enderror
                      </div>
                      <div class="form-group">
                          <div class="input-group">
                              <div class="row p-0 mx-auto input-group-text inputRounnd overflow-hidden">
                                  <div class="input-group-text border-0 row ml-1">
                                      <i class="fas fa-lock col-2"></i>
                                      <input name="password" type="password" placeholder="رمز عبور "
                                             class="form-control text-center border-0 col-10">
                                  </div>
                              </div>
                          </div>
                          @error('password')
                          <span class="text-danger mt-3">{{$message}}</span>
                          @enderror
                      </div>
                      <div class="row p-0 pr-5 mx-auto input-group my-4">
                          <a class="text-muted" href="{{route('change_password')}}">رمز عبور خود را فراموش کردم!</a>
                      </div>
                      <button type="submit" class="btn btn-success  px-5">ورود</button>
                  </form>
              </div>

            </div>
        </div>
    </div>

    <div class="col-12 col-md-12 d-none d-lg-flex col-lg-8  bgLeft">

    </div>
</div>
</body>
<!-- jQuery -->
<script src="{{asset('login-files/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('login-files/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('login-files/uikit.min.js')}}"></script>
<script src="{{asset('login-files/uikit-icons.min.js')}}"></script>
</html>
