<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-lg-block sidebar collapse pt-0 pl-2">
    <div id="sidebarMenu2" class="pt-5">
        <ul id="sidebarMenu3" class="navbar-nav noStyle bgColor boxShadow">
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-home ml-2 bg-primary p-2 uk-text-center py-3"></i>خانه</a></li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-user ml-2 bg-primary p-2 uk-text-center py-3"></i>مشخصات</a></li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-address-card ml-2 bg-primary p-2 uk-text-center py-3"></i>درباره</a>
            </li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-project-diagram ml-2 bg-primary p-2 uk-text-center py-3"></i>نمونه
                    کار</a>
            </li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-blog ml-2 bg-primary p-2 uk-text-center py-3"></i>وبلاگ</a></li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-address-book ml-2 bg-primary p-2 uk-text-center py-3"></i>مخاطب</a>
            </li>
            <li class="nav-item p-0"><a href="#"><i
                            class="fas fa-map-pin ml-2 bg-primary p-2 uk-text-center py-3"></i>نقشه</a></li>
        </ul>
    </div>
</nav>
<div id="mySidebar" class="sidebar2 pt-5 d-lg-none w3-sidebar w3-bar-block w3-animate-right"
     style="display:none;z-index:5">
    <ul class="navbar-nav noStyle bgColor shadow">
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-home ml-2 bg-primary p-2 uk-text-center py-3"></i>خانه</a></li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-user ml-2 bg-primary p-2 uk-text-center py-3"></i>مشخصات</a></li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-address-card ml-2 bg-primary p-2 uk-text-center py-3"></i>درباره</a>
        </li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-project-diagram ml-2 bg-primary p-2 uk-text-center py-3"></i>نمونه
                کار</a>
        </li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-blog ml-2 bg-primary p-2 uk-text-center py-3"></i>وبلاگ</a></li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-address-book ml-2 bg-primary p-2 uk-text-center py-3"></i>مخاطب</a>
        </li>
        <li class="nav-item p-0"><a href="#"><i
                        class="fas fa-map-pin ml-2 bg-primary p-2 uk-text-center py-3"></i>نقشه</a></li>
    </ul>
</div>
<div class="w3-overlay w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" id="myOverlay"></div>