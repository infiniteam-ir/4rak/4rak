<!--Footer-->

<footer class="row w-100 p-0 bg-dark py-4 rounded d-flex d-inline">
    <div class="col-12 col-md-7 w-100">
            <span class="text-white">کليه حقوق اين سايت متعلق به چارک می باشد. طراحی توسط گروه طراحان وب <a
                        href="https://haranet.ir/" class="text-warning">هرانت</a></span>
    </div>
    <div class="mt-sm-2 col-12 col-md-4 w-100 p-0 m-0 float-left">
        <a href="https://twitter.com/tweeter"> <i
                    class="mx-2 fab fa-twitter-square circleBox float-left fa-2x uk-border-circle"></i></a>
        <a href="https://www.facebook.com"> <i
                    class=" mx-2 fab fa-facebook-square circleBox float-left fa-2x uk-border-circle"></i></a>
        <a href="https://www.instagram.com/"> <i
                    class="mx-2 fab fa-instagram circleBox float-left fa-2x uk-border-circle"></i></a>
        <a href="https://www.gmail.com/"> <i
                    class="mx-2 fab fa-google-plus-g circleBox float-left fa-2x uk-border-circle"></i></a>
    </div>
</footer>
