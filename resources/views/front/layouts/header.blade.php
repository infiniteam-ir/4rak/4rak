<!--Nav Bar-->


<div class="row">
    <nav class="navbar navbar-expand-lg fixed-top myColor p-0" id="navBarTop">
        <button id="openSidebar" class="navbar-toggler" onclick="w3_toggle()" type="button"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-ellipsis-h"></i></span>
        </button>
        <a class="col-5 col-md-2 col-lg-2 navbar-brand mx-0" href="#">
            <img src="{{asset('images/Logo')}}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
        </button>
        <div class="row collapse navbar-collapse" id="navbarSupportedContent">
            <div class="form-group col-6 col-md-3 nav-link m-0">
                <select class="nav-item form-control navbar-nav w-100 justify-content-start">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="form-group col-6 col-md-3 nav-link m-0">
                <select class="nav-item form-control navbar-nav w-100 justify-content-start">
                    <option>2222</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="col-8 col-md-4 nav-link">
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text rounded"><span class="fas fa-search "></span></span>
                    </div>
                    <input type="text" class="form-control rounded" placeholder="جست و جو" aria-label="جست و جو">
                </div>
            </div>
            <div class="form-group col-4 col-md-2 text-center nav-link m-0" id="navIcon">
                <a class="mx-2 text-decoration-none" href="#" title="ورود">
                    <i class="fas fa-sign-in-alt fa-lg"></i>
                </a>
                <a class="mx-2 text-decoration-none" href="{{route('register')}}" title="ثبت نام">
                    <i class="fas fa-user-plus fa-lg"></i>
                </a>
            </div>
        </div>
    </nav>
</div>
{{--
<div class="row">
    <nav class="navbar navbar-expand-lg fixed-top myColor p-0" id="navBarTop">
        <button id="openSidebar" class="text-right navbar-toggler col" onclick="w3_toggle()" type="button"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
        </button>
        <a class="col-6 col-md-2 col-lg-2 navbar-brand py-1" href="#">
            <img src="asset/img/Logo">
        </a>
        <div class="col d-lg-none"></div>
    </nav>
</div>--}}
