<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>چارک | پلتفرم تجارت آنلاین</title>

    <!-- Bootstrap CSS -->
    <!--Private for this page-->

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/w3s.css')}}">
    <link href="{{asset('css/uikit.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/uikit-rtl.css')}}" rel="stylesheet">
    <link href="{{asset('css/all.min.css')}}" rel="stylesheet">
{{--    <link href="{{asset('css/frontStyle.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <link href="{{asset('css/master.css')}}" rel="stylesheet">

    @yield('css')

</head>

<body class=" @yield('body-class') ">
<div class="container-fluid">
<!-- sideBar show -->
@include('front.layouts.sidebar')

<!--Nav Bar-->
@include('front.layouts.header')

<!--Main-->

    <div class="row w-100 px-lg-0 contentMragin">
        <div class="col-lg-2">
        </div>
        <div class="col-12 col-lg-10 main_content mt-3 px-lg-5 text-center mb-3 py-3">

            @yield('main')
        </div>
    </div>

    <!--Footer-->

</div>

@include('front.layouts.footer')

</body>
<!-- jQuery -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vahidjs.js')}}"></script>


</html>
