<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>چارک| فروشگاه {{$shop->name}}</title>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">

    <link rel="stylesheet" href="{{asset('asset/css/store/bootstrap-rtl.css')}}">
    {{--    <link rel="stylesheet" href="{{asset('asset/css/store/font-awesome.min.css')}}">--}}
    <link type="text/css" href="{{asset('asset/css/store/style.css')}}" rel="stylesheet"/>

    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/fontawesome.min.css')}}">
</head>
<body class="profile-page" data-spy="scroll" data-target=".navbar" data-offset="50">

<!-- The HEADER -->

<header id="header" style="background: url({{asset('img/header.png')}})">
    <nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img class="bg-warning img-rounded" src="{{asset('assets/img/fox-logo.png')}}">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navigation-example">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{route('store')}}">صفحه نخست</a></li>
                    <li class="active"><a href="{{route('about')}}">درباره ما</a></li>
                    <li>
                        <a  href="{{route('ShoppingCart')}}" class=" text-decoration-none ">
                        <span class="badge badge-warning p-5" style="padding-right:20px;padding-left: 20px;padding-top: 5px;padding-bottom: 10px; background-color: #7781ff">
                            <i class="fa fa-shopping-bag fa-2x m-5 "></i>
                            <span class="text-white mr-1 pb-5">
                                {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}
                            </span>
                        </span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- Shop Logo -->
    @if($logo)
        <img id="profilePic" src="{{asset('images/logos/'.$logo->path)}}">
    @else
        <img id="profilePic" src="{{asset('img/logo.jpg')}}">
    @endif
</header>
<!-- sidebar -->
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <!-- navigation side bar + Categories -->
                <ul class="nav nav-pills nav-pills-success nav-stacked">
                    @foreach($categories as $cat)
                        @if($cat->products)
                        <li class="">
                            <a data-toggle="pill" href="#{{$cat->title}}">{{$cat->title}} </a>
                        </li>
                        @endif
                    @endforeach
                    <li><a data-toggle="pill" href="#contact">ارتباط با فروشنده</a></li>
                </ul>

                <!-- recent activity -->
                <div class="recent">
                    <h4>اخبار کارکنان </h4>
                    <ul>
                        <li> شما یک نظر از طرف <a class="text-success">احمد</a> دارید.</li>
                        <li> شما یک نظر از طرف <a class="text-success">احمد</a> دارید.</li>
                        <li> شما یک نظر از طرف <a class="text-success">احمد</a> دارید.</li>
                        <li> شما یک نظر از طرف <a class="text-success">احمد</a> دارید.</li>
                        <li> شما یک نظر از طرف <a class="text-success">احمد</a> دارید.</li>
                    </ul>
                    <a class="text-success more2">بیشتر</a>
                </div>
            </div>
            <!-- start tab content -->
            <div class="col-md-6 col-sm-8 col-xs-12">
                <div class="tab-content">
                    <!-- about -->
                        <div id="products" class="tab-pane fade in active  about">
                            <h3>همه محصولات</h3>
                            <div class="row">
                                @foreach($products as $product)
                                        <a class="text-decoration-none" href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="thumbnail">
                                                    @if($product->photo)
                                                    <img class="img-fluid" style="max-height: 200px;min-width: 200px" src="{{asset('images/'.$product->photo->path)}}">
                                                    @else
                                                    <img class="img-fluid" style="max-height: 200px;min-width: 200px" src="{{asset('img/default-image.svg')}}">
                                                   @endif
                                                    <div class="caption">
                                                        <p>{{$product->name}}
                                                            <small></small>
                                                        <p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                @endforeach
                            </div>
                        </div>

                    @foreach($categories as $cat )
                        <div id="{{$cat->title}}" class="tab-pane fade in  about">
                            <h3>{{$cat->title}}</h3>
                            <div class="row">
                                @foreach($products as $product)
                                    @if($product->cat_id==$cat->id)
                                        <a class="text-decoration-none" href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="thumbnail">
                                                    @if($product->photo)
                                                    <img class="img-fluid" style="max-height: 200px;min-width: 200px" src="{{asset('images/'.$product->photo->path)}}">
                                                    @else
                                                    <img class="img-fluid" style="max-height: 200px;min-width: 200px" src="{{asset('img/default-image.svg')}}">
                                                    @endif
                                                    <div class="caption">
                                                        <p>{{$product->name}}
                                                            <small></small>
                                                        <p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                @endforeach
                <!-- subset -->
                {{--<div id="subset" class="tab-pane fade">--}}
                {{--<h3>لوازم جانبی</h3>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/headphone.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>هدفون--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!-- adventures and blog -->--}}
                {{--<div id="adventures" class="tab-pane fade">--}}
                {{--<h3>قطعات</h3>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="{{asset('images/card-grph.jpg')}}">--}}
                {{--<div class="caption">--}}
                {{--<p>کارت گرافیک--}}
                {{--<small></small>--}}
                {{--<p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}


                <!-- contact form -->
                    <div id="contact" class="tab-pane fade">
                        <h3>ارتباط با فروشنده</h3>
                        <div class="card card-signup">
                            <form class="form" method="" action="">
                                <div class="content">
                                    <div class="input-group">
											 <span class="input-group-addon">
												<i class="fa fa-user-circle"></i>نام
											 </span>
                                        <input type="text" class="form-control" placeholder="نام و نام خانوادگی">
                                    </div>
                                    <div class="input-group">
											<span class="input-group-addon">
											  <i class="fa fa-envelope"></i>پست الکترونیک
											</span>
                                        <input type="text" class="form-control" placeholder="پست الکترونیک">
                                    </div>
                                    <div class="input-group">
											<span class="input-group-addon">
											  <i class="fa fa-pencil"></i>متن پیام
											</span>
                                        <textarea placeholder="متن پیام ..." class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <a href="#" class="btn btn-simple btn-success btn-lg text-blue">ارسال پیام</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- left sidebar friends status -->
            <div class="col-md-3 col-sm-12 col-xs-12 ">
                <div id="onlineFriends">
                    <h3 class="about my-2">درباره فروشگاه</h3>
                    <p><span><i class="fa fa-user-circle ml-2"></i><strong>نام:</strong></span> {{$shop->name}} </p>
                    <p><span><i class="fa fa-user-circle-o ml-2"></i><strong>صنف :</strong></span> {{$shop->class}}
                    </p>
                    <p><span><i class="fa fa-calendar ml-2"></i><strong>رسته :</strong></span> {{$shop->guild}} </p>

                    <p><span><i class="fa fa-map-marker ml-2"></i><strong>آدرس :</strong></span> {{$shop->province}}
                        | {{$shop->city}} </p>
                    <p><span><i class="fa fa-phone ml-2"></i><strong>توضیحات :</strong></span> {{$shop->description}}
                    </p>
                </div>
                <!--<div id="onlineFriends">
                    <h3 class="about">وضعیت کارکنان</h3>
                    <ul>
                        <li><button class="btn btn-success btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">صفا</span></button><span class="text-success status"> آنلاین</span></li>
                        <li><button class="btn btn-danger btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">مریم</span></button><span class="text-danger status"> آفلاین</span></li>
                        <li><button class="btn btn-success btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">محسن</span></button><span class="text-success status"> آنلاین</span></li>
                        <li><button class="btn btn-primary btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">احمد</span></button><span class="text-primary status"> در حال مکالمه</span></li>
                        <li><button class="btn btn-warning btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">علی</span></button><span class="text-warning status"> معلق</span></li>
                        <li><button class="btn btn-primary btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">سینا</span></button><span class="text-primary status"> در حال مکالمه</span></li>
                        <li><button class="btn btn-info btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">مبینا</span></button><span class="text-info status"> مشغول</span></li>
                        <li><button class="btn btn-success btn-xs"><img class="onlineFriendsPics" src="asset/img/store/f-avatar.jpg" width="35px"><span class="onlineFriendsNames">میثم</span></button><span class="text-success status"> آنلاین</span></li>
                    </ul>
                    <a class="text-success more" href="#">بیشتر</a>
                </div>-->
            </div>
        </div>
    </div>
</main>

<!-- page footer -->
<footer class="footer">
    <div class="container">
        <nav class="pull-left">
            {{-- <div id="share-buttons">
                 <a href="#" target="_blank">
                     <i class="fa fa-facebook-square"></i>
                 </a>
                 <a href="#" target="_blank">
                     <i class="fa fa-twitter-square"></i>
                 </a>
                 <a href="#" target="_blank">
                     <i class="fas fa-instagram-square"></i>
                 </a>
                 <a href="#" target="_blank">
                     <i class="fas fa-telegram"></i>
                 </a>
             </div>--}}
        </nav>
        <div class="copyright pull-right">
            طراحی واجرا <a class="text-dark text-bold text-decoration-none " href="http://haranet.ir/" target="_blank">اینفینیـــتیم
                </a>
        </div>
    </div>
</footer>

<script src="{{asset('asset/js/store/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('asset/js/store/bootstrap.min.js')}}"></script>
</body>
</html>
