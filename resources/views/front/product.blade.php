<!DOCTYPE html>
<html lang="en" id="html" class="bg-light">
<head>
    <meta charset="UTF-8">
    <title>محصولات | {{$product->name}}</title>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    <!--Css and Bootstrap-->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('asset/css/w3s.css')}}">
    <!-- <link href="asset/css/uikit.min.css" rel="stylesheet">-->
    <link href="{{asset('asset/css/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/css/all.min.css')}}" rel="stylesheet">
    <!--Private for This page-->
    <link href="{{asset('asset/css/mall.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
</head>
<body id="main" class="bg-light">
<!--bottom badge fixed-->
<div class="position-fixed mr-3 mb-3 bg-info rounded-circle text-center shopdw"  style="z-index: 100">
    <a href="{{route('ShoppingCart')}}">
    <i class="fa fa-shopping-bag fa-2x mt-2 text-white"></i>
    <div class="badge-danger rounded-circle position-relative badgedw">
        <span> {{Session::has('cart')?Session::get('cart')->totalQty:"0"}}</span>
    </div>
    </a>
</div>
<!--End bottom badge fixed-->
<!--Layer for close side bar-->
<div class="d-none" id="layer" onclick="closeNav()"></div>
<!--End Layer for close side bar-->
<!-- Start first Header -->
<header class="bg-white shadow rounded d-none d-lg-block" id="firstHeader">
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu1">
                <ul class="navbar-nav ml-auto col-md-4">
                    <li class="nav-item ml-4">
                        <a href="" class="nav-link disabled text-muted">
                            <i class="fas fa-headset text-warning ml-1"></i>
                            <span>09137179363</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="mailto:info@4rak.ir">
                            <i class="fas fa-at text-warning ml-1"></i>
                            <span>4rak.ir</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav mt-0 col-md-8 justify-content-end">
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-shipping-fast text-warning ml-1"></i>
                            <span>پیگیری سفارشات</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-question text-warning ml-1"></i>
                            <span>سوالات متدوال</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-phone text-warning ml-1"></i>
                            <span>تماس با ما</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu2">
                <ul class="navbar-nav ml-5 col-md-2">
                    <li class="nav-item">
                        <a class="navbar-brand mr-0" href="{{route('about')}}">
                            <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 150px"
                                 class="bg-warning rounded" alt="">
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto col-md-4">
                    <div class="input-icons pl-0">
                        <i class="fa fa-search icon"></i>
                        <i class="fab fa-searchengin icon2 fa-lg"></i>
                        <input class="form-control rounded-right" id="searchBox" type="search"
                               placeholder="جست و جو در چارک..." style="font-size: medium" aria-label="Search">
                    </div>
                    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

                </ul>
                @guest()
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('login')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    ورود / عضویت
                                </button>
                            </form>
                        </li>
                    </ul>
                @else
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('Profile.index')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    پروفایل
                                </button>
                            </form>
                        </li>
                    </ul>
                @endguest
                <ul class="navbar-nav ml-auto col-md-3">
                    <a class="pop" href="{{route('ShoppingCart')}}" data-container="body" data-toggle="popover"
                       id="navLink"
                       data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge">
                            <i class="fa fa-shopping-bag fa-2x bad"></i>
                            <span class="text-warning mr-1 pb-5">
                                  <span> {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}</span>
                            </span>
                            <i class="fa fa-shopping-bag fa-2x mt-2 text-white"></i>

                        </span>
                    </a>
                    <a class="pop mr-5" href="#" data-container="body" data-toggle="popover" id="navLink"
                       data-placement="bottom" data-content="مورد علاقه ها">
                        <span class="badge"><i class="fa fa-heart fa-2x bad"></i><span
                                    class="text-warning"></span></span>

                    </a>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu3">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('allShops')}}">لیست فروشگاه ها
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="{{route('store')}}">خانه
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#about">درباره</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#main_features">ویژگی ها</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End First Header -->
<!--Start Responsive Header-->
<header class="bg-white shadow rounded d-lg-none" id="responsiveHeader">
    <nav class="navbar navbar-expand navbar-light  d-lg-none">
        <div class="container-fluid">
            <div class="collapse navbar-collapse row" id="response_menu">
                <div class="navbar-nav col-11 col-sm-3 d-flex justify-content-center pr-0">
                    <a class="navbar-brand" href="#">
                        <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 50px" class="" alt="">
                    </a>
                </div>
                <div class="navbar-nav mr-0 col-12 col-sm-6 mt-2 mt-sm-0 row">
                    <a class="pop col-4 d-flex justify-content-center text-muted" href="{{route('ShoppingCart')}}"
                       data-container="body"
                       data-toggle="popover" data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge"><i class="fa fa-shopping-basket fa-2x"></i><span
                                    class="text-warning mr-1 pb-5">
                                {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}
                            </span></span>
                    </a>
                    {{-- <a class="pop col-4 d-flex justify-content-center text-muted" href="#" data-container="body"
                        data-toggle="popover"
                        data-placement="bottom" data-content="مورد علاقه ها">
                         <span class="badge"><i class="fa fa-heart fa-2x"></i><span class="text-warning">9</span></span>
                     </a>--}}
                    <a class="col-4 d-flex justify-content-center text-muted" href="#">
                        <i class="fa fa-search fa-2x bad"></i>

                    </a>
                </div>
                <div class="navbar-nav col-12 uk-position-top-left position-fixed mt-3 ml-4 col-sm-3 justify-content-end ml-1">
                    <div id="mySidenav" class="sidenav">

                        <a class="" href="{{route('allShops')}}">فروشگاه ها</a>
                        <a class="" href="{{route('store')}}">خانه</a>
                        <a class="" href="#about">درباره</a>
                        <a class="" href="#main_features">ویژگی ها</a>
                        <a class="" href="#screenshots">تصاویر</a>
                        <a class="" href="#team">تیم</a>

                    </div>
                    <!-- Use any element to open the sidenav -->
                    <i class="fa fa-bars fa-2x text-danger" onclick="openNav()"></i>
                    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
                </div>
            </div>
        </div>
    </nav>
</header>
<!--End Responsive Header-->
<!--Main Content-->
<div class="container">
    <div class="row p-0 mt-3">
        <div class="col-12 boxContent mr-0">
            فروشگاه / {{$product->category->title}} / {{$product->name}}
        </div>
    </div>
    <div class="row p-0 m-0 boxContent">
        <div class="col-12 col-md-3 my-2 my-md-0">
            @if($product->photo)
                <img src="{{asset('images/'.$product->photo->path)}}" class=" m-2 rounded">
            @else
                <img src="{{asset('img/default-image.svg')}}" class=" m-2 rounded">
            @endif
        </div>
        <div class="col-12 col-md-6 my-2 my-md-0 ">
            <div>
                <h6 class="font-weight-bold mt-2 mb-0">
                    {{$product->name}}
                </h6>
                <small class="text-primary"></small>
            </div>
            <div class="line w-100"></div>

            <div>
                <p class="text-bold mt-3">توضیحات این محصول:</p>
                <h3>
                    @php
                        echo $product->description;
                    @endphp
                </h3>
            </div>

            <div>
                <ul>
                    <li class="mt-3">
                        <span class="liBox">
                            دسته بندی :  <a href="#">{{$product->category->title}}</a>
                        </span>
                    </li>
                </ul>
                <div class="mr-3">
                    @if($product->guaranty)
                    <p><i class="fa fa-truck-loading ml-2 text-danger"></i>این کالا دارای گارانتی به مدت
                        @switch($product->guaranty->duration_unit)
                            @case('day')
                            <span class="badge badge-success p-1">{{$product->guaranty->duration}} روز</span>
                                @break
                            @case('month')
                                <span class="badge badge-success p-1">{{$product->guaranty->duration}} ماه</span>
                                @break
                            @case('year')
                            <span class="badge badge-success p-1">{{$product->guaranty->duration}} سال</span>
                                @break
                            @endswitch
                         است.</p>
                    @endif
                    {{--<p><i class="fas fa-star-half-alt ml-2 text-primary"></i>این محصول امتیاز {{$product->rate}} را از دیدگاه 0
                        کاربر به دست آورده است.
                    </p>--}}
                </div>
            </div>

        </div>
        <div class="col-12 col-md-3 my-2 my-md-0 border-right p-0">
            <ul class="list-unstyled p-3 text-muted mb-3">
                <li class="mb-2 situation"><i class="fa fa-store ml-2 text-muted iconSize"></i>فروشنده: <span
                            class="text-primary">{{$product->shop->name}}</span>
                    <br>
                    <small>ارسال کالا از انبار فروشنده</small>
                </li>
                <li class="mb-2 situation"><i class="fa fa-check-square ml-2 iconSize"></i>وضعیت انبار: <span
                            class="text-primary">موجود در انبار</span><br>
                    <small>آماده ارسال از
                        انبار
                        فروشگاه
                    </small>
                </li>
                <li class="mb-2 situation"><i class="fa fa-shield-alt ml-2 iconSize"></i> گارانتی <span
                            class="text-primary">بازگشت وجه و اصالت کالا</span><br>
                    <small> همراه با
                        گارانتی
                        ارائه بهترین محصول
                    </small>
                </li>
                <li class="mb-2 situation"><i class="fa fa-check ml-2 iconSize"></i> امکان <span
                            class="text-primary">تحویل و پرداخت در محل</span>
                    <br>
                    <small> با تمامی کارت
                        های
                        عضو شتاب
                    </small>
                </li>
            </ul>
            <div class="justify-content-between d-flex px-3 text-muted mb-3">

                <bdi class="text-success font-weight-bolder" style="font-size: large">
                    @php echo number_format($product->sell_price); @endphp
                    <span class="mr-1 text-muted">تومان</span></bdi>
            </div>

            <div class="mt-2 justify-content-center d-flex">
                <a href="{{route('addToCart',$product->id)}}" class="btn btn-primary justify-content-between d-flex">
                    <i class="fa fa-shopping-bag mt-1"></i>
                    <span class="px-5">افزودن به سبد خرید</span>
                </a>
            </div>
            <div class="mt-4 mr-2">
                <i class="fa fa-dolly bg-primary fa-2x iconSize text-white p-2 uk-border-rounded"></i>
                <span>ارسال به تمام نقاط کشور</span>
            </div>
            <hr>
            <div class="mx-2 mb-3 d-flex justify-content-between">
                <i class="fa fa-gem fa-2x iconSize mt-2 rightIcon" data-toggle="popover2"
                   data-trigger="hover"
                   data-placement="top" data-content="محصولات ویژه"></i>
                <i class="fa fa-shield-alt fa-2x iconSize mt-2 rightIcon" data-toggle="popover2"
                   data-trigger="hover"
                   data-placement="top" data-content="دارای گارانتی"></i>
                <i class="fa fa-analytics fa-2x iconSize mt-2 rightIcon" data-toggle="popover2"
                   data-trigger="hover"
                   data-placement="top" data-content="پایین ترین قیمت"></i>
                <i class="fa fa-truck-loading fa-2x iconSize mt-2 rightIcon" data-toggle="popover2"
                   data-trigger="hover"
                   data-placement="top" data-content="ارسال رایگان"></i>
                <i class="fa fa-credit-card fa-2x iconSize mt-2 rightIcon" data-toggle="popover2"
                   data-trigger="hover"
                   data-placement="top" data-content="پرداخت درمحل"></i>
            </div>
        </div>
    </div>
    <div class="row p-0 m-0 mt-3">
        <div class="mt-4 boxContent w-100 mx-0">
            <ul class="nav nav-pills col-12 mb-3 d-flex justify-content-center pillcolor" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#pills-describ" role="tab" aria-selected="false"><i
                                class="fa fa-align-right"></i>
                        جزییات محصول
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#pills-idea" role="tab" aria-selected="true"><i
                                class="fa fa-comments ml-2"></i>نظرات
                        کابران</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#pills-moredescrib" role="tab"
                       aria-selected="false"><i
                                class="fa fa-align-right ml-2"></i>جزییات
                        بیشتر</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#pills-sellerinfo" role="tab"
                       aria-selected="false"><i
                                class="fa fa-store ml-2"></i>اطلاعات
                        فروشنده </a>
                </li>
            </ul>
            <div class="tab-content mt-3" id="pills-tabContent">
                <div class="tab-pane fade mt-5" id="pills-describ" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-12 mr-1 text-muted font-weight-bolder iconSize mb-4">
                            جزییات محصول
                            <hr>
                        </div>
                        <div class="col-12 px-4 textstyle">
                            <p>
                                @php
                                    echo $product->description;
                                @endphp
                            </p>
                            @if($product->photo)
                                <img src="{{asset('images/'.$product->photo->path)}}" class="rounded w-100 mx-auto mb-5">
                            @else
                                <img src="{{asset('img/default-image.svg')}}" class="rounded w-100 mx-auto mb-5">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-idea" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    <div class="row mt-5">
                        <div class="col-md-7 col-12">
                            <p class="text-muted font-weight-bolder iconSize mt-3 mb-0 mr-2">ارسال دیدگاه</p>
                            <hr class="w-25 mt-0 mr-2" style="border-top: 5px solid #007bff;">
                            <ul class="list-unstyled pr-2 mb-3">
                                <li class="mb-2">قوانین ارسال دیدگاه</li>
                                <li class="my-2">
                                    <i class="fa fa-caret-left text-muted"></i>
                                    <span class="text-muted font-weight-light mr-1">چنانچه دیدگاهی توهین آمیز باشد و متوجه اشخاص مدیر، نویسندگان و سایر کاربران باشد تایید نخواهد شد.</span>
                                </li>
                                <li class="my-2">
                                    <i class="fa fa-caret-left text-muted"></i>
                                    <span class="text-muted font-weight-light mr-1">چنانچه دیدگاه شما جنبه ی تبلیغاتی داشته باشد تایید نخواهد شد.</span>
                                </li>
                                <li class="my-2">
                                    <i class="fa fa-caret-left text-muted"></i>
                                    <span class="text-muted font-weight-light mr-1">چنانچه از لینک سایر وبسایت ها و یا وبسایت خود در دیدگاه استفاده کرده باشید تایید نخواهد شد.</span>
                                </li>
                                <li class="my-2">
                                    <i class="fa fa-caret-left text-muted"></i>
                                    <span class="text-muted font-weight-light mr-1">چنانچه در دیدگاه خود از شماره تماس، ایمیل و آیدی تلگرام استفاده کرده باشید تایید نخواهد شد.</span>
                                </li>
                                <li class="my-2">
                                    <i class="fa fa-caret-left text-muted"></i>
                                    <span class="text-muted font-weight-light mr-1"> چنانچه دیدگاهی بی ارتباط با موضوع آموزش مطرح شود تایید نخواهد شد.</span>
                                </li>

                            </ul>
                            <span class="text-muted font-weight-bold mr-2"> دیدگاهتان را بنویسید</span>
                            <br><br>
                            <span class="text-muted font-weight-light mr-2"> نشانی ایمیل شما منتشر نخواهد شد. بخش‌های موردنیاز علامت‌گذاری شده‌اند *</span>
                            <br><br>
                            <span class="text-muted font-weight-light mr-2">امتیاز شما</span><br>
                            <div class="rating text-muted font-weight-light mr-2 iconSize">
                                <span class="" style="font-size: 25px">☆</span>
                                <span class="" style="font-size: 25px">☆</span>
                                <span class="" style="font-size: 25px">☆</span>
                                <span class="" style="font-size: 25px">☆</span>
                                <span class="" style="font-size: 25px">☆</span>
                            </div>
                            <div class="form-group w-75">
                                <span class="text-muted font-weight-light mr-2">دیدگاه شما *</span><br>
                                <textarea class="form-control my-2 mr-2" rows="5">
                                </textarea><br>
                                <span class="text-muted font-weight-light mr-2">نام *</span><br>
                                <input type="text" class="form-control mt-2 mr-2" placeholder="نام خود را وارد کنید">
                                <br>
                                <span class="text-muted font-weight-light mr-2">ایمیل *</span><br>
                                <input type="email" class="form-control mt-2 mr-2" placeholder="ایمیل خود را وارد کنید">
                                <br><br>
                                <input class="form-check-input mr-2" type="checkbox" value="" id="defaultCheck1">
                                <span class="mr-md-4 mr-auto text-muted">ذخیره نام، ایمیل و وبسایت من در مرورگر برای زمانی که دوباره دیدگاهی می‌نویسم.</span>
                                <button class="btn btn-primary w-100 mt-4">ثبت</button>
                            </div>

                        </div>
                        <div class="col-md-5 col-12">
                            <i class="fa fa-comment-lines fa-3x float-left mt-4 commentBg"></i>
                            <p class="text-muted font-weight-bolder iconSize mt-3 mb-0 mr-2">نظرات</p>
                            <hr class="w-25 mt-0" style="border-top: 5px solid #007bff;">
                            <div>
                                <i class="fa fa-user-circle fa-3x text-muted mt-4"></i>
                                <span class="mr-2 font-weight-bold text-muted">
                                عباس پورهادی گفت :<br>
                                <span class="mr-5 font-weight-lighter fa-1x">99/05/05 در قبل از ظهر ساعت 10:00</span>
                                <p class="mr-2">
                                    «به صورت حرفه‌ای عکاسی می‌کردم و درآمد خیلی خوبی هم داشتم اما این کسب و کار سنتی به شدت وقت مرا می‌گرفت.» خانم اسدی دانشجوی وبمستران هوشمند، در این دوره سایت خود در حوزه‌ی معرفی تصاویر و کلیپ‌های واقعیت افزوده را راه اندازی کردند.
                                </p>
                            </span>
                            </div>
                        </div>

                    </div>
                </div>
               {{-- <div class="tab-pane fade" id="pills-moredescrib" role="tabpanel"
                     aria-labelledby="pills-contact-tab">
                    <div class="mr-4">
                        <p class="text-muted font-weight-bolder iconSize mt-3 mb-0 mr-2">جزئیات محصول</p>
                        <hr class="w-25 mt-0 mr-2" style="border-top: 5px solid #007bff;">
                        <div class="row">
                            <div class="col-4">
                                <ul class="list-unstyled">
                                    <li class="my-4 text-muted font-weight-lighter">رنگ</li>
                                    <li class="my-4 text-muted font-weight-lighter">رم</li>
                                    <li class="my-4 text-muted font-weight-lighter"> سری پردازنده</li>
                                    <li class="my-4 text-muted font-weight-lighter">سیستم عامل</li>
                                    <li class="my-4 text-muted font-weight-lighter"> دقت صفحه نمایش</li>
                                </ul>
                            </div>
                            <div class="col-8">
                                <ul class="list-unstyled">
                                    <li class="my-4 text-muted font-weight-lighter">سفید ,مشکی ,آبی ,نقره ای ,طلایی</li>
                                    <li class="my-4 text-muted font-weight-lighter">8 Gb</li>
                                    <li class="my-4 text-muted font-weight-lighter">Core i7</li>
                                    <li class="my-4 text-muted font-weight-lighter">Windows</li>
                                    <li class="my-4 text-muted font-weight-lighter">FULL HD</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="tab-pane fade show active" id="pills-sellerinfo" role="tabpanel"
                     aria-labelledby="pills-contact-tab">
                    <div class="row mr-5 mt-5">
                        <div class="col-2 m-0 p-0">
                            <i class="fa fa-store fa-4x text-white rounded bg-primary p-2"></i>
                        </div>
                        <div class="col-10 ">
                            <a href="{{route('singleshop',$product->shop->id)}}" class=" text-decoration-none h3 text-warning my-5">فروشگاه {{$product->shop->name}}</a><br>
                            <h6 class="text-muted mt-5">© کليه حقوق محصولات و محتوای اين سایت متعلق به مدیر سایت می باشد و هر گونه کپی برداری از محتوا و محصولات سایت پیگرد قانونی دارد.</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End Main Content-->
<!-- Start  Footer -->
<div class="container-fluid">
    <div class="row">
        <footer class="padding-100 pb-0 container-fluid w-100">
            <div class="subscribe">
                <div class="container mb-5">
                    <form class="subscribe-form row m-0 align-items-center" action="#" method="POST">
                        <div class="col-lg-9 col-md-8">
                            <div class="form-group mb-0">
                                <input type="email" class="form-control text-left" placeholder="ایمیل خود را وارد کنید"
                                       dir="ltr" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4">
                            <button type="submit" class="btn btn-primary shadow d-block w-100 btn-colord btn-theme">
                                <span>اشتراک</span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <img src="{{asset('assets/img/fox-logo.png')}}" class="img-fluid bg-dark rounded-circle"
                                     alt="">
                                <p> چارک پلتفرم آنلاین تجارتی است که دغدغه های فروش کالا و مدیریت فروشگاه را برای
                                    فروشگاه داران و دغدغه ها و مشکلات خرید کالا و تهیه اجناس مورد نیاز خریداران را با
                                    راهکار هایی نو و برخط برطرف می نماید.

                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>دسترسی سریع</h6>
                                <ul>
                                    <li>
                                        <a href="#">خانه</a>
                                    </li>
                                    <li>
                                        <a href="#">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="#">خدمات</a>
                                    </li>
                                    <li>
                                        <a href="#">محصولات</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>شبکه اجتماعی</h6>
                                <ul>
                                    <li>
                                        <a href="https://t.me/ir4rak">کانال تلگرام</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/4rak.ir">اینستاگرام</a>
                                    </li>
                                    <li>
                                        <a href="#">لینکدین</a>
                                    </li>
                                    <li>
                                        <a href="#">توییتر</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>تماس سریع</h6>
                                <ul>
                                    <li>
                                        <span>تلفن : </span> <span class="ltr-text">0913 233 89 38</span>
                                    </li>
                                    <li>
                                        <span>ایمیل : </span>
                                        <a href="#">info@4rak.ir</a>
                                    </li>
                                    <li>
                                        <span>آدرس : </span>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="copyright">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <p>طراحی و اجرا توسط <a class="text-light" href="https://haranet.ir" target="_blank">اینفینیــتیم</a>
                            </p>
                        </div>
                        <div class="offset-md-2 col-md-5">
                            <ul class="nav justify-content-center justify-content-md-end">
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">قوانین و مقررات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">سیاست حریم خصوصی</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- End  Footer  -->
</body>
<!-- jQuery -->
<script src="{{asset('asset/js/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('asset/js/uikit.min.js')}}"></script>
<script src="{{asset('asset/js/uikit-icons.min.js')}}"></script>
<!--js For this page-->
<script src="{{asset('asset/js/mall.js')}}"></script>

</html>
