<!DOCTYPE html>
<html lang="en" id="html" class="bg-light">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <title>فروشگاه | چارک</title>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    <!--Css and Bootstrap-->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('asset/css/w3s.css')}}">
    <!-- <link href="asset/css/uikit.min.css" rel="stylesheet">-->
    <link href="{{asset('asset/css/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/css/all.min.css')}}" rel="stylesheet">
    <!--Private for This page-->
    <link href="{{asset('asset/css/mall.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
</head>
<body id="main" class="bg-light">
<!--bottom badge fixed-->
<div class="position-fixed mr-3 mb-3 bg-info rounded-circle text-center shopdw" style="z-index: 100">
    <a href="{{route('ShoppingCart')}}">
        <i class="fa fa-shopping-bag fa-2x mt-2 text-white"></i>
        <div class="badge-danger rounded-circle position-relative badgedw">
            <span> {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}</span>
        </div>
    </a>

</div>
<!--End bottom badge fixed-->
<!--Layer for close side bar-->
<div class="d-none" id="layer" onclick="closeNav()"></div>
<!--End Layer for close side bar-->
<!-- Start first Header -->
<header class="bg-white shadow rounded d-none d-lg-block" id="firstHeader">
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu1">
                <ul class="navbar-nav ml-auto col-md-4">
                    <li class="nav-item ml-4">
                        <a href="" class="nav-link disabled text-muted">
                            <i class="fas fa-headset text-warning ml-1"></i>
                            <span>09137179363</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="mailto:info@4rak.ir">
                            <i class="fas fa-at text-warning ml-1"></i>
                            <span>4rak.ir</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav mt-0 col-md-8 justify-content-end">
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-shipping-fast text-warning ml-1"></i>
                            <span>پیگیری سفارشات</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-question text-warning ml-1"></i>
                            <span>سوالات متدوال</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-phone text-warning ml-1"></i>
                            <span>تماس با ما</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu2">
                <ul class="navbar-nav ml-5 col-md-2">
                    <li class="nav-item">
                        <a class="navbar-brand mr-0" href="{{route('about')}}">
                            <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 150px"
                                 class="bg-warning rounded" alt="">
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto col-md-4">
                    <div class="input-icons pl-0">
                        <i class="fa fa-search icon"></i>
                        <i class="fab fa-searchengin icon2 fa-lg"></i>
                        <input class="form-control rounded-right" id="searchBox" type="search"
                               placeholder="جست و جو در چارک..." style="font-size: medium" aria-label="Search">
                    </div>
                    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

                </ul>
                @guest()
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('login')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    ورود / عضویت
                                </button>
                            </form>
                        </li>
                    </ul>
                @else
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('Profile.index')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    پروفایل
                                </button>
                            </form>
                        </li>
                    </ul>
                @endguest
                <ul class="navbar-nav ml-auto col-md-3">
                    <a class="pop" href="{{route('ShoppingCart')}}" data-container="body" data-toggle="popover"
                       id="navLink"
                       data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge">
                            <i class="fa fa-shopping-bag fa-2x bad"></i>
                            <span class="text-warning mr-1 pb-5">
                                  <span> {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}</span>
                            </span>
                            <i class="fa fa-shopping-bag fa-2x mt-2 text-white"></i>

                        </span>
                    </a>
                    <a class="pop mr-5" href="#" data-container="body" data-toggle="popover" id="navLink"
                       data-placement="bottom" data-content="مورد علاقه ها">
                        <span class="badge"><i class="fa fa-heart fa-2x bad"></i><span
                                    class="text-warning"></span></span>

                    </a>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu3">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('allShops')}}">لیست فروشگاه ها
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="{{route('store')}}">خانه
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#about">درباره</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#main_features">ویژگی ها</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End First Header -->
<!--Start Responsive Header-->
<header class="bg-white shadow rounded d-lg-none" id="responsiveHeader">
    <nav class="navbar navbar-expand navbar-light  d-lg-none">
        <div class="container-fluid">
            <div class="collapse navbar-collapse row" id="response_menu">
                <div class="navbar-nav col-11 col-sm-3 d-flex justify-content-center pr-0">
                    <a class="navbar-brand" href="#">
                        <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 50px" class="" alt="">
                    </a>
                </div>
                <div class="navbar-nav mr-0 col-12 col-sm-6 mt-2 mt-sm-0 row">
                    <a class="pop col-4 d-flex justify-content-center text-muted" href="{{route('ShoppingCart')}}"
                       data-container="body"
                       data-toggle="popover" data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge"><i class="fa fa-shopping-basket fa-2x"></i><span
                                    class="text-warning mr-1 pb-5">
                                {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}
                            </span></span>
                    </a>
                    {{-- <a class="pop col-4 d-flex justify-content-center text-muted" href="#" data-container="body"
                        data-toggle="popover"
                        data-placement="bottom" data-content="مورد علاقه ها">
                         <span class="badge"><i class="fa fa-heart fa-2x"></i><span class="text-warning">9</span></span>
                     </a>--}}
                    <a class="col-4 d-flex justify-content-center text-muted" href="#">
                        <i class="fa fa-search fa-2x bad"></i>

                    </a>
                </div>
                <div class="navbar-nav col-12 uk-position-top-left position-fixed mt-3 ml-4 col-sm-3 justify-content-end ml-1">
                    <div id="mySidenav" class="sidenav">

                        <a class="" href="{{route('allShops')}}">فروشگاه ها</a>
                        <a class="" href="{{route('store')}}">خانه</a>
                        <a class="" href="#about">درباره</a>
                        <a class="" href="#main_features">ویژگی ها</a>
                        <a class="" href="#screenshots">تصاویر</a>
                        <a class="" href="#team">تیم</a>

                    </div>
                    <!-- Use any element to open the sidenav -->
                    <i class="fa fa-bars fa-2x text-danger" onclick="openNav()"></i>
                    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
                </div>
            </div>
        </div>
    </nav>
</header>
<!--End Responsive Header-->
<div class="container mt-5">
    <!--slide Show and right Banner-->
    <div class="row">
        <div class="col-12 col-md-4 rounded">
            <div class="row">
                <img class="col-12 mb-3 bannerRounded" src="{{asset('asset/img/banner.jpg')}}" alt="">
                <img class="col-12 mt-3 bannerRounded" src="{{asset('asset/img/banner2.jpg')}}" alt="">
            </div>
        </div>
        <div class="col-12 col-md-8 overflow-hidden slideHeight pl-0">
            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
                <ul class="uk-slideshow-items">
                    <li>
                        <img src="{{asset('asset/img/slider-02-dgs-opt.jpg')}}" alt="" uk-cover>
                    </li>
                    <li>
                        <img src="{{asset('asset/img/dg-slider-01-opt.jpg')}}" alt="" uk-cover>
                    </li>
                </ul>
                <a class="uk-position-center-left uk-position-small text-dark" href="#" uk-slidenav-previous
                   uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small text-dark" href="#" uk-slidenav-next
                   uk-slideshow-item="next"></a>
            </div>
        </div>
    </div>
    <!--End slide Show and right Banner-->
    <!--off slideshow and banner-->
    <div class="row mt-5 mb-5 offBox">
        <div class="col-12 col-md-3 px-0">
            <div class="text-center flag">
                <p>پیشنهادات ویژه</p>
                <p style="font-size: 11px;font-weight: lighter">special products</p>
                <br>
                <p>Until 90% Off</p>
                <br>
                <br>
                <br>
                <a class="btn btn-outline-light btn-sm removeFlag">مشاهده همه تخفیف ها</a>
                <br>
                <br>
                <br>
            </div>
        </div>
        <div class="col-12 col-md-9">
            <div uk-slider="center: true" style="direction: ltr">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-3@s uk-grid p-0 offHover">
                        @foreach($products as $product)
                            <li class=" text-dark mt-5">
                                <div class="card border-0 align-items-center">
                                    <div class="uk-card-media-top justify-content-center d-flex"
                                         style="max-height: 150px;max-width:150px;">
                                        @if($product->photo)
                                        <img class="img-fluid rounded-circle"
                                             src="{{asset('images/'.$product->photo->path)}}" alt="">
                                            @else
                                            <img class="img-fluid rounded-circle"
                                                 src="{{asset('img/default-image.svg')}}" alt="">
                                        @endif
                                    </div>
                                    <div class="uk-card-body text-center p-0 mt-5">
                                            <span class="uk-card-title bg-light py-1 rounded px-2"
                                                  style="font-size: small">{{$product->category->title}}</span>
                                        <p class="text-dark" style="font-size: small">
                                            <a class="text-decoration-none"
                                               href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                <span class="text-dark font-weight-bold">{{$product->name}}</span>
                                            </a>
                                            <br>
                                            @php
                                                echo $product->description;
                                            @endphp
                                        </p>
                                        <bdi class="text-danger float-left mt-2">{{$product->sell_price}}
                                            <span> تومان</span>
                                        </bdi>
                                        <bdi class="text-muted float-right mt-2" style="font-size: small">
                                        </bdi>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <a class="uk-position-center-right uk-position-large text-danger mx-0" href="#" uk-slidenav-next
                       uk-slider-item="next"></a>
                    <a class="uk-position-center-left uk-position-large text-danger mx-0" href="#" uk-slidenav-previous
                       uk-slider-item="previous"></a>

                </div>
            </div>
        </div>
    </div>
    <!--End off slideshow and banner-->
    <!--card Banner-->
    <div class="row text-center">
        <div class="col-12 col-md-3 my-3 my-md-0">
            <img class="rounded-circle" src="{{asset('asset/img/banner02-1.jpeg')}}">
        </div>
        <div class="col-12 col-md-3 my-3 my-md-0">
            <img class="rounded-circle" src="{{asset('asset/img/banner03-1.jpeg')}}">
        </div>
        <div class="col-12 col-md-3 my-3 my-md-0">
            <img class="rounded-circle" src="{{asset('asset/img/benner01-1.jpeg')}}">
        </div>
        <div class="col-12 col-md-3 my-3 my-md-0">
            <img class="rounded-circle" src="{{asset('asset/img/banner04-1.jpeg')}}">
        </div>
    </div>
    <!--End card Banner-->
    <!--Note Book and laptop Bannner-->
    <div class="row my-5 offBox">
        <div class="my-3 pt-2 col-12 text-dark">
            <i class="far fa-clock text-danger fa-2x mr-3"></i>
            <strong class="mr-3">لپتاپ و گوشی<br>
                <small class="text-muted mr-5">Laptop and Ultrabook</small>
            </strong>
            <div class="borderDw mr-5"></div>
        </div>
        <div class="col-12">
            <div uk-slider="center: true" style="direction: ltr">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-3@s uk-grid p-0 offHover">
                        @foreach($products as $product)
                            @if($product->category->title=='لپتاپ')
                                <li class=" text-dark mt-5">
                                    <div class="card border-0 align-items-center">
                                        <div class="uk-card-media-top justify-content-center d-flex"
                                             style="max-height: 150px;max-width:150px;">
                                            <img class="img-fluid rounded-circle"
                                                 src="{{asset('images/'.$product->photo->path)}}" alt="">
                                        </div>
                                        <div class="uk-card-body text-center p-0 mt-5">
                                            <span class="uk-card-title bg-light py-1 rounded px-2"
                                                  style="font-size: small">{{$product->category->title}}</span>
                                            <p style="font-size: small">
                                                <a class="text-decoration-none"
                                                   href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                    <span class="text-dark font-weight-bold">{{$product->name}}</span>
                                                </a> <br>
                                                @php
                                                    echo $product->description;
                                                @endphp
                                            </p>
                                            <bdi class="text-danger float-left mt-2">{{$product->sell_price}}<span> تومان</span>
                                            </bdi>
                                            <bdi class="text-muted float-right mt-2" style="font-size: small">
                                                {{--<del>250,000<span> تومان</span></del>--}}
                                            </bdi>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <a class="uk-position-center-right uk-position-large text-danger mx-0" href="#" uk-slidenav-next
                       uk-slider-item="next"></a>
                    <a class="uk-position-center-left uk-position-large text-danger mx-0" href="#" uk-slidenav-previous
                       uk-slider-item="previous"></a>
                </div>
            </div>
        </div>
    </div>
    <!--End Note Book and laptop Bannner-->
    <!--card Banner-->
    <div class="row">
        <div class="col-12 col-md-6 my-3 my-md-0">
            <img class="rounded" src="{{asset('asset/img/banner06-1.jpeg')}}">
        </div>
        <div class="col-12 col-md-6 my-3 my-md-0">
            <img class="rounded" src="{{asset('asset/img/banner05-1.jpeg')}}">
        </div>
    </div>
    <!--End card Banner-->
    <!--Note Book and laptop Bannner-->
    <div class="row my-5 offBox">
        <div class="my-3 pt-2 col-12 text-dark">
            <i class="far fa-clock text-danger fa-2x mr-3"></i>
            <strong class="mr-3">جانبی<br>
                <small class="text-muted mr-5"></small>
            </strong>
            <div class="borderDw mr-5"></div>
        </div>
        <div class="col-12">
            <div uk-slider="center: true" style="direction: ltr">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-3@s uk-grid p-0 offHover">
                        @foreach($products as $product)
                            @if($product->category->title=='جانبی')
                                <li class=" text-dark mt-5">
                                    <div class="card border-0 align-items-center">
                                        <div class="uk-card-media-top justify-content-center d-flex"
                                             style="max-height: 150px;max-width:150px;">
                                            @if($product->photo)
                                            <img class="img-fluid rounded-circle"
                                                 src="{{asset('images/'.$product->photo->path)}}" alt="">
                                                @else
                                                <img class="img-fluid rounded-circle"
                                                          src="{{asset('img/default-image.svg')}}" alt="">
                                                @endif
                                        </div>
                                        <div class="uk-card-body text-center p-0 mt-5">
                                            <span class="uk-card-title bg-light py-1 rounded px-2"
                                                  style="font-size: small">{{$product->category->title}}</span>
                                            <p style="font-size: small">
                                                <a class="text-decoration-none"
                                                   href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                    <span class="text-dark font-weight-bold">{{$product->name}}</span>
                                                </a> <br>
                                                @php
                                                    echo $product->description;
                                                @endphp
                                            </p>
                                            <bdi class="text-danger float-left mt-2">{{$product->sell_price}}<span> تومان</span>
                                            </bdi>
                                            <bdi class="text-muted float-right mt-2" style="font-size: small">
                                                {{--<del>250,000<span> تومان</span></del>--}}
                                            </bdi>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <a class="uk-position-center-right uk-position-large text-danger mx-0" href="#" uk-slidenav-next
                       uk-slider-item="next"></a>
                    <a class="uk-position-center-left uk-position-large text-danger mx-0" href="#" uk-slidenav-previous
                       uk-slider-item="previous"></a>

                </div>
            </div>
        </div>
    </div>
    <!--End Note Book and laptop Bannner-->
    <!--card Banner-->
    <div class="row">
        <div class="col-12 my-3">
            <img class="rounded" src="{{asset('asset/img/banner-30-1.jpeg')}}">
        </div>
    </div>
    <!--End card Banner-->
    <!--Brand bar-->
    <div class="row my-5 offBox">
        <div class="my-3 pt-2 col-12 text-dark">
            <i class="far fa-edit text-danger fa-2x mr-3"></i>
            <strong class="mr-3">برندهای محبوب<br>
                <small class="text-muted mr-5">popular brands</small>
            </strong>
            <div class="borderDw mr-5"></div>
        </div>
        <div class="col-12">
            <div uk-slider="center: true;autoplay:true" style="direction: ltr">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slider-items uk-child-width-1-5@s uk-grid p-0 offHover pb-4">
                        <li class="align-items-center justify-content-center d-flex">
                            <div style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/apple.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/acer.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/facebook.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/lg.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/canon.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/nvidia.png')}}" alt="">
                            </div>
                        </li>
                        <li class="align-items-center justify-content-center d-flex">
                            <div class="align-items-center justify-content-center d-flex"
                                 style="max-height: 75px;max-width:75px;">
                                <img src="{{asset('asset/img/mall-logo-2.png')}}" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--End Brand bar-->
</div>
<!-- Start  Footer -->
<div class="container-fluid">
    <div class="row">
        <footer class="padding-100 pb-0 container-fluid w-100">
            <div class="subscribe">
                <div class="container mb-5">
                    <form class="subscribe-form row m-0 align-items-center" action="#" method="POST">
                        <div class="col-lg-9 col-md-8">
                            <div class="form-group mb-0">
                                <input type="email" class="form-control text-left" placeholder="ایمیل خود را وارد کنید"
                                       dir="ltr" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4">
                            <button type="submit" class="btn btn-primary shadow d-block w-100 btn-colord btn-theme">
                                <span>اشتراک</span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <img src="{{asset('assets/img/fox-logo.png')}}" class="img-fluid bg-dark rounded-circle"
                                     alt="">
                                <p> چارک پلتفرم آنلاین تجارتی است که دغدغه های فروش کالا و مدیریت فروشگاه را برای
                                    فروشگاه داران و دغدغه ها و مشکلات خرید کالا و تهیه اجناس مورد نیاز خریداران را با
                                    راهکار هایی نو و برخط برطرف می نماید.

                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>دسترسی سریع</h6>
                                <ul>
                                    <li>
                                        <a href="#">خانه</a>
                                    </li>
                                    <li>
                                        <a href="#">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="#">خدمات</a>
                                    </li>
                                    <li>
                                        <a href="#">محصولات</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>شبکه اجتماعی</h6>
                                <ul>
                                    <li>
                                        <a href="https://t.me/ir4rak">کانال تلگرام</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/4rak.ir">اینستاگرام</a>
                                    </li>
                                    <li>
                                        <a href="#">لینکدین</a>
                                    </li>
                                    <li>
                                        <a href="#">توییتر</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>تماس سریع</h6>
                                <ul>
                                    <li>
                                        <span>تلفن : </span> <span class="ltr-text">0913 233 89 38</span>
                                    </li>
                                    <li>
                                        <span>ایمیل : </span>
                                        <a href="#">info@4rak.ir</a>
                                    </li>
                                    <li>
                                        <span>آدرس : </span>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="copyright">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <p>طراحی و اجرا توسط <a class="text-light" href="https://haranet.ir" target="_blank">اینفینیــتیم</a>
                            </p>
                        </div>
                        <div class="offset-md-2 col-md-5">
                            <ul class="nav justify-content-center justify-content-md-end">
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">قوانین و مقررات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">سیاست حریم خصوصی</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- End  Footer  -->
</body>
<!-- jQuery -->
<script src="{{asset('js/jquery.min.js')}}"></script>
{{--<script src="{{asset('asset/js/jQuery.min.js')}}"></script>--}}

<!-- Bootstrap JavaScript -->
<script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('asset/js/uikit.min.js')}}"></script>
<script src="{{asset('asset/js/uikit-icons.min.js')}}"></script>
<!--js For this page-->
<script src="{{asset('asset/js/mall.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#searchBox").on("keyup", function () {
            var data = null
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: $('.ajax.d-none').attr('id'),
                data: {// change data to this object
                    key: $('#searchBox').val(),
                    do: 'search'
                },
                dataType: 'json',
                success: function (response) {
                    data = response
                    console.log("ok")
                    $("#searchBox").autoComplete({
                        minLength: 1,
                        source: data,
                        select: function (event, ui) {
                            window.location.href = ui.item.url;
                        },
                        focus: function (event, ui) {
                            $("#searchBox").val(ui.item.label);
                            return false;
                        },
                    })
                        .autocomplete("instance")._renderItem = function (ul, item) {
                        $(ul).addClass('rounded')
                        return $("<li>")
                            .append("<div class='text-right'>" + '<strong>' + item.label + '</strong>' + '  ' + "<label> در  </label>" + '  ' + item.cat + "<hr class='m-0 p-0'></div>")
                            .appendTo(ul);

                    };
                },
                error: function (response) {
                    console.log('error')
                }
            });

        });
    })
</script>

</html>
