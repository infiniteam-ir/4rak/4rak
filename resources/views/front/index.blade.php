<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>چارک | پلتفرم تجارت آنلاین</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">

    <!-- Bootstrap CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Slick Slider -->
    <link href="{{asset('assets/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/slick-theme.css')}}" rel="stylesheet">

    <!-- AOS CSS -->
    <link href="{{asset('assets/css/aos.css')}}" rel="stylesheet">

    <!-- Lity CSS -->
    <link href="{{asset('assets/css/lity.min.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <!-- linearicons CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/linearicons.css')}}">

    <!-- Our Min CSS -->
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet">

    <!-- Themes * You can select your color from color-1 , color-2 , color-3 , color-4 , ..etc * -->
    <link id="themes_colors" href="{{asset('assets/css/color-1.css')}}" rel="stylesheet">
    <!-- <link href="assets/css/color-1.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-2.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-3.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-4.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-5.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-6.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-7.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-8.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-9.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-10.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-11.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-12.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-13.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-14.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-15.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-16.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-17.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-18.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-19.css" rel="stylesheet"> -->
    <!-- <link href="assets/css/color-20.css" rel="stylesheet"> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->

    <script src="{{asset('assets/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('assets/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#main_menu" data-offset="70">
<!-- Start Preloader -->
<div class="preloader">
    <div class="loader-wrapper">
        <div class="loader"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Themes Colors -->
<div class="themes-colors">
    <h3>سفارشی سازی قالب</h3>
    <h6>تک رنگ</h6>
    <span><i class="fas fa-cog"></i></span>
    <ul class="solid">
        <li>
            <a href="javascript:void(0)" data-style="color-1"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-2"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-3"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-4"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-5"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-6"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-7"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-8"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-9"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-10"></a>
        </li>
    </ul>
    <h6>طیف رنگ</h6>
    <ul class="gradient">
        <li>
            <a href="javascript:void(0)" data-style="color-11"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-12"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-13"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-14"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-15"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-16"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-17"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-18"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-19"></a>
        </li>
        <li>
            <a href="javascript:void(0)" data-style="color-20"></a>
        </li>
    </ul>
</div>
<!-- End Themes Colors -->

<!-- Start Header -->
<header class="foxapp-header">
    <nav class="navbar navbar-expand-lg navbar-light" id="foxapp_menu">
        <div class="container">
            <a class="navbar-brand" href="{{route('about')}}">
                <img src="{{asset('assets/img/fox-logo.png')}}" class="img-fluid" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu"
                    aria-controls="main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main_menu">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('store')}}">فروشگاه
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link anchor active" href="#slide">خانه
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link anchor" href="#about">درباره</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link anchor" href="#main_features">ویژگی ها</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link anchor" href="#screenshots">تصاویر</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link anchor" href="#team">تیم</a>
                    </li>
                    <!--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            اخبار
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item anchor" href="#recent_news">اخبار جدید</a>
                            <a class="dropdown-item anchor" href="news-without-sidebar.html">اخبار یک</a>
                            <a class="dropdown-item anchor" href="news-with-sidebar.html">اخبار دو</a>
                            <a class="dropdown-item anchor" href="news-with-sidebar-one-col.html">اخبار سه</a>
                            <a class="dropdown-item anchor" href="news-single.html">جزئیات خبر</a>
                        </div>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link anchor" href="#git_in_touch">تماس</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End Header -->

<!-- Start Header -->
<section id="slide" class="slide background-withcolor">
    <div class="content-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6" data-aos="fade-left">
                    <p class="mb-0">شما با ما میتوانید</p>
                    <h2>موفق شوید</h2>
                    <p>
                        چارک به عنوان یک پلتفرم آنلاین در زمینه Sales Assistant (دستیار فروش و فروشنده) برای دغدغه ها و مشکلات فروشندگان کالا راهکار های بروز آنلاین گوناگون فراهم و در یکجا جمع آوری کرده است.
                    </p>
                    <a href="{{route('login')}}" class="btn btn-primary btn-white shadow btn-theme"><span>چارکی شو !</span></a>
                </div>
                <div class="col-md-6" data-aos="fade-right" data-aos-delay="200">
                    <img src="{{asset('assets/img/mobile-1.png')}}" class="img-fluid d-block mx-auto" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Header -->

<!-- Start Boxes -->
<section id="boxes" class="boxes padding-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="box" data-aos="fade-up">
                    <div class="icon">
                        <span class="lnr lnr lnr-magic-wand"></span>
                    </div>
                    <div class="space-20"></div>
                    <h4>طراحی خلاقانه</h4>
                    <div class="space-15"></div>
                    <p>چارک طراحی شده برای آسان کردن کار فروشنده و خریدار نه فقط برای یکطرف بلکه برای طرفین</p>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="box" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon">
                        <span class="lnr lnr-rocket"></span>
                    </div>
                    <div class="space-20"></div>
                    <h4>پشتیبانی رایگان</h4>
                    <div class="space-15"></div>
                    <p>ما برای اطمینان دادن به مشتری ها پشتیبانی رایگان رو در اختیارشون گذاشتیم</p>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="box" data-aos="fade-up" data-aos-delay="400">
                    <div class="icon">
                        <span class="lnr lnr-diamond"></span>
                    </div>
                    <div class="space-20"></div>
                    <h4>طراحی اختصاصی</h4>
                    <div class="space-15"></div>
                    <p>طراحی ما از جمع آوری تجربیات بچه های تیم بوده و کاملا مختص به چارک ساخته شده</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Boxes -->

<!-- Start Why Us -->
<section id="about" class="why-us padding-100 background-fullwidth background-fixed "
         style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 text-center" data-aos="fade-left">
                <img src="{{asset('assets/img/mobile-2.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-6" data-aos="fade-zoom-in" data-aos-delay="200">
                <h3>کسب و کار را با ما آسان کنید</h3>
                <p>
                    چارک راهی برای جواب به نیاز های عرضه کننده و متقاضی
                </p>
                <div class="space-50"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-aos="zoom-in" data-aos-delay="400">
                        <div class="why-us-icon">
                            <span class="lnr lnr-lock"></span>
                            <p>اطلاعات شخصی، کاری، تجاری و همه تراکنش های شما نزد چارک امنیت کامل دارد
                            </p>
                        </div>
                        <div class="space-25"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-aos="zoom-in" data-aos-delay="600">
                        <div class="why-us-icon">
                            <span class="lnr lnr-laptop-phone"></span>
                            <p>
                                همیشه و همه جا برای فروشنده و خریدار دردسترس
                            </p>
                        </div>
                    </div>
                    <div class="space-25"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-aos="zoom-in" data-aos-delay="800">
                        <div class="why-us-icon">
                            <span class="lnr lnr-database"></span>
                            <p>با چارک بدون نیاز به حسابدار، با روشی جدید بصورت ساده حسابداری خود را انجام دهید
                            </p>
                        </div>
                        <div class="space-25"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-aos="zoom-in" data-aos-delay="1000">
                        <div class="why-us-icon">
                            <span class="lnr lnr-briefcase"></span>
                            <p>با چارک میتوان نیروی انسانی مورد نیاز خود را با اطمینان و در زمان کم پیدا کرد
                            </p>
                        </div>
                        <div class="space-25"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Why Us -->

<!-- Start Main Features -->
<section id="main_features" class="main-features padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3>
                    <span>ویژگی های</span> اصلی
                </h3>
                <div class="space-25"></div>
                <p>چارک برای راحتی کار عمده ، خرده فروش و خریدار</p>
                <div class="space-25"></div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-3 text-lg-right left-side">
                <div class="one-feature one" data-aos="fade-left" data-aos-delay="1000">
                    <h5>سریع</h5>
                    <span class="lnr lnr-rocket"></span>
                    <p>نیاز به نرم افزار و سخت افزار خاصی ندارد</p>
                </div>
                <div class="one-feature" data-aos="fade-left" data-aos-delay="1400">
                    <h5>منظم و یکپارچه</h5>
                    <span class="lnr lnr-cog"></span>
                    <p>همه نیاز های فرونشده و خریدار به صورت جامع  در چارک رفع میشود</p>
                </div>
                <div class="one-feature" data-aos="fade-left" data-aos-delay="1800">
                    <h5>در دسترس</h5>
                    <span class="lnr lnr-cloud"></span>
                    <p>هر زمان از شبانه روز و از هرکجا که باشید همیشه چارک دردسترس</p>
                </div>
            </div>
            <div class="col-lg-6 text-center">
                <div class="features-circle">
                    <div class="circle-svg" data-aos="zoom-in" data-aos-delay="400">
                        <svg version="1.1" id="features_circle" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="543px" height="542.953px"
                             viewbox="0 0 543 542.953" enable-background="new 0 0 543 542.953" xml:space="preserve">
                                <g>
                                    <circle fill="none" stroke="#" stroke-width="3" stroke-miterlimit="10"
                                            stroke-dasharray="11.9474,11.9474" cx="271.5" cy="271.516" r="270"/>
                                    <animateTransform attributename="transform" type="rotate" from="0" to="360"
                                                      dur="50s" repeatcount="indefinite"/>
                                </g>
                            </svg>
                    </div>
                    <img data-aos="fade-up" data-aos-delay="200" src="{{asset('assets/img/mobile-3.png')}}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-3 right-side">
                <div class="one-feature" data-aos="fade-right" data-aos-delay="1000">
                    <h5>حفاظت اطلاعات</h5>
                    <span class="lnr lnr-construction"></span>
                    <p>اطلاعات شخصی و فروشگاه خود را به ما بسپارید و آسوده خاطر باشید</p>
                </div>
                <div class="one-feature" data-aos="fade-right" data-aos-delay="1400">
                    <h5>تخفیف</h5>
                    <span class="lnr lnr-gift"></span>
                    <p>از تخفیفات و جشنواره های فروشگاه های نزدیک خود باخبر شوید</p>
                </div>
                <div class="one-feature" data-aos="fade-right" data-aos-delay="1800">
                    <h5>دسته بندی فروشندگان</h5>
                    <span class="lnr lnr-database"></span>
                    <p>به هرچیزی نیاز دارید توی دسته مربوط به خودش پیداش کنید</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Main Features -->

<!-- Start Other Features -->
<!--<section id="other_features" class="other-features padding-100 background-withcolor">
    <div class="container-fluid">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3>سایر <span class="white">ویژگی های</span> عالی
                </h3>
                <div class="space-25"></div>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و</p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12">
                <div class="other-features-slider" data-aos="fade-up">

                    <div class="item text-center">
                        <span class="lnr lnr-rocket"></span>
                        <h4>طراحی خلاقانه</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-cog"></span>
                        <h4>ورود آسان</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-cloud"></span>
                        <h4>نصب سریع</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-construction"></span>
                        <h4>محافظت داده</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-gift"></span>
                        <h4>وضوح بالا</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-database"></span>
                        <h4>کدهای مرتب</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>
                    <div class="item text-center">
                        <span class="lnr lnr-gift"></span>
                        <h4>پشتیبانی سریع</h4>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- End Other Features  -->

<!-- Start Watch Video -->
<section id="watch_video" class="watch-video padding-100">
    <div class="container-fluid">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3>مشاهده <span> ویدئو</span>
                </h3>
                <div class="space-25"></div>
                <p>مدیریت فروشگاه، انبار، اجناس، نیروی انسانی و ... همه و همه به همین سادگی و به همین سرعت قابل انجام است</p>
                <div class="space-50"></div>
            </div>
            <div class="col-md-6 offset-md-3" data-aos="fade-up">
                <div class="video" style="background-image: url('{{asset('assets/img/people.jpg')}}')">
                    <img src="{{asset('assets/img/mobile-4-4.png')}}" class="img-fluid d-block mx-auto" alt="">
                    <a href="{{asset('assets/video/video-iframe.html')}}" data-lity></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Watch Video  -->

<!-- Start Screenshots -->
<section id="screenshots" class="screenshots padding-100 background-fullwidth background-fixed"
         style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
    <div class="container-fluid">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span> محیط</span> برنامه
                </h3>
                <div class="space-25"></div>
                <p>
                    چارک در پلتفرم های مختلف (Web Application , Android Application) با تکنولوژی روز دنیا دردسترس همگان میباشید
                </p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12">
                <div class="screenshots-slider" data-aos="fade-up">


                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>
                    <div class="item text-center">
                        <img src="{{asset('assets/img/mobile-5.png')}}" class="img-fluid d-block mx-auto" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Screenshots -->

<!-- Start Clients Testimonial -->
<section id="testimonial" class="clients-testimonial padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span> ما</span> از نظر دیگران
                </h3>
                <div class="space-25"></div>
                <p>
                    متن هایی که در پایین مشاهده میکنید صحبت های افراد باتجربه در این کار و رشته است
                </p>
                <div class="space-50"></div>
            </div>
            <div class="col-12">
                <div class="testimonial-slider" data-aos="fade-up">
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                ایده شما جدید و خوب است باید روی برنامه و پله به پله جلو برید تا به سرانجام برسد البته با پشتکار و یک تیم بزرگ
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/client.jpg')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>آقای صولتی</h3>
                                <h6>مسئول برگزاری نمایشگاه تراکنش</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                پروژه خوبی است و به آن امیدوارم
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/Dr-rohani.png')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>دکتر روحانی</h3>
                                <h6>ریاست مرکز رشد</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                فعلی تکنولوژی مورد نیاز و کاربردهای متنوع
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/client.jpg')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>پاول دوروف</h3>
                                <h6>موسس تلگرام</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                فعلی تکنولوژی مورد
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/client.jpg')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>جف بزوس</h3>
                                <h6>موسس آمازون</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                فعلی تکنولوژی مورد نیاز و کاربردهای
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/client.jpg')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>مارک زاکربرگ</h3>
                                <h6>شرکت فیسبوک</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-testimonial">
                            <p>
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                فعلی تکنولوژی مورد نیاز و کاربردهای
                            </p>
                        </div>
                        <div class="client-info d-flex align-items-center">
                            <figure>
                                <img src="{{asset('assets/img/client.jpg')}}" class="img-fluid" alt="">
                            </figure>
                            <div>
                                <h3>جان اسنو</h3>
                                <h6>نگهبان دیوار بزرگ</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Clients Testimonial  -->

<!-- Start Fun Facts -->
<!--<section id="facts" class="fun-facts padding-100 background-withcolor">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span class="white"> حقایق</span> جالب
                </h3>
                <div class="space-25"></div>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و</p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-6 col-12">
                <div class="fact-box text-center" data-aos="fade-up" data-aos-delay="400">
                    <span class="lnr lnr-coffee-cup"></span>
                    <h5>245</h5>
                    <h6>فنجان قهوه</h6>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="fact-box text-center" data-aos="fade-up" data-aos-delay="800">
                    <span class="lnr lnr-code"></span>
                    <h5>3000</h5>
                    <h6>خط کد</h6>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="fact-box text-center" data-aos="fade-up" data-aos-delay="1200">
                    <span class="lnr lnr-download"></span>
                    <h5>763</h5>
                    <h6>دریافت</h6>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="fact-box text-center" data-aos="fade-up" data-aos-delay="1600">
                    <span class="lnr lnr-heart"></span>
                    <h5>360</h5>
                    <h6>اشتراک گذاری</h6>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- End Fun Facts  -->

<!-- Start App Price -->
<section id="prices" class="prices padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span> قیمت</span> اشتراک ها
                </h3>
                <div class="space-25"></div>
                <p>برای آشنایی با ما میتوانید از یک ماه تست رایگان سرویس چارک استفاده کنید یا برای افزایش هرچه سریعتر راندمان کاری فروشگاهتان پلن های کامل را تهیه کنید.</p>
                <div class="space-50"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="price-table text-center" data-aos="fade-left" data-aos-delay="400">
                    <div class="top background-fullwidth" style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
                        <h4>آشنایی</h4>
                        <h3>
                            رایگان
                        </h3>
                        <h5>یک ماهه</h5>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li>خرید</li>
                            <li>فروش</li>
                            <li>حسابداری</li>
                            <li>انبار داری</li>
                            <li>نیروی انسانی</li>
                            <li>فروشگاه آنلاین</li>
                        </ul>
                        <div class="space-50"></div>
                        <a href="#" class="btn btn-primary btn-white btn-theme"><span>انتخاب پلن</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="price-table text-center" data-aos="fade-up" data-aos-delay="800">
                    <div class="top background-fullwidth" style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
                        <span class="offer">پیشنهاد</span>
                        <h4>کسب و کار کوچک</h4>
                        <h3>
                            50,000 <span>تومان</span>
                        </h3>
                        <h5>ماهانه</h5>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li>خرید</li>
                            <li>فروش</li>
                            <li>حسابداری</li>
                            <li>انبار داری</li>
                            <li>نیروی انسانی</li>
                            <li>فروشگاه آنلاین</li>
                        </ul>
                        <div class="space-50"></div>
                        <a href="#" class="btn btn-primary btn-white btn-theme"><span>انتخاب پلن</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="price-table text-center" data-aos="fade-right" data-aos-delay="1200">
                    <div class="top background-fullwidth" style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
                        <h4>شرکتی</h4>
                        <h3>
                            80,000 <span>تومان</span>
                        </h3>
                        <h5>ماهانه</h5>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li>خرید</li>
                            <li>فروش</li>
                            <li>حسابداری</li>
                            <li>انبار داری</li>
                            <li>نیروی انسانی</li>
                            <li>فروشگاه آنلاین</li>
                        </ul>
                        <div class="space-50"></div>
                        <a href="#" class="btn btn-primary btn-white btn-theme"><span>انتخاب پلن</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End App Price  -->

<!-- Start Our Team -->
<section id="team" class="our-team padding-100 background-fullwidth background-fixed"
         style="background-image: url({{asset('assets/img/gray-bg.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span>تیم</span> ما
                </h3>
                <div class="space-25"></div>
                <p>
                    پلتفرم آنلاین چارک حاصل دسترنج و تلاش بی وقفه تیم حرفه ای اینفینیــتیم است.
                </p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="team-slider" data-aos="fade-up">
                    <div class="item person text-center">
                        <img src="{{asset('assets/img/hamid.png')}}" class="img-fluid d-block mx-auto" alt="">
                        <div class="space-20"></div>
                        <h3>حمید ربیعیان</h3>
                        <div class="space-20"></div>
                        <h5>بنیان گذار</h5>
                        <div class="space-20"></div>
                        <p>کارآفرین</p>
                        <p>مدیر و موسس</p>
                        <p>توسعه دهنده وب و موبایل</p>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="item person text-center">
                        <img src="{{asset('assets/img/abbas.png')}}" class="img-fluid d-block mx-auto" alt="">
                        <div class="space-20"></div>
                        <h3>عباس پورهادی</h3>
                        <div class="space-20"></div>
                        <h5>توسعه دهنده سمت سرور</h5>
                        <div class="space-20"></div>
                        <p>هم بنیان گذار</p>
                        <p>مهندس نرم افزار</p>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="item person text-center">
                        <img src="{{asset('assets/img/vahidkhoo.png')}}" class="img-fluid d-block mx-auto" alt="">
                        <div class="space-20"></div>
                        <h3>محمدامین وحیدخو</h3>
                        <div class="space-20"></div>
                        <h5>توسعه دهنده سمت کاربر</h5>
                        <div class="space-20"></div>
                        <p>هم بنیان گذار</p>
                        <p>مهندس نرم افزار</p>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="item person text-center">
                        <img src="{{asset('assets/img/team_04.png')}}" class="img-fluid d-block mx-auto" alt="">
                        <div class="space-20"></div>
                        <h3>فائزه اسماعیلیان</h3>
                        <div class="space-20"></div>
                        <h5>توسعه دهنده رابط کاربری</h5>
                        <div class="space-20"></div>
                        <p>هم بنیان گذار</p>
                        <p>مهندس نرم افزار</p>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- <div class="item person text-center">
                         <img src="assets/img/team-1.jpg" class="img-fluid d-block mx-auto" alt="">
                         <div class="space-20"></div>
                         <h3>اولیور کویین</h3>
                         <div class="space-20"></div>
                         <h5>توسعه دهنده رابط کاربری</h5>
                         <div class="space-20"></div>
                         <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                         </p>
                         <ul>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-facebook-f"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-twitter"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-linkedin-in"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-instagram"></i>
                                 </a>
                             </li>
                         </ul>
                     </div>
                     <div class="item person text-center">
                         <img src="assets/img/team-2.jpg" class="img-fluid d-block mx-auto" alt="">
                         <div class="space-20"></div>
                         <h3>بری الن</h3>
                         <div class="space-20"></div>
                         <h5>توسعه دهنده رابط کاربری</h5>
                         <div class="space-20"></div>
                         <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                         </p>
                         <ul>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-facebook-f"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-twitter"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-linkedin-in"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     <i class="fab fa-instagram"></i>
                                 </a>
                             </li>
                         </ul>
                     </div>-->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Team -->

<!-- Start FAQ -->
<section id="faq" class="faq padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span>سوالات</span> متداول
                </h3>
                <div class="space-25"></div>
                <p>
                    سوالات و توضیحات متداول اکثر کاربران در مورد پلتفرم چارک و چگونگی کارکرد آن.
                </p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row align-items-center">
            <!--align-items-center-->
            <div class="col-md-8 col-12" data-aos="fade-left">
                <div class="accordion" id="faqAccordion">
                    <div class="card shadow">
                        <div class="card-header" id="heading_1">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapse_1" aria-expanded="true" aria-controls="collapse_1">
                                    چارک به چه معناست؟
                                </button>
                            </h5>
                        </div>

                        <div id="collapse_1" class="collapse show" aria-labelledby="heading_1"
                             data-parent="#faqAccordion">
                            <div class="card-body">
                                از لحاظ لغوی چارک به طور همزمان دارای دو معنی گوناگون است :<br>
                                1: شهر بندرچارک مرکز بخش شیبکوه شهرستان بندر لنگه در استان هرمزگان ایران است. شهر چارک را «بندر چارک» نیز می‌نامند. در لغت‌نامه دهخدا راجع به پیشینه تاریخی بندر چارک می‌خوانیم، بندری از دهستان چارکی بخش لنگه است که در شهرستان لار واقع است.<br>
                                2:وسیله ای در اکثر فروشگاه های ایرانی است که در قدیم حکم ترازو و پیمانه را داشته،در حال حاظر هم به عنوان پیمانه در فروشگاه های آجیل، خشکبار و... هست.
                            </div>
                        </div>
                    </div>
                    <div class="card shadow">
                        <div class="card-header" id="heading_2">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapse_2" aria-expanded="false" aria-controls="collapse_2">
                                    پلتفرم چارک چیست؟
                                </button>
                            </h5>
                        </div>
                        <div id="collapse_2" class="collapse" aria-labelledby="heading_2" data-parent="#faqAccordion">
                            <div class="card-body">
                                ولی در عمل؛ چارک پلتفرمی آنلاین و سریع جهت تسهیل و افزایش راندمان، کیفیت و کمیت تجارت (خرید و فروش) می باشد که با جمع آوری راه حل های مشکلات فعلی فروشندگان و خریداران کالا،این امر را برای همه آسان تر میکند.
                            </div>
                        </div>
                    </div>
                    <div class="card shadow">
                        <div class="card-header" id="heading_3">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapse_3" aria-expanded="false" aria-controls="collapse_3">
                                    چه کسانی میتوانند از پلتفرم چارک استفاده کنند؟
                                </button>
                            </h5>
                        </div>
                        <div id="collapse_3" class="collapse" aria-labelledby="heading_3" data-parent="#faqAccordion">
                            <div class="card-body">
                                همه فروشندگان کالا و اکثر فروشندگان خدمات میتوانند از امکانات کمک فروش چارک استفاده کنند و فروش خود را بیشتر، راحت تر و سریع تر کنند.
                                <br>
                                <br>همچنین همه افراد جامعه میتوانند به عنوان خریدار، از امکانات فوق العاده چارک به صورت رایگان استفاده کرده و در اسرع وقت بهترین کالا مورد نیاز خود را از نزدیک ترین و بهترین فروشنده خریداری نماید.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12" data-aos="fade-right" data-aos-delay="400">
                <img src="{{asset('assets/img/mobile-1.png')}}" class="img-fluid b-block mx-auto" alt="">
            </div>
        </div>
    </div>
</section>
<!-- End FAQ  -->

<!-- Start Fun Facts -->
<section id="logos" class="logos background-withcolor">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logos-slider">
                    <div class="item">
                        <img src="{{asset('assets/img/logo-1.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-2.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-3.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-4.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-5.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-6.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-1.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-2.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>
                    <div class="item">
                        <img src="{{asset('assets/img/logo-3.png')}}" class="img-fluid mx-auto b-block" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Fun Facts  -->

<!-- Start Recent News -->
<!--<section id="recent_news" class="recent-news padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3><span> اخبار</span> جدید
                </h3>
                <div class="space-25"></div>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و</p>
                <div class="space-50"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="recent-news-slider" data-aos="fade-up">
                    <div class="item">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <img src="assets/img/news-1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-6 col-12">
                                <h3>لورم ایپسوم متن ساختگی</h3>
                                <div class="space-15"></div>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                                    برای شرایط
                                </p>
                                <div class="space-15"></div>
                                <ul class="news-meta">
                                    <li>
                                        <span class="lnr lnr-user"></span> توسط مدیر
                                    </li>
                                    <li>
                                        <span class="lnr lnr-clock"></span> 26 تیر 1399
                                    </li>
                                </ul>
                                <div class="space-25"></div>
                                <a href="news-single.html" class="btn btn-primary shadow btn-colord btn-theme"><span>بیشتر بخوانید</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <img src="assets/img/news-1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-6 col-12">
                                <h3>لورم ایپسوم متن ساختگی با</h3>
                                <div class="space-15"></div>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                                    برای شرایط
                                </p>
                                <div class="space-15"></div>
                                <ul class="news-meta">
                                    <li>
                                        <span class="lnr lnr-user"></span> توسط مدیر
                                    </li>
                                    <li>
                                        <span class="lnr lnr-clock"></span> 26 تیر 1399
                                    </li>
                                </ul>
                                <div class="space-25"></div>
                                <a href="news-single.html" class="btn btn-primary shadow btn-colord btn-theme"><span>بیشتر بخوانید</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <img src="assets/img/news-1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-6 col-12">
                                <h3>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</h3>
                                <div class="space-15"></div>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                                    برای شرایط
                                </p>
                                <div class="space-15"></div>
                                <ul class="news-meta">
                                    <li>
                                        <span class="lnr lnr-user"></span> توسط مدیر
                                    </li>
                                    <li>
                                        <span class="lnr lnr-clock"></span> 26 تیر 1399
                                    </li>
                                </ul>
                                <div class="space-25"></div>
                                <a href="news-single.html" class="btn btn-primary shadow btn-colord btn-theme"><span>بیشتر بخوانید</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>-->
<!-- End Recent News  -->

<!-- Start Download App -->
<!--<section id="download_app" class="download-app padding-100 pb-0 background-fullwidth background-fixed"
         style="background-image: url(assets/img/gray-bg.jpg);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-12" data-aos="fade-left">
                <h2>دریافت نسخه آزمایشی برنامه</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون
                </p>
                <a href="#" class="btn btn-primary shadow btn-colord btn-theme" tabindex="0">
                    <i class="fab fa-apple"></i>
                    <span>دریافت از
                            <br>اپ استور</span>
                </a>
                <a href="#" class="btn btn-primary shadow  btn-colord btn-theme" tabindex="0">
                    <i class="fab fa-google-play"></i>
                    <span>دریافت از
                            <br>گوگل پلی</span>
                </a>
            </div>
            <div class="col-lg-6 col-12" data-aos="fade-right" data-aos-delay="400">
                <img src="assets/img/mobile-6.png" class="img-fluid d-block mx-auto" alt="">
            </div>
        </div>
    </div>
</section>-->
<!-- End Download App -->

<!-- Start  Git in touch -->
<section id="git_in_touch" class="git-in-touch padding-100">
    <div class="container">
        <div class="row">
            <div class="text-center col-12 section-title" data-aos="fade-zoom-in">
                <h3>در <span>ارتباط</span> باشید
                </h3>
                <div class="space-25"></div>
                <p>چنان چه انتقاد، پیشنهاد، پرسش یا هر گونه گفتگویی با تیم اجرایی چارک دارید از راه های ارتباطی زیر پذیرای شما هستیم.</p>
                <div class="space-50"></div>
            </div>
        </div>
        <form data-aos="fade-up" action="{{asset('assets/php/mail.php')}}" method="POST">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="نام خود را وارد کنید" required>
                        <span class="focus-border"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="email" name="email" class="form-control text-left"
                               placeholder="ایمیل خود را وارد کنید" dir="ltr" required>
                        <span class="focus-border"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" name="subject" class="form-control" placeholder="موضوع پیام را وارد کنید">
                        <span class="focus-border"></span>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <textarea name="message" class="form-control" rows="4" placeholder="پیام خود را وارد کنید"
                                  required></textarea>
                        <span class="focus-border"></span>
                    </div>
                </div>
                <div class="col-12">
                    <div class="space-25"></div>
                    <button type="submit" class="btn btn-primary shadow btn-colord btn-theme"><span>ارسال پیام</span>
                    </button>
                </div>
            </div>
        </form>
        <div class="space-50"></div>
        <div class="row contact-info">
            <div class="col-md-4 col-12 text-center">
                <div class="info-box" data-aos="fade-left" data-aos-delay="400">
                    <span class="lnr lnr-map-marker"></span>
                    <h5>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم</h5>
                </div>
            </div>
            <div class="col-md-4 col-12 text-center">
                <div class="info-box" data-aos="fade-up" data-aos-delay="800">
                    <span class="lnr lnr-phone"></span>
                    <h5 dir="ltr">+98 123 456 789</h5>
                    <h5 dir="ltr">+98 123 789 456</h5>
                </div>
            </div>
            <div class="col-md-4 col-12 text-center">
                <div class="info-box" data-aos="fade-right" data-aos-delay="1200">
                    <span class="lnr lnr-envelope"></span>
                    <a href="mailto:info@yourcompany.com">info@4rak.ir</a>
                    <a href="mailto:sales@yourcompany.com">sales@4rak.ir</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End  Git in touch  -->

<!-- Start  Map -->
<section class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1679.3325895422404!2d51.39164665810099!3d32.66835299525123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzLCsDQwJzA2LjEiTiA1McKwMjMnMzMuOSJF!5e0!3m2!1sen!2s!4v1600156927509!5m2!1sen!2s"
            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
            tabindex="0"></iframe>
</section>
<!-- End  Map  -->

<!-- Start  Footer -->
<footer class="padding-100 pb-0">
    <div class="subscribe">
        <div class="container">
            <form class="subscribe-form row m-0 align-items-center" action="#" method="POST">
                <div class="col-lg-9 col-md-8">
                    <div class="form-group mb-0">
                        <input type="email" class="form-control text-left" placeholder="ایمیل خود را وارد کنید"
                               dir="ltr" required>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <button type="submit" class="btn btn-primary shadow d-block w-100 btn-colord btn-theme">
                        <span>اشتراک</span></button>
                </div>
            </form>
        </div>
    </div>
    <div class="space-50"></div>
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="widget">
                        <img src="{{asset('assets/img/fox-logo.png')}}" class="img-fluid" alt="">
                        <p>
                            چارک پلتفرم آنلاین تجارتی است که دغدغه های فروش کالا و مدیریت فروشگاه را برای فروشگاه داران و دغدغه ها و مشکلات خرید کالا و تهیه اجناس مورد نیاز خریداران را با راهکار هایی نو و برخط برطرف می نماید.
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="widget">
                        <h6>دسترسی سریع</h6>
                        <ul>
                            <li>
                                <a href="#">خانه</a>
                            </li>
                            <li>
                                <a href="#">درباره ما</a>
                            </li>
                            <li>
                                <a href="#">خدمات</a>
                            </li>
                            <li>
                                <a href="#">محصولات</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="widget">
                        <h6>شبکه اجتماعی</h6>
                        <ul>
                            <li>
                                <a href="https://t.me/ir4rak">کانال تلگرام</a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/4rak.ir">اینستاگرام</a>
                            </li>
                            <li>
                                <a href="#">لینکدین</a>
                            </li>
                            <li>
                                <a href="#">توییتر</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="widget">
                        <h6>تماس سریع</h6>
                        <ul>
                            <li>
                                <span>تلفن : </span> <span class="ltr-text">0913 717 93 63</span>
                            </li>
                            <li>
                                <span>ایمیل : </span>
                                <a href="#">info@4rak.ir</a>
                            </li>
                            <li>
                                <span>آدرس : </span>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space-50"></div>
    <div class="copyright">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <p>طراحی و اجرا توسط <a href="https://haranet.ir" target="_blank">اینفینیــتیم</a></p>
                </div>
                <div class="offset-md-2 col-md-5">
                    <ul class="nav justify-content-center justify-content-md-end">
                        <li class="nav-item">
                            <a class="nav-link" href="#">قوانین و مقررات</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">سیاست حریم خصوصی</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End  Footer  -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap JS -->
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

<!-- svg -->
<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>

<!-- Slick Slider JS -->
<script src="{{asset('assets/js/slick.min.js')}}"></script>

<!-- Counterup JS -->
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.counterup.js')}}"></script>

<!-- AOS JS -->
<script src="{{asset('assets/js/aos.js')}}"></script>

<!-- lity JS -->
<script src="{{asset('assets/js/lity.min.js')}}"></script>

<!-- Our Main JS -->
<script src="{{asset('assets/js/main.js')}}"></script>

</body>

</html>
