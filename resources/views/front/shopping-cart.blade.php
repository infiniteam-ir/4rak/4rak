<!DOCTYPE html>
<html lang="en" id="html" class="bg-light">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>سبد خرید</title>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    <!--Css and Bootstrap-->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('asset/css/w3s.css')}}">
    <!-- <link href="asset/css/uikit.min.css" rel="stylesheet">-->
    <link href="{{asset('asset/css/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/css/all.min.css')}}" rel="stylesheet">
    <!--Private for This page-->
    <link href="{{asset('asset/css/mall.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
</head>
<body id="main" class="bg-light">
<!--bottom badge fixed-->

<!--End bottom badge fixed-->
<!--Layer for close side bar-->
<div class="d-none" id="layer" onclick="closeNav()"></div>
<!--End Layer for close side bar-->
<!-- Start first Header -->
<header class="bg-white shadow rounded d-none d-lg-block" id="firstHeader">
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu1">
                <ul class="navbar-nav ml-auto col-md-4">
                    <li class="nav-item ml-4">
                        <a href="" class="nav-link disabled text-muted">
                            <i class="fas fa-headset text-warning ml-1"></i>
                            <span>09137179363</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="mailto:info@4rak.ir">
                            <i class="fas fa-at text-warning ml-1"></i>
                            <span>4rak.ir</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav mt-0 col-md-8 justify-content-end">
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-shipping-fast text-warning ml-1"></i>
                            <span>پیگیری سفارشات</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-question text-warning ml-1"></i>
                            <span>سوالات متدوال</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-phone text-warning ml-1"></i>
                            <span>تماس با ما</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu2">
                <ul class="navbar-nav ml-5 col-md-2">
                    <li class="nav-item">
                        <a class="navbar-brand mr-0" href="{{route('about')}}">
                            <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 150px"
                                 class="bg-warning rounded" alt="">
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto col-md-4">
                    <div class="input-icons pl-0">
                        <i class="fa fa-search icon"></i>
                        <i class="fab fa-searchengin icon2 fa-lg"></i>
                        <input class="form-control rounded-right" id="searchBox" type="search"
                               placeholder="جست و جو در چارک..." style="font-size: medium" aria-label="Search">
                    </div>
                    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

                </ul>
                @guest()
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('login')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    ورود / عضویت
                                </button>
                            </form>
                        </li>
                    </ul>
                @else
                    <ul class="navbar-nav ml-auto col-md-3 mr-5">
                        <li class="nav-item">
                            <form action="{{route('Profile.index')}}" method="get">
                                <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                            class="fa fa-user-alt ml-2"></i>
                                    پروفایل
                                </button>
                            </form>
                        </li>
                    </ul>
                @endguest
                <ul class="navbar-nav ml-auto col-md-3">
                    <a class="pop" href="{{route('ShoppingCart')}}" data-container="body" data-toggle="popover"
                       id="navLink"
                       data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge">
                            <i class="fa fa-shopping-bag fa-2x bad"></i>
                            <span class="text-warning mr-1 pb-5">
                                  <span> {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}</span>
                            </span>
                            <i class="fa fa-shopping-bag fa-2x mt-2 text-white"></i>

                        </span>
                    </a>
                    <a class="pop mr-5" href="#" data-container="body" data-toggle="popover" id="navLink"
                       data-placement="bottom" data-content="مورد علاقه ها">
                        <span class="badge"><i class="fa fa-heart fa-2x bad"></i><span
                                    class="text-warning"></span></span>

                    </a>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu3">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('allShops')}}">لیست فروشگاه ها
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="{{route('store')}}">خانه
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#about">درباره</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#main_features">ویژگی ها</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End First Header -->
<!--Start Responsive Header-->
<header class="bg-white shadow rounded d-lg-none" id="responsiveHeader">
    <nav class="navbar navbar-expand navbar-light  d-lg-none">
        <div class="container-fluid">
            <div class="collapse navbar-collapse row" id="response_menu">
                <div class="navbar-nav col-11 col-sm-3 d-flex justify-content-center pr-0">
                    <a class="navbar-brand" href="#">
                        <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 50px" class="" alt="">
                    </a>
                </div>
                <div class="navbar-nav mr-0 col-12 col-sm-6 mt-2 mt-sm-0 row">
                    <a class="pop col-4 d-flex justify-content-center text-muted" href="{{route('ShoppingCart')}}"
                       data-container="body"
                       data-toggle="popover" data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge"><i class="fa fa-shopping-basket fa-2x"></i><span
                                    class="text-warning mr-1 pb-5">
                                {{Session::has('cart') ? Session::get('cart')->totalQty : "0"}}
                            </span></span>
                    </a>
                    {{-- <a class="pop col-4 d-flex justify-content-center text-muted" href="#" data-container="body"
                        data-toggle="popover"
                        data-placement="bottom" data-content="مورد علاقه ها">
                         <span class="badge"><i class="fa fa-heart fa-2x"></i><span class="text-warning">9</span></span>
                     </a>--}}
                    <a class="col-4 d-flex justify-content-center text-muted" href="#">
                        <i class="fa fa-search fa-2x bad"></i>

                    </a>
                </div>
                <div class="navbar-nav col-12 uk-position-top-left position-fixed mt-3 ml-4 col-sm-3 justify-content-end ml-1">
                    <div id="mySidenav" class="sidenav">

                        <a class="" href="{{route('allShops')}}">فروشگاه ها</a>
                        <a class="" href="{{route('store')}}">خانه</a>
                        <a class="" href="#about">درباره</a>
                        <a class="" href="#main_features">ویژگی ها</a>
                        <a class="" href="#screenshots">تصاویر</a>
                        <a class="" href="#team">تیم</a>

                    </div>
                    <!-- Use any element to open the sidenav -->
                    <i class="fa fa-bars fa-2x text-danger" onclick="openNav()"></i>
                    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
                </div>
            </div>
        </div>
    </nav>
</header>
<!--End Responsive Header-->
<!--Main Content-->
<div class="container">
    <div class="container">
        <div class="row  d-flex justify-content-center ">
            <div class="col-10  text-right p-3 buy-box-shadow">
                @if(Session::has('Error'))
                    <div class="alert alert-danger">
                        {{Session('Error')}}
                    </div>
                @endif
            </div>
            <div class="col-10 bg-white text-right p-3 mt-5 text-center buy-box-shadow border mb-5">
                @if(Session::has('cart'))
                    <h4>سبد خرید</h4>
                    <table class="table table-bordered table-responsive-md">
                        <tr>
                            <th>ردیف</th>
                            <th>محصول</th>
                            <th>قیمت</th>
                            <th>تعداد</th>
                            <th>مجموع</th>
                            <th>فروشگاه</th>
                            <th>حذف</th>
                        </tr>
                        @php($sum=0)
                        @php($id=1)
                        @foreach($products as $product)
                            @php($sum+= $product['Item']->sell_price*$product['Qty'])
                            <tr id="{{$product['Item']->id}}" class="pro-row">
                                <td>{{$id}}</td>
                                <td>{{$product['Item']->name}}</td>
                                <td> {{$product['Item']->sell_price}} تومان</td>
                                <td><i id="{{$product['Item']->id}}" style="cursor: pointer" class="inc fa fa-plus"
                                       onclick=""></i>
                                    <span id="{{$product['Item']->id}}" class="pro-qty mx-2">{{$product['Qty']}}</span>
                                    <i id="{{$product['Item']->id}}" style="cursor: pointer" class="dec fa fa-minus"
                                       onclick=""></i></td>
                                <td>{{$product['Item']->sell_price*$product['Qty']}} تومان</td>
                                <td><a href="{{route('singleshop',$product['Item']->shop_id)}}">
                                        {{$product['Item']->shop->name}}
                                    </a></td>
                                <td><i id="{{$product['Item']->id}}" style="cursor: pointer" class="del fa fa-times"
                                       onclick=""></i></td>
                            </tr>
                            @php($id++)
                        @endforeach
                        <tr>
                            <td colspan="6">
                                <div class="row">
                                    <div class="col">
                                        {!! Form::open(['method'=>'POST','action'=>'ShopController@deleteCart']) !!}
                                        <button class="btn btn-primary" type="submit">خالی کردن سبد خرید</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    @if(Auth::user()->user_info_id==null)
                        @php($user=Auth::user())
                        {{--{!! Form::model($user,['method'=>'PATCH','action'=>['ProfileController@update',$user->id],'files'=>true] ) !!}--}}
                        <div id="null-info" class="row p-4 mx-3 d-flex " style="border: 1px #c9c9c9 solid">
                            <h4 class="alert alert-warning text-center w-100">
                                برای ادامه خرید لطفا اطلاعات خود را وارد کنید.
                            </h4>
                            <div class="row w-100 justify-content-center align-items-center ">
                                <div class="col-12 mt-3 col-md-4">
                                    <h6 class="text-muted">تلفن ثابت:</h6>
                                    {!! Form::text('phone',null,['class'=>'form-control','id'=>'phone']) !!}
                                </div>
                                <div class="col-12 mt-3 col-md-4">
                                    <h6 class="text-muted">استان:</h6>
                                    {!! Form::text('province',null,['class'=>'form-control','id'=>'province']) !!}
                                </div>
                                <div class="col-12 mt-3 col-md-4">
                                    <h6 class="text-muted">شهر:</h6>
                                    {!! Form::text('city',null,['class'=>'form-control','id'=>'city']) !!}
                                </div>
                                <div class="col-12 mt-3 col-md-4">
                                    <h6 class="text-muted">آدرس دقیق پستی:</h6>
                                    {!! Form::text('address',null,['class'=>'form-control','id'=>'address']) !!}
                                </div>
                            </div>
                            <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                <div class="col-12 mt-3 col-md-4 text-center">
                                    {!! Form::submit('ذخیره اطلاعات',['class'=>'form-control btn btn-success w-50','id'=>'set-info']) !!}
                                </div>
                            </div>
                        </div>
                        {{--{!! Form::close() !!}--}}
                    @endif
                    <div class="row d-flex justify-content-center text-center">
                        <h4 class="mt-3 w-25 border-1-gainsboro">مجموع سبد خرید</h4>
                    </div>
                    {!! Form::open(['method'=>'POST','action'=>'ShopController@payment']) !!}
                    <div class="row d-flex justify-content-center">
                        <table class="table table-bordered w-50 ">
                            <tr>
                                <th>قیمت کل</th>
                                @php($total=$sum)
                                <th>
                                    {!! Form::label('total',$total)!!} تومان
                                    {!! Form::text('total',$total,['class'=>' d-none']) !!}
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <button id="payment-btn" disabled="disabled" class="btn btn-success" type="submit" >پرداخت</button>
                    </div>
                    {!! Form::close() !!}
                @else
                    <span class="fa fa-shopping-cart"></span>
                    <h4>سبد خرید شما خالی است</h4>
                @endif
            </div>
        </div>
    </div>
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

</div>
<!-- Start  Footer -->
<div class="container-fluid">
    <div class="row">
        <footer class="padding-100 pb-0 container-fluid w-100">
            <div class="subscribe">
                <div class="container mb-5">
                    <form class="subscribe-form row m-0 align-items-center" action="#" method="POST">
                        <div class="col-lg-9 col-md-8">
                            <div class="form-group mb-0">
                                <input type="email" class="form-control text-left" placeholder="ایمیل خود را وارد کنید"
                                       dir="ltr" required>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4">
                            <button type="submit" class="btn btn-primary shadow d-block w-100 btn-colord btn-theme">
                                <span>اشتراک</span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <img src="{{asset('assets/img/fox-logo.png')}}" class="img-fluid bg-dark rounded-circle"
                                     alt="">
                                <p> چارک پلتفرم آنلاین تجارتی است که دغدغه های فروش کالا و مدیریت فروشگاه را برای
                                    فروشگاه داران و دغدغه ها و مشکلات خرید کالا و تهیه اجناس مورد نیاز خریداران را با
                                    راهکار هایی نو و برخط برطرف می نماید.

                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>دسترسی سریع</h6>
                                <ul>
                                    <li>
                                        <a href="#">خانه</a>
                                    </li>
                                    <li>
                                        <a href="#">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="#">خدمات</a>
                                    </li>
                                    <li>
                                        <a href="#">محصولات</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>شبکه اجتماعی</h6>
                                <ul>
                                    <li>
                                        <a href="https://t.me/ir4rak">کانال تلگرام</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/4rak.ir">اینستاگرام</a>
                                    </li>
                                    <li>
                                        <a href="#">لینکدین</a>
                                    </li>
                                    <li>
                                        <a href="#">توییتر</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>تماس سریع</h6>
                                <ul>
                                    <li>
                                        <span>تلفن : </span> <span class="ltr-text">0913 233 89 38</span>
                                    </li>
                                    <li>
                                        <span>ایمیل : </span>
                                        <a href="#">info@4rak.ir</a>
                                    </li>
                                    <li>
                                        <span>آدرس : </span>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="copyright">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <p>طراحی و اجرا توسط <a class="text-light" href="https://haranet.ir" target="_blank">اینفینیــتیم</a>
                            </p>
                        </div>
                        <div class="offset-md-2 col-md-5">
                            <ul class="nav justify-content-center justify-content-md-end">
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">قوانین و مقررات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-light" href="#">سیاست حریم خصوصی</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- End  Footer  -->
<!--End Main Content-->

</body>
<!-- jQuery -->
<script src="{{asset('asset/js/jQuery.min.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('asset/js/uikit.min.js')}}"></script>
<script src="{{asset('asset/js/uikit-icons.min.js')}}"></script>
<!--js For this page-->
<script src="{{asset('asset/js/mall.js')}}"></script>

<script>
    $(document).ready(function () {
        var info_div=document.getElementById("null-info")
        if(info_div==null){
            document.getElementById("payment-btn").removeAttribute('disabled')
        }
z
        $('.inc').click(function (event) {
            var id = event.target.id;
            var span = $('#' + id + '.pro-qty');
            var newQty = parseInt(span.text()) + 1;

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: ajaxUrl,
                data: {// change data to this object
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    do: 'inc-card-pro',
                    id: id
                },
                dataType: 'json',
                success: function (response) {

                    // span.text(newQty);
                    location.reload();

                },
                error: function (response) {
                    console.log(response)
                }
            });
        });
        $('.dec').click(function (event) {
            var id = event.target.id;
            var span = $('#' + id + '.pro-qty');
            var newQty = parseInt(span.text()) - 1;
            if (newQty > 0)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'dec-card-pro',
                        id: id
                    },
                    dataType: 'json',
                    success: function (response) {
                        // span.text(newQty);
                        location.reload();
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
        });
        $('.del').click(function (event) {
            var id = event.target.id;
            var trow = $('#' + id + '.pro-row');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: ajaxUrl,
                data: {// change data to this object
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    do: 'del-card-pro',
                    id: id
                },
                dataType: 'json',
                success: function (response) {
                    location.reload();
                    console.log(response)
                },
                error: function (response) {
                    console.log(response)
                }
            });
        });
        $('#set-info').click(function () {
            var phone=document.getElementById("phone").value
            var province=document.getElementById("province").value
            var city=document.getElementById("city").value
            var address=document.getElementById("address").value
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: ajaxUrl,
                data: {// change data to this object
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    do: 'set-user-info',
                    phone: phone,
                    province: province,
                    city: city,
                    address: address
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    location.reload()
                },
                error: function (response) {
                    console.log(response)
                }
            });

        })

    });
</script>


</html>
