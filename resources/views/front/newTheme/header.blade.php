<header class="site-header d-none d-xl-block d-lg-block">
    <div data-elementor-type="header" data-elementor-id="1562"
         class="elementor elementor-1562 elementor-location-header" data-elementor-settings="[]">
        <div class="elementor-section-wrap">
            <section
                class="elementor-section elementor-top-section elementor-element elementor-element-80c8889 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                data-id="80c8889" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-extended">
                    <div class="elementor-row">
                        <div
                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-6b78921"
                            data-id="6b78921" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-7fa6280 elementor-widget elementor-widget-image"
                                        data-id="7fa6280" data-element_type="widget" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-image">
                                                {{--LOGO--}}
                                                <a href="{{route('store')}}">
                                                    <img width="143" height="58" src="{{asset('img/4rak-logo.png')}}"
                                                         class="attachment-large size-large" alt="" loading="lazy"
                                                         srcset="{{asset('img/4rak-logo.png')}} 143w, {{asset('img/4rak-logo.png')}} 50w"
                                                         sizes="(max-width: 143px) 100vw, 143px"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-1bacd51"
                            data-id="1bacd51" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-4dc7224 elementor-widget elementor-widget-negarshop_header_search"
                                        data-id="4dc7224" data-element_type="widget"
                                        data-widget_type="negarshop_header_search.default">
                                        <div class="elementor-widget-container">
                                            <div
                                                class="header-search search-in-product text-right darken-color-mode style-3 style-4 whiter">
                                                {{-- SEARCH--}}
                                                <div data-type="product" class="search-box btn-style-dark ajax-form">
                                                    <form class="form-tag" action="#">
                                                        <input type="hidden" name="post_type" value="product"/>
                                                        <input type="search" autocomplete="off" name="s" value=""
                                                               class="search-input search-field"
                                                               placeholder="جستجو در محصولات ...">

                                                        <div class="action-btns">
                                                            <button class="action-btn search-filters" type="button"><i
                                                                    class="flaticon-settings-1"></i></button>
                                                            <button class="action-btn search-submit" type="submit"><i
                                                                    class="far fa-search"></i>
                                                            </button>
                                                        </div>

                                                        <div class="search-options">
                                                            <button class="close-popup" type="button"><i
                                                                    class="fal fa-check"></i></button>
                                                            <div class="filters-parent">
                                                                <div class="list-item">
                                                                    <label for="header-search-cat">دسته بندی ها</label>
                                                                    <select name="pcat" id="header-search-cat"
                                                                            class="negar-select">
                                                                        <option value="" selected>همه دسته ها</option>
                                                                        @foreach($categories as $cat)
                                                                            <option
                                                                                value="{{$cat->id}}">{{$cat->title}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="list-item">
                                                                    <label for="header-search-stock">وضعیت محصول</label>
                                                                    <select name="stock" id="header-search-stock"
                                                                            class="negar-select">
                                                                        <option value="" selected>همه محصولات</option>
                                                                        <option value="instock">فقط موجود ها</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <ul class="search-result"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-bcd4445"
                            data-id="bcd4445" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-2b720eb elementor-widget elementor-widget-negarshop_header_account"
                                        data-id="2b720eb" data-element_type="widget"
                                        data-widget_type="negarshop_header_account.default">
                                        <div class="elementor-widget-container">
                                            <div class="header-account text-left style-1">
                                                <div
                                                    class="account-box text-center align-items-center justify-content-center">
                                                    @guest()
                                                        <i class="flaticon-avatar"></i>
                                                    @else
                                                        @if(\Illuminate\Support\Facades\Auth::user()->photo)
                                                            <img width="50px" height="50px"
                                                                 style="max-height: 50px;min-width: 50px"
                                                                 class="img-fluid rounded-circle"
                                                                 src="{{asset('images/profile/'.\Illuminate\Support\Facades\Auth::user()->photo->path)}}"
                                                                 alt="">
                                                        @else
                                                            <img width="50px" height="50px" class="img-fluid"
                                                                 src="{{asset('img/user-avatar.svg')}}" alt="">
                                                        @endif
                                                    @endguest
                                                    @guest
                                                        <span class="subtitle">لطفا وارد حساب خود شوید!</span>

                                                        <ul class="account-links">
                                                            <li>
                                                                <a href="{{route('login')}}" data-toggle="modal"
                                                                   data-target="#login-popup-modal">ورود به حساب</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('login')}}">ثبت نام</a>
                                                            </li>
                                                        </ul>
                                                    @else
                                                        <span
                                                            class="subtitle">{{Auth::user()->name}} {{Auth::user()->family}}</span>

                                                        <ul class="account-links">
                                                            <li>
                                                                <a href="{{route('Profile.index')}}" data-toggle="modal"
                                                                   data-target="#login-popup-modal">پروفایل</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('dashboard')}}"> پنل کاربری</a>
                                                            </li>
                                                            <li>
                                                                <form method="POST" action="{{route('logout')}}">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-danger w-100">خروج</button>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    @endguest
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section
                class="elementor-section elementor-top-section elementor-element elementor-element-286854c elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                data-id="286854c" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-narrow">
                    <div class="elementor-row">
                        <div
                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-a24c8ec"
                            data-id="a24c8ec" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-38f7b83 elementor-widget elementor-widget-negarshop_header_menu"
                                        data-id="38f7b83" data-element_type="widget"
                                        data-widget_type="negarshop_header_menu.default">
                                        <div class="elementor-widget-container">
                                            <div class="">
                                                <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                    <div class="row align-items-center">
                                                        <div class="col-auto header-main-menu vertical-menu">
                                                            <button class="vertical-menu-toggle"><i class="fal fa-bars"></i>دسته بندی ها</button>
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c-%d8%a2%db%8c%da%a9%d9%86-%d8%af%d8%a7%d8%b1"
                                                                class="main-menu category-menu">
                                                                @foreach($categories as $cat)
                                                                    @if($cat->parent_id==null && $cat->shop_id==0)
                                                                        @if(Route::CurrentRouteName()=='blog' ||Route::CurrentRouteName()=='article' ||Route::CurrentRouteName()=='blogCategory')
                                                                            @php($route=route('blogCategory', [$cat->title,$cat->id]))
                                                                        @else
                                                                            @php($route=route('advanceCategory', ['همه','همه',$cat->title]))
                                                                        @endif
                                                                        <li id="menu-item-{{$cat->id}}" data-col="4"
                                                                            data-id="{{$cat->id}}"
                                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-{{$cat->id}}"
                                                                            data-col="4" data-id="{{$cat->id}}">
                                                                            <a href="{{$route}}">
                                                                                @if($cat->photo)
                                                                                    <i class="item-icon"
                                                                                       style="background-image: url('{{asset('images/cats/'.$cat->photo->path)}}');"></i>
                                                                                @else
                                                                                    <i class="item-icon fab fa-microsoft"></i>
                                                                                @endif
                                                                                {{$cat->title}}
                                                                            </a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach

                                                            </ul>

                                                        </div>
                                                        <div
                                                            class="col header-main-menu-col header-main-menu text-right">
                                                            <ul>
                                                                <li id="menu-item-357"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357">
                                                                    <a href="{{route('blog')}}">وبلاگ</a></li>
                                                                <li id="menu-item-359"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-359">
                                                                    <a href="{{route('allShops')}}">لیست فروشگاه ها</a>
                                                                </li>
                                                                <li id="menu-item-358"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                    <a href="{{route('guilds')}}">اصناف</a></li>
                                                                <li id="menu-item-358"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                    <a href="{{route('advanceCategory',['همه','همه','همه'])}}">گروه
                                                                        های کالا</a></li>

                                                                <li id="menu-item-360"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                    <a href="{{route('Profile.index')}}">حساب من</a>
                                                                </li>
                                                                <li id="menu-item-360"
                                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                    <a href="{{route('about')}}"> درباره ما</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5fa7d22"
                            data-id="5fa7d22" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-519427f elementor-widget elementor-widget-negarshop_header_basket"
                                        data-id="519427f" data-element_type="widget"
                                        data-widget_type="negarshop_header_basket.default">
                                        <div class="elementor-widget-container">
                                            <div class="header-cart-basket style-2 child-hover-right">
                                                <a href="{{route('ShoppingCart')}}"
                                                   class="cart-basket-box cart-customlocation">
                                                    <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                    <span class="title">سبد خرید</span>
                                                    <span class="subtitle">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <bdi><span class="woocommerce-Price-currencySymbol">تومان</span>
                                                                    <span
                                                                        id="total-price">{{Session::has('cart') ? number_format(Session::get('cart')['totalPrice']) : "0"}}</span></bdi>
                                                            </span>
                                                        </span>
                                                    <span id="total-qty"
                                                          class="count">{{Session::has('cart') ? Session::get('cart')['totalQty'] : "0"}}</span>
                                                </a>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</header>
<header class="responsive-header d-block d-xl-none d-lg-none">
    <div data-elementor-type="section" data-elementor-id="2188"
         class="elementor elementor-2188 elementor-location-responsive-header" data-elementor-settings="[]">
        <div class="elementor-section-wrap">
            <section
                class="elementor-section elementor-top-section elementor-element elementor-element-345f71c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                data-id="345f71c" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div
                            class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-79a8412"
                            data-id="79a8412" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-082d926 elementor-widget elementor-widget-negarshop_responsive_menu"
                                        data-id="082d926" data-element_type="widget"
                                        data-widget_type="negarshop_responsive_menu.default">
                                        <div class="elementor-widget-container">
                                            <div class="responsive-menu">
                                                <button class="btn btn-transparent toggle-menu"><i
                                                        class="far fa-bars"></i></button>
                                                <div class="menu-popup">
                                                    <nav class="responsive-navbar">
                                                        <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c" class="categories-menu">
                                                            @foreach($categories as $cat)
                                                                <li id="menu-item-190" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-{{$cat->id}}">
                                                                    <a href="{{route('advanceCategory', ['همه','همه',$cat->title])}}">
                                                                        {{$cat->title}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="overlay"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6503f75"
                            data-id="6503f75" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-974f707 elementor-widget elementor-widget-image"
                                        data-id="974f707" data-element_type="widget" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-image">
                                                <a href="{{route('store')}}">
                                                    <img width="143" height="58" src="{{asset('img/4rak-logo.png')}}"
                                                         class="attachment-full size-full" alt="" loading="lazy"
                                                         srcset="{{asset('img/4rak-logo.png')}} 143w, {{asset('img/4rak-logo.png')}} 50w"
                                                         sizes="(max-width: 143px) 100vw, 143px"/> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-18a1aeb"
                            data-id="18a1aeb" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-704fc73 elementor-view-default elementor-widget elementor-widget-icon"
                                        data-id="704fc73" data-element_type="widget" data-widget_type="icon.default">
                                        <div class="elementor-widget-container">
                                            <div class="elementor-icon-wrapper">
                                                <a class="elementor-icon" href="{{route('dashboard')}}">
                                                    <i aria-hidden="true" class="far fa-user"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-87822a2"
                            data-id="87822a2" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-92646d4 elementor-widget elementor-widget-negarshop_header_basket"
                                        data-id="92646d4" data-element_type="widget"
                                        data-widget_type="negarshop_header_basket.default">
                                        <div class="elementor-widget-container">
                                            <div class="header-cart-basket style-3 child-hover-right">
                                                <a href="{{route('ShoppingCart')}}"
                                                   class="cart-basket-box cart-customlocation">
                                                    <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                    <span class="title">سبد خرید</span>
                                                    <span class="subtitle">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <bdi><span class="woocommerce-Price-currencySymbol">تومان</span>
                                                                    <span
                                                                        id="total-price">{{Session::has('cart') ? Session::get('cart')['totalPrice'] : "0"}}</span></bdi>
                                                            </span>
                                                        </span>
                                                    <span id="total-qty"
                                                          class="count">{{Session::has('cart') ? Session::get('cart')['totalQty'] : "0"}}</span>
                                                </a>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-row align-items-center mt-2 bg-light text-center">
                        <div class="elementor-column  align-items-center elementor-col-50 elementor-top-column elementor-element elementor-element-4190cd5" data-id="4190cd5" data-element_type="column">
                            <div class="elementor-column-wrap align-items-center elementor-element-populated">
                                <div class="elementor-widget-wrap align-items-center">
                                    <div class="elementor-element align-items-center elementor-element-3dfebc5 elementor-widget elementor-widget-negarshop_header_menu" data-id="3dfebc5" data-element_type="widget" data-widget_type="negarshop_header_menu.default">
                                        <div class="elementor-widget-container align-items-center">
                                            <div class="">
                                                <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                    <div class="row align-items-center">
                                                        <div class="col header-main-menu-col header-main-menu text-center">
                                                            <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d9%86%d9%88%d8%a7%d8%b1-%d8%a8%d8%a7%d9%84%d8%a7%db%8c%db%8c" class="main-menu">
                                                                <li id="menu-item-357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357">
                                                                    <a href="{{route('blog')}}" >وبلاگ</a></li>
                                                                <li id="menu-item-359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-359">
                                                                    <a href="{{route('allShops')}}" >لیست فروشگاه ها</a></li>
                                                                <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                    <a href="{{route('guilds')}}" >اصناف</a></li>
                                                                <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                    <a href="{{route('advanceCategory',['همه','همه','همه'])}}" >گروه های کالا</a></li>

                                                                <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                    <a href="{{route('about')}}" > درباره ما</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <section
                class="elementor-section elementor-top-section elementor-element elementor-element-275eab9 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                data-id="275eab9" data-element_type="section"
                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-row">
                        <div
                            class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28b1d92"
                            data-id="28b1d92" data-element_type="column">
                            <div class="elementor-column-wrap elementor-element-populated">
                                <div class="elementor-widget-wrap">
                                    <div
                                        class="elementor-element elementor-element-7ea9db8 elementor-widget elementor-widget-negarshop_header_search"
                                        data-id="7ea9db8" data-element_type="widget"
                                        data-widget_type="negarshop_header_search.default">
                                        <div class="elementor-widget-container">
                                            <div
                                                class="header-search search-in-product text-right darken-color-mode style-5 whiter">
                                                <div data-type="product" class="search-box btn-style-dark ajax-form">
                                                    <form class="form-tag" action="#">
                                                        <input type="hidden" name="post_type" value="product"/>
                                                        <input type="search" autocomplete="off" name="s" value=""
                                                               class="search-input search-field"
                                                               placeholder="جستجو در محصولات ...">

                                                        <div class="action-btns">
                                                            <button class="action-btn search-submit" type="submit"><i
                                                                    class="far fa-search"></i>
                                                            </button>
                                                        </div>

                                                    </form>
                                                    <ul class="search-result"></ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</header>
