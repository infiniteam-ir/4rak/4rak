@extends('front.newTheme.master')
@section('title','محصولات')
@section('main')
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <nav class="woocommerce-breadcrumb"><i class="flaticon-placeholder"></i><a href="{{route('store')}}">خانه</a><span>&nbsp;&#47;&nbsp;</span><span class="current">محصولات</span></nav><div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header">
                            <h1 class="woocommerce-products-header__title page-title">محصولات </h1>
                        </header>
                        <div class="woocommerce-notices-wrapper"></div>
                        <p class="woocommerce-result-count">
                            نمایش دادن همه <i id="res_qty">{{count($products)}}</i> نتیجه</p>

                        <div class="clear">
                        </div>
                        <div id="shops-list" class="products columns-3 columns-res-2">
                            @if(count($products)>0)
                            @foreach($products as $product)
                                <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">
                                    <figure class="thumb text-center">
                                        <a class="text-center" href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                            @if($product->photo)
                                            <img width="300" height="300" src="{{asset('images/'.$product->photo->path)}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="{{asset('images/'.$product->photo->path)}} 300w, {{asset('images/'.$product->photo->path)}} 150w, {{asset('images/'.$product->photo->path)}} 100w, {{asset('images/'.$product->photo->path)}} 600w, {{asset('images/'.$product->photo->path)}} 96w" sizes="(max-width: 300px) 100vw, 300px" />
                                        @else
                                                <img width="300" height="300" src="{{asset('img/default.jpg')}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="{{asset('img/default.jpg')}} 300w, {{asset('img/default.jpg')}} 150w, {{asset('img/default.jpg')}} 100w, {{asset('img/default.jpg')}} 600w, {{asset('img/default.jpg')}} 96w" sizes="(max-width: 300px) 100vw, 300px" />
                                        @endif
                                    </figure>
                                    <div class="title">
                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}"><h6>{{$product->name}}</h6></a>
                                    </div>
                                    <div class="product-variables text-center ">
                                        <p class="text-muted font-weight-lighter">{{number_format($product->sell_price)}}</p>
                                        <p class="text-muted font-weight-lighter">{{$product->category->title}}</p>
                                    </div>
                                </article>
                            @endforeach
                                <div class="row mt-3 d-flex justify-content-center mt-3">{{$products->links()}}</div>


                            @else
                                <div class="alert alert-danger text-center py-5">
                                    <h4 class="text-info">
                                        محصولی یافت نشد
                                    </h4>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2" class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6>دیگر دسته بندی ها</h6></header>
                                <ul class="list-unstyled">
                                    @foreach($categories as $cat)
                                        <li class="w-100 my-2">
                                        <!--                                            <button class="cat-title btn" id="{{$cat->id}}" type="button">
                                                {{$cat->title}}
                                            </button>-->
                                            <a class="" href="{{route('advanceCategory', ['همه','همه',$cat->title])}}">
                                                @if($cat->photo)
                                                    <img class="img-fluid mx-2" style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%" src="{{asset('images/'.$cat->photo->path)}}" alt="">
                                                @else
                                                    <img class="img-fluid mx-2" style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%" src="{{asset('images/1608550517default.jpg')}}" alt="">
                                                @endif
                                                {{$cat->title}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </section>
                        </aside>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i class="far fa-filter"></i></button>
                    </div>
                </div>
        </main>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function (){

            var ajaxUrl = $('.front-ajax.d-none').attr('id')
            $('#province').on("change",function (){
                var province = $('#province').val()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            document.getElementById('city').value = re['id']
                        })
                        console.log(document.getElementById('city'))
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })

            $('#search-city').on("click",function (){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-shops-by-province',
                        province: $('#province').val(),
                        city: $('#city').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')

                        var data=''
                        var path
                        var count_res=0
                        var x='{{route('singleshop',0)}}'
                        response.forEach(function (re){
                            var res = x.replace("0", re['unique_name']);
                           if (re['logo'])
                                path='images/logos/'+re['logo']['path']
                            else
                               path='img/logo.jpg'

                            if (re['name']){
                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+res+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+res+'"><h6>'+re['name']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter">'+re['subguild']['name']+'</p>'+
                                    '</div>'+
                                    '</article>';
                            }
                              count_res++
                        });
                        list.empty()
                        list.append(data)

                        $('#res_qty').empty()
                        $('#res_qty').append(count_res)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })

            $('#search-guild').on("click",function (){
                var guild_id=$('#guilds').val()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-shops-by-guild',
                        id: guild_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')
                        var data=''
                        var path
                        var count_res=0
                        var x='{{route('singleshop',0)}}'
                        response.forEach(function (re){
                            var res = x.replace("0", re['unique_name']);
                            if (re['logo'])
                                path='images/logos/'+re['logo']['path']
                            else
                                path='img/logo.jpg'

                            if (re['name']){
                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+res+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+res+'"><h6>'+re['name']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter">'+re['subguild']['name']+'</p>'+
                                    '</div>'+
                                    '</article>';
                            }
                            count_res++
                        });
                        list.empty()
                        list.append(data)

                        $('#res_qty').empty()
                        $('#res_qty').append(count_res)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })


        })
    </script>
    @endsection
