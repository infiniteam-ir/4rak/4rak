@extends('front.newTheme.master')
@section('title','اصناف')
@section('main')
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <nav class="woocommerce-breadcrumb"><i class="flaticon-placeholder"></i><a href="{{route('store')}}">خانه</a><span>&nbsp;&#47;&nbsp;</span><span class="current">فروشگاه ها</span></nav><div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header" >
                            @if(count($guilds)>0)
                            <ul class="nav px-0 d-flex flex-nowrap  " style="overflow-x: auto">
                                @foreach($guilds as $guild)
                                <li class="nav-item text-center my-3 ">
                                    @if($guild->photo)
                                        <img src="{{asset('images/'.$guild->photo->path)}}" class="img-fluid rounded" style="max-height: 50px;max-width: 50px;min-width: 50px;min-height: 50px">
                                    @else
                                        <img src="{{asset('img/guild.svg')}}" class="img-fluid " style="max-height: 50px;max-width: 50px">
                                    @endif
                                    <a class="guild nav-link d-flex flex-nowrap " style="white-space: nowrap" id="{{$guild->id}}" aria-current="page" href="#">{{$guild->name}}</a>
                                </li>
                                @endforeach
                            </ul>
                            @else
                            <h6 class="text-center text-muted">صنفی جهت نمایش وجود ندارد</h6>
                                @endif
                        </header>

                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="clear">
                        </div>
                        <div id="shops-list" class="products columns-3 columns-res-2 text-center">
                            <div class="alert alert-danger py-5">
                                <h4 class="text-info">
                                    برای نمایش فروشگاه ها از قسمت فیلتر ها اقدام کنید یا روی یکی از اصناف فوق کلیک کنید
                                </h4>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2" class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6>جستجوی پیشرفته: </h6></header>
                                <p class="text-muted font-weight-light" style="font-size: small"> ابتدا استان را انتخاب کنید<i class="text-danger">*</i> </p>
                                {!! Form::select('province',$arr,null,['placeholder'=>'انتخاب استان','class'=>'uk-select rounded','id'=>'province']) !!}
                                <label class="d-none uk-label-success rounded p-2 mt-2 w-100"  style="font-size: smaller" id="guilds-in-province"></label>
                                <label class="d-none uk-label-success rounded p-2 mb-4 w-100"  style="font-size: smaller" id="shops-in-province"></label>

                                <label class="d-none  text-muted rounded p-2 mb-0  w-100"  style="font-size: smaller" id="msg1">شهر را انتخاب کنید<i class="text-danger">*</i></label>
                                <select id="city"  name="city " class="d-none mt-0 uk-select rounded mb-2"></select>
                                <label class="d-none uk-label rounded p-2 mt-2 w-100"  style="font-size: smaller" id="guilds-in-city"></label>
                                <label class="d-none uk-label rounded p-2 mb-4 w-100"  style="font-size: smaller" id="shops-in-city"></label>

                                <label class="d-none  text-muted rounded p-2 mb-0  w-100"  style="font-size: smaller" id="msg2">صنف را انتخاب کنید<i class="text-danger">*</i></label>
                                <select id="guild"  name="guild " class="d-none mt-0 uk-select rounded mb-2">
                                    <option value="">انتخاب صنف...</option>
                                </select>
                                <label class="d-none uk-label rounded p-2 mb-4 w-100"  style="font-size: smaller" id="subguilds"></label>

                                <label class="d-none  text-muted rounded p-2 mb-0  w-100"  style="font-size: smaller" id="msg3">رسته را انتخاب کنید</label>
                                <select id="subguild"  name="subguild " class="d-none mt-0 uk-select rounded mb-2">
                                    <option value="">انتخاب رسته...</option>
                                </select>



{{--                                <button type="button" id="search-city" class="btn search-city my-2 btn-sm  disabled">جست و جو</button>--}}
                            </section>

                        </aside>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i class="far fa-filter"></i></button>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function (){

            var ajaxUrl = $('.front-ajax.d-none').attr('id')
            $('#province').on("change",function (){
                $('#guilds-in-province').addClass('d-none')
                $('#shops-in-province').addClass('d-none')
                $('#city').addClass('d-none')

                var province = $('#province').val()
                if (province!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-guilds-by-province',
                            province_id: province
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            if (response['guilds']!=0){
                                document.getElementById('guilds-in-province').innerHTML='در این استان '+response['guilds']+' صنف وجود دارد'
                                document.getElementById('shops-in-province').innerHTML='در این استان '+response['shops']+' فروشگاه وجود دارد'
                                $('#guilds-in-province').removeClass('d-none')
                                $('#shops-in-province').removeClass('d-none')
                                $('#city').removeClass('d-none')
                                $('#msg1').removeClass('d-none')
                                document.getElementById('city').innerHTML = ''
                                response['cities'].forEach(function (re) {
                                    document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                })
                            }else {
                                alert('هنوز فروشگاهی در این استان ثبت نشده است.')
                            }


                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }

            })

            $('#city').on("change",function (){
                $('#guilds-in-city').addClass('d-none')
                $('#shops-in-city').addClass('d-none')
                var city = $(this).val()
                if (city!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-guilds-by-city',
                            city_id: city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            if (response['guilds']!=0){
                                document.getElementById('guilds-in-city').innerHTML='در این شهر '+response['guilds']+' صنف وجود دارد'
                                document.getElementById('shops-in-city').innerHTML='در این شهر '+response['shops']+' فروشگاه وجود دارد'
                                $('#guilds-in-city').removeClass('d-none')
                                $('#shops-in-city').removeClass('d-none')
                                document.getElementById('guild').innerHTML = '<option value="" class="form-control">انتخاب صنف ...</option>'
                                response['guild'].forEach(function (re) {
                                    document.getElementById('guild').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    $('#guild').removeClass('d-none')
                                    $('#msg2').removeClass('d-none')
                                })
                            }else {
                                alert('هنوز فروشگاهی در این شهر ثبت نشده است.')
                            }
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('#guild').on("change",function (){
                $('#subguilds').addClass('d-none')
                var guild_id=$(this).val()
                var city=$('#city').val()
                if (guild_id!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-shops-by-guild-city',
                            id: guild_id,
                            city_id:city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            document.getElementById('subguilds').innerHTML='در این صنف '+response['subguild']+' رسته وجود دارد'
                            $('#subguilds').removeClass('d-none')
                            if (response['subguilds']!=''){
                                document.getElementById('subguild').innerHTML = '<option value="" class="form-control">انتخاب رسته ...</option>'
                                response['subguilds'].forEach(function (re) {
                                    document.getElementById('subguild').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    $('#subguild').removeClass('d-none')
                                    $('#msg3').removeClass('d-none')
                                })
                            }
                            var list=$('#shops-list')
                            var data=''
                            if (response['shops']!=''){
                                var path
                                var subguild
                                var count_res=0
                                var x='{{route('singleshop','*')}}'
                                response['shops'].forEach(function (re){
                                    var res = x.replace("*", re['unique_name']);
                                    if (re['logo'])
                                        path='images/logos/'+re['logo']['path']
                                    else
                                        path='img/logo.jpg'

                                    if (re['subguild'])
                                        subguild= re['subguild']['name']
                                    else
                                        subguild='سایر'

                                    if (re['name']){
                                        data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                            ' <figure class="thumb text-center">'+
                                            '<a class="text-center" href="'+res+'">'+
                                            ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                            ' </figure>'+
                                            '<div class="title">'+
                                            '<a href="'+res+'"><h6 class="text-primary">فروشگاه '+re['name']+'</h6></a>'+
                                            '</div>'+
                                            '  <div class="product-variables text-center ">'+
                                            ' <p class="text-muted font-weight-lighter mt-3 mb-0"><small>صنف:</small> '+re['guild']['name']+'</p>'+
                                            ' <p class="text-muted font-weight-lighter mt-0"><small>رسته:</small> '+subguild+'</p>'+
                                            '</div>'+
                                            '</article>';
                                    }
                                    count_res++
                                });
                            }else{
                                data='<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                            }
                            list.empty()
                            list.append(data)
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('#subguild').on("change",function (){
                $('#subguilds').addClass('d-none')
                var subguild_id=$(this).val()
                var city=$('#city').val()
                if (subguild_id!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-shops-by-subguild',
                            id: subguild_id,
                            city_id:city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            var list=$('#shops-list')
                            var data=''
                            if (response!=''){
                                var path
                                var subguild
                                var count_res=0
                                var x='{{route('singleshop','*')}}'
                                response.forEach(function (re){
                                    var res = x.replace("*", re['unique_name']);
                                    if (re['logo'])
                                        path='images/logos/'+re['logo']['path']
                                    else
                                        path='img/logo.jpg'

                                    if (re['subguild'])
                                        subguild= re['subguild']['name']
                                    else
                                        subguild='سایر'

                                    if (re['name']){
                                        data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                            ' <figure class="thumb text-center">'+
                                            '<a class="text-center" href="'+res+'">'+
                                            ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                            ' </figure>'+
                                            '<div class="title">'+
                                            '<a href="'+res+'"><h6 class="text-primary">فروشگاه '+re['name']+'</h6></a>'+
                                            '</div>'+
                                            '  <div class="product-variables text-center ">'+
                                            ' <p class="text-muted font-weight-lighter mt-3 mb-0"><small>صنف:</small> '+re['guild']['name']+'</p>'+
                                            ' <p class="text-muted font-weight-lighter mt-0"><small>رسته:</small> '+subguild+'</p>'+
                                            '</div>'+
                                            '</article>';
                                    }
                                    count_res++
                                });
                            }else{
                                data='<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                            }
                            list.empty()
                            list.append(data)
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('.guild.nav-link').on("click",function (){
                var guild_id=$(this).attr('id')
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-shops-by-guild',
                        id: guild_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')
                        var data=''
                        if (response!=''){
                            var path
                            var subguild
                            var count_res=0
                            var x='{{route('singleshop','*')}}'
                            response.forEach(function (re){
                                var res = x.replace("*", re['unique_name']);
                                if (re['logo'])
                                    path='images/logos/'+re['logo']['path']
                                else
                                    path='img/logo.jpg'

                                if (re['subguild'])
                                    subguild= re['subguild']['name']
                                else
                                    subguild='سایر'

                                if (re['name']){
                                    data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                        ' <figure class="thumb text-center">'+
                                        '<a class="text-center" href="'+res+'">'+
                                        ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                        ' </figure>'+
                                        '<div class="title">'+
                                        '<a href="'+res+'"><h6 class="text-primary">فروشگاه '+re['name']+'</h6></a>'+
                                        '</div>'+
                                        '  <div class="product-variables text-center ">'+
                                        ' <p class="text-muted font-weight-lighter mt-3 mb-0"><small>صنف:</small> '+re['guild']['name']+'</p>'+
                                        ' <p class="text-muted font-weight-lighter mt-0"><small>رسته:</small> '+subguild+'</p>'+
                                        '</div>'+
                                        '</article>';
                                }
                                count_res++
                            });
                        }else{
                            data='<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                        }
                        list.empty()
                        list.append(data)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })


        })
    </script>
@endsection

