@extends('front.newTheme.master')
@section('title','دسته بندی ها')
@section('main')
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <nav class="woocommerce-breadcrumb"><i class="flaticon-placeholder"></i><a href="{{route('store')}}">خانه</a><span>&nbsp;&#47;&nbsp;</span><span
                        class="current">دسته بندی ها</span></nav>
                <div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header">
                            <h6 class="text-muted">لیست محصولات</h6>
<!--                            <ul class="nav px-0  " style="overflow-x: auto">
                                @foreach($categories as $cat)
                                    @if($cat->products)
                                        <li class="nav-item text-center my-3">
                                            @if($cat->photo)
                                                <img src="{{asset('images/products/'.$cat->photo->path)}}"
                                                     class="img-fluid rounded"
                                                     style="max-height: 50px;max-width: 50px;min-width: 50px;min-height: 50px">
                                            @else
                                                <img src="{{asset('img/categories.svg')}}" class="img-fluid "
                                                     style="max-height: 50px;max-width: 50px">
                                            @endif
                                            <a class="cat nav-link" id="{{$cat->id}}" aria-current="page"
                                               href="{{route('advanceCategory', ['همه','همه',$cat->title])}}">{{$cat->title}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>-->
                        </header>

                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="clear">
                        </div>
                        <div id="shops-list" class="products columns-3 columns-res-2">
                            @if(count($products)>0)
                                @foreach($products as $product)
                                    <article
                                        class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">
                                        <figure class="thumb text-center">
                                            <a class="text-center"
                                               href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                @if($product->photo)
                                                    <img width="300" height="300"
                                                         src="{{asset('images/products/'.$product->photo->path)}}"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt="" loading="lazy"
                                                         srcset="{{asset('images/products/'.$product->photo->path)}} 300w, {{asset('images/products/'.$product->photo->path)}} 150w, {{asset('images/products/'.$product->photo->path)}} 100w, {{asset('images/products/'.$product->photo->path)}} 600w, {{asset('images/products/'.$product->photo->path)}} 96w"
                                                         sizes="(max-width: 300px) 100vw, 300px"/>
                                                @else
                                                    <img width="300" height="300" src="{{asset('img/default.jpg')}}"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt="" loading="lazy"
                                                         srcset="{{asset('img/default.jpg')}} 300w, {{asset('img/default.jpg')}} 150w, {{asset('img/default.jpg')}} 100w, {{asset('img/default.jpg')}} 600w, {{asset('img/default.jpg')}} 96w"
                                                         sizes="(max-width: 300px) 100vw, 300px"/>
                                            @endif
                                        </figure>
                                        <div class="title">
                                            <a href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                <h6>{{$product->name}}</h6></a>
                                        </div>
                                        <div class="product-variables text-center ">
                                            <p class="text-muted font-weight-lighter">{{number_format($product->sell_price)}}</p>
                                            <p class="text-muted font-weight-lighter">{{$product->category->title}}</p>
                                        </div>
                                    </article>
                                @endforeach
                            @else
                                <div class="alert alert-danger text-center py-5">
                                    <h4 class="text-info">
                                        محصولی یافت نشد
                                    </h4>
                                </div>
                            @endif
                        </div>
                        <div class="row mt-3 d-flex justify-content-center mt-3">{{$products->links()}}</div>

                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2"
                                     class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6>جستجوی پیشرفته: </h6></header>
                                <p class="text-muted font-weight-light" style="font-size: small"> ابتدا استان را انتخاب
                                    کنید<i class="text-danger">*</i></p>

                                <select id="province" name="province" class="mt-0 uk-select rounded mb-2">
                                    <option value="">همه</option>
                                    @foreach($provinces as $pro)
                                        @if($pro->name==$provincee)
                                            <option selected="selected" value="{{$pro->id}}">{{$pro->name}}</option>
                                        @else
                                            <option value="{{$pro->id}}">{{$pro->name}}</option>
                                        @endif
                                    @endforeach
                                </select>

                                {{--                                                                {!! Form::select('province',$arr,null,['placeholder'=>'همه','class'=>'uk-select rounded','id'=>'province']) !!}--}}


                                <label class="d-none text-muted rounded p-2 mb-0 mt-4  w-100" style="font-size: smaller"
                                       id="msg1">شهر را انتخاب کنید<i class="text-danger">*</i></label>
                                <select id="city" name="city " class="d-none mt-0 uk-select rounded mb-2"></select>


                                <label class=" text-muted rounded p-2 mb-0 mt-4 w-100" style="font-size: smaller"
                                       id="msg2">گروه کالا را انتخاب کنید<i class="text-danger">*</i></label>
                                <select id="category" name="category " class=" mt-0 uk-select rounded mb-2">
                                    <option value="">همه</option>
                                    @foreach($categories as $cat)
                                        @if($cat->products)
                                            @if($cat->title==$catt)
                                                <option selected="selected"
                                                        value="{{$cat->id}}">{{$cat->title}}</option>
                                            @else
                                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>

                                <button id="search" class="btn search my-2 btn-sm ">جست و جو</button>

                            </section>

                        </aside>
                        <span class="d-none paginate" id="{{route('advanceCategory',['PRO','CIT','CAT'])}}"></span>
                        <span class="d-none cityy" id="{{$cityy}}"></span>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i
                                class="far fa-filter"></i></button>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            var ajaxUrl = $('.front-ajax.d-none').attr('id')
            var advanceCatUrl = $('.d-none.paginate').attr('id')


            $('#province').on("change", function () {
                var province = $('#province').val()
                if (province != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-cities',
                            province_id: province
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            var city=$('.d-none.cityy').attr('id')
                            if (response != 0) {
                                document.getElementById('city').innerHTML = ''
                                document.getElementById('city').innerHTML = '<option value="" class="form-control">همه</option>'
                                response.forEach(function (re) {
                                    if (re['name']==city){
                                        document.getElementById('city').innerHTML += '<option selected="selected"  value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    } else
                                        document.getElementById('city').innerHTML += '<option value="' + re['id'] + '"  class="form-control">' + re['name'] + '</option>'
                                })
                                $('#city').removeClass('d-none')
                                $('#msg1').removeClass('d-none')
                            } else {
                                alert('هنوز فروشگاهی در این استان ثبت نشده است.')
                            }


                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                } else {
                    $('#city').addClass('d-none')
                }

            })
            var province = $('#province').val()

            if (province != "") {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province_id: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var city=$('.d-none.cityy').attr('id')
                        if (response != 0) {
                            document.getElementById('city').innerHTML = ''
                            document.getElementById('city').innerHTML = '<option value="" class="form-control">همه</option>'
                            response.forEach(function (re) {
                                if (re['name']==city){
                                    document.getElementById('city').innerHTML += '<option selected="selected"  value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                } else
                                    document.getElementById('city').innerHTML += '<option value="' + re['id'] + '"  class="form-control">' + re['name'] + '</option>'
                            })
                            $('#city').removeClass('d-none')
                            $('#msg1').removeClass('d-none')
                        } else {
                            alert('هنوز فروشگاهی در این استان ثبت نشده است.')
                        }


                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            } else {
                $('#city').addClass('d-none')
            }

            $('#city').on("change", function () {
                $('#guilds-in-city').addClass('d-none')
                $('#shops-in-city').addClass('d-none')
                var city = $(this).val()
                if (city != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-guilds-by-city',
                            city_id: city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            if (response['guilds'] != 0) {
                                document.getElementById('guilds-in-city').innerHTML = 'در این شهر ' + response['guilds'] + ' صنف وجود دارد'
                                document.getElementById('shops-in-city').innerHTML = 'در این شهر ' + response['shops'] + ' فروشگاه وجود دارد'
                                $('#guilds-in-city').removeClass('d-none')
                                $('#shops-in-city').removeClass('d-none')
                                document.getElementById('guild').innerHTML = '<option value="" class="form-control">انتخاب صنف ...</option>'
                                response['guild'].forEach(function (re) {
                                    document.getElementById('guild').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    document.getElementById('guild').value = re['id']
                                    $('#guild').removeClass('d-none')
                                    $('#msg2').removeClass('d-none')
                                })
                            } else {
                                alert('هنوز فروشگاهی در این شهر ثبت نشده است.')
                            }
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('#guild').on("change", function () {
                $('#subguilds').addClass('d-none')
                var guild_id = $(this).val()
                var city = $('#city').val()
                if (guild_id != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-shops-by-guild-city',
                            id: guild_id,
                            city_id: city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            document.getElementById('subguilds').innerHTML = 'در این صنف ' + response['subguild'] + ' رسته وجود دارد'
                            $('#subguilds').removeClass('d-none')
                            if (response['subguilds'] != '') {
                                document.getElementById('subguild').innerHTML = '<option value="" class="form-control">انتخاب رسته ...</option>'
                                response['subguilds'].forEach(function (re) {
                                    document.getElementById('subguild').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    document.getElementById('subguild').value = re['id']
                                    $('#subguild').removeClass('d-none')
                                    $('#msg3').removeClass('d-none')
                                })
                            }
                            var list = $('#shops-list')
                            var data = ''
                            if (response['shops'] != '') {
                                var path
                                var count_res = 0
                                var x = '{{route('singleshop',0)}}'
                                response['shops'].forEach(function (re) {
                                    var res = x.replace("0", re['unique_name']);
                                    if (re['logo'])
                                        path = 'images/logos/' + re['logo']['path']
                                    else
                                        path = 'img/logo.jpg'

                                    if (re['name']) {
                                        data += ' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">' +
                                            ' <figure class="thumb text-center">' +
                                            '<a class="text-center" href="' + res + '">' +
                                            ' <img width="300" height="300" src="' + path + '" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="' + path + ' 300w, ' + path + ' 150w, ' + path + ' 100w, ' + path + ' 600w, ' + path + ' 96w" sizes="(max-width: 300px) 100vw, 300px" />' +
                                            ' </figure>' +
                                            '<div class="title">' +
                                            '<a href="' + res + '"><h6>' + re['name'] + '</h6></a>' +
                                            '</div>' +
                                            '  <div class="product-variables text-center ">' +
                                            ' <p class="text-muted font-weight-lighter">' + re['subguild']['name'] + '</p>' +
                                            '</div>' +
                                            '</article>';
                                    }
                                    count_res++
                                });
                            } else {
                                data = '<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                            }
                            list.empty()
                            list.append(data)
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('#subguild').on("change", function () {
                $('#subguilds').addClass('d-none')
                var subguild_id = $(this).val()
                var city = $('#city').val()
                if (subguild_id != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-shops-by-subguild',
                            id: subguild_id,
                            city_id: city
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            var list = $('#shops-list')
                            var data = ''
                            if (response != '') {
                                var path
                                var count_res = 0
                                var x = '{{route('singleshop',0)}}'
                                response.forEach(function (re) {
                                    var res = x.replace("0", re['unique_name']);
                                    if (re['logo'])
                                        path = 'images/logos/' + re['logo']['path']
                                    else
                                        path = 'img/logo.jpg'

                                    if (re['name']) {
                                        data += ' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">' +
                                            ' <figure class="thumb text-center">' +
                                            '<a class="text-center" href="' + res + '">' +
                                            ' <img width="300" height="300" src="' + path + '" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="' + path + ' 300w, ' + path + ' 150w, ' + path + ' 100w, ' + path + ' 600w, ' + path + ' 96w" sizes="(max-width: 300px) 100vw, 300px" />' +
                                            ' </figure>' +
                                            '<div class="title">' +
                                            '<a href="' + res + '"><h6>' + re['name'] + '</h6></a>' +
                                            '</div>' +
                                            '  <div class="product-variables text-center ">' +
                                            ' <p class="text-muted font-weight-lighter">' + re['subguild']['name'] + '</p>' +
                                            '</div>' +
                                            '</article>';
                                    }
                                    count_res++
                                });
                            } else {
                                data = '<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                            }
                            list.empty()
                            list.append(data)
                        },
                        error: function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            $('#search').on("click", function () {
                var province = $('#province option:selected').text()
                var city = $('#city option:selected').text()
                var cat = $('#category option:selected').text()
                advanceCatUrl = advanceCatUrl.replace("PRO", province)
                advanceCatUrl = advanceCatUrl.replace("CAT", cat)
                if (city == "")
                    city = "همه"
                advanceCatUrl = advanceCatUrl.replace("CIT", city)
                window.location.href = advanceCatUrl;
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-products-fot-category-page',
                        city: city,
                        cat: cat,
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list = $('#shops-list')
                        var data = ''
                        if (response['data'] != '') {
                            var path
                            var count_res = 0
                            var x = '{{route('singleProduct',[0,1])}}'
                            response['data'].forEach(function (res) {
                                var url = x.replace("0", res['slug']);
                                url = url.replace("1", res['id']);
                                if (res['photo'])
                                    path = 'images/' + res['photo']['path']
                                else
                                    path = './img/default.jpg'

                                data += ' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">' +
                                    ' <figure class="thumb text-center">' +
                                    '<a class="text-center" href="' + url + '">' +
                                    ' <img width="300" height="300" src="' + path + '" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="' + path + ' 300w, ' + path + ' 150w, ' + path + ' 100w, ' + path + ' 600w, ' + path + ' 96w" sizes="(max-width: 300px) 100vw, 300px" />' +
                                    ' </figure>' +
                                    '<div class="title">' +
                                    '<a href="' + url + '"><h6>' + res['name'] + '</h6></a>' +
                                    '</div>' +
                                    '  <div class="product-variables text-center ">' +
                                    ' <p class="text-muted font-weight-lighter"><span>قیمت: </span>' + res['sell_price'] + '</p>' +
                                    ' <p class="text-muted font-weight-lighter">' + res['category']['title'] + '</p>' +
                                    ' <a href="' + url + '" class=" btn btn-primary font-weight-lighter">مشاهده محصول </a>' +
                                    '</div>' +
                                    '</article>';
                            })
                        } else {
                            data = '<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                        }
                        list.empty()
                        list.append(data)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })

            $/*('.cat.nav-link').on("click",function (){
                var cat_id=$(this).attr('id')
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-products-by-cat',
                        id: cat_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')
                        var data=''
                        if (response['data']!=''){
                            var path
                            var count_res=0
                            var x='{{route('singleProduct',[0,1])}}'
                            response['data'].forEach(function(res){
                                var url = x.replace("0", res['slug']);
                                 url = url.replace("1", res['id']);
                                if (res['photo'])
                                    path='images/'+res['photo']['path']
                                else
                                    path='./img/default.jpg'

                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+url+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+url+'"><h6>'+res['name']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter"><span>قیمت: </span>'+res['sell_price']+'</p>'+
                                    ' <p class="text-muted font-weight-lighter">'+res['category']['title']+'</p>'+
                                    ' <a href="'+url+'" class=" btn btn-primary font-weight-lighter">مشاهده محصول </a>'+
                                    '</div>'+
                                    '</article>';
                            })
                        }else{
                            data='<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                        }
                        list.empty()
                        list.append(data)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })*/


        })
    </script>
@endsection

