@extends('front.newTheme.master')
@section('title','سبد خرید')
@section('main')
    <main id="main">
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <section class="blog-home">
                        @if($products==null)
                         <article class="post-item post-8 text-center page type-page status-publish hentry" id="post-8">
                                                    <div class="content">
                                                        <div class="woocommerce"><div class="cart-empty">
                                                                <div class="cart-empty-icon">
                                                                    <img class="img-fluid" width="300px" height="300x" src="{{asset('img/shopping-cart.svg')}}" alt="shopping cart">
                                                                </div>
                                                                <div class="woocommerce-notices-wrapper"></div>
                                                                <p class="cart-empty woocommerce-info">سبد خرید شما در حال حاضر خالی است.</p>
                                                                @guest
                                                                <a href="{{route('login')}}" class="account-btn btn btn-primary">
                                                                    <i class="fal fa-sign-in"></i>
                                                                    <span>به حساب کاربری خود وارد شوید</span>
                                                                </a>
                                                                @endguest
                                                                <p class="return-to-shop">
                                                                    <a class="button wc-backward btn" href="{{route('store')}}">
                                                                        بازگشت به فروشگاه		</a>
                                                                </p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                        @else
                        <div class="row  d-flex justify-content-center rounded  ">
                            <div class="col-12  text-right p-3 buy-box-shadow">
                                @if(Session::has('Error'))
                                    <div class="alert alert-danger">
                                        {{Session('Error')}}
                                    </div>
                                @endif
                            </div>
                            <div class="col-12 bg-white text-right p-3 mt-5 text-center buy-box-shadow border mb-5">
                                @if(Session::has('cart'))
                                    @if(Session::has('limited'))
                                        <div class="alert alert-danger text-danger">
                                            {{Session::pull('limited')}}
                                        </div>
                                    @endif
                                    <h4>سبد خرید</h4>
                                    <table class="table table-bordered table-responsive-md">
                                        <tr>
                                            <th>ردیف</th>
                                            <th>تصویر</th>
                                            <th>محصول</th>
                                            <th>قیمت</th>
                                            <th>تعداد</th>
                                            <th>مجموع</th>
                                            <th>فروشگاه</th>
                                            <th>حذف</th>
                                        </tr>
                                        @php($sum=0)
                                        @php($id=1)
                                        @foreach($products as $product)
                                            @php($sum+= $product->sell_price*$product->quantity)
                                            <tr id="{{$product->id}}" class="pro-row">
                                                <td>{{$id}}</td>
                                                <td>
                                                    @if($product->photo)
                                                        <img class="img-product-list rounded" height="80px" width="80px"
                                                             src="{{asset('images/products/'.$product->photo->path)}}">
                                                    @else
                                                        <img class="img-product-list" height="80px" width="80px" src="{{asset('img/default-2.svg')}}">
                                                    @endif
                                                    </td>
                                                <td>{{$product->name}}</td>
                                                <td> <span id="{{$product->id}}" class="sell-price" >{{$product->sell_price}}</span> تومان</td>
                                                <td><i id="{{$product->id}}" style="cursor: pointer"
                                                       class="inc fa fa-plus text-success"
                                                       onclick=""></i>
                                                    <span id="{{$product->id}}"
                                                          class="pro-qty mx-2">{{$product->quantity}}</span>
                                                    <i id="{{$product->id}}" style="cursor: pointer"
                                                       class="dec fa fa-minus text-danger"
                                                       onclick=""></i></td>
                                                <td><span id="{{$product->id}}" class="price">{{$product->sell_price*$product->quantity}}</span> تومان</td>
                                                <td><a href="{{route('singleshop',$product->shop->unique_name)}}">
                                                        {{$product->shop->name}}
                                                    </a>
                                                </td>
                                                <td><i id="{{$product->id}}" style="cursor: pointer"
                                                       class="del fa fa-trash text-danger"
                                                       onclick=""></i></td>
                                            </tr>
                                            @php($id++)
                                        @endforeach
                                        <tr>
                                            <td colspan="6">
                                                <div class="row">
                                                    <div class="col">
                                                        {!! Form::open(['method'=>'POST','action'=>'ShopController@deleteCart']) !!}
                                                        <button class="btn btn-primary" type="submit">خالی کردن سبد
                                                            خرید
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                        @if(Auth::user()->user_info_id==null)
                                            @php($user=Auth::user())
                                            {{--{!! Form::model($user,['method'=>'PATCH','action'=>['ProfileController@update',$user->id],'files'=>true] ) !!}--}}
                                            <div id="null-info" class="row p-4 rounded mx-3 d-flex "
                                                 style="border: 1px #c9c9c9 solid">
                                                <h4 class=" py-2 rounded alert-warning text-center w-100">
                                                    برای ادامه خرید لطفا اطلاعات خود را وارد کنید.
                                                </h4>
                                                <div class="row w-100 justify-content-center align-items-center ">
                                                    <div class="col-12 mt-3 col-md-4">
                                                        <h6 class="text-muted">تلفن ثابت:</h6>
                                                        {!! Form::text('phone',null,['class'=>'form-control','id'=>'phone']) !!}
                                                    </div>
                                                    <div class="col-12 mt-3 col-md-4">
                                                        <h6 class="text-muted">استان:</h6>
                                                        <select class="form-control" name="province" id="province">
                                                            <option value="">انتخاب استان</option>
                                                            @foreach($provinces as $province)
                                                                <option value="{{$province->id}}">{{$province->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-12 mt-3 col-md-4">
                                                        <h6 class="text-muted">شهر:</h6>
                                                        <select class="form-control" name="city" id="city">
                                                        </select>
                                                    </div>
                                                    <div class="col-12 mt-3 col-md-4">
                                                        <h6 class="text-muted">آدرس دقیق پستی:</h6>
                                                        {!! Form::text('address',null,['class'=>'form-control','id'=>'address']) !!}
                                                    </div>
                                                </div>
                                                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                                    <div class="col-12 mt-3 col-md-4 text-center">
                                                        {!! Form::submit('ذخیره اطلاعات',['class'=>'btn  w-50','id'=>'set-info']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            {{--{!! Form::close() !!}--}}
                                        @endif
                                        <div class="row px-2 my-5">
                                            <div class="col-12 col-md-6">
                                                <div class="row d-flex justify-content-center text-center px-2">
                                                    <h4 class=" text-muted  text-center w-100 border-1-gainsboro">انتخاب روش ارسال</h4>
                                                    <div class="col-12 bg-white justify-content-center p-3  text-center buy-box-shadow border mb-5">
                                                        @foreach($delivery_methods as $method)
                                                            <div class="radiocheck row w-100 my-1 pb-3 justify-content-center align-items-center" style="border-bottom: 2px dashed #beb8b8">
                                                                <div class="col-12 text-left align-items-center justify-content-center col-md-1 my-1">
                                                                    <input class="uk-radio method" id="{{$method->price}}" value="{{$method->id}}" name="delivery_method" type="radio">
                                                                </div>
                                                                <div class="col-12  col-md-1 my-1"><img src="{{asset('img/default.jpg')}}" height="60px" width="60px"  alt=""></div>
                                                                <div class="col-12 text-right col-md-6 my-1">
                                                                    <h5>{{$method->method}}</h5>
                                                                    <p class="text-muted has-small-font-size">{{$method->description}}</p>
                                                                </div>
                                                                <div class="col-12 text-right col-md-3 my-1">{{number_format($method->price)}} تومان </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="row text-center justify-content-center py-3 ">
                                                    <div class="col-12 col-md-6 border rounded py-3 mt-4">
                                                        <div class="w-100 my-2"><h5>کد تخفیف:</h5></div>
                                                        {!! Form::text('dis',null,['class'=>'dis form-control']) !!}
                                                        <button id="discount-btn" class="btn btn-dark mt-3" type="submit">اعمال تخفیف</button>
                                                    </div>
                                                </div>
                                                <div class="row d-flex justify-content-center text-center mt-5">
                                                    <h4 class="mt-3 w-50 border-1-gainsboro">مجموع سبد خرید</h4>
                                                </div>
                                                <div class="row d-flex justify-content-center">
                                                    <table class="table table-bordered w-50 ">
                                                        <tr>
                                                            <th>قیمت کل اجناس</th>
                                                            @php($total=Session::get('cart'))
                                                            @php($delivery_price=$total['deliveryPrice'])
                                                            @php($totalprice=$total['totalPrice'])
                                                            @php($discount=$total['discount'])
                                                            <td>
                                                                {!! Form::label('total',number_format($sum).' تومان',['class'=>'price','id'=>'lbl-total'])!!}
                                                                {!! Form::text('total',$totalprice,['class'=>'total d-none','id'=>'txt-total']) !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>تخفیف</th>
                                                            <td>
                                                                <span id="total-discount">{{number_format($discount)}}</span>
                                                                تومان
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>هزینه ارسال</th>
                                                            <td>
                                                                <span id="delivery-price">{{number_format($delivery_price)}}</span>
                                                                تومان
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>مجموع</th>
                                                            <td>
                                                                <span id="total-factor-price">{{number_format($totalprice)}}</span>
                                                                تومان
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="row d-flex justify-content-center">
                                                    <button id="payment-btn"  class="btn btn-success bg-success py-3 px-5" type="button">پرداخت</button>
                                                </div>
                                            </div>
                                        </div>
                                @else
                                    <span class="fa fa-shopping-cart"></span>
                                    <h4>سبد خرید شما خالی است</h4>
                                @endif
                            </div>
                        </div>
                        @endif
                        <span class="ajax d-none" id="{{route('webAjax')}}"></span>
                    </section>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            function separate(Number)
            {
                Number+= '';
                Number= Number.replace(',', '');
                x = Number.split('.');
                y = x[0];
                z= x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(y))
                    y= y.replace(rgx, '$1' + ',' + '$2');
                return y+ z;
            }

            var ajaxUrl = $('.front-ajax.d-none').attr('id')
            var ajax = $('.ajax.d-none').attr('id')

            $('#province').on("change", function () {
                var province = $('#province').val()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajax,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $('.inc').click(function (event) {
                var id = event.target.id;
                var span = $('#' + id + '.pro-qty');
                var sellprice=$('#' + id + '.sell-price');
                var price=$('#' + id + '.price');
                var newQty = parseInt(span.text()) + 1;

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'inc-card-pro',
                        id: id
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var total=parseInt(response['totalPrice'])
                        total=separate(total)
                        $('#total-price').text(total)
                        $('#lbl-total').text(total)
                        $('#txt-total').val(total)
                        $('#total-qty').text(response['totalQty'])
                        $('#total-factor-price').text(total)
                        $('#total-factor-price').val(total)
                        var qty=span.text();
                        qty=parseInt(qty)+1
                        span.text(qty)
                        price.text(qty * parseInt(sellprice.text()));
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            });

            $('.dec').click(function (event) {
                var id = event.target.id;
                var span = $('#' + id + '.pro-qty');
                var sellprice=$('#' + id + '.sell-price');
                var price=$('#' + id + '.price');

                var newQty = parseInt(span.text()) - 1;
                if (newQty > 0)
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {// change data to this object
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'dec-card-pro',
                            id: id
                        },
                        dataType: 'json',
                        success: function (response) {
                            var total=parseInt(response['totalPrice'])
                            total=separate(total)
                            $('#total-price').text(total)
                            $('#total-qty').text(response['totalQty'])
                            $('#lbl-total').text(total)
                            $('#txt-total').val(total)
                            var qty=span.text()
                            qty=parseInt(qty)-1
                            span.text(qty)
                            price.text(qty * parseInt(sellprice.text()))
                            $('#total-factor-price').text(total)
                            $('#total-factor-price').val(total)
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
            });

            $('.del').click(function (event) {
                var id = event.target.id;
                var trow = $('#' + id + '.pro-row');
                console.log(id)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'del-card-pro',
                        id: id
                    },
                    dataType: 'json',
                    success: function (response) {
                        trow.remove()
                        var total=parseInt(response['totalPrice'])
                        total=separate(total)
                        $('#total-price').text(total)
                        $('#total-qty').text(response['totalQty'])
                        $('#lbl-total').text(total)
                        $('#txt-total').val(total)
                        var qty=span.text()
                        qty=parseInt(qty)-1
                        span.text(qty)
                        price.text(qty * parseInt(sellprice.text()))
                        $('#total-factor-price').text(total)
                        $('#total-factor-price').val(total)
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            });

            $('#set-info').click(function () {
                var phone=document.getElementById("phone").value
                var province=document.getElementById("province").value
                var city=document.getElementById("city").value
                var address=document.getElementById("address").value
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajax,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'set-user-info',
                        phone: phone,
                        province: province,
                        city: city,
                        address: address
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        location.reload()
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });

            })

            $('.uk-radio.method').click(function (){
                var price=$(this).attr('id')
                var id=$(this).attr('value')
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajax,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'add-delivery-price-to-cart',
                        price:price,
                        id:id
                    },
                    dataType: 'json',
                    success: function (response) {
                        var total=separate(response['totalPrice'])
                        $('.labelPrice').val(response['totalPrice'])
                        $('.labelPrice').text(total)
                        $('.total.d-none').val(response['totalPrice'])
                        $('.total.d-none').text(response['totalPrice'])
                        $('#delivery-price').text(separate(response['deliveryPrice']))
                        $('#total-factor-price').text(total)
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('#payment-btn').click(function (){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajax,
                    data: {// change data to this object
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'check-pre-payment-requirement',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        switch (response){
                            case 'null_info':
                                alert('اطلاعات کاربری شما ناقص است');
                                break;
                            case 'delivery_null':
                                alert('روش ارسال انتخاب نشده است');
                                break;
                            case true:
                                // $('#check-payment-btn').addClass('d-none')
                                // $('#payment-btn').removeClass('d-none')
                                // alert('روش ارسال انتخاب نشده است');
                                window.location.href = "{{route('payment')}}";
                                break;
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('#discount-btn').on("click",function (){
                var dis=$('.dis').val()
                console.log(dis)
                if (dis!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajax,
                        data: {// change data to this object
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'set-discount',
                            dis: dis
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            if (response['status']){
                                $('#total-discount').text(response['discount'])
                                $('#total-factor-price').text(response['cart']['totalPrice'])
                            }
                            else
                                alert('کد تخفیف معتبر نمیباشد')
                        },
                        error: function (response) {
                        }
                    });
                }
            })
        });
    </script>
    @endsection
