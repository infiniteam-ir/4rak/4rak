@extends('front.newTheme.master')
@section('title','مطالب')
@section('main')
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <nav class="woocommerce-breadcrumb"><i class="flaticon-placeholder"></i><a href="{{route('store')}}">خانه</a><span>&nbsp;&#47;&nbsp;</span><span
                        class="current">دسته بندی مطالب</span></nav>
                <div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header">
                            <h6 class="text-muted">لیست مطالب</h6>
                        </header>

                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="clear">
                        </div>
                        <div id="shops-list" class="products columns-3 columns-res-2">
                            @if(count($posts)>0)
                                @foreach($posts as $post)
                                    <article
                                        class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">
                                        <figure class="thumb text-center">
                                            <a class="text-center"
                                               href="{{route('article',$post->id)}}">
                                                @if($post->photo)
                                                    <img width="300" height="300"
                                                         src="{{asset('images/'.$post->photo->path)}}"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt="" loading="lazy"
                                                         srcset="{{asset('images/'.$post->photo->path)}} 300w, {{asset('images/'.$post->photo->path)}} 150w, {{asset('images/'.$post->photo->path)}} 100w, {{asset('images/'.$post->photo->path)}} 600w, {{asset('images/'.$post->photo->path)}} 96w"
                                                         sizes="(max-width: 300px) 100vw, 300px"/>
                                                @else
                                                    <img width="300" height="300" src="{{asset('img/default.jpg')}}"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt="" loading="lazy"
                                                         srcset="{{asset('img/default.jpg')}} 300w, {{asset('img/default.jpg')}} 150w, {{asset('img/default.jpg')}} 100w, {{asset('img/default.jpg')}} 600w, {{asset('img/default.jpg')}} 96w"
                                                         sizes="(max-width: 300px) 100vw, 300px"/>
                                            @endif
                                        </figure>
                                        <div class="title">
                                            <a href="{{route('article',$post->id)}}">
                                                <h6>{{$post->title}}</h6></a>
                                        </div>
                                        <div class="product-variables text-center ">
                                            <p class="text-muted font-weight-lighter">{{$post->category->title}}</p>
                                        </div>
                                    </article>
                                @endforeach
                            @else
                                <div class="alert alert-danger text-center py-5">
                                    <h4 class="text-info">
                                        مطلبی یافت نشد
                                    </h4>
                                </div>
                            @endif
                        </div>
                        <div class="row mt-3 d-flex justify-content-center mt-3">{{$posts->links()}}</div>

                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2"
                                     class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6> دسته بندی مطالب </h6></header>
                                <ul class="list-unstyled">
                                    @foreach($categories as $cat)
                                        <li class="w-100 my-2">
                                        <!--                                            <button class="cat-title btn" id="{{$cat->id}}" type="button">
                                                {{$cat->title}}
                                            </button>-->
                                            <a class="cat nav-link" href="javascript:void(0)" id="{{$cat->id}}">
                                                @if($cat->photo)
                                                    <img class="img-fluid mx-2" style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%" src="{{asset('images/'.$cat->photo->path)}}" alt="">
                                                @else
                                                    <img class="img-fluid mx-2" style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%" src="{{asset('images/1608550517default.jpg')}}" alt="">
                                                @endif
                                                {{$cat->title}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </section>

                        </aside>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i
                                class="far fa-filter"></i></button>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            var ajaxUrl = $('.front-ajax.d-none').attr('id')

            $('.cat.nav-link').on("click",function (){
                var cat_id=$(this).attr('id')
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-articles-by-cat',
                        id: cat_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')
                        var data=''
                        if (response['data']!=''){
                            var path
                            var count_res=0
                            var x='{{route('article',['x'])}}'
                            response['data'].forEach(function(res){
                                var url = x.replace("x", res['id']);
                                if (res['photo'])
                                    path='images/'+res['photo']['path']
                                else
                                    path='./img/default.jpg'

                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+url+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+url+'"><h6>'+res['title']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter">'+res['category']['title']+'</p>'+
                                    '</div>'+
                                    '</article>';
                            })
                        }else{
                            data='<div class="alert alert-danger py-5">نتیجه ای یافت نشد</div>';
                        }
                        list.empty()
                        list.append(data)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })


        })
    </script>
@endsection

