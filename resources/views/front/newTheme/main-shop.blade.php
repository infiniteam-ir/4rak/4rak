<!DOCTYPE html>
<html dir="rtl" lang="fa-IR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>چارک | پلتفرم آنلاین تجارت</title>
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="https://4rak.ir" />
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="چارک - پاتفرم آنلاین تجارت" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="https://4rak.ir" />
    <meta property="og:site_name" content="چارک" />
    <meta property="article:modified_time" content="2020-09-26T14:15:48+00:00" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:label1" content="زمان تقریبی برای خواندن">
    <meta name="twitter:data1" content="0 دقیقه">
    <!-- / Yoast SEO plugin. -->

    <link rel="stylesheet" href="{{asset('newFront/css/uikit.min.css')}}" />
    <link rel='dns-prefetch' href='s.w.org' >
    <link rel="alternate" type="application/rss+xml" title="چارک &raquo; خوراک" href="https://4rak.ir/feed/" />
    <link rel="alternate" type="application/rss+xml" title="چارک &raquo; خوراک دیدگاه‌ها" href="https://4rak.ir/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.coderboy.ir\/negarshop\/wp-includes\/js\/wp-emoji-release.min.js"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='dashicons-css'  href='{{asset('newFront/css/dashicons.min.css')}}' type='text/css' media='all' />
    <style id='dashicons-inline-css' type='text/css'>
        [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='wp-block-library-rtl-css'  href='{{asset('newFront/css/style-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{asset('newFront/css/vendors-style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-rtl-css'  href='{{asset('newFront/css/style-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcaf-css'  href='{{asset('newFront/css/yith-wcaf.css')}}' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='hint-css'  href='{{asset('newFront/css/hint.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-css'  href='{{asset('newFront/css/perfect-scrollbar.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-wpc-css'  href='{{asset('newFront/css/custom-theme.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-flaticon-css'  href='{{asset('newFront/fonts/flaticon/flaticon.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-fontawesome-css'  href='{{asset('newFront/fonts/fontawesome/fa-all.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-iransans-css'  href='{{asset('newFront/fonts/iransans/iransans.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-bootstrap-css'  href='{{asset('newFront/css/bootstrap.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-nouislider-css'  href='{{asset('newFront/css/nouislider.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owl-css'  href='{{asset('newFront/css/owl.carousel.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owltheme-css'  href='{{asset('newFront/css/owl.theme.default.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-lightbox-css'  href='{{asset('newFront/css/lightgallery.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-compare-css'  href='{{asset('newFront/css/compare-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-magnify-css'  href='{{asset('newFront/css/magnify.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-style-css'  href='{{asset('newFront/css/core.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-builder-frontend-grid-css'  href='{{asset('newFront/css/frontend-grid.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-forms-default-styles-css'  href='{{asset('newFront/css/frontend.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='{{asset('newFront/css/font-awesome.min.css')}}' type='text/css' media='all' />
    <style id='font-awesome-inline-css' type='text/css'>
        [data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='elementor-icons-css'  href='{{asset('newFront/css/elementor-icons.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'  href='{{asset('newFront/css/animations.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-legacy-css'  href='{{asset('newFront/css/frontend-legacy-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1558-css'  href='{{asset('newFront/css/post-1558.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-1768.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-1763.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-1850.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-2188.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1562-css'  href='{{asset('newFront/css/post-1562.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1572-css'  href='{{asset('newFront/css/post-1572.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-style-css'  href='{{asset('newFront/css/style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-rtl-style-css'  href='{{asset('newFront/css/rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='premmerce-brands-css'  href='{{asset('newFront/css/premmerce-brands.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.6' type='text/css' media='all' />
{{--    <link rel="stylesheet" href="href='{{asset('newFront/css/slider.css')}}">--}}

    <script type="text/template" id="tmpl-unavailable-variation-template">
        <p>با عرض پوزش، این كالا در دسترس نیست. لطفاً ترکیب دیگری را انتخاب کنید.</p>
    </script>
    <script type='text/javascript' src='{{asset('newFront/js/jquery.min.js')}}' id='jquery-core-js'></script>
    <script type='text/javascript' src='{{asset('js/jquery-3.5.1.min.js')}}' ></script>

    <script type='text/javascript' src='{{asset('newFront/js/elementor_fixes.js')}}' id='script-ns_elementor_fixes-js'></script>
    {{--<link rel="https://api.w.org/" href="https://demo.coderboy.ir/negarshop/wp-json/" /><link rel="alternate" type="application/json" href="https://demo.coderboy.ir/negarshop/wp-json/wp/v2/pages/1768" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demo.coderboy.ir/negarshop/xmlrpc.php?rsd" />--}}
    {{--<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demo.coderboy.ir/negarshop/wp-includes/wlwmanifest.xml" />--}}
    {{--<meta name="generator" content="WordPress 5.6" />--}}
    {{--<meta name="generator" content="WooCommerce 4.8.0" />--}}
    {{--<link rel='shortlink' href='https://demo.coderboy.ir/negarshop/?p=1768' />--}}
    {{--<link rel="alternate" type="application/json+oembed" href="https://demo.coderboy.ir/negarshop/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.coderboy.ir%2Fnegarshop%2Fhome2%2F" />--}}
    {{--<link rel="alternate" type="text/xml+oembed" href="https://demo.coderboy.ir/negarshop/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.coderboy.ir%2Fnegarshop%2Fhome2%2F&#038;format=xml" />--}}
    <meta name="theme-color" content="#039be5"/><style>body{background-color: #eee;}html, body, .flip-clock-a *, .tooltip, #orders_statistics_chart_container, #orders_statistics_chart_container *{font-family: IRANSans_Fa;}.content-widget article.item figure, article.product figure.thumb, article.w-p-item figure{background-image: url(https://demo.coderboy.ir/negarshop/wp-content/themes/negarshop/statics/images/product-placeholder.png);}/* ------------------------- Normal ------------------------- */.content-widget.slider-2.style-2 .product-info .feature-daels-price .sale-price, .ns-add-to-cart-inner .price-update, article.product div.title a:hover, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .woocommerce-LostPassword a, .ns-checkbox input[type="radio"]:checked + label::before, section.blog-home article.post-item.post .content a, a:hover, .header-account .account-box:hover .title, .header-main-nav .header-main-menu > ul > li.loaded:hover>a, section.widget ul li a:hover, .content-widget.slider-2.style-2 .product-info .footer-sec .finished, .content-widget.slider-2.style-2 .product-info .static-title span, article.w-p-item .info > .price span span, article.w-p-item .info > .price span.amount, section.blog-home article.post-item .title .title-tag:hover, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-tabs.wc-tabs-wrapper ul.wc-tabs li.active a, .woocommerce.single-product .sale-timer .right .title span, .offer-moments .owl-item .price ins, .offer-moments .owl-item .price span.amount, .page-template-amazing-offer article.product .price ins, .ns-checkbox input[type="checkbox"]:checked+label::before, .content-widget.products-carousel.tabs ul.tabs li.active a, .product .product-sales-count i, .cb-comment-tabs .cb-tabs .active, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a{color: #039be5;}.product-add-to-cart-sticky .add-product, .negarshop-countdown .countdown-section:last-of-type::before, .select_option.select_option_colorpicker.selected,.select_option.select_option_label.selected, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit:hover, .content-widget.slider.product-archive .wg-title, .lg-actions .lg-next:hover, .lg-actions .lg-prev:hover,.lg-outer .lg-toogle-thumb:hover, .lg-outer.lg-dropdown-active #lg-share,.lg-toolbar .lg-icon:hover, .lg-progress-bar .lg-progress, .dokan-progress>.dokan-progress-bar, #negarshop-to-top>span, .header-search .search-box .action-btns .action-btn.search-submit::after, .select2-container--default .select2-results__option--highlighted[aria-selected], .header-account .account-box:hover .icon, .header-main-nav .header-main-menu > ul > li.loaded:hover>a::after, .btn-negar, .content-widget.slider-2.style-2 .carousel-indicators li.active, .btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled , .navigation.pagination .nav-links a:hover, .woocommerce-message a:active, .woocommerce .onsale, header.section-header a.archive, .woocommerce.single-product .sale-timer .left .discount span, .woocommerce-pagination ul li a:hover , .ui-slider .ui-slider-range, .switch input:checked + .slider, .woocommerce .quantity.custom-num span:hover, .content-widget.slider-2.style-2 .carousel-inner .carousel-item .discount-percent, .sidebar .woocommerce-product-search button, .product-single-ribbons .ribbons>div>span, .content-widget.products-carousel.tabs ul.tabs li a::after, .cb-nouislider .noUi-connect, .comment-users-reviews .progress .progress-bar, .cb-comment-tabs .cb-tabs a::after, .ns-table tbody td.actions a.dislike_product:hover, .ns-store-header .nav-pills .nav-item a.active, .ns-store-avatar header.store-avatar-header{background-color: #039be5;}.content-widget.slider.product-archive .wg-title, .lg-outer .lg-thumb-item.active, .lg-outer .lg-thumb-item:hover,.btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled, .ui-slider span, .ns-store-avatar header.store-avatar-header .avatar{border-color: #039be5;}.spinner, nav#main-menu li.loading>a::after{border-top-color: #039be5;}.content-widget.slider-2.style-2 .carousel-indicators li::before{border-right-color: #039be5;}.content-widget.slider.product-archive .slide-details .prd-price, .woocommerce-variation-price, .woocommerce p.price > span, .woocommerce p.price ins,.table-cell .woocommerce-Price-amount,#order_review span.amount{color: #4caf50;}/* ------------------------- Importants ------------------------- */.woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .product-section .sale-timer-box, .btn-transparent, .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a{color: #039be5 !important;}.dokan-btn-theme, .wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn{background-color: #039be5 !important;}.woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .dokan-btn-theme{border-color: #039be5 !important;}.cls-3{fill: #039be5;}.negarshop-countdown .countdown-section:last-of-type{background: rgba(3,155,229,0.2);color: #039be5;}/* ------------------------- Customs ------------------------- */.woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected, .woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected:hover {box-shadow: 0 0 0 2px #039be5 !important;}.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a{background-color: rgba(3,155,229,0.1) !important;}/*-------------basket----------------*/.header-cart-basket .cart-basket-box::before, .header-cart-basket.style-2 .cart-basket-box{background-color: rgba(0, 191, 214, 0.1);}.header-cart-basket .cart-basket-box, .header-cart-basket.style-2 .cart-basket-box{color: #00bfd6;}.header-cart-basket .cart-basket-box > span.count, .header-cart-basket.style-2 .cart-basket-box > span.count{background-color: #00bfd6;color: #fff;}.header-cart-basket > .widget.widget_shopping_cart ul li a.remove:hover{background-color: #00bfd6;}.header-cart-basket > .widget.widget_shopping_cart p.total{color: #00bfd6;background-color: rgba(0,191,214,0.1);}.header-cart-basket > .widget.widget_shopping_cart .buttons .button:hover{color: #00bfd6;}.header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout:hover{background-color: #00bfd6;}article.product figure.thumb, .negarshop-countdown .countdown-section:last-of-type::before, .negarshop-countdown .countdown-section, #negarshopAlertBox, #negarshopAlertBox #closeBtn, .product-alerts .alert-item, .select_option, .select_option *, .product-single-actions, .cb-chips ul.chip-items li, .select2-container--default .select2-selection--single, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit, ul.dokan-account-migration-lists li, .product-summary-left, .header-main-menu.vertical-menu ul.main-menu > li:hover, article.w-p-item, article.w-p-item figure, .img-banner-wg, .btn, .form-control, .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-append > .btn, .input-group-sm > .input-group-append > .input-group-text, .input-group-sm > .input-group-prepend > .btn, .input-group-sm > .input-group-prepend > .input-group-text,#negarshop-to-top > span i,section.widget:not(.widget_media_image), .dokan-widget-area aside.widget:not(.widget_media_image),.content-widget:not(.transparent),.woocommerce.single-product div.product > .product-section, nav.woocommerce-breadcrumb, .woocommerce.single-product div.product .woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel, .dokan-other-vendor-camparison,.woocommerce .title-bg,.product-single-actions li a,.woocommerce .quantity.custom-num input.input-text,.woocommerce .quantity.custom-num span,.table-gray,.comment-form-header .comment-notes,.cb-nouislider .noUi-connects,input[type="submit"]:not(.browser-default), input[type="button"]:not(.browser-default), input[type="reset"]:not(.browser-default), .btn, .dokan-btn,article.product,.colored-dots .dot-item,.woocommerce.single-product div.product .product-section.single-style-2-gallery .owl-carousel.wc-product-carousel .owl-item .car-dtag,.owl-carousel.wc-product-carousel .owl-nav button,.owl-carousel.wc-product-carousel-thumbs .owl-item,.account-box .account-links,.account-box .account-links > li a,.header-main-nav .header-main-menu li > ul,.is-mega-menu-con.is-product-mega-menu .tabs a.item-hover,figure.optimized-1-1,.is-mega-menu-con.is-product-mega-menu .owl-carousel .owl-nav button,.content-widget.slider.product-archive figure.thumb,.content-widget.slider .carousel .carousel-indicators li,ul li.wc-layered-nav-rating a,.switch .slider,.switch .slider::before,.woocommerce-products-header,.price_slider_amount button.button,.modal-content, #cbQVModalCarousel .carousel-item,.woocommerce-pagination ul li a, .woocommerce-pagination ul li span,.tile-posts article.blog-item,section.blog-home article.post-item,section.blog-home article.post-item figure.post-thumb,section.blog-home article.post-item .entry-video .inner,.navigation.pagination .nav-links > *,.content-widget.blog-posts article.blog-item figure.thumbnail,.content-widget.blog-posts article.blog-item time,.content-widget.blog-posts article.blog-item,.content-widget.blog-posts .owl-dots button,section.blog-home .post-wg,section.blog-home .post-wg ol.comment-list article.comment-body,section.blog-home article.post-item .tags a,blockquote,nav.top-bar li > ul ,.header-search .search-box .search-result,.product-section .sale-timer-box, .product-section .sale-timer-box .counter-sec,.product-section .sale-timer-box .title .badge,.header-cart-basket.style-2 .cart-basket-box,.header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li a,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li,.woocommerce nav.woocommerce-MyAccount-navigation ul li a,.woocommerce-message, .woocommerce-error,.woocommerce-message a, .woocommerce-page.woocommerce-cart table.shop_table td.product-remove a.remove, .cart-collaterals .cart_totals,ul#shipping_method li label,.list-group,ul.woocommerce-order-overview, .header-cart-basket > .widget.widget_shopping_cart > .widget_shopping_cart_content,.header-cart-basket > .widget.widget_shopping_cart,footer.site-footer .about-site,ul.product-categories li.open .children,.wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn,.woocommerce-tabs.wc-tabs-wrapper > div.woocommerce-Tabs-panel .product-seller .store-avatar img,.ns-table tbody td.actions a.dislike_product,.is-mega-menu-con.is-product-mega-menu li.contents,footer.site-footer .support-times,li.recentcomments,section.widget.widget_media_image img,.header-main-menu.vertical-menu ul.main-menu,.prm-brands-list__item,.prm-brands-list__item .prm-brands-list__title,.content-widget.slider .carousel,.content-widget.title-widget span.icon,.products-carousel .carousel-banner,footer.site-footer .footer-socials ul li a,header.site-header .header-socials ul li a,.header-search .search-box .action-btns{-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px;}.product-video-carousel .owl-nav button, .input-group .form-control:not(select), textarea:not(.browser-default), .dokan-form-control, .form-control, input[type="text"]:not(.browser-default), input[type="search"]:not(.browser-default), input[type="email"]:not(.browser-default), input[type="url"]:not(.browser-default), input[type="number"]:not(.browser-default) ,.input-group .btn,.dokan-message, .dokan-info, .dokan-error{-webkit-border-radius: 20px !important; -moz-border-radius: 20px !important; border-radius: 20px !important;}.header-search.style-3 .search-box .action-btns {border-radius: 20px 0 0 20px;}.woocommerce-account:not(.logged-in) article.post-item .nav-pills, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .ns-store-avatar header.store-avatar-header,.woocommerce-page.woocommerce-cart table.shop_table tr:first-of-type td.product-subtotal{border-radius: 20px 20px 0 0;}.ns-store-avatar.wc-dashboard .user-actions a,.woocommerce-page.woocommerce-cart table.shop_table tr:nth-last-of-type(2) td.product-subtotal{border-radius: 0 0 20px 20px;}.ns-table tbody tr td:first-of-type {border-radius: 0 20px 20px 0;}.ns-table tbody tr td:last-of-type {border-radius: 20px 0 0 20px;}.comment .comment-awaiting-moderation::before{content: 'در انتظار تایید مدیریت'} .woocommerce-variation-price:not(:empty)::before{content: 'قیمت: '} .woocommerce-pagination ul li a.next::before{content: 'بعدی'} .woocommerce-pagination ul li a.prev::before{content: 'قبلی'} .woocommerce .quantity.custom-num label.screen-reader-text::before{content: 'تعداد: '} .yith-woocompare-widget ul.products-list li .remove::after{content: 'حذف'} .woocommerce .product .product_meta > span.product-brand::before{content: 'برند: '} .show-ywsl-box::before{content: 'برای ورود کلیک کنید'} a.reset_variations::before{content: 'پاک کردن ویژگی ها'} .woocommerce form .form-row .required::before{content: '(ضروری)'} .content-widget.price-changes .prices-table tbody td.past-price::before{content: 'قیمت قبل: '} .content-widget.price-changes .prices-table tbody td.new-price::before{content: 'قیمت جدید: '} .content-widget.price-changes .prices-table tbody td.changes::before{content: 'تغییرات: '} .content-widget.price-changes .prices-table tbody td.difference::before{content: 'مابه التفاوت: '} </style><script type='text/javascript'>var jsVars = {"borderActiveColor":"#039be5"};</script>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}" sizes="192x192" />
    <link rel="apple-touch-icon" href="{{asset('assets/img/favicon.png')}}" />
    <meta name="msapplication-TileImage" content="{{asset('assets/img/favicon.png')}}" />
    <style type="text/css" id="wp-custom-css">
        .baby-newspaper .elementor-widget-container .widget_mc4wp_form_widget{
            background:none !important
        }		</style>
</head>
<body class="rtl page-template page-template-elementor_header_footer page page-id-1768 res-banner-bg-trans pop-up-login theme-negarshop woocommerce-no-js elementor-default elementor-template-full-width elementor-kit-1558 elementor-page elementor-page-1768 dokan-theme-negarshop">
<div class="wrapper">
    @if(\Illuminate\Support\Facades\Session::has('Error'))
        <div class="alert text-danger text-center">{{\Illuminate\Support\Facades\Session::pull('Error')}}</div>
    @endif
    <span class="front-ajax d-none" id="{{route('frontAjax')}}"></span>

    <header class="site-header d-none d-xl-block d-lg-block">
        <div data-elementor-type="header" data-elementor-id="1763" class="elementor elementor-1763 elementor-location-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-10545fa elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="10545fa" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1d60e77" data-id="1d60e77" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a2f7654 elementor-widget elementor-widget-image" data-id="a2f7654" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="143" height="58" src="{{asset('newFront/images/4rak-logo.png')}}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{asset('newFront/images/4rak-logo.png')}} 143w, {{asset('newFront/images/4rak-logo.png')}} 50w" sizes="(max-width: 143px) 100vw, 143px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6aaef70" data-id="6aaef70" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-fb28708 elementor-widget elementor-widget-negarshop_header_search" data-id="fb28708" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-right darken-color-mode style-3 style-4 whiter">
                                                    <div data-type="product" class="search-box btn-style-dark ajax-form ">
                                                        <form class="form-tag" action="#">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-filters" type="button">
                                                                    <i class="flaticon-settings-1"></i></button>
                                                                <button class="action-btn search-submit" type="submit">
                                                                    <i class="far fa-search"></i></button>
                                                            </div>

                                                            <div class="search-options">
                                                                <button class="close-popup" type="button">
                                                                    <i class="fal fa-check"></i>
                                                                </button>
                                                                <div class="filters-parent">
                                                                    <div class="list-item">
                                                                        <label for="header-search-cat">دسته بندی ها</label>
                                                                        <select name="pcat" id="header-search-cat" class="negar-select">
                                                                            <option value="" selected>همه دسته ها</option>
                                                                            @foreach($categories as $cat)
                                                                                <option value="{{$cat->id}}" >{{$cat->title}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="list-item">
                                                                        <label for="header-search-stock">وضعیت محصول</label>
                                                                        <select name="stock" id="header-search-stock" class="negar-select">
                                                                            <option value="" selected>همه محصولات</option>
                                                                            <option value="instock" >فقط موجود ها</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <ul class="search-result"></ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-736120d" data-id="736120d" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-523432f elementor-widget elementor-widget-negarshop_header_basket" data-id="523432f" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-2 child-hover-right">
                                                    <a href="{{route('ShoppingCart')}}" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <bdi><span class="woocommerce-Price-currencySymbol">تومان</span>
                                                                    <span id="total-price">{{Session::has('cart') ? number_format(Session::get('cart')['totalPrice']) : "0"}}</span></bdi>
                                                            </span>
                                                        </span>
                                                        <span id="total-qty" class="count">{{Session::has('cart') ? Session::get('cart')['totalQty'] : "0"}}</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 mx-3 elementor-top-column elementor-element elementor-element-6d8277b" data-id="6d8277b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated p-0 align-items-center">
                                    <div class="elementor-widget-wrap align-items-center">
                                        <div class="elementor-element elementor-element-93a7144 elementor-widget elementor-widget-negarshop_header_account" data-id="93a7144" data-element_type="widget" data-widget_type="negarshop_header_account.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-account text-right style-2">
                                                    <div class="account-box">
                                                        @guest()
                                                        <i class="flaticon-avatar"></i>
                                                        @else
                                                            @if(\Illuminate\Support\Facades\Auth::user()->photo)
                                                                <img width="50px" height="50px" style="max-height: 50px;min-width: 50px" class="img-fluid rounded-circle" src="{{asset('images/profile/'.\Illuminate\Support\Facades\Auth::user()->photo->path)}}" alt="">
                                                            @else
                                                                <img width="50px" height="50px" class="img-fluid" src="{{asset('img/user-avatar.svg')}}" alt="">
                                                            @endif
                                                            @endguest
                                                            @guest()
                                                                <span class="subtitle">لطفا وارد حساب خود شوید!</span>
                                                                <ul class="account-links">
                                                                <li>
                                                                    <a href="{{route('login')}}">ورود به حساب</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{route('login')}}">ثبت نام</a>
                                                                </li>
                                                                </ul>
                                                            @else
                                                                <span class="subtitle">{{Auth::user()->name}} {{Auth::user()->family}}</span>
                                                                <ul class="account-links">
                                                                <li>
                                                                    <a href="{{route('dashboard')}}">پنل کاربری</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{route('Profile.index')}}">پروفایل</a>
                                                                </li>
                                                                <li>
                                                                    <form method="POST" action="{{route('logout')}}">
                                                                        @csrf
                                                                        <button type="submit" class="btn btn-danger w-100">خروج</button>
                                                                    </form>
                                                                </li>
                                                                </ul>
                                                            @endguest
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-42473b5 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="42473b5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-4190cd5" data-id="4190cd5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-3dfebc5 elementor-widget elementor-widget-negarshop_header_menu" data-id="3dfebc5" data-element_type="widget" data-widget_type="negarshop_header_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="">
                                                    <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                        <div class="row align-items-center">
                                                            <div class="col-auto header-main-menu vertical-menu">
                                                                <button class="vertical-menu-toggle"><i class="fal fa-bars"></i>دسته بندی ها</button>
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c-%d8%a2%db%8c%da%a9%d9%86-%d8%af%d8%a7%d8%b1" class="main-menu category-menu">
                                                             @foreach($categories as $cat)
                                                                <li id="menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-has-icon menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}">
                                                                     <a href="{{route('advanceCategory', ['همه','همه',$cat->title])}}" >
                                                                         @if($cat->photo)
                                                                         <i class="item-icon" style="background-image: url('{{asset('images/cats/'.$cat->photo->path)}}');"></i>
                                                                         @else
                                                                             <i class="item-icon fab fa-microsoft" ></i>
                                                                         @endif
                                                                         {{$cat->title}}</a>
                                                                 </li>
                                                             @endforeach
                                                                </ul>
                                                            </div>
                                                            <div class="col header-main-menu-col header-main-menu text-right">
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d9%86%d9%88%d8%a7%d8%b1-%d8%a8%d8%a7%d9%84%d8%a7%db%8c%db%8c" class="main-menu">
                                                                    <li id="menu-item-357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357">
                                                                        <a href="{{route('blog')}}" >وبلاگ</a></li>
                                                                    <li id="menu-item-359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-359">
                                                                        <a href="{{route('allShops')}}" >لیست فروشگاه ها</a></li>
                                                                    <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                        <a href="{{route('guilds')}}" >اصناف</a></li>
                                                                    <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                        <a href="{{route('advanceCategory',['همه','همه','همه'])}}" >گروه های کالا</a></li>

                                                                    <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                            <a href="{{route('Profile.index')}}" >حساب من</a>
                                                                    </li>
                                                                    <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                            <a href="{{route('about')}}" > درباره ما</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-cdf1b22" data-id="cdf1b22" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-fe9f4df elementor-icon-list--layout-inline elementor-align-left elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="fe9f4df" data-element_type="widget" data-widget_type="icon-list.default">
                                            <div class="elementor-widget-container">
                                                <ul class="elementor-icon-list-items elementor-inline-items">
                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                        <a target="_blank" href="https://www.linkedin.com/company/4rak/">
                                                            <span class="elementor-icon-list-icon">
                                                        <i aria-hidden="true" class="fab fa-linkedin"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"></span>
                                                        </a>
                                                    </li>
                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                        <a href="mailto:info@4rak.ir">
                                                            <span class="elementor-icon-list-icon">
                                                        <i aria-hidden="true" class="fas fa-envelope"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"></span>
                                                        </a>
                                                    </li>
                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                        <a target="_blank" href="https:\\instagram.com\4rak.ir">
                                                            <span class="elementor-icon-list-icon">
                                                        <i aria-hidden="true" class="fab fa-instagram"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"></span>
                                                        </a>
                                                    </li>
                                                    <li class="elementor-icon-list-item elementor-inline-item">
                                                        <a target="_blank" href="https:\\t.me\ir4rak">
                                                            <span class="elementor-icon-list-icon">
                                                        <i aria-hidden="true" class="fab fa-telegram"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>

    <header class="responsive-header d-block d-xl-none d-lg-none">
        <div data-elementor-type="section" data-elementor-id="2188" class="elementor elementor-2188 elementor-location-responsive-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-345f71c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="345f71c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-79a8412" data-id="79a8412" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-082d926 elementor-widget elementor-widget-negarshop_responsive_menu" data-id="082d926" data-element_type="widget" data-widget_type="negarshop_responsive_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="responsive-menu">
                                                    <button class="btn btn-transparent toggle-menu"><i class="far fa-bars"></i></button>
                                                    <div class="menu-popup">
                                                        <nav class="responsive-navbar">
                                                            <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c" class="categories-menu">
                                                           @foreach($categories as $cat)
                                                                    <li id="menu-item-190" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-{{$cat->id}}">
                                                                        <a href="{{route('advanceCategory', ['همه','همه',$cat->title])}}">
                                                                            {{$cat->title}}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </nav>
                                                    </div>

                                                    <div class="overlay">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6503f75" data-id="6503f75" data-element_type="column">

                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-18a1aeb" data-id="18a1aeb" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-704fc73 elementor-view-default elementor-widget elementor-widget-icon" data-id="704fc73" data-element_type="widget" data-widget_type="icon.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-wrapper">
                                                    <a class="elementor-icon" href="{{route('Profile.index')}}">
                                                        <i aria-hidden="true" class="far fa-user"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-87822a2" data-id="87822a2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-92646d4 elementor-widget elementor-widget-negarshop_header_basket" data-id="92646d4" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-3 child-hover-right">
                                                    <a href="{{route('ShoppingCart')}}" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle">
                                                            <span class="woocommerce-Price-amount amount">
                                                                <bdi><span class="woocommerce-Price-currencySymbol">تومان</span>
                                                                    <span id="total-price">{{Session::has('cart') ? Session::get('cart')['totalPrice'] : "0"}}</span></bdi>
                                                            </span>
                                                        </span>
                                                        <span id="total-qty" class="count">{{Session::has('cart') ? Session::get('cart')['totalQty'] : "0"}}</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-row align-items-center mt-2 bg-light text-center">
                            <div class="elementor-column  align-items-center elementor-col-50 elementor-top-column elementor-element elementor-element-4190cd5" data-id="4190cd5" data-element_type="column">
                                <div class="elementor-column-wrap align-items-center elementor-element-populated">
                                    <div class="elementor-widget-wrap align-items-center">
                                        <div class="elementor-element align-items-center elementor-element-3dfebc5 elementor-widget elementor-widget-negarshop_header_menu" data-id="3dfebc5" data-element_type="widget" data-widget_type="negarshop_header_menu.default">
                                            <div class="elementor-widget-container align-items-center">
                                                <div class="">
                                                    <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                        <div class="row align-items-center">
                                                            <div class="col header-main-menu-col header-main-menu text-center">
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d9%86%d9%88%d8%a7%d8%b1-%d8%a8%d8%a7%d9%84%d8%a7%db%8c%db%8c" class="main-menu">
                                                                    <li id="menu-item-357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-357">
                                                                        <a href="{{route('blog')}}" >وبلاگ</a></li>
                                                                    <li id="menu-item-359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-359">
                                                                        <a href="{{route('allShops')}}" >لیست فروشگاه ها</a></li>
                                                                    <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                        <a href="{{route('guilds')}}" >اصناف</a></li>
                                                                    <li id="menu-item-358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358">
                                                                        <a href="{{route('advanceCategory',['همه','همه','همه'])}}" >گروه های کالا</a></li>

                                                                    <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360">
                                                                        <a href="{{route('about')}}" > درباره ما</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-275eab9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="275eab9" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28b1d92" data-id="28b1d92" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7ea9db8 elementor-widget elementor-widget-negarshop_header_search" data-id="7ea9db8" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-right darken-color-mode style-5 whiter">
                                                    <div data-type="product" class="search-box btn-style-dark ajax-form ">
                                                        <form class="form-tag" action="https://demo.coderboy.ir/negarshop">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-submit" type="submit"><i class="far fa-search"></i>
                                                                </button>
                                                            </div>

                                                        </form>
                                                        <ul class="search-result">

                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>

    <div data-elementor-type="wp-page" data-elementor-id="1768" class="elementor elementor-1768" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-2d16284 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="2d16284" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-0851335" data-id="0851335" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f3f1748 elementor-widget elementor-widget-sidebar" data-id="f3f1748" data-element_type="widget" data-widget_type="sidebar.default">
                                            <div class="elementor-widget-container">
                                                <section id="megamenu_widget-2" class="widget negarshop_wg pro-menus header-main-nav d-none d-xl-block">
                                                    <div class="header-main-menu vertical-menu">
                                                        <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c-%d8%a2%db%8c%da%a9%d9%86-%d8%af%d8%a7%d8%b1-1" class="main-menu">
                                                      @foreach($categories as $cat)
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-has-icon menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}">
                                                                    <a href="{{route('advanceCategory', ['همه','همه',$cat->title])}}" >
                                                                        @if($cat->photo)
                                                                            <i class="item-icon" style="background-image: url('{{asset('images/cats/'.$cat->photo->path)}}');"></i>
                                                                        @else
                                                                            <i class="item-icon fab fa-microsoft" ></i>
                                                                        @endif
                                                                        {{$cat->title}}</a></li>
                                                      @endforeach
                                                        </ul>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-5aee37f elementor-widget elementor-widget-negarshop_moment" data-id="5aee37f" data-element_type="widget" data-widget_type="negarshop_moment.default">
                                            <div class="elementor-widget-container">
                                                <section class="content-widget exc-header offer-moments no-title" id="content-widget-5aee37f">
                                                    <div class="owl-carousel">
                                                        @foreach($top_5_products as $product)
                                                            <div class="om-item simple">
                                                                <figure>
                                                                    <a href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                                        @if($product->photo)
                                                                            <img class="lazy" data-src="{{asset('images/products/'.$product->photo->path)}}" alt="{{$product->name}}">
                                                                        @else
                                                                        <img class="lazy" data-src="{{asset('img/product-placeholder.png')}}" alt="{{$product->name}}">
                                                                            @endif
                                                                    </a>
                                                                </figure>
                                                                <div class="item-footer">
                                                                    <h3 class="title">
                                                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}">{{$product->name}}</a>
                                                                    </h3>
                                                                    <div class="price">
                                                                        <ins>
                                                                            <span class="woocommerce-Price-amount amount">
                                                                                <bdi>
                                                                                    <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;{{number_format($product->sell_price)}}
                                                                                </bdi>
                                                                            </span>
                                                                        </ins>
                                                                    </div>
                                                                </div>
                                                                <div class="loader-bar"></div>
                                                            </div>
                                                        @endforeach

                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-5df20c3 elementor-widget elementor-widget-sidebar" data-id="5df20c3" data-element_type="widget" data-widget_type="sidebar.default">
                                            <div class="elementor-widget-container">

                                                <section id="premmerce_brands_widget-3" class="widget widget_premmerce_brands_widget">
                                                    <header class="wg-header"><h6>اصناف</h6></header>
                                                    <div class="">
                                                        <ul class="d-inline-block">
                                                            @foreach($guilds as $guild)
                                                                <li class="w-100 my-2">
                                                                    <a class="" href="{{route('guilds')}}">
                                                                        <img class="img-fluid rounded mx-2" style="max-height: 50px;max-width: 50px" src="{{asset('images/1608550517default.jpg')}}" alt="">
                                                                        {{$guild->name}}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                </section>
                                                <section id="media_image-2" class="widget widget_media_image">
                                                    @if($side_banner)
                                                    <a href="{{$side_banner->url}}">
                                                        <img width="300" height="284" src="{{asset('images/banner/'.$side_banner->photo->path)}}" class="image wp-image-677 my-2 attachment-medium size-medium" alt="" loading="lazy" style="max-width: 100%; height: auto;" srcset="{{asset('images/banner/'.$side_banner->photo->path)}} 300w, {{asset('images/banner/'.$side_banner->photo->path)}} 50w, {{asset('images/banner/'.$side_banner->photo->path)}} 305w" sizes="(max-width: 300px) 100vw, 300px" />
                                                    </a>
                                                        @endif
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-c560b3a" data-id="c560b3a" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-8ec98fd elementor--h-position-left elementor--v-position-bottom elementor-arrows-position-inside elementor-widget elementor-widget-slides" data-id="8ec98fd" data-element_type="widget" data-settings="{&quot;navigation&quot;:&quot;arrows&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                            <div class="elementor-widget-container">
                                            {{-- FIRST SLIDER--}}
                                                <div  class="elementor-swiper">
                                                        <div class="uk-position-relative uk-visible-toggle uk-light" style="border-radius: 25px;cursor: pointer" tabindex="-1" uk-slider="clsActivated: uk-transition-active; center: true">
                                                            <ul class="uk-slider-items uk-grid" style="max-height: 300px" >
                                                                @foreach($slider_imgs as $img)
                                                                    <li class="uk-width-4-4">
                                                                        <div class="uk-panel">
                                                                            <img src="{{asset('images/slider/'.$img->photo->path)}}" alt="">
                                                                            <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-transition-slide-bottom">
                                                                                <h3 class="uk-margin-remove"></h3>
                                                                                <p class="uk-margin-remove">{{$img->note}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                                                            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                                                        </div>
                                                </div>
                                           {{--end FIRST SLIDER--}}
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-f71ed3c elementor-widget elementor-widget-negarshop_amazing" data-id="f71ed3c" data-element_type="widget" data-widget_type="negarshop_amazing.default">
                                            <div class="elementor-widget-container">
                                                <section class="content-widget slider-2 style-2  mini-tab"
                                                         id="content-widget-f71ed3c">
                                                    <h6 class="responsive-title">پیشنهاد های شگفت انگیز</h6>
                                                    <div id="carouselIndicators-f71ed3c" class="carousel ns-fade" data-ride="carousel">
                                                        <div class="row align-items-center">
                                                            <div class="col-lg-auto">
                                                                <div class="cind-inner">
                                                                    <ol class="carousel-indicators">
                                                                        @php($flag=true)
                                                                        @php($row=0)
                                                                       @foreach($top_5_products as $t5)
                                                                           @if($flag)
                                                                            <li data-target="#carouselIndicators-f71ed3c"
                                                                                data-slide-to="{{$row}}"
                                                                                class="active">
                                                                                <span>{{$t5->name}}</span>
                                                                            </li>
                                                                            @else
                                                                                <li data-target="#carouselIndicators-f71ed3c"
                                                                                    data-slide-to="{{$row}}"
                                                                                    class="">
                                                                                    <span>{{$t5->name}}</span>
                                                                                </li>
                                                                               @endif
                                                                           @php($flag=false)
                                                                           @php($row++)
                                                                    @endforeach
                                                                   {{--   <li data-target="#carouselIndicators-f71ed3c"
                                                                            data-slide-to="0"
                                                                            class="active">
                                                                            <span>ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه</span>
                                                                        </li>
                                                                        <li data-target="#carouselIndicators-f71ed3c"
                                                                            data-slide-to="1"
                                                                            class=""><span>ماشین شارژی مدل 5256</span></li>
                                                                        <li data-target="#carouselIndicators-f71ed3c"
                                                                            data-slide-to="2"
                                                                            class=""><span>کفش مخصوص دویدن مردانه ریباک مدل ZQuick TR 3.0 کد M47653</span></li>
                                                                        <li data-target="#carouselIndicators-f71ed3c"
                                                                            data-slide-to="3"
                                                                            class=""><span>آبمیوه گیر سه کاره ایز هولد 65</span></li>
                                                                        <li data-target="#carouselIndicators-f71ed3c"
                                                                            data-slide-to="4"
                                                                            class=""><span>متابوک هواوای دی رایزن 5</span></li>--}}
                                                                    </ol>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg">
                                                                <div class="carousel-inner">
                                                                    @php($f=true)
                                                                    @php($row=0)
                                                                    @foreach($top_5_products as $product)
                                                                        @if($f)
                                                                            <div class="carousel-item active">
                                                                                <div class="row">
                                                                                    <div class="col product-thumb">
                                                                                        <div class="pt-content">
                                                                                            <div class="inner">
                                                                                                <div class="thumbnail-fig">
                                                                                                    @if($product->photo)
                                                                                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}"
                                                                                                           style="background-image: url({{asset('images/products/'.$product->photo->path)}});">
                                                                                                        </a>
                                                                                                    @else
                                                                                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}"
                                                                                                           style="background-image: url({{asset('img/product-placeholder.png')}});">
                                                                                                        </a>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-auto product-info">
                                                                                        <div class="footer-sec">
                                                                                            <div class="ribbon-discount-outer">
                                                                                            </div>
                                                                                            <div class="title-rate-sec">
                                                                                                <h2 class="item-title">
                                                                                                    <a
                                                                                                        href="{{route('singleProduct',[$product->slug,$product->id])}}">{{$product->name}}</a>
                                                                                                </h2>
                                                                                            </div>

                                                                                            <div class="feature-daels-price">
                                                                                                <span class="sale-price variable w-100">
                                                                                                    <span class="woocommerce-Price-amount amount">
                                                                                                        <bdi>
                                                                                                            <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;{{number_format($product->sell_price)}}
                                                                                                        </bdi>
                                                                                                    </span>
                                                                                            </div>

                                                                                            <ul class="feature-attr-p">
                                                                                            </ul>

                                                                                            <div class="countdown-outer">
                                                                                            </div>

                                                                                            <div class="product-quantity">
                                                                                                <div>
                                                                                                    <span class="sale">فروش: 0</span>
                                                                                                    <span class="in-stuck float-left">موجودی: {{$product->quantity}}</span>
                                                                                                </div>
                                                                                                <div class="progress mb-2" style="height: 5px;">
                                                                                                    <div class="progress-bar" role="progressbar"
                                                                                                         style="width: 0%;"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @else
                                                                            <div class="carousel-item ">
                                                                                <div class="row">
                                                                                    <div class="col product-thumb">
                                                                                        <div class="pt-content">
                                                                                            <div class="inner">
                                                                                                <div class="thumbnail-fig">
                                                                                                    @if($product->photo)
                                                                                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}"
                                                                                                           style="background-image: url({{asset('images/products/'.$product->photo->path)}});">
                                                                                                        </a>
                                                                                                    @else
                                                                                                        <a href="{{route('singleProduct',[$product->slug,$product->id])}}"
                                                                                                           style="background-image: url({{asset('img/product-placeholder.png')}});">
                                                                                                        </a>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-auto product-info">
                                                                                        <div class="footer-sec">
                                                                                            <div class="ribbon-discount-outer">
                                                                                            </div>
                                                                                            <div class="title-rate-sec">
                                                                                                <h2 class="item-title">
                                                                                                    <a
                                                                                                        href="{{route('singleProduct',[$product->slug,$product->id])}}">{{$product->name}}</a>
                                                                                                </h2>
                                                                                            </div>

                                                                                            <div class="feature-daels-price">
                                                                                                <span class="sale-price variable w-100">
                                                                                                    <span class="woocommerce-Price-amount amount">
                                                                                                        <bdi>
                                                                                                            <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;{{number_format($product->sell_price)}}
                                                                                                        </bdi>
                                                                                                    </span>
                                                                                            </div>

                                                                                            <ul class="feature-attr-p">
                                                                                            </ul>

                                                                                            <div class="countdown-outer">
                                                                                            </div>

                                                                                            <div class="product-quantity">
                                                                                                <div>
                                                                                                    <span class="sale">فروش: 0</span>
                                                                                                    <span class="in-stuck float-left">موجودی: {{$product->quantity}}</span>
                                                                                                </div>
                                                                                                <div class="progress mb-2" style="height: 5px;">
                                                                                                    <div class="progress-bar" role="progressbar"
                                                                                                         style="width: 0%;"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                        @php($f=false)
                                                                        @php($row++)
                                                                    @endforeach

                                                                 {{--   <div class="carousel-item active">
                                                                        <div class="row">
                                                                            <div class="col product-thumb">
                                                                                <div class="pt-content">
                                                                                    <div class="inner">
                                                                                        <div class="thumbnail-fig">
                                                                                            <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/"
                                                                                               style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806598-400x336.jpg);"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-auto product-info">
                                                                                <div class="footer-sec">
                                                                                    <div class="ribbon-discount-outer">
                                                                                    </div>
                                                                                    <div class="title-rate-sec">
                                                                                        <h2 class="item-title"><a
                                                                                                    href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/">ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه</a>

                                                                                        </h2>
                                                                                    </div>

                                                                                    <div class="feature-daels-price">
                                                                                        <span class="sale-price variable w-100"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۳۵,۴۰۰</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۳۵,۵۰۰</bdi></span></span>
                                                                                    </div>


                                                                                    <ul class="feature-attr-p">
                                                                                    </ul>

                                                                                    <div class="countdown-outer">
                                                                                    </div>

                                                                                    <div class="product-quantity">
                                                                                        <div>
                                                                                            <span class="sale">فروش: 0</span>
                                                                                            <span class="in-stuck float-left">موجودی: نامحدود</span>
                                                                                        </div>
                                                                                        <div class="progress mb-2" style="height: 5px;">
                                                                                            <div class="progress-bar" role="progressbar"
                                                                                                 style="width: 0%;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item ">
                                                                        <div class="row">
                                                                            <div class="col product-thumb">
                                                                                <div class="pt-content">
                                                                                    <div class="inner">
                                                                                        <div class="thumbnail-fig">
                                                                                            <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/"
                                                                                               style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269259-400x400.jpg);"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-auto product-info">
                                                                                <div class="footer-sec">
                                                                                    <div class="ribbon-discount-outer">
                                                                                    </div>
                                                                                    <div class="title-rate-sec">
                                                                                        <h2 class="item-title"><a
                                                                                                    href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/">ماشین شارژی مدل 5256</a>

                                                                                        </h2>
                                                                                    </div>

                                                                                    <div class="feature-daels-price">
                                                                                        <span class="sale-price variable w-100"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶,۰۰۰,۰۰۰</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶,۳۰۰,۰۰۰</bdi></span></span>
                                                                                    </div>


                                                                                    <ul class="feature-attr-p">
                                                                                    </ul>

                                                                                    <div class="countdown-outer">
                                                                                    </div>

                                                                                    <div class="product-quantity">
                                                                                        <div>
                                                                                            <span class="sale">فروش: 0</span>
                                                                                            <span class="in-stuck float-left">موجودی: نامحدود</span>
                                                                                        </div>
                                                                                        <div class="progress mb-2" style="height: 5px;">
                                                                                            <div class="progress-bar" role="progressbar"
                                                                                                 style="width: 0%;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item ">
                                                                        <div class="row">
                                                                            <div class="col product-thumb">
                                                                                <div class="pt-content">
                                                                                    <div class="inner">
                                                                                        <div class="thumbnail-fig">
                                                                                            <a href="https://demo.coderboy.ir/negarshop/product/%da%a9%d9%81%d8%b4-%d9%85%d8%ae%d8%b5%d9%88%d8%b5-%d8%af%d9%88%db%8c%d8%af%d9%86-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-zquick-tr-3-0-%da%a9%d8%af-m47653/"
                                                                                               style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/adidasFALCON-400x400.png);"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-auto product-info">
                                                                                <div class="footer-sec">
                                                                                    <div class="ribbon-discount-outer">
												                                                    <span class="ribbon-discount">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     viewBox="0 0 175.92 174"><g class="cls-1"><g
                                                                id="Layer_2" data-name="Layer 2"><g id="OBJECTS">
                                                                <path
                                                                        class="cls-3"
                                                                        d="M148.38,109.14a53,53,0,0,1-1-7.61,31,31,0,0,1,.26-4.62c.24-2,1-2.49-1-3,1.15.19,1.36.62,1.63-.66a29.28,29.28,0,0,1,4.65-10.79c3.67-5.74,5.86-11.34,4.41-18.28-1.66-7.92-6.74-12.43-13.6-16.13a32.52,32.52,0,0,1-14.38-15.4c-2.57-6-3.7-12.09-9.33-16.16S107.12,10.76,100.39,13c-5.47,1.82-10.11,4.21-16,4.52a30.72,30.72,0,0,1-11.16-1.26c-2.64-.87-5.21-1.93-7.91-2.59a21.82,21.82,0,0,0-22.07,8.82C40.62,26.27,40,30.8,38.47,35a27.38,27.38,0,0,1-2.82,5.62c-.23.35-1.78,2.32-1.58,2.48l1,.8c-.24-.17-.91-.88-1.19-.87a31.29,31.29,0,0,1-3.67,4.21c-2.45,2.34-5.37,4-8.19,5.79a21.62,21.62,0,0,0-6.13,30.78,59.05,59.05,0,0,1,5,7.67,30.42,30.42,0,0,1,1.88,5.14c.36,1.33.51.89,1.68.61-1.18.31-1.5,0-1.17,1.35a31,31,0,0,1,.83,5.43c.22,4.06-.81,8-.81,12.08C23.2,123.65,28,131,34.47,134.61c5.64,3.18,11.83,2.1,17.9,3.35a32.69,32.69,0,0,1,15.86,8.49c4.52,4.35,8.57,8.4,15.12,9.21,7.84,1,14.48-1.72,19.77-7.44,4.6-5.06,9.13-9.1,15.71-11.46,7-2.49,14.64-1.11,20.89-5.69A22,22,0,0,0,148.38,109.14Z"/><circle
                                                                        class="cls-4" cx="84.87" cy="80.93" r="56.59"
                                                                        transform="translate(-32.37 83.72) rotate(-45)"/></g></g></g></svg>
                                                    <span class="count"><i
                                                                class="fal fa-percent"></i>6.7</span>
                                                </span>
                                                                                    </div>
                                                                                    <div class="title-rate-sec">
                                                                                        <h2 class="item-title"><a
                                                                                                    href="https://demo.coderboy.ir/negarshop/product/%da%a9%d9%81%d8%b4-%d9%85%d8%ae%d8%b5%d9%88%d8%b5-%d8%af%d9%88%db%8c%d8%af%d9%86-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-zquick-tr-3-0-%da%a9%d8%af-m47653/">کفش مخصوص دویدن مردانه ریباک مدل ZQuick TR 3.0 کد M47653</a>

                                                                                        </h2>
                                                                                    </div>

                                                                                    <div class="feature-daels-price">
												                                                    <span class="
                                remove-price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۵۰,۰۰۰</bdi></span></span>
                                                                                        <span class="sale-price">
                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۴۰,۰۰۰</bdi></span></span>                        												                                            </div>


                                                                                    <ul class="feature-attr-p">
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">جنس: </span>
                                                                                            <span class="product-attr-text"><span> پنبه</span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">جیب: </span>
                                                                                            <span class="product-attr-text"><span>ندارد </span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">فرم یقه: </span>
                                                                                            <span class="product-attr-text"><span>برگردان</span></span>
                                                                                        </li>
                                                                                    </ul>

                                                                                    <div class="countdown-outer">
                                                                                        <div class="negarshop-countdown"
                                                                                             data-date="2021/09/27 20:29:59"></div>
                                                                                    </div>

                                                                                    <div class="product-quantity">
                                                                                        <div>
                                                                                            <span class="sale">فروش: 26</span>
                                                                                            <span class="in-stuck float-left">موجودی: نامحدود</span>
                                                                                        </div>
                                                                                        <div class="progress mb-2" style="height: 5px;">
                                                                                            <div class="progress-bar" role="progressbar"
                                                                                                 style="width: 0%;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item ">
                                                                        <div class="row">
                                                                            <div class="col product-thumb">
                                                                                <div class="pt-content">
                                                                                    <div class="inner">
                                                                                        <div class="thumbnail-fig">
                                                                                            <a href="https://demo.coderboy.ir/negarshop/product/%d8%a2%d8%a8%d9%85%db%8c%d9%88%d9%87-%da%af%db%8c%d8%b1-%d8%b3%d9%87-%da%a9%d8%a7%d8%b1%d9%87-%d8%a7%db%8c%d8%b2-%d9%87%d9%88%d9%84%d8%af-65/"
                                                                                               style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/61DJMqGm7qL._SL1000_-400x400.jpg);"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-auto product-info">
                                                                                <div class="footer-sec">
                                                                                    <div class="ribbon-discount-outer">
												                                                    <span class="ribbon-discount">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     viewBox="0 0 175.92 174"><g class="cls-1"><g
                                                                id="Layer_2" data-name="Layer 2"><g id="OBJECTS">
                                                                <path
                                                                        class="cls-3"
                                                                        d="M148.38,109.14a53,53,0,0,1-1-7.61,31,31,0,0,1,.26-4.62c.24-2,1-2.49-1-3,1.15.19,1.36.62,1.63-.66a29.28,29.28,0,0,1,4.65-10.79c3.67-5.74,5.86-11.34,4.41-18.28-1.66-7.92-6.74-12.43-13.6-16.13a32.52,32.52,0,0,1-14.38-15.4c-2.57-6-3.7-12.09-9.33-16.16S107.12,10.76,100.39,13c-5.47,1.82-10.11,4.21-16,4.52a30.72,30.72,0,0,1-11.16-1.26c-2.64-.87-5.21-1.93-7.91-2.59a21.82,21.82,0,0,0-22.07,8.82C40.62,26.27,40,30.8,38.47,35a27.38,27.38,0,0,1-2.82,5.62c-.23.35-1.78,2.32-1.58,2.48l1,.8c-.24-.17-.91-.88-1.19-.87a31.29,31.29,0,0,1-3.67,4.21c-2.45,2.34-5.37,4-8.19,5.79a21.62,21.62,0,0,0-6.13,30.78,59.05,59.05,0,0,1,5,7.67,30.42,30.42,0,0,1,1.88,5.14c.36,1.33.51.89,1.68.61-1.18.31-1.5,0-1.17,1.35a31,31,0,0,1,.83,5.43c.22,4.06-.81,8-.81,12.08C23.2,123.65,28,131,34.47,134.61c5.64,3.18,11.83,2.1,17.9,3.35a32.69,32.69,0,0,1,15.86,8.49c4.52,4.35,8.57,8.4,15.12,9.21,7.84,1,14.48-1.72,19.77-7.44,4.6-5.06,9.13-9.1,15.71-11.46,7-2.49,14.64-1.11,20.89-5.69A22,22,0,0,0,148.38,109.14Z"/><circle
                                                                        class="cls-4" cx="84.87" cy="80.93" r="56.59"
                                                                        transform="translate(-32.37 83.72) rotate(-45)"/></g></g></g></svg>
                                                    <span class="count"><i
                                                                class="fal fa-percent"></i>3.3</span>
                                                </span>
                                                                                    </div>
                                                                                    <div class="title-rate-sec">
                                                                                        <h2 class="item-title"><a
                                                                                                    href="https://demo.coderboy.ir/negarshop/product/%d8%a2%d8%a8%d9%85%db%8c%d9%88%d9%87-%da%af%db%8c%d8%b1-%d8%b3%d9%87-%da%a9%d8%a7%d8%b1%d9%87-%d8%a7%db%8c%d8%b2-%d9%87%d9%88%d9%84%d8%af-65/">آبمیوه گیر سه کاره ایز هولد 65</a>

                                                                                        </h2>
                                                                                    </div>

                                                                                    <div class="feature-daels-price">
												                                                    <span class="
                                remove-price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱,۵۰۰,۰۰۰</bdi></span></span>
                                                                                        <span class="sale-price">
                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱,۴۵۰,۰۰۰</bdi></span></span>                        												                                            </div>


                                                                                    <ul class="feature-attr-p">
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">قفل کودک: </span>
                                                                                            <span class="product-attr-text"><span>دارد</span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">اخطار باز ماندن درب: </span>
                                                                                            <span class="product-attr-text"><span>بله</span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">آبسردکن: </span>
                                                                                            <span class="product-attr-text"><span>دارد</span></span>
                                                                                        </li>
                                                                                    </ul>

                                                                                    <div class="countdown-outer">
                                                                                        <div class="negarshop-countdown"
                                                                                             data-date="2021/03/17 20:29:59"></div>
                                                                                    </div>

                                                                                    <div class="product-quantity">
                                                                                        <div>
                                                                                            <span class="sale">فروش: 257</span>
                                                                                            <span class="in-stuck float-left">موجودی: نامحدود</span>
                                                                                        </div>
                                                                                        <div class="progress mb-2" style="height: 5px;">
                                                                                            <div class="progress-bar" role="progressbar"
                                                                                                 style="width: 0%;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item ">
                                                                        <div class="row">
                                                                            <div class="col product-thumb">
                                                                                <div class="pt-content">
                                                                                    <div class="inner">
                                                                                        <div class="thumbnail-fig">
                                                                                            <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%aa%d8%a7%d8%a8%d9%88%da%a9-%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-d-%d8%b1%d8%a7%db%8c%d8%b2%d9%86-5/"
                                                                                               style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/61FaWQvnFxL._SL1000_-400x400.jpg);"></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-auto product-info">
                                                                                <div class="footer-sec">
                                                                                    <div class="ribbon-discount-outer">
												                                                    <span class="ribbon-discount">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     viewBox="0 0 175.92 174"><g class="cls-1"><g
                                                                id="Layer_2" data-name="Layer 2"><g id="OBJECTS">
                                                                <path
                                                                        class="cls-3"
                                                                        d="M148.38,109.14a53,53,0,0,1-1-7.61,31,31,0,0,1,.26-4.62c.24-2,1-2.49-1-3,1.15.19,1.36.62,1.63-.66a29.28,29.28,0,0,1,4.65-10.79c3.67-5.74,5.86-11.34,4.41-18.28-1.66-7.92-6.74-12.43-13.6-16.13a32.52,32.52,0,0,1-14.38-15.4c-2.57-6-3.7-12.09-9.33-16.16S107.12,10.76,100.39,13c-5.47,1.82-10.11,4.21-16,4.52a30.72,30.72,0,0,1-11.16-1.26c-2.64-.87-5.21-1.93-7.91-2.59a21.82,21.82,0,0,0-22.07,8.82C40.62,26.27,40,30.8,38.47,35a27.38,27.38,0,0,1-2.82,5.62c-.23.35-1.78,2.32-1.58,2.48l1,.8c-.24-.17-.91-.88-1.19-.87a31.29,31.29,0,0,1-3.67,4.21c-2.45,2.34-5.37,4-8.19,5.79a21.62,21.62,0,0,0-6.13,30.78,59.05,59.05,0,0,1,5,7.67,30.42,30.42,0,0,1,1.88,5.14c.36,1.33.51.89,1.68.61-1.18.31-1.5,0-1.17,1.35a31,31,0,0,1,.83,5.43c.22,4.06-.81,8-.81,12.08C23.2,123.65,28,131,34.47,134.61c5.64,3.18,11.83,2.1,17.9,3.35a32.69,32.69,0,0,1,15.86,8.49c4.52,4.35,8.57,8.4,15.12,9.21,7.84,1,14.48-1.72,19.77-7.44,4.6-5.06,9.13-9.1,15.71-11.46,7-2.49,14.64-1.11,20.89-5.69A22,22,0,0,0,148.38,109.14Z"/><circle
                                                                        class="cls-4" cx="84.87" cy="80.93" r="56.59"
                                                                        transform="translate(-32.37 83.72) rotate(-45)"/></g></g></g></svg>
                                                    <span class="count"><i
                                                                class="fal fa-percent"></i>2.7</span>
                                                </span>
                                                                                    </div>
                                                                                    <div class="title-rate-sec">
                                                                                        <h2 class="item-title"><a
                                                                                                    href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%aa%d8%a7%d8%a8%d9%88%da%a9-%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-d-%d8%b1%d8%a7%db%8c%d8%b2%d9%86-5/">متابوک هواوای دی رایزن 5</a>

                                                                                        </h2>
                                                                                    </div>

                                                                                    <div class="feature-daels-price">
												                                                    <span class="
                                remove-price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۷,۵۰۰,۰۰۰</bdi></span></span>
                                                                                        <span class="sale-price">
                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۷,۳۰۰,۰۰۰</bdi></span></span>                        												                                            </div>


                                                                                    <ul class="feature-attr-p">
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">ظرفیت حافظه داخلی: </span>
                                                                                            <span class="product-attr-text"><span>یک ترابایت </span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">ظرفیت حافظه رم: </span>
                                                                                            <span class="product-attr-text"><span>8 گیگابایت </span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">سری پردازنده: </span>
                                                                                            <span class="product-attr-text"><span>Core i5 </span></span>
                                                                                        </li>
                                                                                        <li class="product-attr">
                                                                                            <i class="far fa-check"></i>
                                                                                            <span class="product-attr-title">دقت صفحه نمایش: </span>
                                                                                            <span class="product-attr-text"><span> 1080 × 1920 </span></span>
                                                                                        </li>
                                                                                    </ul>

                                                                                    <div class="countdown-outer">
                                                                                        <div class="negarshop-countdown"
                                                                                             data-date="2023/05/13 19:29:59"></div>
                                                                                    </div>

                                                                                    <div class="product-quantity">
                                                                                        <div>
                                                                                            <span class="sale">فروش: 33</span>
                                                                                            <span class="in-stuck float-left">موجودی: نامحدود</span>
                                                                                        </div>
                                                                                        <div class="progress mb-2" style="height: 5px;">
                                                                                            <div class="progress-bar" role="progressbar"
                                                                                                 style="width: 0%;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0495f4f elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0495f4f" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    @foreach($top_banners as $banner)
                                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-f6e11c8" data-id="f6e11c8" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-b02b945 elementor-widget elementor-widget-image" data-id="b02b945" data-element_type="widget" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="{{$banner->url}}">
                                                                                    <img width="400" height="300" src="{{asset('images/banner/'.$banner->photo->path)}}" class="attachment-full size-full my-2" alt="" loading="lazy" srcset="{{asset('images/banner/'.$banner->photo->path)}} 400w, {{asset('images/banner/'.$banner->photo->path)}} 300w, {{asset('images/banner/'.$banner->photo->path)}} 50w" sizes="(max-width: 400px) 100vw, 400px" />
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-98a9b14 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="98a9b14" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-5bf5510" data-id="5bf5510" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-08ca580 elementor-widget elementor-widget-spacer" data-id="08ca580" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-879478c elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="879478c" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    @foreach($bot_banners as $banner)
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-672f8a6" data-id="672f8a6" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-cba11f2 elementor-widget elementor-widget-image" data-id="cba11f2" data-element_type="widget" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="{{$banner->url}}">
                                                                                    <img width="820" height="300" src="{{asset('images/banner/'.$banner->photo->path)}}" class="attachment-full my-2 size-full" alt="" loading="lazy" srcset="{{asset('images/banner/'.$banner->photo->path)}} 820w, {{asset('images/banner/'.$banner->photo->path)}} 600w, {{asset('images/banner/'.$banner->photo->path)}} 300w, {{asset('images/banner/'.$banner->photo->path)}} 768w, {{asset('images/banner/'.$banner->photo->path)}} 400w, {{asset('images/banner/'.$banner->photo->path)}} 50w" sizes="(max-width: 820px) 100vw, 820px" />
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0ddeed9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0ddeed9" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-218175f" data-id="218175f" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-cf624d8 elementor-widget elementor-widget-spacer" data-id="cf624d8" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-6ff671a elementor-widget elementor-widget-negarshop_carousel" data-id="6ff671a" data-element_type="widget" data-widget_type="negarshop_carousel.default">
                                                                    <div class="elementor-widget-container">
                                                                        <section class="content-widget products-carousel tabs-count-4 tabs banner-false style-2" id="content-widget-6ff671a">
                                                                            <header class="section-header" id="content-widget-header-6ff671a">
                                                                                <ul class="tabs">
                                                                                    @php($flag=1)
                                                                                    @foreach($top_4_cats as $cat)
                                                                                        @if($flag==1)
                                                                                            <li id="carousel-cat" class="active">
                                                                                                <a class="carousel-cat " href="#" id="{{$cat->id}}" data-query="" data-opts="">{{$cat->title}}</a>
                                                                                            </li>
                                                                                        @else
                                                                                            <li class="">
                                                                                                <a href="#" id="{{$cat->id}}" data-query="" data-opts="">{{$cat->title}}</a>
                                                                                            </li>
                                                                                        @endif
                                                                                        @php($flag=0)
                                                                                    @endforeach
                                                                                   {{-- <li class="">
                                                                                        <a
                                                                                                href="#6ff671a_3"
                                                                                                data-query="dVHbDoIwDP0V02ce5CIq32KyzDHJ4mCEDY2S_bvtRETFl24756ztaQdw_MgaXkso4NCv8yyjmOxC3FJM01UgduHg-_CIA5NBBG1nyl44y9ytpSSCO1mZ7janhOkbB0W8jkA5WU_alwC1WjVnROa_rDOCMK41wldj2JhbSYtwnoyo4xW-m17r6TNT5QRdlLxiRUUWn4iVWn8AKMeE-02ZpBsB8yZZq8RZdlAM72Zn93-ahWFMV5QtefERrqJ6sW9PiAczw683P5oj8tulf9ok6suu90ub8f9WE-pTi6dTvo05TafmlWRW3Uln7yxlMfgH"
                                                                                                data-opts="bYxBCoAwDAT_krMXr34mxDZCoFpJY4uIf7f1oqC3ndllDygFTSwwDDBRSAwdrCqusenWUGUc4_Jwsv2e9zWL8YwuhqjpddBswkw_0mRm_VjyHi2iI7Xakc-0OPZwXg">لباس و کفش</a>
                                                                                    </li>--}}
                                                                                </ul>
                                                                                <a href=""
                                                                                   class="btn archive-link">دیدن همه</a>
                                                                            </header>
                                                                            <div class="carousel-content">
                                                                                <div class="loading">
                                                                                    <div class="spinner"></div>
                                                                                </div>
                                                                                <div class="owl-carousel last" data-carousel="{&quot;nav&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:false}" data-items="{&quot;sm&quot;:2.5,&quot;md&quot;:2,&quot;lg&quot;:3,&quot;xl&quot;:3}"
                                                                                     id="product-carousel-6ff671a">

                                                                                </div>
                                                                            </div>
                                                                            <style>
                                                                                #content-widget-6ff671a article.w-p-item .info > .price {
                                                                                    font-size: 0;
                                                                                }

                                                                                #content-widget-6ff671a article.w-p-item .info > .price > span + span {
                                                                                    display: none;
                                                                                }

                                                                                #content-widget-6ff671a .info > .price del {
                                                                                    display: none;
                                                                                }

                                                                                #content-widget-6ff671a article.item .info > .price * {
                                                                                    font-size: 15px;
                                                                                    margin-top: 3px;
                                                                                }

                                                                                #content-widget-6ff671a article.item.product-type-variable .price {
                                                                                    font-size: 0;
                                                                                }

                                                                                #content-widget-6ff671a article.item.product-type-variable .price > .amount:last-of-type {
                                                                                    display: none;
                                                                                }

                                                                            </style>
                                                                            <script>
                                                                                jQuery(document).ready(function ($) {
                                                                                    $('#product-carousel-6ff671a').owlCarousel({
                                                                                        rtl: true,
                                                                                        nav: true,
                                                                                        dots: false,
                                                                                        loop: true,
                                                                                        navText: ["<i class='fal fa-angle-right'></i>", "<i class='fal fa-angle-left'></i>"],
                                                                                        responsive: {
                                                                                            0: {
                                                                                                items: 2.5,
                                                                                            },
                                                                                            480: {
                                                                                                items: 2,
                                                                                            },
                                                                                            700: {
                                                                                                items: 3,
                                                                                            },
                                                                                            991: {
                                                                                                items: 3,
                                                                                            },
                                                                                        },
                                                                                        autoplayHoverPause: true,
                                                                                        margin: 15
                                                                                    });

                                                                                });
                                                                            </script>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0057712 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0057712" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-bf6ae27" data-id="bf6ae27" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-927bd3d elementor-widget elementor-widget-spacer" data-id="927bd3d" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-9d2a646 elementor-widget elementor-widget-negarshop_carousel" data-id="9d2a646" data-element_type="widget" data-widget_type="negarshop_carousel.default">
                                                                    <div class="elementor-widget-container">
                                                                        @if(count($top_5_products)>0)
                                                                            <section class="content-widget products-carousel tabs-count-1 tabs banner-false style-2" id="content-widget-9d2a646">
                                                                            <header class="section-header" id="content-widget-header-9d2a646">
                                                                                <ul class="tabs">
                                                                                    <li class="active">
                                                                                        <a href="#9d2a646_0" data-query="dZLdboMwDIVfZfI1Fy1_3XiWSVEaUhQ1JIiEVS3Ku88OFBhrb0z4jo2PY0bw_MwMbyVU8D0cylTEeKEoNueUU8yOK8_Lj_goolxHOZ7zzxjzyE9TMSTQ9bYehHfM3zvqZuTNbbGwg_FQFQkwVaP-JbJzeSgwRXnZLmXPfORamSuS7Uect4IY1xrxzVomuJeN7ZV0UJlB64l63izvczF2XdCPQnPMK7qWiTip9R-wmmKdElfZQzWu5jbndzm7e5h93inlle-Q4Kqap7r6Rx6Nj__nCPMgJO4nCtNIJO1GC-HFUsK7LcT2tK065fhL0LZa3kjm1IPy3INl7AjhFw"
                                                                                                data-opts="bYxBCoAwDAT_krMXr34mxDZCoFpJY4uIf7f1oqC3ndllDygFTSwwDDBRSAwdrCqusenWUGUc4_Jwsv2e9zWL8YwuhqjpddBswkw_0mRm_VjyHi2iI7Xakc-0OPZwXg">جدیدترین محصولات</a>
                                                                                    </li>
                                                                                </ul>
                                                                                <a href=""
                                                                                   class="btn archive-link">دیدن همه</a>
                                                                            </header>
                                                                            <div class="carousel-content">
                                                                                <div class="loading">
                                                                                    <div class="spinner"></div>
                                                                                </div>
                                                                                <div class="owl-carousel" data-carousel="{&quot;nav&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:false}" data-items="{&quot;sm&quot;:2.5,&quot;md&quot;:2,&quot;lg&quot;:3,&quot;xl&quot;:4}"
                                                                                     id="product-carousel-9d2a646">
                                                                                    @foreach($top_5_products as $product)
                                                                                        <div id="car-item-9d2a646-0">
                                                                                            <article class="w-p-item product-type-simple product-item-style-1">
                                                                                                <a href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                                                                    <figure class="thumbnail">
                                                                                                        @if($product->photo)
                                                                                                        <img src="{{asset('images/products/'.$product->photo->path)}}" alt="{{$product->name}}">
                                                                                                        @else
                                                                                                            <img src="{{asset('img/product-placeholder.png')}}" alt="{{$product->name}}">
                                                                                                        @endif
                                                                                                        <div class="ribbons">
                                                                                                            <div>
                                                                                                                <span style="background-color: #999999" title="" class="item">
                                                                                                                    <i class="fa fa-check"></i>
                                                                                                                    <span> اورجینال </span>
                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </figure>
                                                                                                </a>
                                                                                                <div class="card-add-to-cart">
                                                                                                        <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">
                                                                                                            <a href="{{route('addToCart',$product->id)}}" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                                            افزودن به سبد خرید
                                                                                                            </a>
{{--                                                                                                           <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span> --}}
                                                                                                        </div>
                                                                                                    <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                                                </div>
                                                                                                <div class="info">
                                                                                                    <a href="{{route('singleProduct',[$product->slug,$product->id])}}">
                                                                                                        <h3 class="title ">{{$product->name}}</h3>
                                                                                                    </a>

                                                                                                    <div class="price">
                                                                                                        <span class="woocommerce-Price-amount amount">
                                                                                                            <bdi>
                                                                                                                <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;{{number_format($product->sell_price)}}</bdi>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </article>
                                                                                        </div>
                                                                                        @endforeach

                                                                                </div>
                                                                            </div>

                                                                            <style>
                                                                                #content-widget-9d2a646 article.w-p-item .info > .price {
                                                                                    font-size: 0;
                                                                                }

                                                                                #content-widget-9d2a646 article.w-p-item .info > .price > span + span {
                                                                                    display: none;
                                                                                }

                                                                                #content-widget-9d2a646 .info > .price del {
                                                                                    display: none;
                                                                                }

                                                                                #content-widget-9d2a646 article.item .info > .price * {
                                                                                    font-size: 15px;
                                                                                    margin-top: 3px;
                                                                                }

                                                                                #content-widget-9d2a646 article.item.product-type-variable .price {
                                                                                    font-size: 0;
                                                                                }

                                                                                #content-widget-9d2a646 article.item.product-type-variable .price > .amount:last-of-type {
                                                                                    display: none;
                                                                                }

                                                                            </style>
                                                                            <script>
                                                                                jQuery(document).ready(function ($) {
                                                                                    $('#product-carousel-9d2a646').owlCarousel({
                                                                                        rtl: true,
                                                                                        nav: true,
                                                                                        dots: false,
                                                                                        loop: true,
                                                                                        navText: ["<i class='fal fa-angle-right'></i>", "<i class='fal fa-angle-left'></i>"],
                                                                                        responsive: {
                                                                                            0: {
                                                                                                items: 2.5,
                                                                                            },
                                                                                            480: {
                                                                                                items: 2,
                                                                                            },
                                                                                            700: {
                                                                                                items: 3,
                                                                                            },
                                                                                            991: {
                                                                                                items: 4,
                                                                                            },
                                                                                        },
                                                                                        autoplayHoverPause: true,
                                                                                        margin: 15
                                                                                    });

                                                                                });
                                                                            </script>
                                                                        </section>
                                                                 @endif
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-af1c571 elementor-widget elementor-widget-spacer" data-id="af1c571" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-c87f729 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="c87f729" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-31c2a8b" data-id="31c2a8b" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-bb65a52 elementor-widget elementor-widget-spacer" data-id="bb65a52" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div class="elementor-element elementor-element-6a88854 elementor-view-stacked elementor-shape-square elementor-position-right elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="6a88854" data-element_type="widget" data-widget_type="icon-box.default">
                                          @if(count($articles)>0)
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-box-wrapper">
                                                    <div class="elementor-icon-box-icon">
			                                    	    <span class="elementor-icon elementor-animation-" >
                                                          <i aria-hidden="true" class="fas fa-star"></i>
                                                       </span>
                                                    </div>
                                                    <div class="elementor-icon-box-content">
                                                        <h6 class="elementor-icon-box-title">
                                                            <span >جدیدترین مقالات</span>
                                                        </h6>
                                                        <p class="elementor-icon-box-description">آخرین اخبار وبلاگ</p>
                                                    </div>
                                                </div>
                                            </div>
                                              @endif
                                        </div>
                                        <div class="elementor-element elementor-element-314d273 elementor-widget elementor-widget-negarshop_blog_carousel" data-id="314d273" data-element_type="widget" data-widget_type="negarshop_blog_carousel.default">
                                            <div class="elementor-widget-container">
                                                {{-- ATRICLES--}}
                                                <section class="content-widget blog-posts style-1"
                                                         id="content-widget-314d273">
                                                    <div class="owl-carousel blog-posts" id="blog-posts-314d273">
                                                        @foreach($articles as $article)
                                                            <div>
                                                                <article class="blog-item">
                                                                    <a href="{{route('article',$article->id)}}">
                                                                        <figure class="thumbnail">
                                                                            @if($article->photo)
                                                                            <img class="lazy" src="{{asset('images/articles/'.$article->photo->path)}}" data-src="{{asset('images/articles/'.$article->photo->path)}}" alt="{{$article->title}}">
                                                                            @else
                                                                                <img class="lazy" src="{{asset('img/default.jpg')}}" alt="{{$article->title}}" data-src="{{asset('img/default.jpg')}}" alt="{{$article->title}}">
                                                                            @endif
                                                                            <div class="post-format">
                                                                                <i class="fal fa-file-alt"></i>
                                                                            </div>
                                                                            <time>
                                                                                <span class="day">{{verta($article->created_at)->day}}</span>
                                                                                <span class="month">{{verta($article->created_at)->format('%B')}}</span>
                                                                            </time>
                                                                        </figure>
                                                                    </a>
                                                                    <div class="item-footer">
                                                                        <a href="{{route('article',$article->id)}}">
                                                                            <h3 class="title my-2">{{$article->title}}</h3>
                                                                        </a>
                                                                        <div class="cats">
                                                                            <a href="#" rel="category tag">{{$article->category->title}}</a></div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                        @endforeach

                                                    </div>
                                                    <script>
                                                        jQuery(document).ready(function ($) {
                                                            $('#blog-posts-314d273').owlCarousel({
                                                                items: 3,
                                                                autoplay: true,
                                                                rtl: true,
                                                                nav: false,
                                                                loop: true,
                                                                dots: true,
                                                                responsive: {
                                                                    0: {
                                                                        items: 1,
                                                                    },
                                                                    480: {
                                                                        items: 2,
                                                                    },
                                                                    700: {
                                                                        items: 2,
                                                                    },
                                                                    991: {
                                                                        items: 3,
                                                                    },
                                                                },
                                                                margin: 15
                                                            });
                                                        });
                                                    </script>
                                                </section>
                                                {{--eND ATRICLES--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="container">
        <ul class="fixed-bottom-bar style-2">
            <li><a class="btn wooscp-btn" href="{{route('ShoppingCart')}}" data-toggle="tooltip" data-placement="top" title="سبد خرید"><i class="fal fa-shopping-cart"></i></a></li>
            <li><a class="btn" href="{{route('Profile.index')}}" data-toggle="tooltip" data-placement="top" title="علاقه مندی ها"><i class="far fa-heart"></i></a></li>
            <li><a class="btn" href="javascript:void(0);" id="negarshop-to-top"><span><i class="far fa-angle-up"></i></span></a></li>
        </ul>
    </div>

    @include('front.newTheme.footer')

</div>
<script>
    (function() {function maybePrefixUrlField() {
        if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
            this.value = "http://" + this.value;
        }
    }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields) {
            for (var j=0; j < urlFields.length; j++) {
                urlFields[j].addEventListener('blur', maybePrefixUrlField);
            }
        }
    })();
</script>
<script>
    var loadJS = function(url, implementationCode, location){
        //url is URL of external file, implementationCode is the code
        //to be called from the file, location is the location to
        //insert the <script> element

        var scriptTag = document.createElement('script');
        scriptTag.src = url;

        scriptTag.onload = implementationCode;
        scriptTag.onreadystatechange = implementationCode;

        location.appendChild(scriptTag);
    };
    var loadLazyloadPlugin = function(){
        var myLazyLoad = new LazyLoad({
            elements_selector: ".lazy"
        });
        jQuery(document).bind("ajaxComplete", function($){
            myLazyLoad.update();
        });
    }
    loadJS("{{asset('newFront/js/lazyload.min.js')}}", loadLazyloadPlugin, document.body);
    var defaultText = {
        searchArchive:"نتیجه مورد نظر را پیدا نکردید؟",searchAllBtn:"مشاهده همه",searchNotFound:"چیزی پیدا نشد!",errorSend:"مشکلی هنگام ارسال پیش آمد!",bad:"بد",medium:"متوسط",good:"خوب",excelent:"عالی",verybad:"خیلی بد",pleaseWait:"لطفا صبر کنید ...",
    };
</script>
<script src="{{asset('newFront/js/uikit.min.js')}}"></script>
<script src="{{asset('newFront/js/uikit-icons.min.js')}}"></script>
<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<script type='text/javascript' src='{{asset('newFront/js/core.min.js')}}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/datepicker.min.js')}}' id='jquery-ui-datepicker-js'></script>
<script type='text/javascript' id='jquery-ui-datepicker-js-after'>
    jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"\u0628\u0633\u062a\u0646","currentText":"\u0627\u0645\u0631\u0648\u0632","monthNames":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u06cc","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"monthNamesShort":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u0647","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"nextText":"\u0628\u0639\u062f","prevText":"\u0642\u0628\u0644\u06cc","dayNames":["\u06cc\u06a9\u0634\u0646\u0628\u0647","\u062f\u0648\u0634\u0646\u0628\u0647","\u0633\u0647\u200c\u0634\u0646\u0628\u0647","\u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647","\u067e\u0646\u062c\u200c\u0634\u0646\u0628\u0647","\u062c\u0645\u0639\u0647","\u0634\u0646\u0628\u0647"],"dayNamesShort":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dayNamesMin":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dateFormat":"MM d, yy","firstDay":6,"isRTL":true});});
</script>
<script type='text/javascript' src='{{asset('newFront/js/selectWoo.full.min.js')}}' id='selectWoo-js'></script>
<script type='text/javascript' id='wc-country-select-js-extra'>
    /* <![CDATA[ */
    var wc_country_select_params = {"countries":"{\"AF\":[],\"AO\":{\"BGO\":\"Bengo\",\"BLU\":\"Benguela\",\"BIE\":\"Bi\\u00e9\",\"CAB\":\"Cabinda\",\"CNN\":\"Cunene\",\"HUA\":\"Huambo\",\"HUI\":\"Hu\\u00edla\",\"CCU\":\"Kuando Kubango\",\"CNO\":\"Kwanza-Norte\",\"CUS\":\"Kwanza-Sul\",\"LUA\":\"Luanda\",\"LNO\":\"Lunda-Norte\",\"LSU\":\"Lunda-Sul\",\"MAL\":\"Malanje\",\"MOX\":\"Moxico\",\"NAM\":\"Namibe\",\"UIG\":\"U\\u00edge\",\"ZAI\":\"Zaire\"},\"AR\":{\"C\":\"Ciudad Aut\\u00f3noma de Buenos Aires\",\"B\":\"Buenos Aires\",\"K\":\"Catamarca\",\"H\":\"Chaco\",\"U\":\"Chubut\",\"X\":\"C\\u00f3rdoba\",\"W\":\"Corrientes\",\"E\":\"Entre R\\u00edos\",\"P\":\"Formosa\",\"Y\":\"Jujuy\",\"L\":\"La Pampa\",\"F\":\"La Rioja\",\"M\":\"Mendoza\",\"N\":\"Misiones\",\"Q\":\"Neuqu\\u00e9n\",\"R\":\"R\\u00edo Negro\",\"A\":\"Salta\",\"J\":\"San Juan\",\"D\":\"San Luis\",\"Z\":\"Santa Cruz\",\"S\":\"Santa Fe\",\"G\":\"Santiago del Estero\",\"V\":\"Tierra del Fuego\",\"T\":\"Tucum\\u00e1n\"},\"AT\":[],\"AU\":{\"ACT\":\"Australian Capital Territory\",\"NSW\":\"New South Wales\",\"NT\":\"Northern Territory\",\"QLD\":\"Queensland\",\"SA\":\"South Australia\",\"TAS\":\"Tasmania\",\"VIC\":\"Victoria\",\"WA\":\"Western Australia\"},\"AX\":[],\"BD\":{\"BD-05\":\"Bagerhat\",\"BD-01\":\"Bandarban\",\"BD-02\":\"Barguna\",\"BD-06\":\"Barishal\",\"BD-07\":\"Bhola\",\"BD-03\":\"Bogura\",\"BD-04\":\"Brahmanbaria\",\"BD-09\":\"Chandpur\",\"BD-10\":\"Chattogram\",\"BD-12\":\"Chuadanga\",\"BD-11\":\"Cox's Bazar\",\"BD-08\":\"Cumilla\",\"BD-13\":\"Dhaka\",\"BD-14\":\"Dinajpur\",\"BD-15\":\"Faridpur \",\"BD-16\":\"Feni\",\"BD-19\":\"Gaibandha\",\"BD-18\":\"Gazipur\",\"BD-17\":\"Gopalganj\",\"BD-20\":\"Habiganj\",\"BD-21\":\"Jamalpur\",\"BD-22\":\"Jashore\",\"BD-25\":\"Jhalokati\",\"BD-23\":\"Jhenaidah\",\"BD-24\":\"Joypurhat\",\"BD-29\":\"Khagrachhari\",\"BD-27\":\"Khulna\",\"BD-26\":\"Kishoreganj\",\"BD-28\":\"Kurigram\",\"BD-30\":\"Kushtia\",\"BD-31\":\"Lakshmipur\",\"BD-32\":\"Lalmonirhat\",\"BD-36\":\"Madaripur\",\"BD-37\":\"Magura\",\"BD-33\":\"Manikganj \",\"BD-39\":\"Meherpur\",\"BD-38\":\"Moulvibazar\",\"BD-35\":\"Munshiganj\",\"BD-34\":\"Mymensingh\",\"BD-48\":\"Naogaon\",\"BD-43\":\"Narail\",\"BD-40\":\"Narayanganj\",\"BD-42\":\"Narsingdi\",\"BD-44\":\"Natore\",\"BD-45\":\"Nawabganj\",\"BD-41\":\"Netrakona\",\"BD-46\":\"Nilphamari\",\"BD-47\":\"Noakhali\",\"BD-49\":\"Pabna\",\"BD-52\":\"Panchagarh\",\"BD-51\":\"Patuakhali\",\"BD-50\":\"Pirojpur\",\"BD-53\":\"Rajbari\",\"BD-54\":\"Rajshahi\",\"BD-56\":\"Rangamati\",\"BD-55\":\"Rangpur\",\"BD-58\":\"Satkhira\",\"BD-62\":\"Shariatpur\",\"BD-57\":\"Sherpur\",\"BD-59\":\"Sirajganj\",\"BD-61\":\"Sunamganj\",\"BD-60\":\"Sylhet\",\"BD-63\":\"Tangail\",\"BD-64\":\"Thakurgaon\"},\"BE\":[],\"BG\":{\"BG-01\":\"Blagoevgrad\",\"BG-02\":\"Burgas\",\"BG-08\":\"Dobrich\",\"BG-07\":\"Gabrovo\",\"BG-26\":\"Haskovo\",\"BG-09\":\"Kardzhali\",\"BG-10\":\"Kyustendil\",\"BG-11\":\"Lovech\",\"BG-12\":\"Montana\",\"BG-13\":\"Pazardzhik\",\"BG-14\":\"Pernik\",\"BG-15\":\"Pleven\",\"BG-16\":\"Plovdiv\",\"BG-17\":\"Razgrad\",\"BG-18\":\"Ruse\",\"BG-27\":\"Shumen\",\"BG-19\":\"Silistra\",\"BG-20\":\"Sliven\",\"BG-21\":\"Smolyan\",\"BG-23\":\"Sofia\",\"BG-22\":\"Sofia-Grad\",\"BG-24\":\"Stara Zagora\",\"BG-25\":\"Targovishte\",\"BG-03\":\"Varna\",\"BG-04\":\"Veliko Tarnovo\",\"BG-05\":\"Vidin\",\"BG-06\":\"Vratsa\",\"BG-28\":\"Yambol\"},\"BH\":[],\"BI\":[],\"BJ\":{\"AL\":\"Alibori\",\"AK\":\"Atakora\",\"AQ\":\"Atlantique\",\"BO\":\"Borgou\",\"CO\":\"Collines\",\"KO\":\"Kouffo\",\"DO\":\"Donga\",\"LI\":\"Littoral\",\"MO\":\"Mono\",\"OU\":\"Ou\\u00e9m\\u00e9\",\"PL\":\"Plateau\",\"ZO\":\"Zou\"},\"BO\":{\"B\":\"\\u0686\\u0648\\u06a9\\u06cc\\u0633\\u0627\\u06a9\\u0627\",\"H\":\"\\u0628\\u0646\\u06cc\",\"C\":\"\\u06a9\\u0686\\u0627\\u0628\\u0627\\u0645\\u0628\\u0627\",\"L\":\"\\u0644\\u0627\\u067e\\u0627\\u0632\",\"O\":\"\\u0627\\u0648\\u0631\\u0648\\u0631\\u0648\",\"N\":\"\\u067e\\u0627\\u0646\\u062f\\u0648\",\"P\":\"\\u067e\\u0648\\u062a\\u0648\\u0633\\u06cc\",\"S\":\"Santa Cruz\",\"T\":\"\\u062a\\u0631\\u06cc\\u062c\\u0627\"},\"BR\":{\"AC\":\"Acre\",\"AL\":\"Alagoas\",\"AP\":\"Amap\\u00e1\",\"AM\":\"Amazonas\",\"BA\":\"Bahia\",\"CE\":\"Cear\\u00e1\",\"DF\":\"Distrito Federal\",\"ES\":\"Esp\\u00edrito Santo\",\"GO\":\"Goi\\u00e1s\",\"MA\":\"Maranh\\u00e3o\",\"MT\":\"Mato Grosso\",\"MS\":\"Mato Grosso do Sul\",\"MG\":\"Minas Gerais\",\"PA\":\"Par\\u00e1\",\"PB\":\"Para\\u00edba\",\"PR\":\"Paran\\u00e1\",\"PE\":\"Pernambuco\",\"PI\":\"Piau\\u00ed\",\"RJ\":\"Rio de Janeiro\",\"RN\":\"Rio Grande do Norte\",\"RS\":\"Rio Grande do Sul\",\"RO\":\"Rond\\u00f4nia\",\"RR\":\"Roraima\",\"SC\":\"Santa Catarina\",\"SP\":\"S\\u00e3o Paulo\",\"SE\":\"Sergipe\",\"TO\":\"Tocantins\"},\"CA\":{\"AB\":\"Alberta\",\"BC\":\"British Columbia\",\"MB\":\"Manitoba\",\"NB\":\"New Brunswick\",\"NL\":\"Newfoundland and Labrador\",\"NT\":\"Northwest Territories\",\"NS\":\"Nova Scotia\",\"NU\":\"Nunavut\",\"ON\":\"Ontario\",\"PE\":\"Prince Edward Island\",\"QC\":\"Quebec\",\"SK\":\"Saskatchewan\",\"YT\":\"Yukon Territory\"},\"CH\":{\"AG\":\"Aargau\",\"AR\":\"Appenzell Ausserrhoden\",\"AI\":\"Appenzell Innerrhoden\",\"BL\":\"Basel-Landschaft\",\"BS\":\"Basel-Stadt\",\"BE\":\"Bern\",\"FR\":\"Fribourg\",\"GE\":\"Geneva\",\"GL\":\"Glarus\",\"GR\":\"Graub\\u00fcnden\",\"JU\":\"Jura\",\"LU\":\"Luzern\",\"NE\":\"Neuch\\u00e2tel\",\"NW\":\"Nidwalden\",\"OW\":\"Obwalden\",\"SH\":\"Schaffhausen\",\"SZ\":\"Schwyz\",\"SO\":\"Solothurn\",\"SG\":\"St. Gallen\",\"TG\":\"Thurgau\",\"TI\":\"Ticino\",\"UR\":\"Uri\",\"VS\":\"Valais\",\"VD\":\"Vaud\",\"ZG\":\"Zug\",\"ZH\":\"Z\\u00fcrich\"},\"CN\":{\"CN1\":\"Yunnan \\\/ \\u4e91\\u5357\",\"CN2\":\"Beijing \\\/ \\u5317\\u4eac\",\"CN3\":\"Tianjin \\\/ \\u5929\\u6d25\",\"CN4\":\"Hebei \\\/ \\u6cb3\\u5317\",\"CN5\":\"Shanxi \\\/ \\u5c71\\u897f\",\"CN6\":\"Inner Mongolia \\\/ \\u5167\\u8499\\u53e4\",\"CN7\":\"Liaoning \\\/ \\u8fbd\\u5b81\",\"CN8\":\"Jilin \\\/ \\u5409\\u6797\",\"CN9\":\"Heilongjiang \\\/ \\u9ed1\\u9f99\\u6c5f\",\"CN10\":\"Shanghai \\\/ \\u4e0a\\u6d77\",\"CN11\":\"Jiangsu \\\/ \\u6c5f\\u82cf\",\"CN12\":\"Zhejiang \\\/ \\u6d59\\u6c5f\",\"CN13\":\"Anhui \\\/ \\u5b89\\u5fbd\",\"CN14\":\"Fujian \\\/ \\u798f\\u5efa\",\"CN15\":\"Jiangxi \\\/ \\u6c5f\\u897f\",\"CN16\":\"Shandong \\\/ \\u5c71\\u4e1c\",\"CN17\":\"Henan \\\/ \\u6cb3\\u5357\",\"CN18\":\"Hubei \\\/ \\u6e56\\u5317\",\"CN19\":\"Hunan \\\/ \\u6e56\\u5357\",\"CN20\":\"Guangdong \\\/ \\u5e7f\\u4e1c\",\"CN21\":\"Guangxi Zhuang \\\/ \\u5e7f\\u897f\\u58ee\\u65cf\",\"CN22\":\"Hainan \\\/ \\u6d77\\u5357\",\"CN23\":\"Chongqing \\\/ \\u91cd\\u5e86\",\"CN24\":\"Sichuan \\\/ \\u56db\\u5ddd\",\"CN25\":\"Guizhou \\\/ \\u8d35\\u5dde\",\"CN26\":\"Shaanxi \\\/ \\u9655\\u897f\",\"CN27\":\"Gansu \\\/ \\u7518\\u8083\",\"CN28\":\"Qinghai \\\/ \\u9752\\u6d77\",\"CN29\":\"Ningxia Hui \\\/ \\u5b81\\u590f\",\"CN30\":\"Macao \\\/ \\u6fb3\\u95e8\",\"CN31\":\"Tibet \\\/ \\u897f\\u85cf\",\"CN32\":\"Xinjiang \\\/ \\u65b0\\u7586\"},\"CZ\":[],\"DE\":[],\"DK\":[],\"DZ\":{\"DZ-01\":\"\\u0622\\u062f\\u0631\\u0627\\u0631\",\"DZ-02\":\"\\u0686\\u0644\\u0641\",\"DZ-03\":\"\\u0644\\u0627\\u063a\\u0648\\u0627\\u062a\",\"DZ-04\":\"\\u0627\\u0648\\u0645 \\u0627\\u0644\\u0628\\u0648\\u0627\\u063a\\u06cc\",\"DZ-05\":\"\\u0628\\u0627\\u062a\\u0646\\u0627\",\"DZ-06\":\"B\\u00e9ja\\u00efa\",\"DZ-07\":\"\\u0628\\u06cc\\u0633\\u06a9\\u0631\\u0627\",\"DZ-08\":\"B\\u00e9char\",\"DZ-09\":\"\\u0628\\u0644\\u06cc\\u062f\\u0627\",\"DZ-10\":\"\\u0628\\u0648\\u06cc\\u0631\\u0627\",\"DZ-11\":\"\\u062a\\u0627\\u0645\\u0627\\u0646\\u06af\\u0647\\u0627\\u0633\\u062a\",\"DZ-12\":\"T\\u00e9bessa\",\"DZ-13\":\"\\u062a\\u0644\\u0645\\u0633\\u0646\",\"DZ-14\":\"\\u062a\\u06cc\\u0627\\u0631\\u062a\",\"DZ-15\":\"\\u062a\\u064a\\u0632\\u064a \\u0627\\u0648\\u0632\\u0648\",\"DZ-16\":\"\\u0627\\u0644\\u062c\\u0632\\u0627\\u06cc\\u0631\",\"DZ-17\":\"\\u062c\\u0644\\u0641\\u0627\",\"DZ-18\":\"\\u062c\\u06cc\\u0698\\u0644\",\"DZ-19\":\"S\\u00e9tif\",\"DZ-20\":\"Sa\\u00efda\",\"DZ-21\":\"Skikda\",\"DZ-22\":\"Sidi Bel Abb\\u00e8s\",\"DZ-23\":\"Annaba\",\"DZ-24\":\"Guelma\",\"DZ-25\":\"Constantine\",\"DZ-26\":\"M\\u00e9d\\u00e9a\",\"DZ-27\":\"Mostaganem\",\"DZ-28\":\"M\\u2019Sila\",\"DZ-29\":\"\\u0645\\u0627\\u0633\\u06a9\\u0627\\u0631\\u0627\",\"DZ-30\":\"\\u0627\\u0648\\u0627\\u0631\\u06af\\u0644\\u0627\",\"DZ-31\":\"Oran\",\"DZ-32\":\"El Bayadh\",\"DZ-33\":\"\\u0627\\u06cc\\u0644\\u06cc\\u0632\\u06cc\",\"DZ-34\":\"Bordj Bou Arr\\u00e9ridj\",\"DZ-35\":\"Boumerd\\u00e8s\",\"DZ-36\":\"\\u0627\\u0644 \\u062a\\u0631\\u0641\",\"DZ-37\":\"\\u062a\\u06cc\\u0646\\u062f\\u0648\\u0641\",\"DZ-38\":\"Tissemsilt\",\"DZ-39\":\"\\u0627\\u0644 \\u0627\\u0648\\u0648\\u062f\",\"DZ-40\":\"\\u06a9\\u0646\\u0686\\u0644\\u0627\",\"DZ-41\":\"Souk Ahras\",\"DZ-42\":\"\\u062a\\u06cc\\u067e\\u0627\\u0633\\u0627\",\"DZ-43\":\"Mila\",\"DZ-44\":\"A\\u00efn Defla\",\"DZ-45\":\"Naama\",\"DZ-46\":\"A\\u00efn T\\u00e9mouchent\",\"DZ-47\":\"Gharda\\u00efa\",\"DZ-48\":\"Relizane\"},\"EE\":[],\"EG\":{\"EGALX\":\"Alexandria\",\"EGASN\":\"Aswan\",\"EGAST\":\"Asyut\",\"EGBA\":\"Red Sea\",\"EGBH\":\"Beheira\",\"EGBNS\":\"Beni Suef\",\"EGC\":\"Cairo\",\"EGDK\":\"Dakahlia\",\"EGDT\":\"Damietta\",\"EGFYM\":\"Faiyum\",\"EGGH\":\"Gharbia\",\"EGGZ\":\"Giza\",\"EGIS\":\"Ismailia\",\"EGJS\":\"South Sinai\",\"EGKB\":\"Qalyubia\",\"EGKFS\":\"Kafr el-Sheikh\",\"EGKN\":\"Qena\",\"EGLX\":\"Luxor\",\"EGMN\":\"Minya\",\"EGMNF\":\"Monufia\",\"EGMT\":\"Matrouh\",\"EGPTS\":\"Port Said\",\"EGSHG\":\"Sohag\",\"EGSHR\":\"Al Sharqia\",\"EGSIN\":\"North Sinai\",\"EGSUZ\":\"Suez\",\"EGWAD\":\"New Valley\"},\"ES\":{\"C\":\"A Coru\\u00f1a\",\"VI\":\"Araba\\\/\\u00c1lava\",\"AB\":\"Albacete\",\"A\":\"Alicante\",\"AL\":\"Almer\\u00eda\",\"O\":\"Asturias\",\"AV\":\"\\u00c1vila\",\"BA\":\"Badajoz\",\"PM\":\"Baleares\",\"B\":\"Barcelona\",\"BU\":\"Burgos\",\"CC\":\"C\\u00e1ceres\",\"CA\":\"C\\u00e1diz\",\"S\":\"Cantabria\",\"CS\":\"Castell\\u00f3n\",\"CE\":\"Ceuta\",\"CR\":\"Ciudad Real\",\"CO\":\"C\\u00f3rdoba\",\"CU\":\"Cuenca\",\"GI\":\"Girona\",\"GR\":\"Granada\",\"GU\":\"Guadalajara\",\"SS\":\"Gipuzkoa\",\"H\":\"Huelva\",\"HU\":\"Huesca\",\"J\":\"Ja\\u00e9n\",\"LO\":\"La Rioja\",\"GC\":\"Las Palmas\",\"LE\":\"Le\\u00f3n\",\"L\":\"Lleida\",\"LU\":\"Lugo\",\"M\":\"Madrid\",\"MA\":\"M\\u00e1laga\",\"ML\":\"Melilla\",\"MU\":\"Murcia\",\"NA\":\"Navarra\",\"OR\":\"Ourense\",\"P\":\"Palencia\",\"PO\":\"Pontevedra\",\"SA\":\"Salamanca\",\"TF\":\"Santa Cruz de Tenerife\",\"SG\":\"Segovia\",\"SE\":\"Sevilla\",\"SO\":\"Soria\",\"T\":\"Tarragona\",\"TE\":\"Teruel\",\"TO\":\"Toledo\",\"V\":\"Valencia\",\"VA\":\"Valladolid\",\"BI\":\"Biscay\",\"ZA\":\"Zamora\",\"Z\":\"Zaragoza\"},\"FI\":[],\"FR\":[],\"GH\":{\"AF\":\"Ahafo\",\"AH\":\"Ashanti\",\"BA\":\"Brong-Ahafo\",\"BO\":\"Bono\",\"BE\":\"Bono East\",\"CP\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"EP\":\"\\u0634\\u0631\\u0642\\u06cc\",\"AA\":\"Greater Accra\",\"NE\":\"North East\",\"NP\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"OT\":\"Oti\",\"SV\":\"Savannah\",\"UE\":\"\\u062e\\u0627\\u0648\\u0631 \\u0634\\u0631\\u0642\\u06cc\",\"UW\":\"\\u062e\\u0627\\u0648\\u0631\\u0645\\u06cc\\u0627\\u0646\\u0647\",\"TV\":\"Volta\",\"WP\":\"\\u063a\\u0631\\u0628\\u06cc\",\"WN\":\"Western North\"},\"GP\":[],\"GR\":{\"I\":\"Attica\",\"A\":\"East Macedonia and Thrace\",\"B\":\"Central Macedonia\",\"C\":\"\\u0645\\u0642\\u062f\\u0648\\u0646\\u06cc\\u0647 \\u063a\\u0631\\u0628\\u06cc\",\"D\":\"\\u0627\\u06cc\\u067e\\u06cc\\u0631\\u0648\\u0633\",\"E\":\"\\u062a\\u0633\\u0627\\u0644\\u0627\\u0644\\u06cc\",\"F\":\"Ionian Islands\",\"G\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"H\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"J\":\"\\u067e\\u0644\\u0648\\u067e\\u0648\\u0646\\u0632\",\"K\":\"North Aegean\",\"L\":\"South Aegean\",\"M\":\"\\u06a9\\u0631\\u062a\"},\"GF\":[],\"HK\":{\"HONG KONG\":\"Hong Kong Island\",\"KOWLOON\":\"Kowloon\",\"NEW TERRITORIES\":\"New Territories\"},\"HU\":{\"BK\":\"B\\u00e1cs-Kiskun\",\"BE\":\"B\\u00e9k\\u00e9s\",\"BA\":\"Baranya\",\"BZ\":\"Borsod-Aba\\u00faj-Zempl\\u00e9n\",\"BU\":\"Budapest\",\"CS\":\"Csongr\\u00e1d-Csan\\u00e1d\",\"FE\":\"Fej\\u00e9r\",\"GS\":\"Gy\\u0151r-Moson-Sopron\",\"HB\":\"Hajd\\u00fa-Bihar\",\"HE\":\"Heves\",\"JN\":\"J\\u00e1sz-Nagykun-Szolnok\",\"KE\":\"Kom\\u00e1rom-Esztergom\",\"NO\":\"N\\u00f3gr\\u00e1d\",\"PE\":\"Pest\",\"SO\":\"Somogy\",\"SZ\":\"Szabolcs-Szatm\\u00e1r-Bereg\",\"TO\":\"Tolna\",\"VA\":\"Vas\",\"VE\":\"Veszpr\\u00e9m\",\"ZA\":\"Zala\"},\"ID\":{\"AC\":\"Daerah Istimewa Aceh\",\"SU\":\"Sumatera Utara\",\"SB\":\"Sumatera Barat\",\"RI\":\"Riau\",\"KR\":\"Kepulauan Riau\",\"JA\":\"Jambi\",\"SS\":\"Sumatera Selatan\",\"BB\":\"Bangka Belitung\",\"BE\":\"Bengkulu\",\"LA\":\"Lampung\",\"JK\":\"DKI Jakarta\",\"JB\":\"Jawa Barat\",\"BT\":\"Banten\",\"JT\":\"Jawa Tengah\",\"JI\":\"Jawa Timur\",\"YO\":\"Daerah Istimewa Yogyakarta\",\"BA\":\"Bali\",\"NB\":\"Nusa Tenggara Barat\",\"NT\":\"Nusa Tenggara Timur\",\"KB\":\"Kalimantan Barat\",\"KT\":\"Kalimantan Tengah\",\"KI\":\"Kalimantan Timur\",\"KS\":\"Kalimantan Selatan\",\"KU\":\"Kalimantan Utara\",\"SA\":\"Sulawesi Utara\",\"ST\":\"Sulawesi Tengah\",\"SG\":\"Sulawesi Tenggara\",\"SR\":\"Sulawesi Barat\",\"SN\":\"Sulawesi Selatan\",\"GO\":\"Gorontalo\",\"MA\":\"Maluku\",\"MU\":\"Maluku Utara\",\"PA\":\"Papua\",\"PB\":\"Papua Barat\"},\"IE\":{\"CW\":\"Carlow\",\"CN\":\"Cavan\",\"CE\":\"Clare\",\"CO\":\"Cork\",\"DL\":\"Donegal\",\"D\":\"Dublin\",\"G\":\"Galway\",\"KY\":\"Kerry\",\"KE\":\"Kildare\",\"KK\":\"Kilkenny\",\"LS\":\"Laois\",\"LM\":\"Leitrim\",\"LK\":\"Limerick\",\"LD\":\"Longford\",\"LH\":\"Louth\",\"MO\":\"Mayo\",\"MH\":\"Meath\",\"MN\":\"Monaghan\",\"OY\":\"Offaly\",\"RN\":\"Roscommon\",\"SO\":\"Sligo\",\"TA\":\"Tipperary\",\"WD\":\"Waterford\",\"WH\":\"Westmeath\",\"WX\":\"Wexford\",\"WW\":\"Wicklow\"},\"IN\":{\"AP\":\"Andhra Pradesh\",\"AR\":\"Arunachal Pradesh\",\"AS\":\"Assam\",\"BR\":\"Bihar\",\"CT\":\"Chhattisgarh\",\"GA\":\"Goa\",\"GJ\":\"Gujarat\",\"HR\":\"Haryana\",\"HP\":\"Himachal Pradesh\",\"JK\":\"Jammu and Kashmir\",\"JH\":\"Jharkhand\",\"KA\":\"Karnataka\",\"KL\":\"Kerala\",\"MP\":\"Madhya Pradesh\",\"MH\":\"Maharashtra\",\"MN\":\"Manipur\",\"ML\":\"Meghalaya\",\"MZ\":\"Mizoram\",\"NL\":\"Nagaland\",\"OR\":\"Orissa\",\"PB\":\"Punjab\",\"RJ\":\"Rajasthan\",\"SK\":\"Sikkim\",\"TN\":\"Tamil Nadu\",\"TS\":\"Telangana\",\"TR\":\"Tripura\",\"UK\":\"Uttarakhand\",\"UP\":\"Uttar Pradesh\",\"WB\":\"West Bengal\",\"AN\":\"Andaman and Nicobar Islands\",\"CH\":\"Chandigarh\",\"DN\":\"Dadra and Nagar Haveli\",\"DD\":\"Daman and Diu\",\"DL\":\"Delhi\",\"LD\":\"Lakshadeep\",\"PY\":\"Pondicherry (Puducherry)\"},\"IR\":{\"ABZ\":\"\\u0627\\u0644\\u0628\\u0631\\u0632\",\"ADL\":\"\\u0627\\u0631\\u062f\\u0628\\u06cc\\u0644\",\"EAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u0634\\u0631\\u0642\\u06cc\",\"WAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"BHR\":\"\\u0628\\u0648\\u0634\\u0647\\u0631\",\"CHB\":\"\\u0686\\u0647\\u0627\\u0631\\u0645\\u062d\\u0627\\u0644 \\u0648 \\u0628\\u062e\\u062a\\u06cc\\u0627\\u0631\\u06cc\",\"FRS\":\"\\u0641\\u0627\\u0631\\u0633\",\"GIL\":\"\\u06af\\u06cc\\u0644\\u0627\\u0646\",\"GLS\":\"\\u06af\\u0644\\u0633\\u062a\\u0627\\u0646\",\"HDN\":\"\\u0647\\u0645\\u062f\\u0627\\u0646\",\"HRZ\":\"\\u0647\\u0631\\u0645\\u0632\\u06af\\u0627\\u0646\",\"ILM\":\"\\u0627\\u06cc\\u0644\\u0627\\u0645\",\"ESF\":\"\\u0627\\u0635\\u0641\\u0647\\u0627\\u0646\",\"KRN\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\",\"KRH\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\\u0634\\u0627\\u0647\",\"NKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0634\\u0645\\u0627\\u0644\\u06cc\",\"RKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0631\\u0636\\u0648\\u06cc\",\"SKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u062c\\u0646\\u0648\\u0628\\u06cc\",\"KHZ\":\"\\u062e\\u0648\\u0632\\u0633\\u062a\\u0627\\u0646\",\"KBD\":\"\\u06a9\\u0647\\u06af\\u06cc\\u0644\\u0648\\u06cc\\u0647 \\u0648 \\u0628\\u0648\\u06cc\\u0631\\u0627\\u062d\\u0645\\u062f\",\"KRD\":\"\\u06a9\\u0631\\u062f\\u0633\\u062a\\u0627\\u0646\",\"LRS\":\"\\u0644\\u0631\\u0633\\u062a\\u0627\\u0646\",\"MKZ\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"MZN\":\"\\u0645\\u0627\\u0632\\u0646\\u062f\\u0631\\u0627\\u0646\",\"GZN\":\"\\u0642\\u0632\\u0648\\u06cc\\u0646\",\"QHM\":\"\\u0642\\u0645\",\"SMN\":\"\\u0633\\u0645\\u0646\\u0627\\u0646\",\"SBN\":\"\\u0633\\u06cc\\u0633\\u062a\\u0627\\u0646 \\u0648 \\u0628\\u0644\\u0648\\u0686\\u0633\\u062a\\u0627\\u0646\",\"THR\":\"\\u062a\\u0647\\u0631\\u0627\\u0646\",\"YZD\":\"\\u06cc\\u0632\\u062f\",\"ZJN\":\"\\u0632\\u0646\\u062c\\u0627\\u0646\"},\"IS\":[],\"IT\":{\"AG\":\"Agrigento\",\"AL\":\"Alessandria\",\"AN\":\"Ancona\",\"AO\":\"Aosta\",\"AR\":\"Arezzo\",\"AP\":\"Ascoli Piceno\",\"AT\":\"Asti\",\"AV\":\"Avellino\",\"BA\":\"Bari\",\"BT\":\"Barletta-Andria-Trani\",\"BL\":\"Belluno\",\"BN\":\"Benevento\",\"BG\":\"Bergamo\",\"BI\":\"Biella\",\"BO\":\"Bologna\",\"BZ\":\"Bolzano\",\"BS\":\"Brescia\",\"BR\":\"Brindisi\",\"CA\":\"Cagliari\",\"CL\":\"Caltanissetta\",\"CB\":\"Campobasso\",\"CE\":\"Caserta\",\"CT\":\"Catania\",\"CZ\":\"Catanzaro\",\"CH\":\"Chieti\",\"CO\":\"Como\",\"CS\":\"Cosenza\",\"CR\":\"Cremona\",\"KR\":\"Crotone\",\"CN\":\"Cuneo\",\"EN\":\"Enna\",\"FM\":\"Fermo\",\"FE\":\"Ferrara\",\"FI\":\"Firenze\",\"FG\":\"Foggia\",\"FC\":\"Forl\\u00ec-Cesena\",\"FR\":\"Frosinone\",\"GE\":\"Genova\",\"GO\":\"Gorizia\",\"GR\":\"Grosseto\",\"IM\":\"Imperia\",\"IS\":\"Isernia\",\"SP\":\"La Spezia\",\"AQ\":\"L'Aquila\",\"LT\":\"Latina\",\"LE\":\"Lecce\",\"LC\":\"Lecco\",\"LI\":\"Livorno\",\"LO\":\"Lodi\",\"LU\":\"Lucca\",\"MC\":\"Macerata\",\"MN\":\"Mantova\",\"MS\":\"Massa-Carrara\",\"MT\":\"Matera\",\"ME\":\"Messina\",\"MI\":\"Milano\",\"MO\":\"Modena\",\"MB\":\"Monza e della Brianza\",\"NA\":\"Napoli\",\"NO\":\"Novara\",\"NU\":\"Nuoro\",\"OR\":\"Oristano\",\"PD\":\"Padova\",\"PA\":\"Palermo\",\"PR\":\"Parma\",\"PV\":\"Pavia\",\"PG\":\"Perugia\",\"PU\":\"Pesaro e Urbino\",\"PE\":\"Pescara\",\"PC\":\"Piacenza\",\"PI\":\"Pisa\",\"PT\":\"Pistoia\",\"PN\":\"Pordenone\",\"PZ\":\"Potenza\",\"PO\":\"Prato\",\"RG\":\"Ragusa\",\"RA\":\"Ravenna\",\"RC\":\"Reggio Calabria\",\"RE\":\"Reggio Emilia\",\"RI\":\"Rieti\",\"RN\":\"Rimini\",\"RM\":\"Roma\",\"RO\":\"Rovigo\",\"SA\":\"Salerno\",\"SS\":\"Sassari\",\"SV\":\"Savona\",\"SI\":\"Siena\",\"SR\":\"Siracusa\",\"SO\":\"Sondrio\",\"SU\":\"Sud Sardegna\",\"TA\":\"Taranto\",\"TE\":\"Teramo\",\"TR\":\"Terni\",\"TO\":\"Torino\",\"TP\":\"Trapani\",\"TN\":\"Trento\",\"TV\":\"Treviso\",\"TS\":\"Trieste\",\"UD\":\"Udine\",\"VA\":\"Varese\",\"VE\":\"Venezia\",\"VB\":\"Verbano-Cusio-Ossola\",\"VC\":\"Vercelli\",\"VR\":\"Verona\",\"VV\":\"Vibo Valentia\",\"VI\":\"Vicenza\",\"VT\":\"Viterbo\"},\"IL\":[],\"IM\":[],\"JM\":{\"JM-01\":\"Kingston\",\"JM-02\":\"Saint Andrew\",\"JM-03\":\"Saint Thomas\",\"JM-04\":\"Portland\",\"JM-05\":\"Saint Mary\",\"JM-06\":\"Saint Ann\",\"JM-07\":\"Trelawny\",\"JM-08\":\"Saint James\",\"JM-09\":\"Hanover\",\"JM-10\":\"Westmoreland\",\"JM-11\":\"Saint Elizabeth\",\"JM-12\":\"\\u0645\\u0646\\u062c\\u0633\\u062a\\u0631\",\"JM-13\":\"Clarendon\",\"JM-14\":\"\\u0633\\u0646\\u062a \\u06a9\\u0627\\u062a\\u0631\\u06cc\\u0646\"},\"JP\":{\"JP01\":\"Hokkaido\",\"JP02\":\"Aomori\",\"JP03\":\"Iwate\",\"JP04\":\"Miyagi\",\"JP05\":\"Akita\",\"JP06\":\"Yamagata\",\"JP07\":\"Fukushima\",\"JP08\":\"Ibaraki\",\"JP09\":\"Tochigi\",\"JP10\":\"Gunma\",\"JP11\":\"Saitama\",\"JP12\":\"Chiba\",\"JP13\":\"Tokyo\",\"JP14\":\"Kanagawa\",\"JP15\":\"Niigata\",\"JP16\":\"Toyama\",\"JP17\":\"Ishikawa\",\"JP18\":\"Fukui\",\"JP19\":\"Yamanashi\",\"JP20\":\"Nagano\",\"JP21\":\"Gifu\",\"JP22\":\"Shizuoka\",\"JP23\":\"Aichi\",\"JP24\":\"Mie\",\"JP25\":\"Shiga\",\"JP26\":\"Kyoto\",\"JP27\":\"Osaka\",\"JP28\":\"Hyogo\",\"JP29\":\"Nara\",\"JP30\":\"Wakayama\",\"JP31\":\"Tottori\",\"JP32\":\"Shimane\",\"JP33\":\"Okayama\",\"JP34\":\"Hiroshima\",\"JP35\":\"Yamaguchi\",\"JP36\":\"Tokushima\",\"JP37\":\"Kagawa\",\"JP38\":\"Ehime\",\"JP39\":\"Kochi\",\"JP40\":\"Fukuoka\",\"JP41\":\"Saga\",\"JP42\":\"Nagasaki\",\"JP43\":\"Kumamoto\",\"JP44\":\"Oita\",\"JP45\":\"Miyazaki\",\"JP46\":\"Kagoshima\",\"JP47\":\"Okinawa\"},\"KE\":{\"KE01\":\"Baringo\",\"KE02\":\"Bomet\",\"KE03\":\"Bungoma\",\"KE04\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"KE05\":\"Elgeyo-Marakwet\",\"KE06\":\"Embu\",\"KE07\":\"Garissa\",\"KE08\":\"Homa Bay\",\"KE09\":\"Isiolo\",\"KE10\":\"Kajiado\",\"KE11\":\"Kakamega\",\"KE12\":\"Kericho\",\"KE13\":\"Kiambu\",\"KE14\":\"Kilifi\",\"KE15\":\"Kirinyaga\",\"KE16\":\"Kisii\",\"KE17\":\"Kisumu\",\"KE18\":\"Kitui\",\"KE19\":\"Kwale\",\"KE20\":\"Laikipia\",\"KE21\":\"Lamu\",\"KE22\":\"Machakos\",\"KE23\":\"Makueni\",\"KE24\":\"Mandera\",\"KE25\":\"Marsabit\",\"KE26\":\"Meru\",\"KE27\":\"Migori\",\"KE28\":\"Mombasa\",\"KE29\":\"Murang\\u2019a\",\"KE30\":\"Nairobi County\",\"KE31\":\"Nakuru\",\"KE32\":\"Nandi\",\"KE33\":\"Narok\",\"KE34\":\"Nyamira\",\"KE35\":\"Nyandarua\",\"KE36\":\"Nyeri\",\"KE37\":\"Samburu\",\"KE38\":\"Siaya\",\"KE39\":\"Taita-Taveta\",\"KE40\":\"Tana River\",\"KE41\":\"Tharaka-Nithi\",\"KE42\":\"Trans Nzoia\",\"KE43\":\"Turkana\",\"KE44\":\"Uasin Gishu\",\"KE45\":\"Vihiga\",\"KE46\":\"Wajir\",\"KE47\":\"West Pokot\"},\"KR\":[],\"KW\":[],\"LA\":{\"AT\":\"\\u0622\\u062a\\u0627\\u067e\\u06cc\\u0648\",\"BK\":\"\\u0628\\u0648\\u06a9\\u06cc\\u0648\",\"BL\":\"\\u0628\\u0648\\u0644\\u06cc\\u06a9\\u0627\\u0645\\u200c\\u0633\\u0627\\u06cc\",\"CH\":\"\\u0686\\u0627\\u0645\\u067e\\u0627\\u0633\\u0627\\u06a9\",\"HO\":\"\\u0647\\u0648\\u0627\\u0641\\u0627\\u0646\",\"KH\":\"\\u062e\\u0627\\u0645\\u0648\\u0627\\u0646\",\"LM\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u0646\\u0627\\u0645\\u062a\\u0627\",\"LP\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u067e\\u0631\\u0627\\u0628\\u0627\\u0646\\u06af\",\"OU\":\"\\u0627\\u0648\\u062f\\u0648\\u0645\\u0634\\u0627\\u06cc\",\"PH\":\"\\u0641\\u0648\\u0646\\u06af\\u0633\\u0627\\u0644\\u06cc\",\"SL\":\"\\u0633\\u0627\\u0644\\u0627\\u0648\\u0627\\u0646\",\"SV\":\"\\u0633\\u0627\\u0648\\u0627\\u0646\\u0627\\u062e\\u062a\",\"VI\":\"\\u0627\\u0633\\u062a\\u0627\\u0646 \\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"VT\":\"\\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"XA\":\"\\u0633\\u0627\\u06cc\\u0646\\u06cc\\u0627\\u0628\\u0648\\u0644\\u06cc\",\"XE\":\"\\u0633\\u06a9\\u0648\\u0646\\u06af\",\"XI\":\"\\u0634\\u06cc\\u0627\\u0646\\u06af\\u062e\\u0648\\u0627\\u0646\\u06af\",\"XS\":\"Xaisomboun\"},\"LB\":[],\"LR\":{\"BM\":\"Bomi\",\"BN\":\"Bong\",\"GA\":\"Gbarpolu\",\"GB\":\"Grand Bassa\",\"GC\":\"Grand Cape Mount\",\"GG\":\"Grand Gedeh\",\"GK\":\"Grand Kru\",\"LO\":\"Lofa\",\"MA\":\"Margibi\",\"MY\":\"Maryland\",\"MO\":\"Montserrado\",\"NM\":\"Nimba\",\"RV\":\"Rivercess\",\"RG\":\"River Gee\",\"SN\":\"Sinoe\"},\"LU\":[],\"MD\":{\"C\":\"Chi\\u0219in\\u0103u\",\"BL\":\"B\\u0103l\\u021bi\",\"AN\":\"Anenii Noi\",\"BS\":\"Basarabeasca\",\"BR\":\"Briceni\",\"CH\":\"Cahul\",\"CT\":\"Cantemir\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"C\\u0103u\\u0219eni\",\"CM\":\"Cimi\\u0219lia\",\"CR\":\"Criuleni\",\"DN\":\"Dondu\\u0219eni\",\"DR\":\"Drochia\",\"DB\":\"Dub\\u0103sari\",\"ED\":\"Edine\\u021b\",\"FL\":\"F\\u0103le\\u0219ti\",\"FR\":\"Flore\\u0219ti\",\"GE\":\"UTA G\\u0103g\\u0103uzia\",\"GL\":\"Glodeni\",\"HN\":\"H\\u00eence\\u0219ti\",\"IL\":\"Ialoveni\",\"LV\":\"Leova\",\"NS\":\"Nisporeni\",\"OC\":\"Ocni\\u021ba\",\"OR\":\"Orhei\",\"RZ\":\"Rezina\",\"RS\":\"R\\u00ee\\u0219cani\",\"SG\":\"S\\u00eengerei\",\"SR\":\"Soroca\",\"ST\":\"Str\\u0103\\u0219eni\",\"SD\":\"\\u0218old\\u0103ne\\u0219ti\",\"SV\":\"\\u0218tefan Vod\\u0103\",\"TR\":\"Taraclia\",\"TL\":\"Telene\\u0219ti\",\"UN\":\"Ungheni\"},\"MQ\":[],\"MT\":[],\"MX\":{\"DF\":\"Ciudad de M\\u00e9xico\",\"JA\":\"Jalisco\",\"NL\":\"Nuevo Le\\u00f3n\",\"AG\":\"Aguascalientes\",\"BC\":\"Baja California\",\"BS\":\"Baja California Sur\",\"CM\":\"Campeche\",\"CS\":\"Chiapas\",\"CH\":\"Chihuahua\",\"CO\":\"Coahuila\",\"CL\":\"Colima\",\"DG\":\"Durango\",\"GT\":\"Guanajuato\",\"GR\":\"Guerrero\",\"HG\":\"Hidalgo\",\"MX\":\"Estado de M\\u00e9xico\",\"MI\":\"Michoac\\u00e1n\",\"MO\":\"Morelos\",\"NA\":\"Nayarit\",\"OA\":\"Oaxaca\",\"PU\":\"Puebla\",\"QT\":\"Quer\\u00e9taro\",\"QR\":\"Quintana Roo\",\"SL\":\"San Luis Potos\\u00ed\",\"SI\":\"Sinaloa\",\"SO\":\"Sonora\",\"TB\":\"Tabasco\",\"TM\":\"Tamaulipas\",\"TL\":\"Tlaxcala\",\"VE\":\"Veracruz\",\"YU\":\"Yucat\\u00e1n\",\"ZA\":\"Zacatecas\"},\"MY\":{\"JHR\":\"Johor\",\"KDH\":\"Kedah\",\"KTN\":\"Kelantan\",\"LBN\":\"Labuan\",\"MLK\":\"Malacca (Melaka)\",\"NSN\":\"Negeri Sembilan\",\"PHG\":\"Pahang\",\"PNG\":\"Penang (Pulau Pinang)\",\"PRK\":\"Perak\",\"PLS\":\"Perlis\",\"SBH\":\"Sabah\",\"SWK\":\"Sarawak\",\"SGR\":\"Selangor\",\"TRG\":\"Terengganu\",\"PJY\":\"Putrajaya\",\"KUL\":\"Kuala Lumpur\"},\"MZ\":{\"MZP\":\"Cabo Delgado\",\"MZG\":\"\\u063a\\u0632\\u0647\",\"MZI\":\"Inhambane\",\"MZB\":\"Manica\",\"MZL\":\"Maputo Province\",\"MZMPM\":\"Maputo\",\"MZN\":\"Nampula\",\"MZA\":\"Niassa\",\"MZS\":\"Sofala\",\"MZT\":\"\\u062a\\u062a\\u0647\",\"MZQ\":\"\\u0632\\u0627\\u0645\\u0628\\u0632\\u06cc\\u0627\"},\"NA\":{\"ER\":\"\\u0627\\u0631\\u0648\\u0646\\u06af\\u0648\",\"HA\":\"\\u0647\\u0627\\u0631\\u062f\\u067e\",\"KA\":\"\\u06a9\\u0627\\u0631\\u0627\\u0633\",\"KE\":\"\\u06a9\\u0627\\u0648\\u0627\\u0646\\u06af\\u0648 \\u0634\\u0631\\u0642\\u06cc\",\"KW\":\"Kavango West\",\"KH\":\"Khomas\",\"KU\":\"Kunene\",\"OW\":\"Ohangwena\",\"OH\":\"Omaheke\",\"OS\":\"Omusati\",\"ON\":\"Oshana\",\"OT\":\"Oshikoto\",\"OD\":\"Otjozondjupa\",\"CA\":\"Zambezi\"},\"NG\":{\"AB\":\"Abia\",\"FC\":\"Abuja\",\"AD\":\"Adamawa\",\"AK\":\"Akwa Ibom\",\"AN\":\"Anambra\",\"BA\":\"Bauchi\",\"BY\":\"Bayelsa\",\"BE\":\"Benue\",\"BO\":\"Borno\",\"CR\":\"Cross River\",\"DE\":\"Delta\",\"EB\":\"Ebonyi\",\"ED\":\"Edo\",\"EK\":\"Ekiti\",\"EN\":\"Enugu\",\"GO\":\"Gombe\",\"IM\":\"Imo\",\"JI\":\"Jigawa\",\"KD\":\"Kaduna\",\"KN\":\"Kano\",\"KT\":\"Katsina\",\"KE\":\"Kebbi\",\"KO\":\"Kogi\",\"KW\":\"Kwara\",\"LA\":\"Lagos\",\"NA\":\"Nasarawa\",\"NI\":\"\\u0646\\u06cc\\u062c\\u0631\",\"OG\":\"Ogun\",\"ON\":\"Ondo\",\"OS\":\"Osun\",\"OY\":\"Oyo\",\"PL\":\"Plateau\",\"RI\":\"Rivers\",\"SO\":\"Sokoto\",\"TA\":\"Taraba\",\"YO\":\"Yobe\",\"ZA\":\"Zamfara\"},\"NL\":[],\"NO\":[],\"NP\":{\"BAG\":\"Bagmati\",\"BHE\":\"Bheri\",\"DHA\":\"Dhaulagiri\",\"GAN\":\"Gandaki\",\"JAN\":\"Janakpur\",\"KAR\":\"Karnali\",\"KOS\":\"Koshi\",\"LUM\":\"Lumbini\",\"MAH\":\"Mahakali\",\"MEC\":\"Mechi\",\"NAR\":\"Narayani\",\"RAP\":\"Rapti\",\"SAG\":\"Sagarmatha\",\"SET\":\"Seti\"},\"NZ\":{\"NL\":\"Northland\",\"AK\":\"Auckland\",\"WA\":\"Waikato\",\"BP\":\"Bay of Plenty\",\"TK\":\"Taranaki\",\"GI\":\"Gisborne\",\"HB\":\"Hawke\\u2019s Bay\",\"MW\":\"Manawatu-Wanganui\",\"WE\":\"Wellington\",\"NS\":\"Nelson\",\"MB\":\"Marlborough\",\"TM\":\"Tasman\",\"WC\":\"West Coast\",\"CT\":\"Canterbury\",\"OT\":\"Otago\",\"SL\":\"Southland\"},\"PE\":{\"CAL\":\"El Callao\",\"LMA\":\"Municipalidad Metropolitana de Lima\",\"AMA\":\"Amazonas\",\"ANC\":\"Ancash\",\"APU\":\"Apur\\u00edmac\",\"ARE\":\"\\u0636\\u0631\\u0648\\u0631\\u06cc\",\"AYA\":\"Ayacucho\",\"CAJ\":\"Cajamarca\",\"CUS\":\"Cusco\",\"HUV\":\"Huancavelica\",\"HUC\":\"Hu\\u00e1nuco\",\"ICA\":\"Ica\",\"JUN\":\"Jun\\u00edn\",\"LAL\":\"La Libertad\",\"LAM\":\"Lambayeque\",\"LIM\":\"Lima\",\"LOR\":\"Loreto\",\"MDD\":\"Madre de Dios\",\"MOQ\":\"Moquegua\",\"PAS\":\"Pasco\",\"PIU\":\"Piura\",\"PUN\":\"Puno\",\"SAM\":\"San Mart\\u00edn\",\"TAC\":\"Tacna\",\"TUM\":\"Tumbes\",\"UCA\":\"Ucayali\"},\"PH\":{\"ABR\":\"Abra\",\"AGN\":\"Agusan del Norte\",\"AGS\":\"Agusan del Sur\",\"AKL\":\"Aklan\",\"ALB\":\"Albay\",\"ANT\":\"Antique\",\"APA\":\"Apayao\",\"AUR\":\"Aurora\",\"BAS\":\"Basilan\",\"BAN\":\"Bataan\",\"BTN\":\"Batanes\",\"BTG\":\"Batangas\",\"BEN\":\"Benguet\",\"BIL\":\"Biliran\",\"BOH\":\"Bohol\",\"BUK\":\"Bukidnon\",\"BUL\":\"Bulacan\",\"CAG\":\"Cagayan\",\"CAN\":\"Camarines Norte\",\"CAS\":\"Camarines Sur\",\"CAM\":\"Camiguin\",\"CAP\":\"Capiz\",\"CAT\":\"Catanduanes\",\"CAV\":\"Cavite\",\"CEB\":\"Cebu\",\"COM\":\"Compostela Valley\",\"NCO\":\"Cotabato\",\"DAV\":\"Davao del Norte\",\"DAS\":\"Davao del Sur\",\"DAC\":\"Davao Occidental\",\"DAO\":\"Davao Oriental\",\"DIN\":\"Dinagat Islands\",\"EAS\":\"Eastern Samar\",\"GUI\":\"Guimaras\",\"IFU\":\"Ifugao\",\"ILN\":\"Ilocos Norte\",\"ILS\":\"Ilocos Sur\",\"ILI\":\"Iloilo\",\"ISA\":\"Isabela\",\"KAL\":\"Kalinga\",\"LUN\":\"La Union\",\"LAG\":\"Laguna\",\"LAN\":\"Lanao del Norte\",\"LAS\":\"Lanao del Sur\",\"LEY\":\"Leyte\",\"MAG\":\"Maguindanao\",\"MAD\":\"Marinduque\",\"MAS\":\"Masbate\",\"MSC\":\"Misamis Occidental\",\"MSR\":\"Misamis Oriental\",\"MOU\":\"Mountain Province\",\"NEC\":\"Negros Occidental\",\"NER\":\"Negros Oriental\",\"NSA\":\"Northern Samar\",\"NUE\":\"Nueva Ecija\",\"NUV\":\"Nueva Vizcaya\",\"MDC\":\"Occidental Mindoro\",\"MDR\":\"Oriental Mindoro\",\"PLW\":\"Palawan\",\"PAM\":\"Pampanga\",\"PAN\":\"Pangasinan\",\"QUE\":\"Quezon\",\"QUI\":\"Quirino\",\"RIZ\":\"Rizal\",\"ROM\":\"Romblon\",\"WSA\":\"Samar\",\"SAR\":\"Sarangani\",\"SIQ\":\"Siquijor\",\"SOR\":\"Sorsogon\",\"SCO\":\"South Cotabato\",\"SLE\":\"Southern Leyte\",\"SUK\":\"Sultan Kudarat\",\"SLU\":\"Sulu\",\"SUN\":\"Surigao del Norte\",\"SUR\":\"Surigao del Sur\",\"TAR\":\"Tarlac\",\"TAW\":\"Tawi-Tawi\",\"ZMB\":\"Zambales\",\"ZAN\":\"Zamboanga del Norte\",\"ZAS\":\"Zamboanga del Sur\",\"ZSI\":\"Zamboanga Sibugay\",\"00\":\"Metro Manila\"},\"PK\":{\"JK\":\"Azad Kashmir\",\"BA\":\"Balochistan\",\"TA\":\"FATA\",\"GB\":\"Gilgit Baltistan\",\"IS\":\"Islamabad Capital Territory\",\"KP\":\"Khyber Pakhtunkhwa\",\"PB\":\"Punjab\",\"SD\":\"Sindh\"},\"PL\":[],\"PR\":[],\"PT\":[],\"PY\":{\"PY-ASU\":\"Asunci\\u00f3n\",\"PY-1\":\"Concepci\\u00f3n\",\"PY-2\":\"\\u0633\\u0646 \\u067e\\u062f\\u0631\\u0648\",\"PY-3\":\"\\u06a9\\u0648\\u0631\\u062f\\u06cc\\u0644\\u0631\\u0627\",\"PY-4\":\"Guair\\u00e1\",\"PY-5\":\"Caaguaz\\u00fa\",\"PY-6\":\"Caazap\\u00e1\",\"PY-7\":\"Itap\\u00faa\",\"PY-8\":\"Misiones\",\"PY-9\":\"Paraguar\\u00ed\",\"PY-10\":\"Alto Paran\\u00e1\",\"PY-11\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"PY-12\":\"\\u00d1eembuc\\u00fa\",\"PY-13\":\"\\u0622\\u0645\\u0627\\u0645\\u0628\\u0627\\u06cc\",\"PY-14\":\"Canindey\\u00fa\",\"PY-15\":\"\\u067e\\u0631\\u0632\\u06cc\\u062f\\u0646\\u062a \\u0647\\u06cc\\u0632\",\"PY-16\":\"\\u0622\\u0644\\u062a\\u0648 \\u067e\\u0627\\u0631\\u0627\\u06af\\u0648\\u0626\\u0647\",\"PY-17\":\"Boquer\\u00f3n\"},\"RE\":[],\"RO\":{\"AB\":\"Alba\",\"AR\":\"Arad\",\"AG\":\"Arge\\u0219\",\"BC\":\"Bac\\u0103u\",\"BH\":\"Bihor\",\"BN\":\"Bistri\\u021ba-N\\u0103s\\u0103ud\",\"BT\":\"Boto\\u0219ani\",\"BR\":\"Br\\u0103ila\",\"BV\":\"Bra\\u0219ov\",\"B\":\"Bucure\\u0219ti\",\"BZ\":\"Buz\\u0103u\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"Cara\\u0219-Severin\",\"CJ\":\"Cluj\",\"CT\":\"Constan\\u021ba\",\"CV\":\"Covasna\",\"DB\":\"D\\u00e2mbovi\\u021ba\",\"DJ\":\"Dolj\",\"GL\":\"Gala\\u021bi\",\"GR\":\"Giurgiu\",\"GJ\":\"Gorj\",\"HR\":\"Harghita\",\"HD\":\"Hunedoara\",\"IL\":\"Ialomi\\u021ba\",\"IS\":\"Ia\\u0219i\",\"IF\":\"Ilfov\",\"MM\":\"Maramure\\u0219\",\"MH\":\"Mehedin\\u021bi\",\"MS\":\"Mure\\u0219\",\"NT\":\"Neam\\u021b\",\"OT\":\"Olt\",\"PH\":\"Prahova\",\"SJ\":\"S\\u0103laj\",\"SM\":\"Satu Mare\",\"SB\":\"Sibiu\",\"SV\":\"Suceava\",\"TR\":\"Teleorman\",\"TM\":\"Timi\\u0219\",\"TL\":\"Tulcea\",\"VL\":\"V\\u00e2lcea\",\"VS\":\"Vaslui\",\"VN\":\"Vrancea\"},\"RS\":{\"RS00\":\"Belgrade\",\"RS14\":\"Bor\",\"RS11\":\"Brani\\u010devo\",\"RS02\":\"Central Banat\",\"RS10\":\"Danube\",\"RS23\":\"Jablanica\",\"RS09\":\"Kolubara\",\"RS08\":\"Ma\\u010dva\",\"RS17\":\"Morava\",\"RS20\":\"Ni\\u0161ava\",\"RS01\":\"North Ba\\u010dka\",\"RS03\":\"North Banat\",\"RS24\":\"P\\u010dinja\",\"RS22\":\"Pirot\",\"RS13\":\"Pomoravlje\",\"RS19\":\"Rasina\",\"RS18\":\"Ra\\u0161ka\",\"RS06\":\"South Ba\\u010dka\",\"RS04\":\"South Banat\",\"RS07\":\"Srem\",\"RS12\":\"\\u0160umadija\",\"RS21\":\"Toplica\",\"RS05\":\"West Ba\\u010dka\",\"RS15\":\"Zaje\\u010dar\",\"RS16\":\"Zlatibor\",\"RS25\":\"Kosovo\",\"RS26\":\"Pe\\u0107\",\"RS27\":\"Prizren\",\"RS28\":\"Kosovska Mitrovica\",\"RS29\":\"Kosovo-Pomoravlje\",\"RSKM\":\"Kosovo-Metohija\",\"RSVO\":\"Vojvodina\"},\"SG\":[],\"SK\":[],\"SI\":[],\"TH\":{\"TH-37\":\"Amnat Charoen\",\"TH-15\":\"Ang Thong\",\"TH-14\":\"Ayutthaya\",\"TH-10\":\"Bangkok\",\"TH-38\":\"Bueng Kan\",\"TH-31\":\"Buri Ram\",\"TH-24\":\"Chachoengsao\",\"TH-18\":\"Chai Nat\",\"TH-36\":\"Chaiyaphum\",\"TH-22\":\"Chanthaburi\",\"TH-50\":\"Chiang Mai\",\"TH-57\":\"Chiang Rai\",\"TH-20\":\"Chonburi\",\"TH-86\":\"Chumphon\",\"TH-46\":\"Kalasin\",\"TH-62\":\"Kamphaeng Phet\",\"TH-71\":\"Kanchanaburi\",\"TH-40\":\"Khon Kaen\",\"TH-81\":\"Krabi\",\"TH-52\":\"Lampang\",\"TH-51\":\"Lamphun\",\"TH-42\":\"Loei\",\"TH-16\":\"Lopburi\",\"TH-58\":\"Mae Hong Son\",\"TH-44\":\"Maha Sarakham\",\"TH-49\":\"Mukdahan\",\"TH-26\":\"Nakhon Nayok\",\"TH-73\":\"Nakhon Pathom\",\"TH-48\":\"Nakhon Phanom\",\"TH-30\":\"Nakhon Ratchasima\",\"TH-60\":\"Nakhon Sawan\",\"TH-80\":\"Nakhon Si Thammarat\",\"TH-55\":\"Nan\",\"TH-96\":\"Narathiwat\",\"TH-39\":\"Nong Bua Lam Phu\",\"TH-43\":\"Nong Khai\",\"TH-12\":\"Nonthaburi\",\"TH-13\":\"Pathum Thani\",\"TH-94\":\"Pattani\",\"TH-82\":\"Phang Nga\",\"TH-93\":\"Phatthalung\",\"TH-56\":\"Phayao\",\"TH-67\":\"Phetchabun\",\"TH-76\":\"Phetchaburi\",\"TH-66\":\"Phichit\",\"TH-65\":\"Phitsanulok\",\"TH-54\":\"Phrae\",\"TH-83\":\"Phuket\",\"TH-25\":\"Prachin Buri\",\"TH-77\":\"Prachuap Khiri Khan\",\"TH-85\":\"Ranong\",\"TH-70\":\"Ratchaburi\",\"TH-21\":\"Rayong\",\"TH-45\":\"Roi Et\",\"TH-27\":\"Sa Kaeo\",\"TH-47\":\"Sakon Nakhon\",\"TH-11\":\"Samut Prakan\",\"TH-74\":\"Samut Sakhon\",\"TH-75\":\"Samut Songkhram\",\"TH-19\":\"Saraburi\",\"TH-91\":\"Satun\",\"TH-17\":\"Sing Buri\",\"TH-33\":\"Sisaket\",\"TH-90\":\"Songkhla\",\"TH-64\":\"Sukhothai\",\"TH-72\":\"Suphan Buri\",\"TH-84\":\"Surat Thani\",\"TH-32\":\"Surin\",\"TH-63\":\"Tak\",\"TH-92\":\"Trang\",\"TH-23\":\"Trat\",\"TH-34\":\"Ubon Ratchathani\",\"TH-41\":\"Udon Thani\",\"TH-61\":\"Uthai Thani\",\"TH-53\":\"Uttaradit\",\"TH-95\":\"Yala\",\"TH-35\":\"Yasothon\"},\"TR\":{\"TR01\":\"Adana\",\"TR02\":\"Ad\\u0131yaman\",\"TR03\":\"Afyon\",\"TR04\":\"A\\u011fr\\u0131\",\"TR05\":\"Amasya\",\"TR06\":\"Ankara\",\"TR07\":\"Antalya\",\"TR08\":\"Artvin\",\"TR09\":\"Ayd\\u0131n\",\"TR10\":\"Bal\\u0131kesir\",\"TR11\":\"Bilecik\",\"TR12\":\"Bing\\u00f6l\",\"TR13\":\"Bitlis\",\"TR14\":\"Bolu\",\"TR15\":\"Burdur\",\"TR16\":\"Bursa\",\"TR17\":\"\\u00c7anakkale\",\"TR18\":\"\\u00c7ank\\u0131r\\u0131\",\"TR19\":\"\\u00c7orum\",\"TR20\":\"Denizli\",\"TR21\":\"Diyarbak\\u0131r\",\"TR22\":\"Edirne\",\"TR23\":\"Elaz\\u0131\\u011f\",\"TR24\":\"Erzincan\",\"TR25\":\"Erzurum\",\"TR26\":\"Eski\\u015fehir\",\"TR27\":\"Gaziantep\",\"TR28\":\"Giresun\",\"TR29\":\"G\\u00fcm\\u00fc\\u015fhane\",\"TR30\":\"Hakkari\",\"TR31\":\"Hatay\",\"TR32\":\"Isparta\",\"TR33\":\"\\u0130\\u00e7el\",\"TR34\":\"\\u0130stanbul\",\"TR35\":\"\\u0130zmir\",\"TR36\":\"Kars\",\"TR37\":\"Kastamonu\",\"TR38\":\"Kayseri\",\"TR39\":\"K\\u0131rklareli\",\"TR40\":\"K\\u0131r\\u015fehir\",\"TR41\":\"Kocaeli\",\"TR42\":\"Konya\",\"TR43\":\"K\\u00fctahya\",\"TR44\":\"Malatya\",\"TR45\":\"Manisa\",\"TR46\":\"Kahramanmara\\u015f\",\"TR47\":\"Mardin\",\"TR48\":\"Mu\\u011fla\",\"TR49\":\"Mu\\u015f\",\"TR50\":\"Nev\\u015fehir\",\"TR51\":\"Ni\\u011fde\",\"TR52\":\"Ordu\",\"TR53\":\"Rize\",\"TR54\":\"Sakarya\",\"TR55\":\"Samsun\",\"TR56\":\"Siirt\",\"TR57\":\"Sinop\",\"TR58\":\"Sivas\",\"TR59\":\"Tekirda\\u011f\",\"TR60\":\"Tokat\",\"TR61\":\"Trabzon\",\"TR62\":\"Tunceli\",\"TR63\":\"\\u015eanl\\u0131urfa\",\"TR64\":\"U\\u015fak\",\"TR65\":\"Van\",\"TR66\":\"Yozgat\",\"TR67\":\"Zonguldak\",\"TR68\":\"Aksaray\",\"TR69\":\"Bayburt\",\"TR70\":\"Karaman\",\"TR71\":\"K\\u0131r\\u0131kkale\",\"TR72\":\"Batman\",\"TR73\":\"\\u015e\\u0131rnak\",\"TR74\":\"Bart\\u0131n\",\"TR75\":\"Ardahan\",\"TR76\":\"I\\u011fd\\u0131r\",\"TR77\":\"Yalova\",\"TR78\":\"Karab\\u00fck\",\"TR79\":\"Kilis\",\"TR80\":\"Osmaniye\",\"TR81\":\"D\\u00fczce\"},\"TZ\":{\"TZ01\":\"Arusha\",\"TZ02\":\"Dar es Salaam\",\"TZ03\":\"Dodoma\",\"TZ04\":\"Iringa\",\"TZ05\":\"Kagera\",\"TZ06\":\"Pemba North\",\"TZ07\":\"Zanzibar North\",\"TZ08\":\"Kigoma\",\"TZ09\":\"Kilimanjaro\",\"TZ10\":\"Pemba South\",\"TZ11\":\"Zanzibar South\",\"TZ12\":\"Lindi\",\"TZ13\":\"Mara\",\"TZ14\":\"Mbeya\",\"TZ15\":\"Zanzibar West\",\"TZ16\":\"Morogoro\",\"TZ17\":\"Mtwara\",\"TZ18\":\"Mwanza\",\"TZ19\":\"Coast\",\"TZ20\":\"Rukwa\",\"TZ21\":\"Ruvuma\",\"TZ22\":\"Shinyanga\",\"TZ23\":\"Singida\",\"TZ24\":\"Tabora\",\"TZ25\":\"Tanga\",\"TZ26\":\"Manyara\",\"TZ27\":\"Geita\",\"TZ28\":\"Katavi\",\"TZ29\":\"Njombe\",\"TZ30\":\"Simiyu\"},\"LK\":[],\"SE\":[],\"UG\":{\"UG314\":\"\\u0622\\u0628\\u06cc\\u0645\",\"UG301\":\"\\u0622\\u062f\\u0648\\u0645\\u0627\\u0646\\u06cc\",\"UG322\":\"\\u0622\\u06af\\u0627\\u06af\\u0648\",\"UG323\":\"\\u0622\\u0644\\u0628\\u0648\\u0646\\u06af\",\"UG315\":\"\\u0622\\u0645\\u0648\\u0644\\u0627\\u062a\\u0627\\u0631\",\"UG324\":\"\\u0622\\u0645\\u0648\\u062f\\u0627\\u062a\",\"UG216\":\"\\u0622\\u0645\\u0648\\u0631\\u06cc\\u0627\",\"UG316\":\"\\u0622\\u0645\\u0648\\u0631\\u0648\",\"UG302\":\"\\u0622\\u067e\\u0627\\u06a9\",\"UG303\":\"\\u0622\\u0631\\u0648\\u0627\",\"UG217\":\"\\u0628\\u0648\\u062f\\u0627\\u06a9\\u0627\",\"UG218\":\"\\u0628\\u0648\\u062f\\u0627\",\"UG201\":\"\\u0628\\u0648\\u06af\\u06cc\\u0631\\u06cc\",\"UG235\":\"\\u0628\\u0648\\u06af\\u0648\\u0631\\u064a\",\"UG420\":\"\\u0628\\u0648\\u0648\\u0647\\u0648\\u062c\\u0648\",\"UG117\":\"\\u0628\\u06cc\\u0648\\u06a9\\u0648\\u0647\",\"UG219\":\"\\u0628\\u0648\\u06a9\\u0648\\u062f\\u0627\",\"UG118\":\"\\u0628\\u0648\\u06a9\\u0648\\u0645\\u0646\\u0633\\u06cc\\u0628\\u06cc\",\"UG220\":\"\\u0628\\u0648\\u06a9\\u0648\\u0627\",\"UG225\":\"\\u0628\\u0648\\u0644\\u0627\\u0645\\u0628\\u0648\\u0644\\u06cc\",\"UG416\":\"\\u0628\\u0648\\u0644\\u06cc\\u0633\\u0627\",\"UG401\":\"Bundibugyo\",\"UG430\":\"\\u0628\\u0648\\u0646\\u0627\\u0646\\u06af\\u0627\\u0628\\u0648\",\"UG402\":\"\\u0628\\u0648\\u0634\\u0646\\u06cc\",\"UG202\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"UG221\":\"\\u0628\\u0648\\u062a\\u0627\\u0644\\u0647\\u06cc\\u0627\",\"UG119\":\"\\u0628\\u0648\\u062a\\u0627\\u0645\\u0628\\u0644\\u0627\",\"UG233\":\"Butebo\",\"UG120\":\"\\u0628\\u0648\\u0648\\u0648\\u0645\\u0627\",\"UG226\":\"Buyende\",\"UG317\":\"\\u062f\\u0648\\u06a9\\u0648\\u0644\\u0648\",\"UG121\":\"\\u06af\\u0645\\u0628\\u0627\",\"UG304\":\"\\u06af\\u0644\\u0648\",\"UG403\":\"\\u0647\\u0648\\u06cc\\u0645\\u0627\",\"UG417\":\"\\u0627\\u06cc\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG203\":\"\\u0627\\u06cc\\u06af\\u0627\\u0646\\u06af\\u0627\",\"UG418\":\"\\u0627\\u06cc\\u0633\\u06cc\\u0646\\u06af\\u06cc\\u0631\\u0648\",\"UG204\":\"\\u062c\\u06cc\\u0646\\u062c\\u0627\",\"UG318\":\"\\u06a9\\u0627\\u0628\\u0648\\u0646\\u06af\",\"UG404\":\"\\u06a9\\u0627\\u0628\\u0644\",\"UG405\":\"\\u06a9\\u0627\\u0628\\u0627\\u0631\\u0648\\u0644\",\"UG213\":\"\\u06a9\\u0627\\u0628\\u06cc\\u0631\\u0627\\u0645\\u06cc\\u062f\\u0648\",\"UG427\":\"\\u06a9\\u0627\\u06af\\u0627\\u062f\\u06cc\",\"UG428\":\"\\u06a9\\u0627\\u06a9\\u0648\\u0645\\u06cc\\u0631\\u0648\",\"UG101\":\"\\u06a9\\u0627\\u0644\\u0646\\u06af\\u0627\\u0644\\u0627\",\"UG222\":\"\\u06a9\\u0627\\u0644\\u06cc\\u0631\\u0648\",\"UG122\":\"\\u06a9\\u0627\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG102\":\"\\u06a9\\u0627\\u0645\\u067e\\u0627\\u0644\\u0627\",\"UG205\":\"\\u06a9\\u0627\\u0645\\u0648\\u0644\\u06cc\",\"UG413\":\"\\u06a9\\u0627\\u0645\\u0648\\u0648\\u0646\\u06af\",\"UG414\":\"\\u06a9\\u0627\\u0646\\u0648\\u0646\\u06af\\u0648\",\"UG206\":\"\\u06a9\\u0627\\u067e\\u0686\\u0648\\u0631\\u0648\\u0627\",\"UG236\":\"\\u06a9\\u0627\\u067e\\u0644\\u0628\\u0628\\u06cc\\u0648\\u0646\\u06af\",\"UG126\":\"\\u06a9\\u0627\\u0633\\u0627\\u0646\\u062f\\u0627\",\"UG406\":\"\\u06a9\\u0627\\u0633\\u06cc\",\"UG207\":\"\\u06a9\\u062a\\u0627\\u06a9\\u0648\\u06cc\",\"UG112\":\"\\u06a9\\u0627\\u06cc\\u0648\\u0646\\u06af\\u0627\",\"UG407\":\"\\u06a9\\u06cc\\u0628\\u0627\\u0644\\u0647\",\"UG103\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06af\\u0627\",\"UG227\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06a9\\u0648\",\"UG432\":\"Kikuube\",\"UG419\":\"\\u06a9\\u06cc\\u0631\\u0648\\u0648\\u0631\\u0627\",\"UG421\":\"\\u06a9\\u06cc\\u0631\\u0627\\u0646\\u062f\\u0648\\u0646\\u06af\\u0648\",\"UG408\":\"\\u06a9\\u06cc\\u0633\\u0648\\u0631\\u0648\",\"UG305\":\"\\u06a9\\u06cc\\u062a\\u06af\\u0648\\u0645\",\"UG319\":\"\\u06a9\\u0648\\u0628\\u0648\\u06a9\\u0648\",\"UG325\":\"\\u06a9\\u0648\\u0644\",\"UG306\":\"\\u06a9\\u0648\\u062a\\u06cc\\u062f\\u0648\",\"UG208\":\"\\u0643\\u0648\\u0645\\u064a\",\"UG333\":\"\\u06a9\\u0648\\u0627\\u0646\\u06cc\\u0627\",\"UG228\":\"\\u06a9\\u0648\\u0646\\u06cc\\u0646\\u06af\",\"UG123\":\"\\u06a9\\u06cc\\u0627\\u0646\\u06a9\\u0648\\u0627\\u0646\\u0632\\u06cc\",\"UG422\":\"\\u06a9\\u06cc\\u06af\\u06af\\u0648\\u0627\",\"UG415\":\"\\u06a9\\u06cc\\u0648\\u0646\\u062c\\u0648\\u062c\\u0648\",\"UG125\":\"\\u06a9\\u06cc\\u0648\\u062a\\u0631\\u0627\",\"UG326\":\"\\u0644\\u0627\\u0645\\u0648\",\"UG307\":\"\\u0644\\u06cc\\u0631\",\"UG229\":\"\\u0644\\u0648\\u0648\\u06a9\\u0627\",\"UG104\":\"\\u0644\\u0648\\u0648\\u0631\\u0648\",\"UG124\":\"\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG114\":\"Lyantonde\",\"UG223\":\"\\u0645\\u0627\\u0646\\u0627\\u0641\\u0648\\u0627\",\"UG320\":\"\\u0645\\u0627\\u0631\\u0627\\u0686\\u0627\",\"UG105\":\"\\u0645\\u0627\\u0633\\u0627\\u06a9\\u0627\",\"UG409\":\"\\u0645\\u0627\\u0633\\u0646\\u062f\\u06cc\",\"UG214\":\"\\u0645\\u0627\\u06cc\\u0648\",\"UG209\":\"\\u0645\\u0628\\u0627\\u0644\\u0647\",\"UG410\":\"\\u0645\\u0628\\u0627\\u0631\\u06a9\\u0647\",\"UG423\":\"\\u0645\\u06cc\\u062a\\u0648\\u0645\\u0627\",\"UG115\":\"\\u0645\\u06cc\\u062a\\u0627\\u0646\\u0627\",\"UG308\":\"\\u0645\\u0631\\u0648\\u0648\\u062a\\u0648\",\"UG309\":\"\\u0645\\u0648\\u0648\\u06cc\",\"UG106\":\"Mpigi\",\"UG107\":\"\\u0645\\u0648\\u0648\\u0628\\u0646\\u062f\",\"UG108\":\"\\u0645\\u0648\\u06a9\\u0648\\u0646\\u0648\",\"UG334\":\"\\u0646\\u0628\\u06cc\\u0644\\u0627\\u062a\\u0648\\u06a9\",\"UG311\":\"\\u0646\\u0627\\u06a9\\u0627\\u067e\\u06cc\\u0631\\u06cc\\u067e\\u06cc\\u0631\\u06cc\\u062a\",\"UG116\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u06a9\\u0647\",\"UG109\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u0648\\u0646\\u06af\\u0644\\u0627\",\"UG230\":\"\\u0646\\u0627\\u0645\\u0627\\u06cc\\u06cc\\u0646\\u06af\\u0648\",\"UG234\":\"\\u0646\\u0627\\u0645\\u06cc\\u0633\\u06cc\\u0646\\u0648\\u0627\",\"UG224\":\"\\u0646\\u0627\\u0645\\u0648\\u062a\\u0648\\u0645\\u0628\\u0627\",\"UG327\":\"\\u0646\\u0627\\u067e\\u0627\\u06a9\",\"UG310\":\"\\u0646\\u0628\\u06cc\",\"UG231\":\"\\u0646\\u06af\\u0648\\u0631\\u0627\",\"UG424\":\"\\u0646\\u062a\\u0648\\u0631\\u0648\\u06a9\\u0648\",\"UG411\":\"\\u0646\\u0627\\u062a\\u0648\\u0646\\u06af\\u0627\\u0645\\u0648\",\"UG328\":\"\\u0646\\u0648\\u06cc\\u0627\",\"UG331\":\"\\u0627\\u0648\\u0645\\u0648\\u0631\\u0648\",\"UG329\":\"\\u0627\\u0648\\u062a\\u0648\\u06a9\\u0647\",\"UG321\":\"\\u0627\\u0648\\u06cc\\u0627\\u0645\",\"UG312\":\"\\u067e\\u062f\\u0631\",\"UG332\":\"\\u067e\\u0627\\u06a9\\u0648\\u0627\\u0686\",\"UG210\":\"\\u067e\\u0627\\u0644\\u06cc\\u0632\\u0627\",\"UG110\":\"\\u0631\\u0627\\u06a9\\u0627\\u06cc\\u06cc\",\"UG429\":\"\\u0631\\u0648\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG425\":\"\\u0631\\u0648\\u0628\\u06cc\\u0631\\u06cc\\u0632\\u06cc\",\"UG431\":\"\\u0631\\u06a9\\u06cc\\u06af\\u0627\",\"UG412\":\"Rukungiri\",\"UG111\":\"\\u0627\\u0633\\u0645\\u0628\\u0644\",\"UG232\":\"\\u0633\\u06cc\\u0631\",\"UG426\":\"\\u0634\\u06cc\\u0645\\u0627\",\"UG215\":\"\\u0633\\u06cc\\u0631\\u0648\\u0646\\u06a9\\u0648\",\"UG211\":\"\\u0633\\u0648\\u0631\\u0648\\u062a\\u06cc\",\"UG212\":\"\\u062a\\u0648\\u0631\\u0648\\u0631\\u0648\",\"UG113\":\"\\u0648\\u0627\\u06a9\\u06cc\\u0633\\u0648\",\"UG313\":\"\\u06cc\\u0648\\u0645\\u0628\",\"UG330\":\"\\u0632\\u0648\\u0645\\u0628\\u0648\"},\"UM\":{\"81\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0628\\u06cc\\u06a9\\u0631\",\"84\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 Howland\",\"86\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u062c\\u0627\\u0631\\u0648\\u06cc\\u0633\",\"67\":\"\\u062c\\u0627\\u0646\\u0633\\u062a\\u0648\\u0646 \\u0627\\u062a\\u0644\",\"89\":\"\\u06a9\\u06cc\\u0646\\u06af\\u0645\\u0646 \\u0631\\u06cc\\u0641\",\"71\":\"Midway Atoll\",\"76\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0646\\u0627\\u0648\\u0627\\u0633\\u0627\",\"95\":\"\\u067e\\u0627\\u0644\\u0645\\u06cc\\u0631\\u0627 \\u0627\\u062a\\u0644\",\"79\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0648\\u06cc\\u06a9\"},\"US\":{\"AL\":\"Alabama\",\"AK\":\"Alaska\",\"AZ\":\"Arizona\",\"AR\":\"Arkansas\",\"CA\":\"California\",\"CO\":\"Colorado\",\"CT\":\"Connecticut\",\"DE\":\"Delaware\",\"DC\":\"District Of Columbia\",\"FL\":\"Florida\",\"GA\":\"Georgia\",\"HI\":\"Hawaii\",\"ID\":\"Idaho\",\"IL\":\"Illinois\",\"IN\":\"Indiana\",\"IA\":\"Iowa\",\"KS\":\"Kansas\",\"KY\":\"Kentucky\",\"LA\":\"Louisiana\",\"ME\":\"Maine\",\"MD\":\"Maryland\",\"MA\":\"Massachusetts\",\"MI\":\"Michigan\",\"MN\":\"Minnesota\",\"MS\":\"Mississippi\",\"MO\":\"Missouri\",\"MT\":\"Montana\",\"NE\":\"Nebraska\",\"NV\":\"Nevada\",\"NH\":\"New Hampshire\",\"NJ\":\"New Jersey\",\"NM\":\"New Mexico\",\"NY\":\"New York\",\"NC\":\"North Carolina\",\"ND\":\"North Dakota\",\"OH\":\"Ohio\",\"OK\":\"Oklahoma\",\"OR\":\"Oregon\",\"PA\":\"Pennsylvania\",\"RI\":\"Rhode Island\",\"SC\":\"South Carolina\",\"SD\":\"South Dakota\",\"TN\":\"Tennessee\",\"TX\":\"Texas\",\"UT\":\"Utah\",\"VT\":\"Vermont\",\"VA\":\"Virginia\",\"WA\":\"Washington\",\"WV\":\"West Virginia\",\"WI\":\"Wisconsin\",\"WY\":\"Wyoming\",\"AA\":\"Armed Forces (AA)\",\"AE\":\"Armed Forces (AE)\",\"AP\":\"Armed Forces (AP)\"},\"VN\":[],\"YT\":[],\"ZA\":{\"EC\":\"Eastern Cape\",\"FS\":\"Free State\",\"GP\":\"Gauteng\",\"KZN\":\"KwaZulu-Natal\",\"LP\":\"Limpopo\",\"MP\":\"Mpumalanga\",\"NC\":\"Northern Cape\",\"NW\":\"North West\",\"WC\":\"Western Cape\"},\"ZM\":{\"ZM-01\":\"\\u063a\\u0631\\u0628\\u06cc\",\"ZM-02\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"ZM-03\":\"\\u0634\\u0631\\u0642\\u06cc\",\"ZM-04\":\"\\u0644\\u0648\\u0622\\u067e\\u0648\\u0644\\u0627\",\"ZM-05\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"ZM-06\":\"\\u0634\\u0645\\u0627\\u0644 \\u063a\\u0631\\u0628\\u06cc\",\"ZM-07\":\"\\u062c\\u0646\\u0648\\u0628\\u06cc\",\"ZM-08\":\"\\u0645\\u0633\\u06cc\",\"ZM-09\":\"\\u0644\\u0648\\u0633\\u0627\\u06a9\\u0627\",\"ZM-10\":\"\\u0645\\u0648\\u0686\\u06cc\\u0646\\u06af\\u0627\"}}","i18n_select_state_text":"\u06cc\u06a9 \u06af\u0632\u06cc\u0646\u0647 \u0627\u0646\u062a\u062e\u0627\u0628 \u0646\u0645\u0627\u0626\u06cc\u062f\u2026","i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631\u2026","i18n_searching":"\u062c\u0633\u062a\u062c\u0648 \u2026"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/country-select.min.js')}}' id='wc-country-select-js'></script>
<script type='text/javascript' id='yith-wcaf-js-extra'>
    /* <![CDATA[ */
    var yith_wcaf = {"labels":{"select2_i18n_matches_1":"One result is available, press enter to select it.","select2_i18n_matches_n":"%qty% results are available, use up and down arrow keys to navigate.","select2_i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","select2_i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","select2_i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","select2_i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","select2_i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631&hellip;","select2_i18n_searching":"\u062c\u0633\u062a\u062c\u0648 &hellip;","link_copied_message":"Url copied"},"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","set_cookie_via_ajax":"","referral_var":"ref","search_products_nonce":"e3b64f2522","set_referrer_nonce":"1b6e71ef82","get_withdraw_amount":"91abc0fe0e"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/yith-wcaf.min.js')}}' id='yith-wcaf-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.blockUI.min.js')}}' id='jquery-blockui-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart.min.js')}}' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/js.cookie.min.js')}}' id='js-cookie-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/cart-fragments.min.js')}}' id='wc-cart-fragments-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/drag-arrange.js')}}' id='dragarrange-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/table-head-fixer.js')}}' id='table-head-fixer-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/perfect-scrollbar.jquery.min.js')}}' id='perfect-scrollbar-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/frontend.js')}}' id='wooscp-frontend-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/popper.min.js')}}' id='negarshop-popper-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap.min.js')}}' id='negarshop-bootstrap-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap-notify.min.js')}}' id='negarshop-bootstrap-notify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/nouislider.min.js')}}' id='negarshop-nouislider-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.confetti.js')}}' id='negarshop-confetti-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/owl.carousel.min.js')}}' id='negarshop-owl-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/lightgallery.min.js')}}' id='negarshop-lightbox-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/printThis.min.js')}}' id='negarshop-print-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/accounting.min.js')}}' id='negarshop-accounting-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/select2.js')}}' id='negarshop-select2-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/tab-carousel.js')}}' id='negarshop-ajax-tab-carousel-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/variable_product_data.js')}}' id='negarshop-var-product-js'></script>
<script type='text/javascript' src='{{'newFront/js/underscore.min.js'}}' id='underscore-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/wp-util.min.js')}}' id='wp-util-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart-variation.min.js')}}' id='wc-add-to-cart-variation-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/script.js')}}' id='negarshop-script-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/price-changes.js')}}' id='negarshop-cb-change-price-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/product-3d.js')}}' id='negarshop-three-int-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify.js')}}' id='negarshop-magnify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify-mobile.js')}}' id='negarshop-magnify-mobile-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.countdown.min.js')}}' id='negarshop-countdown-js-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/yith-color-atts.js')}}' id='yith_wccl_frontend_cb-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnific-popup.min.js')}}' id='dokan-popup-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jed.js')}}' id='dokan-i18n-jed-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/login-form-popup.js')}}' id='dokan-login-form-popup-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/wp-embed.min.js')}}' id='wp-embed-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/frontend-modules.min.js')}}' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/dialog.min.js')}}' id='elementor-dialog-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/waypoints.min.js')}}' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/swiper.min.js')}}' id='swiper-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/share-link.min.js')}}' id='share-link-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u0641\u06cc\u0633\u200c\u0628\u0648\u06a9","shareOnTwitter":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u062a\u0648\u06cc\u06cc\u062a\u0631","pinIt":"\u0633\u0646\u062c\u0627\u0642 \u06a9\u0646","download":"\u062f\u0631\u06cc\u0627\u0641\u062a","downloadImage":"\u062f\u0627\u0646\u0644\u0648\u062f \u062a\u0635\u0648\u06cc\u0631","fullscreen":"\u062a\u0645\u0627\u0645 \u0635\u0641\u062d\u0647","zoom":"\u0628\u0632\u0631\u06af\u0646\u0645\u0627\u06cc\u06cc","share":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc","playVideo":"\u067e\u062e\u0634 \u0648\u06cc\u062f\u06cc\u0648","previous":"\u0642\u0628\u0644\u06cc","next":"\u0628\u0639\u062f\u06cc","close":"\u0628\u0633\u062a\u0646"},"is_rtl":true,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.14","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"body_background_background":"classic","global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":1768,"title":"%D8%B5%D9%81%D8%AD%D9%87%20%D9%86%D8%AE%D8%B3%D8%AA%20-%20%D8%B9%D9%85%D9%88%D9%85%DB%8C%202-%D8%A7%D9%84%D9%85%D9%86%D8%AA%D9%88%D8%B1%20-%20%D9%86%DA%AF%D8%A7%D8%B1%D8%B4%D8%A7%D9%BE","excerpt":"","featuredImage":false}};
</script>
<script type='text/javascript' src='{{asset('newFront/js/frontend.min.js')}}' id='elementor-frontend-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/element-moment.js')}}' id='script-negarshop_moment-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/imagesloaded.min.js')}}' id='imagesloaded-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/forms.min.js')}}' id='mc4wp-forms-api-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.sticky.min.js')}}' id='elementor-sticky-js'></script>

<script type='text/javascript' src='{{asset('newFront/js/frontend.min.js')}}' id='elementor-pro-frontend-js'></script>

</body>
</html>
