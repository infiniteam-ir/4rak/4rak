@extends('front.newTheme.master')
@section('title','بلاگ')
@section('main')
    <section class="fw-main-row  d-block d-xl-block d-lg-block d-md-block d-sm-block  ">
        <div class="container">
{{--            @if($top_right_article != null && count($top_mid_articles)>0 && count($top_left_articles)>0)--}}
            <div class="row">
                <div class="col-lg">
                    <section class="content-widget tile-posts transparent template-1" id="cw-tile-posts-7f64e34bed80388fcdd316d0ba8bfe0e">
                        <div class="row">
                            {{--TOP-RIGHT-POST--}}
                            @if($top_right_article != null)
                            <div class="col-12 col-md-4 col-lg-4 col-xl-5 items-1">
                                <article class="blog-item">
                                    <figure class="figure">
                                        @if($top_right_article->article->photo)
                                            <img data-src="{{asset('images/articles/'.$top_right_article->article->photo->path)}}" alt="{{$top_right_article->article->title}}" class="lazy">
                                        @else
                                            <img data-src="{{asset('img/default.jpg')}}" alt="{{$top_right_article->article->title}}" class="lazy">
                                        @endif
                                        <figcaption class="figure-caption">
                                            <a href="{{route('article',$top_right_article->article->id)}}">
                                                <h4>{{$top_right_article->article->title}}</h4>
                                            </a>
                                            <div class="info">
                                                <ul>
                                                    <i class="far fa-user"></i>
                                                    <li class="author">
                                                        <span>{{$top_right_article->article->author->name}} {{$top_right_article->article->author->family}}</span>
                                                    </li>
                                                    <li><i class="far fa-calendar"></i><span>
                                                            <time>{{verta($top_right_article->article->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::now())}}</time>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </article>
                            </div>
                            @endif
                            {{--end-TOP-RIGHT-POST--}}

                            {{--TOP-mid-POST--}}
                            @if(count($top_mid_articles)>0)
                            <div class="col-12 col-md-4 col-lg-4 col-xl-4 items-2">
                                @foreach($top_mid_articles as $item)
                                    <article class="blog-item">
                                        <figure class="figure">
                                            @if($item->article->photo)
                                                <img data-src="{{asset('images/articles/'.$item->article->photo->path)}}" alt="{{$item->article->title}}" class="lazy">
                                            @else
                                                <img data-src="{{asset('img/default.jpg')}}" alt="{{$item->article->title}}" class="lazy">
                                            @endif
                                            <figcaption class="figure-caption">
                                                <a href="{{route('article',$item->article->id)}}">
                                                    <h4>{{$item->article->title}}</h4></a>
                                                <div class="info">
                                                    <ul>
                                                        <li class="author">
                                                            <i class="far fa-user"></i>
                                                            <span>{{$item->article->author->name}} {{$item->article->author->family}}</span>
                                                        </li>
                                                        <li><i class="far fa-calendar"></i><span>
                                                                <time>{{verta($item->article->created_at)->formatDifference(Verta::now()) }}</time></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                @endforeach

                            </div>
                            @endif
                            {{--END-TOP-mid-POST--}}


                            {{--TOP-LEFT-POST--}}
                            @if(count($top_left_articles)>0)
                            <div class="col-12 col-md-4 col-lg-4 col-xl-3 items-3">
                                @foreach($top_left_articles as $item)
                                    <article class="blog-item">
                                        <figure class="figure">
                                            @if($item->article->photo)
                                                <img data-src="{{asset('images/articles/'.$item->article->photo->path)}}" alt="{{$item->article->title}}" class="lazy">
                                            @else
                                                <img data-src="{{asset('img/default.jpg')}}" alt="{{$item->article->title}}" class="lazy">
                                            @endif
                                            <figcaption class="figure-caption">
                                                <a href="{{route('article',$item->article->id)}}">
                                                    <h4>{{$item->article->title}}</h4></a>
                                                <div class="info">
                                                    <ul>
                                                        <li class="author">
                                                            <i class="far fa-user"></i>
                                                            <span>{{$item->article->author->name}} {{$item->article->author->family}}</span>
                                                        </li>
                                                        <li><i class="far fa-calendar"></i><span>
                                                           <time>{{verta($item->article->created_at)->formatDifference(Verta::now()) }}</time>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </article>
                                @endforeach
                            </div>
                            @endif
                            {{--END-TOP-LEFT-POST--}}
                        </div>
                    </section>
                </div>
            </div>


            <div class="row">
                <div class="col-lg">
                    <div class="fw-divider-space  d-block d-xl-block d-lg-block d-md-block d-sm-block"
                         style="margin-top: 40px;">

                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="fw-main-row  d-block d-xl-block d-lg-block d-md-block d-sm-block  ">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    @if(count($articles)>0)
                    <section class="content-widget blog-home transparent style-2" id="content-widget-b72ae18cf1808ee3b1d015873ba3e773">
                     @foreach($articles as $article)
                            <article
                                class="post-item post-63 post type-post status-publish format-standard has-post-thumbnail hentry category-16 tag-fortnite"
                                id="post-63">
                                <figure class="post-thumb">
                                    <a href="{{route('article',$article->id)}}">
                                        @if($article->photo)
                                        <img data-src="{{asset('images/articles/'.$article->photo->path)}}" alt="{{$article->title}}" class="lazy">
                                        @else
                                            <img data-src="{{asset('img/default.jpg')}}" alt="{{$article->title}}" class="lazy">
                                        @endif
                                    </a>
                                </figure>
                                <div class="title">
                                    <a href="{{route('article',$article->id)}}">
                                        <h2 class="title-tag">{{$article->title}}</h2>
                                    </a>
                                </div>
                                <div class="excerpt">
                                    <?php echo Str::limit($article->content,'50','...');?>
                                </div>
                                <div class="info">
                                    <ul>
                                        <li><i class="fal fa-user"></i><span>{{$article->author->name}} {{$article->author->family}}</span></li>
                                        <li><i class="fal fa-calendar"></i><span>{{verta($article->created_at)->format('%B %d، %Y')}}</span></li>
                                        <li><i class="fal fa-archive"></i>
                                    <span>
                                       <ul class="post-categories">
	                                     <li>
                                            <a href="#" rel="category tag">{{$article->category->title}}</a>
                                          </li>
                                      </ul>
                                   </span>
                                 </li>
                                    </ul>
                                </div>
                            </article>
                        @endforeach

                         <div class="row mt-3 d-flex justify-content-center mt-3">{{$articles->links()}}</div>

                    </section>
                    @else
                        <div class="alert py-5 text-center">
                            پستی جهت نمایش وجود ندارد.
                        </div>
                    @endif
                </div>
                <div class="col-lg-3">
                    <div class="shortcode-widget-area darken-color-mode">
                        <section id="recent-comments-4" class="widget widget_recent_comments">
                            <header class="wg-header"><h6>آخرین دیدگاه‌ها</h6></header>
                            @if(count($comments)>0)
                            <ul id="recentcomments-4">
                                @foreach($comments as $comment)
                                    <li class="recentcomments">
                                        <span class="comment-author-link">{{$comment->user->name}} {{$comment->user->family}}</span> در
                                        <a href="{{route('article',$comment->groupable->id)}}">{{Str::limit($comment->groupable->title,'20','...')}}</a>
                                    </li>
                                @endforeach
                            </ul>
                            @else
                                <h6 class="text-muted has-small-font-size">دیدگاهی جهت نمایش وجود ندارد</h6>
                            @endif
                        </section>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="fw-main-row mt-5 d-block d-xl-block d-lg-block d-md-block d-sm-block  ">
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <section class="content-widget blog-posts style-1"
                             id="content-widget-de2aa13de3171780a533005acfb00708">
                        <div class="owl-carousel blog-posts" id="blog-posts-de2aa13de3171780a533005acfb00708">
                            @foreach($slider_articles as $article)
                            <div>
                                <article class="blog-item">
                                    <a href="{{route('article',$article->id)}}">
                                        <figure class="thumbnail">
                                            @if($article->photo)
                                                <img data-src="{{asset('images/articles/'.$article->photo->path)}}" alt="{{$article->title}}" class="lazy">
                                            @else
                                                <img data-src="{{asset('img/default.jpg')}}" alt="{{$article->title}}" class="lazy">
                                            @endif
                                            <div class="post-format">
                                                <i class="fal fa-file-alt"></i></div>
                                            <time>
                                                <span class="day">{{verta($article->created_at)->day}}</span>
                                                <span class="month">{{verta($article->created_at)->format('%B')}}</span>
                                            </time>
                                        </figure>
                                    </a>
                                    <div class="item-footer">
                                        <a href="{{route('article',$article->id)}}">
                                            <h3 class="title">{{$article->title}}</h3>
                                        </a>
                                        <div class="cats">
                                            <a href="#" rel="category tag">{{$article->category->name}}</a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                                @endforeach
                        </div>
                        <style>
                            #content-widget-header-de2aa13de3171780a533005acfb00708 a.archive {
                                background-color: #00bfd6;
                            }

                            #content-widget-de2aa13de3171780a533005acfb00708 article.item figcaption time {
                                background-color: #00bfd6;
                            }
                        </style>
                        <script>
                            jQuery(document).ready(function ($) {
                                $('#blog-posts-de2aa13de3171780a533005acfb00708').owlCarousel({
                                    items: 4,
                                    autoplay: true,
                                    rtl: true,
                                    nav: false,
                                    loop: true,
                                    dots: true,
                                    responsive: {
                                        0: {
                                            items: 1,
                                        },
                                        480: {
                                            items: 2,
                                        },
                                        700: {
                                            items: 2,
                                        },
                                        991: {
                                            items: 4,
                                        },
                                    },
                                    margin: 15
                                });
                            });
                        </script>
                    </section>
                </div>
            </div>

        </div>
    </section>
@endsection

