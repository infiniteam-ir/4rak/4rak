@extends('front.newTheme.master')
@php($title='بلاگ'.' | '.$article->title)
@section('title',$title)
@section('main')
    <main id="main">
        <div class="container">
            <div class="row">
                <div class="col-lg ">
                    <section class="blog-home">
                        <article class="post-item post-63 post type-post status-publish format-standard has-post-thumbnail hentry category-16 tag-fortnite" id="post-63">
                            <figure class="post-thumb">
                                <a href="{{route('article',$article->id)}}">
                                    @if($article->photo)
                                    <img width="822" height="522" src="{{asset('images/articles/'.$article->photo->path)}}" class="attachment-large size-large wp-post-image" alt="" loading="lazy" srcset="{{asset('images/articles/'.$article->photo->path)}} 822w, {{asset('images/articles/'.$article->photo->path)}} 300w, {{asset('images/articles/'.$article->photo->path)}} 768w, {{asset('images/articles/'.$article->photo->path)}} 472w, {{asset('images/articles/'.$article->photo->path)}} 400w, {{asset('images/articles/'.$article->photo->path)}} 50w, {{asset('images/articles/'.$article->photo->path)}} 600w" sizes="(max-width: 822px) 100vw, 822px" />
                                @else
                                    <img width="822" height="522" src="{{asset('img/default.jpg')}}" class="attachment-large size-large wp-post-image" alt="" loading="lazy" srcset="{{asset('img/default.jpg')}} 822w, {{asset('img/default.jpg')}} 300w, {{asset('img/default.jpg')}} 768w, {{asset('img/default.jpg')}} 472w, {{asset('img/default.jpg')}} 400w, {{asset('img/default.jpg')}} 50w, {{asset('img/default.jpg')}} 600w" sizes="(max-width: 822px) 100vw, 822px" />
                                @endif
                                </a>
                            </figure>
                            <div class="title">
                                <a href="{{route('article',$article->id)}}">
                                    <h1 class="title-tag">{{$article->title}}</h1>
                                </a>
                            </div>
                            <div class="content">
                                <p>
                                    <?php echo $article->content ?>
                                </p>
                            </div>
                            @if($article->keywords)
                            <div class="tags">برچسب‌ها:
                                <a href="#" rel="tag">{{$article->keywords}}</a>
                            </div>
                            @endif
                            <div class="info">
                                <ul>
                                    <li><i class="fal fa-user"></i><span>{{$article->author->name}} {{$article->author->family}}</span></li>
                                    <li><i class="fal fa-calendar"></i><span>{{verta($article->created_at)->format('%B %d، %Y')}}</span></li>
                                    <li><i class="fal fa-archive"></i>
                                        <span>
                                            <ul class="post-categories">
	                                <li>
                                        <a href="{{route('blogCategory',[$article->category->title,$article->category->id])}}" rel="category tag">{{$article->category->title}}</a></li>
                                            </ul>
                                        </span>
                                    </li>
                                    <li><i class="fal fa-eye"></i><span>1671 بازدید</span></li>
                                    @livewire('like-article',['article'=>$article])
                                </ul>
                            </div>
                        </article>
                        <div class="post-comments post-wg">
                            <div id="comments" class="comments-area">
                                @if(count($article_comments)>0)
                                <h2 class="comments-title">
                                    <i class="fal fa-comment"></i>
                                    {{count($article_comments)}} دیدگاه برای &ldquo;{{$article->title}}&rdquo;
                                </h2>
                                <ol class="comment-list">
                                    @foreach($article_comments as $comment)
                                    <li id="comment-2" class="comment even thread-even depth-1">
                                        <article id="div-comment-2" class="comment-body">
                                            <footer class="comment-meta">
                                                <div class="comment-author vcard">
                                                    @if($comment->user->photo)
                                                    <img alt='' src='{{asset('images/'.$comment->user->photo->path)}}' srcset='{{asset('images/'.$comment->user->photo->path)}}' class='avatar avatar-100 photo' height='100' width='100' loading='lazy'/>
                                                    @else
                                                        <img alt='' src='{{asset('img/user-avatar.svg')}}' srcset='{{asset('img/user-avatar.svg')}}' class='avatar avatar-100 photo' height='100' width='100' loading='lazy'/>
                                                    @endif
                                                    <b class="fn">{{$comment->user->name}} {{$comment->user->family}}</b> <span class="says">گفت:</span>
                                                </div>
                                                <!-- .comment-author -->

                                                <div class="comment-metadata">
                                                    <a href=""><time datetime="{{$comment->created_at}}">{{verta($comment->created_at)->format('%B %d، %Y')}} در
                                                        {{verta($comment->created_at)->formattime()}}</time></a>
                                                </div><!-- .comment-metadata -->

                                            </footer><!-- .comment-meta -->

                                            <div class="comment-content">
                                                <p>{{$comment->content}}</p>
                                            </div><!-- .comment-content -->

                                            <div class="reply">
                                                <a rel='nofollow' class='comment-reply-link' href='#comment-2' data-commentid="2" data-postid="63" data-belowelement="div-comment-2" data-respondelement="respond" data-replyto="پاسخ به {{$comment->user->name}}" aria-label='پاسخ به {{$comment->user->name}}'>پاسخ دادن</a>
                                            </div>
                                        </article><!-- .comment-body -->
                                    </li>
                                @endforeach

                                    <!-- #comment-## -->
                                </ol>
                                @endif
                                @auth
                                        <div id="respond" class="comment-respond">
                                            <h3 id="reply-title" class="comment-reply-title">دیدگاهتان را بنویسید
                                                <!--            <small>
                                                                <a rel="nofollow" id="cancel-comment-reply-link" href="" style="display:none;">لغو پاسخ</a>
                                                            </small>-->
                                            </h3>
                                                @livewire('article-comment',['post_id'=>$article->id])
                                        </div>
                                @endauth
                                    <!-- #respond -->
                            </div>
                            <!-- #comments -->
                        </div>
                    </section>
                </div>
                <div class="col-lg-auto">
                    <aside id="blog-sidebar" class="sidebar mt-xl-0 mt-lg-0 mt-md-3 mt-sm-3 mt-3">
                        <section id="recent-comments-4" class="widget widget_recent_comments">
                            <header class="wg-header"><h6>آخرین دیدگاه‌ها</h6></header>
                            <ul id="recentcomments">
                                @foreach($comments as $comment)
                                    <li class="recentcomments">
                                        <span class="comment-author-link">{{$comment->user->name}} {{$comment->user->family}}</span> در
                                        <a href="{{route('article',$comment->groupable->id)}}">{{Str::limit($comment->groupable->title,'20','...')}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </section>
                    </aside>
                </div>
            </div>
        </div>

    </main>
@endsection
@section('script')
    <script>
        window.addEventListener('success', event => {
            alert('دیدگاه شما با موفقیت ثبت شد');
        })
    </script>
@endsection

