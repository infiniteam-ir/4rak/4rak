<!DOCTYPE html>
<html dir="rtl" lang="fa-IR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>{{$shop->name}} | چارک </title>
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="https://4rak.ir/" />
    <meta property="og:locale" content="fa_IR" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content=" چارک{{$shop->name}}" />
    <meta property="og:description" content="{{$shop->description}} [&hellip;]" />
    <meta property="og:url" content="https://4rak.ir" />
    <meta property="og:site_name" content="چارک" />
    <meta property="article:modified_time" content="2020-09-26T12:06:12+00:00" />
    <meta property="og:image" content="{{asset('img/4rak-logo.png')}}" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:label1" content="زمان تقریبی برای خواندن">
    <meta name="twitter:data1" content="0 دقیقه">
    <!-- / Yoast SEO plugin. -->


    <link rel='dns-prefetch' href='//s.w.org' />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.coderboy.ir\/negarshop\/wp-includes\/js\/wp-emoji-release.min.js"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='dashicons-css'  href='{{asset('newFront/css/dashicons.min.css')}}' type='text/css' media='all' />
    <style id='dashicons-inline-css' type='text/css'>
        [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='wp-block-library-rtl-css'  href='{{asset('newFront/css/style-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{asset('newFront/css/vendors-style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-rtl-css'  href='{{asset('newFront/css/style-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcaf-css'  href='{{asset('newFront/css/yith-wcaf.css')}}' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='hint-css'  href='{{asset('newFront/css/hint.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-css'  href='{{asset('newFront/css/perfect-scrollbar.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-wpc-css'  href='{{asset('newFront/css/custom-theme.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-flaticon-css'  href='{{asset('newFront/fonts/flaticon/flaticon.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-fontawesome-css'  href='{{asset('newFront/fonts/fontawesome/fa-all.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-iransans-css'  href='{{asset('newFront/fonts/iransans/iransans.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-bootstrap-css'  href='{{asset('newFront/css/bootstrap.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-nouislider-css'  href='{{asset('newFront/css/nouislider.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owl-css'  href='{{asset('newFront/css/owl.carousel.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owltheme-css'  href='{{asset('newFront/css/owl.theme.default.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-lightbox-css'  href='{{asset('newFront/css/lightgallery.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-compare-css'  href='{{asset('newFront/css/compare-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-magnify-css'  href='{{asset('newFront/css/magnify.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-style-css'  href='{{asset('newFront/css/core.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-builder-frontend-grid-css'  href='{{asset('newFront/css/frontend-grid.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-forms-default-styles-css'  href='{{asset('newFront/css/frontend.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='{{asset('newFront/css/font-awesome.min.css')}}' type='text/css' media='all' />
    <style id='font-awesome-inline-css' type='text/css'>
        [data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='elementor-icons-css'  href='{{asset('newFront/css/elementor-icons.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'  href='{{asset('newFront/css/animations.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-legacy-css'  href='{{asset('newFront/css/frontend-legacy-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1558-css'  href='{{asset('newFront/css/post-1558.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-2019-css'  href='{{asset('newFront/css/post-2019.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-2023-css'  href='{{asset('newFront/css/post-2023.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-2039-css'  href='{{asset('newFront/css/post-2039.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-2188.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-style-css'  href='{{asset('newFront/css/style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-rtl-style-css'  href='{{asset('newFront/css/rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='premmerce-brands-css'  href='{{asset('newFront/css/premmerce-brands.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.6' type='text/css' media='all' />

    <script type='text/javascript' src='{{asset('newFront/js/jquery.min.js')}}' id='jquery-core-js'></script>
{{--    <script type='text/javascript' src='{{asset('newFront/js/elementor_fixes.js')}}' id='script-ns_elementor_fixes-js'></script>--}}
{{--    <link rel="https://api.w.org/" href="https://demo.coderboy.ir/negarshop/wp-json/" /><link rel="alternate" type="application/json" href="https://demo.coderboy.ir/negarshop/wp-json/wp/v2/pages/2019" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demo.coderboy.ir/negarshop/xmlrpc.php?rsd" />--}}
{{--    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demo.coderboy.ir/negarshop/wp-includes/wlwmanifest.xml" />--}}
{{--    <meta name="generator" content="WordPress 5.6" />--}}
{{--    <meta name="generator" content="WooCommerce 4.8.0" />--}}
{{--    <link rel='shortlink' href='https://demo.coderboy.ir/negarshop/?p=2019' />--}}
{{--    <link rel="alternate" type="application/json+oembed" href="https://demo.coderboy.ir/negarshop/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.coderboy.ir%2Fnegarshop%2Fminimal%2F" />--}}
{{--    <link rel="alternate" type="text/xml+oembed" href="https://demo.coderboy.ir/negarshop/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.coderboy.ir%2Fnegarshop%2Fminimal%2F&#038;format=xml" />--}}
    <meta name="theme-color" content="#666666"/><style>body{background-color: #ffffff;}html, body, .flip-clock-a *, .tooltip, #orders_statistics_chart_container, #orders_statistics_chart_container *{font-family: IRANSans_Fa;}.content-widget article.item figure, article.product figure.thumb, article.w-p-item figure{background-image: url(https://demo.coderboy.ir/negarshop/wp-content/themes/negarshop/statics/images/product-placeholder.png);}/* ------------------------- Normal ------------------------- */.content-widget.slider-2.style-2 .product-info .feature-daels-price .sale-price, .ns-add-to-cart-inner .price-update, article.product div.title a:hover, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .woocommerce-LostPassword a, .ns-checkbox input[type="radio"]:checked + label::before, section.blog-home article.post-item.post .content a, a:hover, .header-account .account-box:hover .title, .header-main-nav .header-main-menu > ul > li.loaded:hover>a, section.widget ul li a:hover, .content-widget.slider-2.style-2 .product-info .footer-sec .finished, .content-widget.slider-2.style-2 .product-info .static-title span, article.w-p-item .info > .price span span, article.w-p-item .info > .price span.amount, section.blog-home article.post-item .title .title-tag:hover, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-tabs.wc-tabs-wrapper ul.wc-tabs li.active a, .woocommerce.single-product .sale-timer .right .title span, .offer-moments .owl-item .price ins, .offer-moments .owl-item .price span.amount, .page-template-amazing-offer article.product .price ins, .ns-checkbox input[type="checkbox"]:checked+label::before, .content-widget.products-carousel.tabs ul.tabs li.active a, .product .product-sales-count i, .cb-comment-tabs .cb-tabs .active, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a{color: #666666;}.product-add-to-cart-sticky .add-product, .negarshop-countdown .countdown-section:last-of-type::before, .select_option.select_option_colorpicker.selected,.select_option.select_option_label.selected, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit:hover, .content-widget.slider.product-archive .wg-title, .lg-actions .lg-next:hover, .lg-actions .lg-prev:hover,.lg-outer .lg-toogle-thumb:hover, .lg-outer.lg-dropdown-active #lg-share,.lg-toolbar .lg-icon:hover, .lg-progress-bar .lg-progress, .dokan-progress>.dokan-progress-bar, #negarshop-to-top>span, .header-search .search-box .action-btns .action-btn.search-submit::after, .select2-container--default .select2-results__option--highlighted[aria-selected], .header-account .account-box:hover .icon, .header-main-nav .header-main-menu > ul > li.loaded:hover>a::after, .btn-negar, .content-widget.slider-2.style-2 .carousel-indicators li.active, .btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled , .navigation.pagination .nav-links a:hover, .woocommerce-message a:active, .woocommerce .onsale, header.section-header a.archive, .woocommerce.single-product .sale-timer .left .discount span, .woocommerce-pagination ul li a:hover , .ui-slider .ui-slider-range, .switch input:checked + .slider, .woocommerce .quantity.custom-num span:hover, .content-widget.slider-2.style-2 .carousel-inner .carousel-item .discount-percent, .sidebar .woocommerce-product-search button, .product-single-ribbons .ribbons>div>span, .content-widget.products-carousel.tabs ul.tabs li a::after, .cb-nouislider .noUi-connect, .comment-users-reviews .progress .progress-bar, .cb-comment-tabs .cb-tabs a::after, .ns-table tbody td.actions a.dislike_product:hover, .ns-store-header .nav-pills .nav-item a.active, .ns-store-avatar header.store-avatar-header{background-color: #666666;}.content-widget.slider.product-archive .wg-title, .lg-outer .lg-thumb-item.active, .lg-outer .lg-thumb-item:hover,.btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled, .ui-slider span, .ns-store-avatar header.store-avatar-header .avatar{border-color: #666666;}.spinner, nav#main-menu li.loading>a::after{border-top-color: #666666;}.content-widget.slider-2.style-2 .carousel-indicators li::before{border-right-color: #666666;}.content-widget.slider.product-archive .slide-details .prd-price, .woocommerce-variation-price, .woocommerce p.price > span, .woocommerce p.price ins,.table-cell .woocommerce-Price-amount,#order_review span.amount{color: #666666;}/* ------------------------- Importants ------------------------- */.woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .product-section .sale-timer-box, .btn-transparent, .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a{color: #666666 !important;}.dokan-btn-theme, .wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn{background-color: #666666 !important;}.woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .dokan-btn-theme{border-color: #666666 !important;}.cls-3{fill: #666666;}.negarshop-countdown .countdown-section:last-of-type{background: rgba(102,102,102,0.2);color: #666666;}/* ------------------------- Customs ------------------------- */.woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected, .woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected:hover {box-shadow: 0 0 0 2px #666666 !important;}.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a{background-color: rgba(102,102,102,0.1) !important;}/*-------------basket----------------*/.header-cart-basket .cart-basket-box::before, .header-cart-basket.style-2 .cart-basket-box{background-color: rgba(0, 191, 214, 0.1);}.header-cart-basket .cart-basket-box, .header-cart-basket.style-2 .cart-basket-box{color: #666666;}.header-cart-basket .cart-basket-box > span.count, .header-cart-basket.style-2 .cart-basket-box > span.count{background-color: #666666;color: #fff;}.header-cart-basket > .widget.widget_shopping_cart ul li a.remove:hover{background-color: #666666;}.header-cart-basket > .widget.widget_shopping_cart p.total{color: #666666;background-color: rgba(102,102,102,0.1);}.header-cart-basket > .widget.widget_shopping_cart .buttons .button:hover{color: #666666;}.header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout:hover{background-color: #666666;}article.product figure.thumb, .negarshop-countdown .countdown-section:last-of-type::before, .negarshop-countdown .countdown-section, #negarshopAlertBox, #negarshopAlertBox #closeBtn, .product-alerts .alert-item, .select_option, .select_option *, .product-single-actions, .cb-chips ul.chip-items li, .select2-container--default .select2-selection--single, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit, ul.dokan-account-migration-lists li, .product-summary-left, .header-main-menu.vertical-menu ul.main-menu > li:hover, article.w-p-item, article.w-p-item figure, .img-banner-wg, .btn, .form-control, .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-append > .btn, .input-group-sm > .input-group-append > .input-group-text, .input-group-sm > .input-group-prepend > .btn, .input-group-sm > .input-group-prepend > .input-group-text,#negarshop-to-top > span i,section.widget:not(.widget_media_image), .dokan-widget-area aside.widget:not(.widget_media_image),.content-widget:not(.transparent),.woocommerce.single-product div.product > .product-section, nav.woocommerce-breadcrumb, .woocommerce.single-product div.product .woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel, .dokan-other-vendor-camparison,.woocommerce .title-bg,.product-single-actions li a,.woocommerce .quantity.custom-num input.input-text,.woocommerce .quantity.custom-num span,.table-gray,.comment-form-header .comment-notes,.cb-nouislider .noUi-connects,input[type="submit"]:not(.browser-default), input[type="button"]:not(.browser-default), input[type="reset"]:not(.browser-default), .btn, .dokan-btn,article.product,.colored-dots .dot-item,.woocommerce.single-product div.product .product-section.single-style-2-gallery .owl-carousel.wc-product-carousel .owl-item .car-dtag,.owl-carousel.wc-product-carousel .owl-nav button,.owl-carousel.wc-product-carousel-thumbs .owl-item,.account-box .account-links,.account-box .account-links > li a,.header-main-nav .header-main-menu li > ul,.is-mega-menu-con.is-product-mega-menu .tabs a.item-hover,figure.optimized-1-1,.is-mega-menu-con.is-product-mega-menu .owl-carousel .owl-nav button,.content-widget.slider.product-archive figure.thumb,.content-widget.slider .carousel .carousel-indicators li,ul li.wc-layered-nav-rating a,.switch .slider,.switch .slider::before,.woocommerce-products-header,.price_slider_amount button.button,.modal-content, #cbQVModalCarousel .carousel-item,.woocommerce-pagination ul li a, .woocommerce-pagination ul li span,.tile-posts article.blog-item,section.blog-home article.post-item,section.blog-home article.post-item figure.post-thumb,section.blog-home article.post-item .entry-video .inner,.navigation.pagination .nav-links > *,.content-widget.blog-posts article.blog-item figure.thumbnail,.content-widget.blog-posts article.blog-item time,.content-widget.blog-posts article.blog-item,.content-widget.blog-posts .owl-dots button,section.blog-home .post-wg,section.blog-home .post-wg ol.comment-list article.comment-body,section.blog-home article.post-item .tags a,blockquote,nav.top-bar li > ul ,.header-search .search-box .search-result,.product-section .sale-timer-box, .product-section .sale-timer-box .counter-sec,.product-section .sale-timer-box .title .badge,.header-cart-basket.style-2 .cart-basket-box,.header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li a,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li,.woocommerce nav.woocommerce-MyAccount-navigation ul li a,.woocommerce-message, .woocommerce-error,.woocommerce-message a, .woocommerce-page.woocommerce-cart table.shop_table td.product-remove a.remove, .cart-collaterals .cart_totals,ul#shipping_method li label,.list-group,ul.woocommerce-order-overview, .header-cart-basket > .widget.widget_shopping_cart > .widget_shopping_cart_content,.header-cart-basket > .widget.widget_shopping_cart,footer.site-footer .about-site,ul.product-categories li.open .children,.wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn,.woocommerce-tabs.wc-tabs-wrapper > div.woocommerce-Tabs-panel .product-seller .store-avatar img,.ns-table tbody td.actions a.dislike_product,.is-mega-menu-con.is-product-mega-menu li.contents,footer.site-footer .support-times,li.recentcomments,section.widget.widget_media_image img,.header-main-menu.vertical-menu ul.main-menu,.prm-brands-list__item,.prm-brands-list__item .prm-brands-list__title,.content-widget.slider .carousel,.content-widget.title-widget span.icon,.products-carousel .carousel-banner,footer.site-footer .footer-socials ul li a,header.site-header .header-socials ul li a,.header-search .search-box .action-btns{-webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px;}.product-video-carousel .owl-nav button, .input-group .form-control:not(select), textarea:not(.browser-default), .dokan-form-control, .form-control, input[type="text"]:not(.browser-default), input[type="search"]:not(.browser-default), input[type="email"]:not(.browser-default), input[type="url"]:not(.browser-default), input[type="number"]:not(.browser-default) ,.input-group .btn,.dokan-message, .dokan-info, .dokan-error{-webkit-border-radius: 8px !important; -moz-border-radius: 8px !important; border-radius: 8px !important;}.header-search.style-3 .search-box .action-btns {border-radius: 8px 0 0 8px;}.woocommerce-account:not(.logged-in) article.post-item .nav-pills, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .ns-store-avatar header.store-avatar-header,.woocommerce-page.woocommerce-cart table.shop_table tr:first-of-type td.product-subtotal{border-radius: 8px 8px 0 0;}.ns-store-avatar.wc-dashboard .user-actions a,.woocommerce-page.woocommerce-cart table.shop_table tr:nth-last-of-type(2) td.product-subtotal{border-radius: 0 0 8px 8px;}.ns-table tbody tr td:first-of-type {border-radius: 0 8px 8px 0;}.ns-table tbody tr td:last-of-type {border-radius: 8px 0 0 8px;}.comment .comment-awaiting-moderation::before{content: 'در انتظار تایید مدیریت'} .woocommerce-variation-price:not(:empty)::before{content: 'قیمت: '} .woocommerce-pagination ul li a.next::before{content: 'بعدی'} .woocommerce-pagination ul li a.prev::before{content: 'قبلی'} .woocommerce .quantity.custom-num label.screen-reader-text::before{content: 'تعداد: '} .yith-woocompare-widget ul.products-list li .remove::after{content: 'حذف'} .woocommerce .product .product_meta > span.product-brand::before{content: 'برند: '} .show-ywsl-box::before{content: 'برای ورود کلیک کنید'} a.reset_variations::before{content: 'پاک کردن ویژگی ها'} .woocommerce form .form-row .required::before{content: '(ضروری)'} .content-widget.price-changes .prices-table tbody td.past-price::before{content: 'قیمت قبل: '} .content-widget.price-changes .prices-table tbody td.new-price::before{content: 'قیمت جدید: '} .content-widget.price-changes .prices-table tbody td.changes::before{content: 'تغییرات: '} .content-widget.price-changes .prices-table tbody td.difference::before{content: 'مابه التفاوت: '} </style><script type='text/javascript'>var jsVars = {"borderActiveColor":"#666666"};</script>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link rel="icon" href="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/Inipagi-Business-Economic-Store.ico" sizes="32x32" />
    <link rel="icon" href="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/Inipagi-Business-Economic-Store.ico" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/Inipagi-Business-Economic-Store.ico" />
    <meta name="msapplication-TileImage" content="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/Inipagi-Business-Economic-Store.ico" />
    <style type="text/css" id="wp-custom-css">
        .baby-newspaper .elementor-widget-container .widget_mc4wp_form_widget{
            background:none !important
        }		</style>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>



<body class="rtl page-template page-template-elementor_header_footer page page-id-2019 res-banner-bg-trans pop-up-login theme-negarshop woocommerce-no-js elementor-default elementor-template-full-width elementor-kit-1558 elementor-page elementor-page-2019 dokan-theme-negarshop">
<div class="wrapper">
    <span class="front-ajax d-none" id="{{route('frontAjax')}}"></span>

    <header class="site-header d-none d-xl-block d-lg-block">
        <div data-elementor-type="header" data-elementor-id="2023" class="elementor elementor-2023 elementor-location-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-bee68cf elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bee68cf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-c17b43b" data-id="c17b43b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-c3b623b elementor-widget elementor-widget-image" data-id="c3b623b" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="143" height="58" src="{{asset('img/4rak-logo.png')}}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/4rak-logo.png')}} 143w, {{asset('img/4rak-logo.png')}} 50w" sizes="(max-width: 143px) 100vw, 143px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-856a8da" data-id="856a8da" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6e99cc8 elementor-widget elementor-widget-negarshop_header_menu" data-id="6e99cc8" data-element_type="widget" data-widget_type="negarshop_header_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="">
                                                    <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                        <div class="row align-items-center">
                                                            <div class="col-auto header-main-menu vertical-menu">
                                                                <button class="vertical-menu-toggle"><i class="fal fa-bars"></i>دسته بندی ها </button>
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c" class="main-menu category-menu">
                                                                    @foreach($categories as $cat)
                                                                        <li id="menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu  menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}">
                                                                            <a href="#" > {{$cat->title}}</a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-bed76fc" data-id="bed76fc" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-1c77a2d elementor-widget elementor-widget-negarshop_header_search" data-id="1c77a2d" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-left darken-color-mode style-3 colored">
                                                    <div data-type="product" class="search-box btn-style-light ajax-form ">
                                                        <form class="form-tag" action="https://demo.coderboy.ir/negarshop">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-filters" type="button"><i class="flaticon-settings-1"></i></button>
                                                                <button class="action-btn search-submit" type="submit"><i class="far fa-search"></i>
                                                                </button>
                                                            </div>

                                                            <div class="search-options">
                                                                <button class="close-popup" type="button"><i class="fal fa-check"></i></button>
                                                                <div class="filters-parent">
                                                                    <div class="list-item">
                                                                        <label for="header-search-cat">دسته بندی ها</label>
                                                                        <select name="pcat" id="header-search-cat" class="negar-select">
                                                                            <option value="" selected>همه دسته ها</option>
                                                                            <option value="15" >کالای دیجیتال</option>
                                                                            <option value="121" >اسباب بازی</option>
                                                                            <option value="102" >تی شرت</option>
                                                                            <option value="103" >سویشرت و هودی</option>
                                                                            <option value="101" >شلوار</option>
                                                                            <option value="46" >کامپیوتر و تجهیزات جانبی</option>
                                                                            <option value="104" >کفش ورزشی</option>
                                                                            <option value="44" >لپ تاپ</option>
                                                                            <option value="92" >لوازم جانبی موبایل</option>
                                                                            <option value="86" >لوازم خانگی</option>
                                                                            <option value="62" >مد و پوشاک</option>
                                                                            <option value="42" >موبایل</option>
                                                                            <option value="120" >نوزاد</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="list-item">
                                                                        <label for="header-search-stock">وضعیت محصول</label>
                                                                        <select name="stock" id="header-search-stock" class="negar-select">
                                                                            <option value="" selected>همه محصولات</option>
                                                                            <option value="instock" >فقط موجود ها</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <ul class="search-result"></ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-edc0fdd" data-id="edc0fdd" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-d8959bd" data-id="d8959bd" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a70ca33 elementor-widget elementor-widget-negarshop_header_basket" data-id="a70ca33" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-3 child-hover-right">
                                                    <a href="{{route('ShoppingCart')}}" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle"><span class="woocommerce-Price-amount amount"><bdi>
                                                         <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۰</bdi></span>
                                                        </span>
                                                        <span class="count">0</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-f96d2b1" data-id="f96d2b1" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-10faa2a elementor-widget elementor-widget-negarshop_header_account" data-id="10faa2a" data-element_type="widget" data-widget_type="negarshop_header_account.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-account text-left style-2">
                                                    <div class="account-box">
                                                        <i class="flaticon-avatar"></i>
                                                        <ul class="account-links">
                                                            @guest
                                                            <li>
                                                                <a href="{{route('login')}}" data-toggle="modal" data-target="#login-popup-modal">ورود به حساب</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('login')}}">ثبت نام</a>
                                                            </li>
                                                                @else
                                                                <li>
                                                                    <a href="{{route('Profile.index')}}" data-toggle="modal" data-target="#login-popup-modal">پروفایل</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{route('dashboard')}}"> پنل کاربری</a>
                                                                </li>
                                                            @endguest
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>
    <header class="responsive-header d-block d-xl-none d-lg-none">
        <div data-elementor-type="section" data-elementor-id="2188" class="elementor elementor-2188 elementor-location-responsive-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-345f71c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="345f71c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-79a8412" data-id="79a8412" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-082d926 elementor-widget elementor-widget-negarshop_responsive_menu" data-id="082d926" data-element_type="widget" data-widget_type="negarshop_responsive_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="responsive-menu">
                                                    <button class="btn btn-transparent toggle-menu"><i class="far fa-bars"></i></button>
                                                    <div class="menu-popup">
                                                        <nav class="responsive-navbar">
                                                            <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c-1" class="categories-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-188"><a href="https://demo.coderboy.ir/negarshop/product-category/%da%a9%d8%a7%d9%84%d8%a7%db%8c-%d8%af%db%8c%d8%ac%db%8c%d8%aa%d8%a7%d9%84/">کالای دیجیتال</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-190"><a href="#">آرایشی و بهداشتی</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-191"><a href="#">ابزار و اداری</a>
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-497"><a href="#">دریل، پیچ گوشتی، چکشی</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-498"><a href="#">اره برقی</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-499"><a href="#">کارواش</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-502"><a href="#">لوازم مصرفی خودرو</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-503"><a href="#">انواع فیلتر</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-504"><a href="#">روغن موتور و ضد یخ</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-505"><a href="#">مکمل سوخت و روغن</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-506"><a href="#">یدکی، لاستیک، لامپ و چراغ</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-500"><a href="#">مکنده و دمنده</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-501"><a href="#">فرز و سنگ رومیزی</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-189"><a href="https://demo.coderboy.ir/negarshop/product-category/%d9%85%d8%af-%d9%88-%d9%be%d9%88%d8%b4%d8%a7%da%a9/">مد و پوشاک</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-192"><a href="#">خانه و آشپزخانه</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-193"><a href="#">لوازم تحریر و هنر</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194"><a href="#">کودک و نوزاد</a></li>
                                                            </ul>                </nav>
                                                    </div>
                                                    <div class="overlay"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6503f75" data-id="6503f75" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-974f707 elementor-widget elementor-widget-image" data-id="974f707" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop">
                                                        <img width="143" height="58" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo.png 143w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo-50x20.png 50w" sizes="(max-width: 143px) 100vw, 143px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-18a1aeb" data-id="18a1aeb" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-704fc73 elementor-view-default elementor-widget elementor-widget-icon" data-id="704fc73" data-element_type="widget" data-widget_type="icon.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-wrapper">
                                                    <a class="elementor-icon" href="https://demo.coderboy.ir/negarshop/my-account/">
                                                        <i aria-hidden="true" class="far fa-user"></i>			</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-87822a2" data-id="87822a2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-92646d4 elementor-widget elementor-widget-negarshop_header_basket" data-id="92646d4" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-3 child-hover-right">
                                                    <a href="https://demo.coderboy.ir/negarshop/cart/" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۰</bdi></span></span>
                                                        <span class="count">0</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-275eab9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="275eab9" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28b1d92" data-id="28b1d92" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7ea9db8 elementor-widget elementor-widget-negarshop_header_search" data-id="7ea9db8" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-right darken-color-mode style-5 whiter">        <div data-type="product" class="search-box btn-style-dark ajax-form ">
                                                        <form class="form-tag" action="https://demo.coderboy.ir/negarshop">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-submit" type="submit"><i class="far fa-search"></i>
                                                                </button>
                                                            </div>

                                                        </form>
                                                        <ul class="search-result"></ul>    </div>

                                                </div>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>
    <div data-elementor-type="wp-page" data-elementor-id="2019" class="elementor elementor-2019" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-c8ecd29 elementor-section-full_width elementor-hidden-tablet elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id="c8ecd29" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9cca42a" data-id="9cca42a" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated d-block">
                                    <div class="w-100">
                                        @if($shop->header)
                                            <img src="{{asset('images/headers/'.$shop->header->path)}}" alt="">
                                        @else
                                            <img src="{{asset('images/headers/1608466272header.png')}}" alt="">
                                        @endif

                                        @if($shop->logo)
                                            <img class="img-fluid  " style="height: 300px;position:absolute;bottom:50px;width: 300px;border-radius: 100%;right: 50px" src="{{asset('images/logos/'.$shop->logo->path)}}" alt="">
                                        @else
                                            <img class="img-fluid " style="height: 300px;position:relative;bottom:50px;width: 300px;border-radius: 100%;right: 50px" src="{{asset('images/logos/1608809296logo.jpg')}}" alt="">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="291fd28" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-4d61851" data-id="4d61851" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a39cccb elementor-widget elementor-widget-image" data-id="a39cccb" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-bfb71dd" data-id="bfb71dd" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-1ce0270 elementor-widget elementor-widget-image" data-id="1ce0270" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-a2500a7" data-id="a2500a7" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-2f5fed7 elementor-widget elementor-widget-image" data-id="2f5fed7" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-ba34446" data-id="ba34446" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-273368c elementor-widget elementor-widget-image" data-id="273368c" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-5b303a4" data-id="5b303a4" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f89c249 elementor-widget elementor-widget-image" data-id="f89c249" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-14 elementor-top-column elementor-element elementor-element-f4c7f1c" data-id="f4c7f1c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-fb80eb8 elementor-widget elementor-widget-image" data-id="fb80eb8" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/ابزار-و-اداری.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/ابزار-و-اداری.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/ابزار-و-اداری-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="elementor-section elementor-top-section elementor-element elementor-element-130164c elementor-section-full_width elementor-hidden-desktop elementor-hidden-phone elementor-section-height-default elementor-section-height-default" data-id="130164c" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6774cf0" data-id="6774cf0" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f52e7ed elementor--h-position-center elementor--v-position-middle elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides" data-id="f52e7ed" data-element_type="widget" data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-slides-wrapper elementor-main-swiper swiper-container" dir="rtl" data-animation="fadeInUp">
                                                        <div class="swiper-wrapper elementor-slides">
                                                            <div class="elementor-repeater-item-b33e8e6 swiper-slide"><div class="swiper-slide-bg"></div><a class="swiper-slide-inner" href="https://demo.coderboy.ir/negarshop/shop/"><div class="swiper-slide-contents"><div class="elementor-slide-heading">کلکسیون لباس مردانه</div><div class="elementor-slide-description">پیراهن ، شلوار ، تیشرت ، کاپشن ، کفش مجلسی و ورزشی</div></div></a></div><div class="elementor-repeater-item-9573cf8 swiper-slide"><div class="swiper-slide-bg"></div><a class="swiper-slide-inner" href="https://demo.coderboy.ir/negarshop/shop/"><div class="swiper-slide-contents"><div class="elementor-slide-heading">کلکسیون لباس زنانه</div><div class="elementor-slide-description">پیراهن ، شلوار ، تیشرت ، کاپشن ، کفش مجلسی و ورزشی</div></div></a></div>				</div>
                                                        <div class="swiper-pagination"></div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-prev">
                                                            <i class="eicon-chevron-right" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">قبلی</span>
                                                        </div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-next">
                                                            <i class="eicon-chevron-left" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">بعدی</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-e22b1de elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="e22b1de" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9869285" data-id="9869285" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-faac947 elementor-widget elementor-widget-image" data-id="faac947" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-a7426c4" data-id="a7426c4" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-e2d51a6 elementor-widget elementor-widget-image" data-id="e2d51a6" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1e80bf0" data-id="1e80bf0" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-91b8285 elementor-widget elementor-widget-image" data-id="91b8285" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9891dfc" data-id="9891dfc" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7fa6afa elementor-widget elementor-widget-image" data-id="7fa6afa" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-04b3986 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="04b3986" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-b27c5e1" data-id="b27c5e1" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f9b7ce9 elementor-widget elementor-widget-image" data-id="f9b7ce9" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-4-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1c1cc5d" data-id="1c1cc5d" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6f2b0a9 elementor-widget elementor-widget-image" data-id="6f2b0a9" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-2-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-498afca" data-id="498afca" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-3453455 elementor-widget elementor-widget-image" data-id="3453455" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-3-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-749bc5e" data-id="749bc5e" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a422d31 elementor-widget elementor-widget-image" data-id="a422d31" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/">
                                                        <img width="600" height="600" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/h5-product-1-600x600-96x96.jpg 96w" sizes="(max-width: 600px) 100vw, 600px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-c09d099 elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="c09d099" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ecaff24" data-id="ecaff24" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-89af973 elementor-widget elementor-widget-heading" data-id="89af973" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h6 class="elementor-heading-title elementor-size-default">
                                                    جدیدترین محصولات
                                                </h6>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-f7dd8c9 elementor-widget elementor-widget-negarshop_custom_product" data-id="f7dd8c9" data-element_type="widget" data-widget_type="negarshop_custom_product.default">
                                            <div class="elementor-widget-container">
                                                <div class="content-widget elementor-wg transparent mini-products"><div class="large-products"><div class="row"><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d8%a7%d8%b2%d9%84-1000-%d8%aa%da%a9%d9%87-%d9%81%d8%a7%da%a9%d8%b3-%d9%be%d8%a7%d8%b2%d9%84-%d8%b7%d8%b1%d8%ad-%d9%85%d8%af%d8%b1%d8%b3%d9%87-%d8%a2%d9%81%d8%aa%d8%a7%d8%a8-%da%a9%d8%af-62242/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/119698248-300x300.jpg" alt="پازل 1000 تکه فاکس پازل طرح مدرسه آفتاب کد 62242">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d8%a7%d8%b2%d9%84-1000-%d8%aa%da%a9%d9%87-%d9%81%d8%a7%da%a9%d8%b3-%d9%be%d8%a7%d8%b2%d9%84-%d8%b7%d8%b1%d8%ad-%d9%85%d8%af%d8%b1%d8%b3%d9%87-%d8%a2%d9%81%d8%aa%d8%a7%d8%a8-%da%a9%d8%af-62242/">پازل 1000 تکه فاکس پازل طرح مدرسه آفتاب کد 62242</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۵۰,۰۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%a8%d8%a7%d8%b2%db%8c-%d9%81%da%a9%d8%b1%db%8c-%d9%81%da%a9%d8%b1%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-%da%a9%d9%87%d8%b1%d8%a8%d8%a7/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570865-347x300.jpg" alt="بازی فکری فکرانه مدل کهربا">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d8%a8%d8%a7%d8%b2%db%8c-%d9%81%da%a9%d8%b1%db%8c-%d9%81%da%a9%d8%b1%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-%da%a9%d9%87%d8%b1%d8%a8%d8%a7/">بازی فکری فکرانه مدل کهربا</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۰۵,۰۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806598-358x300.jpg" alt="ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/">ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۳۵,۴۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269259-300x300.jpg" alt="ماشین شارژی مدل 5256">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/">ماشین شارژی مدل 5256</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶,۰۰۰,۰۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/112870089-300x300.png" alt="تی شرت مردانه لووین">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/">تی شرت مردانه لووین</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۸۵,۰۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a2%da%af%d8%b1%db%8c%d9%86/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/112549996-300x300.png" alt="تی شرت مردانه زرد آگرین">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a2%da%af%d8%b1%db%8c%d9%86/">تی شرت مردانه زرد آگرین</a></div>
                                                                    <div class="price">
                                                                        <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۶۵,۰۰۰</bdi></span>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%d8%a8%d9%84%d9%86%d8%af-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a8%d8%a7%db%8c-%d9%86%d8%aa/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/111842725-300x300.png" alt="تی شرت آستین کوتاه مردانه بای نت">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%d8%a8%d9%84%d9%86%d8%af-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a8%d8%a7%db%8c-%d9%86%d8%aa/">تی شرت آستین کوتاه مردانه بای نت</a></div>
                                                                    <div class="price">
                                                                        <del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۱۵,۰۰۰</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶۵,۰۰۰</bdi></span></ins>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div><div class="col-lg-3">						<article class="product">
                                                                    <figure class="thumb">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d9%88%d9%84%d9%88%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%b2%db%8c/">
                                                                            <img class="lazy" data-src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/111244826-300x300.png" alt="تیشرت مردانه سبز زی">								</a>
                                                                    </figure>
                                                                    <div class="title"><a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d9%88%d9%84%d9%88%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%b2%db%8c/">تیشرت مردانه سبز زی</a></div>
                                                                    <div class="price">
                                                                        <del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۱۵,۰۰۰</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶۵,۰۰۰</bdi></span></ins>							</div>
                                                                    <div class="rate"><div class="star-rating"><span style="width:0%">امتیاز <strong class="rating">0</strong> از 5</span></div></div>
                                                                </article>
                                                            </div></div></div></div>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-d575907 elementor-align-center elementor-widget elementor-widget-button" data-id="d575907" data-element_type="widget" data-widget_type="button.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-button-wrapper">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/" class="elementor-button-link elementor-button elementor-size-xs" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">بارگذاری بیشتر</span>
		</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-574d8ba elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="574d8ba" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9745406" data-id="9745406" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-ebb94dd elementor-widget elementor-widget-heading" data-id="ebb94dd" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h6 class="elementor-heading-title elementor-size-default">
                                                    جدیدترین محصولات
                                                </h6>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-d54f9da elementor-widget elementor-widget-negarshop_carousel" data-id="d54f9da" data-element_type="widget" data-widget_type="negarshop_carousel.default">
                                            <div class="elementor-widget-container">
                                                <section
                                                    class="content-widget products-carousel tabs-count-1 tabs banner-false style-1" id="content-widget-d54f9da">
                                                    <div class="carousel-content">
                                                        <div class="loading">
                                                            <div class="spinner"></div>
                                                        </div>
                                                        <div class="owl-carousel" data-carousel="{&quot;nav&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:false}" data-items="{&quot;sm&quot;:2.5,&quot;md&quot;:2,&quot;lg&quot;:3,&quot;xl&quot;:4}"
                                                             id="product-carousel-d54f9da">
                                                            <div id="car-item-d54f9da-0">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d8%a7%d8%b2%d9%84-1000-%d8%aa%da%a9%d9%87-%d9%81%d8%a7%da%a9%d8%b3-%d9%be%d8%a7%d8%b2%d9%84-%d8%b7%d8%b1%d8%ad-%d9%85%d8%af%d8%b1%d8%b3%d9%87-%d8%a2%d9%81%d8%aa%d8%a7%d8%a8-%da%a9%d8%af-62242/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/119698248-300x300.jpg"
                                                                                alt="پازل 1000 تکه فاکس پازل طرح مدرسه آفتاب کد 62242">
                                                                            <div class="ribbons"><div><span style="background-color: #999999" title="" class="item"><i class="fa fa-check"></i> <span> اورجینال </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d9%be%d8%a7%d8%b2%d9%84-1000-%d8%aa%da%a9%d9%87-%d9%81%d8%a7%da%a9%d8%b3-%d9%be%d8%a7%d8%b2%d9%84-%d8%b7%d8%b1%d8%ad-%d9%85%d8%af%d8%b1%d8%b3%d9%87-%d8%a2%d9%81%d8%aa%d8%a7%d8%a8-%da%a9%d8%af-62242/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f07cc68c" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1753" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d8%a7%d8%b2%d9%84-1000-%d8%aa%da%a9%d9%87-%d9%81%d8%a7%da%a9%d8%b3-%d9%be%d8%a7%d8%b2%d9%84-%d8%b7%d8%b1%d8%ad-%d9%85%d8%af%d8%b1%d8%b3%d9%87-%d8%a2%d9%81%d8%aa%d8%a7%d8%a8-%da%a9%d8%af-62242/"><h3
                                                                                class="title ">پازل 1000 تکه فاکس پازل طرح مدرسه آفتاب کد 62242</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۵۰,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1753"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1753" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1753"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1753"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1753"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-d54f9da-1">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%a8%d8%a7%d8%b2%db%8c-%d9%81%da%a9%d8%b1%db%8c-%d9%81%da%a9%d8%b1%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-%da%a9%d9%87%d8%b1%d8%a8%d8%a7/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570865-347x300.jpg"
                                                                                alt="بازی فکری فکرانه مدل کهربا">
                                                                            <img width="293" height="300" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-293x300.jpg" class="attachment-sz_3_1 size-sz_3_1" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-293x300.jpg 293w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-1002x1024.jpg 1002w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-768x785.jpg 768w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-391x400.jpg 391w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872-600x613.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/113570872.jpg 1298w" sizes="(max-width: 293px) 100vw, 293px" /><div class="ribbons"><div><span style="background-color: #999999" title="" class="item"><i class="fa fa-check"></i> <span> اورجینال </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%a8%d8%a7%d8%b2%db%8c-%d9%81%da%a9%d8%b1%db%8c-%d9%81%da%a9%d8%b1%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-%da%a9%d9%87%d8%b1%d8%a8%d8%a7/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f07ce67c" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1744" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%a8%d8%a7%d8%b2%db%8c-%d9%81%da%a9%d8%b1%db%8c-%d9%81%da%a9%d8%b1%d8%a7%d9%86%d9%87-%d9%85%d8%af%d9%84-%da%a9%d9%87%d8%b1%d8%a8%d8%a7/"><h3
                                                                                class="title ">بازی فکری فکرانه مدل کهربا</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۰۵,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1744"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1744" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1744"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1744"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1744"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-d54f9da-2">
                                                                <article
                                                                    class="w-p-item product-type-variable product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806598-358x300.jpg"
                                                                                alt="ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه">
                                                                            <img width="300" height="300" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-300x300.jpg" class="attachment-sz_3_1 size-sz_3_1" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640-96x96.jpg 96w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/2806640.jpg 698w" sizes="(max-width: 300px) 100vw, 300px" /><div class="ribbons"><div><span style="background-color: #999999" title="" class="item"><i class="fa fa-check"></i> <span> اورجینال </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">

                                                                        <form class="variations_form cart" action="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/" method="post" enctype='multipart/form-data' data-product_id="1737" data-product_variations="[{&quot;attributes&quot;:{&quot;attribute_pa_colors&quot;:&quot;golden&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;\u0646\u0627\u0645\u0639\u0644\u0648\u0645&quot;,&quot;display_price&quot;:35400,&quot;display_regular_price&quot;:35400,&quot;image&quot;:{&quot;title&quot;:&quot;2806598&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-600x503.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-600x503.jpg 600w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-300x252.jpg 300w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-768x644.jpg 768w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-358x300.jpg 358w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-400x336.jpg 400w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-50x42.jpg 50w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg 833w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg&quot;,&quot;full_src_w&quot;:833,&quot;full_src_h&quot;:699,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-300x300.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:300,&quot;src_w&quot;:600,&quot;src_h&quot;:503},&quot;image_id&quot;:1740,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;yes&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:1,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;bdi&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;\u062a\u0648\u0645\u0627\u0646&lt;\/span&gt;&amp;nbsp;\u06f3\u06f5,\u06f4\u06f0\u06f0&lt;\/bdi&gt;&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1738,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;.1&quot;,&quot;weight_html&quot;:&quot;.1 kg&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_colors&quot;:&quot;silver&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;\u0646\u0627\u0645\u0639\u0644\u0648\u0645&quot;,&quot;display_price&quot;:35500,&quot;display_regular_price&quot;:35500,&quot;image&quot;:{&quot;title&quot;:&quot;2806598&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-600x503.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-600x503.jpg 600w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-300x252.jpg 300w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-768x644.jpg 768w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-358x300.jpg 358w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-400x336.jpg 400w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-50x42.jpg 50w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg 833w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598.jpg&quot;,&quot;full_src_w&quot;:833,&quot;full_src_h&quot;:699,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/2806598-300x300.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:300,&quot;src_w&quot;:600,&quot;src_h&quot;:503},&quot;image_id&quot;:1740,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;yes&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:1,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;bdi&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;\u062a\u0648\u0645\u0627\u0646&lt;\/span&gt;&amp;nbsp;\u06f3\u06f5,\u06f5\u06f0\u06f0&lt;\/bdi&gt;&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1739,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;.1&quot;,&quot;weight_html&quot;:&quot;.1 kg&quot;}]">

                                                                            <table class="variations w-100" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="label"><label for="pa_colors" class="m-0">رنگبندی</label></td>
                                                                                    <td class="value">
                                                                                        <div class="mb-0">
                                                                                            <select id="pa_colors" class="form-control" name="attribute_pa_colors" data-attribute_name="attribute_pa_colors" data-show_option_none="yes"><option value="">یک گزینه را انتخاب کنید</option><option value="golden" >طلایی</option><option value="silver" >نقره ای</option></select><a class="reset_variations" href="#">پاک کردن</a>                            </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <div class="single_variation_wrap">
                                                                                <div class="woocommerce-variation single_variation"></div><div class="woocommerce-variation-add-to-cart variations_button mt-3">
                                                                                    <div class="ns-add-to-cart-inner clearfix">
                                                                                        <div class="quantity hidden">
                                                                                            <input type="hidden" id="quantity_60094f07d2e9b" class="qty" name="quantity" value="1" />
                                                                                        </div>
                                                                                        <button type="submit" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                            <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>                افزودن به سبد خرید</button>
                                                                                    </div>



                                                                                    <input type="hidden" name="add-to-cart" value="1737" />
                                                                                    <input type="hidden" name="product_id" value="1737" />
                                                                                    <input type="hidden" name="variation_id" class="variation_id" value="0" />
                                                                                </div>
                                                                            </div>

                                                                        </form>

                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%a7%d8%b3%d8%a8%d8%a7%d8%a8-%d8%a8%d8%a7%d8%b2%db%8c-%d9%86%db%8c%da%a9%d9%88%d8%aa%d9%88%db%8c%d8%b2-%d9%85%d8%af%d9%84-%d8%a7%d8%aa%d9%88%d8%a8%d9%88%d8%b3-%d9%85/"><h3
                                                                                class="title ">ماشین اسباب بازی نیکوتویز مدل اتوبوس مدرسه</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۳۵,۴۰۰</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۳۵,۵۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1737"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1737" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1737"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1737"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1737"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-d54f9da-3">
                                                                <article
                                                                    class="w-p-item product-type-variable product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269259-300x300.jpg"
                                                                                alt="ماشین شارژی مدل 5256">
                                                                            <img width="300" height="300" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-300x300.jpg" class="attachment-sz_3_1 size-sz_3_1" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-300x300.jpg 300w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-150x150.jpg 150w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-768x768.jpg 768w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-400x400.jpg 400w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-50x50.jpg 50w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-600x600.jpg 600w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-100x100.jpg 100w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263-96x96.jpg 96w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/121269263.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" /><div class="ribbons"><div><span style="background-color: #999999" title="" class="item"><i class="fa fa-check"></i> <span> اورجینال </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">

                                                                        <form class="variations_form cart" action="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/" method="post" enctype='multipart/form-data' data-product_id="1724" data-product_variations="[{&quot;attributes&quot;:{&quot;attribute_pa_colors&quot;:&quot;purple&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;\u0646\u0627\u0645\u0639\u0644\u0648\u0645&quot;,&quot;display_price&quot;:6000000,&quot;display_regular_price&quot;:6000000,&quot;image&quot;:{&quot;title&quot;:&quot;121269259&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-600x600.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-600x600.jpg 600w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-300x300.jpg 300w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-150x150.jpg 150w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-768x768.jpg 768w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-400x400.jpg 400w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-50x50.jpg 50w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-100x100.jpg 100w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-96x96.jpg 96w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg 1000w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg&quot;,&quot;full_src_w&quot;:1000,&quot;full_src_h&quot;:1000,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-300x300.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:300,&quot;src_w&quot;:600,&quot;src_h&quot;:600},&quot;image_id&quot;:1725,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;yes&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:1,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;bdi&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;\u062a\u0648\u0645\u0627\u0646&lt;\/span&gt;&amp;nbsp;\u06f6,\u06f0\u06f0\u06f0,\u06f0\u06f0\u06f0&lt;\/bdi&gt;&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1731,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;.1&quot;,&quot;weight_html&quot;:&quot;.1 kg&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_colors&quot;:&quot;silver&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;\u0646\u0627\u0645\u0639\u0644\u0648\u0645&quot;,&quot;display_price&quot;:6300000,&quot;display_regular_price&quot;:6300000,&quot;image&quot;:{&quot;title&quot;:&quot;121269259&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-600x600.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-600x600.jpg 600w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-300x300.jpg 300w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-150x150.jpg 150w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-768x768.jpg 768w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-400x400.jpg 400w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-50x50.jpg 50w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-100x100.jpg 100w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-96x96.jpg 96w, https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg 1000w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259.jpg&quot;,&quot;full_src_w&quot;:1000,&quot;full_src_h&quot;:1000,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/uploads\/2020\/07\/121269259-300x300.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:300,&quot;src_w&quot;:600,&quot;src_h&quot;:600},&quot;image_id&quot;:1725,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;yes&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:1,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;bdi&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;\u062a\u0648\u0645\u0627\u0646&lt;\/span&gt;&amp;nbsp;\u06f6,\u06f3\u06f0\u06f0,\u06f0\u06f0\u06f0&lt;\/bdi&gt;&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1732,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;.1&quot;,&quot;weight_html&quot;:&quot;.1 kg&quot;}]">

                                                                            <table class="variations w-100" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="label"><label for="pa_colors" class="m-0">رنگبندی</label></td>
                                                                                    <td class="value">
                                                                                        <div class="mb-0">
                                                                                            <select id="pa_colors" class="form-control" name="attribute_pa_colors" data-attribute_name="attribute_pa_colors" data-show_option_none="yes"><option value="">یک گزینه را انتخاب کنید</option><option value="purple" >بنفش</option><option value="silver" >نقره ای</option></select><a class="reset_variations" href="#">پاک کردن</a>                            </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <div class="single_variation_wrap">
                                                                                <div class="woocommerce-variation single_variation"></div><div class="woocommerce-variation-add-to-cart variations_button mt-3">
                                                                                    <div class="ns-add-to-cart-inner clearfix">
                                                                                        <div class="quantity hidden">
                                                                                            <input type="hidden" id="quantity_60094f07da673" class="qty" name="quantity" value="1" />
                                                                                        </div>
                                                                                        <button type="submit" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                            <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>                افزودن به سبد خرید</button>
                                                                                    </div>



                                                                                    <input type="hidden" name="add-to-cart" value="1724" />
                                                                                    <input type="hidden" name="product_id" value="1724" />
                                                                                    <input type="hidden" name="variation_id" class="variation_id" value="0" />
                                                                                </div>
                                                                            </div>

                                                                        </form>

                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%85%d8%a7%d8%b4%db%8c%d9%86-%d8%b4%d8%a7%d8%b1%da%98%db%8c-%d9%85%d8%af%d9%84-5256/"><h3
                                                                                class="title ">ماشین شارژی مدل 5256</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶,۰۰۰,۰۰۰</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶,۳۰۰,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1724"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1724" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1724"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1724"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1724"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-d54f9da-4">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/112870089-300x300.png"
                                                                                alt="تی شرت مردانه لووین">
                                                                        </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f07ddeb2" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1321" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/"><h3
                                                                                class="title ">تی شرت مردانه لووین</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۸۵,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1321"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1321" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1321"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1321"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1321"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        #content-widget-d54f9da article.w-p-item .info > .price {
                                                            font-size: 0;
                                                        }

                                                        #content-widget-d54f9da article.w-p-item .info > .price > span + span {
                                                            display: none;
                                                        }

                                                        #content-widget-d54f9da .info > .price del {
                                                            display: none;
                                                        }

                                                        #content-widget-d54f9da article.item .info > .price * {
                                                            font-size: 15px;
                                                            margin-top: 3px;
                                                        }

                                                        #content-widget-d54f9da article.item.product-type-variable .price {
                                                            font-size: 0;
                                                        }

                                                        #content-widget-d54f9da article.item.product-type-variable .price > .amount:last-of-type {
                                                            display: none;
                                                        }

                                                    </style>
                                                    <script>
                                                        jQuery(document).ready(function ($) {
                                                            $('#product-carousel-d54f9da').owlCarousel({
                                                                rtl: true,
                                                                nav: true,
                                                                dots: false,
                                                                loop: true,
                                                                navText: ["<i class='fal fa-angle-right'></i>", "<i class='fal fa-angle-left'></i>"],
                                                                responsive: {
                                                                    0: {
                                                                        items: 2.5,
                                                                    },
                                                                    480: {
                                                                        items: 2,
                                                                    },
                                                                    700: {
                                                                        items: 3,
                                                                    },
                                                                    991: {
                                                                        items: 4,
                                                                    },
                                                                },
                                                                autoplayHoverPause: true,
                                                                margin: 15
                                                            });

                                                        });
                                                    </script>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-da13ee8 elementor-align-center elementor-widget elementor-widget-button" data-id="da13ee8" data-element_type="widget" data-widget_type="button.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-button-wrapper">
                                                    <a href="https://demo.coderboy.ir/negarshop/shop/" class="elementor-button-link elementor-button elementor-size-xs" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-angle-left"></i>			</span>
						<span class="elementor-button-text">بارگذاری بیشتر</span>
		</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-80d8606 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="80d8606" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-59f8df6" data-id="59f8df6" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-335c2a7 elementor-widget elementor-widget-image" data-id="335c2a7" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/مد-و-پوشاک-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-0349e1a" data-id="0349e1a" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-9b48432 elementor-widget elementor-widget-image" data-id="9b48432" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/لوازم-تحریر-و-هنر-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-854f3e3" data-id="854f3e3" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-4755216 elementor-widget elementor-widget-image" data-id="4755216" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کودک-و-نوزاد-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-a67387e" data-id="a67387e" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-2d8d98d elementor-widget elementor-widget-image" data-id="2d8d98d" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/کالای-دیجیتال-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-9e3c425" data-id="9e3c425" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-1f07c56 elementor-widget elementor-widget-image" data-id="1f07c56" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/خانه-و-آشپزخانه-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-16 elementor-top-column elementor-element elementor-element-69a78c0" data-id="69a78c0" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a87c3db elementor-widget elementor-widget-image" data-id="a87c3db" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <img width="120" height="188" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی.png 120w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/آرایشی-و-بهداشتی-32x50.png 32w" sizes="(max-width: 120px) 100vw, 120px" />											</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-29da879 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="29da879" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-795e06b" data-id="795e06b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-766342d elementor-widget elementor-widget-negarshop_carousel" data-id="766342d" data-element_type="widget" data-widget_type="negarshop_carousel.default">
                                            <div class="elementor-widget-container">
                                                <section
                                                    class="content-widget products-carousel tabs-count-1 tabs banner-false style-1" id="content-widget-766342d">
                                                    <header class="section-header" id="content-widget-header-766342d">
                                                        <ul class="tabs">
                                                            <li class="active"><a
                                                                    href="#766342d_0"
                                                                    data-query="dZHdjoMgEIVfZTPXXrT2h9Vn2YSwiIYUxQhu0xrevTNo1XbtzQDfHJg5zABe_PJG1Apy-Ol35-OJYlp-xcN3XJgaDxQPx5hnFEUGCbSdLXrpHfe3lh6RwqvKdrd1Stq-8ZCfEuC6QE2WlSxjJUq0V_V89alHbnRzQbJ-xHkriQljEF-t5VMprRzi_S6dsBcVgqY3Zr6NZWf0p9UVS2qyPBKnjHkBS1e81fKiOsiHpbvV_pNm4zPmLco2mw8JzqJ6phcTyGP3w38zYXJDyXdbYfRFqTd_IWyMJnyaRSyPgJ1x9mlBM6tFpbjTd9K5Oz_wPYQH"
                                                                    data-opts="bYxBCoAwDAT_krMXr34mxDZCoFpJY4uIf7f1oqC3ndllDygFTSwwDDBRSAwdrCqusenWUGUc4_Jwsv2e9zWL8YwuhqjpddBswkw_0mRm_VjyHi2iI7Xakc-0OPZwXg">مد و پوشاک</a>
                                                            </li>
                                                        </ul>
                                                        <a href=""
                                                           class="btn archive-link">دیدن همه</a>
                                                    </header>
                                                    <div class="carousel-content">
                                                        <div class="loading">
                                                            <div class="spinner"></div>
                                                        </div>
                                                        <div class="owl-carousel" data-carousel="{&quot;nav&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:false}" data-items="{&quot;sm&quot;:2.5,&quot;md&quot;:2,&quot;lg&quot;:3,&quot;xl&quot;:4}"
                                                             id="product-carousel-766342d">
                                                            <div id="car-item-766342d-0">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/112870089-300x300.png"
                                                                                alt="تی شرت مردانه لووین">
                                                                        </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f080b8fc" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1321" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d9%84%d9%88%d9%88%db%8c%d9%86/"><h3
                                                                                class="title ">تی شرت مردانه لووین</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۸۵,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1321"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1321" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1321"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1321"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1321"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-766342d-1">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a2%da%af%d8%b1%db%8c%d9%86/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/112549996-300x300.png"
                                                                                alt="تی شرت مردانه زرد آگرین">
                                                                        </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a2%da%af%d8%b1%db%8c%d9%86/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f080d29e" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1310" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a2%da%af%d8%b1%db%8c%d9%86/"><h3
                                                                                class="title ">تی شرت مردانه زرد آگرین</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۶۵,۰۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1310"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1310" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1310"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1310"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1310"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-766342d-2">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%d8%a8%d9%84%d9%86%d8%af-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a8%d8%a7%db%8c-%d9%86%d8%aa/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/111842725-300x300.png"
                                                                                alt="تی شرت آستین کوتاه مردانه بای نت">
                                                                            <div class="ribbons"><div><span style="background-color: #81d742" title="43.5" class="item"><i class="fal fa-percent"></i> <span> 43.5 </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%d8%a8%d9%84%d9%86%d8%af-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a8%d8%a7%db%8c-%d9%86%d8%aa/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f080ecd0" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1309" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%d8%a8%d9%84%d9%86%d8%af-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%a8%d8%a7%db%8c-%d9%86%d8%aa/"><h3
                                                                                class="title ">تی شرت آستین کوتاه مردانه بای نت</h3></a>


                                                                        <div class="price">
                                                                            <del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۱۵,۰۰۰</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶۵,۰۰۰</bdi></span></ins>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1309"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1309" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1309"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1309"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1309"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-766342d-3">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d9%88%d9%84%d9%88%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%b2%db%8c/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/111244826-300x300.png"
                                                                                alt="تیشرت مردانه سبز زی">
                                                                            <div class="ribbons"><div><span style="background-color: #81d742" title="43.5" class="item"><i class="fal fa-percent"></i> <span> 43.5 </span></span></div></div>                </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d9%be%d9%88%d9%84%d9%88%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%b2%db%8c/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity hidden">
                                                                                    <input type="hidden" id="quantity_60094f0810723" class="qty" name="quantity" value="1" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="1301" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d9%be%d9%88%d9%84%d9%88%d8%b4%d8%b1%d8%aa-%d9%85%d8%b1%d8%af%d8%a7%d9%86%d9%87-%d8%b2%db%8c/"><h3
                                                                                class="title ">تیشرت مردانه سبز زی</h3></a>


                                                                        <div class="price">
                                                                            <del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۱۱۵,۰۰۰</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۶۵,۰۰۰</bdi></span></ins>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="1301"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-1301" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="1301"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="1301"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="1301"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                            <div id="car-item-766342d-4">
                                                                <article
                                                                    class="w-p-item product-type-simple product-item-style-1">
                                                                    <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%da%a9%d9%88%d8%aa%d8%a7%d9%87-%d8%aa%d8%a7%d8%b1%da%a9%d8%a7%d9%86-%da%a9%d8%af-btt-152-1/">
                                                                        <figure class="thumbnail"><img
                                                                                src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/05/2791658-364x300.jpg"
                                                                                alt="تی شرت آستین کوتاه تارکان کد btt 152-1">
                                                                        </figure>
                                                                    </a>
                                                                    <div class="card-add-to-cart">


                                                                        <form class="cart" action="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%da%a9%d9%88%d8%aa%d8%a7%d9%87-%d8%aa%d8%a7%d8%b1%da%a9%d8%a7%d9%86-%da%a9%d8%af-btt-152-1/" method="post" enctype='multipart/form-data'>

                                                                            <div class="ns-add-to-cart-inner woocommerce-variation-add-to-cart clearfix">

                                                                                <div class="quantity">
                                                                                    <label class="screen-reader-text" for="quantity_60094f0813629">تی شرت آستین کوتاه تارکان کد btt 152-1 عدد</label>
                                                                                    <input
                                                                                        type="number"
                                                                                        id="quantity_60094f0813629"
                                                                                        class="input-text qty text"
                                                                                        step="1"
                                                                                        min="1"
                                                                                        max=""
                                                                                        name="quantity"
                                                                                        value="1"
                                                                                        title="تعداد"
                                                                                        size="4"
                                                                                        placeholder=""
                                                                                        inputmode="numeric" />
                                                                                </div>
                                                                                <button type="submit" name="add-to-cart" value="163" class="single_add_to_cart_button button alt btn btn-primary">
                                                                                    <span class="price-update">آخرین بروزرسانی قیمت: ۱۳۹۹/۰۷/۰۵</span>            افزودن به سبد خرید</button>
                                                                            </div>

                                                                        </form>


                                                                        <button class="close"><i class="far fa-times"></i> بستن</button>
                                                                    </div>
                                                                    <div class="info">
                                                                        <a href="https://demo.coderboy.ir/negarshop/product/%d8%aa%db%8c-%d8%b4%d8%b1%d8%aa-%d8%a2%d8%b3%d8%aa%db%8c%d9%86-%da%a9%d9%88%d8%aa%d8%a7%d9%87-%d8%aa%d8%a7%d8%b1%da%a9%d8%a7%d9%86-%da%a9%d8%af-btt-152-1/"><h3
                                                                                class="title ">تی شرت آستین کوتاه تارکان کد btt 152-1</h3></a>


                                                                        <div class="price">
                                                                            <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۴۹,۸۰۰</bdi></span>                    </div>
                                                                        <div class="actions">
                                                                            <ul>
                                                                                <li class="like"><a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به علاقه مندی ها"  class="add-product-favo  login_req" data-id="163"><i class="fal fa-heart"></i></a></li>	    	                            <li class="woocommerce product compare-button"><a class="compare wooscp-btn wooscp-btn-163" data-toggle="tooltip" data-placement="bottom" data-original-title="مقایسه محصول" data-id="163"></a></li>
                                                                                <li class="quick-view">
                                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="مشاهده سریع محصول" class="cb-quick-view" data-id="163"><i class="flaticon-magnifying-glass"></i></a>                        </li>
                                                                                <li class="add-to-cart">
                                                                                    <a class="cb-add-to-cart  advanced_cart" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="1031" data-id="163"><i class="flaticon-shopping-cart"></i></a>                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        #content-widget-766342d article.w-p-item .info > .price {
                                                            font-size: 0;
                                                        }

                                                        #content-widget-766342d article.w-p-item .info > .price > span + span {
                                                            display: none;
                                                        }

                                                        #content-widget-766342d .info > .price del {
                                                            display: none;
                                                        }

                                                        #content-widget-766342d article.item .info > .price * {
                                                            font-size: 15px;
                                                            margin-top: 3px;
                                                        }

                                                        #content-widget-766342d article.item.product-type-variable .price {
                                                            font-size: 0;
                                                        }

                                                        #content-widget-766342d article.item.product-type-variable .price > .amount:last-of-type {
                                                            display: none;
                                                        }

                                                    </style>
                                                    <script>
                                                        jQuery(document).ready(function ($) {
                                                            $('#product-carousel-766342d').owlCarousel({
                                                                rtl: true,
                                                                nav: true,
                                                                dots: false,
                                                                loop: true,
                                                                navText: ["<i class='fal fa-angle-right'></i>", "<i class='fal fa-angle-left'></i>"],
                                                                responsive: {
                                                                    0: {
                                                                        items: 2.5,
                                                                    },
                                                                    480: {
                                                                        items: 2,
                                                                    },
                                                                    700: {
                                                                        items: 3,
                                                                    },
                                                                    991: {
                                                                        items: 4,
                                                                    },
                                                                },
                                                                autoplayHoverPause: true,
                                                                margin: 15
                                                            });

                                                        });
                                                    </script>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-d3737f0 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="d3737f0" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-116dbcc" data-id="116dbcc" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-beabeeb elementor-pagination-position-inside elementor-skin-carousel elementor-arrows-yes elementor-pagination-type-bullets elementor-widget elementor-widget-media-carousel" data-id="beabeeb" data-element_type="widget" data-settings="{&quot;slides_per_view&quot;:&quot;6&quot;,&quot;slides_per_view_tablet&quot;:&quot;4&quot;,&quot;skin&quot;:&quot;carousel&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;show_arrows&quot;:&quot;yes&quot;,&quot;pagination&quot;:&quot;bullets&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="media-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-main-swiper swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/fgghd-1.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/dfgdfg.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/sdsdf-1.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/Layer-5.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/Layer-3.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/Layer-4.png)">
                                                                    </div>
                                                                </a>						</div>
                                                        </div>
                                                        <div class="swiper-pagination"></div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-prev">
                                                            <i class="eicon-chevron-right" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">قبلی</span>
                                                        </div>
                                                        <div class="elementor-swiper-button elementor-swiper-button-next">
                                                            <i class="eicon-chevron-left" aria-hidden="true"></i>
                                                            <span class="elementor-screen-only">بعدی</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <ul class="fixed-bottom-bar style-2">
            <li><a class="btn wooscp-btn" href="{{route('ShoppingCart')}}" data-toggle="tooltip" data-placement="top" title="سبد خرید"><i class="fal fa-shopping-cart"></i></a></li>
            <li><a class="btn" href="#" data-toggle="tooltip" data-placement="top" title="علاقه مندی ها"><i class="far fa-heart"></i></a></li>
            <li><a class="btn" href="javascript:void(0);" id="negarshop-to-top"><span><i class="far fa-angle-up"></i></span></a></li>
        </ul>
    </div>
    <footer class="site-footer">
        <div data-elementor-type="footer" data-elementor-id="2039" class="elementor elementor-2039 elementor-location-footer" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-4c9a706 elementor-hidden-phone elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="4c9a706" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-wide">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-60de0ba" data-id="60de0ba" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-1b94377 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1b94377" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eb72e0c" data-id="eb72e0c" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-0bf9f8a elementor-view-default elementor-widget elementor-widget-icon" data-id="0bf9f8a" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-paper-plane"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-85a6c2a" data-id="85a6c2a" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-224479d elementor-widget elementor-widget-heading" data-id="224479d" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">تحویل اکسپرس</h6>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-496f170 elementor-widget elementor-widget-heading" data-id="496f170" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">در کمترین زمان دریافت کنید</span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-118e2f1" data-id="118e2f1" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-4542e8e elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="4542e8e" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9fd6162" data-id="9fd6162" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-adaa1a0 elementor-view-default elementor-widget elementor-widget-icon" data-id="adaa1a0" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-phone-volume"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-1f966e8" data-id="1f966e8" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-6f49b94 elementor-widget elementor-widget-heading" data-id="6f49b94" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پشتیبانی ۲۴ ساعته</h6>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8ba74c6 elementor-widget elementor-widget-heading" data-id="8ba74c6" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">پشتیبانی هفت روز هفته</span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-7f85409" data-id="7f85409" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-c58ce6d elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="c58ce6d" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-73cb6da" data-id="73cb6da" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-954eab8 elementor-view-default elementor-widget elementor-widget-icon" data-id="954eab8" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-home"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-743d9fc" data-id="743d9fc" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-9c8617d elementor-widget elementor-widget-heading" data-id="9c8617d" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پرداخت در محل</h6>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8a738cb elementor-widget elementor-widget-heading" data-id="8a738cb" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">هنگام دریافت پرداخت کنید</span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-5d0eb9e" data-id="5d0eb9e" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-3006c68 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3006c68" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-c051b45" data-id="c051b45" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-ce7bdbb elementor-view-default elementor-widget elementor-widget-icon" data-id="ce7bdbb" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-redo"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-350e889" data-id="350e889" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-f0f5a98 elementor-widget elementor-widget-heading" data-id="f0f5a98" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">۷ روز ضمانت بازگشت</h6>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-747ba64 elementor-widget elementor-widget-heading" data-id="747ba64" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">هفت روز مهلت دارید </span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-c7478dd" data-id="c7478dd" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-3102b3c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3102b3c" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-3e725cf" data-id="3e725cf" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b06ecd3 elementor-view-default elementor-widget elementor-widget-icon" data-id="b06ecd3" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-shield-alt"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f80ae43" data-id="f80ae43" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-9d7728a elementor-widget elementor-widget-heading" data-id="9d7728a" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">ضمانت اصل‌ بودن کالا</h6>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-4ea6858 elementor-widget elementor-widget-heading" data-id="4ea6858" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">تایید اصالت کالا</span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-fe763d9 elementor-hidden-phone elementor-hidden-desktop elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="fe763d9" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-wide">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-2a880de" data-id="2a880de" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-1578208 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1578208" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-dd4e375" data-id="dd4e375" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8abc468 elementor-view-default elementor-widget elementor-widget-icon" data-id="8abc468" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-paper-plane"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5e64d44" data-id="5e64d44" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8eebd6e elementor-widget elementor-widget-heading" data-id="8eebd6e" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">تحویل اکسپرس</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-d95042f" data-id="d95042f" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-a8faad7 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a8faad7" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-52ff9cc" data-id="52ff9cc" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-6d45d79 elementor-view-default elementor-widget elementor-widget-icon" data-id="6d45d79" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-phone-volume"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-812aa57" data-id="812aa57" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-4e97683 elementor-widget elementor-widget-heading" data-id="4e97683" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پشتیبانی ۲۴ ساعته</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-09cfdc6" data-id="09cfdc6" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-7826b6d elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="7826b6d" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-28c67de" data-id="28c67de" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-60937eb elementor-view-default elementor-widget elementor-widget-icon" data-id="60937eb" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-home"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0854399" data-id="0854399" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-3ef080a elementor-widget elementor-widget-heading" data-id="3ef080a" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پرداخت در محل</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-7bcfa25" data-id="7bcfa25" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-a7bb893 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a7bb893" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ceed1e5" data-id="ceed1e5" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-aa7362c elementor-view-default elementor-widget elementor-widget-icon" data-id="aa7362c" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-redo"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0c25585" data-id="0c25585" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-fedf04b elementor-widget elementor-widget-heading" data-id="fedf04b" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">۷ روز ضمانت بازگشت</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-b9b8491" data-id="b9b8491" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-9c15ede elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9c15ede" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-53776f5" data-id="53776f5" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b2247ee elementor-view-default elementor-widget elementor-widget-icon" data-id="b2247ee" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-shield-alt"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-119729b" data-id="119729b" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-e79b162 elementor-widget elementor-widget-heading" data-id="e79b162" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">ضمانت اصل‌ بودن</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-0cbcafc elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0cbcafc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-wide">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-e2a9e9c" data-id="e2a9e9c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-2fbfb7f elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="2fbfb7f" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e9a0a29" data-id="e9a0a29" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b1d6fa0 elementor-view-default elementor-widget elementor-widget-icon" data-id="b1d6fa0" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-paper-plane"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-033d626" data-id="033d626" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-538bfd4 elementor-widget elementor-widget-heading" data-id="538bfd4" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">تحویل اکسپرس</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-7ea71a5" data-id="7ea71a5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-882b8cc elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="882b8cc" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-374204e" data-id="374204e" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b6d0f56 elementor-view-default elementor-widget elementor-widget-icon" data-id="b6d0f56" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-phone-volume"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-fd5898b" data-id="fd5898b" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-76bd5f4 elementor-widget elementor-widget-heading" data-id="76bd5f4" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پشتیبانی ۲۴ ساعته</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-13daa0c" data-id="13daa0c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-3e47bfa elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3e47bfa" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-473388f" data-id="473388f" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b4c89ba elementor-view-default elementor-widget elementor-widget-icon" data-id="b4c89ba" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-home"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e62250c" data-id="e62250c" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-2023f76 elementor-widget elementor-widget-heading" data-id="2023f76" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">پرداخت در محل</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-96a1e6b" data-id="96a1e6b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0f8987b elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0f8987b" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-bd47992" data-id="bd47992" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-5829b58 elementor-view-default elementor-widget elementor-widget-icon" data-id="5829b58" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-redo"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0bc7d5a" data-id="0bc7d5a" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-f5a87da elementor-widget elementor-widget-heading" data-id="f5a87da" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">۷ روز ضمانت بازگشت</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-ff64976" data-id="ff64976" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-1bfa4a1 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1bfa4a1" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-55b1fbf" data-id="55b1fbf" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-497b927 elementor-view-default elementor-widget elementor-widget-icon" data-id="497b927" data-element_type="widget" data-widget_type="icon.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-wrapper">
                                                                            <div class="elementor-icon">
                                                                                <i aria-hidden="true" class="fas fa-shield-alt"></i>			</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ff7d0e6" data-id="ff7d0e6" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-7ca2a63 elementor-widget elementor-widget-heading" data-id="7ca2a63" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h6 class="elementor-heading-title elementor-size-default">ضمانت اصل‌ بودن</h6>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-6810beb elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6810beb" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-16bfc0b" data-id="16bfc0b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-4876e6b elementor-widget elementor-widget-sidebar" data-id="4876e6b" data-element_type="widget" data-widget_type="sidebar.default">
                                            <div class="elementor-widget-container">
                                                <section id="nav_menu-4" class="widget widget_nav_menu"><header class="wg-header"><h6>با نگارشاپ</h6></header><div class="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be-container"><ul id="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be" class="menu"><li id="menu-item-476" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-476"><a href="#">اتاق خبر نگارشاپ</a></li>
                                                            <li id="menu-item-477" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-477"><a href="#">فروش در نگارشاپ</a></li>
                                                            <li id="menu-item-478" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-478"><a href="#">همکاری با سازمان‌ها</a></li>
                                                            <li id="menu-item-480" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-480"><a href="#">فرصت‌های شغلی</a></li>
                                                        </ul></div></section>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-425961b" data-id="425961b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-bc954e0 elementor-skin-carousel elementor-widget elementor-widget-media-carousel" data-id="bc954e0" data-element_type="widget" data-settings="{&quot;slides_per_view&quot;:&quot;1&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;skin&quot;:&quot;carousel&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="media-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-main-swiper swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/50a7c6fb.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo.aspx_.png)">
                                                                    </div>
                                                                </a>						</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-66c6596" data-id="66c6596" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-07cb7a1 elementor-widget elementor-widget-image" data-id="07cb7a1" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop">
                                                        <img width="143" height="58" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png 143w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2-50x20.png 50w" sizes="(max-width: 143px) 100vw, 143px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-98cbce0" data-id="98cbce0" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-b148fa4 elementor-widget elementor-widget-heading" data-id="b148fa4" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">فروشگاه اینترنتی نگارشاپ</h2>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-f756e69 elementor-widget elementor-widget-text-editor" data-id="f756e69" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>نگارشاپ به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با پایبندی به سه اصل کلیدی، پرداخت در محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های معتبر جهان، به بزرگ‌ترین فروشگاه اینترنتی ایران تبدیل شود. به محض ورود به نگارشاپ با یک سایت پر از کالا رو به رو می‌شوید! هر آنچه که نیاز دارید و به ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-acbd5af elementor-hidden-desktop elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="acbd5af" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-28dd643" data-id="28dd643" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6dc8d1d elementor-widget elementor-widget-image" data-id="6dc8d1d" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop">
                                                        <img width="143" height="58" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png 143w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2-50x20.png 50w" sizes="(max-width: 143px) 100vw, 143px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-ac206b1" data-id="ac206b1" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-fc17a32 elementor-widget elementor-widget-sidebar" data-id="fc17a32" data-element_type="widget" data-widget_type="sidebar.default">
                                            <div class="elementor-widget-container">
                                                <section id="nav_menu-4" class="widget widget_nav_menu"><header class="wg-header"><h6>با نگارشاپ</h6></header><div class="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be-container"><ul id="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be-1" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-476"><a href="#">اتاق خبر نگارشاپ</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-477"><a href="#">فروش در نگارشاپ</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-478"><a href="#">همکاری با سازمان‌ها</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-480"><a href="#">فرصت‌های شغلی</a></li>
                                                        </ul></div></section>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-0d2184d" data-id="0d2184d" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-3f88e2c elementor-skin-carousel elementor-widget elementor-widget-media-carousel" data-id="3f88e2c" data-element_type="widget" data-settings="{&quot;slides_per_view&quot;:&quot;1&quot;,&quot;slides_per_view_tablet&quot;:&quot;1&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;skin&quot;:&quot;carousel&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="media-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-main-swiper swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/50a7c6fb.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo.aspx_.png)">
                                                                    </div>
                                                                </a>						</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-491861b" data-id="491861b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-923026d elementor-widget elementor-widget-heading" data-id="923026d" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">فروشگاه اینترنتی نگارشاپ</h2>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-c9bb41f elementor-widget elementor-widget-text-editor" data-id="c9bb41f" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>نگارشاپ به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با پایبندی به سه اصل کلیدی، پرداخت در محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های معتبر جهان، به بزرگ‌ترین فروشگاه اینترنتی ایران تبدیل شود. به محض ورود به نگارشاپ با یک سایت پر از کالا رو به رو می‌شوید! هر آنچه که نیاز دارید و به ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-63e691d elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="63e691d" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9ec0bc8" data-id="9ec0bc8" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-8ffa247 elementor-widget elementor-widget-image" data-id="8ffa247" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="https://demo.coderboy.ir/negarshop">
                                                        <img width="143" height="58" src="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2.png 143w, https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/11/logo-minimal2-50x20.png 50w" sizes="(max-width: 143px) 100vw, 143px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-af8f3cc" data-id="af8f3cc" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-6437d79 elementor-widget elementor-widget-sidebar" data-id="6437d79" data-element_type="widget" data-widget_type="sidebar.default">
                                            <div class="elementor-widget-container">
                                                <section id="nav_menu-4" class="widget widget_nav_menu"><header class="wg-header"><h6>با نگارشاپ</h6></header><div class="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be-container"><ul id="menu-%d8%a8%d8%a7-%d9%86%da%af%d8%a7%d8%b1%d8%b4%d8%a7%d9%be-2" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-476"><a href="#">اتاق خبر نگارشاپ</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-477"><a href="#">فروش در نگارشاپ</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-478"><a href="#">همکاری با سازمان‌ها</a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-480"><a href="#">فرصت‌های شغلی</a></li>
                                                        </ul></div></section>		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9457b16" data-id="9457b16" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-72eb90f elementor-skin-carousel elementor-widget elementor-widget-media-carousel" data-id="72eb90f" data-element_type="widget" data-settings="{&quot;slides_per_view&quot;:&quot;1&quot;,&quot;slides_per_view_tablet&quot;:&quot;1&quot;,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;skin&quot;:&quot;carousel&quot;,&quot;effect&quot;:&quot;slide&quot;,&quot;speed&quot;:500,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="media-carousel.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-swiper">
                                                    <div class="elementor-main-swiper swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/50a7c6fb.png)">
                                                                    </div>
                                                                </a>						</div>
                                                            <div class="swiper-slide">
                                                                <a href="#">		<div class="elementor-carousel-image" style="background-image: url(https://demo.coderboy.ir/negarshop/wp-content/uploads/2020/07/logo.aspx_.png)">
                                                                    </div>
                                                                </a>						</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-8eddbb8" data-id="8eddbb8" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-9eab68e elementor-widget elementor-widget-heading" data-id="9eab68e" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 class="elementor-heading-title elementor-size-default">فروشگاه اینترنتی نگارشاپ</h2>		</div>
                                        </div>
                                        <div class="elementor-element elementor-element-07bfb78 elementor-widget elementor-widget-text-editor" data-id="07bfb78" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>نگارشاپ به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با پایبندی به سه اصل کلیدی، پرداخت در محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های معتبر جهان، به بزرگ‌ترین فروشگاه اینترنتی ایران تبدیل شود. به محض ورود به نگارشاپ با یک سایت پر از کالا رو به رو می‌شوید! هر آنچه که نیاز دارید و به ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-120f544 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="120f544" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-c4f1d81" data-id="c4f1d81" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-9300c4a elementor-widget elementor-widget-text-editor" data-id="9300c4a" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>تمامی مطالب، عکس ها و&#8230; مطعلق به سایت نگارشاپ می باشد.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ffc0c8e" data-id="ffc0c8e" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a9887a8 elementor-widget elementor-widget-text-editor" data-id="a9887a8" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p><a title="پسرک کدنویس" href="https://www.coderboy.ir" target="_blank" rel="noopener">طراح: پسرک کدنویس</a></p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-6bad4c8 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6bad4c8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ce7be8b" data-id="ce7be8b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a7b2712 elementor-widget elementor-widget-text-editor" data-id="a7b2712" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p>تمامی مطالب، عکس ها و&#8230; مطعلق به سایت نگارشاپ می باشد.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2e41c0b" data-id="2e41c0b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f0bef88 elementor-widget elementor-widget-text-editor" data-id="f0bef88" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix"><p><a title="پسرک کدنویس" href="https://www.coderboy.ir" target="_blank" rel="noopener">طراح: پسرک کدنویس</a></p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </footer>
</div>
<div class="wooscp-search">
    <div class="wooscp-search-inner">
        <div class="wooscp-search-content">
            <div class="wooscp-search-content-inner">
                <div class="wooscp-search-close"></div>
                <div class="wooscp-search-input">
                    <input type="search" id="wooscp_search_input"
                           placeholder="عنوان محصول مورد نظر را وارد کنید..."/>
                </div>
                <div class="wooscp-search-result"></div>
            </div>
        </div>
    </div>
</div>
<div id="wooscp-area" class="wooscp-area wooscp-bar-bottom wooscp-bar-right wooscp-hide-checkout"
     data-bg-color="#292a30"
     data-btn-color="#00a0d2">
    <div class="wooscp-inner">
        <div class="wooscp-table">
            <div class="wooscp-table-inner">
                <span id="wooscp-table-close" class="wooscp-table-close">بستن</span>                                    <div class="wooscp-table-items"></div>
            </div>
        </div>
        <div class="wooscp-bar">

            <div class="wooscp-bar-items"></div>
            <div class="wooscp-bar-btn wooscp-bar-btn-text">
                <div class="wooscp-bar-btn-icon-wrapper">
                    <div class="wooscp-bar-btn-icon-inner"><span></span><span></span><span></span>
                    </div>
                </div>
                مقایسه                                </div>
        </div>
    </div>
</div>
<script>
    var loadJS = function(url, implementationCode, location){
        //url is URL of external file, implementationCode is the code
        //to be called from the file, location is the location to
        //insert the <script> element

        var scriptTag = document.createElement('script');
        scriptTag.src = url;

        scriptTag.onload = implementationCode;
        scriptTag.onreadystatechange = implementationCode;

        location.appendChild(scriptTag);
    };
    var loadLazyloadPlugin = function(){
        var myLazyLoad = new LazyLoad({
            elements_selector: ".lazy"
        });
        jQuery(document).bind("ajaxComplete", function($){
            myLazyLoad.update();
        });
    }
    loadJS("{{asset('newFront/js/lazyload.min.js')}}}", loadLazyloadPlugin, document.body);
    var defaultText = {
        searchArchive:"نتیجه مورد نظر را پیدا نکردید؟",searchAllBtn:"مشاهده همه",searchNotFound:"چیزی پیدا نشد!",errorSend:"مشکلی هنگام ارسال پیش آمد!",bad:"بد",medium:"متوسط",good:"خوب",excelent:"عالی",verybad:"خیلی بد",pleaseWait:"لطفا صبر کنید ...",
    };
</script>
<div class="modal fade" id="login-popup-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <form class="header-popup-login">
            <div class="modal-content">

                <button type="button" class="btn close-modal cb-custom-close" data-dismiss="modal">
                    <i class="far fa-times"></i>
                </button>
                <div class="modal-body p-1">
                    <h6 class="modal-title">ورود به حساب کاربری</h6>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fal fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" id="login-username"
                               placeholder="نام کاربری یا ایمیل"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fal fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control" id="login-pass"
                               placeholder="گذرواژه"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>


                    <div class="wc-social-login">
                        <style>
                            a.ywsl-social{
                                text-decoration: none;
                                display: inline-block;
                                margin-right: 2px;
                            }
                        </style>
                        <p class="ywsl-label">ورود با:</p>
                        <p class="socials-list">
                            <a class="ywsl-social ywsl-facebook" href="https://demo.coderboy.ir/negarshop/wp-login.php?ywsl_social=facebook&#038;redirect=https%3A%2F%2Fdemo.coderboy.ir%3A443%2Fnegarshop%2Fminimal%2F"><img src="https://demo.coderboy.ir/negarshop/wp-content/plugins/yith-woocommerce-social-login/assets/images/facebook.png" alt="Facebook"/></a><a class="ywsl-social ywsl-twitter" href="https://demo.coderboy.ir/negarshop/wp-login.php?ywsl_social=twitter&#038;redirect=https%3A%2F%2Fdemo.coderboy.ir%3A443%2Fnegarshop%2Fminimal%2F"><img src="{{asset('img/twitter.png')}}" alt="Twitter"/></a><a class="ywsl-social ywsl-google" href="https://demo.coderboy.ir/negarshop/wp-login.php?ywsl_social=google&#038;redirect=https%3A%2F%2Fdemo.coderboy.ir%3A443%2Fnegarshop%2Fminimal%2F"><img src="https://demo.coderboy.ir/negarshop/wp-content/plugins/yith-woocommerce-social-login/assets/images/google.png" alt="Google"/></a></p></div>                            <div class="form-group ns-checkbox mt-4 mb-0">
                        <input type="checkbox" class="form-check-input" id="login-remember">
                        <label class="form-check-label"
                               for="login-remember">مرا به خاطر بسپار</label>
                    </div>
                    <div class="my-2">
                        <a href="https://demo.coderboy.ir/negarshop/my-account/lost-password/">رمز عبور خود را فراموش کرده اید؟</a>
                    </div>
                    <p id="login-res" class="float-right my-3 d-none"></p>
                </div>
                <div class="modal-footer d-flex">
                    <a href="https://demo.coderboy.ir/negarshop/my-account/?register"
                       class="btn btn-transparent">ثبت نام</a>
                    <button class="btn btn-primary float-left"
                            id="login-submit">ورود به حساب <i
                            class="far fa-angle-left"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade cb-quick-view-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn close-modal" data-dismiss="modal">
                <i class="far fa-times"></i>
            </button>
            <div class="w-100"></div>
            <div class="loading"><span class="spinner"></span></div>
            <div class="content"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<script type='text/javascript' src='{{asset('newFront/js/core.min.js')}}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/datepicker.min.js')}}' id='jquery-ui-datepicker-js'></script>
<script type='text/javascript' id='jquery-ui-datepicker-js-after'>
    jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"\u0628\u0633\u062a\u0646","currentText":"\u0627\u0645\u0631\u0648\u0632","monthNames":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u06cc","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"monthNamesShort":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u0647","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"nextText":"\u0628\u0639\u062f","prevText":"\u0642\u0628\u0644\u06cc","dayNames":["\u06cc\u06a9\u0634\u0646\u0628\u0647","\u062f\u0648\u0634\u0646\u0628\u0647","\u0633\u0647\u200c\u0634\u0646\u0628\u0647","\u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647","\u067e\u0646\u062c\u200c\u0634\u0646\u0628\u0647","\u062c\u0645\u0639\u0647","\u0634\u0646\u0628\u0647"],"dayNamesShort":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dayNamesMin":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dateFormat":"MM d, yy","firstDay":6,"isRTL":true});});
</script>
<script type='text/javascript' src='{{asset('newFront/js/selectWoo.full.min.js')}}' id='selectWoo-js'></script>
<script type='text/javascript' id='wc-country-select-js-extra'>
    /* <![CDATA[ */
    var wc_country_select_params = {"countries":"{\"AF\":[],\"AO\":{\"BGO\":\"Bengo\",\"BLU\":\"Benguela\",\"BIE\":\"Bi\\u00e9\",\"CAB\":\"Cabinda\",\"CNN\":\"Cunene\",\"HUA\":\"Huambo\",\"HUI\":\"Hu\\u00edla\",\"CCU\":\"Kuando Kubango\",\"CNO\":\"Kwanza-Norte\",\"CUS\":\"Kwanza-Sul\",\"LUA\":\"Luanda\",\"LNO\":\"Lunda-Norte\",\"LSU\":\"Lunda-Sul\",\"MAL\":\"Malanje\",\"MOX\":\"Moxico\",\"NAM\":\"Namibe\",\"UIG\":\"U\\u00edge\",\"ZAI\":\"Zaire\"},\"AR\":{\"C\":\"Ciudad Aut\\u00f3noma de Buenos Aires\",\"B\":\"Buenos Aires\",\"K\":\"Catamarca\",\"H\":\"Chaco\",\"U\":\"Chubut\",\"X\":\"C\\u00f3rdoba\",\"W\":\"Corrientes\",\"E\":\"Entre R\\u00edos\",\"P\":\"Formosa\",\"Y\":\"Jujuy\",\"L\":\"La Pampa\",\"F\":\"La Rioja\",\"M\":\"Mendoza\",\"N\":\"Misiones\",\"Q\":\"Neuqu\\u00e9n\",\"R\":\"R\\u00edo Negro\",\"A\":\"Salta\",\"J\":\"San Juan\",\"D\":\"San Luis\",\"Z\":\"Santa Cruz\",\"S\":\"Santa Fe\",\"G\":\"Santiago del Estero\",\"V\":\"Tierra del Fuego\",\"T\":\"Tucum\\u00e1n\"},\"AT\":[],\"AU\":{\"ACT\":\"Australian Capital Territory\",\"NSW\":\"New South Wales\",\"NT\":\"Northern Territory\",\"QLD\":\"Queensland\",\"SA\":\"South Australia\",\"TAS\":\"Tasmania\",\"VIC\":\"Victoria\",\"WA\":\"Western Australia\"},\"AX\":[],\"BD\":{\"BD-05\":\"Bagerhat\",\"BD-01\":\"Bandarban\",\"BD-02\":\"Barguna\",\"BD-06\":\"Barishal\",\"BD-07\":\"Bhola\",\"BD-03\":\"Bogura\",\"BD-04\":\"Brahmanbaria\",\"BD-09\":\"Chandpur\",\"BD-10\":\"Chattogram\",\"BD-12\":\"Chuadanga\",\"BD-11\":\"Cox's Bazar\",\"BD-08\":\"Cumilla\",\"BD-13\":\"Dhaka\",\"BD-14\":\"Dinajpur\",\"BD-15\":\"Faridpur \",\"BD-16\":\"Feni\",\"BD-19\":\"Gaibandha\",\"BD-18\":\"Gazipur\",\"BD-17\":\"Gopalganj\",\"BD-20\":\"Habiganj\",\"BD-21\":\"Jamalpur\",\"BD-22\":\"Jashore\",\"BD-25\":\"Jhalokati\",\"BD-23\":\"Jhenaidah\",\"BD-24\":\"Joypurhat\",\"BD-29\":\"Khagrachhari\",\"BD-27\":\"Khulna\",\"BD-26\":\"Kishoreganj\",\"BD-28\":\"Kurigram\",\"BD-30\":\"Kushtia\",\"BD-31\":\"Lakshmipur\",\"BD-32\":\"Lalmonirhat\",\"BD-36\":\"Madaripur\",\"BD-37\":\"Magura\",\"BD-33\":\"Manikganj \",\"BD-39\":\"Meherpur\",\"BD-38\":\"Moulvibazar\",\"BD-35\":\"Munshiganj\",\"BD-34\":\"Mymensingh\",\"BD-48\":\"Naogaon\",\"BD-43\":\"Narail\",\"BD-40\":\"Narayanganj\",\"BD-42\":\"Narsingdi\",\"BD-44\":\"Natore\",\"BD-45\":\"Nawabganj\",\"BD-41\":\"Netrakona\",\"BD-46\":\"Nilphamari\",\"BD-47\":\"Noakhali\",\"BD-49\":\"Pabna\",\"BD-52\":\"Panchagarh\",\"BD-51\":\"Patuakhali\",\"BD-50\":\"Pirojpur\",\"BD-53\":\"Rajbari\",\"BD-54\":\"Rajshahi\",\"BD-56\":\"Rangamati\",\"BD-55\":\"Rangpur\",\"BD-58\":\"Satkhira\",\"BD-62\":\"Shariatpur\",\"BD-57\":\"Sherpur\",\"BD-59\":\"Sirajganj\",\"BD-61\":\"Sunamganj\",\"BD-60\":\"Sylhet\",\"BD-63\":\"Tangail\",\"BD-64\":\"Thakurgaon\"},\"BE\":[],\"BG\":{\"BG-01\":\"Blagoevgrad\",\"BG-02\":\"Burgas\",\"BG-08\":\"Dobrich\",\"BG-07\":\"Gabrovo\",\"BG-26\":\"Haskovo\",\"BG-09\":\"Kardzhali\",\"BG-10\":\"Kyustendil\",\"BG-11\":\"Lovech\",\"BG-12\":\"Montana\",\"BG-13\":\"Pazardzhik\",\"BG-14\":\"Pernik\",\"BG-15\":\"Pleven\",\"BG-16\":\"Plovdiv\",\"BG-17\":\"Razgrad\",\"BG-18\":\"Ruse\",\"BG-27\":\"Shumen\",\"BG-19\":\"Silistra\",\"BG-20\":\"Sliven\",\"BG-21\":\"Smolyan\",\"BG-23\":\"Sofia\",\"BG-22\":\"Sofia-Grad\",\"BG-24\":\"Stara Zagora\",\"BG-25\":\"Targovishte\",\"BG-03\":\"Varna\",\"BG-04\":\"Veliko Tarnovo\",\"BG-05\":\"Vidin\",\"BG-06\":\"Vratsa\",\"BG-28\":\"Yambol\"},\"BH\":[],\"BI\":[],\"BJ\":{\"AL\":\"Alibori\",\"AK\":\"Atakora\",\"AQ\":\"Atlantique\",\"BO\":\"Borgou\",\"CO\":\"Collines\",\"KO\":\"Kouffo\",\"DO\":\"Donga\",\"LI\":\"Littoral\",\"MO\":\"Mono\",\"OU\":\"Ou\\u00e9m\\u00e9\",\"PL\":\"Plateau\",\"ZO\":\"Zou\"},\"BO\":{\"B\":\"\\u0686\\u0648\\u06a9\\u06cc\\u0633\\u0627\\u06a9\\u0627\",\"H\":\"\\u0628\\u0646\\u06cc\",\"C\":\"\\u06a9\\u0686\\u0627\\u0628\\u0627\\u0645\\u0628\\u0627\",\"L\":\"\\u0644\\u0627\\u067e\\u0627\\u0632\",\"O\":\"\\u0627\\u0648\\u0631\\u0648\\u0631\\u0648\",\"N\":\"\\u067e\\u0627\\u0646\\u062f\\u0648\",\"P\":\"\\u067e\\u0648\\u062a\\u0648\\u0633\\u06cc\",\"S\":\"Santa Cruz\",\"T\":\"\\u062a\\u0631\\u06cc\\u062c\\u0627\"},\"BR\":{\"AC\":\"Acre\",\"AL\":\"Alagoas\",\"AP\":\"Amap\\u00e1\",\"AM\":\"Amazonas\",\"BA\":\"Bahia\",\"CE\":\"Cear\\u00e1\",\"DF\":\"Distrito Federal\",\"ES\":\"Esp\\u00edrito Santo\",\"GO\":\"Goi\\u00e1s\",\"MA\":\"Maranh\\u00e3o\",\"MT\":\"Mato Grosso\",\"MS\":\"Mato Grosso do Sul\",\"MG\":\"Minas Gerais\",\"PA\":\"Par\\u00e1\",\"PB\":\"Para\\u00edba\",\"PR\":\"Paran\\u00e1\",\"PE\":\"Pernambuco\",\"PI\":\"Piau\\u00ed\",\"RJ\":\"Rio de Janeiro\",\"RN\":\"Rio Grande do Norte\",\"RS\":\"Rio Grande do Sul\",\"RO\":\"Rond\\u00f4nia\",\"RR\":\"Roraima\",\"SC\":\"Santa Catarina\",\"SP\":\"S\\u00e3o Paulo\",\"SE\":\"Sergipe\",\"TO\":\"Tocantins\"},\"CA\":{\"AB\":\"Alberta\",\"BC\":\"British Columbia\",\"MB\":\"Manitoba\",\"NB\":\"New Brunswick\",\"NL\":\"Newfoundland and Labrador\",\"NT\":\"Northwest Territories\",\"NS\":\"Nova Scotia\",\"NU\":\"Nunavut\",\"ON\":\"Ontario\",\"PE\":\"Prince Edward Island\",\"QC\":\"Quebec\",\"SK\":\"Saskatchewan\",\"YT\":\"Yukon Territory\"},\"CH\":{\"AG\":\"Aargau\",\"AR\":\"Appenzell Ausserrhoden\",\"AI\":\"Appenzell Innerrhoden\",\"BL\":\"Basel-Landschaft\",\"BS\":\"Basel-Stadt\",\"BE\":\"Bern\",\"FR\":\"Fribourg\",\"GE\":\"Geneva\",\"GL\":\"Glarus\",\"GR\":\"Graub\\u00fcnden\",\"JU\":\"Jura\",\"LU\":\"Luzern\",\"NE\":\"Neuch\\u00e2tel\",\"NW\":\"Nidwalden\",\"OW\":\"Obwalden\",\"SH\":\"Schaffhausen\",\"SZ\":\"Schwyz\",\"SO\":\"Solothurn\",\"SG\":\"St. Gallen\",\"TG\":\"Thurgau\",\"TI\":\"Ticino\",\"UR\":\"Uri\",\"VS\":\"Valais\",\"VD\":\"Vaud\",\"ZG\":\"Zug\",\"ZH\":\"Z\\u00fcrich\"},\"CN\":{\"CN1\":\"Yunnan \\\/ \\u4e91\\u5357\",\"CN2\":\"Beijing \\\/ \\u5317\\u4eac\",\"CN3\":\"Tianjin \\\/ \\u5929\\u6d25\",\"CN4\":\"Hebei \\\/ \\u6cb3\\u5317\",\"CN5\":\"Shanxi \\\/ \\u5c71\\u897f\",\"CN6\":\"Inner Mongolia \\\/ \\u5167\\u8499\\u53e4\",\"CN7\":\"Liaoning \\\/ \\u8fbd\\u5b81\",\"CN8\":\"Jilin \\\/ \\u5409\\u6797\",\"CN9\":\"Heilongjiang \\\/ \\u9ed1\\u9f99\\u6c5f\",\"CN10\":\"Shanghai \\\/ \\u4e0a\\u6d77\",\"CN11\":\"Jiangsu \\\/ \\u6c5f\\u82cf\",\"CN12\":\"Zhejiang \\\/ \\u6d59\\u6c5f\",\"CN13\":\"Anhui \\\/ \\u5b89\\u5fbd\",\"CN14\":\"Fujian \\\/ \\u798f\\u5efa\",\"CN15\":\"Jiangxi \\\/ \\u6c5f\\u897f\",\"CN16\":\"Shandong \\\/ \\u5c71\\u4e1c\",\"CN17\":\"Henan \\\/ \\u6cb3\\u5357\",\"CN18\":\"Hubei \\\/ \\u6e56\\u5317\",\"CN19\":\"Hunan \\\/ \\u6e56\\u5357\",\"CN20\":\"Guangdong \\\/ \\u5e7f\\u4e1c\",\"CN21\":\"Guangxi Zhuang \\\/ \\u5e7f\\u897f\\u58ee\\u65cf\",\"CN22\":\"Hainan \\\/ \\u6d77\\u5357\",\"CN23\":\"Chongqing \\\/ \\u91cd\\u5e86\",\"CN24\":\"Sichuan \\\/ \\u56db\\u5ddd\",\"CN25\":\"Guizhou \\\/ \\u8d35\\u5dde\",\"CN26\":\"Shaanxi \\\/ \\u9655\\u897f\",\"CN27\":\"Gansu \\\/ \\u7518\\u8083\",\"CN28\":\"Qinghai \\\/ \\u9752\\u6d77\",\"CN29\":\"Ningxia Hui \\\/ \\u5b81\\u590f\",\"CN30\":\"Macao \\\/ \\u6fb3\\u95e8\",\"CN31\":\"Tibet \\\/ \\u897f\\u85cf\",\"CN32\":\"Xinjiang \\\/ \\u65b0\\u7586\"},\"CZ\":[],\"DE\":[],\"DK\":[],\"DZ\":{\"DZ-01\":\"\\u0622\\u062f\\u0631\\u0627\\u0631\",\"DZ-02\":\"\\u0686\\u0644\\u0641\",\"DZ-03\":\"\\u0644\\u0627\\u063a\\u0648\\u0627\\u062a\",\"DZ-04\":\"\\u0627\\u0648\\u0645 \\u0627\\u0644\\u0628\\u0648\\u0627\\u063a\\u06cc\",\"DZ-05\":\"\\u0628\\u0627\\u062a\\u0646\\u0627\",\"DZ-06\":\"B\\u00e9ja\\u00efa\",\"DZ-07\":\"\\u0628\\u06cc\\u0633\\u06a9\\u0631\\u0627\",\"DZ-08\":\"B\\u00e9char\",\"DZ-09\":\"\\u0628\\u0644\\u06cc\\u062f\\u0627\",\"DZ-10\":\"\\u0628\\u0648\\u06cc\\u0631\\u0627\",\"DZ-11\":\"\\u062a\\u0627\\u0645\\u0627\\u0646\\u06af\\u0647\\u0627\\u0633\\u062a\",\"DZ-12\":\"T\\u00e9bessa\",\"DZ-13\":\"\\u062a\\u0644\\u0645\\u0633\\u0646\",\"DZ-14\":\"\\u062a\\u06cc\\u0627\\u0631\\u062a\",\"DZ-15\":\"\\u062a\\u064a\\u0632\\u064a \\u0627\\u0648\\u0632\\u0648\",\"DZ-16\":\"\\u0627\\u0644\\u062c\\u0632\\u0627\\u06cc\\u0631\",\"DZ-17\":\"\\u062c\\u0644\\u0641\\u0627\",\"DZ-18\":\"\\u062c\\u06cc\\u0698\\u0644\",\"DZ-19\":\"S\\u00e9tif\",\"DZ-20\":\"Sa\\u00efda\",\"DZ-21\":\"Skikda\",\"DZ-22\":\"Sidi Bel Abb\\u00e8s\",\"DZ-23\":\"Annaba\",\"DZ-24\":\"Guelma\",\"DZ-25\":\"Constantine\",\"DZ-26\":\"M\\u00e9d\\u00e9a\",\"DZ-27\":\"Mostaganem\",\"DZ-28\":\"M\\u2019Sila\",\"DZ-29\":\"\\u0645\\u0627\\u0633\\u06a9\\u0627\\u0631\\u0627\",\"DZ-30\":\"\\u0627\\u0648\\u0627\\u0631\\u06af\\u0644\\u0627\",\"DZ-31\":\"Oran\",\"DZ-32\":\"El Bayadh\",\"DZ-33\":\"\\u0627\\u06cc\\u0644\\u06cc\\u0632\\u06cc\",\"DZ-34\":\"Bordj Bou Arr\\u00e9ridj\",\"DZ-35\":\"Boumerd\\u00e8s\",\"DZ-36\":\"\\u0627\\u0644 \\u062a\\u0631\\u0641\",\"DZ-37\":\"\\u062a\\u06cc\\u0646\\u062f\\u0648\\u0641\",\"DZ-38\":\"Tissemsilt\",\"DZ-39\":\"\\u0627\\u0644 \\u0627\\u0648\\u0648\\u062f\",\"DZ-40\":\"\\u06a9\\u0646\\u0686\\u0644\\u0627\",\"DZ-41\":\"Souk Ahras\",\"DZ-42\":\"\\u062a\\u06cc\\u067e\\u0627\\u0633\\u0627\",\"DZ-43\":\"Mila\",\"DZ-44\":\"A\\u00efn Defla\",\"DZ-45\":\"Naama\",\"DZ-46\":\"A\\u00efn T\\u00e9mouchent\",\"DZ-47\":\"Gharda\\u00efa\",\"DZ-48\":\"Relizane\"},\"EE\":[],\"EG\":{\"EGALX\":\"Alexandria\",\"EGASN\":\"Aswan\",\"EGAST\":\"Asyut\",\"EGBA\":\"Red Sea\",\"EGBH\":\"Beheira\",\"EGBNS\":\"Beni Suef\",\"EGC\":\"Cairo\",\"EGDK\":\"Dakahlia\",\"EGDT\":\"Damietta\",\"EGFYM\":\"Faiyum\",\"EGGH\":\"Gharbia\",\"EGGZ\":\"Giza\",\"EGIS\":\"Ismailia\",\"EGJS\":\"South Sinai\",\"EGKB\":\"Qalyubia\",\"EGKFS\":\"Kafr el-Sheikh\",\"EGKN\":\"Qena\",\"EGLX\":\"Luxor\",\"EGMN\":\"Minya\",\"EGMNF\":\"Monufia\",\"EGMT\":\"Matrouh\",\"EGPTS\":\"Port Said\",\"EGSHG\":\"Sohag\",\"EGSHR\":\"Al Sharqia\",\"EGSIN\":\"North Sinai\",\"EGSUZ\":\"Suez\",\"EGWAD\":\"New Valley\"},\"ES\":{\"C\":\"A Coru\\u00f1a\",\"VI\":\"Araba\\\/\\u00c1lava\",\"AB\":\"Albacete\",\"A\":\"Alicante\",\"AL\":\"Almer\\u00eda\",\"O\":\"Asturias\",\"AV\":\"\\u00c1vila\",\"BA\":\"Badajoz\",\"PM\":\"Baleares\",\"B\":\"Barcelona\",\"BU\":\"Burgos\",\"CC\":\"C\\u00e1ceres\",\"CA\":\"C\\u00e1diz\",\"S\":\"Cantabria\",\"CS\":\"Castell\\u00f3n\",\"CE\":\"Ceuta\",\"CR\":\"Ciudad Real\",\"CO\":\"C\\u00f3rdoba\",\"CU\":\"Cuenca\",\"GI\":\"Girona\",\"GR\":\"Granada\",\"GU\":\"Guadalajara\",\"SS\":\"Gipuzkoa\",\"H\":\"Huelva\",\"HU\":\"Huesca\",\"J\":\"Ja\\u00e9n\",\"LO\":\"La Rioja\",\"GC\":\"Las Palmas\",\"LE\":\"Le\\u00f3n\",\"L\":\"Lleida\",\"LU\":\"Lugo\",\"M\":\"Madrid\",\"MA\":\"M\\u00e1laga\",\"ML\":\"Melilla\",\"MU\":\"Murcia\",\"NA\":\"Navarra\",\"OR\":\"Ourense\",\"P\":\"Palencia\",\"PO\":\"Pontevedra\",\"SA\":\"Salamanca\",\"TF\":\"Santa Cruz de Tenerife\",\"SG\":\"Segovia\",\"SE\":\"Sevilla\",\"SO\":\"Soria\",\"T\":\"Tarragona\",\"TE\":\"Teruel\",\"TO\":\"Toledo\",\"V\":\"Valencia\",\"VA\":\"Valladolid\",\"BI\":\"Biscay\",\"ZA\":\"Zamora\",\"Z\":\"Zaragoza\"},\"FI\":[],\"FR\":[],\"GH\":{\"AF\":\"Ahafo\",\"AH\":\"Ashanti\",\"BA\":\"Brong-Ahafo\",\"BO\":\"Bono\",\"BE\":\"Bono East\",\"CP\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"EP\":\"\\u0634\\u0631\\u0642\\u06cc\",\"AA\":\"Greater Accra\",\"NE\":\"North East\",\"NP\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"OT\":\"Oti\",\"SV\":\"Savannah\",\"UE\":\"\\u062e\\u0627\\u0648\\u0631 \\u0634\\u0631\\u0642\\u06cc\",\"UW\":\"\\u062e\\u0627\\u0648\\u0631\\u0645\\u06cc\\u0627\\u0646\\u0647\",\"TV\":\"Volta\",\"WP\":\"\\u063a\\u0631\\u0628\\u06cc\",\"WN\":\"Western North\"},\"GP\":[],\"GR\":{\"I\":\"Attica\",\"A\":\"East Macedonia and Thrace\",\"B\":\"Central Macedonia\",\"C\":\"\\u0645\\u0642\\u062f\\u0648\\u0646\\u06cc\\u0647 \\u063a\\u0631\\u0628\\u06cc\",\"D\":\"\\u0627\\u06cc\\u067e\\u06cc\\u0631\\u0648\\u0633\",\"E\":\"\\u062a\\u0633\\u0627\\u0644\\u0627\\u0644\\u06cc\",\"F\":\"Ionian Islands\",\"G\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"H\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"J\":\"\\u067e\\u0644\\u0648\\u067e\\u0648\\u0646\\u0632\",\"K\":\"North Aegean\",\"L\":\"South Aegean\",\"M\":\"\\u06a9\\u0631\\u062a\"},\"GF\":[],\"HK\":{\"HONG KONG\":\"Hong Kong Island\",\"KOWLOON\":\"Kowloon\",\"NEW TERRITORIES\":\"New Territories\"},\"HU\":{\"BK\":\"B\\u00e1cs-Kiskun\",\"BE\":\"B\\u00e9k\\u00e9s\",\"BA\":\"Baranya\",\"BZ\":\"Borsod-Aba\\u00faj-Zempl\\u00e9n\",\"BU\":\"Budapest\",\"CS\":\"Csongr\\u00e1d-Csan\\u00e1d\",\"FE\":\"Fej\\u00e9r\",\"GS\":\"Gy\\u0151r-Moson-Sopron\",\"HB\":\"Hajd\\u00fa-Bihar\",\"HE\":\"Heves\",\"JN\":\"J\\u00e1sz-Nagykun-Szolnok\",\"KE\":\"Kom\\u00e1rom-Esztergom\",\"NO\":\"N\\u00f3gr\\u00e1d\",\"PE\":\"Pest\",\"SO\":\"Somogy\",\"SZ\":\"Szabolcs-Szatm\\u00e1r-Bereg\",\"TO\":\"Tolna\",\"VA\":\"Vas\",\"VE\":\"Veszpr\\u00e9m\",\"ZA\":\"Zala\"},\"ID\":{\"AC\":\"Daerah Istimewa Aceh\",\"SU\":\"Sumatera Utara\",\"SB\":\"Sumatera Barat\",\"RI\":\"Riau\",\"KR\":\"Kepulauan Riau\",\"JA\":\"Jambi\",\"SS\":\"Sumatera Selatan\",\"BB\":\"Bangka Belitung\",\"BE\":\"Bengkulu\",\"LA\":\"Lampung\",\"JK\":\"DKI Jakarta\",\"JB\":\"Jawa Barat\",\"BT\":\"Banten\",\"JT\":\"Jawa Tengah\",\"JI\":\"Jawa Timur\",\"YO\":\"Daerah Istimewa Yogyakarta\",\"BA\":\"Bali\",\"NB\":\"Nusa Tenggara Barat\",\"NT\":\"Nusa Tenggara Timur\",\"KB\":\"Kalimantan Barat\",\"KT\":\"Kalimantan Tengah\",\"KI\":\"Kalimantan Timur\",\"KS\":\"Kalimantan Selatan\",\"KU\":\"Kalimantan Utara\",\"SA\":\"Sulawesi Utara\",\"ST\":\"Sulawesi Tengah\",\"SG\":\"Sulawesi Tenggara\",\"SR\":\"Sulawesi Barat\",\"SN\":\"Sulawesi Selatan\",\"GO\":\"Gorontalo\",\"MA\":\"Maluku\",\"MU\":\"Maluku Utara\",\"PA\":\"Papua\",\"PB\":\"Papua Barat\"},\"IE\":{\"CW\":\"Carlow\",\"CN\":\"Cavan\",\"CE\":\"Clare\",\"CO\":\"Cork\",\"DL\":\"Donegal\",\"D\":\"Dublin\",\"G\":\"Galway\",\"KY\":\"Kerry\",\"KE\":\"Kildare\",\"KK\":\"Kilkenny\",\"LS\":\"Laois\",\"LM\":\"Leitrim\",\"LK\":\"Limerick\",\"LD\":\"Longford\",\"LH\":\"Louth\",\"MO\":\"Mayo\",\"MH\":\"Meath\",\"MN\":\"Monaghan\",\"OY\":\"Offaly\",\"RN\":\"Roscommon\",\"SO\":\"Sligo\",\"TA\":\"Tipperary\",\"WD\":\"Waterford\",\"WH\":\"Westmeath\",\"WX\":\"Wexford\",\"WW\":\"Wicklow\"},\"IN\":{\"AP\":\"Andhra Pradesh\",\"AR\":\"Arunachal Pradesh\",\"AS\":\"Assam\",\"BR\":\"Bihar\",\"CT\":\"Chhattisgarh\",\"GA\":\"Goa\",\"GJ\":\"Gujarat\",\"HR\":\"Haryana\",\"HP\":\"Himachal Pradesh\",\"JK\":\"Jammu and Kashmir\",\"JH\":\"Jharkhand\",\"KA\":\"Karnataka\",\"KL\":\"Kerala\",\"MP\":\"Madhya Pradesh\",\"MH\":\"Maharashtra\",\"MN\":\"Manipur\",\"ML\":\"Meghalaya\",\"MZ\":\"Mizoram\",\"NL\":\"Nagaland\",\"OR\":\"Orissa\",\"PB\":\"Punjab\",\"RJ\":\"Rajasthan\",\"SK\":\"Sikkim\",\"TN\":\"Tamil Nadu\",\"TS\":\"Telangana\",\"TR\":\"Tripura\",\"UK\":\"Uttarakhand\",\"UP\":\"Uttar Pradesh\",\"WB\":\"West Bengal\",\"AN\":\"Andaman and Nicobar Islands\",\"CH\":\"Chandigarh\",\"DN\":\"Dadra and Nagar Haveli\",\"DD\":\"Daman and Diu\",\"DL\":\"Delhi\",\"LD\":\"Lakshadeep\",\"PY\":\"Pondicherry (Puducherry)\"},\"IR\":{\"ABZ\":\"\\u0627\\u0644\\u0628\\u0631\\u0632\",\"ADL\":\"\\u0627\\u0631\\u062f\\u0628\\u06cc\\u0644\",\"EAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u0634\\u0631\\u0642\\u06cc\",\"WAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"BHR\":\"\\u0628\\u0648\\u0634\\u0647\\u0631\",\"CHB\":\"\\u0686\\u0647\\u0627\\u0631\\u0645\\u062d\\u0627\\u0644 \\u0648 \\u0628\\u062e\\u062a\\u06cc\\u0627\\u0631\\u06cc\",\"FRS\":\"\\u0641\\u0627\\u0631\\u0633\",\"GIL\":\"\\u06af\\u06cc\\u0644\\u0627\\u0646\",\"GLS\":\"\\u06af\\u0644\\u0633\\u062a\\u0627\\u0646\",\"HDN\":\"\\u0647\\u0645\\u062f\\u0627\\u0646\",\"HRZ\":\"\\u0647\\u0631\\u0645\\u0632\\u06af\\u0627\\u0646\",\"ILM\":\"\\u0627\\u06cc\\u0644\\u0627\\u0645\",\"ESF\":\"\\u0627\\u0635\\u0641\\u0647\\u0627\\u0646\",\"KRN\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\",\"KRH\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\\u0634\\u0627\\u0647\",\"NKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0634\\u0645\\u0627\\u0644\\u06cc\",\"RKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0631\\u0636\\u0648\\u06cc\",\"SKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u062c\\u0646\\u0648\\u0628\\u06cc\",\"KHZ\":\"\\u062e\\u0648\\u0632\\u0633\\u062a\\u0627\\u0646\",\"KBD\":\"\\u06a9\\u0647\\u06af\\u06cc\\u0644\\u0648\\u06cc\\u0647 \\u0648 \\u0628\\u0648\\u06cc\\u0631\\u0627\\u062d\\u0645\\u062f\",\"KRD\":\"\\u06a9\\u0631\\u062f\\u0633\\u062a\\u0627\\u0646\",\"LRS\":\"\\u0644\\u0631\\u0633\\u062a\\u0627\\u0646\",\"MKZ\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"MZN\":\"\\u0645\\u0627\\u0632\\u0646\\u062f\\u0631\\u0627\\u0646\",\"GZN\":\"\\u0642\\u0632\\u0648\\u06cc\\u0646\",\"QHM\":\"\\u0642\\u0645\",\"SMN\":\"\\u0633\\u0645\\u0646\\u0627\\u0646\",\"SBN\":\"\\u0633\\u06cc\\u0633\\u062a\\u0627\\u0646 \\u0648 \\u0628\\u0644\\u0648\\u0686\\u0633\\u062a\\u0627\\u0646\",\"THR\":\"\\u062a\\u0647\\u0631\\u0627\\u0646\",\"YZD\":\"\\u06cc\\u0632\\u062f\",\"ZJN\":\"\\u0632\\u0646\\u062c\\u0627\\u0646\"},\"IS\":[],\"IT\":{\"AG\":\"Agrigento\",\"AL\":\"Alessandria\",\"AN\":\"Ancona\",\"AO\":\"Aosta\",\"AR\":\"Arezzo\",\"AP\":\"Ascoli Piceno\",\"AT\":\"Asti\",\"AV\":\"Avellino\",\"BA\":\"Bari\",\"BT\":\"Barletta-Andria-Trani\",\"BL\":\"Belluno\",\"BN\":\"Benevento\",\"BG\":\"Bergamo\",\"BI\":\"Biella\",\"BO\":\"Bologna\",\"BZ\":\"Bolzano\",\"BS\":\"Brescia\",\"BR\":\"Brindisi\",\"CA\":\"Cagliari\",\"CL\":\"Caltanissetta\",\"CB\":\"Campobasso\",\"CE\":\"Caserta\",\"CT\":\"Catania\",\"CZ\":\"Catanzaro\",\"CH\":\"Chieti\",\"CO\":\"Como\",\"CS\":\"Cosenza\",\"CR\":\"Cremona\",\"KR\":\"Crotone\",\"CN\":\"Cuneo\",\"EN\":\"Enna\",\"FM\":\"Fermo\",\"FE\":\"Ferrara\",\"FI\":\"Firenze\",\"FG\":\"Foggia\",\"FC\":\"Forl\\u00ec-Cesena\",\"FR\":\"Frosinone\",\"GE\":\"Genova\",\"GO\":\"Gorizia\",\"GR\":\"Grosseto\",\"IM\":\"Imperia\",\"IS\":\"Isernia\",\"SP\":\"La Spezia\",\"AQ\":\"L'Aquila\",\"LT\":\"Latina\",\"LE\":\"Lecce\",\"LC\":\"Lecco\",\"LI\":\"Livorno\",\"LO\":\"Lodi\",\"LU\":\"Lucca\",\"MC\":\"Macerata\",\"MN\":\"Mantova\",\"MS\":\"Massa-Carrara\",\"MT\":\"Matera\",\"ME\":\"Messina\",\"MI\":\"Milano\",\"MO\":\"Modena\",\"MB\":\"Monza e della Brianza\",\"NA\":\"Napoli\",\"NO\":\"Novara\",\"NU\":\"Nuoro\",\"OR\":\"Oristano\",\"PD\":\"Padova\",\"PA\":\"Palermo\",\"PR\":\"Parma\",\"PV\":\"Pavia\",\"PG\":\"Perugia\",\"PU\":\"Pesaro e Urbino\",\"PE\":\"Pescara\",\"PC\":\"Piacenza\",\"PI\":\"Pisa\",\"PT\":\"Pistoia\",\"PN\":\"Pordenone\",\"PZ\":\"Potenza\",\"PO\":\"Prato\",\"RG\":\"Ragusa\",\"RA\":\"Ravenna\",\"RC\":\"Reggio Calabria\",\"RE\":\"Reggio Emilia\",\"RI\":\"Rieti\",\"RN\":\"Rimini\",\"RM\":\"Roma\",\"RO\":\"Rovigo\",\"SA\":\"Salerno\",\"SS\":\"Sassari\",\"SV\":\"Savona\",\"SI\":\"Siena\",\"SR\":\"Siracusa\",\"SO\":\"Sondrio\",\"SU\":\"Sud Sardegna\",\"TA\":\"Taranto\",\"TE\":\"Teramo\",\"TR\":\"Terni\",\"TO\":\"Torino\",\"TP\":\"Trapani\",\"TN\":\"Trento\",\"TV\":\"Treviso\",\"TS\":\"Trieste\",\"UD\":\"Udine\",\"VA\":\"Varese\",\"VE\":\"Venezia\",\"VB\":\"Verbano-Cusio-Ossola\",\"VC\":\"Vercelli\",\"VR\":\"Verona\",\"VV\":\"Vibo Valentia\",\"VI\":\"Vicenza\",\"VT\":\"Viterbo\"},\"IL\":[],\"IM\":[],\"JM\":{\"JM-01\":\"Kingston\",\"JM-02\":\"Saint Andrew\",\"JM-03\":\"Saint Thomas\",\"JM-04\":\"Portland\",\"JM-05\":\"Saint Mary\",\"JM-06\":\"Saint Ann\",\"JM-07\":\"Trelawny\",\"JM-08\":\"Saint James\",\"JM-09\":\"Hanover\",\"JM-10\":\"Westmoreland\",\"JM-11\":\"Saint Elizabeth\",\"JM-12\":\"\\u0645\\u0646\\u062c\\u0633\\u062a\\u0631\",\"JM-13\":\"Clarendon\",\"JM-14\":\"\\u0633\\u0646\\u062a \\u06a9\\u0627\\u062a\\u0631\\u06cc\\u0646\"},\"JP\":{\"JP01\":\"Hokkaido\",\"JP02\":\"Aomori\",\"JP03\":\"Iwate\",\"JP04\":\"Miyagi\",\"JP05\":\"Akita\",\"JP06\":\"Yamagata\",\"JP07\":\"Fukushima\",\"JP08\":\"Ibaraki\",\"JP09\":\"Tochigi\",\"JP10\":\"Gunma\",\"JP11\":\"Saitama\",\"JP12\":\"Chiba\",\"JP13\":\"Tokyo\",\"JP14\":\"Kanagawa\",\"JP15\":\"Niigata\",\"JP16\":\"Toyama\",\"JP17\":\"Ishikawa\",\"JP18\":\"Fukui\",\"JP19\":\"Yamanashi\",\"JP20\":\"Nagano\",\"JP21\":\"Gifu\",\"JP22\":\"Shizuoka\",\"JP23\":\"Aichi\",\"JP24\":\"Mie\",\"JP25\":\"Shiga\",\"JP26\":\"Kyoto\",\"JP27\":\"Osaka\",\"JP28\":\"Hyogo\",\"JP29\":\"Nara\",\"JP30\":\"Wakayama\",\"JP31\":\"Tottori\",\"JP32\":\"Shimane\",\"JP33\":\"Okayama\",\"JP34\":\"Hiroshima\",\"JP35\":\"Yamaguchi\",\"JP36\":\"Tokushima\",\"JP37\":\"Kagawa\",\"JP38\":\"Ehime\",\"JP39\":\"Kochi\",\"JP40\":\"Fukuoka\",\"JP41\":\"Saga\",\"JP42\":\"Nagasaki\",\"JP43\":\"Kumamoto\",\"JP44\":\"Oita\",\"JP45\":\"Miyazaki\",\"JP46\":\"Kagoshima\",\"JP47\":\"Okinawa\"},\"KE\":{\"KE01\":\"Baringo\",\"KE02\":\"Bomet\",\"KE03\":\"Bungoma\",\"KE04\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"KE05\":\"Elgeyo-Marakwet\",\"KE06\":\"Embu\",\"KE07\":\"Garissa\",\"KE08\":\"Homa Bay\",\"KE09\":\"Isiolo\",\"KE10\":\"Kajiado\",\"KE11\":\"Kakamega\",\"KE12\":\"Kericho\",\"KE13\":\"Kiambu\",\"KE14\":\"Kilifi\",\"KE15\":\"Kirinyaga\",\"KE16\":\"Kisii\",\"KE17\":\"Kisumu\",\"KE18\":\"Kitui\",\"KE19\":\"Kwale\",\"KE20\":\"Laikipia\",\"KE21\":\"Lamu\",\"KE22\":\"Machakos\",\"KE23\":\"Makueni\",\"KE24\":\"Mandera\",\"KE25\":\"Marsabit\",\"KE26\":\"Meru\",\"KE27\":\"Migori\",\"KE28\":\"Mombasa\",\"KE29\":\"Murang\\u2019a\",\"KE30\":\"Nairobi County\",\"KE31\":\"Nakuru\",\"KE32\":\"Nandi\",\"KE33\":\"Narok\",\"KE34\":\"Nyamira\",\"KE35\":\"Nyandarua\",\"KE36\":\"Nyeri\",\"KE37\":\"Samburu\",\"KE38\":\"Siaya\",\"KE39\":\"Taita-Taveta\",\"KE40\":\"Tana River\",\"KE41\":\"Tharaka-Nithi\",\"KE42\":\"Trans Nzoia\",\"KE43\":\"Turkana\",\"KE44\":\"Uasin Gishu\",\"KE45\":\"Vihiga\",\"KE46\":\"Wajir\",\"KE47\":\"West Pokot\"},\"KR\":[],\"KW\":[],\"LA\":{\"AT\":\"\\u0622\\u062a\\u0627\\u067e\\u06cc\\u0648\",\"BK\":\"\\u0628\\u0648\\u06a9\\u06cc\\u0648\",\"BL\":\"\\u0628\\u0648\\u0644\\u06cc\\u06a9\\u0627\\u0645\\u200c\\u0633\\u0627\\u06cc\",\"CH\":\"\\u0686\\u0627\\u0645\\u067e\\u0627\\u0633\\u0627\\u06a9\",\"HO\":\"\\u0647\\u0648\\u0627\\u0641\\u0627\\u0646\",\"KH\":\"\\u062e\\u0627\\u0645\\u0648\\u0627\\u0646\",\"LM\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u0646\\u0627\\u0645\\u062a\\u0627\",\"LP\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u067e\\u0631\\u0627\\u0628\\u0627\\u0646\\u06af\",\"OU\":\"\\u0627\\u0648\\u062f\\u0648\\u0645\\u0634\\u0627\\u06cc\",\"PH\":\"\\u0641\\u0648\\u0646\\u06af\\u0633\\u0627\\u0644\\u06cc\",\"SL\":\"\\u0633\\u0627\\u0644\\u0627\\u0648\\u0627\\u0646\",\"SV\":\"\\u0633\\u0627\\u0648\\u0627\\u0646\\u0627\\u062e\\u062a\",\"VI\":\"\\u0627\\u0633\\u062a\\u0627\\u0646 \\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"VT\":\"\\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"XA\":\"\\u0633\\u0627\\u06cc\\u0646\\u06cc\\u0627\\u0628\\u0648\\u0644\\u06cc\",\"XE\":\"\\u0633\\u06a9\\u0648\\u0646\\u06af\",\"XI\":\"\\u0634\\u06cc\\u0627\\u0646\\u06af\\u062e\\u0648\\u0627\\u0646\\u06af\",\"XS\":\"Xaisomboun\"},\"LB\":[],\"LR\":{\"BM\":\"Bomi\",\"BN\":\"Bong\",\"GA\":\"Gbarpolu\",\"GB\":\"Grand Bassa\",\"GC\":\"Grand Cape Mount\",\"GG\":\"Grand Gedeh\",\"GK\":\"Grand Kru\",\"LO\":\"Lofa\",\"MA\":\"Margibi\",\"MY\":\"Maryland\",\"MO\":\"Montserrado\",\"NM\":\"Nimba\",\"RV\":\"Rivercess\",\"RG\":\"River Gee\",\"SN\":\"Sinoe\"},\"LU\":[],\"MD\":{\"C\":\"Chi\\u0219in\\u0103u\",\"BL\":\"B\\u0103l\\u021bi\",\"AN\":\"Anenii Noi\",\"BS\":\"Basarabeasca\",\"BR\":\"Briceni\",\"CH\":\"Cahul\",\"CT\":\"Cantemir\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"C\\u0103u\\u0219eni\",\"CM\":\"Cimi\\u0219lia\",\"CR\":\"Criuleni\",\"DN\":\"Dondu\\u0219eni\",\"DR\":\"Drochia\",\"DB\":\"Dub\\u0103sari\",\"ED\":\"Edine\\u021b\",\"FL\":\"F\\u0103le\\u0219ti\",\"FR\":\"Flore\\u0219ti\",\"GE\":\"UTA G\\u0103g\\u0103uzia\",\"GL\":\"Glodeni\",\"HN\":\"H\\u00eence\\u0219ti\",\"IL\":\"Ialoveni\",\"LV\":\"Leova\",\"NS\":\"Nisporeni\",\"OC\":\"Ocni\\u021ba\",\"OR\":\"Orhei\",\"RZ\":\"Rezina\",\"RS\":\"R\\u00ee\\u0219cani\",\"SG\":\"S\\u00eengerei\",\"SR\":\"Soroca\",\"ST\":\"Str\\u0103\\u0219eni\",\"SD\":\"\\u0218old\\u0103ne\\u0219ti\",\"SV\":\"\\u0218tefan Vod\\u0103\",\"TR\":\"Taraclia\",\"TL\":\"Telene\\u0219ti\",\"UN\":\"Ungheni\"},\"MQ\":[],\"MT\":[],\"MX\":{\"DF\":\"Ciudad de M\\u00e9xico\",\"JA\":\"Jalisco\",\"NL\":\"Nuevo Le\\u00f3n\",\"AG\":\"Aguascalientes\",\"BC\":\"Baja California\",\"BS\":\"Baja California Sur\",\"CM\":\"Campeche\",\"CS\":\"Chiapas\",\"CH\":\"Chihuahua\",\"CO\":\"Coahuila\",\"CL\":\"Colima\",\"DG\":\"Durango\",\"GT\":\"Guanajuato\",\"GR\":\"Guerrero\",\"HG\":\"Hidalgo\",\"MX\":\"Estado de M\\u00e9xico\",\"MI\":\"Michoac\\u00e1n\",\"MO\":\"Morelos\",\"NA\":\"Nayarit\",\"OA\":\"Oaxaca\",\"PU\":\"Puebla\",\"QT\":\"Quer\\u00e9taro\",\"QR\":\"Quintana Roo\",\"SL\":\"San Luis Potos\\u00ed\",\"SI\":\"Sinaloa\",\"SO\":\"Sonora\",\"TB\":\"Tabasco\",\"TM\":\"Tamaulipas\",\"TL\":\"Tlaxcala\",\"VE\":\"Veracruz\",\"YU\":\"Yucat\\u00e1n\",\"ZA\":\"Zacatecas\"},\"MY\":{\"JHR\":\"Johor\",\"KDH\":\"Kedah\",\"KTN\":\"Kelantan\",\"LBN\":\"Labuan\",\"MLK\":\"Malacca (Melaka)\",\"NSN\":\"Negeri Sembilan\",\"PHG\":\"Pahang\",\"PNG\":\"Penang (Pulau Pinang)\",\"PRK\":\"Perak\",\"PLS\":\"Perlis\",\"SBH\":\"Sabah\",\"SWK\":\"Sarawak\",\"SGR\":\"Selangor\",\"TRG\":\"Terengganu\",\"PJY\":\"Putrajaya\",\"KUL\":\"Kuala Lumpur\"},\"MZ\":{\"MZP\":\"Cabo Delgado\",\"MZG\":\"\\u063a\\u0632\\u0647\",\"MZI\":\"Inhambane\",\"MZB\":\"Manica\",\"MZL\":\"Maputo Province\",\"MZMPM\":\"Maputo\",\"MZN\":\"Nampula\",\"MZA\":\"Niassa\",\"MZS\":\"Sofala\",\"MZT\":\"\\u062a\\u062a\\u0647\",\"MZQ\":\"\\u0632\\u0627\\u0645\\u0628\\u0632\\u06cc\\u0627\"},\"NA\":{\"ER\":\"\\u0627\\u0631\\u0648\\u0646\\u06af\\u0648\",\"HA\":\"\\u0647\\u0627\\u0631\\u062f\\u067e\",\"KA\":\"\\u06a9\\u0627\\u0631\\u0627\\u0633\",\"KE\":\"\\u06a9\\u0627\\u0648\\u0627\\u0646\\u06af\\u0648 \\u0634\\u0631\\u0642\\u06cc\",\"KW\":\"Kavango West\",\"KH\":\"Khomas\",\"KU\":\"Kunene\",\"OW\":\"Ohangwena\",\"OH\":\"Omaheke\",\"OS\":\"Omusati\",\"ON\":\"Oshana\",\"OT\":\"Oshikoto\",\"OD\":\"Otjozondjupa\",\"CA\":\"Zambezi\"},\"NG\":{\"AB\":\"Abia\",\"FC\":\"Abuja\",\"AD\":\"Adamawa\",\"AK\":\"Akwa Ibom\",\"AN\":\"Anambra\",\"BA\":\"Bauchi\",\"BY\":\"Bayelsa\",\"BE\":\"Benue\",\"BO\":\"Borno\",\"CR\":\"Cross River\",\"DE\":\"Delta\",\"EB\":\"Ebonyi\",\"ED\":\"Edo\",\"EK\":\"Ekiti\",\"EN\":\"Enugu\",\"GO\":\"Gombe\",\"IM\":\"Imo\",\"JI\":\"Jigawa\",\"KD\":\"Kaduna\",\"KN\":\"Kano\",\"KT\":\"Katsina\",\"KE\":\"Kebbi\",\"KO\":\"Kogi\",\"KW\":\"Kwara\",\"LA\":\"Lagos\",\"NA\":\"Nasarawa\",\"NI\":\"\\u0646\\u06cc\\u062c\\u0631\",\"OG\":\"Ogun\",\"ON\":\"Ondo\",\"OS\":\"Osun\",\"OY\":\"Oyo\",\"PL\":\"Plateau\",\"RI\":\"Rivers\",\"SO\":\"Sokoto\",\"TA\":\"Taraba\",\"YO\":\"Yobe\",\"ZA\":\"Zamfara\"},\"NL\":[],\"NO\":[],\"NP\":{\"BAG\":\"Bagmati\",\"BHE\":\"Bheri\",\"DHA\":\"Dhaulagiri\",\"GAN\":\"Gandaki\",\"JAN\":\"Janakpur\",\"KAR\":\"Karnali\",\"KOS\":\"Koshi\",\"LUM\":\"Lumbini\",\"MAH\":\"Mahakali\",\"MEC\":\"Mechi\",\"NAR\":\"Narayani\",\"RAP\":\"Rapti\",\"SAG\":\"Sagarmatha\",\"SET\":\"Seti\"},\"NZ\":{\"NL\":\"Northland\",\"AK\":\"Auckland\",\"WA\":\"Waikato\",\"BP\":\"Bay of Plenty\",\"TK\":\"Taranaki\",\"GI\":\"Gisborne\",\"HB\":\"Hawke\\u2019s Bay\",\"MW\":\"Manawatu-Wanganui\",\"WE\":\"Wellington\",\"NS\":\"Nelson\",\"MB\":\"Marlborough\",\"TM\":\"Tasman\",\"WC\":\"West Coast\",\"CT\":\"Canterbury\",\"OT\":\"Otago\",\"SL\":\"Southland\"},\"PE\":{\"CAL\":\"El Callao\",\"LMA\":\"Municipalidad Metropolitana de Lima\",\"AMA\":\"Amazonas\",\"ANC\":\"Ancash\",\"APU\":\"Apur\\u00edmac\",\"ARE\":\"\\u0636\\u0631\\u0648\\u0631\\u06cc\",\"AYA\":\"Ayacucho\",\"CAJ\":\"Cajamarca\",\"CUS\":\"Cusco\",\"HUV\":\"Huancavelica\",\"HUC\":\"Hu\\u00e1nuco\",\"ICA\":\"Ica\",\"JUN\":\"Jun\\u00edn\",\"LAL\":\"La Libertad\",\"LAM\":\"Lambayeque\",\"LIM\":\"Lima\",\"LOR\":\"Loreto\",\"MDD\":\"Madre de Dios\",\"MOQ\":\"Moquegua\",\"PAS\":\"Pasco\",\"PIU\":\"Piura\",\"PUN\":\"Puno\",\"SAM\":\"San Mart\\u00edn\",\"TAC\":\"Tacna\",\"TUM\":\"Tumbes\",\"UCA\":\"Ucayali\"},\"PH\":{\"ABR\":\"Abra\",\"AGN\":\"Agusan del Norte\",\"AGS\":\"Agusan del Sur\",\"AKL\":\"Aklan\",\"ALB\":\"Albay\",\"ANT\":\"Antique\",\"APA\":\"Apayao\",\"AUR\":\"Aurora\",\"BAS\":\"Basilan\",\"BAN\":\"Bataan\",\"BTN\":\"Batanes\",\"BTG\":\"Batangas\",\"BEN\":\"Benguet\",\"BIL\":\"Biliran\",\"BOH\":\"Bohol\",\"BUK\":\"Bukidnon\",\"BUL\":\"Bulacan\",\"CAG\":\"Cagayan\",\"CAN\":\"Camarines Norte\",\"CAS\":\"Camarines Sur\",\"CAM\":\"Camiguin\",\"CAP\":\"Capiz\",\"CAT\":\"Catanduanes\",\"CAV\":\"Cavite\",\"CEB\":\"Cebu\",\"COM\":\"Compostela Valley\",\"NCO\":\"Cotabato\",\"DAV\":\"Davao del Norte\",\"DAS\":\"Davao del Sur\",\"DAC\":\"Davao Occidental\",\"DAO\":\"Davao Oriental\",\"DIN\":\"Dinagat Islands\",\"EAS\":\"Eastern Samar\",\"GUI\":\"Guimaras\",\"IFU\":\"Ifugao\",\"ILN\":\"Ilocos Norte\",\"ILS\":\"Ilocos Sur\",\"ILI\":\"Iloilo\",\"ISA\":\"Isabela\",\"KAL\":\"Kalinga\",\"LUN\":\"La Union\",\"LAG\":\"Laguna\",\"LAN\":\"Lanao del Norte\",\"LAS\":\"Lanao del Sur\",\"LEY\":\"Leyte\",\"MAG\":\"Maguindanao\",\"MAD\":\"Marinduque\",\"MAS\":\"Masbate\",\"MSC\":\"Misamis Occidental\",\"MSR\":\"Misamis Oriental\",\"MOU\":\"Mountain Province\",\"NEC\":\"Negros Occidental\",\"NER\":\"Negros Oriental\",\"NSA\":\"Northern Samar\",\"NUE\":\"Nueva Ecija\",\"NUV\":\"Nueva Vizcaya\",\"MDC\":\"Occidental Mindoro\",\"MDR\":\"Oriental Mindoro\",\"PLW\":\"Palawan\",\"PAM\":\"Pampanga\",\"PAN\":\"Pangasinan\",\"QUE\":\"Quezon\",\"QUI\":\"Quirino\",\"RIZ\":\"Rizal\",\"ROM\":\"Romblon\",\"WSA\":\"Samar\",\"SAR\":\"Sarangani\",\"SIQ\":\"Siquijor\",\"SOR\":\"Sorsogon\",\"SCO\":\"South Cotabato\",\"SLE\":\"Southern Leyte\",\"SUK\":\"Sultan Kudarat\",\"SLU\":\"Sulu\",\"SUN\":\"Surigao del Norte\",\"SUR\":\"Surigao del Sur\",\"TAR\":\"Tarlac\",\"TAW\":\"Tawi-Tawi\",\"ZMB\":\"Zambales\",\"ZAN\":\"Zamboanga del Norte\",\"ZAS\":\"Zamboanga del Sur\",\"ZSI\":\"Zamboanga Sibugay\",\"00\":\"Metro Manila\"},\"PK\":{\"JK\":\"Azad Kashmir\",\"BA\":\"Balochistan\",\"TA\":\"FATA\",\"GB\":\"Gilgit Baltistan\",\"IS\":\"Islamabad Capital Territory\",\"KP\":\"Khyber Pakhtunkhwa\",\"PB\":\"Punjab\",\"SD\":\"Sindh\"},\"PL\":[],\"PR\":[],\"PT\":[],\"PY\":{\"PY-ASU\":\"Asunci\\u00f3n\",\"PY-1\":\"Concepci\\u00f3n\",\"PY-2\":\"\\u0633\\u0646 \\u067e\\u062f\\u0631\\u0648\",\"PY-3\":\"\\u06a9\\u0648\\u0631\\u062f\\u06cc\\u0644\\u0631\\u0627\",\"PY-4\":\"Guair\\u00e1\",\"PY-5\":\"Caaguaz\\u00fa\",\"PY-6\":\"Caazap\\u00e1\",\"PY-7\":\"Itap\\u00faa\",\"PY-8\":\"Misiones\",\"PY-9\":\"Paraguar\\u00ed\",\"PY-10\":\"Alto Paran\\u00e1\",\"PY-11\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"PY-12\":\"\\u00d1eembuc\\u00fa\",\"PY-13\":\"\\u0622\\u0645\\u0627\\u0645\\u0628\\u0627\\u06cc\",\"PY-14\":\"Canindey\\u00fa\",\"PY-15\":\"\\u067e\\u0631\\u0632\\u06cc\\u062f\\u0646\\u062a \\u0647\\u06cc\\u0632\",\"PY-16\":\"\\u0622\\u0644\\u062a\\u0648 \\u067e\\u0627\\u0631\\u0627\\u06af\\u0648\\u0626\\u0647\",\"PY-17\":\"Boquer\\u00f3n\"},\"RE\":[],\"RO\":{\"AB\":\"Alba\",\"AR\":\"Arad\",\"AG\":\"Arge\\u0219\",\"BC\":\"Bac\\u0103u\",\"BH\":\"Bihor\",\"BN\":\"Bistri\\u021ba-N\\u0103s\\u0103ud\",\"BT\":\"Boto\\u0219ani\",\"BR\":\"Br\\u0103ila\",\"BV\":\"Bra\\u0219ov\",\"B\":\"Bucure\\u0219ti\",\"BZ\":\"Buz\\u0103u\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"Cara\\u0219-Severin\",\"CJ\":\"Cluj\",\"CT\":\"Constan\\u021ba\",\"CV\":\"Covasna\",\"DB\":\"D\\u00e2mbovi\\u021ba\",\"DJ\":\"Dolj\",\"GL\":\"Gala\\u021bi\",\"GR\":\"Giurgiu\",\"GJ\":\"Gorj\",\"HR\":\"Harghita\",\"HD\":\"Hunedoara\",\"IL\":\"Ialomi\\u021ba\",\"IS\":\"Ia\\u0219i\",\"IF\":\"Ilfov\",\"MM\":\"Maramure\\u0219\",\"MH\":\"Mehedin\\u021bi\",\"MS\":\"Mure\\u0219\",\"NT\":\"Neam\\u021b\",\"OT\":\"Olt\",\"PH\":\"Prahova\",\"SJ\":\"S\\u0103laj\",\"SM\":\"Satu Mare\",\"SB\":\"Sibiu\",\"SV\":\"Suceava\",\"TR\":\"Teleorman\",\"TM\":\"Timi\\u0219\",\"TL\":\"Tulcea\",\"VL\":\"V\\u00e2lcea\",\"VS\":\"Vaslui\",\"VN\":\"Vrancea\"},\"RS\":{\"RS00\":\"Belgrade\",\"RS14\":\"Bor\",\"RS11\":\"Brani\\u010devo\",\"RS02\":\"Central Banat\",\"RS10\":\"Danube\",\"RS23\":\"Jablanica\",\"RS09\":\"Kolubara\",\"RS08\":\"Ma\\u010dva\",\"RS17\":\"Morava\",\"RS20\":\"Ni\\u0161ava\",\"RS01\":\"North Ba\\u010dka\",\"RS03\":\"North Banat\",\"RS24\":\"P\\u010dinja\",\"RS22\":\"Pirot\",\"RS13\":\"Pomoravlje\",\"RS19\":\"Rasina\",\"RS18\":\"Ra\\u0161ka\",\"RS06\":\"South Ba\\u010dka\",\"RS04\":\"South Banat\",\"RS07\":\"Srem\",\"RS12\":\"\\u0160umadija\",\"RS21\":\"Toplica\",\"RS05\":\"West Ba\\u010dka\",\"RS15\":\"Zaje\\u010dar\",\"RS16\":\"Zlatibor\",\"RS25\":\"Kosovo\",\"RS26\":\"Pe\\u0107\",\"RS27\":\"Prizren\",\"RS28\":\"Kosovska Mitrovica\",\"RS29\":\"Kosovo-Pomoravlje\",\"RSKM\":\"Kosovo-Metohija\",\"RSVO\":\"Vojvodina\"},\"SG\":[],\"SK\":[],\"SI\":[],\"TH\":{\"TH-37\":\"Amnat Charoen\",\"TH-15\":\"Ang Thong\",\"TH-14\":\"Ayutthaya\",\"TH-10\":\"Bangkok\",\"TH-38\":\"Bueng Kan\",\"TH-31\":\"Buri Ram\",\"TH-24\":\"Chachoengsao\",\"TH-18\":\"Chai Nat\",\"TH-36\":\"Chaiyaphum\",\"TH-22\":\"Chanthaburi\",\"TH-50\":\"Chiang Mai\",\"TH-57\":\"Chiang Rai\",\"TH-20\":\"Chonburi\",\"TH-86\":\"Chumphon\",\"TH-46\":\"Kalasin\",\"TH-62\":\"Kamphaeng Phet\",\"TH-71\":\"Kanchanaburi\",\"TH-40\":\"Khon Kaen\",\"TH-81\":\"Krabi\",\"TH-52\":\"Lampang\",\"TH-51\":\"Lamphun\",\"TH-42\":\"Loei\",\"TH-16\":\"Lopburi\",\"TH-58\":\"Mae Hong Son\",\"TH-44\":\"Maha Sarakham\",\"TH-49\":\"Mukdahan\",\"TH-26\":\"Nakhon Nayok\",\"TH-73\":\"Nakhon Pathom\",\"TH-48\":\"Nakhon Phanom\",\"TH-30\":\"Nakhon Ratchasima\",\"TH-60\":\"Nakhon Sawan\",\"TH-80\":\"Nakhon Si Thammarat\",\"TH-55\":\"Nan\",\"TH-96\":\"Narathiwat\",\"TH-39\":\"Nong Bua Lam Phu\",\"TH-43\":\"Nong Khai\",\"TH-12\":\"Nonthaburi\",\"TH-13\":\"Pathum Thani\",\"TH-94\":\"Pattani\",\"TH-82\":\"Phang Nga\",\"TH-93\":\"Phatthalung\",\"TH-56\":\"Phayao\",\"TH-67\":\"Phetchabun\",\"TH-76\":\"Phetchaburi\",\"TH-66\":\"Phichit\",\"TH-65\":\"Phitsanulok\",\"TH-54\":\"Phrae\",\"TH-83\":\"Phuket\",\"TH-25\":\"Prachin Buri\",\"TH-77\":\"Prachuap Khiri Khan\",\"TH-85\":\"Ranong\",\"TH-70\":\"Ratchaburi\",\"TH-21\":\"Rayong\",\"TH-45\":\"Roi Et\",\"TH-27\":\"Sa Kaeo\",\"TH-47\":\"Sakon Nakhon\",\"TH-11\":\"Samut Prakan\",\"TH-74\":\"Samut Sakhon\",\"TH-75\":\"Samut Songkhram\",\"TH-19\":\"Saraburi\",\"TH-91\":\"Satun\",\"TH-17\":\"Sing Buri\",\"TH-33\":\"Sisaket\",\"TH-90\":\"Songkhla\",\"TH-64\":\"Sukhothai\",\"TH-72\":\"Suphan Buri\",\"TH-84\":\"Surat Thani\",\"TH-32\":\"Surin\",\"TH-63\":\"Tak\",\"TH-92\":\"Trang\",\"TH-23\":\"Trat\",\"TH-34\":\"Ubon Ratchathani\",\"TH-41\":\"Udon Thani\",\"TH-61\":\"Uthai Thani\",\"TH-53\":\"Uttaradit\",\"TH-95\":\"Yala\",\"TH-35\":\"Yasothon\"},\"TR\":{\"TR01\":\"Adana\",\"TR02\":\"Ad\\u0131yaman\",\"TR03\":\"Afyon\",\"TR04\":\"A\\u011fr\\u0131\",\"TR05\":\"Amasya\",\"TR06\":\"Ankara\",\"TR07\":\"Antalya\",\"TR08\":\"Artvin\",\"TR09\":\"Ayd\\u0131n\",\"TR10\":\"Bal\\u0131kesir\",\"TR11\":\"Bilecik\",\"TR12\":\"Bing\\u00f6l\",\"TR13\":\"Bitlis\",\"TR14\":\"Bolu\",\"TR15\":\"Burdur\",\"TR16\":\"Bursa\",\"TR17\":\"\\u00c7anakkale\",\"TR18\":\"\\u00c7ank\\u0131r\\u0131\",\"TR19\":\"\\u00c7orum\",\"TR20\":\"Denizli\",\"TR21\":\"Diyarbak\\u0131r\",\"TR22\":\"Edirne\",\"TR23\":\"Elaz\\u0131\\u011f\",\"TR24\":\"Erzincan\",\"TR25\":\"Erzurum\",\"TR26\":\"Eski\\u015fehir\",\"TR27\":\"Gaziantep\",\"TR28\":\"Giresun\",\"TR29\":\"G\\u00fcm\\u00fc\\u015fhane\",\"TR30\":\"Hakkari\",\"TR31\":\"Hatay\",\"TR32\":\"Isparta\",\"TR33\":\"\\u0130\\u00e7el\",\"TR34\":\"\\u0130stanbul\",\"TR35\":\"\\u0130zmir\",\"TR36\":\"Kars\",\"TR37\":\"Kastamonu\",\"TR38\":\"Kayseri\",\"TR39\":\"K\\u0131rklareli\",\"TR40\":\"K\\u0131r\\u015fehir\",\"TR41\":\"Kocaeli\",\"TR42\":\"Konya\",\"TR43\":\"K\\u00fctahya\",\"TR44\":\"Malatya\",\"TR45\":\"Manisa\",\"TR46\":\"Kahramanmara\\u015f\",\"TR47\":\"Mardin\",\"TR48\":\"Mu\\u011fla\",\"TR49\":\"Mu\\u015f\",\"TR50\":\"Nev\\u015fehir\",\"TR51\":\"Ni\\u011fde\",\"TR52\":\"Ordu\",\"TR53\":\"Rize\",\"TR54\":\"Sakarya\",\"TR55\":\"Samsun\",\"TR56\":\"Siirt\",\"TR57\":\"Sinop\",\"TR58\":\"Sivas\",\"TR59\":\"Tekirda\\u011f\",\"TR60\":\"Tokat\",\"TR61\":\"Trabzon\",\"TR62\":\"Tunceli\",\"TR63\":\"\\u015eanl\\u0131urfa\",\"TR64\":\"U\\u015fak\",\"TR65\":\"Van\",\"TR66\":\"Yozgat\",\"TR67\":\"Zonguldak\",\"TR68\":\"Aksaray\",\"TR69\":\"Bayburt\",\"TR70\":\"Karaman\",\"TR71\":\"K\\u0131r\\u0131kkale\",\"TR72\":\"Batman\",\"TR73\":\"\\u015e\\u0131rnak\",\"TR74\":\"Bart\\u0131n\",\"TR75\":\"Ardahan\",\"TR76\":\"I\\u011fd\\u0131r\",\"TR77\":\"Yalova\",\"TR78\":\"Karab\\u00fck\",\"TR79\":\"Kilis\",\"TR80\":\"Osmaniye\",\"TR81\":\"D\\u00fczce\"},\"TZ\":{\"TZ01\":\"Arusha\",\"TZ02\":\"Dar es Salaam\",\"TZ03\":\"Dodoma\",\"TZ04\":\"Iringa\",\"TZ05\":\"Kagera\",\"TZ06\":\"Pemba North\",\"TZ07\":\"Zanzibar North\",\"TZ08\":\"Kigoma\",\"TZ09\":\"Kilimanjaro\",\"TZ10\":\"Pemba South\",\"TZ11\":\"Zanzibar South\",\"TZ12\":\"Lindi\",\"TZ13\":\"Mara\",\"TZ14\":\"Mbeya\",\"TZ15\":\"Zanzibar West\",\"TZ16\":\"Morogoro\",\"TZ17\":\"Mtwara\",\"TZ18\":\"Mwanza\",\"TZ19\":\"Coast\",\"TZ20\":\"Rukwa\",\"TZ21\":\"Ruvuma\",\"TZ22\":\"Shinyanga\",\"TZ23\":\"Singida\",\"TZ24\":\"Tabora\",\"TZ25\":\"Tanga\",\"TZ26\":\"Manyara\",\"TZ27\":\"Geita\",\"TZ28\":\"Katavi\",\"TZ29\":\"Njombe\",\"TZ30\":\"Simiyu\"},\"LK\":[],\"SE\":[],\"UG\":{\"UG314\":\"\\u0622\\u0628\\u06cc\\u0645\",\"UG301\":\"\\u0622\\u062f\\u0648\\u0645\\u0627\\u0646\\u06cc\",\"UG322\":\"\\u0622\\u06af\\u0627\\u06af\\u0648\",\"UG323\":\"\\u0622\\u0644\\u0628\\u0648\\u0646\\u06af\",\"UG315\":\"\\u0622\\u0645\\u0648\\u0644\\u0627\\u062a\\u0627\\u0631\",\"UG324\":\"\\u0622\\u0645\\u0648\\u062f\\u0627\\u062a\",\"UG216\":\"\\u0622\\u0645\\u0648\\u0631\\u06cc\\u0627\",\"UG316\":\"\\u0622\\u0645\\u0648\\u0631\\u0648\",\"UG302\":\"\\u0622\\u067e\\u0627\\u06a9\",\"UG303\":\"\\u0622\\u0631\\u0648\\u0627\",\"UG217\":\"\\u0628\\u0648\\u062f\\u0627\\u06a9\\u0627\",\"UG218\":\"\\u0628\\u0648\\u062f\\u0627\",\"UG201\":\"\\u0628\\u0648\\u06af\\u06cc\\u0631\\u06cc\",\"UG235\":\"\\u0628\\u0648\\u06af\\u0648\\u0631\\u064a\",\"UG420\":\"\\u0628\\u0648\\u0648\\u0647\\u0648\\u062c\\u0648\",\"UG117\":\"\\u0628\\u06cc\\u0648\\u06a9\\u0648\\u0647\",\"UG219\":\"\\u0628\\u0648\\u06a9\\u0648\\u062f\\u0627\",\"UG118\":\"\\u0628\\u0648\\u06a9\\u0648\\u0645\\u0646\\u0633\\u06cc\\u0628\\u06cc\",\"UG220\":\"\\u0628\\u0648\\u06a9\\u0648\\u0627\",\"UG225\":\"\\u0628\\u0648\\u0644\\u0627\\u0645\\u0628\\u0648\\u0644\\u06cc\",\"UG416\":\"\\u0628\\u0648\\u0644\\u06cc\\u0633\\u0627\",\"UG401\":\"Bundibugyo\",\"UG430\":\"\\u0628\\u0648\\u0646\\u0627\\u0646\\u06af\\u0627\\u0628\\u0648\",\"UG402\":\"\\u0628\\u0648\\u0634\\u0646\\u06cc\",\"UG202\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"UG221\":\"\\u0628\\u0648\\u062a\\u0627\\u0644\\u0647\\u06cc\\u0627\",\"UG119\":\"\\u0628\\u0648\\u062a\\u0627\\u0645\\u0628\\u0644\\u0627\",\"UG233\":\"Butebo\",\"UG120\":\"\\u0628\\u0648\\u0648\\u0648\\u0645\\u0627\",\"UG226\":\"Buyende\",\"UG317\":\"\\u062f\\u0648\\u06a9\\u0648\\u0644\\u0648\",\"UG121\":\"\\u06af\\u0645\\u0628\\u0627\",\"UG304\":\"\\u06af\\u0644\\u0648\",\"UG403\":\"\\u0647\\u0648\\u06cc\\u0645\\u0627\",\"UG417\":\"\\u0627\\u06cc\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG203\":\"\\u0627\\u06cc\\u06af\\u0627\\u0646\\u06af\\u0627\",\"UG418\":\"\\u0627\\u06cc\\u0633\\u06cc\\u0646\\u06af\\u06cc\\u0631\\u0648\",\"UG204\":\"\\u062c\\u06cc\\u0646\\u062c\\u0627\",\"UG318\":\"\\u06a9\\u0627\\u0628\\u0648\\u0646\\u06af\",\"UG404\":\"\\u06a9\\u0627\\u0628\\u0644\",\"UG405\":\"\\u06a9\\u0627\\u0628\\u0627\\u0631\\u0648\\u0644\",\"UG213\":\"\\u06a9\\u0627\\u0628\\u06cc\\u0631\\u0627\\u0645\\u06cc\\u062f\\u0648\",\"UG427\":\"\\u06a9\\u0627\\u06af\\u0627\\u062f\\u06cc\",\"UG428\":\"\\u06a9\\u0627\\u06a9\\u0648\\u0645\\u06cc\\u0631\\u0648\",\"UG101\":\"\\u06a9\\u0627\\u0644\\u0646\\u06af\\u0627\\u0644\\u0627\",\"UG222\":\"\\u06a9\\u0627\\u0644\\u06cc\\u0631\\u0648\",\"UG122\":\"\\u06a9\\u0627\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG102\":\"\\u06a9\\u0627\\u0645\\u067e\\u0627\\u0644\\u0627\",\"UG205\":\"\\u06a9\\u0627\\u0645\\u0648\\u0644\\u06cc\",\"UG413\":\"\\u06a9\\u0627\\u0645\\u0648\\u0648\\u0646\\u06af\",\"UG414\":\"\\u06a9\\u0627\\u0646\\u0648\\u0646\\u06af\\u0648\",\"UG206\":\"\\u06a9\\u0627\\u067e\\u0686\\u0648\\u0631\\u0648\\u0627\",\"UG236\":\"\\u06a9\\u0627\\u067e\\u0644\\u0628\\u0628\\u06cc\\u0648\\u0646\\u06af\",\"UG126\":\"\\u06a9\\u0627\\u0633\\u0627\\u0646\\u062f\\u0627\",\"UG406\":\"\\u06a9\\u0627\\u0633\\u06cc\",\"UG207\":\"\\u06a9\\u062a\\u0627\\u06a9\\u0648\\u06cc\",\"UG112\":\"\\u06a9\\u0627\\u06cc\\u0648\\u0646\\u06af\\u0627\",\"UG407\":\"\\u06a9\\u06cc\\u0628\\u0627\\u0644\\u0647\",\"UG103\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06af\\u0627\",\"UG227\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06a9\\u0648\",\"UG432\":\"Kikuube\",\"UG419\":\"\\u06a9\\u06cc\\u0631\\u0648\\u0648\\u0631\\u0627\",\"UG421\":\"\\u06a9\\u06cc\\u0631\\u0627\\u0646\\u062f\\u0648\\u0646\\u06af\\u0648\",\"UG408\":\"\\u06a9\\u06cc\\u0633\\u0648\\u0631\\u0648\",\"UG305\":\"\\u06a9\\u06cc\\u062a\\u06af\\u0648\\u0645\",\"UG319\":\"\\u06a9\\u0648\\u0628\\u0648\\u06a9\\u0648\",\"UG325\":\"\\u06a9\\u0648\\u0644\",\"UG306\":\"\\u06a9\\u0648\\u062a\\u06cc\\u062f\\u0648\",\"UG208\":\"\\u0643\\u0648\\u0645\\u064a\",\"UG333\":\"\\u06a9\\u0648\\u0627\\u0646\\u06cc\\u0627\",\"UG228\":\"\\u06a9\\u0648\\u0646\\u06cc\\u0646\\u06af\",\"UG123\":\"\\u06a9\\u06cc\\u0627\\u0646\\u06a9\\u0648\\u0627\\u0646\\u0632\\u06cc\",\"UG422\":\"\\u06a9\\u06cc\\u06af\\u06af\\u0648\\u0627\",\"UG415\":\"\\u06a9\\u06cc\\u0648\\u0646\\u062c\\u0648\\u062c\\u0648\",\"UG125\":\"\\u06a9\\u06cc\\u0648\\u062a\\u0631\\u0627\",\"UG326\":\"\\u0644\\u0627\\u0645\\u0648\",\"UG307\":\"\\u0644\\u06cc\\u0631\",\"UG229\":\"\\u0644\\u0648\\u0648\\u06a9\\u0627\",\"UG104\":\"\\u0644\\u0648\\u0648\\u0631\\u0648\",\"UG124\":\"\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG114\":\"Lyantonde\",\"UG223\":\"\\u0645\\u0627\\u0646\\u0627\\u0641\\u0648\\u0627\",\"UG320\":\"\\u0645\\u0627\\u0631\\u0627\\u0686\\u0627\",\"UG105\":\"\\u0645\\u0627\\u0633\\u0627\\u06a9\\u0627\",\"UG409\":\"\\u0645\\u0627\\u0633\\u0646\\u062f\\u06cc\",\"UG214\":\"\\u0645\\u0627\\u06cc\\u0648\",\"UG209\":\"\\u0645\\u0628\\u0627\\u0644\\u0647\",\"UG410\":\"\\u0645\\u0628\\u0627\\u0631\\u06a9\\u0647\",\"UG423\":\"\\u0645\\u06cc\\u062a\\u0648\\u0645\\u0627\",\"UG115\":\"\\u0645\\u06cc\\u062a\\u0627\\u0646\\u0627\",\"UG308\":\"\\u0645\\u0631\\u0648\\u0648\\u062a\\u0648\",\"UG309\":\"\\u0645\\u0648\\u0648\\u06cc\",\"UG106\":\"Mpigi\",\"UG107\":\"\\u0645\\u0648\\u0648\\u0628\\u0646\\u062f\",\"UG108\":\"\\u0645\\u0648\\u06a9\\u0648\\u0646\\u0648\",\"UG334\":\"\\u0646\\u0628\\u06cc\\u0644\\u0627\\u062a\\u0648\\u06a9\",\"UG311\":\"\\u0646\\u0627\\u06a9\\u0627\\u067e\\u06cc\\u0631\\u06cc\\u067e\\u06cc\\u0631\\u06cc\\u062a\",\"UG116\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u06a9\\u0647\",\"UG109\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u0648\\u0646\\u06af\\u0644\\u0627\",\"UG230\":\"\\u0646\\u0627\\u0645\\u0627\\u06cc\\u06cc\\u0646\\u06af\\u0648\",\"UG234\":\"\\u0646\\u0627\\u0645\\u06cc\\u0633\\u06cc\\u0646\\u0648\\u0627\",\"UG224\":\"\\u0646\\u0627\\u0645\\u0648\\u062a\\u0648\\u0645\\u0628\\u0627\",\"UG327\":\"\\u0646\\u0627\\u067e\\u0627\\u06a9\",\"UG310\":\"\\u0646\\u0628\\u06cc\",\"UG231\":\"\\u0646\\u06af\\u0648\\u0631\\u0627\",\"UG424\":\"\\u0646\\u062a\\u0648\\u0631\\u0648\\u06a9\\u0648\",\"UG411\":\"\\u0646\\u0627\\u062a\\u0648\\u0646\\u06af\\u0627\\u0645\\u0648\",\"UG328\":\"\\u0646\\u0648\\u06cc\\u0627\",\"UG331\":\"\\u0627\\u0648\\u0645\\u0648\\u0631\\u0648\",\"UG329\":\"\\u0627\\u0648\\u062a\\u0648\\u06a9\\u0647\",\"UG321\":\"\\u0627\\u0648\\u06cc\\u0627\\u0645\",\"UG312\":\"\\u067e\\u062f\\u0631\",\"UG332\":\"\\u067e\\u0627\\u06a9\\u0648\\u0627\\u0686\",\"UG210\":\"\\u067e\\u0627\\u0644\\u06cc\\u0632\\u0627\",\"UG110\":\"\\u0631\\u0627\\u06a9\\u0627\\u06cc\\u06cc\",\"UG429\":\"\\u0631\\u0648\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG425\":\"\\u0631\\u0648\\u0628\\u06cc\\u0631\\u06cc\\u0632\\u06cc\",\"UG431\":\"\\u0631\\u06a9\\u06cc\\u06af\\u0627\",\"UG412\":\"Rukungiri\",\"UG111\":\"\\u0627\\u0633\\u0645\\u0628\\u0644\",\"UG232\":\"\\u0633\\u06cc\\u0631\",\"UG426\":\"\\u0634\\u06cc\\u0645\\u0627\",\"UG215\":\"\\u0633\\u06cc\\u0631\\u0648\\u0646\\u06a9\\u0648\",\"UG211\":\"\\u0633\\u0648\\u0631\\u0648\\u062a\\u06cc\",\"UG212\":\"\\u062a\\u0648\\u0631\\u0648\\u0631\\u0648\",\"UG113\":\"\\u0648\\u0627\\u06a9\\u06cc\\u0633\\u0648\",\"UG313\":\"\\u06cc\\u0648\\u0645\\u0628\",\"UG330\":\"\\u0632\\u0648\\u0645\\u0628\\u0648\"},\"UM\":{\"81\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0628\\u06cc\\u06a9\\u0631\",\"84\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 Howland\",\"86\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u062c\\u0627\\u0631\\u0648\\u06cc\\u0633\",\"67\":\"\\u062c\\u0627\\u0646\\u0633\\u062a\\u0648\\u0646 \\u0627\\u062a\\u0644\",\"89\":\"\\u06a9\\u06cc\\u0646\\u06af\\u0645\\u0646 \\u0631\\u06cc\\u0641\",\"71\":\"Midway Atoll\",\"76\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0646\\u0627\\u0648\\u0627\\u0633\\u0627\",\"95\":\"\\u067e\\u0627\\u0644\\u0645\\u06cc\\u0631\\u0627 \\u0627\\u062a\\u0644\",\"79\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0648\\u06cc\\u06a9\"},\"US\":{\"AL\":\"Alabama\",\"AK\":\"Alaska\",\"AZ\":\"Arizona\",\"AR\":\"Arkansas\",\"CA\":\"California\",\"CO\":\"Colorado\",\"CT\":\"Connecticut\",\"DE\":\"Delaware\",\"DC\":\"District Of Columbia\",\"FL\":\"Florida\",\"GA\":\"Georgia\",\"HI\":\"Hawaii\",\"ID\":\"Idaho\",\"IL\":\"Illinois\",\"IN\":\"Indiana\",\"IA\":\"Iowa\",\"KS\":\"Kansas\",\"KY\":\"Kentucky\",\"LA\":\"Louisiana\",\"ME\":\"Maine\",\"MD\":\"Maryland\",\"MA\":\"Massachusetts\",\"MI\":\"Michigan\",\"MN\":\"Minnesota\",\"MS\":\"Mississippi\",\"MO\":\"Missouri\",\"MT\":\"Montana\",\"NE\":\"Nebraska\",\"NV\":\"Nevada\",\"NH\":\"New Hampshire\",\"NJ\":\"New Jersey\",\"NM\":\"New Mexico\",\"NY\":\"New York\",\"NC\":\"North Carolina\",\"ND\":\"North Dakota\",\"OH\":\"Ohio\",\"OK\":\"Oklahoma\",\"OR\":\"Oregon\",\"PA\":\"Pennsylvania\",\"RI\":\"Rhode Island\",\"SC\":\"South Carolina\",\"SD\":\"South Dakota\",\"TN\":\"Tennessee\",\"TX\":\"Texas\",\"UT\":\"Utah\",\"VT\":\"Vermont\",\"VA\":\"Virginia\",\"WA\":\"Washington\",\"WV\":\"West Virginia\",\"WI\":\"Wisconsin\",\"WY\":\"Wyoming\",\"AA\":\"Armed Forces (AA)\",\"AE\":\"Armed Forces (AE)\",\"AP\":\"Armed Forces (AP)\"},\"VN\":[],\"YT\":[],\"ZA\":{\"EC\":\"Eastern Cape\",\"FS\":\"Free State\",\"GP\":\"Gauteng\",\"KZN\":\"KwaZulu-Natal\",\"LP\":\"Limpopo\",\"MP\":\"Mpumalanga\",\"NC\":\"Northern Cape\",\"NW\":\"North West\",\"WC\":\"Western Cape\"},\"ZM\":{\"ZM-01\":\"\\u063a\\u0631\\u0628\\u06cc\",\"ZM-02\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"ZM-03\":\"\\u0634\\u0631\\u0642\\u06cc\",\"ZM-04\":\"\\u0644\\u0648\\u0622\\u067e\\u0648\\u0644\\u0627\",\"ZM-05\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"ZM-06\":\"\\u0634\\u0645\\u0627\\u0644 \\u063a\\u0631\\u0628\\u06cc\",\"ZM-07\":\"\\u062c\\u0646\\u0648\\u0628\\u06cc\",\"ZM-08\":\"\\u0645\\u0633\\u06cc\",\"ZM-09\":\"\\u0644\\u0648\\u0633\\u0627\\u06a9\\u0627\",\"ZM-10\":\"\\u0645\\u0648\\u0686\\u06cc\\u0646\\u06af\\u0627\"}}","i18n_select_state_text":"\u06cc\u06a9 \u06af\u0632\u06cc\u0646\u0647 \u0627\u0646\u062a\u062e\u0627\u0628 \u0646\u0645\u0627\u0626\u06cc\u062f\u2026","i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631\u2026","i18n_searching":"\u062c\u0633\u062a\u062c\u0648 \u2026"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/country-select.min.js')}}' id='wc-country-select-js'></script>
<script type='text/javascript' id='yith-wcaf-js-extra'>
    /* <![CDATA[ */
    var yith_wcaf = {"labels":{"select2_i18n_matches_1":"One result is available, press enter to select it.","select2_i18n_matches_n":"%qty% results are available, use up and down arrow keys to navigate.","select2_i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","select2_i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","select2_i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","select2_i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","select2_i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631&hellip;","select2_i18n_searching":"\u062c\u0633\u062a\u062c\u0648 &hellip;","link_copied_message":"Url copied"},"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","set_cookie_via_ajax":"","referral_var":"ref","search_products_nonce":"f3c6d3cfd3","set_referrer_nonce":"ca9c7e179a","get_withdraw_amount":"bceab8b49d"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/yith-wcaf.min.js')}}' id='yith-wcaf-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.blockUI.min.js')}}' id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/negarshop\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/negarshop\/?wc-ajax=%%endpoint%%","i18n_view_cart":"\u0645\u0634\u0627\u0647\u062f\u0647 \u0633\u0628\u062f \u062e\u0631\u06cc\u062f","cart_url":"https:\/\/demo.coderboy.ir\/negarshop\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart.min.js')}}' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/js.cookie.min.js')}}' id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/negarshop\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/negarshop\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js' id='woocommerce-js'></script>--}}
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/negarshop\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/negarshop\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_d8e9239e4db63156710984725129ff18","fragment_name":"wc_fragments_d8e9239e4db63156710984725129ff18","request_timeout":"5000"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/cart-fragments.min.js')}}' id='wc-cart-fragments-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/drag-arrange.js')}}' id='dragarrange-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/table-head-fixer.js')}}' id='table-head-fixer-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/perfect-scrollbar.jquery.min.js')}}' id='perfect-scrollbar-js'></script>
<script type='text/javascript' id='wooscp-frontend-js-extra'>
    /* <![CDATA[ */
    var wooscpVars = {"ajaxurl":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","user_id":"5d683352529913f1f848a4c9e3f5b2b8","open_button":"","open_table":"yes","open_bar":"no","click_again":"no","remove_all":"Do you want to remove all products from the compare?","hide_empty":"no","click_outside":"yes","freeze_column":"yes","freeze_row":"yes","limit":"100","limit_notice":"You can add a maximum of {limit} products to the compare table.","nonce":"3cb767fe9e"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/frontend.js')}}' id='wooscp-frontend-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/popper.min.js')}}' id='negarshop-popper-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap.min.js')}}' id='negarshop-bootstrap-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap-notify.min.js')}}' id='negarshop-bootstrap-notify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/nouislider.min.js')}}' id='negarshop-nouislider-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.confetti.js')}}' id='negarshop-confetti-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/owl.carousel.min.js')}}' id='negarshop-owl-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/lightgallery.min.js')}}' id='negarshop-lightbox-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/printThis.min.js')}}' id='negarshop-print-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/accounting.min.js')}}' id='negarshop-accounting-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/select2.js')}}' id='negarshop-select2-js'></script>
<script type='text/javascript' id='negarshop-ajax-tab-carousel-js-extra'>
    /* <![CDATA[ */
    var negarshop_obj = {"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/tab-carousel.js')}}' id='negarshop-ajax-tab-carousel-js'></script>
<script type='text/javascript' id='negarshop-var-product-js-extra'>
    /* <![CDATA[ */
    var negarshop_obj = {"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/variable_product_data.js')}}' id='negarshop-var-product-js'></script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-includes/js/underscore.min.js' id='underscore-js'></script>--}}
<script type='text/javascript' id='wp-util-js-extra'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax":{"url":"\/negarshop\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-includes/js/wp-util.min.js' id='wp-util-js'></script>--}}
<script type='text/javascript' id='wc-add-to-cart-variation-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/negarshop\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"\u0628\u0627 \u0639\u0631\u0636 \u067e\u0648\u0632\u0634\u060c \u0647\u06cc\u0686 \u0643\u0627\u0644\u0627\u064a\u06cc \u0645\u0637\u0627\u0628\u0642 \u0627\u0646\u062a\u062e\u0627\u0628 \u0634\u0645\u0627 \u06cc\u0627\u0641\u062a \u0646\u0634\u062f. \u0644\u0637\u0641\u0627\u064b \u062a\u0631\u06a9\u06cc\u0628 \u062f\u06cc\u06af\u0631\u06cc \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_make_a_selection_text":"\u0644\u0637\u0641\u0627 \u0628\u0631\u062e\u06cc \u0627\u0632 \u06af\u0632\u06cc\u0646\u0647\u200c\u0647\u0627\u06cc \u0645\u062d\u0635\u0648\u0644 \u0631\u0627 \u0642\u0628\u0644 \u0627\u0632 \u0627\u0636\u0627\u0641\u0647 \u06a9\u0631\u062f\u0646 \u0622\u0646 \u0628\u0647 \u0633\u0628\u062f \u062e\u0631\u06cc\u062f\u060c \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_unavailable_text":"\u0628\u0627 \u0639\u0631\u0636 \u067e\u0648\u0632\u0634\u060c \u0627\u06cc\u0646 \u0643\u0627\u0644\u0627 \u062f\u0631 \u062f\u0633\u062a\u0631\u0633 \u0646\u06cc\u0633\u062a. \u0644\u0637\u0641\u0627\u064b \u062a\u0631\u06a9\u06cc\u0628 \u062f\u06cc\u06af\u0631\u06cc \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f."};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart-variation.min.js')}}' id='wc-add-to-cart-variation-js'></script>
<script type='text/javascript' id='negarshop-script-js-extra'>
    /* <![CDATA[ */
    var negarshop_obj = {"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","my_account":"https:\/\/demo.coderboy.ir\/negarshop\/my-account\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/script.js')}}' id='negarshop-script-js'></script>
<script type='text/javascript' id='negarshop-cb-change-price-js-extra'>
    /* <![CDATA[ */
    var negarshop_obj = {"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/price-changes.js')}}' id='negarshop-cb-change-price-js'></script>
<script type='text/javascript' id='negarshop-three-int-js-extra'>
    /* <![CDATA[ */
    var negarshop_obj = {"ajax_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","assets_url":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/themes\/negarshop\/statics\/js\/","my_account":"https:\/\/demo.coderboy.ir\/negarshop\/my-account\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/product-3d.js')}}' id='negarshop-three-int-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify.js')}}' id='negarshop-magnify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify-mobile.js')}}' id='negarshop-magnify-mobile-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.countdown.min.js')}}' id='negarshop-countdown-js-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/yith-color-atts.js')}}' id='yith_wccl_frontend_cb-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnific-popup.min.js')}}' id='dokan-popup-js'></script>
<script type='text/javascript' id='dokan-i18n-jed-js-extra'>
    /* <![CDATA[ */
    var dokan = {"ajaxurl":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","nonce":"498cd8370e","ajax_loader":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/plugins\/dokan-lite\/assets\/images\/ajax-loader.gif","seller":{"available":"\u062f\u0631 \u062f\u0633\u062a\u0631\u0633","notAvailable":"\u0645\u0648\u062c\u0648\u062f \u0646\u06cc\u0633\u062a."},"delete_confirm":"\u0634\u0645\u0627 \u0645\u0637\u0645\u0626\u0646 \u0647\u0633\u062a\u06cc\u062f\u061f","wrong_message":"\u0638\u0627\u0647\u0631\u0627\u064b \u0627\u0634\u062a\u0628\u0627\u0647\u06cc \u0635\u0648\u0631\u062a \u06af\u0631\u0641\u062a\u0647 \u0627\u0633\u062a. \u0644\u0637\u0641\u0627 \u062f\u0648\u0628\u0627\u0631\u0647 \u062a\u0644\u0627\u0634 \u06a9\u0646\u06cc\u062f.","vendor_percentage":"80","commission_type":"percentage","rounding_precision":"6","mon_decimal_point":".","product_types":["simple"],"rest":{"root":"https:\/\/demo.coderboy.ir\/negarshop\/wp-json\/","nonce":"bb72a071da","version":"dokan\/v1"},"api":null,"libs":[],"routeComponents":{"default":null},"routes":[],"urls":{"assetsUrl":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/plugins\/dokan-lite\/assets"}};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/jed.js')}}' id='dokan-i18n-jed-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/login-form-popup.js')}}' id='dokan-login-form-popup-js'></script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-includes/js/wp-embed.min.js' id='wp-embed-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-includes/js/imagesloaded.min.js' id='imagesloaded-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/js/frontend-modules.min.js' id='elementor-frontend-modules-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js' id='elementor-sticky-js'></script>--}}
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
    var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/demo.coderboy.ir\/negarshop\/wp-admin\/admin-ajax.php","nonce":"5cea7468ea","i18n":{"toc_no_headings_found":"\u0647\u06cc\u0686 \u0639\u0646\u0648\u0627\u0646\u06cc \u062f\u0631 \u0627\u06cc\u0646 \u0635\u0641\u062d\u0647 \u06cc\u0627\u0641\u062a \u0646\u0634\u062f."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"google":{"title":"Google+","has_counter":true},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"menu_cart":{"cart_page_url":"https:\/\/demo.coderboy.ir\/negarshop\/cart\/","checkout_page_url":"https:\/\/demo.coderboy.ir\/negarshop\/checkout\/"},"facebook_sdk":{"lang":"fa_IR","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor-pro/assets/js/frontend.min.js' id='elementor-pro-frontend-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js' id='elementor-dialog-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js' id='elementor-waypoints-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js' id='swiper-js'></script>--}}
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js' id='share-link-js'></script>--}}
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u0641\u06cc\u0633\u200c\u0628\u0648\u06a9","shareOnTwitter":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u062a\u0648\u06cc\u06cc\u062a\u0631","pinIt":"\u0633\u0646\u062c\u0627\u0642 \u06a9\u0646","download":"\u062f\u0631\u06cc\u0627\u0641\u062a","downloadImage":"\u062f\u0627\u0646\u0644\u0648\u062f \u062a\u0635\u0648\u06cc\u0631","fullscreen":"\u062a\u0645\u0627\u0645 \u0635\u0641\u062d\u0647","zoom":"\u0628\u0632\u0631\u06af\u0646\u0645\u0627\u06cc\u06cc","share":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc","playVideo":"\u067e\u062e\u0634 \u0648\u06cc\u062f\u06cc\u0648","previous":"\u0642\u0628\u0644\u06cc","next":"\u0628\u0639\u062f\u06cc","close":"\u0628\u0633\u062a\u0646"},"is_rtl":true,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.14","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/demo.coderboy.ir\/negarshop\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"body_background_background":"classic","global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":2019,"title":"%D8%B5%D9%81%D8%AD%D9%87%20%D9%86%D8%AE%D8%B3%D8%AA%20-%D9%85%DB%8C%D9%86%DB%8C%D9%85%D8%A7%D9%84-%D8%A7%D9%84%D9%85%D9%86%D8%AA%D9%88%D8%B1%20-%20%D9%86%DA%AF%D8%A7%D8%B1%D8%B4%D8%A7%D9%BE","excerpt":"","featuredImage":false}};
</script>
{{--<script type='text/javascript' src='https://demo.coderboy.ir/negarshop/wp-content/plugins/elementor/assets/js/frontend.min.js' id='elementor-frontend-js'></script>--}}
</body>
</html>
