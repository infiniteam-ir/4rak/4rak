<!DOCTYPE html>
<html dir="rtl" lang="fa-IR">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>{{$service->title}}| چارک</title>
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />

    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title=" &raquo; خوراک" href="" />
    <link rel="alternate" type="application/rss+xml" title="چارک &raquo; خوراک دیدگاه‌ها" href="" />
    <link rel="alternate" type="application/rss+xml" title="چارک &raquo; خوراک دیدگاه‌ها" href="" />

    <link rel='stylesheet' id='dashicons-css'  href='{{asset('newFront/css/dashicons.min.css')}}' type='text/css' media='all' />
    <style id='dashicons-inline-css' type='text/css'>
        [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='wp-block-library-rtl-css'  href='{{asset('newFront/css/style-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{asset('newFront/css/vendors-style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-rtl-css'  href='{{asset('newFront/css/style-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcaf-css'  href='{{asset('newFront/css/yith-wcaf.css')}}' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='yith_wccl_frontend-css'  href='{{asset('newFront/css/frontend2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='hint-css'  href='{{asset('newFront/css/hint.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-css'  href='{{asset('newFront/css/perfect-scrollbar.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='perfect-scrollbar-wpc-css'  href='{{asset('newFront/css/custom-theme.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-flaticon-css'  href='{{asset('newFront/fonts/flaticon/flaticon.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-fontawesome-css'  href='{{asset('newFront/fonts/fontawesome/fa-all.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-iransans-css'  href='{{asset('newFront/fonts/iransans/iransans.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-bootstrap-css'  href='{{asset('newFront/css/bootstrap.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-nouislider-css'  href='{{asset('newFront/css/nouislider.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owl-css'  href='{{asset('newFront/css/owl.carousel.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-owltheme-css'  href='{{asset('newFront/css/owl.theme.default.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-lightbox-css'  href='{{asset('newFront/css/lightgallery.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-select2-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-compare-css'  href='{{asset('newFront/css/compare-rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-magnify-css'  href='{{asset('newFront/css/magnify.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='negarshop-style-css'  href='{{asset('newFront/css/core.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-builder-frontend-grid-css'  href='{{asset('newFront/css/frontend-grid.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='fw-ext-forms-default-styles-css'  href='{{asset('newFront/css/frontend.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='{{asset('newFront/css/font-awesome.min.css')}}' type='text/css' media='all' />
    <style id='font-awesome-inline-css' type='text/css'>
        [data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>
    <link rel='stylesheet' id='elementor-icons-css'  href='{{asset('newFront/css/elementor-icons.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'  href='{{asset('newFront/css/animations.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-legacy-css'  href='{{asset('newFront/css/frontend-legacy-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1558-css'  href='{{asset('newFront/css/post-1558.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css'  href='{{asset('newFront/css/frontend-rtl.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1562-css'  href='{{asset('newFront/css/post-1562.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1572-css'  href='{{asset('newFront/css/post-1572.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1768-css'  href='{{asset('newFront/css/post-2188.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-style-css'  href='{{asset('newFront/css/style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-rtl-style-css'  href='{{asset('newFront/css/rtl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-select2-css-css'  href='{{asset('newFront/css/select2.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='premmerce-brands-css'  href='{{asset('newFront/css/premmerce-brands.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.6' type='text/css' media='all' />

    <script type="text/template" id="tmpl-unavailable-variation-template">
        <p>با عرض پوزش، این كالا در دسترس نیست. لطفاً ترکیب دیگری را انتخاب کنید.</p>
    </script>

    <link rel="stylesheet" href="{{asset('newFront/css/uikit.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.css">
    <script type='text/javascript' src='{{asset('newFront/js/jquery.min.js')}}' id='jquery-core-js'></script>
{{--    <script type='text/javascript' src='./../../wp-content/plugins/woocommerce-account-funds/assets/js/account-funds.js' id='account_funds-js'></script>--}}
    <script type='text/javascript' src='{{asset('newFront/js/elementor_fixes.js')}}' id='script-ns_elementor_fixes-js'></script>

{{--    <link rel="https://api.w.org/" href="./../../wp-json/index.html" />--}}
{{--    <link rel="alternate" type="application/json" href="./../../wp-json/wp/v2/product/281/index.html" />--}}
{{--    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="./../../xmlrpc.php?rsd" />--}}
{{--    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="./../../wp-includes/wlwmanifest.xml" />--}}
{{--    <meta name="generator" content="WordPress 5.6" />--}}
{{--    <meta name="generator" content="WooCommerce 4.8.0" />--}}
{{--    <link rel='shortlink' href='./../../index.html?p=281' />--}}
{{--    <link rel="alternate" type="application/json+oembed" href="./../../wp-json/oembed/1.0/embed/index.html?url=.%2Fproduct%2F%25d9%2587%25d9%2588%25d8%25a7%25d9%2588%25d8%25a7%25db%258c-%25d9%2585%25db%258c%25d8%25aa-%25d8%25a8%25d9%2588%25da%25a9-x-%25d9%25be%25d8%25b1%25d9%2588-13-9-%25d8%25a7%25db%258c%25d9%2586%25da%2586%2F" />--}}
{{--    <link rel="alternate" type="text/xml+oembed" href="./../../wp-json/oembed/1.0/embed/index.html?url=.%2Fproduct%2F%25d9%2587%25d9%2588%25d8%25a7%25d9%2588%25d8%25a7%25db%258c-%25d9%2585%25db%258c%25d8%25aa-%25d8%25a8%25d9%2588%25da%25a9-x-%25d9%25be%25d8%25b1%25d9%2588-13-9-%25d8%25a7%25db%258c%25d9%2586%25da%2586%2F&#038;format=xml" />--}}
    <meta name="theme-color" content="#00bfd6"/>
    <style>
        body{
            background-color: #eee;

        }
        html, body, .flip-clock-a *, .tooltip, #orders_statistics_chart_container, #orders_statistics_chart_container *{
            font-family: IRANSans_Fa;
        }
        .content-widget article.item figure, article.product figure.thumb, article.w-p-item figure{
            background-image: url({{asset('newFront/images/product-placeholder.png')}});
        }
        /* ------------------------- Normal ------------------------- */
        .content-widget.slider-2.style-2 .product-info .feature-daels-price .sale-price, .ns-add-to-cart-inner .price-update, article.product div.title a:hover, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .woocommerce-LostPassword a, .ns-checkbox input[type="radio"]:checked + label::before, section.blog-home article.post-item.post .content a, a:hover, .header-account .account-box:hover .title, .header-main-nav .header-main-menu > ul > li.loaded:hover>a, section.widget ul li a:hover, .content-widget.slider-2.style-2 .product-info .footer-sec .finished, .content-widget.slider-2.style-2 .product-info .static-title span, article.w-p-item .info > .price span span, article.w-p-item .info > .price span.amount, section.blog-home article.post-item .title .title-tag:hover, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a, .woocommerce-tabs.wc-tabs-wrapper ul.wc-tabs li.active a, .woocommerce.single-product .sale-timer .right .title span, .offer-moments .owl-item .price ins, .offer-moments .owl-item .price span.amount, .page-template-amazing-offer article.product .price ins, .ns-checkbox input[type="checkbox"]:checked+label::before, .content-widget.products-carousel.tabs ul.tabs li.active a, .product .product-sales-count i, .cb-comment-tabs .cb-tabs .active, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a
        {
            color: #00bfd6;
        }
        .product-add-to-cart-sticky .add-product, .negarshop-countdown .countdown-section:last-of-type::before, .select_option.select_option_colorpicker.selected,.select_option.select_option_label.selected, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit:hover, .content-widget.slider.product-archive .wg-title, .lg-actions .lg-next:hover, .lg-actions .lg-prev:hover,.lg-outer .lg-toogle-thumb:hover, .lg-outer.lg-dropdown-active #lg-share,.lg-toolbar .lg-icon:hover, .lg-progress-bar .lg-progress, .dokan-progress>.dokan-progress-bar, #negarshop-to-top>span, .header-search .search-box .action-btns .action-btn.search-submit::after, .select2-container--default .select2-results__option--highlighted[aria-selected], .header-account .account-box:hover .icon, .header-main-nav .header-main-menu > ul > li.loaded:hover>a::after, .btn-negar, .content-widget.slider-2.style-2 .carousel-indicators li.active, .btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled , .navigation.pagination .nav-links a:hover, .woocommerce-message a:active, .woocommerce .onsale, header.section-header a.archive, .woocommerce.single-product .sale-timer .left .discount span, .woocommerce-pagination ul li a:hover , .ui-slider .ui-slider-range, .switch input:checked + .slider, .woocommerce .quantity.custom-num span:hover, .content-widget.slider-2.style-2 .carousel-inner .carousel-item .discount-percent, .sidebar .woocommerce-product-search button, .product-single-ribbons .ribbons>div>span, .content-widget.products-carousel.tabs ul.tabs li a::after, .cb-nouislider .noUi-connect, .comment-users-reviews .progress .progress-bar, .cb-comment-tabs .cb-tabs a::after, .ns-table tbody td.actions a.dislike_product:hover, .ns-store-header .nav-pills .nav-item a.active, .ns-store-avatar header.store-avatar-header
        {
            background-color: #00bfd6;
        }
        .content-widget.slider.product-archive .wg-title, .lg-outer .lg-thumb-item.active, .lg-outer .lg-thumb-item:hover,.btn-primary, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle, .btn-primary.disabled, .btn-primary:disabled, .ui-slider span, .ns-store-avatar header.store-avatar-header .avatar
        {
            border-color: #00bfd6;
        }
        .spinner, nav#main-menu li.loading>a::after
        {
            border-top-color: #00bfd6;
        }
        .content-widget.slider-2.style-2 .carousel-indicators li::before
        {
            border-right-color: #00bfd6;
        }
        .content-widget.slider.product-archive .slide-details .prd-price, .woocommerce-variation-price, .woocommerce p.price > span, .woocommerce p.price ins,.table-cell .woocommerce-Price-amount,#order_review span.amount{
            color: #4caf50;
        }
        /* ------------------------- Importants ------------------------- */
        .woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .product-section .sale-timer-box, .btn-transparent, .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a
        {
            color: #00bfd6 !important;
        }
        .dokan-btn-theme, .wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn{
            background-color: #00bfd6 !important;
        }
        .woocommerce .product .product_meta > span a:hover, .woocommerce .product .product_meta > span span:hover, .dokan-btn-theme{
            border-color: #00bfd6 !important;
        }
        .cls-3{
            fill: #00bfd6;
        }
        .negarshop-countdown .countdown-section:last-of-type{
            background: rgba(0,191,214,0.2);
            color: #00bfd6;
        }
        /* ------------------------- Customs ------------------------- */
        .woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected, .woo-variation-swatches-stylesheet-enabled .variable-items-wrapper .variable-item:not(.radio-variable-item).selected:hover {
            box-shadow: 0 0 0 2px #00bfd6 !important;
        }
        .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active a, .woocommerce nav.woocommerce-MyAccount-navigation ul li.is-active a{
            background-color: rgba(0,191,214,0.1) !important;
        }
        /*-------------basket----------------*/
        .header-cart-basket .cart-basket-box::before, .header-cart-basket.style-2 .cart-basket-box{
            background-color: #00bfd6;
        }
        .header-cart-basket .cart-basket-box, .header-cart-basket.style-2 .cart-basket-box{
            color: #ffffff;
        }
        .header-cart-basket .cart-basket-box > span.count, .header-cart-basket.style-2 .cart-basket-box > span.count{
            background-color: #ffffff;
            color: #00bfd6;
        }
        .header-cart-basket > .widget.widget_shopping_cart ul li a.remove:hover{
            background-color: #00bfd6;
        }
        .header-cart-basket > .widget.widget_shopping_cart p.total{
            color: #00bfd6;
            background-color: rgba(0,191,214,0.1);
        }
        .header-cart-basket > .widget.widget_shopping_cart .buttons .button:hover{
            color: #00bfd6;
        }
        .header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout:hover{
            background-color: #00bfd6;
        }
        article.product figure.thumb, .negarshop-countdown .countdown-section:last-of-type::before, .negarshop-countdown .countdown-section, #negarshopAlertBox, #negarshopAlertBox #closeBtn, .product-alerts .alert-item, .select_option, .select_option *, .product-single-actions, .cb-chips ul.chip-items li, .select2-container--default .select2-selection--single, .header-search.style-5.darken-color-mode .search-box .action-btns .action-btn.search-submit, ul.dokan-account-migration-lists li, .product-summary-left, .header-main-menu.vertical-menu ul.main-menu > li:hover, article.w-p-item, article.w-p-item figure, .img-banner-wg, .btn, .form-control, .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-append > .btn, .input-group-sm > .input-group-append > .input-group-text, .input-group-sm > .input-group-prepend > .btn, .input-group-sm > .input-group-prepend > .input-group-text,#negarshop-to-top > span i,section.widget:not(.widget_media_image), .dokan-widget-area aside.widget:not(.widget_media_image),.content-widget:not(.transparent),.woocommerce.single-product div.product > .product-section, nav.woocommerce-breadcrumb, .woocommerce.single-product div.product .woocommerce-tabs.wc-tabs-wrapper .woocommerce-Tabs-panel, .dokan-other-vendor-camparison,.woocommerce .title-bg,.product-single-actions li a,.woocommerce .quantity.custom-num input.input-text,.woocommerce .quantity.custom-num span,.table-gray,.comment-form-header .comment-notes,.cb-nouislider .noUi-connects,input[type="submit"]:not(.browser-default), input[type="button"]:not(.browser-default), input[type="reset"]:not(.browser-default), .btn, .dokan-btn,article.product,.colored-dots .dot-item,.woocommerce.single-product div.product .product-section.single-style-2-gallery .owl-carousel.wc-product-carousel .owl-item .car-dtag,.owl-carousel.wc-product-carousel .owl-nav button,.owl-carousel.wc-product-carousel-thumbs .owl-item,.account-box .account-links,.account-box .account-links > li a,.header-main-nav .header-main-menu li > ul,.is-mega-menu-con.is-product-mega-menu .tabs a.item-hover,figure.optimized-1-1,.is-mega-menu-con.is-product-mega-menu .owl-carousel .owl-nav button,.content-widget.slider.product-archive figure.thumb,.content-widget.slider .carousel .carousel-indicators li,ul li.wc-layered-nav-rating a,.switch .slider,.switch .slider::before,.woocommerce-products-header,.price_slider_amount button.button,.modal-content, #cbQVModalCarousel .carousel-item,.woocommerce-pagination ul li a, .woocommerce-pagination ul li span,.tile-posts article.blog-item,section.blog-home article.post-item,section.blog-home article.post-item figure.post-thumb,section.blog-home article.post-item .entry-video .inner,.navigation.pagination .nav-links > *,.content-widget.blog-posts article.blog-item figure.thumbnail,.content-widget.blog-posts article.blog-item time,.content-widget.blog-posts article.blog-item,.content-widget.blog-posts .owl-dots button,section.blog-home .post-wg,section.blog-home .post-wg ol.comment-list article.comment-body,section.blog-home article.post-item .tags a,blockquote,nav.top-bar li > ul ,.header-search .search-box .search-result,.product-section .sale-timer-box, .product-section .sale-timer-box .counter-sec,.product-section .sale-timer-box .title .badge,.header-cart-basket.style-2 .cart-basket-box,.header-cart-basket > .widget.widget_shopping_cart .buttons a.checkout,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li a,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li,.woocommerce nav.woocommerce-MyAccount-navigation ul li a,.woocommerce-message, .woocommerce-error,.woocommerce-message a, .woocommerce-page.woocommerce-cart table.shop_table td.product-remove a.remove, .cart-collaterals .cart_totals,ul#shipping_method li label,.list-group,ul.woocommerce-order-overview, .header-cart-basket > .widget.widget_shopping_cart > .widget_shopping_cart_content,.header-cart-basket > .widget.widget_shopping_cart,footer.site-footer .about-site,ul.product-categories li.open .children,.wooscp-area .wooscp-inner .wooscp-bar .wooscp-bar-btn,.woocommerce-tabs.wc-tabs-wrapper > div.woocommerce-Tabs-panel .product-seller .store-avatar img,.ns-table tbody td.actions a.dislike_product,.is-mega-menu-con.is-product-mega-menu li.contents,footer.site-footer .support-times,li.recentcomments,section.widget.widget_media_image img,.header-main-menu.vertical-menu ul.main-menu,.prm-brands-list__item,.prm-brands-list__item .prm-brands-list__title,.content-widget.slider .carousel,.content-widget.title-widget span.icon,.products-carousel .carousel-banner,footer.site-footer .footer-socials ul li a,header.site-header .header-socials ul li a,.header-search .search-box .action-btns{
            -webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px;
        }
        .product-video-carousel .owl-nav button, .input-group .form-control:not(select), textarea:not(.browser-default), .dokan-form-control, .form-control, input[type="text"]:not(.browser-default), input[type="search"]:not(.browser-default), input[type="email"]:not(.browser-default), input[type="url"]:not(.browser-default), input[type="number"]:not(.browser-default) ,.input-group .btn,.dokan-message, .dokan-info, .dokan-error{
            -webkit-border-radius: 20px !important; -moz-border-radius: 20px !important; border-radius: 20px !important;
        }
        .header-search.style-3 .search-box .action-btns {
            border-radius: 20px 0 0 20px;
        }
        .woocommerce-account:not(.logged-in) article.post-item .nav-pills, .woocommerce-account:not(.logged-in) article.post-item .negarshop-userlogin .nav-pills .nav-item .active, .ns-store-avatar header.store-avatar-header,.woocommerce-page.woocommerce-cart table.shop_table tr:first-of-type td.product-subtotal{
            border-radius: 20px 20px 0 0;
        }
        .ns-store-avatar.wc-dashboard .user-actions a,.woocommerce-page.woocommerce-cart table.shop_table tr:nth-last-of-type(2) td.product-subtotal{
            border-radius: 0 0 20px 20px;
        }
        .ns-table tbody tr td:first-of-type {
            border-radius: 0 20px 20px 0;
        }
        .ns-table tbody tr td:last-of-type {
            border-radius: 20px 0 0 20px;
        }
        .comment .comment-awaiting-moderation::before{content: 'در انتظار تایید مدیریت'} .woocommerce-variation-price:not(:empty)::before{content: 'قیمت: '} .woocommerce-pagination ul li a.next::before{content: 'بعدی'} .woocommerce-pagination ul li a.prev::before{content: 'قبلی'} .woocommerce .quantity.custom-num label.screen-reader-text::before{content: 'تعداد: '} .yith-woocompare-widget ul.products-list li .remove::after{content: 'حذف'} .woocommerce .product .product_meta > span.product-brand::before{content: 'برند: '} .show-ywsl-box::before{content: 'برای ورود کلیک کنید'} a.reset_variations::before{content: 'پاک کردن ویژگی ها'} .woocommerce form .form-row .required::before{content: '(ضروری)'} .content-widget.price-changes .prices-table tbody td.past-price::before{content: 'قیمت قبل: '} .content-widget.price-changes .prices-table tbody td.new-price::before{content: 'قیمت جدید: '} .content-widget.price-changes .prices-table tbody td.changes::before{content: 'تغییرات: '} .content-widget.price-changes .prices-table tbody td.difference::before{content: 'مابه التفاوت: '} </style>
    <script type='text/javascript'>var jsVars = {"borderActiveColor":"#00bfd6"};</script>
    <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link rel="icon" href="./../../wp-content/uploads/2019/05/Inipagi-Business-Economic-Store.ico" sizes="32x32" />
    <link rel="icon" href="{{asset('newFront/images/4rak-logo.png')}}" sizes="192x192" />
    <link rel="apple-touch-icon" href="{{asset('newFront/images/4rak-logo.png')}}" />
    <meta name="msapplication-TileImage" content="{{asset('newFront/images/4rak-logo.png')}}" />
    <style type="text/css" id="wp-custom-css">
        .baby-newspaper .elementor-widget-container .widget_mc4wp_form_widget{
            background:none !important
        }		</style>
</head>
<body class="rtl product-template-default single single-product postid-281 res-banner-bg-trans pop-up-login theme-negarshop woocommerce woocommerce-page woocommerce-no-js elementor-default elementor-kit-1558 dokan-theme-negarshop">
<div class="wrapper">
    <span class="front-ajax d-none" id="{{route('frontAjax')}}"></span>

    <header class="site-header d-none d-xl-block d-lg-block">
        <div data-elementor-type="header" data-elementor-id="1562" class="elementor elementor-1562 elementor-location-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-80c8889 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="80c8889" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-6b78921" data-id="6b78921" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7fa6280 elementor-widget elementor-widget-image" data-id="7fa6280" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    {{--LOGO--}}
                                                    <a href="{{route('store')}}">
                                                        <img width="143" height="58" src="{{asset('img/4rak-logo.png')}}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/4rak-logo.png')}} 143w, {{asset('img/4rak-logo.png')}} 50w" sizes="(max-width: 143px) 100vw, 143px" />
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-1bacd51" data-id="1bacd51" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-4dc7224 elementor-widget elementor-widget-negarshop_header_search" data-id="4dc7224" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-right darken-color-mode style-3 style-4 whiter">
                                                    {{-- SEARCH--}}
                                                    <div data-type="product" class="search-box btn-style-dark ajax-form">
                                                        <form class="form-tag" action="#">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-filters" type="button"><i class="flaticon-settings-1"></i></button>
                                                                <button class="action-btn search-submit" type="submit"><i class="far fa-search"></i>
                                                                </button>
                                                            </div>

                                                            <div class="search-options">
                                                                <button class="close-popup" type="button"><i class="fal fa-check"></i></button>
                                                                <div class="filters-parent">
                                                                    <div class="list-item">
                                                                        <label for="header-search-cat">دسته بندی ها</label>
                                                                        <select name="pcat" id="header-search-cat" class="negar-select">
                                                                            <option value="" selected>همه دسته ها</option>
                                                                            <option value="15" >کالای دیجیتال</option>
                                                                            <option value="121" >اسباب بازی</option>
                                                                            <option value="102" >تی شرت</option>
                                                                            <option value="103" >سویشرت و هودی</option>
                                                                            <option value="101" >شلوار</option>
                                                                            <option value="46" >کامپیوتر و تجهیزات جانبی</option>
                                                                            <option value="104" >کفش ورزشی</option>
                                                                            <option value="44" >لپ تاپ</option>
                                                                            <option value="92" >لوازم جانبی موبایل</option>
                                                                            <option value="86" >لوازم خانگی</option>
                                                                            <option value="62" >مد و پوشاک</option>
                                                                            <option value="42" >موبایل</option>
                                                                            <option value="120" >نوزاد</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="list-item">
                                                                        <label for="header-search-stock">وضعیت محصول</label>
                                                                        <select name="stock" id="header-search-stock" class="negar-select">
                                                                            <option value="" selected>همه محصولات</option>
                                                                            <option value="instock" >فقط موجود ها</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <ul class="search-result"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-bcd4445" data-id="bcd4445" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-2b720eb elementor-widget elementor-widget-negarshop_header_account" data-id="2b720eb" data-element_type="widget" data-widget_type="negarshop_header_account.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-account text-left style-1">
                                                    <div class="account-box">
                                                      <span class="icon">
                                                          <i class="flaticon-avatar"></i>
                                                      </span>
                                                        @guest
                                                        <span class="title">حساب کاربری</span>
                                                        <span class="subtitle">لطفا وارد حساب خود شوید!</span>

                                                        <ul class="account-links">
                                                            <li>
                                                                <a href="{{route('login')}}" data-toggle="modal" data-target="#login-popup-modal">ورود به حساب</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('login')}}">ثبت نام</a>
                                                            </li>
                                                        </ul>
                                                            @else
                                                            <span class="title">حساب کاربری</span>
                                                            <span class="subtitle">{{Auth::user()->name}} {{Auth::user()->family}}</span>

                                                            <ul class="account-links">
                                                                <li>
                                                                    <a href="{{route('Profile.index')}}" data-toggle="modal" data-target="#login-popup-modal">پروفایل</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{route('dashboard')}}"> پنل کاربری</a>
                                                                </li>
                                                            </ul>
                                                        @endguest
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-286854c elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="286854c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-narrow">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-a24c8ec" data-id="a24c8ec" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-38f7b83 elementor-widget elementor-widget-negarshop_header_menu" data-id="38f7b83" data-element_type="widget" data-widget_type="negarshop_header_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="">
                                                    <nav class="header-main-nav header_main_nav_1 style-2 transparent">
                                                        <div class="row align-items-center">
                                                            <div class="col header-main-menu-col header-main-menu text-right">
                                                                <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c" class="main-menu">
                                                                    @foreach($categories as $cat)
                                                                        @if($cat->shop_id==0 && $cat->parent_id==null)
                                                                        <li id="menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-has-icon menu-item-{{$cat->id}}" data-col="4" data-id="{{$cat->id}}">
                                                                            <a href="#" ><i class="item-icon" style="background-image: url('https://demo.coderboy.ir/negarshop/wp-content/uploads/2019/09/home.png');"></i> {{$cat->title}}</a>
                                                                        </li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5fa7d22" data-id="5fa7d22" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-519427f elementor-widget elementor-widget-negarshop_header_basket" data-id="519427f" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-2 child-hover-right">
                                                    <a href="{{route('ShoppingCart')}}" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۰</bdi></span></span>
                                                        <span class="count">0</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>
    <header class="responsive-header d-block d-xl-none d-lg-none">
        <div data-elementor-type="section" data-elementor-id="2188" class="elementor elementor-2188 elementor-location-responsive-header" data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-345f71c elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="345f71c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-79a8412" data-id="79a8412" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-082d926 elementor-widget elementor-widget-negarshop_responsive_menu" data-id="082d926" data-element_type="widget" data-widget_type="negarshop_responsive_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="responsive-menu">
                                                    <button class="btn btn-transparent toggle-menu"><i class="far fa-bars"></i></button>
                                                    <div class="menu-popup">
                                                        <nav class="responsive-navbar">
                                                            <ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%af%d8%b3%d8%aa%d9%87-%d8%a8%d9%86%d8%af%db%8c-1" class="categories-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-188"><a href="./../-category/%da%a9%d8%a7%d9%84%d8%a7%db%8c-%d8%af%db%8c%d8%ac%db%8c%d8%aa%d8%a7%d9%84/index.html">کالای دیجیتال</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-190"><a href="#">آرایشی و بهداشتی</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-191"><a href="#">ابزار و اداری</a>
                                                                    <ul class="sub-menu">
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-497"><a href="#">دریل، پیچ گوشتی، چکشی</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-498"><a href="#">اره برقی</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-499"><a href="#">کارواش</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-502"><a href="#">لوازم مصرفی خودرو</a>
                                                                            <ul class="sub-menu">
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-503"><a href="#">انواع فیلتر</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-504"><a href="#">روغن موتور و ضد یخ</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-505"><a href="#">مکمل سوخت و روغن</a></li>
                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-506"><a href="#">یدکی، لاستیک، لامپ و چراغ</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-500"><a href="#">مکنده و دمنده</a></li>
                                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-501"><a href="#">فرز و سنگ رومیزی</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-189"><a href="./../-category/%d9%85%d8%af-%d9%88-%d9%be%d9%88%d8%b4%d8%a7%da%a9/index.html">مد و پوشاک</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-mega-menu menu-item-192"><a href="#">خانه و آشپزخانه</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-193"><a href="#">لوازم تحریر و هنر</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-194"><a href="#">کودک و نوزاد</a></li>
                                                            </ul>                </nav>
                                                    </div>
                                                    <div class="overlay"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-6503f75" data-id="6503f75" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-974f707 elementor-widget elementor-widget-image" data-id="974f707" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <a href="{{route('store')}}">
                                                        <img width="143" height="58" src="{{asset('img/4rak-logo.png')}}" class="attachment-full size-full" alt="" loading="lazy" srcset="{{asset('img/4rak-logo.png')}} 143w, {{asset('img/4rak-logo.png')}} 50w" sizes="(max-width: 143px) 100vw, 143px" />								</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-18a1aeb" data-id="18a1aeb" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-704fc73 elementor-view-default elementor-widget elementor-widget-icon" data-id="704fc73" data-element_type="widget" data-widget_type="icon.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-wrapper">
                                                    <a class="elementor-icon" href="{{route('dashboard')}}">
                                                        <i aria-hidden="true" class="far fa-user"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-87822a2" data-id="87822a2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-92646d4 elementor-widget elementor-widget-negarshop_header_basket" data-id="92646d4" data-element_type="widget" data-widget_type="negarshop_header_basket.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-cart-basket style-3 child-hover-right">
                                                    <a href="{{route('ShoppingCart')}}" class="cart-basket-box cart-customlocation">
                                                        <span class="icon"><i class="flaticon-shopping-bag"></i></span>
                                                        <span class="title">سبد خرید</span>
                                                        <span class="subtitle"><span class="woocommerce-Price-amount amount">
                                                                <bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۰</bdi></span></span>
                                                        <span class="count">0</span>
                                                    </a>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-275eab9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="275eab9" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28b1d92" data-id="28b1d92" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7ea9db8 elementor-widget elementor-widget-negarshop_header_search" data-id="7ea9db8" data-element_type="widget" data-widget_type="negarshop_header_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="header-search search-in-product text-right darken-color-mode style-5 whiter">        <div data-type="product" class="search-box btn-style-dark ajax-form">
                                                        <form class="form-tag" action="#">
                                                            <input type="hidden" name="post_type" value="product"/>
                                                            <input type="search" autocomplete="off" name="s" value=""
                                                                   class="search-input search-field" placeholder="جستجو در محصولات ...">

                                                            <div class="action-btns">
                                                                <button class="action-btn search-submit" type="submit"><i class="far fa-search"></i>
                                                                </button>
                                                            </div>

                                                        </form>
                                                        <ul class="search-result"></ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </header>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="woocommerce-notices-wrapper"></div>
                <div id="product-281" class="product type-product post-281 status-publish first instock product_cat-15 product_cat-44 product_tag-80 has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="product-section">
                        <div class="row">
                            <div class="col-12 py-2 justify-content-start">
                                <a class="btn btn-purple py-2" href="{{route('singleshop',$service->shop->unique_name)}}">بازگشت به فروشگاه</a>
                            </div>
                            <div class="col-lg-4 mb-5 mb-xl-0 mb-lg-0">
                            {{-- PRODUCT IMAGE--}}
                                <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 0; transition: opacity .25s ease-in-out;">
                                    <div class="owl-carousel wc-product-carousel images">
                                        @if ($service->photo)
                                            <div class="car-dtag" data-src="{{asset('images/services/'.$service->photo->path)}}">
                                                <a href="{{asset('images/services/'.$service->photo->path)}}" class="img-magnifier-container">
                                                    <img class="product-gallery-img" data-magnify-src="{{asset('images/services/'.$service->photo->path)}}" src="{{asset('images/services/'.$service->photo->path)}}" alt="{{$service->name}}" id="wc-carousel-image-0" />
                                                </a>
                                            </div>
                                        @else
                                            <div class="car-dtag" data-src="{{asset('img/default.jpg')}}">
                                                <a href="{{asset('img/default.jpg')}}" class="img-magnifier-container">
                                                    <img class="product-gallery-img" data-magnify-src="{{asset('img/default.jpg')}}" src="{{asset('img/default.jpg')}}" alt="{{$service->name}}" id="wc-carousel-image-0" />
                                                </a>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                           {{--END PRODUCT IMAGE--}}

                            </div>
                            <div class="col-lg">
                                <div class="title-bg">
                                    <h1 class="product_title entry-title">{{$service->title}}</h1>
                                </div>
                                <div class="row">
                                    <div class="col-lg">
                                        <div class="row">
                                            <div class="col-lg mb-3">
                                                <div class="summary entry-summary">
                                                    <div class="product-featured-attrs">
                                                        <p class="text-muted">
                                                            <?php
                                                            echo  Str::limit($service->description,40,'(...)')
                                                            ?>
                                                        </p>

                                                    </div>
                                                    <div class="product-alerts">
                                                        <div class="alert-item" style="background-color: rgba(247,169,0,0.1); color: rgba(247,164,0,0.65);">
                                                            <span class="icon">
                                                                <i class="fa fa-hourglass-end"></i>
                                                            </span>
                                                            <span class="title">در سریع ترین زمان ممکن تحویل داده میشود.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg">
                                                <div class="product-summary-left not-sticky">
                                                    <div class="details vendor">
                                                        <span class="title"> ارائه دهنده:</span>
                                                        <a href="{{route('singleshop',$service->shop->id)}}">{{$service->shop->name}}</a>
                                                    </div>


                                                    <p class="price mt-0"><span class="price-text">قیمت: </span>
                                                        <span class="woocommerce-Price-amount amount">
                                                                <bdi>
                                                                    <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;
                                                                    {{number_format($service->price)}}
                                                                </bdi>
                                                        </span>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="product-add-to-cart-sticky">
                            <div class="price-outer">
                                <div class="price">خرید محصول</div>
                            </div>
                            <button class="add-product"><i class="far fa-cart-plus"></i></button>
                        </div>
                    </div>

                    </div>

                </div>
        </main>
    </div>
</div>


<div class="container">
    <ul class="fixed-bottom-bar style-2">
        <li><a class="btn wooscp-btn" href="{{route('ShoppingCart')}}" data-toggle="tooltip" data-placement="top" title="سبد خرید"><i class="fal fa-shopping-cart"></i></a></li>
        <li><a class="btn" href="#" data-toggle="tooltip" data-placement="top" title="علاقه مندی ها"><i class="far fa-heart"></i></a></li>
        <li><a class="btn" href="javascript:void(0);" id="negarshop-to-top"><span><i class="far fa-angle-up"></i></span></a></li>
    </ul>
</div>

@include('front.newTheme.footer')

</div>
<script>
    (function() {function maybePrefixUrlField() {
        if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
            this.value = "http://" + this.value;
        }
    }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields) {
            for (var j=0; j < urlFields.length; j++) {
                urlFields[j].addEventListener('blur', maybePrefixUrlField);
            }
        }
    })();
</script>
<div class="wooscp-search">
    <div class="wooscp-search-inner">
        <div class="wooscp-search-content">
            <div class="wooscp-search-content-inner">
                <div class="wooscp-search-close"></div>
                <div class="wooscp-search-input">
                    <input type="search" id="wooscp_search_input"
                           placeholder="عنوان محصول مورد نظر را وارد کنید..."/>
                </div>
                <div class="wooscp-search-result"></div>
            </div>
        </div>
    </div>
</div>
<div id="wooscp-area" class="wooscp-area wooscp-bar-bottom wooscp-bar-right wooscp-hide-checkout"
     data-bg-color="#292a30"
     data-btn-color="#00a0d2">
    <div class="wooscp-inner">
        <div class="wooscp-table">
            <div class="wooscp-table-inner">
                <span id="wooscp-table-close" class="wooscp-table-close">بستن</span>                                    <div class="wooscp-table-items"></div>
            </div>
        </div>
        <div class="wooscp-bar">

            <div class="wooscp-bar-items"></div>
            <div class="wooscp-bar-btn wooscp-bar-btn-text">
                <div class="wooscp-bar-btn-icon-wrapper">
                    <div class="wooscp-bar-btn-icon-inner"><span></span><span></span><span></span>
                    </div>
                </div>
                مقایسه                                </div>
        </div>
    </div>
</div>
<script>
    var loadJS = function(url, implementationCode, location){
        //url is URL of external file, implementationCode is the code
        //to be called from the file, location is the location to
        //insert the <script> element

        var scriptTag = document.createElement('script');
        scriptTag.src = url;

        scriptTag.onload = implementationCode;
        scriptTag.onreadystatechange = implementationCode;

        location.appendChild(scriptTag);
    };
    var loadLazyloadPlugin = function(){
        var myLazyLoad = new LazyLoad({
            elements_selector: ".lazy"
        });
        jQuery(document).bind("ajaxComplete", function($){
            myLazyLoad.update();
        });
    }
    loadJS("./wp-content/themes/negarshop/statics/js/lazyload.min.js", loadLazyloadPlugin, document.body);
    var defaultText = {
        searchArchive:"نتیجه مورد نظر را پیدا نکردید؟",searchAllBtn:"مشاهده همه",searchNotFound:"چیزی پیدا نشد!",errorSend:"مشکلی هنگام ارسال پیش آمد!",bad:"بد",medium:"متوسط",good:"خوب",excelent:"عالی",verybad:"خیلی بد",pleaseWait:"لطفا صبر کنید ...",
    };
</script>
<div class="modal fade" id="login-popup-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <form class="header-popup-login">
            <div class="modal-content">

                <button type="button" class="btn close-modal cb-custom-close" data-dismiss="modal">
                    <i class="far fa-times"></i>
                </button>
                <div class="modal-body p-1">
                    <h6 class="modal-title">ورود به حساب کاربری</h6>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fal fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" id="login-username"
                               placeholder="نام کاربری یا ایمیل"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fal fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control" id="login-pass"
                               placeholder="گذرواژه"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>


                    <div class="wc-social-login">
                        <style>
                            a.ywsl-social{
                                text-decoration: none;
                                display: inline-block;
                                margin-right: 2px;
                            }
                        </style>
                        <p class="ywsl-label">ورود با:</p>
                        <p class="socials-list">
                            <a class="ywsl-social ywsl-facebook" href="./../../wp-login.php?ywsl_social=facebook&#038;redirect=.%2Fproduct%2F%25D9%2587%25D9%2588%25D8%25A7%25D9%2588%25D8%25A7%25DB%258C-%25D9%2585%25DB%258C%25D8%25AA-%25D8%25A8%25D9%2588%25DA%25A9-x-%25D9%25BE%25D8%25B1%25D9%2588-13-9-%25D8%25A7%25DB%258C%25D9%2586%25DA%2586%2F"><img src="{{asset('asset/img/facebook.png')}}" alt="Facebook"/></a><a class="ywsl-social ywsl-twitter" href="./../../wp-login.php?ywsl_social=twitter&#038;redirect=.%2Fproduct%2F%25D9%2587%25D9%2588%25D8%25A7%25D9%2588%25D8%25A7%25DB%258C-%25D9%2585%25DB%258C%25D8%25AA-%25D8%25A8%25D9%2588%25DA%25A9-x-%25D9%25BE%25D8%25B1%25D9%2588-13-9-%25D8%25A7%25DB%258C%25D9%2586%25DA%2586%2F"><img src="{{asset('img/twitter.png')}}" alt="Twitter"/></a><a class="ywsl-social ywsl-google" href="./../../wp-login.php?ywsl_social=google&#038;redirect=.%2Fproduct%2F%25D9%2587%25D9%2588%25D8%25A7%25D9%2588%25D8%25A7%25DB%258C-%25D9%2585%25DB%258C%25D8%25AA-%25D8%25A8%25D9%2588%25DA%25A9-x-%25D9%25BE%25D8%25B1%25D9%2588-13-9-%25D8%25A7%25DB%258C%25D9%2586%25DA%2586%2F"><img src="./../../wp-content/plugins/yith-woocommerce-social-login/assets/images/google.png" alt="Google"/></a></p></div>                            <div class="form-group ns-checkbox mt-4 mb-0">
                        <input type="checkbox" class="form-check-input" id="login-remember">
                        <label class="form-check-label"
                               for="login-remember">مرا به خاطر بسپار</label>
                    </div>
                    <div class="my-2">
                        <a href="./../../my-account/lost-password/index.html">رمز عبور خود را فراموش کرده اید؟</a>
                    </div>
                    <p id="login-res" class="float-right my-3 d-none"></p>
                </div>
                <div class="modal-footer d-flex">
                    <a href="./../../my-account/index.html?register"
                       class="btn btn-transparent">ثبت نام</a>
                    <button class="btn btn-primary float-left"
                            id="login-submit">ورود به حساب <i
                            class="far fa-angle-left"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade cb-quick-view-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn close-modal" data-dismiss="modal">
                <i class="far fa-times"></i>
            </button>
            <div class="w-100"></div>
            <div class="loading"><span class="spinner"></span></div>
            <div class="content"></div>
        </div>
    </div>
</div>
<!-- Product survey Modal -->
<div class="modal fade" id="product-survey" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">آیا از قیمت های ما رضایت دارید؟</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <figure class="thumbnail"><img width="150" height="150" src="./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-150x150.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" loading="lazy" srcset="./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-150x150.jpg 150w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-300x300.jpg 300w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-768x768.jpg 768w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-400x400.jpg 400w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-50x50.jpg 50w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-600x600.jpg 600w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-100x100.jpg 100w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1-96x96.jpg 96w, ./../../wp-content/uploads/2019/05/8149py2u1cL._SL1500_-1.jpg 1000w" sizes="(max-width: 150px) 100vw, 150px" /></figure>
                    </div>
                    <div class="col">
                        <p class="title">هواوای میت بوک X پرو 13.9 اینچ</p>
                        <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;۸,۰۰۰,۰۰۰</bdi></span></p>
                    </div>
                </div>
                <form method="post" id="product-survey-form">
                    <div class="form-group">
                        <label for="desc1">این کالا را با چه قیمتی دیده‌اید؟</label>
                        <input type="text" class="form-control" name="desc1" id="desc1" required>
                    </div>
                    <div class="form-group">
                        <label for="desc2">آدرس اینترنتی یا واقعی فروشگاهی</label>
                        <input type="text" class="form-control" name="desc2" id="desc2" required>
                    </div>
                    <input type="hidden" value="281" name="product">
                    <input type="hidden" id="_wpnonce" name="_wpnonce" value="bc3f7b9257" /><input type="hidden" name="_wp_http_referer" value="/wp/product/%D9%87%D9%88%D8%A7%D9%88%D8%A7%DB%8C-%D9%85%DB%8C%D8%AA-%D8%A8%D9%88%DA%A9-x-%D9%BE%D8%B1%D9%88-13-9-%D8%A7%DB%8C%D9%86%DA%86/" />                        <button type="submit" class="btn btn-primary"><i class="far fa-check"></i> ثبت نظر</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade product-features-modal" id="product-modal-1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">امکان تحویل اکسپرس</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود .</p>            </div>
        </div>
    </div>
</div>
<div class="modal fade product-features-modal" id="product-modal-2" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">۷ روز هفته ۲۴ ساعته</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود .</p>            </div>
        </div>
    </div>
</div>
<div class="modal fade product-features-modal" id="product-modal-3" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">امکان پرداخت در محل</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود .</p>            </div>
        </div>
    </div>
</div>
<div class="modal fade product-features-modal" id="product-modal-4" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">هفت روز ضمانت بازگشت کالا</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود .</p>            </div>
        </div>
    </div>
</div>
<div class="modal fade product-features-modal" id="product-modal-5" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ضمانت اصل بودن کالا</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود .</p>            </div>
        </div>
    </div>
</div>
<script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@graph":[{"@context":"https:\/\/schema.org\/","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"name":"\u062e\u0627\u0646\u0647","@id":"."}},{"@type":"ListItem","position":2,"item":{"name":"\u06a9\u0627\u0644\u0627\u06cc \u062f\u06cc\u062c\u06cc\u062a\u0627\u0644","@id":".\/product-category\/%da%a9%d8%a7%d9%84%d8%a7%db%8c-%d8%af%db%8c%d8%ac%db%8c%d8%aa%d8%a7%d9%84\/"}},{"@type":"ListItem","position":3,"item":{"name":"\u0644\u067e \u062a\u0627\u067e","@id":".\/product-category\/%da%a9%d8%a7%d9%84%d8%a7%db%8c-%d8%af%db%8c%d8%ac%db%8c%d8%aa%d8%a7%d9%84\/%d9%84%d9%be-%d8%aa%d8%a7%d9%be\/"}},{"@type":"ListItem","position":4,"item":{"name":"\u0647\u0648\u0627\u0648\u0627\u06cc \u0645\u06cc\u062a \u0628\u0648\u06a9 X \u067e\u0631\u0648 13.9 \u0627\u06cc\u0646\u0686","@id":".\/product\/%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-%d9%85%db%8c%d8%aa-%d8%a8%d9%88%da%a9-x-%d9%be%d8%b1%d9%88-13-9-%d8%a7%db%8c%d9%86%da%86\/"}}]},{"@context":"https:\/\/schema.org\/","@type":"Product","@id":".\/product\/%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-%d9%85%db%8c%d8%aa-%d8%a8%d9%88%da%a9-x-%d9%be%d8%b1%d9%88-13-9-%d8%a7%db%8c%d9%86%da%86\/#product","name":"\u0647\u0648\u0627\u0648\u0627\u06cc \u0645\u06cc\u062a \u0628\u0648\u06a9 X \u067e\u0631\u0648 13.9 \u0627\u06cc\u0646\u0686","url":".\/product\/%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-%d9%85%db%8c%d8%aa-%d8%a8%d9%88%da%a9-x-%d9%be%d8%b1%d9%88-13-9-%d8%a7%db%8c%d9%86%da%86\/","description":"\u06a9\u0627\u0645\u067e\u06cc\u0648\u062a\u0631\u0647\u0627\u06cc \u0628\u062f\u0648\u0646 \u06a9\u06cc\u0633 \u00abApple\u00bb \u0628\u0627 \u0646\u0627\u0645 iMac \u062c\u0632\u0648 \u0645\u0639\u062f\u0648\u062f \u062f\u0633\u062a\u06af\u0627\u0647\u200c\u0647\u0627\u06cc \u0647\u0645\u0647\u200c\u06a9\u0627\u0631\u0647 (All-in-One) \u0645\u062d\u0633\u0648\u0628 \u0645\u06cc\u200c\u0634\u0648\u0646\u062f \u06a9\u0647 \u062a\u0648\u0627\u0646 \u0648 \u06a9\u0627\u0631\u0627\u06cc\u06cc \u0628\u0627\u0644\u0627\u06cc\u06cc \u062f\u0627\u0634\u062a\u0647 \u0648 \u0628\u0633\u06cc\u0627\u0631\u06cc \u0627\u0632 \u0637\u0631\u0627\u062d\u0627\u0646 \u06af\u0631\u0627\u0641\u06cc\u06a9\u060c \u0645\u0647\u0646\u062f\u0633\u0627\u0646 \u0646\u0642\u0634\u0647\u200c\u06a9\u0634\u060c \u062a\u062f\u0648\u06cc\u0646\u06af\u0631\u0647\u0627\u06cc \u0641\u06cc\u0644\u0645 \u0648 \u0648\u06cc\u062f\u0626\u0648\u060c \u0633\u0627\u0632\u0646\u062f\u06af\u0627\u0646 \u0645\u062f\u0644\u200c\u0647\u0627\u06cc \u0633\u0647\u200c\u0628\u0639\u062f\u06cc \u0648 \u06a9\u0627\u0631\u0628\u0631\u0627\u0646 \u062d\u0631\u0641\u0647\u200c\u0627\u06cc \u0627\u0632 \u0627\u06cc\u0646 \u0633\u0631\u06cc \u062f\u0633\u062a\u06af\u0627\u0647\u200c\u0647\u0627 \u0627\u0633\u062a\u0641\u0627\u062f\u0647 \u0645\u06cc\u200c\u06a9\u0646\u0646\u062f. \u062c\u062f\u06cc\u062f\u062a\u0631\u06cc\u0646 \u0646\u0633\u0644 \u0627\u06cc\u0646 \u0645\u062d\u0635\u0648\u0644\u0627\u062a \u06a9\u0647 \u062f\u0631 \u0645\u06cc\u0627\u0646\u0647 \u0633\u0627\u0644 2017 \u0639\u0631\u0636\u0647 \u0634\u062f\u0647\u060c \u06cc\u06a9 \u06a9\u0627\u0645\u067e\u06cc\u0648\u062a\u0631 \u062a\u0645\u0627\u0645\u200c\u0639\u06cc\u0627\u0631 \u0627\u0633\u062a \u06a9\u0647 \u0627\u0632 \u0644\u062d\u0627\u0638 \u0637\u0631\u0627\u062d\u06cc \u0648 \u06a9\u06cc\u0641\u06cc\u062a \u0633\u0627\u062e\u062a\u060c \u0642\u062f\u0631\u062a \u0633\u062e\u062a\u200c\u0627\u0641\u0632\u0627\u0631\u06cc \u0648 \u06a9\u06cc\u0641\u06cc\u062a \u0635\u0641\u062d\u0647\u200c\u200c\u0646\u0645\u0627\u06cc\u0634 \u0628\u062f\u0648\u0646 \u0631\u0642\u06cc\u0628 \u0627\u0633\u062a. \u0646\u0633\u0628\u062a \u0628\u0647 \u0645\u062f\u0644\u200c\u0647\u0627\u06cc\u06cc \u06a9\u0647 \u062f\u0631 \u0645\u06cc\u0627\u0646\u0647 \u0633\u0627\u0644 2015 \u0639\u0631\u0636\u0647 \u0634\u062f\u0646\u062f\u060c \u062a\u063a\u06cc\u06cc\u0631\u06cc \u062f\u0631 \u0628\u062f\u0646\u0647 \u0648 \u0634\u06a9\u0644 \u0638\u0627\u0647\u0631\u06cc \u0646\u0633\u0644 \u062c\u062f\u06cc\u062f \u0627\u06cc\u0646 \u062f\u0633\u062a\u06af\u0627\u0647 \u0627\u06cc\u062c\u0627\u062f \u0646\u0634\u062f\u0647 \u0648 \u0647\u0645\u0686\u0646\u0627\u0646 \u0628\u0627 \u0647\u0645\u0627\u0646 \u0628\u062f\u0646\u0647\u200c\u06cc \u0622\u0644\u0648\u0645\u06cc\u0646\u06cc\u0648\u0645\u06cc \u062e\u06cc\u0631\u0647\u200c\u06a9\u0646\u0646\u062f\u0647 \u0648 \u0686\u0634\u0645\u200c\u0646\u0648\u0627\u0632 \u0645\u0648\u0627\u062c\u0647 \u0647\u0633\u062a\u06cc\u0645. \u0628\u062f\u0646\u0647\u200c\u0627\u06cc \u0628\u0627\u0631\u06cc\u06a9 \u06a9\u0647 \u062f\u0631 \u0642\u0633\u0645\u062a \u0644\u0628\u0647\u200c\u0647\u0627\u060c \u0641\u0642\u0637 5 \u0645\u06cc\u0644\u06cc\u200c\u0645\u062a\u0631 \u0636\u062e\u0627\u0645\u062a \u062f\u0627\u0631\u062f \u0648 \u0628\u0627 \u0628\u0627\u0644\u0627\u062a\u0631\u06cc\u0646 \u062d\u062f\u00a0\u0627\u0633\u062a\u0627\u0646\u062f\u0627\u0631\u062f\u0647\u0627\u06cc \u0645\u0645\u06a9\u0646 \u0633\u0627\u062e\u062a\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a. \u0635\u0641\u062d\u0647\u200c\u200c\u0646\u0645\u0627\u06cc\u0634 iMac \u0628\u0647\u062a\u0631\u06cc\u0646 \u0648 \u0628\u0627\u06a9\u06cc\u0641\u06cc\u062a\u200c\u062a\u0631\u06cc\u0646 \u0646\u0645\u0648\u0646\u0647 \u062f\u0631 \u0628\u06cc\u0646 \u062a\u0645\u0627\u0645\u06cc\u00a0All-in-One\u0647\u0627\u06cc \u0645\u0648\u062c\u0648\u062f \u0627\u0633\u062a. iMac \u0645\u062f\u0644 21.5\u0627\u06cc\u0646\u0686\u06cc \u0628\u0627 \u06a9\u06cc\u0641\u06cc\u062a Full HD \u0628\u0627 \u067e\u0646\u0644 TFT \u062a\u0648\u0644\u06cc\u062f \u0634\u062f\u0647 \u0627\u0633\u062a. \u0627\u0632 \u062f\u06cc\u062f \u0627\u06a9\u062b\u0631 \u06af\u0631\u0627\u0641\u06cc\u0633\u062a\u200c\u0647\u0627 \u0648 \u0637\u0631\u0627\u062d\u0627\u0646 \u062d\u0631\u0641\u0647\u200c\u0627\u06cc\u060c \u0631\u0646\u06af\u200c\u0647\u0627\u06cc\u06cc \u06a9\u0647 \u062f\u0631\u00a0iMac\u200c\u0647\u0627 \u0646\u0645\u0627\u06cc\u0634 \u062f\u0627\u062f\u0647 \u0645\u06cc\u200c\u0634\u0648\u0646\u062f\u060c \u0646\u0632\u062f\u06cc\u06a9\u200c\u062a\u0631\u06cc\u0646 \u0631\u0646\u06af\u200c\u0647\u0627 \u0628\u0647 \u0648\u0627\u0642\u0639\u06cc\u062a \u0647\u0633\u062a\u0646\u062f\u061b \u0627\u0645\u0627 \u0645\u0647\u0645\u200c\u062a\u0631\u06cc\u0646 \u067e\u06cc\u0634\u0631\u0641\u062a \u062f\u0631 \u0646\u0633\u0644 \u062c\u062f\u06cc\u062f\u00a0iMac\u0647\u0627 \u062f\u0631 \u0642\u0637\u0639\u0627\u062a \u0633\u062e\u062a\u200c\u0627\u0641\u0632\u0627\u0631\u06cc \u0627\u0633\u062a \u06a9\u0647 \u0628\u0631\u0627\u0633\u0627\u0633 \u062a\u0633\u062a\u200c\u0647\u0627\u06cc \u0627\u0646\u062c\u0627\u0645\u200c\u06af\u0631\u0641\u062a\u0647\u060c \u0633\u0631\u0639\u062a \u0648 \u06a9\u0627\u0631\u0627\u06cc\u06cc \u0633\u06cc\u0633\u062a\u0645 \u062a\u0627 20\u062f\u0631\u0635\u062f \u0646\u0633\u0628\u062a \u0628\u0647 \u0645\u062f\u0644\u200c\u0647\u0627\u06cc \u0642\u0628\u0644\u06cc \u0627\u0641\u0632\u0627\u06cc\u0634 \u067e\u06cc\u062f\u0627 \u06a9\u0631\u062f\u0647 \u0627\u0633\u062a. \u0627\u0633\u062a\u0641\u0627\u062f\u0647 \u0627\u0632 \u067e\u0631\u062f\u0627\u0632\u0646\u062f\u0647\u200c\u0647\u0627\u06cc \u0645\u0631\u06a9\u0632\u06cc \u0646\u0633\u0644 \u067e\u0646\u062c \u0648 \u0634\u0634 \u0627\u06cc\u0646\u062a\u0644 \u0648 \u067e\u0631\u062f\u0627\u0632\u0634\u06af\u0631 \u06af\u0631\u0627\u0641\u06cc\u06a9\u06cc Iris Plus Graphics 640\u00a0\u0634\u0631\u06a9\u062a\u00a0Intel\u060c \u0627\u0635\u0644\u06cc\u200c\u062a\u0631\u06cc\u0646 \u062f\u0644\u06cc\u0644 \u0627\u06cc\u0646 \u0627\u0641\u0632\u0627\u06cc\u0634 \u0631\u0627\u0646\u062f\u0645\u0627\u0646 \u0627\u0633\u062a\u061b \u0627\u0644\u0628\u062a\u0647 \u0645\u062f\u0644\u200c\u0647\u0627\u06cc 27\u0627\u06cc\u0646\u0686\u06cc \u0645\u062c\u0647\u0632 \u0628\u0647 \u067e\u0631\u062f\u0627\u0632\u0634\u06af\u0631\u0647\u0627\u06cc \u0645\u062c\u0632\u0627 \u0647\u0633\u062a\u0646\u062f. \u0645\u06cc\u0632\u0627\u0646 \u0628\u0627\u0633 \u0631\u0645\u200c\u0647\u0627\u06cc DDR4 \u0647\u0645 \u0627\u0632 1600 \u0628\u0647 1867 \u0645\u06af\u0627\u0647\u0631\u062a\u0632 \u0627\u0631\u062a\u0642\u0627\u0621 \u067e\u06cc\u062f\u0627 \u06a9\u0631\u062f\u0647 \u06a9\u0647 \u0628\u0627\u0639\u062b \u0639\u0645\u0644\u06a9\u0631\u062f \u0633\u0631\u06cc\u0639\u200c\u062a\u0631 \u0633\u06cc\u0633\u062a\u0645 \u0645\u06cc\u200c\u0634\u0648\u0646\u062f. \u0627\u06cc\u0646 \u0645\u062f\u0644 \u0642\u062f\u0631\u062a\u0645\u0646\u062f\u060c \u0645\u062c\u0647\u0632 \u0628\u0647 \u067e\u0631\u062f\u0627\u0632\u0646\u062f\u0647\u200c\u06cc \u0645\u0631\u06a9\u0632\u06cc Core i5 \u0628\u0627 \u0641\u0631\u06a9\u0627\u0646\u0633 \u067e\u0627\u06cc\u0647 2.3 \u06af\u06cc\u06af\u0627\u0647\u0631\u062a\u0632 \u0627\u0633\u062a \u06a9\u0647 \u0628\u0647\u200c\u0648\u0627\u0633\u0637\u0647\u200c\u06cc \u062a\u06a9\u0646\u0648\u0644\u0648\u0698\u06cc Turbo Boost \u062f\u0631 \u0645\u0648\u0627\u0642\u0639 \u0646\u06cc\u0627\u0632 \u062a\u0627 3.6 \u06af\u06cc\u06af\u0627\u0647\u0631\u062a\u0632 \u0647\u0645 \u0627\u0641\u0632\u0627\u06cc\u0634 \u067e\u06cc\u062f\u0627 \u0645\u06cc\u200c\u06a9\u0646\u062f. \u067e\u0631\u062f\u0627\u0632\u0634\u06af\u0631 \u06af\u0631\u0627\u0641\u06cc\u06a9\u06cc Iris Plus Graphics 640 \u0628\u062f\u0648\u0646 \u062d\u0627\u0641\u0638\u0647\u200c\u06cc \u0627\u062e\u062a\u0635\u0627\u0635\u06cc\u060c \u0628\u0647 \u0647\u0645\u0631\u0627\u0647 8 \u06af\u06cc\u06af\u0627\u0628\u0627\u06cc\u062a \u0631\u0645 \u0648 \u06cc\u06a9 \u062a\u0631\u0627\u0628\u0627\u06cc\u062a \u062d\u0627\u0641\u0638\u0647\u200c\u06cc \u062f\u0627\u062e\u0644\u06cc \u0627\u0632 \u0646\u0648\u0639 \u0647\u0627\u0631\u062f\u062f\u06cc\u0633\u06a9 \u0647\u0645 \u0627\u0632 \u062f\u06cc\u06af\u0631 \u0633\u062e\u062a\u200c\u0627\u0641\u0632\u0627\u0631\u0647\u0627\u06cc \u0627\u06cc\u0646 \u062f\u0633\u062a\u06af\u0627\u0647 \u0647\u0633\u062a\u0646\u062f \u06a9\u0647 \u06a9\u0627\u0631\u0627\u06cc\u06cc \u0639\u0627\u0644\u06cc \u0648 \u062e\u06cc\u0631\u0647\u200c\u06a9\u0646\u0646\u062f\u0647\u200c\u0627\u06cc \u062f\u0627\u0634\u062a\u0647 \u0648 \u0628\u0647\u200c\u0631\u0627\u062d\u062a\u06cc \u0627\u0632 \u067e\u0633 \u0633\u0646\u06af\u06cc\u0646\u200c\u062a\u0631\u06cc\u0646 \u0645\u062d\u0627\u0633\u0628\u0627\u062a \u0647\u0645 \u0628\u0631\u0645\u06cc\u200c\u0622\u06cc\u0646\u062f. \u067e\u0648\u0631\u062a\u200c\u0647\u0627\u06cc \u0648\u0631\u0648\u062f\u06cc \u0648 \u062e\u0631\u0648\u062c\u06cc \u0645\u0648\u062c\u0648\u062f \u0631\u0648\u06cc \u0627\u06cc\u0646 \u0645\u062d\u0635\u0648\u0644 \u062a\u063a\u06cc\u06cc\u0631 \u06a9\u0631\u062f\u0647 \u0648 \u0634\u0627\u0645\u0644 \u062f\u0648 \u067e\u0648\u0631\u062a Thunderbolt 3\u060c \u0686\u0647\u0627\u0631 \u067e\u0648\u0631\u062a\u00a0USB 3.0\u060c \u067e\u0648\u0631\u062a \u0634\u0628\u06a9\u0647\u00a0LAN\u060c \u06a9\u0627\u0631\u062a\u200c\u062e\u0648\u0627\u0646 \u0648 \u062c\u06a9 3.5\u0645\u06cc\u0644\u06cc\u200c\u0645\u062a\u0631\u06cc \u0635\u062f\u0627 \u0645\u06cc\u200c\u0634\u0648\u0646\u062f \u06a9\u0647 \u0628\u0631\u0627\u06cc \u0627\u062a\u0635\u0627\u0644 \u0627\u0646\u0648\u0627\u0639 \u0627\u0628\u0632\u0627\u0631\u0647\u0627 \u0628\u0627 \u0628\u0627\u0644\u0627\u062a\u0631\u06cc\u0646 \u0633\u0631\u0639\u062a \u0627\u0646\u062a\u0642\u0627\u0644 \u062f\u0627\u062f\u0647\u200c\u0647\u0627 \u06a9\u0627\u0641\u06cc \u0647\u0633\u062a\u0646\u062f. \u062f\u0631 \u0628\u0633\u062a\u0647\u200c\u0628\u0646\u062f\u06cc\u00a0iMac\u0647\u0627\u06cc \u062c\u062f\u06cc\u062f\u060c \u0639\u0644\u0627\u0648\u0647\u200c\u0628\u0631 \u06a9\u0627\u0628\u0644 \u0628\u0631\u0642 \u0648 \u06a9\u0627\u0628\u0644 Lightning \u0628\u0647\u00a0USB\u060c \u06cc\u06a9 \u06a9\u06cc\u0628\u0648\u0631\u062f \u0628\u06cc\u200c\u0633\u06cc\u0645 Magic Keyboard \u0648 \u06cc\u06a9 \u0645\u0627\u0648\u0633 \u0628\u06cc\u200c\u0633\u06cc\u0645 Magic Mouse 2 \u0642\u0631\u0627\u0631 \u06af\u0631\u0641\u062a\u0647 \u06a9\u0647 \u0628\u0647 \u062a\u0646\u0647\u0627\u06cc\u06cc \u06a9\u0627\u0644\u0627\u0647\u0627\u06cc \u0627\u0631\u0632\u0634\u0645\u0646\u062f\u06cc \u0628\u0648\u062f\u0647 \u0648 \u06a9\u0627\u0631 \u0628\u0627 \u0627\u06cc\u0646 \u0645\u062d\u0635\u0648\u0644 \u0648 \u0633\u06cc\u0633\u062a\u0645\u200c\u0639\u0627\u0645\u0644 Mac OS Sierra \u0631\u0627 \u0628\u0633\u06cc\u0627\u0631 \u0622\u0633\u0627\u0646\u200c\u062a\u0631 \u0648 \u062f\u0644\u0686\u0633\u0628\u200c\u062a\u0631 \u0645\u06cc\u200c\u06a9\u0646\u0646\u062f. \u062f\u0631\u0645\u062c\u0645\u0648\u0639 \u0627\u06af\u0631 \u0642\u0635\u062f \u062e\u0631\u06cc\u062f \u06cc\u06a9 \u06a9\u0627\u0645\u067e\u06cc\u0648\u062a\u0631 \u0628\u062f\u0648\u0646 \u06a9\u06cc\u0633 \u0628\u0627 \u06a9\u06cc\u0641\u06cc\u062a \u0633\u0627\u062e\u062a \u0628\u0633\u06cc\u0627\u0631 \u062e\u0648\u0628 \u0648 \u06a9\u0627\u0631\u0627\u06cc\u06cc \u062e\u06cc\u0631\u0647\u200c\u06a9\u0646\u0646\u062f\u0647 \u0628\u0631\u0627\u06cc \u0627\u0646\u062c\u0627\u0645 \u0627\u0645\u0648\u0631 \u062d\u0631\u0641\u0647\u200c\u0627\u06cc \u062f\u0627\u0631\u06cc\u062f \u06cc\u0627 \u062d\u062a\u06cc \u0628\u0647 \u0632\u06cc\u0628\u0627\u06cc\u06cc \u0648 \u0634\u06cc\u06a9\u200c\u0628\u0648\u062f\u0646 \u0645\u062d\u06cc\u0637 \u062e\u0627\u0646\u0647 \u06cc\u0627 \u0645\u062d\u0644 \u06a9\u0627\u0631 \u062e\u0648\u062f \u0627\u0647\u0645\u06cc\u062a \u0632\u06cc\u0627\u062f\u06cc \u0645\u06cc\u200c\u062f\u0647\u06cc\u062f\u060c \u0633\u0631\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a iMac \u0634\u0631\u06a9\u062a \u0627\u067e\u0644 \u0645\u06cc\u200c\u062a\u0648\u0627\u0646\u062f \u06cc\u06a9\u06cc \u0627\u0632 \u0628\u0647\u062a\u0631\u06cc\u0646 \u0627\u0646\u062a\u062e\u0627\u0628\u200c\u0647\u0627\u06cc \u0634\u0645\u0627 \u0628\u0627\u0634\u062f.","image":".\/wp-content\/uploads\/2019\/05\/8149py2u1cL._SL1500_-1.jpg","sku":281,"offers":[{"@type":"Offer","price":"8000000","priceValidUntil":"2022-12-31","priceSpecification":{"price":"8000000","priceCurrency":"IRT","valueAddedTaxIncluded":"false"},"priceCurrency":"IRT","availability":"http:\/\/schema.org\/InStock","url":".\/product\/%d9%87%d9%88%d8%a7%d9%88%d8%a7%db%8c-%d9%85%db%8c%d8%aa-%d8%a8%d9%88%da%a9-x-%d9%be%d8%b1%d9%88-13-9-%d8%a7%db%8c%d9%86%da%86\/","seller":{"@type":"Organization","name":"\u0646\u06af\u0627\u0631\u0634\u0627\u067e","url":"."}}]}]}</script><script type="text/javascript">
    jQuery(document).ready( function($) {
        var data = {
            action: "dokan_pageview",
            _ajax_nonce: "6faedbce0b",
            post_id: 281,
        }
        $.post( "./wp-admin/admin-ajax.php", data );
    } );
</script>


<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<script type='text/javascript' src='{{asset('newFront/js/core.min.js')}}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/datepicker.min.js')}}' id='jquery-ui-datepicker-js'></script>
<script type='text/javascript' id='jquery-ui-datepicker-js-after'>
    jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"\u0628\u0633\u062a\u0646","currentText":"\u0627\u0645\u0631\u0648\u0632","monthNames":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u06cc","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"monthNamesShort":["\u0698\u0627\u0646\u0648\u06cc\u0647","\u0641\u0648\u0631\u06cc\u0647","\u0645\u0627\u0631\u0633","\u0622\u0648\u0631\u06cc\u0644","\u0645\u0647","\u0698\u0648\u0626\u0646","\u062c\u0648\u0644\u0627\u06cc","\u0622\u06af\u0648\u0633\u062a","\u0633\u067e\u062a\u0627\u0645\u0628\u0631","\u0627\u06a9\u062a\u0628\u0631","\u0646\u0648\u0627\u0645\u0628\u0631","\u062f\u0633\u0627\u0645\u0628\u0631"],"nextText":"\u0628\u0639\u062f","prevText":"\u0642\u0628\u0644\u06cc","dayNames":["\u06cc\u06a9\u0634\u0646\u0628\u0647","\u062f\u0648\u0634\u0646\u0628\u0647","\u0633\u0647\u200c\u0634\u0646\u0628\u0647","\u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647","\u067e\u0646\u062c\u200c\u0634\u0646\u0628\u0647","\u062c\u0645\u0639\u0647","\u0634\u0646\u0628\u0647"],"dayNamesShort":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dayNamesMin":["\u06cc","\u062f","\u0633","\u0686","\u067e","\u062c","\u0634"],"dateFormat":"MM d, yy","firstDay":6,"isRTL":true});});
</script>
<script type='text/javascript' src='{{asset('newFront/js/selectWoo.full.min.js')}}' id='selectWoo-js'></script>
<script type='text/javascript' id='wc-country-select-js-extra'>
    /* <![CDATA[ */
    var wc_country_select_params = {"countries":"{\"AF\":[],\"AO\":{\"BGO\":\"Bengo\",\"BLU\":\"Benguela\",\"BIE\":\"Bi\\u00e9\",\"CAB\":\"Cabinda\",\"CNN\":\"Cunene\",\"HUA\":\"Huambo\",\"HUI\":\"Hu\\u00edla\",\"CCU\":\"Kuando Kubango\",\"CNO\":\"Kwanza-Norte\",\"CUS\":\"Kwanza-Sul\",\"LUA\":\"Luanda\",\"LNO\":\"Lunda-Norte\",\"LSU\":\"Lunda-Sul\",\"MAL\":\"Malanje\",\"MOX\":\"Moxico\",\"NAM\":\"Namibe\",\"UIG\":\"U\\u00edge\",\"ZAI\":\"Zaire\"},\"AR\":{\"C\":\"Ciudad Aut\\u00f3noma de Buenos Aires\",\"B\":\"Buenos Aires\",\"K\":\"Catamarca\",\"H\":\"Chaco\",\"U\":\"Chubut\",\"X\":\"C\\u00f3rdoba\",\"W\":\"Corrientes\",\"E\":\"Entre R\\u00edos\",\"P\":\"Formosa\",\"Y\":\"Jujuy\",\"L\":\"La Pampa\",\"F\":\"La Rioja\",\"M\":\"Mendoza\",\"N\":\"Misiones\",\"Q\":\"Neuqu\\u00e9n\",\"R\":\"R\\u00edo Negro\",\"A\":\"Salta\",\"J\":\"San Juan\",\"D\":\"San Luis\",\"Z\":\"Santa Cruz\",\"S\":\"Santa Fe\",\"G\":\"Santiago del Estero\",\"V\":\"Tierra del Fuego\",\"T\":\"Tucum\\u00e1n\"},\"AT\":[],\"AU\":{\"ACT\":\"Australian Capital Territory\",\"NSW\":\"New South Wales\",\"NT\":\"Northern Territory\",\"QLD\":\"Queensland\",\"SA\":\"South Australia\",\"TAS\":\"Tasmania\",\"VIC\":\"Victoria\",\"WA\":\"Western Australia\"},\"AX\":[],\"BD\":{\"BD-05\":\"Bagerhat\",\"BD-01\":\"Bandarban\",\"BD-02\":\"Barguna\",\"BD-06\":\"Barishal\",\"BD-07\":\"Bhola\",\"BD-03\":\"Bogura\",\"BD-04\":\"Brahmanbaria\",\"BD-09\":\"Chandpur\",\"BD-10\":\"Chattogram\",\"BD-12\":\"Chuadanga\",\"BD-11\":\"Cox's Bazar\",\"BD-08\":\"Cumilla\",\"BD-13\":\"Dhaka\",\"BD-14\":\"Dinajpur\",\"BD-15\":\"Faridpur \",\"BD-16\":\"Feni\",\"BD-19\":\"Gaibandha\",\"BD-18\":\"Gazipur\",\"BD-17\":\"Gopalganj\",\"BD-20\":\"Habiganj\",\"BD-21\":\"Jamalpur\",\"BD-22\":\"Jashore\",\"BD-25\":\"Jhalokati\",\"BD-23\":\"Jhenaidah\",\"BD-24\":\"Joypurhat\",\"BD-29\":\"Khagrachhari\",\"BD-27\":\"Khulna\",\"BD-26\":\"Kishoreganj\",\"BD-28\":\"Kurigram\",\"BD-30\":\"Kushtia\",\"BD-31\":\"Lakshmipur\",\"BD-32\":\"Lalmonirhat\",\"BD-36\":\"Madaripur\",\"BD-37\":\"Magura\",\"BD-33\":\"Manikganj \",\"BD-39\":\"Meherpur\",\"BD-38\":\"Moulvibazar\",\"BD-35\":\"Munshiganj\",\"BD-34\":\"Mymensingh\",\"BD-48\":\"Naogaon\",\"BD-43\":\"Narail\",\"BD-40\":\"Narayanganj\",\"BD-42\":\"Narsingdi\",\"BD-44\":\"Natore\",\"BD-45\":\"Nawabganj\",\"BD-41\":\"Netrakona\",\"BD-46\":\"Nilphamari\",\"BD-47\":\"Noakhali\",\"BD-49\":\"Pabna\",\"BD-52\":\"Panchagarh\",\"BD-51\":\"Patuakhali\",\"BD-50\":\"Pirojpur\",\"BD-53\":\"Rajbari\",\"BD-54\":\"Rajshahi\",\"BD-56\":\"Rangamati\",\"BD-55\":\"Rangpur\",\"BD-58\":\"Satkhira\",\"BD-62\":\"Shariatpur\",\"BD-57\":\"Sherpur\",\"BD-59\":\"Sirajganj\",\"BD-61\":\"Sunamganj\",\"BD-60\":\"Sylhet\",\"BD-63\":\"Tangail\",\"BD-64\":\"Thakurgaon\"},\"BE\":[],\"BG\":{\"BG-01\":\"Blagoevgrad\",\"BG-02\":\"Burgas\",\"BG-08\":\"Dobrich\",\"BG-07\":\"Gabrovo\",\"BG-26\":\"Haskovo\",\"BG-09\":\"Kardzhali\",\"BG-10\":\"Kyustendil\",\"BG-11\":\"Lovech\",\"BG-12\":\"Montana\",\"BG-13\":\"Pazardzhik\",\"BG-14\":\"Pernik\",\"BG-15\":\"Pleven\",\"BG-16\":\"Plovdiv\",\"BG-17\":\"Razgrad\",\"BG-18\":\"Ruse\",\"BG-27\":\"Shumen\",\"BG-19\":\"Silistra\",\"BG-20\":\"Sliven\",\"BG-21\":\"Smolyan\",\"BG-23\":\"Sofia\",\"BG-22\":\"Sofia-Grad\",\"BG-24\":\"Stara Zagora\",\"BG-25\":\"Targovishte\",\"BG-03\":\"Varna\",\"BG-04\":\"Veliko Tarnovo\",\"BG-05\":\"Vidin\",\"BG-06\":\"Vratsa\",\"BG-28\":\"Yambol\"},\"BH\":[],\"BI\":[],\"BJ\":{\"AL\":\"Alibori\",\"AK\":\"Atakora\",\"AQ\":\"Atlantique\",\"BO\":\"Borgou\",\"CO\":\"Collines\",\"KO\":\"Kouffo\",\"DO\":\"Donga\",\"LI\":\"Littoral\",\"MO\":\"Mono\",\"OU\":\"Ou\\u00e9m\\u00e9\",\"PL\":\"Plateau\",\"ZO\":\"Zou\"},\"BO\":{\"B\":\"\\u0686\\u0648\\u06a9\\u06cc\\u0633\\u0627\\u06a9\\u0627\",\"H\":\"\\u0628\\u0646\\u06cc\",\"C\":\"\\u06a9\\u0686\\u0627\\u0628\\u0627\\u0645\\u0628\\u0627\",\"L\":\"\\u0644\\u0627\\u067e\\u0627\\u0632\",\"O\":\"\\u0627\\u0648\\u0631\\u0648\\u0631\\u0648\",\"N\":\"\\u067e\\u0627\\u0646\\u062f\\u0648\",\"P\":\"\\u067e\\u0648\\u062a\\u0648\\u0633\\u06cc\",\"S\":\"Santa Cruz\",\"T\":\"\\u062a\\u0631\\u06cc\\u062c\\u0627\"},\"BR\":{\"AC\":\"Acre\",\"AL\":\"Alagoas\",\"AP\":\"Amap\\u00e1\",\"AM\":\"Amazonas\",\"BA\":\"Bahia\",\"CE\":\"Cear\\u00e1\",\"DF\":\"Distrito Federal\",\"ES\":\"Esp\\u00edrito Santo\",\"GO\":\"Goi\\u00e1s\",\"MA\":\"Maranh\\u00e3o\",\"MT\":\"Mato Grosso\",\"MS\":\"Mato Grosso do Sul\",\"MG\":\"Minas Gerais\",\"PA\":\"Par\\u00e1\",\"PB\":\"Para\\u00edba\",\"PR\":\"Paran\\u00e1\",\"PE\":\"Pernambuco\",\"PI\":\"Piau\\u00ed\",\"RJ\":\"Rio de Janeiro\",\"RN\":\"Rio Grande do Norte\",\"RS\":\"Rio Grande do Sul\",\"RO\":\"Rond\\u00f4nia\",\"RR\":\"Roraima\",\"SC\":\"Santa Catarina\",\"SP\":\"S\\u00e3o Paulo\",\"SE\":\"Sergipe\",\"TO\":\"Tocantins\"},\"CA\":{\"AB\":\"Alberta\",\"BC\":\"British Columbia\",\"MB\":\"Manitoba\",\"NB\":\"New Brunswick\",\"NL\":\"Newfoundland and Labrador\",\"NT\":\"Northwest Territories\",\"NS\":\"Nova Scotia\",\"NU\":\"Nunavut\",\"ON\":\"Ontario\",\"PE\":\"Prince Edward Island\",\"QC\":\"Quebec\",\"SK\":\"Saskatchewan\",\"YT\":\"Yukon Territory\"},\"CH\":{\"AG\":\"Aargau\",\"AR\":\"Appenzell Ausserrhoden\",\"AI\":\"Appenzell Innerrhoden\",\"BL\":\"Basel-Landschaft\",\"BS\":\"Basel-Stadt\",\"BE\":\"Bern\",\"FR\":\"Fribourg\",\"GE\":\"Geneva\",\"GL\":\"Glarus\",\"GR\":\"Graub\\u00fcnden\",\"JU\":\"Jura\",\"LU\":\"Luzern\",\"NE\":\"Neuch\\u00e2tel\",\"NW\":\"Nidwalden\",\"OW\":\"Obwalden\",\"SH\":\"Schaffhausen\",\"SZ\":\"Schwyz\",\"SO\":\"Solothurn\",\"SG\":\"St. Gallen\",\"TG\":\"Thurgau\",\"TI\":\"Ticino\",\"UR\":\"Uri\",\"VS\":\"Valais\",\"VD\":\"Vaud\",\"ZG\":\"Zug\",\"ZH\":\"Z\\u00fcrich\"},\"CN\":{\"CN1\":\"Yunnan \\\/ \\u4e91\\u5357\",\"CN2\":\"Beijing \\\/ \\u5317\\u4eac\",\"CN3\":\"Tianjin \\\/ \\u5929\\u6d25\",\"CN4\":\"Hebei \\\/ \\u6cb3\\u5317\",\"CN5\":\"Shanxi \\\/ \\u5c71\\u897f\",\"CN6\":\"Inner Mongolia \\\/ \\u5167\\u8499\\u53e4\",\"CN7\":\"Liaoning \\\/ \\u8fbd\\u5b81\",\"CN8\":\"Jilin \\\/ \\u5409\\u6797\",\"CN9\":\"Heilongjiang \\\/ \\u9ed1\\u9f99\\u6c5f\",\"CN10\":\"Shanghai \\\/ \\u4e0a\\u6d77\",\"CN11\":\"Jiangsu \\\/ \\u6c5f\\u82cf\",\"CN12\":\"Zhejiang \\\/ \\u6d59\\u6c5f\",\"CN13\":\"Anhui \\\/ \\u5b89\\u5fbd\",\"CN14\":\"Fujian \\\/ \\u798f\\u5efa\",\"CN15\":\"Jiangxi \\\/ \\u6c5f\\u897f\",\"CN16\":\"Shandong \\\/ \\u5c71\\u4e1c\",\"CN17\":\"Henan \\\/ \\u6cb3\\u5357\",\"CN18\":\"Hubei \\\/ \\u6e56\\u5317\",\"CN19\":\"Hunan \\\/ \\u6e56\\u5357\",\"CN20\":\"Guangdong \\\/ \\u5e7f\\u4e1c\",\"CN21\":\"Guangxi Zhuang \\\/ \\u5e7f\\u897f\\u58ee\\u65cf\",\"CN22\":\"Hainan \\\/ \\u6d77\\u5357\",\"CN23\":\"Chongqing \\\/ \\u91cd\\u5e86\",\"CN24\":\"Sichuan \\\/ \\u56db\\u5ddd\",\"CN25\":\"Guizhou \\\/ \\u8d35\\u5dde\",\"CN26\":\"Shaanxi \\\/ \\u9655\\u897f\",\"CN27\":\"Gansu \\\/ \\u7518\\u8083\",\"CN28\":\"Qinghai \\\/ \\u9752\\u6d77\",\"CN29\":\"Ningxia Hui \\\/ \\u5b81\\u590f\",\"CN30\":\"Macao \\\/ \\u6fb3\\u95e8\",\"CN31\":\"Tibet \\\/ \\u897f\\u85cf\",\"CN32\":\"Xinjiang \\\/ \\u65b0\\u7586\"},\"CZ\":[],\"DE\":[],\"DK\":[],\"DZ\":{\"DZ-01\":\"\\u0622\\u062f\\u0631\\u0627\\u0631\",\"DZ-02\":\"\\u0686\\u0644\\u0641\",\"DZ-03\":\"\\u0644\\u0627\\u063a\\u0648\\u0627\\u062a\",\"DZ-04\":\"\\u0627\\u0648\\u0645 \\u0627\\u0644\\u0628\\u0648\\u0627\\u063a\\u06cc\",\"DZ-05\":\"\\u0628\\u0627\\u062a\\u0646\\u0627\",\"DZ-06\":\"B\\u00e9ja\\u00efa\",\"DZ-07\":\"\\u0628\\u06cc\\u0633\\u06a9\\u0631\\u0627\",\"DZ-08\":\"B\\u00e9char\",\"DZ-09\":\"\\u0628\\u0644\\u06cc\\u062f\\u0627\",\"DZ-10\":\"\\u0628\\u0648\\u06cc\\u0631\\u0627\",\"DZ-11\":\"\\u062a\\u0627\\u0645\\u0627\\u0646\\u06af\\u0647\\u0627\\u0633\\u062a\",\"DZ-12\":\"T\\u00e9bessa\",\"DZ-13\":\"\\u062a\\u0644\\u0645\\u0633\\u0646\",\"DZ-14\":\"\\u062a\\u06cc\\u0627\\u0631\\u062a\",\"DZ-15\":\"\\u062a\\u064a\\u0632\\u064a \\u0627\\u0648\\u0632\\u0648\",\"DZ-16\":\"\\u0627\\u0644\\u062c\\u0632\\u0627\\u06cc\\u0631\",\"DZ-17\":\"\\u062c\\u0644\\u0641\\u0627\",\"DZ-18\":\"\\u062c\\u06cc\\u0698\\u0644\",\"DZ-19\":\"S\\u00e9tif\",\"DZ-20\":\"Sa\\u00efda\",\"DZ-21\":\"Skikda\",\"DZ-22\":\"Sidi Bel Abb\\u00e8s\",\"DZ-23\":\"Annaba\",\"DZ-24\":\"Guelma\",\"DZ-25\":\"Constantine\",\"DZ-26\":\"M\\u00e9d\\u00e9a\",\"DZ-27\":\"Mostaganem\",\"DZ-28\":\"M\\u2019Sila\",\"DZ-29\":\"\\u0645\\u0627\\u0633\\u06a9\\u0627\\u0631\\u0627\",\"DZ-30\":\"\\u0627\\u0648\\u0627\\u0631\\u06af\\u0644\\u0627\",\"DZ-31\":\"Oran\",\"DZ-32\":\"El Bayadh\",\"DZ-33\":\"\\u0627\\u06cc\\u0644\\u06cc\\u0632\\u06cc\",\"DZ-34\":\"Bordj Bou Arr\\u00e9ridj\",\"DZ-35\":\"Boumerd\\u00e8s\",\"DZ-36\":\"\\u0627\\u0644 \\u062a\\u0631\\u0641\",\"DZ-37\":\"\\u062a\\u06cc\\u0646\\u062f\\u0648\\u0641\",\"DZ-38\":\"Tissemsilt\",\"DZ-39\":\"\\u0627\\u0644 \\u0627\\u0648\\u0648\\u062f\",\"DZ-40\":\"\\u06a9\\u0646\\u0686\\u0644\\u0627\",\"DZ-41\":\"Souk Ahras\",\"DZ-42\":\"\\u062a\\u06cc\\u067e\\u0627\\u0633\\u0627\",\"DZ-43\":\"Mila\",\"DZ-44\":\"A\\u00efn Defla\",\"DZ-45\":\"Naama\",\"DZ-46\":\"A\\u00efn T\\u00e9mouchent\",\"DZ-47\":\"Gharda\\u00efa\",\"DZ-48\":\"Relizane\"},\"EE\":[],\"EG\":{\"EGALX\":\"Alexandria\",\"EGASN\":\"Aswan\",\"EGAST\":\"Asyut\",\"EGBA\":\"Red Sea\",\"EGBH\":\"Beheira\",\"EGBNS\":\"Beni Suef\",\"EGC\":\"Cairo\",\"EGDK\":\"Dakahlia\",\"EGDT\":\"Damietta\",\"EGFYM\":\"Faiyum\",\"EGGH\":\"Gharbia\",\"EGGZ\":\"Giza\",\"EGIS\":\"Ismailia\",\"EGJS\":\"South Sinai\",\"EGKB\":\"Qalyubia\",\"EGKFS\":\"Kafr el-Sheikh\",\"EGKN\":\"Qena\",\"EGLX\":\"Luxor\",\"EGMN\":\"Minya\",\"EGMNF\":\"Monufia\",\"EGMT\":\"Matrouh\",\"EGPTS\":\"Port Said\",\"EGSHG\":\"Sohag\",\"EGSHR\":\"Al Sharqia\",\"EGSIN\":\"North Sinai\",\"EGSUZ\":\"Suez\",\"EGWAD\":\"New Valley\"},\"ES\":{\"C\":\"A Coru\\u00f1a\",\"VI\":\"Araba\\\/\\u00c1lava\",\"AB\":\"Albacete\",\"A\":\"Alicante\",\"AL\":\"Almer\\u00eda\",\"O\":\"Asturias\",\"AV\":\"\\u00c1vila\",\"BA\":\"Badajoz\",\"PM\":\"Baleares\",\"B\":\"Barcelona\",\"BU\":\"Burgos\",\"CC\":\"C\\u00e1ceres\",\"CA\":\"C\\u00e1diz\",\"S\":\"Cantabria\",\"CS\":\"Castell\\u00f3n\",\"CE\":\"Ceuta\",\"CR\":\"Ciudad Real\",\"CO\":\"C\\u00f3rdoba\",\"CU\":\"Cuenca\",\"GI\":\"Girona\",\"GR\":\"Granada\",\"GU\":\"Guadalajara\",\"SS\":\"Gipuzkoa\",\"H\":\"Huelva\",\"HU\":\"Huesca\",\"J\":\"Ja\\u00e9n\",\"LO\":\"La Rioja\",\"GC\":\"Las Palmas\",\"LE\":\"Le\\u00f3n\",\"L\":\"Lleida\",\"LU\":\"Lugo\",\"M\":\"Madrid\",\"MA\":\"M\\u00e1laga\",\"ML\":\"Melilla\",\"MU\":\"Murcia\",\"NA\":\"Navarra\",\"OR\":\"Ourense\",\"P\":\"Palencia\",\"PO\":\"Pontevedra\",\"SA\":\"Salamanca\",\"TF\":\"Santa Cruz de Tenerife\",\"SG\":\"Segovia\",\"SE\":\"Sevilla\",\"SO\":\"Soria\",\"T\":\"Tarragona\",\"TE\":\"Teruel\",\"TO\":\"Toledo\",\"V\":\"Valencia\",\"VA\":\"Valladolid\",\"BI\":\"Biscay\",\"ZA\":\"Zamora\",\"Z\":\"Zaragoza\"},\"FI\":[],\"FR\":[],\"GH\":{\"AF\":\"Ahafo\",\"AH\":\"Ashanti\",\"BA\":\"Brong-Ahafo\",\"BO\":\"Bono\",\"BE\":\"Bono East\",\"CP\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"EP\":\"\\u0634\\u0631\\u0642\\u06cc\",\"AA\":\"Greater Accra\",\"NE\":\"North East\",\"NP\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"OT\":\"Oti\",\"SV\":\"Savannah\",\"UE\":\"\\u062e\\u0627\\u0648\\u0631 \\u0634\\u0631\\u0642\\u06cc\",\"UW\":\"\\u062e\\u0627\\u0648\\u0631\\u0645\\u06cc\\u0627\\u0646\\u0647\",\"TV\":\"Volta\",\"WP\":\"\\u063a\\u0631\\u0628\\u06cc\",\"WN\":\"Western North\"},\"GP\":[],\"GR\":{\"I\":\"Attica\",\"A\":\"East Macedonia and Thrace\",\"B\":\"Central Macedonia\",\"C\":\"\\u0645\\u0642\\u062f\\u0648\\u0646\\u06cc\\u0647 \\u063a\\u0631\\u0628\\u06cc\",\"D\":\"\\u0627\\u06cc\\u067e\\u06cc\\u0631\\u0648\\u0633\",\"E\":\"\\u062a\\u0633\\u0627\\u0644\\u0627\\u0644\\u06cc\",\"F\":\"Ionian Islands\",\"G\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"H\":\"\\u06cc\\u0648\\u0646\\u0627\\u0646 \\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"J\":\"\\u067e\\u0644\\u0648\\u067e\\u0648\\u0646\\u0632\",\"K\":\"North Aegean\",\"L\":\"South Aegean\",\"M\":\"\\u06a9\\u0631\\u062a\"},\"GF\":[],\"HK\":{\"HONG KONG\":\"Hong Kong Island\",\"KOWLOON\":\"Kowloon\",\"NEW TERRITORIES\":\"New Territories\"},\"HU\":{\"BK\":\"B\\u00e1cs-Kiskun\",\"BE\":\"B\\u00e9k\\u00e9s\",\"BA\":\"Baranya\",\"BZ\":\"Borsod-Aba\\u00faj-Zempl\\u00e9n\",\"BU\":\"Budapest\",\"CS\":\"Csongr\\u00e1d-Csan\\u00e1d\",\"FE\":\"Fej\\u00e9r\",\"GS\":\"Gy\\u0151r-Moson-Sopron\",\"HB\":\"Hajd\\u00fa-Bihar\",\"HE\":\"Heves\",\"JN\":\"J\\u00e1sz-Nagykun-Szolnok\",\"KE\":\"Kom\\u00e1rom-Esztergom\",\"NO\":\"N\\u00f3gr\\u00e1d\",\"PE\":\"Pest\",\"SO\":\"Somogy\",\"SZ\":\"Szabolcs-Szatm\\u00e1r-Bereg\",\"TO\":\"Tolna\",\"VA\":\"Vas\",\"VE\":\"Veszpr\\u00e9m\",\"ZA\":\"Zala\"},\"ID\":{\"AC\":\"Daerah Istimewa Aceh\",\"SU\":\"Sumatera Utara\",\"SB\":\"Sumatera Barat\",\"RI\":\"Riau\",\"KR\":\"Kepulauan Riau\",\"JA\":\"Jambi\",\"SS\":\"Sumatera Selatan\",\"BB\":\"Bangka Belitung\",\"BE\":\"Bengkulu\",\"LA\":\"Lampung\",\"JK\":\"DKI Jakarta\",\"JB\":\"Jawa Barat\",\"BT\":\"Banten\",\"JT\":\"Jawa Tengah\",\"JI\":\"Jawa Timur\",\"YO\":\"Daerah Istimewa Yogyakarta\",\"BA\":\"Bali\",\"NB\":\"Nusa Tenggara Barat\",\"NT\":\"Nusa Tenggara Timur\",\"KB\":\"Kalimantan Barat\",\"KT\":\"Kalimantan Tengah\",\"KI\":\"Kalimantan Timur\",\"KS\":\"Kalimantan Selatan\",\"KU\":\"Kalimantan Utara\",\"SA\":\"Sulawesi Utara\",\"ST\":\"Sulawesi Tengah\",\"SG\":\"Sulawesi Tenggara\",\"SR\":\"Sulawesi Barat\",\"SN\":\"Sulawesi Selatan\",\"GO\":\"Gorontalo\",\"MA\":\"Maluku\",\"MU\":\"Maluku Utara\",\"PA\":\"Papua\",\"PB\":\"Papua Barat\"},\"IE\":{\"CW\":\"Carlow\",\"CN\":\"Cavan\",\"CE\":\"Clare\",\"CO\":\"Cork\",\"DL\":\"Donegal\",\"D\":\"Dublin\",\"G\":\"Galway\",\"KY\":\"Kerry\",\"KE\":\"Kildare\",\"KK\":\"Kilkenny\",\"LS\":\"Laois\",\"LM\":\"Leitrim\",\"LK\":\"Limerick\",\"LD\":\"Longford\",\"LH\":\"Louth\",\"MO\":\"Mayo\",\"MH\":\"Meath\",\"MN\":\"Monaghan\",\"OY\":\"Offaly\",\"RN\":\"Roscommon\",\"SO\":\"Sligo\",\"TA\":\"Tipperary\",\"WD\":\"Waterford\",\"WH\":\"Westmeath\",\"WX\":\"Wexford\",\"WW\":\"Wicklow\"},\"IN\":{\"AP\":\"Andhra Pradesh\",\"AR\":\"Arunachal Pradesh\",\"AS\":\"Assam\",\"BR\":\"Bihar\",\"CT\":\"Chhattisgarh\",\"GA\":\"Goa\",\"GJ\":\"Gujarat\",\"HR\":\"Haryana\",\"HP\":\"Himachal Pradesh\",\"JK\":\"Jammu and Kashmir\",\"JH\":\"Jharkhand\",\"KA\":\"Karnataka\",\"KL\":\"Kerala\",\"MP\":\"Madhya Pradesh\",\"MH\":\"Maharashtra\",\"MN\":\"Manipur\",\"ML\":\"Meghalaya\",\"MZ\":\"Mizoram\",\"NL\":\"Nagaland\",\"OR\":\"Orissa\",\"PB\":\"Punjab\",\"RJ\":\"Rajasthan\",\"SK\":\"Sikkim\",\"TN\":\"Tamil Nadu\",\"TS\":\"Telangana\",\"TR\":\"Tripura\",\"UK\":\"Uttarakhand\",\"UP\":\"Uttar Pradesh\",\"WB\":\"West Bengal\",\"AN\":\"Andaman and Nicobar Islands\",\"CH\":\"Chandigarh\",\"DN\":\"Dadra and Nagar Haveli\",\"DD\":\"Daman and Diu\",\"DL\":\"Delhi\",\"LD\":\"Lakshadeep\",\"PY\":\"Pondicherry (Puducherry)\"},\"IR\":{\"ABZ\":\"\\u0627\\u0644\\u0628\\u0631\\u0632\",\"ADL\":\"\\u0627\\u0631\\u062f\\u0628\\u06cc\\u0644\",\"EAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u0634\\u0631\\u0642\\u06cc\",\"WAZ\":\"\\u0622\\u0630\\u0631\\u0628\\u0627\\u06cc\\u062c\\u0627\\u0646 \\u063a\\u0631\\u0628\\u06cc\",\"BHR\":\"\\u0628\\u0648\\u0634\\u0647\\u0631\",\"CHB\":\"\\u0686\\u0647\\u0627\\u0631\\u0645\\u062d\\u0627\\u0644 \\u0648 \\u0628\\u062e\\u062a\\u06cc\\u0627\\u0631\\u06cc\",\"FRS\":\"\\u0641\\u0627\\u0631\\u0633\",\"GIL\":\"\\u06af\\u06cc\\u0644\\u0627\\u0646\",\"GLS\":\"\\u06af\\u0644\\u0633\\u062a\\u0627\\u0646\",\"HDN\":\"\\u0647\\u0645\\u062f\\u0627\\u0646\",\"HRZ\":\"\\u0647\\u0631\\u0645\\u0632\\u06af\\u0627\\u0646\",\"ILM\":\"\\u0627\\u06cc\\u0644\\u0627\\u0645\",\"ESF\":\"\\u0627\\u0635\\u0641\\u0647\\u0627\\u0646\",\"KRN\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\",\"KRH\":\"\\u06a9\\u0631\\u0645\\u0627\\u0646\\u0634\\u0627\\u0647\",\"NKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0634\\u0645\\u0627\\u0644\\u06cc\",\"RKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u0631\\u0636\\u0648\\u06cc\",\"SKH\":\"\\u062e\\u0631\\u0627\\u0633\\u0627\\u0646 \\u062c\\u0646\\u0648\\u0628\\u06cc\",\"KHZ\":\"\\u062e\\u0648\\u0632\\u0633\\u062a\\u0627\\u0646\",\"KBD\":\"\\u06a9\\u0647\\u06af\\u06cc\\u0644\\u0648\\u06cc\\u0647 \\u0648 \\u0628\\u0648\\u06cc\\u0631\\u0627\\u062d\\u0645\\u062f\",\"KRD\":\"\\u06a9\\u0631\\u062f\\u0633\\u062a\\u0627\\u0646\",\"LRS\":\"\\u0644\\u0631\\u0633\\u062a\\u0627\\u0646\",\"MKZ\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"MZN\":\"\\u0645\\u0627\\u0632\\u0646\\u062f\\u0631\\u0627\\u0646\",\"GZN\":\"\\u0642\\u0632\\u0648\\u06cc\\u0646\",\"QHM\":\"\\u0642\\u0645\",\"SMN\":\"\\u0633\\u0645\\u0646\\u0627\\u0646\",\"SBN\":\"\\u0633\\u06cc\\u0633\\u062a\\u0627\\u0646 \\u0648 \\u0628\\u0644\\u0648\\u0686\\u0633\\u062a\\u0627\\u0646\",\"THR\":\"\\u062a\\u0647\\u0631\\u0627\\u0646\",\"YZD\":\"\\u06cc\\u0632\\u062f\",\"ZJN\":\"\\u0632\\u0646\\u062c\\u0627\\u0646\"},\"IS\":[],\"IT\":{\"AG\":\"Agrigento\",\"AL\":\"Alessandria\",\"AN\":\"Ancona\",\"AO\":\"Aosta\",\"AR\":\"Arezzo\",\"AP\":\"Ascoli Piceno\",\"AT\":\"Asti\",\"AV\":\"Avellino\",\"BA\":\"Bari\",\"BT\":\"Barletta-Andria-Trani\",\"BL\":\"Belluno\",\"BN\":\"Benevento\",\"BG\":\"Bergamo\",\"BI\":\"Biella\",\"BO\":\"Bologna\",\"BZ\":\"Bolzano\",\"BS\":\"Brescia\",\"BR\":\"Brindisi\",\"CA\":\"Cagliari\",\"CL\":\"Caltanissetta\",\"CB\":\"Campobasso\",\"CE\":\"Caserta\",\"CT\":\"Catania\",\"CZ\":\"Catanzaro\",\"CH\":\"Chieti\",\"CO\":\"Como\",\"CS\":\"Cosenza\",\"CR\":\"Cremona\",\"KR\":\"Crotone\",\"CN\":\"Cuneo\",\"EN\":\"Enna\",\"FM\":\"Fermo\",\"FE\":\"Ferrara\",\"FI\":\"Firenze\",\"FG\":\"Foggia\",\"FC\":\"Forl\\u00ec-Cesena\",\"FR\":\"Frosinone\",\"GE\":\"Genova\",\"GO\":\"Gorizia\",\"GR\":\"Grosseto\",\"IM\":\"Imperia\",\"IS\":\"Isernia\",\"SP\":\"La Spezia\",\"AQ\":\"L'Aquila\",\"LT\":\"Latina\",\"LE\":\"Lecce\",\"LC\":\"Lecco\",\"LI\":\"Livorno\",\"LO\":\"Lodi\",\"LU\":\"Lucca\",\"MC\":\"Macerata\",\"MN\":\"Mantova\",\"MS\":\"Massa-Carrara\",\"MT\":\"Matera\",\"ME\":\"Messina\",\"MI\":\"Milano\",\"MO\":\"Modena\",\"MB\":\"Monza e della Brianza\",\"NA\":\"Napoli\",\"NO\":\"Novara\",\"NU\":\"Nuoro\",\"OR\":\"Oristano\",\"PD\":\"Padova\",\"PA\":\"Palermo\",\"PR\":\"Parma\",\"PV\":\"Pavia\",\"PG\":\"Perugia\",\"PU\":\"Pesaro e Urbino\",\"PE\":\"Pescara\",\"PC\":\"Piacenza\",\"PI\":\"Pisa\",\"PT\":\"Pistoia\",\"PN\":\"Pordenone\",\"PZ\":\"Potenza\",\"PO\":\"Prato\",\"RG\":\"Ragusa\",\"RA\":\"Ravenna\",\"RC\":\"Reggio Calabria\",\"RE\":\"Reggio Emilia\",\"RI\":\"Rieti\",\"RN\":\"Rimini\",\"RM\":\"Roma\",\"RO\":\"Rovigo\",\"SA\":\"Salerno\",\"SS\":\"Sassari\",\"SV\":\"Savona\",\"SI\":\"Siena\",\"SR\":\"Siracusa\",\"SO\":\"Sondrio\",\"SU\":\"Sud Sardegna\",\"TA\":\"Taranto\",\"TE\":\"Teramo\",\"TR\":\"Terni\",\"TO\":\"Torino\",\"TP\":\"Trapani\",\"TN\":\"Trento\",\"TV\":\"Treviso\",\"TS\":\"Trieste\",\"UD\":\"Udine\",\"VA\":\"Varese\",\"VE\":\"Venezia\",\"VB\":\"Verbano-Cusio-Ossola\",\"VC\":\"Vercelli\",\"VR\":\"Verona\",\"VV\":\"Vibo Valentia\",\"VI\":\"Vicenza\",\"VT\":\"Viterbo\"},\"IL\":[],\"IM\":[],\"JM\":{\"JM-01\":\"Kingston\",\"JM-02\":\"Saint Andrew\",\"JM-03\":\"Saint Thomas\",\"JM-04\":\"Portland\",\"JM-05\":\"Saint Mary\",\"JM-06\":\"Saint Ann\",\"JM-07\":\"Trelawny\",\"JM-08\":\"Saint James\",\"JM-09\":\"Hanover\",\"JM-10\":\"Westmoreland\",\"JM-11\":\"Saint Elizabeth\",\"JM-12\":\"\\u0645\\u0646\\u062c\\u0633\\u062a\\u0631\",\"JM-13\":\"Clarendon\",\"JM-14\":\"\\u0633\\u0646\\u062a \\u06a9\\u0627\\u062a\\u0631\\u06cc\\u0646\"},\"JP\":{\"JP01\":\"Hokkaido\",\"JP02\":\"Aomori\",\"JP03\":\"Iwate\",\"JP04\":\"Miyagi\",\"JP05\":\"Akita\",\"JP06\":\"Yamagata\",\"JP07\":\"Fukushima\",\"JP08\":\"Ibaraki\",\"JP09\":\"Tochigi\",\"JP10\":\"Gunma\",\"JP11\":\"Saitama\",\"JP12\":\"Chiba\",\"JP13\":\"Tokyo\",\"JP14\":\"Kanagawa\",\"JP15\":\"Niigata\",\"JP16\":\"Toyama\",\"JP17\":\"Ishikawa\",\"JP18\":\"Fukui\",\"JP19\":\"Yamanashi\",\"JP20\":\"Nagano\",\"JP21\":\"Gifu\",\"JP22\":\"Shizuoka\",\"JP23\":\"Aichi\",\"JP24\":\"Mie\",\"JP25\":\"Shiga\",\"JP26\":\"Kyoto\",\"JP27\":\"Osaka\",\"JP28\":\"Hyogo\",\"JP29\":\"Nara\",\"JP30\":\"Wakayama\",\"JP31\":\"Tottori\",\"JP32\":\"Shimane\",\"JP33\":\"Okayama\",\"JP34\":\"Hiroshima\",\"JP35\":\"Yamaguchi\",\"JP36\":\"Tokushima\",\"JP37\":\"Kagawa\",\"JP38\":\"Ehime\",\"JP39\":\"Kochi\",\"JP40\":\"Fukuoka\",\"JP41\":\"Saga\",\"JP42\":\"Nagasaki\",\"JP43\":\"Kumamoto\",\"JP44\":\"Oita\",\"JP45\":\"Miyazaki\",\"JP46\":\"Kagoshima\",\"JP47\":\"Okinawa\"},\"KE\":{\"KE01\":\"Baringo\",\"KE02\":\"Bomet\",\"KE03\":\"Bungoma\",\"KE04\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"KE05\":\"Elgeyo-Marakwet\",\"KE06\":\"Embu\",\"KE07\":\"Garissa\",\"KE08\":\"Homa Bay\",\"KE09\":\"Isiolo\",\"KE10\":\"Kajiado\",\"KE11\":\"Kakamega\",\"KE12\":\"Kericho\",\"KE13\":\"Kiambu\",\"KE14\":\"Kilifi\",\"KE15\":\"Kirinyaga\",\"KE16\":\"Kisii\",\"KE17\":\"Kisumu\",\"KE18\":\"Kitui\",\"KE19\":\"Kwale\",\"KE20\":\"Laikipia\",\"KE21\":\"Lamu\",\"KE22\":\"Machakos\",\"KE23\":\"Makueni\",\"KE24\":\"Mandera\",\"KE25\":\"Marsabit\",\"KE26\":\"Meru\",\"KE27\":\"Migori\",\"KE28\":\"Mombasa\",\"KE29\":\"Murang\\u2019a\",\"KE30\":\"Nairobi County\",\"KE31\":\"Nakuru\",\"KE32\":\"Nandi\",\"KE33\":\"Narok\",\"KE34\":\"Nyamira\",\"KE35\":\"Nyandarua\",\"KE36\":\"Nyeri\",\"KE37\":\"Samburu\",\"KE38\":\"Siaya\",\"KE39\":\"Taita-Taveta\",\"KE40\":\"Tana River\",\"KE41\":\"Tharaka-Nithi\",\"KE42\":\"Trans Nzoia\",\"KE43\":\"Turkana\",\"KE44\":\"Uasin Gishu\",\"KE45\":\"Vihiga\",\"KE46\":\"Wajir\",\"KE47\":\"West Pokot\"},\"KR\":[],\"KW\":[],\"LA\":{\"AT\":\"\\u0622\\u062a\\u0627\\u067e\\u06cc\\u0648\",\"BK\":\"\\u0628\\u0648\\u06a9\\u06cc\\u0648\",\"BL\":\"\\u0628\\u0648\\u0644\\u06cc\\u06a9\\u0627\\u0645\\u200c\\u0633\\u0627\\u06cc\",\"CH\":\"\\u0686\\u0627\\u0645\\u067e\\u0627\\u0633\\u0627\\u06a9\",\"HO\":\"\\u0647\\u0648\\u0627\\u0641\\u0627\\u0646\",\"KH\":\"\\u062e\\u0627\\u0645\\u0648\\u0627\\u0646\",\"LM\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u0646\\u0627\\u0645\\u062a\\u0627\",\"LP\":\"\\u0644\\u0648\\u0622\\u0646\\u06af \\u067e\\u0631\\u0627\\u0628\\u0627\\u0646\\u06af\",\"OU\":\"\\u0627\\u0648\\u062f\\u0648\\u0645\\u0634\\u0627\\u06cc\",\"PH\":\"\\u0641\\u0648\\u0646\\u06af\\u0633\\u0627\\u0644\\u06cc\",\"SL\":\"\\u0633\\u0627\\u0644\\u0627\\u0648\\u0627\\u0646\",\"SV\":\"\\u0633\\u0627\\u0648\\u0627\\u0646\\u0627\\u062e\\u062a\",\"VI\":\"\\u0627\\u0633\\u062a\\u0627\\u0646 \\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"VT\":\"\\u0648\\u06cc\\u0646\\u062a\\u06cc\\u0627\\u0646\",\"XA\":\"\\u0633\\u0627\\u06cc\\u0646\\u06cc\\u0627\\u0628\\u0648\\u0644\\u06cc\",\"XE\":\"\\u0633\\u06a9\\u0648\\u0646\\u06af\",\"XI\":\"\\u0634\\u06cc\\u0627\\u0646\\u06af\\u062e\\u0648\\u0627\\u0646\\u06af\",\"XS\":\"Xaisomboun\"},\"LB\":[],\"LR\":{\"BM\":\"Bomi\",\"BN\":\"Bong\",\"GA\":\"Gbarpolu\",\"GB\":\"Grand Bassa\",\"GC\":\"Grand Cape Mount\",\"GG\":\"Grand Gedeh\",\"GK\":\"Grand Kru\",\"LO\":\"Lofa\",\"MA\":\"Margibi\",\"MY\":\"Maryland\",\"MO\":\"Montserrado\",\"NM\":\"Nimba\",\"RV\":\"Rivercess\",\"RG\":\"River Gee\",\"SN\":\"Sinoe\"},\"LU\":[],\"MD\":{\"C\":\"Chi\\u0219in\\u0103u\",\"BL\":\"B\\u0103l\\u021bi\",\"AN\":\"Anenii Noi\",\"BS\":\"Basarabeasca\",\"BR\":\"Briceni\",\"CH\":\"Cahul\",\"CT\":\"Cantemir\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"C\\u0103u\\u0219eni\",\"CM\":\"Cimi\\u0219lia\",\"CR\":\"Criuleni\",\"DN\":\"Dondu\\u0219eni\",\"DR\":\"Drochia\",\"DB\":\"Dub\\u0103sari\",\"ED\":\"Edine\\u021b\",\"FL\":\"F\\u0103le\\u0219ti\",\"FR\":\"Flore\\u0219ti\",\"GE\":\"UTA G\\u0103g\\u0103uzia\",\"GL\":\"Glodeni\",\"HN\":\"H\\u00eence\\u0219ti\",\"IL\":\"Ialoveni\",\"LV\":\"Leova\",\"NS\":\"Nisporeni\",\"OC\":\"Ocni\\u021ba\",\"OR\":\"Orhei\",\"RZ\":\"Rezina\",\"RS\":\"R\\u00ee\\u0219cani\",\"SG\":\"S\\u00eengerei\",\"SR\":\"Soroca\",\"ST\":\"Str\\u0103\\u0219eni\",\"SD\":\"\\u0218old\\u0103ne\\u0219ti\",\"SV\":\"\\u0218tefan Vod\\u0103\",\"TR\":\"Taraclia\",\"TL\":\"Telene\\u0219ti\",\"UN\":\"Ungheni\"},\"MQ\":[],\"MT\":[],\"MX\":{\"DF\":\"Ciudad de M\\u00e9xico\",\"JA\":\"Jalisco\",\"NL\":\"Nuevo Le\\u00f3n\",\"AG\":\"Aguascalientes\",\"BC\":\"Baja California\",\"BS\":\"Baja California Sur\",\"CM\":\"Campeche\",\"CS\":\"Chiapas\",\"CH\":\"Chihuahua\",\"CO\":\"Coahuila\",\"CL\":\"Colima\",\"DG\":\"Durango\",\"GT\":\"Guanajuato\",\"GR\":\"Guerrero\",\"HG\":\"Hidalgo\",\"MX\":\"Estado de M\\u00e9xico\",\"MI\":\"Michoac\\u00e1n\",\"MO\":\"Morelos\",\"NA\":\"Nayarit\",\"OA\":\"Oaxaca\",\"PU\":\"Puebla\",\"QT\":\"Quer\\u00e9taro\",\"QR\":\"Quintana Roo\",\"SL\":\"San Luis Potos\\u00ed\",\"SI\":\"Sinaloa\",\"SO\":\"Sonora\",\"TB\":\"Tabasco\",\"TM\":\"Tamaulipas\",\"TL\":\"Tlaxcala\",\"VE\":\"Veracruz\",\"YU\":\"Yucat\\u00e1n\",\"ZA\":\"Zacatecas\"},\"MY\":{\"JHR\":\"Johor\",\"KDH\":\"Kedah\",\"KTN\":\"Kelantan\",\"LBN\":\"Labuan\",\"MLK\":\"Malacca (Melaka)\",\"NSN\":\"Negeri Sembilan\",\"PHG\":\"Pahang\",\"PNG\":\"Penang (Pulau Pinang)\",\"PRK\":\"Perak\",\"PLS\":\"Perlis\",\"SBH\":\"Sabah\",\"SWK\":\"Sarawak\",\"SGR\":\"Selangor\",\"TRG\":\"Terengganu\",\"PJY\":\"Putrajaya\",\"KUL\":\"Kuala Lumpur\"},\"MZ\":{\"MZP\":\"Cabo Delgado\",\"MZG\":\"\\u063a\\u0632\\u0647\",\"MZI\":\"Inhambane\",\"MZB\":\"Manica\",\"MZL\":\"Maputo Province\",\"MZMPM\":\"Maputo\",\"MZN\":\"Nampula\",\"MZA\":\"Niassa\",\"MZS\":\"Sofala\",\"MZT\":\"\\u062a\\u062a\\u0647\",\"MZQ\":\"\\u0632\\u0627\\u0645\\u0628\\u0632\\u06cc\\u0627\"},\"NA\":{\"ER\":\"\\u0627\\u0631\\u0648\\u0646\\u06af\\u0648\",\"HA\":\"\\u0647\\u0627\\u0631\\u062f\\u067e\",\"KA\":\"\\u06a9\\u0627\\u0631\\u0627\\u0633\",\"KE\":\"\\u06a9\\u0627\\u0648\\u0627\\u0646\\u06af\\u0648 \\u0634\\u0631\\u0642\\u06cc\",\"KW\":\"Kavango West\",\"KH\":\"Khomas\",\"KU\":\"Kunene\",\"OW\":\"Ohangwena\",\"OH\":\"Omaheke\",\"OS\":\"Omusati\",\"ON\":\"Oshana\",\"OT\":\"Oshikoto\",\"OD\":\"Otjozondjupa\",\"CA\":\"Zambezi\"},\"NG\":{\"AB\":\"Abia\",\"FC\":\"Abuja\",\"AD\":\"Adamawa\",\"AK\":\"Akwa Ibom\",\"AN\":\"Anambra\",\"BA\":\"Bauchi\",\"BY\":\"Bayelsa\",\"BE\":\"Benue\",\"BO\":\"Borno\",\"CR\":\"Cross River\",\"DE\":\"Delta\",\"EB\":\"Ebonyi\",\"ED\":\"Edo\",\"EK\":\"Ekiti\",\"EN\":\"Enugu\",\"GO\":\"Gombe\",\"IM\":\"Imo\",\"JI\":\"Jigawa\",\"KD\":\"Kaduna\",\"KN\":\"Kano\",\"KT\":\"Katsina\",\"KE\":\"Kebbi\",\"KO\":\"Kogi\",\"KW\":\"Kwara\",\"LA\":\"Lagos\",\"NA\":\"Nasarawa\",\"NI\":\"\\u0646\\u06cc\\u062c\\u0631\",\"OG\":\"Ogun\",\"ON\":\"Ondo\",\"OS\":\"Osun\",\"OY\":\"Oyo\",\"PL\":\"Plateau\",\"RI\":\"Rivers\",\"SO\":\"Sokoto\",\"TA\":\"Taraba\",\"YO\":\"Yobe\",\"ZA\":\"Zamfara\"},\"NL\":[],\"NO\":[],\"NP\":{\"BAG\":\"Bagmati\",\"BHE\":\"Bheri\",\"DHA\":\"Dhaulagiri\",\"GAN\":\"Gandaki\",\"JAN\":\"Janakpur\",\"KAR\":\"Karnali\",\"KOS\":\"Koshi\",\"LUM\":\"Lumbini\",\"MAH\":\"Mahakali\",\"MEC\":\"Mechi\",\"NAR\":\"Narayani\",\"RAP\":\"Rapti\",\"SAG\":\"Sagarmatha\",\"SET\":\"Seti\"},\"NZ\":{\"NL\":\"Northland\",\"AK\":\"Auckland\",\"WA\":\"Waikato\",\"BP\":\"Bay of Plenty\",\"TK\":\"Taranaki\",\"GI\":\"Gisborne\",\"HB\":\"Hawke\\u2019s Bay\",\"MW\":\"Manawatu-Wanganui\",\"WE\":\"Wellington\",\"NS\":\"Nelson\",\"MB\":\"Marlborough\",\"TM\":\"Tasman\",\"WC\":\"West Coast\",\"CT\":\"Canterbury\",\"OT\":\"Otago\",\"SL\":\"Southland\"},\"PE\":{\"CAL\":\"El Callao\",\"LMA\":\"Municipalidad Metropolitana de Lima\",\"AMA\":\"Amazonas\",\"ANC\":\"Ancash\",\"APU\":\"Apur\\u00edmac\",\"ARE\":\"\\u0636\\u0631\\u0648\\u0631\\u06cc\",\"AYA\":\"Ayacucho\",\"CAJ\":\"Cajamarca\",\"CUS\":\"Cusco\",\"HUV\":\"Huancavelica\",\"HUC\":\"Hu\\u00e1nuco\",\"ICA\":\"Ica\",\"JUN\":\"Jun\\u00edn\",\"LAL\":\"La Libertad\",\"LAM\":\"Lambayeque\",\"LIM\":\"Lima\",\"LOR\":\"Loreto\",\"MDD\":\"Madre de Dios\",\"MOQ\":\"Moquegua\",\"PAS\":\"Pasco\",\"PIU\":\"Piura\",\"PUN\":\"Puno\",\"SAM\":\"San Mart\\u00edn\",\"TAC\":\"Tacna\",\"TUM\":\"Tumbes\",\"UCA\":\"Ucayali\"},\"PH\":{\"ABR\":\"Abra\",\"AGN\":\"Agusan del Norte\",\"AGS\":\"Agusan del Sur\",\"AKL\":\"Aklan\",\"ALB\":\"Albay\",\"ANT\":\"Antique\",\"APA\":\"Apayao\",\"AUR\":\"Aurora\",\"BAS\":\"Basilan\",\"BAN\":\"Bataan\",\"BTN\":\"Batanes\",\"BTG\":\"Batangas\",\"BEN\":\"Benguet\",\"BIL\":\"Biliran\",\"BOH\":\"Bohol\",\"BUK\":\"Bukidnon\",\"BUL\":\"Bulacan\",\"CAG\":\"Cagayan\",\"CAN\":\"Camarines Norte\",\"CAS\":\"Camarines Sur\",\"CAM\":\"Camiguin\",\"CAP\":\"Capiz\",\"CAT\":\"Catanduanes\",\"CAV\":\"Cavite\",\"CEB\":\"Cebu\",\"COM\":\"Compostela Valley\",\"NCO\":\"Cotabato\",\"DAV\":\"Davao del Norte\",\"DAS\":\"Davao del Sur\",\"DAC\":\"Davao Occidental\",\"DAO\":\"Davao Oriental\",\"DIN\":\"Dinagat Islands\",\"EAS\":\"Eastern Samar\",\"GUI\":\"Guimaras\",\"IFU\":\"Ifugao\",\"ILN\":\"Ilocos Norte\",\"ILS\":\"Ilocos Sur\",\"ILI\":\"Iloilo\",\"ISA\":\"Isabela\",\"KAL\":\"Kalinga\",\"LUN\":\"La Union\",\"LAG\":\"Laguna\",\"LAN\":\"Lanao del Norte\",\"LAS\":\"Lanao del Sur\",\"LEY\":\"Leyte\",\"MAG\":\"Maguindanao\",\"MAD\":\"Marinduque\",\"MAS\":\"Masbate\",\"MSC\":\"Misamis Occidental\",\"MSR\":\"Misamis Oriental\",\"MOU\":\"Mountain Province\",\"NEC\":\"Negros Occidental\",\"NER\":\"Negros Oriental\",\"NSA\":\"Northern Samar\",\"NUE\":\"Nueva Ecija\",\"NUV\":\"Nueva Vizcaya\",\"MDC\":\"Occidental Mindoro\",\"MDR\":\"Oriental Mindoro\",\"PLW\":\"Palawan\",\"PAM\":\"Pampanga\",\"PAN\":\"Pangasinan\",\"QUE\":\"Quezon\",\"QUI\":\"Quirino\",\"RIZ\":\"Rizal\",\"ROM\":\"Romblon\",\"WSA\":\"Samar\",\"SAR\":\"Sarangani\",\"SIQ\":\"Siquijor\",\"SOR\":\"Sorsogon\",\"SCO\":\"South Cotabato\",\"SLE\":\"Southern Leyte\",\"SUK\":\"Sultan Kudarat\",\"SLU\":\"Sulu\",\"SUN\":\"Surigao del Norte\",\"SUR\":\"Surigao del Sur\",\"TAR\":\"Tarlac\",\"TAW\":\"Tawi-Tawi\",\"ZMB\":\"Zambales\",\"ZAN\":\"Zamboanga del Norte\",\"ZAS\":\"Zamboanga del Sur\",\"ZSI\":\"Zamboanga Sibugay\",\"00\":\"Metro Manila\"},\"PK\":{\"JK\":\"Azad Kashmir\",\"BA\":\"Balochistan\",\"TA\":\"FATA\",\"GB\":\"Gilgit Baltistan\",\"IS\":\"Islamabad Capital Territory\",\"KP\":\"Khyber Pakhtunkhwa\",\"PB\":\"Punjab\",\"SD\":\"Sindh\"},\"PL\":[],\"PR\":[],\"PT\":[],\"PY\":{\"PY-ASU\":\"Asunci\\u00f3n\",\"PY-1\":\"Concepci\\u00f3n\",\"PY-2\":\"\\u0633\\u0646 \\u067e\\u062f\\u0631\\u0648\",\"PY-3\":\"\\u06a9\\u0648\\u0631\\u062f\\u06cc\\u0644\\u0631\\u0627\",\"PY-4\":\"Guair\\u00e1\",\"PY-5\":\"Caaguaz\\u00fa\",\"PY-6\":\"Caazap\\u00e1\",\"PY-7\":\"Itap\\u00faa\",\"PY-8\":\"Misiones\",\"PY-9\":\"Paraguar\\u00ed\",\"PY-10\":\"Alto Paran\\u00e1\",\"PY-11\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"PY-12\":\"\\u00d1eembuc\\u00fa\",\"PY-13\":\"\\u0622\\u0645\\u0627\\u0645\\u0628\\u0627\\u06cc\",\"PY-14\":\"Canindey\\u00fa\",\"PY-15\":\"\\u067e\\u0631\\u0632\\u06cc\\u062f\\u0646\\u062a \\u0647\\u06cc\\u0632\",\"PY-16\":\"\\u0622\\u0644\\u062a\\u0648 \\u067e\\u0627\\u0631\\u0627\\u06af\\u0648\\u0626\\u0647\",\"PY-17\":\"Boquer\\u00f3n\"},\"RE\":[],\"RO\":{\"AB\":\"Alba\",\"AR\":\"Arad\",\"AG\":\"Arge\\u0219\",\"BC\":\"Bac\\u0103u\",\"BH\":\"Bihor\",\"BN\":\"Bistri\\u021ba-N\\u0103s\\u0103ud\",\"BT\":\"Boto\\u0219ani\",\"BR\":\"Br\\u0103ila\",\"BV\":\"Bra\\u0219ov\",\"B\":\"Bucure\\u0219ti\",\"BZ\":\"Buz\\u0103u\",\"CL\":\"C\\u0103l\\u0103ra\\u0219i\",\"CS\":\"Cara\\u0219-Severin\",\"CJ\":\"Cluj\",\"CT\":\"Constan\\u021ba\",\"CV\":\"Covasna\",\"DB\":\"D\\u00e2mbovi\\u021ba\",\"DJ\":\"Dolj\",\"GL\":\"Gala\\u021bi\",\"GR\":\"Giurgiu\",\"GJ\":\"Gorj\",\"HR\":\"Harghita\",\"HD\":\"Hunedoara\",\"IL\":\"Ialomi\\u021ba\",\"IS\":\"Ia\\u0219i\",\"IF\":\"Ilfov\",\"MM\":\"Maramure\\u0219\",\"MH\":\"Mehedin\\u021bi\",\"MS\":\"Mure\\u0219\",\"NT\":\"Neam\\u021b\",\"OT\":\"Olt\",\"PH\":\"Prahova\",\"SJ\":\"S\\u0103laj\",\"SM\":\"Satu Mare\",\"SB\":\"Sibiu\",\"SV\":\"Suceava\",\"TR\":\"Teleorman\",\"TM\":\"Timi\\u0219\",\"TL\":\"Tulcea\",\"VL\":\"V\\u00e2lcea\",\"VS\":\"Vaslui\",\"VN\":\"Vrancea\"},\"RS\":{\"RS00\":\"Belgrade\",\"RS14\":\"Bor\",\"RS11\":\"Brani\\u010devo\",\"RS02\":\"Central Banat\",\"RS10\":\"Danube\",\"RS23\":\"Jablanica\",\"RS09\":\"Kolubara\",\"RS08\":\"Ma\\u010dva\",\"RS17\":\"Morava\",\"RS20\":\"Ni\\u0161ava\",\"RS01\":\"North Ba\\u010dka\",\"RS03\":\"North Banat\",\"RS24\":\"P\\u010dinja\",\"RS22\":\"Pirot\",\"RS13\":\"Pomoravlje\",\"RS19\":\"Rasina\",\"RS18\":\"Ra\\u0161ka\",\"RS06\":\"South Ba\\u010dka\",\"RS04\":\"South Banat\",\"RS07\":\"Srem\",\"RS12\":\"\\u0160umadija\",\"RS21\":\"Toplica\",\"RS05\":\"West Ba\\u010dka\",\"RS15\":\"Zaje\\u010dar\",\"RS16\":\"Zlatibor\",\"RS25\":\"Kosovo\",\"RS26\":\"Pe\\u0107\",\"RS27\":\"Prizren\",\"RS28\":\"Kosovska Mitrovica\",\"RS29\":\"Kosovo-Pomoravlje\",\"RSKM\":\"Kosovo-Metohija\",\"RSVO\":\"Vojvodina\"},\"SG\":[],\"SK\":[],\"SI\":[],\"TH\":{\"TH-37\":\"Amnat Charoen\",\"TH-15\":\"Ang Thong\",\"TH-14\":\"Ayutthaya\",\"TH-10\":\"Bangkok\",\"TH-38\":\"Bueng Kan\",\"TH-31\":\"Buri Ram\",\"TH-24\":\"Chachoengsao\",\"TH-18\":\"Chai Nat\",\"TH-36\":\"Chaiyaphum\",\"TH-22\":\"Chanthaburi\",\"TH-50\":\"Chiang Mai\",\"TH-57\":\"Chiang Rai\",\"TH-20\":\"Chonburi\",\"TH-86\":\"Chumphon\",\"TH-46\":\"Kalasin\",\"TH-62\":\"Kamphaeng Phet\",\"TH-71\":\"Kanchanaburi\",\"TH-40\":\"Khon Kaen\",\"TH-81\":\"Krabi\",\"TH-52\":\"Lampang\",\"TH-51\":\"Lamphun\",\"TH-42\":\"Loei\",\"TH-16\":\"Lopburi\",\"TH-58\":\"Mae Hong Son\",\"TH-44\":\"Maha Sarakham\",\"TH-49\":\"Mukdahan\",\"TH-26\":\"Nakhon Nayok\",\"TH-73\":\"Nakhon Pathom\",\"TH-48\":\"Nakhon Phanom\",\"TH-30\":\"Nakhon Ratchasima\",\"TH-60\":\"Nakhon Sawan\",\"TH-80\":\"Nakhon Si Thammarat\",\"TH-55\":\"Nan\",\"TH-96\":\"Narathiwat\",\"TH-39\":\"Nong Bua Lam Phu\",\"TH-43\":\"Nong Khai\",\"TH-12\":\"Nonthaburi\",\"TH-13\":\"Pathum Thani\",\"TH-94\":\"Pattani\",\"TH-82\":\"Phang Nga\",\"TH-93\":\"Phatthalung\",\"TH-56\":\"Phayao\",\"TH-67\":\"Phetchabun\",\"TH-76\":\"Phetchaburi\",\"TH-66\":\"Phichit\",\"TH-65\":\"Phitsanulok\",\"TH-54\":\"Phrae\",\"TH-83\":\"Phuket\",\"TH-25\":\"Prachin Buri\",\"TH-77\":\"Prachuap Khiri Khan\",\"TH-85\":\"Ranong\",\"TH-70\":\"Ratchaburi\",\"TH-21\":\"Rayong\",\"TH-45\":\"Roi Et\",\"TH-27\":\"Sa Kaeo\",\"TH-47\":\"Sakon Nakhon\",\"TH-11\":\"Samut Prakan\",\"TH-74\":\"Samut Sakhon\",\"TH-75\":\"Samut Songkhram\",\"TH-19\":\"Saraburi\",\"TH-91\":\"Satun\",\"TH-17\":\"Sing Buri\",\"TH-33\":\"Sisaket\",\"TH-90\":\"Songkhla\",\"TH-64\":\"Sukhothai\",\"TH-72\":\"Suphan Buri\",\"TH-84\":\"Surat Thani\",\"TH-32\":\"Surin\",\"TH-63\":\"Tak\",\"TH-92\":\"Trang\",\"TH-23\":\"Trat\",\"TH-34\":\"Ubon Ratchathani\",\"TH-41\":\"Udon Thani\",\"TH-61\":\"Uthai Thani\",\"TH-53\":\"Uttaradit\",\"TH-95\":\"Yala\",\"TH-35\":\"Yasothon\"},\"TR\":{\"TR01\":\"Adana\",\"TR02\":\"Ad\\u0131yaman\",\"TR03\":\"Afyon\",\"TR04\":\"A\\u011fr\\u0131\",\"TR05\":\"Amasya\",\"TR06\":\"Ankara\",\"TR07\":\"Antalya\",\"TR08\":\"Artvin\",\"TR09\":\"Ayd\\u0131n\",\"TR10\":\"Bal\\u0131kesir\",\"TR11\":\"Bilecik\",\"TR12\":\"Bing\\u00f6l\",\"TR13\":\"Bitlis\",\"TR14\":\"Bolu\",\"TR15\":\"Burdur\",\"TR16\":\"Bursa\",\"TR17\":\"\\u00c7anakkale\",\"TR18\":\"\\u00c7ank\\u0131r\\u0131\",\"TR19\":\"\\u00c7orum\",\"TR20\":\"Denizli\",\"TR21\":\"Diyarbak\\u0131r\",\"TR22\":\"Edirne\",\"TR23\":\"Elaz\\u0131\\u011f\",\"TR24\":\"Erzincan\",\"TR25\":\"Erzurum\",\"TR26\":\"Eski\\u015fehir\",\"TR27\":\"Gaziantep\",\"TR28\":\"Giresun\",\"TR29\":\"G\\u00fcm\\u00fc\\u015fhane\",\"TR30\":\"Hakkari\",\"TR31\":\"Hatay\",\"TR32\":\"Isparta\",\"TR33\":\"\\u0130\\u00e7el\",\"TR34\":\"\\u0130stanbul\",\"TR35\":\"\\u0130zmir\",\"TR36\":\"Kars\",\"TR37\":\"Kastamonu\",\"TR38\":\"Kayseri\",\"TR39\":\"K\\u0131rklareli\",\"TR40\":\"K\\u0131r\\u015fehir\",\"TR41\":\"Kocaeli\",\"TR42\":\"Konya\",\"TR43\":\"K\\u00fctahya\",\"TR44\":\"Malatya\",\"TR45\":\"Manisa\",\"TR46\":\"Kahramanmara\\u015f\",\"TR47\":\"Mardin\",\"TR48\":\"Mu\\u011fla\",\"TR49\":\"Mu\\u015f\",\"TR50\":\"Nev\\u015fehir\",\"TR51\":\"Ni\\u011fde\",\"TR52\":\"Ordu\",\"TR53\":\"Rize\",\"TR54\":\"Sakarya\",\"TR55\":\"Samsun\",\"TR56\":\"Siirt\",\"TR57\":\"Sinop\",\"TR58\":\"Sivas\",\"TR59\":\"Tekirda\\u011f\",\"TR60\":\"Tokat\",\"TR61\":\"Trabzon\",\"TR62\":\"Tunceli\",\"TR63\":\"\\u015eanl\\u0131urfa\",\"TR64\":\"U\\u015fak\",\"TR65\":\"Van\",\"TR66\":\"Yozgat\",\"TR67\":\"Zonguldak\",\"TR68\":\"Aksaray\",\"TR69\":\"Bayburt\",\"TR70\":\"Karaman\",\"TR71\":\"K\\u0131r\\u0131kkale\",\"TR72\":\"Batman\",\"TR73\":\"\\u015e\\u0131rnak\",\"TR74\":\"Bart\\u0131n\",\"TR75\":\"Ardahan\",\"TR76\":\"I\\u011fd\\u0131r\",\"TR77\":\"Yalova\",\"TR78\":\"Karab\\u00fck\",\"TR79\":\"Kilis\",\"TR80\":\"Osmaniye\",\"TR81\":\"D\\u00fczce\"},\"TZ\":{\"TZ01\":\"Arusha\",\"TZ02\":\"Dar es Salaam\",\"TZ03\":\"Dodoma\",\"TZ04\":\"Iringa\",\"TZ05\":\"Kagera\",\"TZ06\":\"Pemba North\",\"TZ07\":\"Zanzibar North\",\"TZ08\":\"Kigoma\",\"TZ09\":\"Kilimanjaro\",\"TZ10\":\"Pemba South\",\"TZ11\":\"Zanzibar South\",\"TZ12\":\"Lindi\",\"TZ13\":\"Mara\",\"TZ14\":\"Mbeya\",\"TZ15\":\"Zanzibar West\",\"TZ16\":\"Morogoro\",\"TZ17\":\"Mtwara\",\"TZ18\":\"Mwanza\",\"TZ19\":\"Coast\",\"TZ20\":\"Rukwa\",\"TZ21\":\"Ruvuma\",\"TZ22\":\"Shinyanga\",\"TZ23\":\"Singida\",\"TZ24\":\"Tabora\",\"TZ25\":\"Tanga\",\"TZ26\":\"Manyara\",\"TZ27\":\"Geita\",\"TZ28\":\"Katavi\",\"TZ29\":\"Njombe\",\"TZ30\":\"Simiyu\"},\"LK\":[],\"SE\":[],\"UG\":{\"UG314\":\"\\u0622\\u0628\\u06cc\\u0645\",\"UG301\":\"\\u0622\\u062f\\u0648\\u0645\\u0627\\u0646\\u06cc\",\"UG322\":\"\\u0622\\u06af\\u0627\\u06af\\u0648\",\"UG323\":\"\\u0622\\u0644\\u0628\\u0648\\u0646\\u06af\",\"UG315\":\"\\u0622\\u0645\\u0648\\u0644\\u0627\\u062a\\u0627\\u0631\",\"UG324\":\"\\u0622\\u0645\\u0648\\u062f\\u0627\\u062a\",\"UG216\":\"\\u0622\\u0645\\u0648\\u0631\\u06cc\\u0627\",\"UG316\":\"\\u0622\\u0645\\u0648\\u0631\\u0648\",\"UG302\":\"\\u0622\\u067e\\u0627\\u06a9\",\"UG303\":\"\\u0622\\u0631\\u0648\\u0627\",\"UG217\":\"\\u0628\\u0648\\u062f\\u0627\\u06a9\\u0627\",\"UG218\":\"\\u0628\\u0648\\u062f\\u0627\",\"UG201\":\"\\u0628\\u0648\\u06af\\u06cc\\u0631\\u06cc\",\"UG235\":\"\\u0628\\u0648\\u06af\\u0648\\u0631\\u064a\",\"UG420\":\"\\u0628\\u0648\\u0648\\u0647\\u0648\\u062c\\u0648\",\"UG117\":\"\\u0628\\u06cc\\u0648\\u06a9\\u0648\\u0647\",\"UG219\":\"\\u0628\\u0648\\u06a9\\u0648\\u062f\\u0627\",\"UG118\":\"\\u0628\\u0648\\u06a9\\u0648\\u0645\\u0646\\u0633\\u06cc\\u0628\\u06cc\",\"UG220\":\"\\u0628\\u0648\\u06a9\\u0648\\u0627\",\"UG225\":\"\\u0628\\u0648\\u0644\\u0627\\u0645\\u0628\\u0648\\u0644\\u06cc\",\"UG416\":\"\\u0628\\u0648\\u0644\\u06cc\\u0633\\u0627\",\"UG401\":\"Bundibugyo\",\"UG430\":\"\\u0628\\u0648\\u0646\\u0627\\u0646\\u06af\\u0627\\u0628\\u0648\",\"UG402\":\"\\u0628\\u0648\\u0634\\u0646\\u06cc\",\"UG202\":\"\\u0628\\u0648\\u0633\\u06cc\\u0627\",\"UG221\":\"\\u0628\\u0648\\u062a\\u0627\\u0644\\u0647\\u06cc\\u0627\",\"UG119\":\"\\u0628\\u0648\\u062a\\u0627\\u0645\\u0628\\u0644\\u0627\",\"UG233\":\"Butebo\",\"UG120\":\"\\u0628\\u0648\\u0648\\u0648\\u0645\\u0627\",\"UG226\":\"Buyende\",\"UG317\":\"\\u062f\\u0648\\u06a9\\u0648\\u0644\\u0648\",\"UG121\":\"\\u06af\\u0645\\u0628\\u0627\",\"UG304\":\"\\u06af\\u0644\\u0648\",\"UG403\":\"\\u0647\\u0648\\u06cc\\u0645\\u0627\",\"UG417\":\"\\u0627\\u06cc\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG203\":\"\\u0627\\u06cc\\u06af\\u0627\\u0646\\u06af\\u0627\",\"UG418\":\"\\u0627\\u06cc\\u0633\\u06cc\\u0646\\u06af\\u06cc\\u0631\\u0648\",\"UG204\":\"\\u062c\\u06cc\\u0646\\u062c\\u0627\",\"UG318\":\"\\u06a9\\u0627\\u0628\\u0648\\u0646\\u06af\",\"UG404\":\"\\u06a9\\u0627\\u0628\\u0644\",\"UG405\":\"\\u06a9\\u0627\\u0628\\u0627\\u0631\\u0648\\u0644\",\"UG213\":\"\\u06a9\\u0627\\u0628\\u06cc\\u0631\\u0627\\u0645\\u06cc\\u062f\\u0648\",\"UG427\":\"\\u06a9\\u0627\\u06af\\u0627\\u062f\\u06cc\",\"UG428\":\"\\u06a9\\u0627\\u06a9\\u0648\\u0645\\u06cc\\u0631\\u0648\",\"UG101\":\"\\u06a9\\u0627\\u0644\\u0646\\u06af\\u0627\\u0644\\u0627\",\"UG222\":\"\\u06a9\\u0627\\u0644\\u06cc\\u0631\\u0648\",\"UG122\":\"\\u06a9\\u0627\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG102\":\"\\u06a9\\u0627\\u0645\\u067e\\u0627\\u0644\\u0627\",\"UG205\":\"\\u06a9\\u0627\\u0645\\u0648\\u0644\\u06cc\",\"UG413\":\"\\u06a9\\u0627\\u0645\\u0648\\u0648\\u0646\\u06af\",\"UG414\":\"\\u06a9\\u0627\\u0646\\u0648\\u0646\\u06af\\u0648\",\"UG206\":\"\\u06a9\\u0627\\u067e\\u0686\\u0648\\u0631\\u0648\\u0627\",\"UG236\":\"\\u06a9\\u0627\\u067e\\u0644\\u0628\\u0628\\u06cc\\u0648\\u0646\\u06af\",\"UG126\":\"\\u06a9\\u0627\\u0633\\u0627\\u0646\\u062f\\u0627\",\"UG406\":\"\\u06a9\\u0627\\u0633\\u06cc\",\"UG207\":\"\\u06a9\\u062a\\u0627\\u06a9\\u0648\\u06cc\",\"UG112\":\"\\u06a9\\u0627\\u06cc\\u0648\\u0646\\u06af\\u0627\",\"UG407\":\"\\u06a9\\u06cc\\u0628\\u0627\\u0644\\u0647\",\"UG103\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06af\\u0627\",\"UG227\":\"\\u06a9\\u06cc\\u0628\\u0648\\u06a9\\u0648\",\"UG432\":\"Kikuube\",\"UG419\":\"\\u06a9\\u06cc\\u0631\\u0648\\u0648\\u0631\\u0627\",\"UG421\":\"\\u06a9\\u06cc\\u0631\\u0627\\u0646\\u062f\\u0648\\u0646\\u06af\\u0648\",\"UG408\":\"\\u06a9\\u06cc\\u0633\\u0648\\u0631\\u0648\",\"UG305\":\"\\u06a9\\u06cc\\u062a\\u06af\\u0648\\u0645\",\"UG319\":\"\\u06a9\\u0648\\u0628\\u0648\\u06a9\\u0648\",\"UG325\":\"\\u06a9\\u0648\\u0644\",\"UG306\":\"\\u06a9\\u0648\\u062a\\u06cc\\u062f\\u0648\",\"UG208\":\"\\u0643\\u0648\\u0645\\u064a\",\"UG333\":\"\\u06a9\\u0648\\u0627\\u0646\\u06cc\\u0627\",\"UG228\":\"\\u06a9\\u0648\\u0646\\u06cc\\u0646\\u06af\",\"UG123\":\"\\u06a9\\u06cc\\u0627\\u0646\\u06a9\\u0648\\u0627\\u0646\\u0632\\u06cc\",\"UG422\":\"\\u06a9\\u06cc\\u06af\\u06af\\u0648\\u0627\",\"UG415\":\"\\u06a9\\u06cc\\u0648\\u0646\\u062c\\u0648\\u062c\\u0648\",\"UG125\":\"\\u06a9\\u06cc\\u0648\\u062a\\u0631\\u0627\",\"UG326\":\"\\u0644\\u0627\\u0645\\u0648\",\"UG307\":\"\\u0644\\u06cc\\u0631\",\"UG229\":\"\\u0644\\u0648\\u0648\\u06a9\\u0627\",\"UG104\":\"\\u0644\\u0648\\u0648\\u0631\\u0648\",\"UG124\":\"\\u0644\\u0648\\u0646\\u06af\\u0648\",\"UG114\":\"Lyantonde\",\"UG223\":\"\\u0645\\u0627\\u0646\\u0627\\u0641\\u0648\\u0627\",\"UG320\":\"\\u0645\\u0627\\u0631\\u0627\\u0686\\u0627\",\"UG105\":\"\\u0645\\u0627\\u0633\\u0627\\u06a9\\u0627\",\"UG409\":\"\\u0645\\u0627\\u0633\\u0646\\u062f\\u06cc\",\"UG214\":\"\\u0645\\u0627\\u06cc\\u0648\",\"UG209\":\"\\u0645\\u0628\\u0627\\u0644\\u0647\",\"UG410\":\"\\u0645\\u0628\\u0627\\u0631\\u06a9\\u0647\",\"UG423\":\"\\u0645\\u06cc\\u062a\\u0648\\u0645\\u0627\",\"UG115\":\"\\u0645\\u06cc\\u062a\\u0627\\u0646\\u0627\",\"UG308\":\"\\u0645\\u0631\\u0648\\u0648\\u062a\\u0648\",\"UG309\":\"\\u0645\\u0648\\u0648\\u06cc\",\"UG106\":\"Mpigi\",\"UG107\":\"\\u0645\\u0648\\u0648\\u0628\\u0646\\u062f\",\"UG108\":\"\\u0645\\u0648\\u06a9\\u0648\\u0646\\u0648\",\"UG334\":\"\\u0646\\u0628\\u06cc\\u0644\\u0627\\u062a\\u0648\\u06a9\",\"UG311\":\"\\u0646\\u0627\\u06a9\\u0627\\u067e\\u06cc\\u0631\\u06cc\\u067e\\u06cc\\u0631\\u06cc\\u062a\",\"UG116\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u06a9\\u0647\",\"UG109\":\"\\u0646\\u0627\\u06a9\\u0627\\u0633\\u0648\\u0646\\u06af\\u0644\\u0627\",\"UG230\":\"\\u0646\\u0627\\u0645\\u0627\\u06cc\\u06cc\\u0646\\u06af\\u0648\",\"UG234\":\"\\u0646\\u0627\\u0645\\u06cc\\u0633\\u06cc\\u0646\\u0648\\u0627\",\"UG224\":\"\\u0646\\u0627\\u0645\\u0648\\u062a\\u0648\\u0645\\u0628\\u0627\",\"UG327\":\"\\u0646\\u0627\\u067e\\u0627\\u06a9\",\"UG310\":\"\\u0646\\u0628\\u06cc\",\"UG231\":\"\\u0646\\u06af\\u0648\\u0631\\u0627\",\"UG424\":\"\\u0646\\u062a\\u0648\\u0631\\u0648\\u06a9\\u0648\",\"UG411\":\"\\u0646\\u0627\\u062a\\u0648\\u0646\\u06af\\u0627\\u0645\\u0648\",\"UG328\":\"\\u0646\\u0648\\u06cc\\u0627\",\"UG331\":\"\\u0627\\u0648\\u0645\\u0648\\u0631\\u0648\",\"UG329\":\"\\u0627\\u0648\\u062a\\u0648\\u06a9\\u0647\",\"UG321\":\"\\u0627\\u0648\\u06cc\\u0627\\u0645\",\"UG312\":\"\\u067e\\u062f\\u0631\",\"UG332\":\"\\u067e\\u0627\\u06a9\\u0648\\u0627\\u0686\",\"UG210\":\"\\u067e\\u0627\\u0644\\u06cc\\u0632\\u0627\",\"UG110\":\"\\u0631\\u0627\\u06a9\\u0627\\u06cc\\u06cc\",\"UG429\":\"\\u0631\\u0648\\u0628\\u0627\\u0646\\u062f\\u0627\",\"UG425\":\"\\u0631\\u0648\\u0628\\u06cc\\u0631\\u06cc\\u0632\\u06cc\",\"UG431\":\"\\u0631\\u06a9\\u06cc\\u06af\\u0627\",\"UG412\":\"Rukungiri\",\"UG111\":\"\\u0627\\u0633\\u0645\\u0628\\u0644\",\"UG232\":\"\\u0633\\u06cc\\u0631\",\"UG426\":\"\\u0634\\u06cc\\u0645\\u0627\",\"UG215\":\"\\u0633\\u06cc\\u0631\\u0648\\u0646\\u06a9\\u0648\",\"UG211\":\"\\u0633\\u0648\\u0631\\u0648\\u062a\\u06cc\",\"UG212\":\"\\u062a\\u0648\\u0631\\u0648\\u0631\\u0648\",\"UG113\":\"\\u0648\\u0627\\u06a9\\u06cc\\u0633\\u0648\",\"UG313\":\"\\u06cc\\u0648\\u0645\\u0628\",\"UG330\":\"\\u0632\\u0648\\u0645\\u0628\\u0648\"},\"UM\":{\"81\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0628\\u06cc\\u06a9\\u0631\",\"84\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 Howland\",\"86\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u062c\\u0627\\u0631\\u0648\\u06cc\\u0633\",\"67\":\"\\u062c\\u0627\\u0646\\u0633\\u062a\\u0648\\u0646 \\u0627\\u062a\\u0644\",\"89\":\"\\u06a9\\u06cc\\u0646\\u06af\\u0645\\u0646 \\u0631\\u06cc\\u0641\",\"71\":\"Midway Atoll\",\"76\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0646\\u0627\\u0648\\u0627\\u0633\\u0627\",\"95\":\"\\u067e\\u0627\\u0644\\u0645\\u06cc\\u0631\\u0627 \\u0627\\u062a\\u0644\",\"79\":\"\\u062c\\u0632\\u06cc\\u0631\\u0647 \\u0648\\u06cc\\u06a9\"},\"US\":{\"AL\":\"Alabama\",\"AK\":\"Alaska\",\"AZ\":\"Arizona\",\"AR\":\"Arkansas\",\"CA\":\"California\",\"CO\":\"Colorado\",\"CT\":\"Connecticut\",\"DE\":\"Delaware\",\"DC\":\"District Of Columbia\",\"FL\":\"Florida\",\"GA\":\"Georgia\",\"HI\":\"Hawaii\",\"ID\":\"Idaho\",\"IL\":\"Illinois\",\"IN\":\"Indiana\",\"IA\":\"Iowa\",\"KS\":\"Kansas\",\"KY\":\"Kentucky\",\"LA\":\"Louisiana\",\"ME\":\"Maine\",\"MD\":\"Maryland\",\"MA\":\"Massachusetts\",\"MI\":\"Michigan\",\"MN\":\"Minnesota\",\"MS\":\"Mississippi\",\"MO\":\"Missouri\",\"MT\":\"Montana\",\"NE\":\"Nebraska\",\"NV\":\"Nevada\",\"NH\":\"New Hampshire\",\"NJ\":\"New Jersey\",\"NM\":\"New Mexico\",\"NY\":\"New York\",\"NC\":\"North Carolina\",\"ND\":\"North Dakota\",\"OH\":\"Ohio\",\"OK\":\"Oklahoma\",\"OR\":\"Oregon\",\"PA\":\"Pennsylvania\",\"RI\":\"Rhode Island\",\"SC\":\"South Carolina\",\"SD\":\"South Dakota\",\"TN\":\"Tennessee\",\"TX\":\"Texas\",\"UT\":\"Utah\",\"VT\":\"Vermont\",\"VA\":\"Virginia\",\"WA\":\"Washington\",\"WV\":\"West Virginia\",\"WI\":\"Wisconsin\",\"WY\":\"Wyoming\",\"AA\":\"Armed Forces (AA)\",\"AE\":\"Armed Forces (AE)\",\"AP\":\"Armed Forces (AP)\"},\"VN\":[],\"YT\":[],\"ZA\":{\"EC\":\"Eastern Cape\",\"FS\":\"Free State\",\"GP\":\"Gauteng\",\"KZN\":\"KwaZulu-Natal\",\"LP\":\"Limpopo\",\"MP\":\"Mpumalanga\",\"NC\":\"Northern Cape\",\"NW\":\"North West\",\"WC\":\"Western Cape\"},\"ZM\":{\"ZM-01\":\"\\u063a\\u0631\\u0628\\u06cc\",\"ZM-02\":\"\\u0645\\u0631\\u06a9\\u0632\\u06cc\",\"ZM-03\":\"\\u0634\\u0631\\u0642\\u06cc\",\"ZM-04\":\"\\u0644\\u0648\\u0622\\u067e\\u0648\\u0644\\u0627\",\"ZM-05\":\"\\u0634\\u0645\\u0627\\u0644\\u06cc\",\"ZM-06\":\"\\u0634\\u0645\\u0627\\u0644 \\u063a\\u0631\\u0628\\u06cc\",\"ZM-07\":\"\\u062c\\u0646\\u0648\\u0628\\u06cc\",\"ZM-08\":\"\\u0645\\u0633\\u06cc\",\"ZM-09\":\"\\u0644\\u0648\\u0633\\u0627\\u06a9\\u0627\",\"ZM-10\":\"\\u0645\\u0648\\u0686\\u06cc\\u0646\\u06af\\u0627\"}}","i18n_select_state_text":"\u06cc\u06a9 \u06af\u0632\u06cc\u0646\u0647 \u0627\u0646\u062a\u062e\u0627\u0628 \u0646\u0645\u0627\u0626\u06cc\u062f\u2026","i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631\u2026","i18n_searching":"\u062c\u0633\u062a\u062c\u0648 \u2026"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/country-select.min.js')}}' id='wc-country-select-js'></script>
<script type='text/javascript' id='yith-wcaf-js-extra'>
    /* <![CDATA[ */
    var yith_wcaf = {"labels":{"select2_i18n_matches_1":"One result is available, press enter to select it.","select2_i18n_matches_n":"%qty% results are available, use up and down arrow keys to navigate.","select2_i18n_no_matches":"\u06cc\u0627\u0641\u062a \u0646\u0634\u062f","select2_i18n_ajax_error":"\u0628\u0627\u0631\u06af\u0632\u0627\u0631\u06cc \u0646\u0627\u0645\u0648\u0641\u0642","select2_i18n_input_too_short_1":"\u062e\u0648\u0627\u0647\u0634\u0645\u0646\u062f \u0627\u0633\u062a 1 \u06cc\u0627 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f","select2_i18n_input_too_short_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a %qty% \u06cc\u0627 \u06a9\u0627\u0631\u06a9\u062a\u0631\u0647\u0627\u06cc \u0628\u06cc\u0634\u062a\u0631\u06cc \u0631\u0627 \u0628\u0646\u0648\u06cc\u0633\u06cc\u062f ","select2_i18n_input_too_long_1":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 1 \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_input_too_long_n":"\u062e\u0648\u0627\u0634\u0645\u0646\u062f \u0627\u0633\u062a \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631\u0647\u0627\u06cc %qty% \u0631\u0627 \u067e\u0627\u06a9 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_1":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 1 \u0645\u0648\u0631\u062f \u0631\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc \u062a\u0648\u0627\u0646\u06cc\u062f \u0645\u0648\u0627\u0631\u062f %qty% \u0631\u0627 \u06af\u0632\u06cc\u0646\u0634 \u06a9\u0646\u06cc\u062f","select2_i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631&hellip;","select2_i18n_searching":"\u062c\u0633\u062a\u062c\u0648 &hellip;","link_copied_message":"Url copied"},"ajax_url":".\/wp-admin\/admin-ajax.php","set_cookie_via_ajax":"","referral_var":"ref","search_products_nonce":"5a71607834","set_referrer_nonce":"d4694eef96","get_withdraw_amount":"93c26c81f7"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/yith-wcaf.min.js')}}' id='yith-wcaf-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.blockUI.min.js')}}' id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/?wc-ajax=%%endpoint%%","i18n_view_cart":"\u0645\u0634\u0627\u0647\u062f\u0647 \u0633\u0628\u062f \u062e\u0631\u06cc\u062f","cart_url":".\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart.min.js')}}' id='wc-add-to-cart-js'></script>
<script type='text/javascript' id='wc-single-product-js-extra'>
    /* <![CDATA[ */
    var wc_single_product_params = {"i18n_required_rating_text":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0627\u0645\u062a\u06cc\u0627\u0632 \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f","review_rating_required":"yes","flexslider":{"rtl":true,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"","zoom_options":[],"photoswipe_enabled":"","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/single-product.min.js')}}' id='wc-single-product-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/js.cookie.min.js')}}' id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/woocommerce.min.js')}}' id='woocommerce-js'></script>
<!--<script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_8bdcabaa74177c68e22f1e2d60dfc53f","fragment_name":"wc_fragments_8bdcabaa74177c68e22f1e2d60dfc53f","request_timeout":"5000"};
    /* ]]> */
</script>-->
<script type='text/javascript' src='{{asset('newFront/js/cart-fragments.min.js')}}' id='wc-cart-fragments-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/drag-arrange.js')}}' id='dragarrange-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/table-head-fixer.js')}}' id='table-head-fixer-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/perfect-scrollbar.jquery.min.js')}}' id='perfect-scrollbar-js'></script>
<script type='text/javascript' id='wooscp-frontend-js-extra'>
    /* <![CDATA[ */
    var wooscpVars = {"ajaxurl":".\/wp-admin\/admin-ajax.php","user_id":"5d683352529913f1f848a4c9e3f5b2b8","open_button":"","open_table":"yes","open_bar":"no","click_again":"no","remove_all":"Do you want to remove all products from the compare?","hide_empty":"no","click_outside":"yes","freeze_column":"yes","freeze_row":"yes","limit":"100","limit_notice":"You can add a maximum of {limit} products to the compare table.","nonce":"b42d132d5b"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/frontend.js')}}' id='wooscp-frontend-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/popper.min.js')}}' id='negarshop-popper-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap.min.js')}}' id='negarshop-bootstrap-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/bootstrap-notify.min.js')}}' id='negarshop-bootstrap-notify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/nouislider.min.js')}}' id='negarshop-nouislider-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.confetti.js')}}' id='negarshop-confetti-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/owl.carousel.min.js')}}' id='negarshop-owl-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/lightgallery.min.js')}}' id='negarshop-lightbox-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/printThis.min.js')}}' id='negarshop-print-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/accounting.min.js')}}' id='negarshop-accounting-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/select2.js')}}' id='negarshop-select2-js'></script>
{{--<script type='text/javascript' src={{asset('newFront/js/highcharts.js')}}' id='negarshop-highchart-js'></script>--}}
{{--<script type='text/javascript' src='{{asset('newFront/js/highcharts-exporting.js')}}' id='negarshop-highchart-exporting-js'></script>--}}
<script type='text/javascript' id='negarshop-ajax-tab-carousel-js-extra'>
    /* <![CDATA[ */
    // var negarshop_obj = {"ajax_url":".\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/tab-carousel.js')}}' id='negarshop-ajax-tab-carousel-js'></script>
<script type='text/javascript' id='negarshop-var-product-js-extra'>
    /* <![CDATA[ */
    // var negarshop_obj = {"ajax_url":".\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/variable_product_data.js')}}' id='negarshop-var-product-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/underscore.min.js')}}' id='underscore-js'></script>
<script type='text/javascript' id='wp-util-js-extra'>
    /* <![CDATA[ */
    // var _wpUtilSettings = {"ajax":{"url":"\/wp\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/wp-util.min.js')}}s' id='wp-util-js'></script>
<script type='text/javascript' id='wc-add-to-cart-variation-js-extra'>
    /* <![CDATA[ */
    // var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/wp\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"\u0628\u0627 \u0639\u0631\u0636 \u067e\u0648\u0632\u0634\u060c \u0647\u06cc\u0686 \u0643\u0627\u0644\u0627\u064a\u06cc \u0645\u0637\u0627\u0628\u0642 \u0627\u0646\u062a\u062e\u0627\u0628 \u0634\u0645\u0627 \u06cc\u0627\u0641\u062a \u0646\u0634\u062f. \u0644\u0637\u0641\u0627\u064b \u062a\u0631\u06a9\u06cc\u0628 \u062f\u06cc\u06af\u0631\u06cc \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_make_a_selection_text":"\u0644\u0637\u0641\u0627 \u0628\u0631\u062e\u06cc \u0627\u0632 \u06af\u0632\u06cc\u0646\u0647\u200c\u0647\u0627\u06cc \u0645\u062d\u0635\u0648\u0644 \u0631\u0627 \u0642\u0628\u0644 \u0627\u0632 \u0627\u0636\u0627\u0641\u0647 \u06a9\u0631\u062f\u0646 \u0622\u0646 \u0628\u0647 \u0633\u0628\u062f \u062e\u0631\u06cc\u062f\u060c \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_unavailable_text":"\u0628\u0627 \u0639\u0631\u0636 \u067e\u0648\u0632\u0634\u060c \u0627\u06cc\u0646 \u0643\u0627\u0644\u0627 \u062f\u0631 \u062f\u0633\u062a\u0631\u0633 \u0646\u06cc\u0633\u062a. \u0644\u0637\u0641\u0627\u064b \u062a\u0631\u06a9\u06cc\u0628 \u062f\u06cc\u06af\u0631\u06cc \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f."};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/add-to-cart-variation.min.js')}}' id='wc-add-to-cart-variation-js'></script>
<script type='text/javascript' id='negarshop-script-js-extra'>
    /* <![CDATA[ */
    // var negarshop_obj = {"ajax_url":".\/wp-admin\/admin-ajax.php","my_account":".\/my-account\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/script.js')}}' id='negarshop-script-js'></script>
{{--<script type='text/javascript' src='{{asset('newFront/script.js')}}' id='negarshop-script-js'></script>--}}
<script type='text/javascript' id='negarshop-cb-change-price-js-extra'>
    /* <![CDATA[ */
    // var negarshop_obj = {"ajax_url":".\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/price-changes.js')}}' id='negarshop-cb-change-price-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/comment-reply.min.js')}}' id='comment-reply-js'></script>

<script type='text/javascript' src='{{asset('newFront/js/product-3d.js')}}' id='negarshop-three-int-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify.js')}}' id='negarshop-magnify-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.magnify-mobile.js')}}' id='negarshop-magnify-mobile-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/jquery.countdown.min.js')}}' id='negarshop-countdown-js-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/yith-color-atts.js')}}' id='yith_wccl_frontend_cb-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/mouse.min.js')}}' id='jquery-ui-mouse-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/sortable.min.js')}}' id='jquery-ui-sortable-js'></script>
<script type='text/javascript' src='{{asset('newFront/js/tooltips.js')}}' id='dokan-tooltip-js'></script>
<script type='text/javascript' id='dokan-form-validate-js-extra'>
    /* <![CDATA[ */
    var DokanValidateMsg = {"required":"\u0627\u06cc\u0646 \u0641\u06cc\u0644\u062f \u0627\u0644\u0632\u0627\u0645\u06cc \u0627\u0633\u062a.","remote":"\u0644\u0637\u0641\u0627 \u0627\u06cc\u0646 \u0641\u06cc\u0644\u062f \u0631\u0627 \u0627\u0635\u0644\u0627\u062d \u06a9\u0646\u06cc\u062f.","email":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0622\u062f\u0631\u0633 \u0627\u06cc\u0645\u06cc\u0644 \u0645\u0639\u062a\u0628\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","url":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0646\u0634\u0627\u0646\u06cc \u0648\u0628 \u0645\u0639\u062a\u0628\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","date":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u062a\u0627\u0631\u06cc\u062e \u0645\u0639\u062a\u0628\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","dateISO":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u062a\u0627\u0631\u06cc\u062e \u0645\u0639\u062a\u0628\u0631 (ISO) \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","number":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0634\u0645\u0627\u0631\u0647 \u062a\u0644\u0641\u0646 \u0645\u0639\u062a\u0628\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","digits":"\u0644\u0637\u0641\u0627 \u0641\u0642\u0637 \u0631\u0642\u0645 \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","creditcard":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0634\u0645\u0627\u0631\u0647 \u06a9\u0627\u0631\u062a \u0627\u0639\u062a\u0628\u0627\u0631\u06cc \u0645\u0639\u062a\u0628\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","equalTo":"\u0644\u0637\u0641\u0627 \u0645\u062c\u062f\u062f\u0627 \u0647\u0645\u0627\u0646 \u0645\u0642\u062f\u0627\u0631 \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","maxlength_msg":"\u0644\u0637\u0641\u0627 \u0628\u06cc\u0634 \u0627\u0632 {0} \u062d\u0631\u0641 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","minlength_msg":"\u0644\u0637\u0641\u0627 \u062d\u062f\u0627\u0642\u0644 {0} \u062d\u0631\u0641 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","rangelength_msg":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u0628\u06cc\u0646 {0} \u0648 {1} \u062d\u0631\u0641 \u0637\u0648\u0644\u0627\u0646\u06cc \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","range_msg":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u0628\u06cc\u0646 {0} \u0648 {1} \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","max_msg":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u06a9\u0645\u062a\u0631 \u06cc\u0627 \u0628\u0631\u0627\u0628\u0631 {0} \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","min_msg":"\u0644\u0637\u0641\u0627 \u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u0628\u0632\u0631\u06af\u062a\u0631 \u06cc\u0627 \u0628\u0631\u0627\u0628\u0631 {0} \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f."};
    /* ]]> */
</script>
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/vendors/form-validate/form-validate.js' id='dokan-form-validate-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/js/speakingurl.min.js' id='speaking-url-js'></script>--}}
<script type='text/javascript' id='dokan-i18n-jed-js-extra'>
    /* <![CDATA[ */
    // var dokan = {"ajaxurl":".\/wp-admin\/admin-ajax.php","nonce":"6baff2cbc8","ajax_loader":".\/wp-content\/plugins\/dokan-lite\/assets\/images\/ajax-loader.gif","seller":{"available":"\u062f\u0631 \u062f\u0633\u062a\u0631\u0633","notAvailable":"\u0645\u0648\u062c\u0648\u062f \u0646\u06cc\u0633\u062a."},"delete_confirm":"\u0634\u0645\u0627 \u0645\u0637\u0645\u0626\u0646 \u0647\u0633\u062a\u06cc\u062f\u061f","wrong_message":"\u0638\u0627\u0647\u0631\u0627\u064b \u0627\u0634\u062a\u0628\u0627\u0647\u06cc \u0635\u0648\u0631\u062a \u06af\u0631\u0641\u062a\u0647 \u0627\u0633\u062a. \u0644\u0637\u0641\u0627 \u062f\u0648\u0628\u0627\u0631\u0647 \u062a\u0644\u0627\u0634 \u06a9\u0646\u06cc\u062f.","vendor_percentage":"80","commission_type":"percentage","rounding_precision":"6","mon_decimal_point":".","product_types":["simple"],"i18n_choose_featured_img":"\u0622\u067e\u0644\u0648\u062f \u062a\u0635\u0648\u06cc\u0631 \u0634\u0627\u062e\u0635","i18n_choose_file":"\u0627\u0646\u062a\u062e\u0627\u0628 \u06cc\u06a9 \u0641\u0627\u06cc\u0644","i18n_choose_gallery":"\u0627\u0641\u0632\u0648\u062f\u0646 \u0639\u06a9\u0633 \u0628\u0647 \u06af\u0627\u0644\u0631\u06cc \u0645\u062d\u0635\u0648\u0644\u0627\u062a","i18n_choose_featured_img_btn_text":"\u062a\u0639\u06cc\u06cc\u0646 \u062a\u0635\u0648\u06cc\u0631 \u0634\u0627\u062e\u0635","i18n_choose_file_btn_text":"\u0648\u0627\u0631\u062f \u06a9\u0631\u062f\u0646 \u0622\u062f\u0631\u0633 \u0641\u0627\u06cc\u0644","i18n_choose_gallery_btn_text":"\u0627\u0641\u0632\u0648\u062f\u0646 \u0628\u0647 \u06af\u0627\u0644\u0631\u06cc","duplicates_attribute_messg":"\u0628\u0627 \u0639\u0631\u0636 \u067e\u0648\u0632\u0634\u060c \u0627\u06cc\u0646 \u06af\u0632\u06cc\u0646\u0647 \u0648\u06cc\u0698\u06af\u06cc \u062f\u0631 \u062d\u0627\u0644 \u062d\u0627\u0636\u0631 \u0648\u062c\u0648\u062f \u062f\u0627\u0631\u062f\u060c \u062f\u06cc\u06af\u0631\u06cc \u0631\u0627 \u0627\u0645\u062a\u062d\u0627\u0646 \u06a9\u0646\u06cc\u062f.","variation_unset_warning":"\u0647\u0634\u062f\u0627\u0631! \u0627\u06af\u0631 \u0627\u06cc\u0646 \u06af\u0632\u06cc\u0646\u0647 \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u0646\u06a9\u0646\u06cc\u062f\u060c \u0627\u06cc\u0646 \u0645\u062d\u0635\u0648\u0644 \u0647\u06cc\u0686 \u0646\u0648\u0639 \u0645\u062a\u063a\u06cc\u0631\u06cc \u0646\u062e\u0648\u0627\u0647\u062f \u062f\u0627\u0634\u062a.","new_attribute_prompt":"\u06cc\u06a9 \u0646\u0627\u0645 \u0628\u0631\u0627\u06cc \u0648\u06cc\u0698\u06af\u06cc \u062c\u062f\u06cc\u062f \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f:","remove_attribute":"\u062d\u0630\u0641 \u0627\u06cc\u0646 \u062e\u0635\u0648\u0635\u06cc\u062a\u061f","dokan_placeholder_img_src":".\/wp-content\/uploads\/woocommerce-placeholder-300x300.png","add_variation_nonce":"e47e1147ff","link_variation_nonce":"e17edc835d","delete_variations_nonce":"cd8f09d2c9","load_variations_nonce":"39e91302dd","save_variations_nonce":"85f7bc7ba2","bulk_edit_variations_nonce":"b3b65797e5","i18n_link_all_variations":"\u0622\u06cc\u0627 \u0634\u0645\u0627 \u0645\u0627\u06cc\u0644 \u0628\u0647 \u062a\u0631\u06a9\u06cc\u0628 \u062a\u0645\u0627\u0645 \u0645\u062a\u063a\u06cc\u0631\u0647\u0627 \u0647\u0633\u062a\u06cc\u062f\u061f \u062f\u0631 \u0627\u06cc\u0646 \u062d\u0627\u0644\u062a \u06cc\u06a9 \u0645\u062a\u063a\u06cc\u0631 \u062c\u062f\u06cc\u062f \u06a9\u0647 \u062d\u0627\u0648\u06cc \u062a\u0631\u06a9\u06cc\u0628 \u062a\u0645\u0627\u0645\u06cc \u0645\u0634\u062e\u0635\u0627\u062a \u0645\u0645\u06a9\u0646 \u0645\u062a\u063a\u06cc\u0631\u0647\u0627\u0633\u062a\u060c \u0633\u0627\u062e\u062a\u0647 \u0645\u06cc\u200c\u0634\u0648\u062f. (\u062f\u0631 \u0628\u06cc\u0634\u062a\u0631\u06cc\u0646 \u062d\u0627\u0644\u062a 50 \u0628\u0627\u0631 \u062f\u0631 \u0647\u0631 \u0627\u062c\u0631\u0627)","i18n_enter_a_value":"\u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u0648\u0627\u0631\u062f \u0646\u0645\u0627\u06cc\u06cc\u062f.","i18n_enter_menu_order":"\u0645\u0646\u0648\u06cc \u062a\u0646\u0648\u0639 \u0633\u0641\u0627\u0631\u0634 (\u062a\u0639\u06cc\u06cc\u0646 \u0645\u0648\u0642\u0639\u06cc\u062a \u062f\u0631 \u0644\u06cc\u0633\u062a \u062a\u063a\u06cc\u06cc\u0631\u0627\u062a)","i18n_enter_a_value_fixed_or_percent":"\u06cc\u06a9 \u0645\u0642\u062f\u0627\u0631 \u0648\u0627\u0631\u062f \u0646\u0645\u0627\u06cc\u06cc\u062f (\u062b\u0627\u0628\u062a \u06cc\u0627 %)","i18n_delete_all_variations":"\u0622\u06cc\u0627 \u0645\u0637\u0645\u0626\u0646 \u0647\u0633\u062a\u06cc\u062f \u06a9\u0647 \u0645\u06cc \u062e\u0648\u0627\u0647\u06cc\u062f \u062a\u0645\u0627\u0645\u06cc \u062a\u063a\u06cc\u06cc\u0631\u0627\u062a \u0631\u0627 \u062d\u0630\u0641 \u06a9\u0646\u06cc\u062f\u061f \u0627\u06cc\u0646 \u0642\u0627\u0628\u0644 \u0628\u0627\u0632\u06af\u0634\u062a \u0646\u06cc\u0633\u062a.","i18n_last_warning":"\u0627\u062e\u0631\u06cc\u0646 \u0647\u0634\u062f\u0627\u0631 \u0622\u06cc\u0627 \u0634\u0645\u0627 \u0627\u0632 \u0627\u06cc\u0646 \u06a9\u0627\u0631 \u0627\u0637\u0645\u06cc\u0646\u0627\u0646 \u062f\u0627\u0631\u06cc\u062f\u061f","i18n_choose_image":"\u0627\u0646\u062a\u062e\u0627\u0628 \u062a\u0635\u0648\u06cc\u0631","i18n_set_image":"\u062a\u0646\u0638\u06cc\u0645 \u062a\u0635\u0648\u06cc\u0631 \u0645\u062a\u063a\u06cc\u0631","i18n_variation_added":"\u062a\u063a\u06cc\u06cc\u0631 \u0627\u0636\u0627\u0641\u0647 \u0634\u062f.","i18n_variations_added":"\u062a\u063a\u06cc\u06cc\u0631\u0627\u062a \u0627\u0636\u0627\u0641\u0647 \u0634\u062f.","i18n_no_variations_added":"\u062a\u063a\u06cc\u06cc\u0631\u0627\u062a\u06cc \u0627\u0636\u0627\u0641\u0647 \u0646\u0634\u062f\u0647 \u0627\u0633\u062a.","i18n_remove_variation":"\u0645\u0637\u0645\u0626\u0646\u06cc\u062f \u06a9\u0647 \u0645\u06cc \u062e\u0648\u0627\u0647\u06cc\u062f  \u0627\u06cc\u0646 \u062a\u063a\u06cc\u06cc\u0631 \u0631\u0627 \u062d\u0630\u0641 \u06a9\u0646\u06cc\u062f\u061f","i18n_scheduled_sale_start":"\u062a\u0627\u0631\u06cc\u062e \u0634\u0631\u0648\u0639 \u062d\u0631\u0627\u062c (\u062f\u0631 \u0642\u0627\u0644\u0628 YYYY-MM-DD \u06cc\u0627 \u062e\u0627\u0644\u06cc \u0628\u06af\u0630\u0627\u0631\u06cc\u062f)","i18n_scheduled_sale_end":"\u062a\u0627\u0631\u06cc\u062e \u067e\u0627\u06cc\u0627\u0646 \u0641\u0631\u0648\u0634 (\u062f\u0631 \u0642\u0627\u0644\u0628 YYYY-MM-DD \u06cc\u0627 \u062e\u0627\u0644\u06cc \u0628\u06af\u0630\u0627\u0631\u06cc\u062f)","i18n_edited_variations":"\u062a\u063a\u06cc\u06cc\u0631\u0627\u062a \u0631\u0627 \u0630\u062e\u06cc\u0631\u0647 \u06a9\u0646\u06cc\u062f \u0642\u0628\u0644 \u0627\u0632 \u0627\u06cc\u0646\u06a9\u0647 \u0635\u0641\u062d\u0647 \u0631\u0627 \u062a\u063a\u06cc\u06cc\u0631 \u062f\u0647\u06cc\u062f.","i18n_variation_count_single":"%qty% \u0645\u062a\u063a\u06cc\u0631","i18n_variation_count_plural":"%qty% \u0645\u062a\u063a\u06cc\u0631\u0647\u0627","i18n_no_result_found":"\u0646\u062a\u06cc\u062c\u0647\u200c\u0627\u06cc \u06cc\u0627\u0641\u062a \u0646\u0634\u062f.","i18n_sales_price_error":"\u0644\u0637\u0641\u0627\u064b \u0627\u0631\u0632\u0634 \u06a9\u0645\u062a\u0631\u06cc \u0627\u0632 \u0642\u06cc\u0645\u062a \u0645\u0639\u0645\u0648\u0644\u06cc \u062f\u0631\u062c \u06a9\u0646\u06cc\u062f!","i18n_decimal_error":"Please enter with one decimal point (.) without thousand separators.","i18n_mon_decimal_error":"Please enter with one monetary decimal point (.) without thousand separators and currency symbols.","i18n_country_iso_error":"Please enter in country code with two capital letters.","i18n_sale_less_than_regular_error":"Please enter in a value less than the regular price.","i18n_delete_product_notice":"This product has produced sales and may be linked to existing orders. Are you sure you want to delete it?","i18n_remove_personal_data_notice":"This action cannot be reversed. Are you sure you wish to erase personal data from the selected orders?","decimal_point":".","variations_per_page":"10","store_banner_dimension":{"width":"625","height":"300","flex-width":true,"flex-height":true},"selectAndCrop":"\u0627\u0646\u062a\u062e\u0627\u0628 \u0648 \u0628\u0631\u0634","chooseImage":"\u0627\u0646\u062a\u062e\u0627\u0628 \u062a\u0635\u0648\u06cc\u0631","product_title_required":"\u0639\u0646\u0648\u0627\u0646 \u06a9\u0627\u0644\u0627 \u0627\u0644\u0632\u0627\u0645\u06cc \u0645\u06cc \u0628\u0627\u0634\u062f.","product_category_required":"\u062f\u0633\u062a\u0647 \u0645\u062d\u0635\u0648\u0644 \u0636\u0631\u0648\u0631\u06cc \u0627\u0633\u062a.","search_products_nonce":"5a71607834","search_products_tags_nonce":"9b6a40e964","search_customer_nonce":"d2b3495bbc","i18n_matches_1":"\u06cc\u06a9 \u0646\u062a\u06cc\u062c\u0647 \u062f\u0631 \u062f\u0633\u062a\u0631\u0633 \u0627\u0633\u062a\u060c \u0628\u0631\u0627\u06cc \u0627\u0646\u062a\u062e\u0627\u0628 \u0622\u0646 \u0631\u0627 \u0641\u0634\u0627\u0631 \u062f\u0647\u06cc\u062f.","i18n_matches_n":"%qty% \u0646\u062a\u06cc\u062c\u0647 \u062f\u0631 \u062f\u0633\u062a\u0631\u0633 \u0627\u0633\u062a. \u0628\u0627 \u0627\u0633\u062a\u0641\u0627\u062f\u0647 \u0627\u0632 \u06a9\u0644\u06cc\u062f\u0647\u0627\u06cc \u0628\u0627\u0644\u0627 \u0648 \u067e\u0627\u06cc\u06cc\u0646 \u0628\u0647 \u0622\u0646\u0647\u0627 \u062f\u0633\u062a\u0631\u0633\u06cc \u062f\u0627\u0634\u062a\u0647 \u0628\u0627\u0634\u06cc\u062f.","i18n_no_matches":"\u062c\u0633\u062a\u062c\u0648 \u062d\u0627\u0635\u0644\u06cc \u062f\u0631\u0628\u0631\u0646\u062f\u0627\u0634\u062a.","i18n_ajax_error":"\u0628\u0627\u0631\u06af\u06cc\u0631\u06cc \u0627\u0646\u062c\u0627\u0645 \u0646\u0634\u062f.","i18n_input_too_short_1":"\u0644\u0637\u0641\u0627 1 \u06cc\u0627 \u0686\u0646\u062f \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 \u0631\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","i18n_input_too_short_n":"\u0644\u0637\u0641\u0627 \u062a\u0639\u062f\u0627\u062f %qty% \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 \u06cc\u0627 \u0628\u06cc\u0634\u062a\u0631 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","i18n_input_too_long_1":"\u0644\u0637\u0641\u0627 1 \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 \u0631\u0627 \u062d\u0630\u0641 \u06a9\u0646\u06cc\u062f.","i18n_input_too_long_n":"\u0644\u0637\u0641\u0627\u064b %qty% \u06a9\u0627\u0631\u0627\u06a9\u062a\u0631 \u0631\u0627 \u062d\u0630\u0641 \u06a9\u0646\u06cc\u062f.","i18n_selection_too_long_1":"\u0634\u0645\u0627 \u0641\u0642\u0637 \u0645\u06cc\u200c\u062a\u0648\u0627\u0646\u06cc\u062f 1 \u0622\u06cc\u062a\u0645 \u0631\u0627 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_selection_too_long_n":"\u0634\u0645\u0627 \u062a\u0646\u0647\u0627 \u0645\u06cc\u200c\u062a\u0648\u0627\u0646\u06cc\u062f %qty% \u0622\u06cc\u062a\u0645 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","i18n_load_more":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0646\u062a\u0627\u06cc\u062c \u0628\u06cc\u0634\u062a\u0631\u2026","i18n_searching":"\u062f\u0631\u062d\u0627\u0644 \u062c\u0633\u062a\u062c\u0648\u2026","i18n_date_format":"F j, Y","rest":{"root":".\/wp-json\/","nonce":"0bb9e24f80","version":"dokan\/v1"},"api":null,"libs":[],"routeComponents":{"default":null},"routes":[],"urls":{"assetsUrl":".\/wp-content\/plugins\/dokan-lite\/assets"}};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('newFront/js/jed.js')}}' id='dokan-i18n-jed-js'></script>
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/js/vendor-registration.js' id='dokan-vendor-registration-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/imgareaselect/jquery.imgareaselect.min.js' id='imgareaselect-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/customize-base.min.js' id='customize-base-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/backbone.min.js' id='backbone-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/customize-models.js' id='customize-model-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/woocommerce/assets/js/jquery-tiptip/jquery.tipTip.min.js' id='jquery-tiptip-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/js/dokan.js' id='dokan-script-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/vendors/select2/select2.full.min.js' id='dokan-select2-js-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/vendors/magnific/jquery.magnific-popup.min.js' id='dokan-popup-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/dokan-lite/assets/js/login-form-popup.js' id='dokan-login-form-popup-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/wp-embed.min.js' id='wp-embed-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-includes/js/imagesloaded.min.js' id='imagesloaded-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/mailchimp-for-wp/assets/js/forms.min.js' id='mc4wp-forms-api-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/js/frontend-modules.min.js' id='elementor-frontend-modules-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js' id='elementor-sticky-js'></script>--}}
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
    var ElementorProFrontendConfig = {"ajaxurl":".\/wp-admin\/admin-ajax.php","nonce":"dec55f885d","i18n":{"toc_no_headings_found":"\u0647\u06cc\u0686 \u0639\u0646\u0648\u0627\u0646\u06cc \u062f\u0631 \u0627\u06cc\u0646 \u0635\u0641\u062d\u0647 \u06cc\u0627\u0641\u062a \u0646\u0634\u062f."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"google":{"title":"Google+","has_counter":true},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"menu_cart":{"cart_page_url":".\/cart\/","checkout_page_url":".\/checkout\/"},"facebook_sdk":{"lang":"fa_IR","app_id":""},"lottie":{"defaultAnimationUrl":".\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor-pro/assets/js/frontend.min.js' id='elementor-pro-frontend-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js' id='elementor-dialog-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js' id='elementor-waypoints-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js' id='swiper-js'></script>--}}
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js' id='share-link-js'></script>--}}
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u0641\u06cc\u0633\u200c\u0628\u0648\u06a9","shareOnTwitter":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc \u0631\u0648\u06cc \u062a\u0648\u06cc\u06cc\u062a\u0631","pinIt":"\u0633\u0646\u062c\u0627\u0642 \u06a9\u0646","download":"\u062f\u0631\u06cc\u0627\u0641\u062a","downloadImage":"\u062f\u0627\u0646\u0644\u0648\u062f \u062a\u0635\u0648\u06cc\u0631","fullscreen":"\u062a\u0645\u0627\u0645 \u0635\u0641\u062d\u0647","zoom":"\u0628\u0632\u0631\u06af\u0646\u0645\u0627\u06cc\u06cc","share":"\u0627\u0634\u062a\u0631\u0627\u06a9\u200c\u06af\u0630\u0627\u0631\u06cc","playVideo":"\u067e\u062e\u0634 \u0648\u06cc\u062f\u06cc\u0648","previous":"\u0642\u0628\u0644\u06cc","next":"\u0628\u0639\u062f\u06cc","close":"\u0628\u0633\u062a\u0646"},"is_rtl":true,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.14","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":".\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"body_background_background":"classic","global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":281,"title":"%D9%87%D9%88%D8%A7%D9%88%D8%A7%DB%8C%20%D9%85%DB%8C%D8%AA%20%D8%A8%D9%88%DA%A9%20X%20%D9%BE%D8%B1%D9%88%2013.9%20%D8%A7%DB%8C%D9%86%DA%86%20-%20%D9%86%DA%AF%D8%A7%D8%B1%D8%B4%D8%A7%D9%BE","excerpt":"","featuredImage":".\/wp-content\/uploads\/2019\/05\/8149py2u1cL._SL1500_-1.jpg"}};
</script>
{{--<script type='text/javascript' src='./../../wp-content/plugins/elementor/assets/js/frontend.min.js' id='elementor-frontend-js'></script>--}}
<!-- WooCommerce JavaScript -->
<!--<script type="text/javascript">
    jQuery(function($) {

        jQuery(document).ready(function($){
            $(".sms-notif-content").hide();
            $(document.body).on( "change", ".sms-notif-enable", function() {
                if( $(this).is(":checked") )
                    $(this).closest("form").find(".sms-notif-content").fadeIn();
                else
                    $(this).closest("form").find(".sms-notif-content").fadeOut();
            }).on( "click", ".sms-notif-submit", function() {
                var form = $(this).closest("form");
                var result = form.find(".sms-notif-result");
                result.html( "<img style=\"width:16px;display:inline;\" src=\"./wp-content/plugins/persian-woocommerce-sms/assets/images/ajax-loader.gif\" />" );
                var sms_group = [];
                form.find(".sms-notif-groups:checked").each(function(i){
                    sms_group[i] = $(this).val();
                });
                $.ajax({
                    url : "./wp-admin/admin-ajax.php",
                    type : "post",
                    data : {
                        action : "wc_sms_save_notification_data",
                        security: "694046828a",
                        sms_mobile : form.find(".sms-notif-mobile").val(),
                        sms_group : sms_group,
                        product_id : "281",
                    },
                    success : function( response ) {
                        result.html( response );
                    }
                });
                return false;
            });
        });

    });
</script>-->
<!--<script  src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.min.css.map"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.3/rangeslider.min.js"></script>
<script>
        // Initialize a new plugin instance for all
        // e.g. $('input[type="range"]') elements.
        $('input[type="range"]').rangeslider();

        // Destroy all plugin instances created from the
        // e.g. $('input[type="range"]') elements.
        $('input[type="range"]').rangeslider('destroy');

        // Update all rangeslider instances for all
        // e.g. $('input[type="range"]') elements.
        // Usefull if you changed some attributes e.g. `min` or `max` etc.
        $('input[type="range"]').rangeslider('update', true);
        $('input[type="range"]').rangeslider({

            // Feature detection the default is `true`.
            // Set this to `false` if you want to use
            // the polyfill also in Browsers which support
            // the native <input type="range"> element.
            polyfill: true,

            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider&#45;&#45;disabled',
            horizontalClass: 'rangeslider&#45;&#45;horizontal',
            verticalClass: 'rangeslider&#45;&#45;vertical',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',

            // Callback function
            onInit: function() {},

            // Callback function
            onSlide: function(position, value) {},

            // Callback function
            onSlideEnd: function(position, value) {}
        });
</script>-->
</body>
</html>
