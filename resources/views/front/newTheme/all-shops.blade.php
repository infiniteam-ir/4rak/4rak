@extends('front.newTheme.master')
@section('title',' فروشگاه ها')
@section('main')
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <nav class="woocommerce-breadcrumb"><i class="flaticon-placeholder"></i><a href="{{route('store')}}">خانه</a><span>&nbsp;&#47;&nbsp;</span><span class="current">فروشگاه ها</span></nav><div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header">
                            <h1 class="woocommerce-products-header__title page-title">فروشگاه ها</h1>

<!--                            <div class="sort-tabs">
                                <h6>مرتب سازی بر اساس :</h6>
                                <ul class="products-archive-tabs">
                                    <li><button class="btn" id="btn-sort-popularity" data-value="popularity">محبوب‌ترین</button></li>
                                    <li><button class="btn" id="btn-sort-rating" data-value="rating">رتبه بندی</button></li>
                                </ul>
                            </div>-->
                        </header>
                        <div class="woocommerce-notices-wrapper"></div>
                        <p class="woocommerce-result-count">
                            نمایش دادن همه <i id="res_qty">{{count($shops)}}</i> نتیجه</p>
<!--                        <form class="woocommerce-ordering float-left mb-3" method="get">
                            <select name="orderby" class="orderby custom-select" aria-label="سفارش خرید">
                                <option value="menu_order"  selected='selected'>مرتب‌سازی پیش‌فرض</option>
                                <option value="popularity" >مرتب‌سازی بر اساس محبوبیت</option>
                                <option value="rating" >مرتب‌سازی بر اساس امتیاز</option>
                            </select>
                            <input type="hidden" name="paged" value="1" />
                        </form>-->
                        <div class="clear">
                        </div>
                        <div id="shops-list" class="products columns-3 columns-res-2">
                            @foreach($shops as $shop)
                                <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">
                                    <figure class="thumb text-center">
                                        <a class="text-center" href="{{route('singleshop',$shop->unique_name)}}">
                                            @if($shop->logo)
                                            <img width="300" height="300" src="{{asset('images/logos/'.$shop->logo->path)}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="{{asset('images/logos/'.$shop->logo->path)}} 300w, {{asset('images/logos/'.$shop->logo->path)}} 150w, {{asset('images/logos/'.$shop->logo->path)}} 100w, {{asset('images/logos/'.$shop->logo->path)}} 600w, {{asset('images/logos/'.$shop->logo->path)}} 96w" sizes="(max-width: 300px) 100vw, 300px" />
                                        @else
                                                <img width="300" height="300" src="{{asset('img/logo.jpg')}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="{{asset('img/logo.jpg')}} 300w, {{asset('img/logo.jpg')}} 150w, {{asset('img/logo.jpg')}} 100w, {{asset('img/logo.jpg')}} 600w, {{asset('img/logo.jpg')}} 96w" sizes="(max-width: 300px) 100vw, 300px" />
                                        @endif
                                    </figure>
                                    <div class="title">
                                        <a href="{{route('singleshop',$shop->unique_name)}}"><h6 class="text-primary">فروشگاه {{$shop->name}}</h6></a>
                                    </div>
                                    <div class="product-variables text-center ">
                                            <p class="text-muted font-weight-lighter mb-0 mt-3"><small>صنف:</small> {{$shop->guild->name}}</p>
                                    </div>
                                    <div class="product-variables text-center ">
                                        @if($shop->subguild)
                                        <p class="text-muted font-weight-lighter"><small>رسته:</small> {{$shop->subguild->name}}</p>
                                            @else
                                            <p class="text-muted font-weight-lighter"><small>رسته:</small> سایر</p>
                                        @endif
                                    </div>
                                </article>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2" class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6>جستجو بر اساس شهر و استان</h6></header>
                                {!! Form::select('province',$arr,null,['placeholder'=>'انتخاب استان','class'=>'uk-select rounded','id'=>'province']) !!}
                                <select id="city" name="city " class="uk-select rounded my-2">
                                    <option value="">شهر...</option>
                                </select>
                                <button type="button" id="search-city" class="btn search-city my-2 btn-sm btn-success">جست و جو</button>
                            </section>
                            <section id="premmerce_brands_widget-2" class="widget widget_premmerce_brands_widget">
                                <header class="wg-header"><h6>جست و جو بر اساس صنف</h6></header>
                                {!! Form::select('guilds',$arr_guilds,null,['class'=>'my-2 uk-select rounded','id'=>'guilds']) !!}
                                <button type="button" id="search-guild" class="btn search-guild my-2 btn-sm btn-success">جست و جو</button>
                            </section>
                        </aside>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i class="far fa-filter"></i></button>
                    </div>
                </div>
        </main>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function (){

            var ajaxUrl = $('.front-ajax.d-none').attr('id')
            $('#province').on("change",function (){
                var province = $('#province').val()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{route('frontAjax')}}',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province_id: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                        })
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })

            $('#search-city').on("click",function (){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{route('frontAjax')}}',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-shops-by-province',
                        province: $('#province').val(),
                        city: $('#city').val()
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')

                        var data=''
                        var path
                        var subguild
                        var count_res=0
                        var x='{{route('singleshop','*')}}'
                        response.forEach(function (re){
                            var res = x.replace("*", re['unique_name']);
                           if (re['logo'])
                                path='images/logos/'+re['logo']['path']
                            else
                               path='img/logo.jpg'

                            if (re['subguild'])
                                subguild= re['subguild']['name']
                            else
                                subguild='سایر'

                            if (re['name']){
                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+res+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+res+'"><h6 class="text-primary">فروشگاه '+re['name']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter mt-3 mb-0"><small>صنف:</small> '+re['guild']['name']+'</p>'+
                                    ' <p class="text-muted font-weight-lighter mt-0"><small>رسته:</small> '+subguild+'</p>'+
                                    '</div>'+
                                    '</article>';
                            }
                              count_res++
                        });
                        list.empty()
                        list.append(data)

                        $('#res_qty').empty()
                        $('#res_qty').append(count_res)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })

            $('#search-guild').on("click",function (){
                var guild_id=$('#guilds').val()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{route('frontAjax')}}',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-shops-by-guild',
                        id: guild_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        var list=$('#shops-list')
                        var data=''
                        var path
                        var count_res=0
                        var x='{{route('singleshop','*')}}'
                        response.forEach(function (re){
                            var res = x.replace("*", re['unique_name']);
                            if (re['logo'])
                                path='images/logos/'+re['logo']['path']
                            else
                                path='img/logo.jpg'

                            if (re['subguild'])
                                subguild= re['subguild']['name']
                            else
                                subguild='سایر'

                            if (re['name']){
                                data+=' <article class="product type-product post-1744 status-publish first instock  has-post-thumbnail  shipping-taxable purchasable product-type-simple">'+
                                    ' <figure class="thumb text-center">'+
                                    '<a class="text-center" href="'+res+'">'+
                                    ' <img width="300" height="300" src="'+path+'" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="'+path+' 300w, '+path+' 150w, '+path+' 100w, '+path+' 600w, '+path+' 96w" sizes="(max-width: 300px) 100vw, 300px" />'+
                                    ' </figure>'+
                                    '<div class="title">'+
                                    '<a href="'+res+'"><h6 class="text-primary">فروشگاه '+re['name']+'</h6></a>'+
                                    '</div>'+
                                    '  <div class="product-variables text-center ">'+
                                    ' <p class="text-muted font-weight-lighter mt-3 mb-0"><small>صنف:</small> '+re['guild']['name']+'</p>'+
                                    ' <p class="text-muted font-weight-lighter mt-0"><small>رسته:</small> '+subguild+'</p>'+
                                    '</div>'+
                                    '</article>';
                            }
                            count_res++
                        });
                        list.empty()
                        list.append(data)

                        $('#res_qty').empty()
                        $('#res_qty').append(count_res)
                    },
                    error: function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })


        })
    </script>
    @endsection
