@extends('front.newTheme.master')
@section('title',$shop->name)
@section('main')
    <span class="d-none sid" id="{{$shop->id}}"></span>
    <div id="primary" class="content-area">
        <main id="main" class="site-main pt-0" role="main">
            <div class="container">
                <div class="row mb-4">
                    <div class="w-100 p-2 position-relative">
                        @if($shop->header)
                            <img class="img-fluid w-100" style="border-radius: 30px; max-height: 300px;"
                                 src="{{asset('images/headers/'.$shop->header->path)}}" alt="">
                        @else
                            <img class="img-fluid" style="border-radius:30px" src="{{asset('img/default-header.jpg')}}"
                                 alt="">
                        @endif

                        @if($shop->logo)
                            <div class="d-flex  align-items-center h-100 position-absolute" style="right: 50px;top: 0px">
                                <img class="img-fluid"
                                     style="height: 80%;border-radius: 100%"
                                     src="{{asset('images/logos/'.$shop->logo->path)}}" alt="">
                            </div>
                        @else
                            <img class="img-fluid "
                                 style="height: 80%;position:absolute;width: 35%;border-radius: 100%;right: 50px;top: 20px"
                                 src="{{asset('img/logo.jpg')}}" alt="">
                        @endif
                    </div>
                </div>

                <div class="row">
                    <header class="woocommerce-products-header w-100 py-0">
                        <section
                            class="elementor-section elementor-top-section elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                            data-id="291fd28" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row justify-content-between d-flex flex-nowrap " style="overflow-x: auto">
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu  rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" href="{{route('singleshop',$shop->unique_name)}}">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/house.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/house.svg')}} 120w, {{asset('img/services/house.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px"/>
                                                        </div>
                                                        <span style="white-space: nowrap">صفحه اصلی</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" href="{{route('services',$shop->unique_name)}}">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/customer-service.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/customer-service.svg')}} 120w, {{asset('img/services/customer-service.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px" style="min-width: 60px;min-height: 60px"/>
                                                        </div>
                                                        <span style="white-space: nowrap">خدمات</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu active rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" href="{{route('shop-gallery',$shop->unique_name)}}">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/gallery.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/gallery.svg')}} 120w, {{asset('img/services/gallery.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px" style="min-width: 60px;min-height: 60px"/>
                                                        </div>
                                                        <span style="white-space: nowrap">گالری</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" data-toggle="modal" style="color: #666"
                                                       data-target="#contactModal" data-whatever="@getbotstrap">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/communicate.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/communicate.svg')}} 120w, {{asset('img/services/communicate.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px"/>

                                                            <div class="modal fade" id="contactModal" tabindex="-1"
                                                                 role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog ">
                                                                    <div class="modal-content">
                                                                        <div class=" modal-header ">
                                                                            <h5 class="modal-title text-success"
                                                                                id="exampleModalLabel" style="white-space: nowrap">
                                                                                تماس با ما</h5>
                                                                        </div>
                                                                        <div class="modal-body ">
                                                                            <h6 class="text-muted">از طریق راه های
                                                                                زیر میتوانید با فروشگاه
                                                                                {{$shop->name}} تماس حاصل
                                                                                فرمائید.</h6>

                                                                            <img class="img-fluid  mt-3 mb-3"
                                                                                 src="{{asset('img/telephone.svg')}}"
                                                                                 style="max-width: 150px;max-height: 150px"
                                                                                 alt=""><br>
                                                                            تلفن: {{$shop->user->mobile}}<br>
                                                                            <img class="img-fluid mt-5 mb-3"
                                                                                 src="{{asset('img/map.svg')}}"
                                                                                 style="max-width: 150px;max-height: 150px"
                                                                                 alt=""><br>آدرس: {{$shop->province->name}}
                                                                            | {{$shop->city->name}} |
                                                                            {{$shop->address}}
                                                                        </div>
                                                                        <div class="modal-footer ">
                                                                            <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">بستن
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span style="white-space: nowrap">تماس با ما</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" data-toggle="modal" style="color: #666"
                                                       data-target="#aboutModal" data-whatever="@getbotstrap">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/about.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/about.svg')}} 120w, {{asset('img/services/about.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px" style="min-width: 60px;min-height: 60px"/>
                                                            {{--MODAL--}}
                                                            <div class="modal fade" id="aboutModal" tabindex="-1"
                                                                 role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog ">
                                                                    <div class="modal-content">
                                                                        <div class=" modal-header ">
                                                                            <h5 class="modal-title text-success"
                                                                                id="exampleModalLabel">
                                                                                درباره فروشگاه {{$shop->name}}</h5>
                                                                        </div>
                                                                        <div class="modal-body ">

                                                                            <img class="img-fluid  mt-3 mb-3"
                                                                                 src="{{asset('img/services/about.svg')}}"
                                                                                 style="max-width: 150px;max-height: 150px"
                                                                                 alt="">
                                                                            <br> صنف:
                                                                            {{$shop->guild->name}}<br>
                                                                            @if($shop->subguild)
                                                                                <br> رسته:
                                                                                {{$shop->subguild->name}}<br>
                                                                            @endif
                                                                            <br> امتیاز:
                                                                            {{$shop->rate}}<br>
                                                                            <br> توضیحات:
                                                                            {{$shop->description}}<br>

                                                                        </div>
                                                                        <div class="modal-footer ">
                                                                            <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">بستن
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span style="white-space: nowrap">درباره</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-14 mx-1 mx-lg-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47"
                                        data-id="63eee47" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div
                                                    class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image"
                                                    data-id="c7b76c2" data-element_type="widget"
                                                    data-widget_type="image.default">
                                                    <a class="" href="{{route('shop-blog',$shop->unique_name)}}">
                                                        <div class="elementor-widget-container ">
                                                            <img width="60" height="60"
                                                                 src="{{asset('img/services/blog.svg')}}"
                                                                 class="mb-3 img-fluid attachment-large size-large"
                                                                 alt="" loading="lazy"
                                                                 srcset="{{asset('img/services/blog.svg')}} 120w, {{asset('img/services/blog.svg')}} 32w"
                                                                 sizes="(max-width: 120px) 100vw, 120px" style="min-width: 60px;min-height: 60px"/>
                                                        </div>
                                                        <span>بلاگ</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </header>
                    <div class="col-lg-3 w-100">
                        <section id="woocommerce_product_categories-2"
                                 class=" widget woocommerce widget_product_categories">
                            <header class="wg-header"><h6 class="text-dark">اطلاعات فروشگاه</h6></header>
                            <ul class="list-unstyled">
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-store"></i> نام فروشگاه: </span> <span
                                            class="text-primary">{{$shop->name}}</span></h6>
                                </li>
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-toolbox"></i>  صنف: </span> <span
                                            class="text-primary">{{$shop->guild->name}}</span></h6>
                                </li>
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-star"></i>  امتیاز: </span> <span
                                            class="text-primary">{{$shop->rate}}</span></h6>
                                </li>
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-map-marker-alt"></i>  استان: </span> <span
                                            class="text-primary">{{$shop->province->name}}</span></h6>
                                </li>
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-map-marker-alt"></i>  شهر: </span> <span
                                            class="text-primary">{{$shop->city->name}}</span></h6>
                                </li>
                                <li>
                                    <h6 class="font-weight-light"><span class="text-muted"> <i
                                                class="fas fa-info"></i>  درباره: </span> <span
                                            class="text-primary">{{Str::limit($shop->description,'50','...')}}</span>
                                    </h6>
                                </li>
                            </ul>
                        </section>
                        <section id="premmerce_brands_widget-2" class="widget widget_premmerce_brands_widget">
                            <header class="wg-header"><h6>دسته بندی محصولات</h6></header>
                            <ul class="list-unstyled">
                                @foreach($categories as $cat)
                                    @if($cat->shop_id===$shop->id)
                                        <li class="w-100 my-2">
                                        <!--                                            <button class="cat-title btn" id="{{$cat->id}}" type="button">
                                                {{$cat->title}}
                                            </button>-->
                                            <a class=""
                                               href="{{route('cat.products',[$shop->unique_name,$cat->id,$cat->title])}}">
                                                @if($cat->photo)
                                                    <img class="img-fluid mx-2"
                                                         style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%"
                                                         src="{{asset('images/cats/'.$cat->photo->path)}}" alt="">
                                                @else
                                                    <img class="img-fluid mx-2"
                                                         style="min-width :50px;min-height:50px;max-height: 50px;max-width: 50px;border-radius:100%"
                                                         src="{{asset('img/default.jpg')}}" alt="">
                                                @endif
                                                {{$cat->title}}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </section>
                    </div>
                    <div class="col-12 col-lg-9 w-100">
                        <section class="elementor-section elementor-top-section bg-white rounded elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="291fd28" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row justify-content-center ">
                                    @if( $video != null )
                                        <div class="row p-2 w-100 justify-content-center" id="">
                                            <div class="col-12 my-2 justify-content-center text-center">
                                                @if($video && $video->path)
                                                    <video src="{{asset('files/gallery/'.$video->path)}}" controls height="500" class="rounded w-100">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="alert alert-danger text-center py-5"><h5>تیزری جهت نمایش وجود ندارد</h5></div>
                                    @endif
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-top-section bg-white rounded elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="291fd28" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row justify-content-center">
                                    @if(count($photos)>0  )
                                        <div class="row p-2 w-100 " id="">
                                            @foreach($photos as $photo)
                                                @if(($shop->logo_id != $photo->id) && ($shop->header && $shop->header_id != $photo->id) )
                                                    <div class="col-12 col-md-4 my-2 ">
                                                        <div class=" rounded">
                                                            <div uk-lightbox>
                                                                <a class="" href="{{asset('images/gallery/'.$photo->path)}}" data-alt="Image">
                                                                    <img src="{{asset('images/gallery/'.$photo->path)}}" style="max-height: 200px;max-width: 200px;min-height: 200px;min-width: 200px" class="card-img-top  rounded" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="alert alert-danger text-center py-5"><h5>تصویری جهت نمایش وجود ندارد</h5></div>
                                    @endif
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
@section('script')
    <script>

    </script>
@endsection


