@extends('front.newTheme.master')
@section('title',$shop->name)
@section('main')
    <span class="d-none sid" id="{{$shop->id}}"></span>
    <div id="primary" class="content-area">
        <main id="main" class="site-main pt-0" role="main">
            <div class="container">
                <div class="row mb-4">
                    <div class="w-100 p-2">
                        @if($shop->header)
                            <img class="img-fluid" style="border-radius: 30px" src="{{asset('images/headers/'.$shop->header->path)}}" alt="">
                        @else
                            <img class="img-fluid" style="border-radius:30px" src="{{asset('img/default-header.jpg')}}" alt="">
                        @endif

                        @if($shop->logo)
                            <img class="img-fluid  " style="height: 200px;position:absolute;bottom:50px;top:50px;width: 200px;border-radius: 100%;right: 50px" src="{{asset('images/logos/'.$shop->logo->path)}}" alt="">
                        @else
                            <img class="img-fluid " style="height: 200px;position:absolute;bottom:50px;top:50px;width: 200px;border-radius: 100%;right: 50px" src="{{asset('img/logo.jpg')}}" alt="">
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg order-lg-2">
                        <header class="woocommerce-products-header py-0">
                            <section class="elementor-section elementor-top-section elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="291fd28" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row justify-content-between">
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu  rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" href="{{route('singleshop',$shop->unique_name)}}">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/house.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/house.svg')}} 120w, {{asset('img/services/house.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                            </div>
                                                            <span>صفحه اصلی</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" href="{{route('services',$shop->unique_name)}}">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/customer-service.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/customer-service.svg')}} 120w, {{asset('img/services/customer-service.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                            </div>
                                                            <span>خدمات</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" href="{{route('shop-gallery',$shop->unique_name)}}">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/gallery.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/gallery.svg')}} 120w, {{asset('img/services/gallery.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                            </div>
                                                            <span>گالری</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" data-toggle="modal" style="color: #666" data-target="#contactModal" data-whatever="@getbotstrap">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/communicate.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/communicate.svg')}} 120w, {{asset('img/services/communicate.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                                {{--MODAL--}}
                                                                <div class="modal fade" id="contactModal" tabindex="-1"
                                                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog ">
                                                                        <div class="modal-content">
                                                                            <div class=" modal-header ">
                                                                                <h5 class="modal-title text-success" id="exampleModalLabel">
                                                                                    تماس با ما</h5>
                                                                            </div>
                                                                            <div class="modal-body ">
                                                                                <h6 class="text-muted">از طریق راه های زیر میتوانید با فروشگاه
                                                                                    {{$shop->name}} تماس حاصل فرمائید.</h6>

                                                                                <img class="img-fluid  mt-3 mb-3" src="{{asset('img/telephone.svg')}}" style="max-width: 150px;max-height: 150px" alt=""><br> تلفن: {{$shop->user->mobile}}<br>
                                                                                <img class="img-fluid mt-5 mb-3" src="{{asset('img/map.svg')}}" style="max-width: 150px;max-height: 150px" alt=""><br>آدرس: {{$shop->province->name}} | {{$shop->city->name}} |
                                                                                {{$shop->address}}
                                                                            </div>
                                                                            <div class="modal-footer ">
                                                                                <button type="button" class="btn btn-secondary"
                                                                                        data-dismiss="modal">بستن
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span>تماس با ما</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" data-toggle="modal" style="color: #666" data-target="#aboutModal" data-whatever="@getbotstrap">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/about.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/about.svg')}} 120w, {{asset('img/services/about.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                                {{--MODAL--}}
                                                                <div class="modal fade" id="aboutModal" tabindex="-1"
                                                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog ">
                                                                        <div class="modal-content">
                                                                            <div class=" modal-header ">
                                                                                <h5 class="modal-title text-success" id="exampleModalLabel">
                                                                                    درباره فروشگاه {{$shop->name}}</h5>
                                                                            </div>
                                                                            <div class="modal-body ">

                                                                                <img class="img-fluid  mt-3 mb-3" src="{{asset('img/services/about.svg')}}" style="max-width: 150px;max-height: 150px" alt="">
                                                                                <br> صنف:
                                                                                {{$shop->guild->name}}<br>
                                                                                @if($shop->subguild)
                                                                                    <br> رسته:
                                                                                    {{$shop->subguild->name}}<br>
                                                                                @endif
                                                                                <br> امتیاز:
                                                                                {{$shop->rate}}<br>
                                                                                <br> توضیحات:
                                                                                {{$shop->description}}<br>

                                                                            </div>
                                                                            <div class="modal-footer ">
                                                                                <button type="button" class="btn btn-secondary"
                                                                                        data-dismiss="modal">بستن
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span>درباره</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-column elementor-col-14 mx-3 shop-menu active rounded elementor-top-column elementor-element elementor-element-63eee47" data-id="63eee47" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div class="elementor-element elementor-element-c7b76c2 elementor-widget elementor-widget-image" data-id="c7b76c2" data-element_type="widget" data-widget_type="image.default">
                                                        <a class="" href="{{route('shop-blog',$shop->unique_name)}}">
                                                            <div class="elementor-widget-container ">
                                                                <img width="60" height="60" src="{{asset('img/services/blog.svg')}}" class="mb-3 img-fluid attachment-large size-large" alt="" loading="lazy" srcset="{{asset('img/services/blog.svg')}} 120w, {{asset('img/services/blog.svg')}} 32w" sizes="(max-width: 120px) 100vw, 120px" />
                                                            </div>
                                                            <span>بلاگ</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </header>

                        <section class="elementor-section elementor-top-section bg-white rounded elementor-element elementor-element-291fd28 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="291fd28" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row justify-content-center ">

                                </div>
                            </div>
                        </section>

                        <div class="row">
                            <div class="col-lg ">
                                <section class="blog-home">
                                    <article class="post-item post-63 post type-post status-publish format-standard has-post-thumbnail hentry category-16 tag-fortnite" id="post-63">
                                        <figure class="post-thumb">
                                            <a href="{{route('article',$article->id)}}">
                                                @if($article->photo)
                                                    <img width="822" height="522" src="{{asset('images/articles/'.$article->photo->path)}}" class="attachment-large size-large wp-post-image" alt="" loading="lazy" srcset="{{asset('images/articles/'.$article->photo->path)}} 822w, {{asset('images/articles/'.$article->photo->path)}} 300w, {{asset('images/articles/'.$article->photo->path)}} 768w, {{asset('images/articles/'.$article->photo->path)}} 472w, {{asset('images/articles/'.$article->photo->path)}} 400w, {{asset('images/articles/'.$article->photo->path)}} 50w, {{asset('images/articles/'.$article->photo->path)}} 600w" sizes="(max-width: 822px) 100vw, 822px" />
                                                @else
                                                    <img width="822" height="522" src="{{asset('img/default.jpg')}}" class="attachment-large size-large wp-post-image" alt="" loading="lazy" srcset="{{asset('img/default.jpg')}} 822w, {{asset('img/default.jpg')}} 300w, {{asset('img/default.jpg')}} 768w, {{asset('img/default.jpg')}} 472w, {{asset('img/default.jpg')}} 400w, {{asset('img/default.jpg')}} 50w, {{asset('img/default.jpg')}} 600w" sizes="(max-width: 822px) 100vw, 822px" />
                                                @endif
                                            </a>
                                        </figure>
                                        <div class="title">
                                            <a href="{{route('article',$article->id)}}">
                                                <h1 class="title-tag">{{$article->title}}</h1>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <p>
                                                <?php echo $article->content ?>
                                            </p>
                                        </div>
                                        <div class="tags">برچسب‌ها:
                                            <a href="#" rel="tag">{{$article->keywords}}</a>
                                        </div>
                                        <div class="info">
                                            <ul>
                                                <li><i class="fal fa-user"></i><span>{{$article->author->name}} {{$article->author->family}}</span></li>
                                                <li><i class="fal fa-calendar"></i><span>{{verta($article->created_at)->format('%B %d، %Y')}}</span></li>
                                                <li><i class="fal fa-archive"></i>
                                                    <span>
                                            <ul class="post-categories">
	                                <li>
                                        <a href="#" rel="category tag">{{$article->category->title}}</a></li>
                                            </ul>
                                        </span>
                                                </li>
                                                <li><i class="fal fa-eye"></i><span>1671 بازدید</span></li>
                                            </ul>
                                        </div>
                                    </article>
                                    <div class="post-comments post-wg">
                                        <div id="comments" class="comments-area">
                                            @if(count($article_comments)>0)
                                                <h2 class="comments-title">
                                                    <i class="fal fa-comment"></i>
                                                    {{count($article_comments)}} دیدگاه برای &ldquo;{{$article->title}}&rdquo;
                                                </h2>

                                                <ol class="comment-list">
                                                    @foreach($article_comments as $comment)
                                                        <li id="comment-2" class="comment even thread-even depth-1">
                                                            <article id="div-comment-2" class="comment-body">
                                                                <footer class="comment-meta">
                                                                    <div class="comment-author vcard">
                                                                        @if($comment->user->photo)
                                                                            <img alt='' src='{{asset('images/'.$comment->user->photo->path)}}' srcset='{{asset('images/'.$comment->user->photo->path)}}' class='avatar avatar-100 photo' height='100' width='100' loading='lazy'/>
                                                                        @else
                                                                            <img alt='' src='{{asset('img/user-avatar.svg')}}' srcset='{{asset('img/user-avatar.svg')}}' class='avatar avatar-100 photo' height='100' width='100' loading='lazy'/>
                                                                        @endif
                                                                        <b class="fn">{{$comment->user->name}} {{$comment->user->family}}</b> <span class="says">گفت:</span>
                                                                    </div>
                                                                    <!-- .comment-author -->

                                                                    <div class="comment-metadata">
                                                                        <a href=""><time datetime="{{$comment->created_at}}">{{verta($comment->created_at)->format('%B %d، %Y')}} در
                                                                                {{verta($comment->created_at)->formattime()}}</time></a>
                                                                    </div><!-- .comment-metadata -->

                                                                </footer><!-- .comment-meta -->

                                                                <div class="comment-content">
                                                                    <p>{{$comment->content}}</p>
                                                                </div><!-- .comment-content -->

                                                                <div class="reply">
                                                                    <a rel='nofollow' class='comment-reply-link' href='#comment-2' data-commentid="2" data-postid="63" data-belowelement="div-comment-2" data-respondelement="respond" data-replyto="پاسخ به {{$comment->user->name}}" aria-label='پاسخ به {{$comment->user->name}}'>پاسخ دادن</a>
                                                                </div>
                                                            </article><!-- .comment-body -->
                                                        </li>
                                                @endforeach

                                                <!-- #comment-## -->
                                                </ol>
                                            @endif
                                                @livewire('article-comment',['post_id'=>$article->id])

                                        <!-- #comments -->
                                    </div>
                                </section>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-3">
                        <aside id="secondary" class="sidebar shop-archive-sidebar">
                            <a href="#close" class="btn section-close" rel="nofollow">
                                <i class="far fa-times"></i>
                                بستن فیلتر ها
                            </a>
                            <section id="woocommerce_product_categories-2" class=" widget woocommerce widget_product_categories">
                                <header class="wg-header"><h6>اطلاعات فروشگاه</h6></header>
                                <ul class="list-unstyled">
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-store"></i> نام فروشگاه: </span>  <span class="text-primary">{{$shop->name}}</span></h6>
                                    </li>
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-toolbox"></i>  صنف: </span>  <span class="text-primary">{{$shop->guild->name}}</span></h6>
                                    </li>
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-star"></i>  امتیاز: </span>  <span class="text-primary">{{$shop->rate}}</span></h6>
                                    </li>
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-map-marker-alt"></i>  استان: </span>  <span class="text-primary">{{$shop->province->name}}</span></h6>
                                    </li>
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-map-marker-alt"></i>  شهر: </span>  <span class="text-primary">{{$shop->city->name}}</span></h6>
                                    </li>
                                    <li>
                                        <h6 class="font-weight-light"><span class="text-muted"> <i class="fas fa-info"></i>  درباره: </span>  <span class="text-primary">{{Str::limit($shop->description,'50','...')}}</span></h6>
                                    </li>
                                </ul>
                            </section>
                            <section id="premmerce_brands_widget-2" class="widget widget_premmerce_brands_widget">
                                <header class="wg-header"><h6>آخرین دیدگاه ها</h6></header>
                                @if(count($comments)>0)
                                <ul id="recentcomments">
                                    @foreach($comments as $comment)
                                        <li class="recentcomments">
                                            <span class="comment-author-link">{{$comment->user->name}} {{$comment->user->family}}</span> در
                                            <a href="{{route('article',$comment->groupable->id)}}">{{Str::limit($comment->groupable->title,'20','...')}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                @else
                                    <div class="alert">دیدگاهی وجود ندارد</div>
                                @endif
                            </section>


                        </aside>
                        <!-- #secondary -->
                        <button class="btn shop-filters-show" type="button" title="فیلتر ها"><i class="far fa-filter"></i></button>
                    </div>
                </div>
        </main>
    </div>
@endsection
@section('script')
    <script>

    </script>
@endsection


