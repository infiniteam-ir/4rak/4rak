<!DOCTYPE html>
<html lang="en" id="html" class="bg-light">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>سبد خرید</title>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    <!--Css and Bootstrap-->
    <link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('asset/css/w3s.css')}}">
    <!-- <link href="asset/css/uikit.min.css" rel="stylesheet">-->
    <link href="{{asset('asset/css/uikit-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/css/all.min.css')}}" rel="stylesheet">
    <!--Private for This page-->
    <link href="{{asset('asset/css/mall.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{asset('css/font-style.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontiran.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
</head>
<body id="main" class="bg-light">
<!--bottom badge fixed-->

<!--End bottom badge fixed-->
<!--Layer for close side bar-->
<div class="d-none" id="layer" onclick="closeNav()"></div>
<!--End Layer for close side bar-->
<!-- Start first Header -->
<header class="bg-white shadow rounded d-none d-lg-block" id="firstHeader">
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu1">
                <ul class="navbar-nav ml-auto col-md-4">
                    <li class="nav-item ml-4">
                        <a href="" class="nav-link disabled text-muted">
                            <i class="fas fa-headset text-warning ml-1"></i>
                            <span>09137179363</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="mailto:info@4rak.ir">
                            <i class="fas fa-at text-warning ml-1"></i>
                            <span>4rak.ir</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav mt-0 col-md-8 justify-content-end">
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-shipping-fast text-warning ml-1"></i>
                            <span>پیگیری سفارشات</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-question text-warning ml-1"></i>
                            <span>سوالات متدوال</span>
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link text-muted" href="#">
                            <i class="fas fa-phone text-warning ml-1"></i>
                            <span>تماس با ما</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu2">
                <ul class="navbar-nav ml-5 col-md-2">
                    <li class="nav-item">
                        <a class="navbar-brand mr-0" href="{{route('about')}}">
                            <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 150px"
                                 class="bg-warning rounded" alt="">
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto col-md-4">
                    <div class="input-icons pl-0">
                        <i class="fa fa-search icon"></i>
                        <i class="fab fa-searchengin icon2 fa-lg"></i>
                        <input class="form-control rounded-right" id="searchBox" type="text"
                               placeholder="جست و جو در چارک..." style="font-size: medium">
                    </div>
                </ul>
                <ul class="navbar-nav ml-auto col-md-3 mr-5">
                    <li class="nav-item">
                        <form action="{{route('login')}}" method="get">
                            <button class="btn btnColor text-white p-2" style="font-size: small"><i
                                        class="fa fa-user-alt ml-2"></i>
                                ورود / عضویت
                            </button>
                        </form>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto col-md-3">
                    <a class="pop" href="#" data-container="body" data-toggle="popover" id="navLink"
                       data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge"><i class="fa fa-shopping-bag fa-2x bad"></i><span
                                    class="text-warning mr-1 pb-5">
                                {{Session::has('cart')?Session::get('cart')->totalQty:"0"}}
                            </span></span>
                    </a>
                    <a class="pop mr-5" href="#" data-container="body" data-toggle="popover" id="navLink"
                       data-placement="bottom" data-content="مورد علاقه ها">
                        <span class="badge"><i class="fa fa-heart fa-2x bad"></i><span
                                    class="text-warning">9</span></span>

                    </a>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block">
        <div class="container">
            <div class="collapse navbar-collapse" id="main_menu3">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">فروشگاه
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#slide">خانه
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#about">درباره</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#main_features">ویژگی ها</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#screenshots">تصاویر</a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#team">تیم</a>
                    </li>
                    <li class="nav-item dropdown mx-3">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            اخبار
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                            <a class="dropdown-item anchor" href="#">اخبار جدید</a>
                            <a class="dropdown-item anchor" href="">اخبار یک</a>
                            <a class="dropdown-item anchor" href="#">اخبار دو</a>
                            <a class="dropdown-item anchor" href="#">اخبار سه</a>
                            <a class="dropdown-item anchor" href="#">جزئیات خبر</a>
                        </div>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="#git_in_touch">تماس</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End First Header -->
<!--Start Responsive Header-->
<header class="bg-white shadow rounded d-lg-none" id="responsiveHeader">
    <nav class="navbar navbar-expand navbar-light  d-lg-none">
        <div class="container-fluid">
            <div class="collapse navbar-collapse row" id="response_menu">
                <div class="navbar-nav col-11 col-sm-3 d-flex justify-content-center pr-0">
                    <a class="navbar-brand" href="#">
                        <img src="{{asset('assets/img/fox-logo.png')}}" style="max-width: 50px" class="" alt="">
                    </a>
                </div>
                <div class="navbar-nav mr-0 col-12 col-sm-6 mt-2 mt-sm-0 row">
                    <a class="pop col-4 d-flex justify-content-center text-muted" href="#" data-container="body"
                       data-toggle="popover" data-trigger="hover"
                       data-placement="bottom" data-content="مشاهده سبد خرید و...">
                        <span class="badge"><i class="fa fa-shopping-basket fa-2x"></i><span
                                    class="text-warning mr-1 pb-5">9</span></span>
                    </a>
                    <a class="pop col-4 d-flex justify-content-center text-muted" href="#" data-container="body"
                       data-toggle="popover"
                       data-placement="bottom" data-content="مورد علاقه ها">
                        <span class="badge"><i class="fa fa-heart fa-2x"></i><span class="text-warning">9</span></span>
                    </a>
                    <a class="col-4 d-flex justify-content-center text-muted" href="#">
                        <i class="fa fa-search fa-2x bad"></i>

                    </a>
                </div>
                <div class="navbar-nav col-12 uk-position-top-left position-fixed mt-3 ml-4 col-sm-3 justify-content-end ml-1">
                    <div id="mySidenav" class="sidenav">

                        <a class="" href="{{route('store')}}">فروشگاه</a>
                        <a class="" href="#slide">خانه</a>
                        <a class="" href="#about">درباره</a>
                        <a class="" href="#main_features">ویژگی ها</a>
                        <a class="" href="#screenshots">تصاویر</a>
                        <a class="" href="#team">تیم</a>

                    </div>
                    <!-- Use any element to open the sidenav -->
                    <i class="fa fa-bars fa-2x text-danger" onclick="openNav()"></i>
                    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
                </div>
            </div>
        </div>
    </nav>
</header>
<!--End Responsive Header-->
<!--Main Content-->
<div class="container">
        <div class="container">
            <div class="row  d-flex justify-content-center ">
                <div class="col-10  text-right p-3 buy-box-shadow">
                    @if(Session::has('Error'))
                        <div class="alert alert-danger">
                            {{Session('Error')}}
                        </div>
                    @endif
                </div>
                <div class="col-10 bg-white text-right p-3 mt-5 text-center buy-box-shadow border mb-5">

                </div>
            </div>
        </div>

</div>
<!-- Start  Footer -->
<div class="container-fluid">
    <div class="row">
        <footer class="padding-100 pb-0 container-fluid w-100">
            <div class="space-50"></div>
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <img src="assets/img/fox-logo.png" class="img-fluid" alt="">
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                                    برای شرایط فعلی تکنولوژی
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>دسترسی سریع</h6>
                                <ul>
                                    <li>
                                        <a href="#">خانه</a>
                                    </li>
                                    <li>
                                        <a href="#">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="#">خدمات</a>
                                    </li>
                                    <li>
                                        <a href="#">محصولات</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>شبکه اجتماعی</h6>
                                <ul>
                                    <li>
                                        <a href="https://t.me/ir4rak">کانال تلگرام</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/4rak.ir">اینستاگرام</a>
                                    </li>
                                    <li>
                                        <a href="#">لینکدین</a>
                                    </li>
                                    <li>
                                        <a href="#">توییتر</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="widget">
                                <h6>تماس سریع</h6>
                                <ul>
                                    <li>
                                        <span>تلفن : </span> <span class="ltr-text">0913 233 89 38</span>
                                    </li>
                                    <li>
                                        <span>ایمیل : </span>
                                        <a href="#">info@4rak.ir</a>
                                    </li>
                                    <li>
                                        <span>آدرس : </span>اصفهان، نجف آباد، دانشگاه آزاد نجف آباد مرکز رشد، اینفینیتیم
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-50"></div>
            <div class="copyright">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <p>طراحی و اجرا توسط <a href="https://haranet.ir" target="_blank">هرانت</a></p>
                        </div>
                        <div class="offset-md-2 col-md-5">
                            <ul class="nav justify-content-center justify-content-md-end">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">قوانین و مقررات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">سیاست حریم خصوصی</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- End  Footer  -->
<!--End Main Content-->

</body>
<!-- jQuery -->
<script src="{{asset('asset/js/jQuery.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{asset('asset/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<!-- uikit JavaScript -->
<script src="{{asset('asset/js/uikit.min.js')}}"></script>
<script src="{{asset('asset/js/uikit-icons.min.js')}}"></script>
<!--js For this page-->
<script src="{{asset('asset/js/mall.js')}}"></script>



</html>
