@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">پلن های پرداختی </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        {{Session::pull('error')}}
                    </h4>
                </div>
            @endif

            <div class=" mt-0 bg-white">
                <div class="col justify-content-center p-0 ">
                    <a href="{{route('new_pay_plan')}}" class="btn btn-outline-success rounded">
                        <i class="fa fa-plus"></i>
                        افزودن
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(count($plans)>0)
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2 "
                           id="table">
                        <tr class="table-header">
                            @php($counter=1)
                            <th>#</th>
                            <th>عنوان</th>
                            <th>هزینه ماهانه (تومان)</th>
                            <th>هزینه سه ماه (تومان)</th>
                            <th>هزینه شش ماه (تومان)</th>
                            <th>هزینه سالانه (تومان)</th>
                            <th>درصد کارمزد</th>
                            <th>امکانات</th>
                        </tr>
                        @foreach($plans as $plan)
                            <tr>
                                <td>{{$counter}}</td>
                                <td>{{$plan->name}}</td>
                                <td>{{number_format($plan->monthlyPrice)}}</td>
                                <td>{{number_format($plan->threeMonthPrice)}}</td>
                                <td>{{number_format($plan->sixMonthPrice)}}</td>
                                <td>{{number_format($plan->yearlyPrice)}}</td>
                                <td>{{$plan->wagePercent}}</td>
                                <td>
                                    <div class="row w-100 justify-content-center">
                                        <div class="col-12 col-md-6 ">
                                            <a class=" btn btn-primary" href="{{route('edit_pay_plan',$plan->id)}}">
                                                <i class="fa fa-edit"></i>
                                                ویرایش
                                            </a>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <button id="{{$plan->id}}" class="delete btn btn-danger ">
                                                <i class="fa fa-trash"></i>
                                                حذف
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php($counter++)
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-light">
                        پلن پرداختی تعریف نکرده اید
                    </div>
                @endif
                {{--                <div class="row d-flex justify-content-center">{{$plans->links()}}</div>--}}
            </div>

        </div>
    </div>

@endsection

@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete', function () {
                var id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این پلن رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-pay-plan',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                var table = document.getElementById('table')
                                table.deleteRow(rowindex)
                                swal("موفق", "پلن حذف شد.", "success")
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "پلن با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection
