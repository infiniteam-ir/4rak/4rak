@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">انتخاب پست های ویژه برای بلاگ </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                <div class="row">
                    @if(count($articles)>0)
                    <div class="col-12 col-md-5">
                         <figure class="figure border rounded p-1">
                             @if($top_right_article)
                                @if($top_right_article->article->photo)
                                    <img class="img-fluid" src="{{asset('images/articles/'.$top_right_article->article->photo->path)}}" data-src="{{asset('images/articles/'.$top_right_article->article->photo->path)}}" alt="{{$top_right_article->article->title}}" >
                                @else
                                    <img class="img-fluid" src="{{asset('img/default.jpg')}}" data-src="{{asset('img/default.jpg')}}" alt="{{$top_right_article->article->title}}" >
                                @endif
                                <figcaption class="figure-caption">
                                    <a href="{{route('article',$top_right_article->article->id)}}">
                                        <h4>{{$top_right_article->article->title}}</h4>
                                    </a>
                                    <form method="post" action="{{route('set-blog-top-article')}}">
                                        @csrf
                                    <select name="new_article" id="right" class="form-control">
                                        @foreach($articles as $article)
                                            <option value="{{$article->id}}">{{$article->title}}</option>
                                        @endforeach
                                        <option value="">انتخاب پست جدید برای این محل</option>
                                    </select>
                                        <input type="text" name="position" value="right" class="d-none">
                                    <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                    </form>
                                </figcaption>
                             @else
                                 <h6>انتخاب پست برای موقعیت سمت راست</h6>
                                 <form method="post" action="{{route('set-blog-top-article')}}">
                                     @csrf
                                     <select name="new_article" id="right" class="form-control">
                                         @foreach($articles as $article)
                                             <option value="{{$article->id}}">{{$article->title}}</option>
                                         @endforeach
                                         <option value="">انتخاب پست جدید برای این محل</option>
                                     </select>
                                     <input type="text" name="position" value="right" class="d-none">
                                     <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                 </form>
                             @endif
                            </figure>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="row w-100 d-inline-block">
                            @if(count($top_mid_articles)>0)
                            @foreach($top_mid_articles as $mid_article)
                                <div class="col-12 ">
                                    <figure class="figure border rounded p-1">
                                        @if($mid_article->article->photo)
                                            <img class="img-fluid" src="{{asset('images/articles/'.$mid_article->article->photo->path)}}" data-src="{{asset('images/articles/'.$mid_article->article->photo->path)}}" alt="{{$mid_article->article->title}}" >
                                        @else
                                            <img class="img-fluid" src="{{asset('img/default.jpg')}}" data-src="{{asset('img/default.jpg')}}" alt="{{$mid_article->article->title}}" >
                                        @endif
                                        <figcaption class="figure-caption">
                                            <a href="{{route('article',$mid_article->article->id)}}">
                                                <h4>{{$mid_article->article->title}}</h4>
                                            </a>
                                            <form method="post" action="{{route('set-blog-top-article')}}">
                                                @csrf
                                                <select name="new_article" id="mid" class="form-control">
                                                    @foreach($articles as $article)
                                                        <option value="{{$article->id}}">{{$article->title}}</option>
                                                    @endforeach
                                                    <option value="">انتخاب پست جدید برای این محل</option>
                                                </select>
                                                <input type="text" name="position" value="mid" class="d-none">
                                                <input type="text" name="old" value="{{$mid_article->id}}" class="d-none">
                                                <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                            </form>
                                        </figcaption>
                                    </figure>
                                </div>
                            @endforeach
                                @if(count($top_mid_articles)<2)
                                    <form method="post" action="{{route('set-blog-top-article')}}">
                                        @csrf
                                        <select name="new_article" id="mid" class="form-control">
                                            @foreach($articles as $article)
                                                <option value="{{$article->id}}">{{$article->title}}</option>
                                            @endforeach
                                            <option value="">انتخاب پست جدید برای این محل</option>
                                        </select>
                                        <input type="text" name="position" value="mid" class="d-none">
                                        <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                    </form>
                                    @endif
                            @else
                                        <h6>انتخاب پست برای موقعیت وسط</h6>
                                    <form method="post" action="{{route('set-blog-top-article')}}">
                                        @csrf
                                        <select name="new_article" id="mid" class="form-control">
                                            @foreach($articles as $article)
                                                <option value="{{$article->id}}">{{$article->title}}</option>
                                            @endforeach
                                            <option value="">انتخاب پست جدید برای این محل</option>
                                        </select>
                                        <input type="text" name="position" value="mid" class="d-none">
                                        <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                    </form>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-md-3">
                        <div class="row w-100 d-inline-block">
                            @if(count($top_left_articles)>0)
                            @foreach($top_left_articles as $left_article)
                                <div class="col-12 ">
                                    <figure class="figure border rounded p-1">
                                        @if($left_article->article->photo)
                                            <img class="img-fluid" src="{{asset('images/articles/'.$left_article->article->photo->path)}}" data-src="{{asset('images/articles/'.$left_article->article->photo->path)}}" alt="{{$left_article->article->title}}" >
                                        @else
                                            <img class="img-fluid" src="{{asset('img/default.jpg')}}" data-src="{{asset('img/default.jpg')}}" alt="{{$left_article->article->title}}" >
                                        @endif
                                        <figcaption class="figure-caption">
                                            <a href="{{route('article',$left_article->article->id)}}">
                                                <h4>{{$left_article->article->title}}</h4>
                                            </a>
                                            <form method="post" action="{{route('set-blog-top-article')}}">
                                                @csrf
                                                <select name="new_article" id="mid" class="form-control">
                                                    @foreach($articles as $article)
                                                        <option value="{{$article->id}}">{{$article->title}}</option>
                                                    @endforeach
                                                    <option value="">انتخاب پست جدید برای این محل</option>
                                                </select>
                                                <input type="text" name="position" value="left" class="d-none">
                                                <input type="text" name="old" value="{{$left_article->id}}" class="d-none">
                                                <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                            </form>
                                        </figcaption>
                                    </figure>
                                </div>
                            @endforeach
                                @if(count($top_left_articles)<3)
                                    <form method="post" action="{{route('set-blog-top-article')}}">
                                        @csrf
                                        <select name="new_article" id="mid" class="form-control">
                                            @foreach($articles as $article)
                                                <option value="{{$article->id}}">{{$article->title}}</option>
                                            @endforeach
                                            <option value="">انتخاب پست جدید برای این محل</option>
                                        </select>
                                        <input type="text" name="position" value="left" class="d-none">
                                        <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                    </form>
                                @endif
                            @else
                                <h6>انتخاب پست برای موقعیت سمت چپ</h6>
                                <form method="post" action="{{route('set-blog-top-article')}}">
                                    @csrf
                                    <select name="new_article" id="mid" class="form-control">
                                        @foreach($articles as $article)
                                            <option value="{{$article->id}}">{{$article->title}}</option>
                                        @endforeach
                                        <option value="">انتخاب پست جدید برای این محل</option>
                                    </select>
                                    <input type="text" name="position" value="left" class="d-none">
                                    <button type="submit" id="right-btn" class="btn my-2 btn-sm btn-success">ذخیره</button>
                                </form>
                            @endif
                        </div>
                    </div>
                    @else
                        <div class="alert alert-warning text-center w-100 mx-3">
                            پستی جهت انتخاب وجود ندارد
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection


