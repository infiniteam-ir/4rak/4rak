@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light mb-2  text-center ">
            <h1 class="text-muted">افزودن صنف جدید</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                    <div class="col-12 mt-3 col-md-10 text-center">
                        <div class="box-body">
                            @livewire('guilds.add-guild',['guilds'=>$guilds])
                        </div>
                    </div>
                </div>
        </div>
    </div>

@endsection


