@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css"/>

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">روش های ارسال بسته های پستی </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        {{Session::pull('error')}}
                    </h4>
                </div>
            @endif

            <div class=" mt-0 bg-white">
                <div class="col justify-content-center p-0 ">
                    <a href="{{route('new_delivery_method')}}" class="btn btn-outline-success rounded">
                        <i class="fa fa-plus"></i>
                        افزودن
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(count($methods)>0)
                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2 "
                       id="table">
                    <tr class="table-header">
                        <th>تصویر</th>
                        <th>روش ارسال</th>
                        <th>هزینه (تومان) </th>
                        <th>توضیحات </th>
                        <th>امکانات</th>
                    </tr>
                    @foreach($methods as $method )
                        <tr>
                            <td>
                                @if($method->photo)
                                    <img class="img-product-list" src="{{asset('images/delivery/'.$method->photo->path)}}">
                                @else
                                    <img class="img-product-list" src="{{asset('img/default-2.svg')}}">
                                @endif
                            </td>
                            <td>{{$method->method}}</td>
                            <td>{{number_format($method->price)}}</td>
                            <td>{{$method->description}}</td>
                            <td>
                                <div class="row w-100 justify-content-center">
                                    <div class="col-12 col-md-6 ">
                                        <a class=" btn btn-primary" href="{{route('edit_delivery_method',$method->id)}}">
                                            <i class="fa fa-edit"></i>
                                            ویرایش
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <button id="{{$method->id}}" class="delete btn btn-danger ">
                                            <i class="fa fa-trash"></i>
                                            حذف
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @else
                    <div class="alert alert-light">
                        هنوز هیچ روش ارسال محصولی را تعریف نکرده اید.
                    </div>
                @endif
{{--                <div class="row d-flex justify-content-center">{{$methods->links()}}</div>--}}
            </div>

        </div>
    </div>

@endsection

@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>

        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete', function () {
                var id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این روش ارسال رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-delivery-method',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                if (response == 'forbidden') {
                                    swal("خطا!", "این گروه دارای محصول است و شما مجاز به حذف آن نیستید.", "error")
                                } else if (response == 'success') {
                                    var table = document.getElementById('table')
                                    table.deleteRow(rowindex)
                                    swal("موفق", "روش ارسال حذف شد.", "success")
                                }
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection
