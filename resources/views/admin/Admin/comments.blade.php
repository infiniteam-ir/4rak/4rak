@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">مدیریت نظرات</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($comments)>0)
                @php($counter=1)
                <div class="box-body">
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                           id="table">
                        <tr class="table-header">
                            <th>ردیف</th>
                            <th>نویسنده</th>
                            <th>متن</th>
                            <th>امکانات</th>
                        </tr>
                        @foreach($comments as $comment )
                            <tr>
                                <td>{{$counter}}</td>
                                <td>{{$comment->user->name.' '.$comment->user->family}} </td>
                                <td>{{$comment->content}}</td>
                                <td>
                                    <div class="row w-100 justify-content-center">
                                        <div class="col-12 col-md-6">
                                            <button id="{{$comment->id}}" class="delete btn btn-danger ">
                                                <i class="fa fa-trash"></i>
                                                حذف
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php($counter++)
                        @endforeach
                    </table>
                    <div class="row d-flex justify-content-center">{{$comments->links()}}</div>
                </div>
            @else
                <div class="alert alert-warning mt-1 text-center">
                    <h4 class="text-warning">
                        نظری جهت نمایش وجود ندارد
                    </h4>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')

    <script>

        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete', function () {
                var id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این نظر رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-comment',
                            },
                            dataType: 'json',
                            success: function (response) {
                                var table = document.getElementById('table')
                                table.deleteRow(rowindex)
                                swal("موفق", " نظر حذف شد.", "success")
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection

