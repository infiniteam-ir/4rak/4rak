@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css"/>

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">درخواست های تسویه </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        {{Session::pull('error')}}
                    </h4>
                </div>
            @endif

            <div class="box-body">
                @if(count($checkouts)>0)
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2 "
                           id="table">
                        <tr class="table-header">
                            @php($counter=1)
                            <th>#</th>
                            <th>فروشگاه</th>
                            <th>مبلغ (تومان)</th>
                            <th>وضعیت</th>
                            <th>صاحب کارت</th>
                            <th>شماره کارت</th>
                            <th>تاریخ</th>
                            <th>امکانات</th>
                        </tr>
                        @foreach($checkouts as $checkout)
                            <tr>
                                <td>{{$counter}}</td>
                                <td>{{$checkout->shop->name}}</td>
                                <td>{{number_format($checkout->total_price)}}</td>
                                <td>{{$checkout->status}}</td>
                                <td>{{$checkout->card_owner}}</td>
                                <td>{{$checkout->card_number}}</td>
                                <td>{{verta($checkout->created_at)}}</td>
                                <td>
                                    <div class="row justify-content-center">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#checkoutModal-{{$checkout->id}}" data-whatever="@getbootstrap">تسویه</button>

                                        <div class="modal fade" id="checkoutModal-{{$checkout->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">کد پیگیری</h5>
                                                        <button type="button" class="close mr-1 mt-1" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    {!! Form::open(['method'=>'POST','action'=>['AdminController@payCheckout',$checkout->id]]) !!}
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">کد پیگیری پرداخت را وارد کنید: </label>
                                                            <input name="tracking_id" type="text" class="form-control" id="recipient-name">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                                                        <button class="btn btn-success w-100" type="submit">تایید</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php($counter++)
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-light">
                        درخواست تسویه ای وجود ندارد
                    </div>
                @endif
                <div class="row d-flex justify-content-center">{{$checkouts->links()}}</div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete', function () {
                var id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این پلن رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-pay-plan',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                var table = document.getElementById('table')
                                table.deleteRow(rowindex)
                                swal("موفق", "پلن حذف شد.", "success")
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "پلن با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection
