@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">تنظیمات فروشگاه </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                <div class="row">
                    <div class="col-21 col-md-6">

                        <div class="alert alert-success">
                            <h5>اسلایدر</h5>
                             اندازه استاندارد تصاویر اسلایدر 300*875 پیکسل میباشد.
                        </div>
                        <div class="row justify-content-center px-2">
                            @if(count($slider_imgs)>0)
                                @php($counter=5 - count($slider_imgs))
                                @foreach($slider_imgs as $img)
                                    <div class="col-12 border rounded py-2 my-2 justify-content-center text-center">
                                        <img class="img-fluid w-100 my-2" style="max-height:150px;" src="{{asset('images/slider/'.$img->photo->path)}}"  alt="">
                                        <a class="btn btn-danger" href="{{route('delete_slider_imgs',$img->id)}}">حذف</a>
                                    </div>
                                @endforeach
                                @while($counter>0)
                                    <div class="col-12 my-2 border rounded p-2">
                                        <form method="POST" action="{{route('save_slider_imgs')}}"
                                              enctype="multipart/form-data">
                                            @csrf
                                        <input type="file" name="img" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                        </form>
                                    </div>
                                    @php($counter--)
                                @endwhile
                            @else
                                <form method="POST" action="{{route('save_slider_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')"  name="img" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_slider_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_slider_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_slider_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_slider_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')"  name="img" class="form-control my-2">
                                        <input type="text" name="note" placeholder="متن" class="form-control  my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                    <div class="col-21 col-md-6">
                        <div class="alert alert-primary">
                            <h5>بنر های ردیف بالا</h5>
                            اندازه استاندارد بنرها  201*268 پیکسل میباشد.
                        </div>
                        <div class="row justify-content-center px-2">
                            @if(count($top_banners)>0)
                                @php($counter=3 - count($top_banners))
                                @foreach($top_banners as $banner)
                                    <div class="col-12 border rounded py-2 my-2 justify-content-center text-center">
                                        <img class="img-fluid w-100 my-2" style="max-height:150px;" src="{{asset('images/banner/'.$banner->photo->path)}}"  alt="">
                                        <a class="btn btn-danger" href="{{route('delete_banner_imgs',$banner->id)}}">حذف</a>
                                    </div>
                                @endforeach
                                @while($counter>0)
                                    <div class="col-12 my-2 border rounded p-2">
                                        <form method="POST" action="{{route('save_banner_imgs')}}"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <input type="file" name="img" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" class="form-control my-2">
                                            <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                            <input type="text" name="type" value="top" class="d-none form-control ">
                                            <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                        </form>
                                    </div>
                                    @php($counter--)
                                @endwhile
                            @else
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')"  name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="text" name="type" value="top" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="text" name="type" value="top" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="text" name="type" value="top" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                            @endif
                        </div>

                        <div class="alert alert-primary">
                            <h5>بنر های ردیف پایین</h5>
                            اندازه استاندارد بنرها  150*410 پیکسل میباشد.
                        </div>
                        <div class="row justify-content-center px-2">
                            @if(count($bot_banners)>0)
                                @php($counter=2 - count($bot_banners))
                                @foreach($bot_banners as $banner)
                                    <div class="col-12 border rounded py-2 my-2 justify-content-center text-center">
                                        <img class="img-fluid w-100 my-2" style="max-height:150px;" src="{{asset('images/banner/'.$banner->photo->path)}}"  alt="">
                                        <a class="btn btn-danger" href="{{route('delete_banner_imgs',$banner->id)}}">حذف</a>
                                    </div>
                                @endforeach
                                @while($counter>0)
                                    <div class="col-12 my-2 border rounded p-2">
                                        <form method="POST" action="{{route('save_banner_imgs')}}"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <input type="file" name="img" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" class="form-control my-2">
                                            <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                            <input type="text" name="type" value="bot" class="d-none form-control ">
                                            <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                        </form>
                                    </div>
                                    @php($counter--)
                                @endwhile
                            @else
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')"  name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                         <input type="text" name="type" value="bot" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')" name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="text" name="type" value="bot" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                            @endif
                        </div>

                        <div class="alert alert-primary">
                            <h5>بنر ساید بار</h5>
                            اندازه استاندارد بنر سایدبار 251*265 پیکسل میباشد.
                        </div>
                        <div class="row justify-content-center px-2">
                            @if($side_banner)
                                    <div class="col-12 border rounded py-2 my-2 justify-content-center text-center">
                                        <img class="img-fluid w-100 my-2" style="max-height:150px;" src="{{asset('images/banner/'.$side_banner->photo->path)}}"  alt="">
                                        <a class="btn btn-danger" href="{{route('delete_banner_imgs',$side_banner->id)}}">حذف</a>
                                    </div>
                            @else
                                <form method="POST" action="{{route('save_banner_imgs')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-12 my-2 border rounded p-2">
                                        <input type="file" required oninvalid="this.setCustomValidity('لطفا تصویر را انتخاب کنید')" oninput="setCustomValidity('')"  name="img" class="form-control my-2">
                                        <input type="text" name="url" placeholder="لینک" class="form-control my-2">
                                        <input type="text" name="type" value="side" class="d-none form-control ">
                                        <input type="submit" class="form-control my-2 btn-outline-success" value="ذخیره">
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


