@extends('admin.layouts.newTheme.master')


@section('main')
    <div class="row">
       <div class="col-12 w-100">
           @if(Session::has('success'))
               <div class="alert alert-success mt-1">
                   <h4 class="text-success text-center">
                       {{Session::pull('success')}}
                   </h4>
               </div>
           @endif
           @if(Session::has('config'))
               <div class="alert alert-success mt-1">
                   <h4 class="text-success text-center">
                       {{Session::pull('config')}}
                   </h4>
               </div>
           @endif
       </div>
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0 m-b-30">موجودی کالاهای شما</h4>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$product_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$product_qty}} </h2>
                        <p class="text-muted">کالا</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">فروش آنلاین امروز</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#238F3E"
                               data-bgColor="#A5F56E" value="{{$total_online_sells[0]}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$total_online_sells[0]}}</h2>
                        <p class="text-muted">فروش</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">فروش آفلاین امروز</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                               data-bgColor="#FFE6BA" value="{{$total_offline_sells[0]}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$total_offline_sells[0]}} </h2>
                        <p class="text-muted">فروش</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">مشتریان فروشگاه</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#4C228F"
                               data-bgColor="#9F6AEB" value="{{$customers_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$customers_qty}} </h2>
                        <p class="text-muted">نفر</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->


    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">هزینه و درآمد کل</h4>
                <canvas id="total-in-out" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">هزینه و درآمد هفتگی</h4>
                <canvas id="weekly-in-out" class=""></canvas>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">نمودار موجودی کالاهای شما در هر انبار</h4>
                <canvas id="storages" width="500px" height="300" class=""></canvas>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">کارکرد هفتگی کل کارکنان بر اساس ساعت</h4>
                <canvas id="work-time" class=""></canvas>
            </div>
        </div>
    </div>

    <div class="row ">
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">کارکنان فروشگاه</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#F5426E "
                               data-bgColor="#EBB198" value="{{$personnel}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$personnel}}</h2>
                        <p class="text-muted"> نفر</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">بدهی فروشگاه</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <img class="img-fluid" width="70px" height="70px" src="{{asset('img/debt.svg')}}" alt=4rak>
                    </div>
                    <div class="widget-detail-1">
                        <h6 class="p-t-10 m-b-0 text-danger"> {{number_format($debts)}}</h6>
                        <p class="text-muted"> تومان</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">بستانکاری فروشگاه</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1 ">
                        <img class="img-fluid" width="70px" height="70px" src="{{asset('img/credit.svg')}}" alt=4rak>
                    </div>
                    <div class="widget-detail-1">
                        <h6 class="p-t-10 m-b-0 text-success"> {{number_format($credits*-1)}} </h6>
                        <p class="text-muted"> تومان</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu p-3 justify-content-center text-center" style="width: 300px" role="menu">
                        <li class="text-right "> <span class="">جهت تسویه، شماره کارت، نام و نام خانوادگی صاحب کارت را وارد کنید.</span></li>

                        <li><input class="form-control my-1" type="text" id="card" placeholder="شماره کارت"></li>
                        <li> <input class="form-control my-1" type="text" id="owner" placeholder="صاحب کارت "></li>
                        <li> <button class="btn btn-success" id="checkout-req" >درخواست تسویه</button></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0 mb-0">کیف پول فروشگاه</h4>
                <span class="text-muted bg-light mt-1 rounded">در انتظار تسویه : <span id="pending">{{number_format($pending_money)}}</span> تومان</span>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <img class="img-fluid" width="70px" height="70px" src="{{asset('img/wallet.svg')}}" alt=4rak>

                    </div>

                    <div class="widget-detail-1">
                        <h6 class="p-t-10 m-b-0" id="wallet"> {{number_format($wallet)}} </h6>
                        <p class="text-muted"> تومان</p>
                    </div>
                </div>

            </div>
        </div><!-- end col -->


    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">مرسولات پستی 7 روز گذشته</h4>
                <canvas id="sent" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">سفارش های آنلاین ثبت شده 7 روز گذشته</h4>
                <canvas id="orders" class=""></canvas>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->


@endsection
@section('script')
    <script>
        $(document).ready(function () {
            let chart1 = document.getElementById('total-in-out');
            let incomes ={{$incomes}};
            let outgoes ={{$outgoes}};
            let chart2 = document.getElementById('weekly-in-out');
            let chart3 = document.getElementById('income-detail');
            let chart4 = document.getElementById('outgo-detail');
            let today_income={{$total_weekly_earn[0]}};
            let today1_income={{$total_weekly_earn[1]}};
            let today2_income={{$total_weekly_earn[2]}};
            let today3_income={{$total_weekly_earn[3]}};
            let today4_income={{$total_weekly_earn[4]}};
            let today5_income={{$total_weekly_earn[5]}};
            let today6_income={{$total_weekly_earn[6]}};

            let today_outgo={{$total_weekly_outgo[0]}};
            let today1_outgo={{$total_weekly_outgo[1]}};
            let today2_outgo={{$total_weekly_outgo[2]}};
            let today3_outgo={{$total_weekly_outgo[3]}};
            let today4_outgo={{$total_weekly_outgo[4]}};
            let today5_outgo={{$total_weekly_outgo[5]}};
            let today6_outgo={{$total_weekly_outgo[6]}};
            let max_in_out={{$max_weekly_in_out}}

                let total_in_out_Chart = new Chart(chart1, {
                type: 'pie',
                data: {
                    labels: ['درآمد', 'هزینه'],
                    datasets: [{
                        label: 'Chart 1',
                        backgroundColor: ['#10e690', '#ff7b73'],
                        data: [incomes, outgoes]
                    }]
                },
                options: {}
            });

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'getLast7Days',
                },
                dataType: 'json',
                success: function (response) {
                    if (max_in_out>0){
                        let weekly_in_out_chart = new Chart(chart2, {
                            type: 'line',
                            data: {
                                labels: response.reverse(),
                                datasets: [
                                    {
                                        label: 'درآمد',
                                        backgroundColor:'rgb(24,162,97,0.8)',
                                        borderColor:'#0c923e',
                                        pointBackgroundColor:'#167225',
                                        lineTension:'.3',
                                        // data: [on_today6,on_today5,on_today4,on_today3,on_today2,on_today1,on_today]
                                        data: [today6_income,today5_income,today4_income,today3_income,today2_income,today1_income,today_income]
                                    },
                                    {
                                        label: 'هزینه',
                                        backgroundColor:'rgba(255, 0,0, 0.5)',
                                        borderColor:'#d22929',
                                        pointBackgroundColor:'#721616',
                                        lineTension:'.3',
                                        data: [today6_outgo,today5_outgo,today4_outgo,today3_outgo,today2_outgo,today1_outgo,today_outgo],
                                    }
                                ]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: Math.ceil(max_in_out+(max_in_out*10/100)),
                                            min: 0,
                                            stepSize: Math.ceil(max_in_out/10)
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        });
                    }else{
                        let weekly_in_out_chart = new Chart(chart2, {
                            type: 'line',
                            data: {
                                labels: response.reverse(),
                                datasets: [
                                    {
                                        label: 'درآمد',
                                        backgroundColor:'rgb(24,162,97,0.8)',
                                        borderColor:'#0c923e',
                                        pointBackgroundColor:'#167225',
                                        lineTension:'.3',
                                        // data: [on_today6,on_today5,on_today4,on_today3,on_today2,on_today1,on_today]
                                        data: [today6_income,today5_income,today4_income,today3_income,today2_income,today1_income,today_income]
                                    },
                                    {
                                        label: 'هزینه',
                                        backgroundColor:'rgba(255, 0,0, 0.5)',
                                        borderColor:'#d22929',
                                        pointBackgroundColor:'#721616',
                                        lineTension:'.3',
                                        data: [today6_outgo,today5_outgo,today4_outgo,today3_outgo,today2_outgo,today1_outgo,today_outgo],
                                    }
                                ]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: 50,
                                            min: 0,
                                            stepSize: 5
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        });
                    }

                },
                error: function (response) {
                    console.log('error')
                }
            });

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-income-outgo-detail-chart',
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    let income_detail = new Chart(chart3, {
                        type: 'pie',
                        data: {
                            labels:response['incomes'],
                            datasets: [{
                                label: 'Chart 1',
                                backgroundColor: ['#10e690', '#ff7b73'],
                                data: response['in-prices']
                            }]
                        },
                        options: {}
                    });
                    let outgo_detail = new Chart(chart4, {
                        type: 'pie',
                        data: {
                            labels:response['outgoes'],
                            datasets: [{
                                label: 'Chart 1',
                                backgroundColor: ['#9abed6', '#f77916', '#f54c52', '#f55abf', '#5DEB94'],
                                data: response['out-prices']
                            }]
                        },
                        options: {}
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });

        })
    </script>
    <script>
        $(document).ready(function () {

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                // url: $('.ajax.d-none').attr('id'),
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-storage-detail-for-chart',
                },
                dataType: 'json',
                success: function (response) {
                    let colors=new Array();
                    for (let i=0;i<response['storage_qty'];i++){
                        let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
                        colors.push(randomColor)
                    }
                    let max_qty=parseInt(response['max_qty'])
                    new Chart(document.getElementById("storages"), {
                        type: 'pie',
                        data: {
                            labels: response['storages'],
                            datasets: [
                                {
                                    backgroundColor: colors,
                                    data: response['products']
                                }
                            ]
                        },
                    });

                },
                error: function (response) {
                    console.log('error')
                }
            });
        })
    </script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-personnel-work-time-chart',
                },
                dataType: 'json',
                success: function (response) {
                    let work_time = new Chart(document.getElementById('work-time') , {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#3dd79a',
                                borderColor:'#0c923e',
                                pointBackgroundColor:'#167225',
                                lineTension:'.3',
                                data: response['work']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+2,
                                        min: 0,
                                        stepSize: 1
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    })
                },
                error: function (response) {
                    console.log('error')
                }
            });

        })
    </script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-last-7days-sent-orders',
                },
                dataType: 'json',
                success: function (response) {
                    let weekly_sent_chart = new Chart(document.getElementById('sent'), {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#3dd79a',
                                borderColor:'#0c923e',
                                pointBackgroundColor:'#167225',
                                lineTension:'.3',
                                data: response['sent']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+10,
                                        min: 0,
                                        stepSize: Math.ceil((response['max']+10)/10)
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-last-7days--online-orders',
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    let weekly_online_orders = new Chart(document.getElementById('orders'), {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#d73d3d',
                                borderColor:'#920c0c',
                                pointBackgroundColor:'#721621',
                                lineTension:'.3',
                                data: response['orders']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+10,
                                        min: 0,
                                        stepSize: Math.ceil((response['max']+10)/10)
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });

            $('#checkout-req').on('click',function (){
                var card=$('#card').val();
                var owner=$('#owner').val();
                if (card!="" && owner !=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        },
                        url: '{{route('webAjax')}}',
                        data: {// change data to this object
                            do: 'checkout-request',
                            card:card,
                            owner:owner,
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response['status']=='done'){
                                $('#pending').text(response['pending'])
                                $('#wallet').text(0);
                                swal('درخواست  تسویه شما ثبت شد.');
                            }else{
                                swal('درخواست نامعتبر');
                            }
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                }else{
                    swal('شماره کارت و نام صاحب کارت را وارد کنید')
                }
            })
        })
    </script>
@endsection
