@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">ویرایش گروه مطلب </h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                @livewire('articles.edit-article-cat',['cats'=>$cats,'cat'=>$cat])
            </div>
        </div>
    </div>

@endsection


