@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">مدیریت گروه مطالب</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <div class="nav  d-flex justify-content-center ">
                            <ul class=" d-flex justify-content-between list-unstyled w-100 user-list-menu p-2 checkout-ul ">
                                <li class="blog-list-item ">
                                    <a href="{{route('article-category-new')}}" class="btn btn-outline-success">
                                        <i class="fa fa-plus"></i>
                                        گروه جدید
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if(count($cats)>0)
                    @php($counter=1)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2" id="table">
                            <tr class="table-header">
                                <th>ردیف</th>
                                <th>تصویر</th>
                                <th>عنوان</th>
                                <th>سرگروه</th>
                                <th>امکانات</th>
                            </tr>
                            @foreach($cats as $cat )
                                <tr>
                                    <td>{{$counter}}</td>
                                    <td>
                                        @if($cat->photo)
                                            <img class="img-fluid img-product-list" src="{{asset('images/articles/cats/'.$cat->photo->path)}}" alt="">
                                        @else
                                            <img class="img-fluid img-product-list" src="{{asset('img/default.jpg')}}" alt="">
                                        @endif
                                    </td>
                                    <td>{{$cat->title}}</td>
                                    <td>{{$cat->parent_id}}</td>
                                    <td> <div class="row w-100 justify-content-center">
                                            <div class="col-12  ">
                                                <a class=" btn btn-primary w-50" href="{{route('article-category-edit',$cat->id)}} " style="white-space: nowrap">
                                                    <i class="fa fa-edit" style="white-space: nowrap"></i>
                                                    ویرایش
                                                </a>
                                            </div>
                                            <div class="col-12 ">
                                                <a href="javascript:" id="{{$cat->id}}"  class="delete btn btn-danger my-2 w-50 ">
                                                    <i class="fa fa-trash"></i>
                                                    حذف
                                                </a>
                                            </div>
                                        </div></td>
                                </tr>
                                @php($counter++)
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$cats->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            گروهی جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')

    <script>

        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete',function () {
                var id=this.id
                var rowindex=$(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این گروه رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-article-category',
                            },
                            dataType: 'json',
                            success: function (response) {
                                if(response=='forbidden'){
                                    swal("خطا!", "این گروه دارای مطلب است و شما مجاز به حذف آن نیستید.", "error")
                                }else if(response=='success'){
                                    var table=document.getElementById('table')
                                    table.deleteRow(rowindex)
                                    swal("موفق", "گروه مطلب حذف شد.", "success")
                                }
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection

