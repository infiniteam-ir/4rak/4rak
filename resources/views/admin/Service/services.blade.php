@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">مدیریت خدمات</h1>
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('deleted'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('deleted')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">

            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <a href="{{route('service.create')}}" class="btn btn-outline-success rounded">
                            <i class="fa fa-plus"></i>
                            افزودن
                        </a>
                        <input type="text" name="service_id" id="service_name" class="service_name d-none">
                        <div class="autocomplete" style="width:300px;">
                            {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو با عنوان ','class'=>'form-control','id'=>'service_names']) !!}
                        </div>

                    </div>
                </div>
                @if(count($services)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                               id="table">
                            <tr class="table-header text-center">
                                <th>عنوان سرویس</th>
                                <th>تصویر</th>
                                <th>نوع</th>
                                <th>هزینه بر اساس نوع (تومان)</th>
                                <th>شماره تماس</th>
                                <th>وضعیت</th>
                                <th>امکانات</th>
                            </tr>
                            @foreach($services as $service )
                                <tr>
                                    <td>
                                        <a class="text-decoration-none" href="{{route('service.edit',$service->id)}}">
                                            {{$service->title}}
                                        </a>
                                    </td>
                                    <td>
                                        @if($service->photo)
                                            <img class="img-product-list rounded" src="{{asset('images/services/'.$service->photo->path)}}">
                                        @else
                                            <img class="img-product-list" src="{{asset('img/default-2.svg')}}">
                                        @endif
                                    </td>
                                    <td>{{$service->type}}</td>
                                    <td>{{number_format($service->price)}}</td>
                                    <td>{{$service->call}}</td>
                                    <td>
                                        @if($service->status=='active')
                                            <i class="badge badge-success px-2">فعال</i>
                                        @else
                                            <i class="badge badge-danger px-2">غیر فعال</i>

                                        @endif
                                    </td>
                                    <td>
                                        <div class="row w-100 justify-content-center">
                                            <div class="col-12 my-1 ">
                                                <a class=" btn btn-primary"
                                                   href="{{route('service.edit',$service->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                    ویرایش
                                                </a>
                                            </div>
                                            <div class="col-12 my-1">
                                                <button id="{{$service->id}}" class="delete btn btn-danger ">
                                                    <i class="fa fa-trash"></i>
                                                    حذف
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$services->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            خدمتی جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->

    <script>

        $(document).ready(function () {
            var table = document.getElementById('table')
            $('.delete.btn').click(function () {
                var service_id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این گروه رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                service_id: service_id,
                                do: 'delete-service',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                var table = document.getElementById('table')
                                table.deleteRow(rowindex)
                                swal("موفق", "سرویس حذف شد.", "success")

                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });
            $("#service_names").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>عنوان سرویس</th>\n' +
                    '                            <th>نوع</th>\n' +
                    '                            <th>هزینه بر اساس نوع(تومان)</th>\n' +
                    '                            <th>شماره تماس</th>\n' +
                    '                            <th> وضعیت</th>\n' +
                    '                            <th> امکانات</th>\n' +
                    '                        </tr>'

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        service_name: $('#service_names').val(),
                        do: 'search-service',
                    },
                    dataType: 'json',
                    success: function (response) {
                        data = response
                        console.log(response)
                        response['data'].forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            var td2 = row.insertCell(2)
                            var td3 = row.insertCell(3)
                            var td4 = row.insertCell(4)
                            var td5 = row.insertCell(5)

                            td0.innerHTML = re['title']
                            td1.innerHTML = re['type']
                            td2.innerHTML = re['price']
                            td3.innerHTML = re['call']
                            if(re['status']=='active')
                                td4.innerHTML = '<i class="badge badge-success px-2">فعال</i>'
                            else
                                td4.innerHTML = '<i class="badge badge-danger px-2">غیر فعال</i>'

                            let s_route='{{route('service.edit','x')}}';
                            s_route=s_route.replace('x',re['id'])
                            td5.innerHTML = '<div class="row w-100 justify-content-center">\n' +
                                '                                        <div class="col-12 col-md-3 ">\n' +
                                '                                            <a class=" btn btn-primary" href="'+s_route+'">\n' +
                                '                                                <i class="fa fa-edit"></i>\n' +
                                '                                                ویرایش\n' +
                                '                                            </a>\n' +
                                '                                        </div>\n' +
                                '                                        <div class="col-12 col-md-3">\n' +
                                '                                            <button class="delete btn btn-danger" id="'+re['id']+'" >\n' +
                                '                                                <i class="fa fa-trash"></i>\n' +
                                '                                                حذف\n' +
                                '                                            </button>\n' +
                                '\n' +
                                '                                        </div>\n' +
                                '                                    </div>'
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });



        })
    </script>

@endsection


