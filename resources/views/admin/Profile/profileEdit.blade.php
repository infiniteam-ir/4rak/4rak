@extends('admin.layouts.newTheme.master')

@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">اطلاعات کاربری</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body px-2">
               @livewire('profile.edit-profile',['provinces'=>$provinces,'user'=>Auth::user()])
            </div>
        </div>

@endsection
        @section('script')
            <script>
                $(document).ready(function () {
                    var ajaxUrl = $('.ajax.d-none').attr('id')

                    $('#province').on("change", function () {
                        var province = $('#province').val()
                        // console.log(province)
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: ajaxUrl,
                            data: {
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do: 'get-cities',
                                province: province
                            },
                            dataType: 'json',
                            success: function (response) {
                                document.getElementById('city').innerHTML = ''
                                response.forEach(function (re) {
                                    document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                                    document.getElementById('city').value = re['id']
                                })
                                console.log(document.getElementById('city'))
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                    });

                })
            </script>
@endsection



