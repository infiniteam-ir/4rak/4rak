@extends('admin.layouts.newTheme.master')

@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <style>
        .account-settings .user-profile {
            margin: 0 0 1rem 0;
            padding-bottom: 1rem;
            text-align: center;
        }

        .account-settings .user-profile .user-avatar {
            margin: 0 0 1rem 0;
        }

        .account-settings .user-profile .user-avatar img {
            width: 90px;
            height: 90px;
            -webkit-border-radius: 100px;
            -moz-border-radius: 100px;
            border-radius: 100px;
        }

        .account-settings .user-profile h5.user-name {
            margin: 0 0 0.5rem 0;
        }

        .account-settings .user-profile h6.user-email {
            margin: 0;
            font-size: 0.8rem;
            font-weight: 400;
            color: #9fa8b9;
        }

        .account-settings .about {
            margin: 2rem 0 0 0;
            text-align: center;
        }

        .account-settings .about h5 {
            margin: 0 0 15px 0;
            color: #007ae1;
        }

        .account-settings .about p {
            font-size: 0.825rem;
        }

        .form-control {
            border: 1px solid #cfd1d8;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            font-size: .825rem;
            background: #ffffff;
            color: #2e323c;
        }

        .card {
            background: #ffffff;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            border: 0;
            margin-bottom: 1rem;
        }


    </style>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">اطلاعات کاربری</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
                @if(Session::has('config'))
                    <div class="alert alert-success mt-1">
                        <h4 class="text-success text-center">
                            {{Session::pull('config')}}
                        </h4>
                    </div>
                @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body  px-2">
                <div class="container">
                    <div class="row gutters">
                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                            <div class="card h-100 border">
                                <div class="card-body">
                                    <div class="account-settings">
                                        <div class="user-profile">
                                            <div class="user-avatar">
                                                @if(Auth::user()->photo)
                                                    <img class="img-fluid"
                                                         src="{{asset('images/profile/'.Auth::user()->photo->path)}}"
                                                         alt="Profile">
                                                @else
                                                    <img class="img-fluid" src="{{asset('img/user-avatar.svg')}}"
                                                         alt="Profile">
                                                @endif
                                            </div>
                                            <h5 class="user-name">{{Auth::user()->name}} {{Auth::user()->family}}</h5>
                                            <h6 class="user-email">{{Auth::user()->email}}</h6>
                                        </div>
                                        <div class="text-right">
                                            <ul class="list-unstyled">
                                                <li class="my-2">
                                                    <a href="{{route('Profile.index')}}">داشبرد</a>
                                                </li>
                                                <li class="my-2">
                                                    <a href="{{route('Profile.edit',Auth::id())}}">ویرایش اطلاعات</a>
                                                </li>
                                                <li class="my-2">
                                                    <a href="javascript:void(0)" id="fave"> علاقه مندی ها</a>
                                                </li>
                                                <li class="my-2">
                                                    <a href="{{route('buyHistory')}}"> سوابق خرید</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                            <div class="card h-100 ">
                                <div class="card-body main-content">
                                    <div class="row gutters">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <h6 class="mb-2 text-primary">اطلاعات شخصی</h6>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="name">نام</label>
<!--                                                <input type="text" class="form-control" id="name"
                                                       value="{{Auth::user()->name}}" disabled>-->
                                                <h5 class="border border-success p-2 rounded">{{Auth::user()->name}}</h5>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="family">نام خانوادگی</label>
<!--                                                <input type="text" class="form-control" id="family"
                                                       value="{{Auth::user()->family}}" disabled>-->
                                                <h5 class="border border-success p-2 rounded">{{Auth::user()->family}}</h5>

                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="mobile">تلفن</label>
<!--                                                <input type="text" class="form-control" id="mobile"
                                                       value="{{Auth::user()->mobile}}" disabled>-->
                                                <h5 class="border border-success p-2 rounded">{{Auth::user()->mobile}}</h5>

                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="email">ایمیل</label>
<!--                                                <input type="email" class="form-control" id="email"
                                                       value="{{Auth::user()->email}}" disabled>-->
                                                <h5 class="border border-success p-2 rounded">{{Auth::user()->email}}</h5>

                                            </div>
                                        </div>
                                    </div>
                                    @if(Auth::user()->info)
                                        <hr class="bg-primary">
                                        <div class="row gutters">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h6 class="mt-3 mb-2 text-primary">آدرس</h6>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="province">استان</label>
<!--                                                    <input type="text" class="form-control" id="province"
                                                           value="{{Auth::user()->info->province->name}}" disabled>-->
                                                    <h5 class="border border-success p-2 rounded">{{Auth::user()->info->province->name}}</h5>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="city">شهر</label>
<!--                                                    <input type="text" class="form-control" id="city"
                                                           value="{{Auth::user()->info->city->name}}" disabled>-->
                                                    <h5 class="border border-success p-2 rounded">{{Auth::user()->info->city->name}}</h5>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="address">آدرس کامل</label>
<!--                                                    <input type="text" class="form-control" id="address"
                                                           value="{{Auth::user()->info->address}}" disabled>-->
                                                    <h5 class="border border-success p-2 rounded">{{Auth::user()->info->address}}</h5>

                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="phone">تلفن ثابت</label>
<!--                                                    <input type="text" class="form-control" id="phone"
                                                           value="{{Auth::user()->info->phone}}" disabled>-->
                                                    <h5 class="border border-success p-2 rounded">{{Auth::user()->info->phone}}</h5>

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection


@section('script')
    <script>
        $(document).ready(function (){
            $('#fave').click(function (){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        do: 'get-favorites',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        if (response==""){
                            swal('لیست علاقه مندی های شما خالیست !')
                        }else {
                            $('.main-content').empty()
                            response.forEach(function (re) {
                                var path='{{route('singleProduct',['slug','num'])}}'
                                path=path.replace('slug',re['product']['slug'])
                                path=path.replace('num',re['product']['id'])
                                var row='<div class="row py-2">'+
                                    '<div class="col-10 py-2 bg-info text-white rounded text-right">'+
                                    '<a class="text-white" href="'+path+'">'+re['product']['name']+'</a>'+
                                    '<a id="'+re['id']+'"  class="del btn btn-sm btn-danger float-left">حذف</a>'+
                                    '<span class="d-none '+re['id']+'" id="'+re['product']['id']+'"></span>'+
                                    '</div>'+
                                    ' </div>';
                                $('.main-content').append(row)
                            })
                        }

                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            })
            $(document).on('click','.del.btn',function (){
                var row=$(this).parent().parent()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        do: 'delete-favorite',
                        id:$(this).attr('id'),
                        pro_id:$('.d-none.'+$(this).attr('id')+'').attr('id')
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        row.remove()
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            })
        })
    </script>
@endsection
