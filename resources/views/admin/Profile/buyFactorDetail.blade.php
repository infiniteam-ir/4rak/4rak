@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

        <div class="row p-4 d-flex pb-5">
            <div class="col-12 alert alert-secondary text-center ">
                <h1 class="text-primary">جزئیات فاکتور</h1>
            </div>
            <div class="col-12 bg-white p-2 rounded box-shadow">
                @if(Session::has('success'))
                    <div class="alert alert-success mt-1">
                        <h4 class="text-success">
                            {{Session('success')}}
                        </h4>
                    </div>
                @endif
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul >
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                 @endif
                <div class="box-header mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <div class="nav  d-flex justify-content-center ">
                            <ul class=" d-flex justify-content-between list-unstyled w-100 user-list-menu p-2 checkout-ul ">
                                <li class="blog-list-item">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-body  border-teacher-index px-2">
                    <div class="row w-100 py-3  text-center mx-2 px-5">
                        <div class="col-12 col-md-2">
                            <img src="{{asset('images/profile.svg')}}" alt="profile" class="img-fluid rounded-circle img-prof-reg">
                        </div>
                        <div class="col-12 col-md-6 text-right ">
                            <h4 class="my-1">رضا احمدی</h4>
                            <h6 class="my-2 text-muted">
                                09131112233 <i class="fa fa-mobile-alt mx-3"></i>
                            </h6>
                            <h6 class="my-2 text-muted">
                                03141112233 <i class="fa fa-phone-square-alt mx-3"></i>
                            </h6>
                            <h6 class="text-muted my-1 text-right">
                                اصفهان، نجف آباد، خیابان منتظری، کوچه رضایی، پلاک 50
                            </h6>
                        </div>
                        <div class="col-12 col-md-4">
                            <h5 class="text-left ">
                                تاریخ:
                                <span class="badge badge-light text-left">1399/01/30</span>
                            </h5>
                            <h5 class="text-left">
                                شماره فاکتور:
                                <span class="badge badge-light text-left">1364</span>
                            </h5>
                        </div>
                    </div>
                    <div class="row w-100 py-3 d-flex justify-content-center ">
                        <div class="col-12 ">
                            <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                <tr class="table-header">
                                    <th>ردیف</th>
                                    <th>تصویر</th>
                                    <th>محصول</th>
                                    <th>تعداد</th>
                                    <th>قیمت واحد(تومان)</th>
                                    <th>قیمت کل(تومان)</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><img src="{{asset('images/Flash-memory.jpg')}}" alt="Product" class="img-fluid img-product-list"></td>
                                    <td>فلش مموری 64</td>
                                    <td>10</td>
                                    <td>150000</td>
                                    <td>1500000</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><img src="{{asset('images/Flash-memory.jpg')}}" alt="Product" class="img-fluid img-product-list"></td>
                                    <td>فلش مموری 64</td>
                                    <td>10</td>
                                    <td>150000</td>
                                    <td>1500000</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><img src="{{asset('images/Flash-memory.jpg')}}" alt="Product" class="img-fluid img-product-list"></td>
                                    <td>فلش مموری 64</td>
                                    <td>10</td>
                                    <td>150000</td>
                                    <td>1500000</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row w-100  d-flex  justify-content-center">
                        <div class="col-12 col-md-6 text-center">
                            <a href="#" class="btn btn-warning">بستن سفارش</a>
                        </div>
                        <div class="col-12 col-md-6 text-center">
                            <h3 class="text-center">
                                مبلغ کل فاکتور (تومان):
                                <span class="badge badge-primary p-2 ">4500000</span>
                            </h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection


