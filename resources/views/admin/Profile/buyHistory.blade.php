@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">سوابق خرید</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center justify-content-center mt-1">
                    <h4 class="text-success text-center">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(count($customer_factors)>0)
                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                    <div class="col-12 mt-3 col-md-10 text-center">
                        <div class="box-body">
                            <table class="table table-success table-bordered table-responsive-md table-striped table-header mt-2  ">
                                <tr class="table-header">
                                    <th>ردیف</th>
                                    <th>مبلغ(تومان)</th>
                                    <th>وضعیت</th>
                                    <th>تاریخ</th>
                                    <th>امکانات</th>
                                </tr>
                                @php($c=0)
                                @foreach($customer_factors as $customer_factor)
                                    @php($c++)
                                    <tr>
                                        <td>{{$c}}</td>
                                        <td>{{$customer_factor->total}}</td>
                                        <td>{{$customer_factor->status}}</td>
                                        <td>{{verta($customer_factor->created_at)->format('Y-n-j H:i')}}</td>
                                        <td>
                                            <a style="cursor: pointer" class="text-decoration-none h5 text-primary" data-toggle="modal"
                                               data-target="#FactorModal-{{$customer_factor->id}}" data-whatever="@getbotstrap">
                                                جزئیات
                                            </a>
                                            {{--MODAL--}}
                                            <div class="modal fade" id="FactorModal-{{$customer_factor->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog ">
                                                    <div class="modal-content">
                                                        <div class=" modal-header ">
                                                            <h5 class="modal-title text-warning" id="exampleModalLabel">
                                                                جزئیات فاکتور</h5>
                                                        </div>
                                                        <div class="modal-body ">
                                                            <table class="table table-sm">
                                                                <tr>
                                                                    <th>محصول</th>
                                                                    <th>تعداد</th>
                                                                    <th>قیمت واحد</th>
                                                                    <th>قیمت کل</th>
                                                                    <th>نام فروشگاه</th>
                                                                </tr>
                                                            @foreach($customer_factor->factor_pro as $pro)
                                                                    <tr>
                                                                        <td>{{$pro->product->name}}</td>
                                                                        <td>{{$pro->qty}}</td>
                                                                        <td>{{$pro->price}}</td>
                                                                        <td>{{$pro->price*$pro->qty}}</td>
                                                                        <td>
                                                                            <a class="text-decoration-none"
                                                                               href="{{route('singleshop',$pro->shop->id)}}">{{$pro->shop->name}}</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </div>

                                                        <div class="modal-footer ">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">بستن
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>

            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز خریدی نداشته اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


