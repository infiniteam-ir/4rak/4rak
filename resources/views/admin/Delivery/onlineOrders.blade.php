@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light mb-2  text-center ">
            <h1 class="text-muted">سفارشات آنلاین</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)

                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                    <div class="col-12 mt-3 col-md-10 text-center">
                        <div class="box-body">
                            <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                <tr class="table-header">
                                    <th>شماره فاکتور</th>
                                    <th>خریدار</th>
                                    <th>مبلغ(تومان)</th>
                                    <th>تاریخ</th>
                                    <th>ساعت</th>
                                    <th>امکانات</th>
                                </tr>
                                @foreach($factors as $factor)
                                    <tr>
                                        <td>{{$factor->id}}</td>
                                        <td>{{$factor->customer->name}} {{$factor->customer->family}}</td>
                                        <td>{{number_format($factor->total_price)}}</td>
                                        <td>{{verta($factor->f_date)->formatdate()}}</td>
                                        <td>{{verta($factor->created_at)->formattime()}}</td>
                                        <td>
                                            <a class="text-decoration-none" href="{{route('onlineOrdersInfo',$factor->id)}}">
                                                <span class="badge badge-primary p-2">جزئیات</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>

            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


