@extends('admin.layouts.newTheme.master')


@section('main')
    <div class="row px-2 my-3 d-flex flex-nowrap overflow-auto">
        <div class="col mx-1">
            <a href="{{route('deliveryReq')}}">
                <div class="btn btn-purple  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/delivery-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white">درخواست پیک</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('delivery.create')}}">
                <div class="btn btn-success text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/add-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white mt-2">افزودن پیک</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('onlineOrders')}}">
                <div class="btn btn-warning  text-center h-100" style="border-radius: 10px">
                    <div class="py-4 px-3">
                        <img src="{{asset('img/online-order-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white"> سفارشات انلاین</h6>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="card-box h-100">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">پیک موتوری های استان شما</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$motor_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">{{$motor_qty}}</h2>
                        <p class="text-muted"> پیک</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="card-box h-100">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">پیک وانت و کامیون استان شما</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$truck_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">{{$truck_qty}}</h2>
                        <p class="text-muted"> پیک</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->


    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">مرسولات پستی 7 روز گذشته</h4>
                <canvas id="sent" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">سفارش های آنلاین ثبت شده 7 روز گذشته</h4>
                <canvas id="orders" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->


@endsection
@section('script')
    <script>
        $(document).ready(function () {
            let chart1 = document.getElementById('sent');
            let chart2 = document.getElementById('orders');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-last-7days-sent-orders',
                },
                dataType: 'json',
                success: function (response) {
                    let weekly_sent_chart = new Chart(chart1, {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#3dd79a',
                                borderColor:'#0c923e',
                                pointBackgroundColor:'#167225',
                                lineTension:'.3',
                                data: response['sent']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+10,
                                        min: 0,
                                        stepSize: Math.ceil((response['max']+10)/10)
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-last-7days--online-orders',
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    let weekly_online_orders = new Chart(chart2, {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#d73d3d',
                                borderColor:'#920c0c',
                                pointBackgroundColor:'#721621',
                                lineTension:'.3',
                                data: response['orders']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+10,
                                        min: 0,
                                        stepSize: Math.ceil((response['max']+10)/10)
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });

        })
    </script>
@endsection
