@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet">

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">درخواست پیک</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1 text-center">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif

            <div class="container">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="1" data-toggle="tab" href="#nav-1" role="tab"
                           aria-controls="nav-1" aria-selected="true">موتور</a>
                        <a class="nav-item nav-link" id="2" data-toggle="tab" href="#nav-2" role="tab"
                           aria-controls="nav-2" aria-selected="false">وانت</a>
                        <a class="nav-item nav-link" id="3" data-toggle="tab" href="#nav-3" role="tab"
                           aria-controls="nav-3" aria-selected="false">کامیون</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active p-3" id="nav-1" role="tabpanel" aria-labelledby="1">
                        <div class="text-center bg-white m-0 w-100">
                            <div class="row">
                                {!! Form::select('province',$arr,null,['class'=>'form-control w-25','id'=>'province']) !!}
                                <select class="form-control w-25 mx-2" name="city" id="city">

                                </select>
                                <button class="btn btn-success" id="search-m">جستجو</button>
                            </div>
                            <h6 class="bg-success text-white py-2">لیست پیک موتوری</h6>
                        </div>
                        @if(count($motors)>0)
                            <div class="box-body">
                                <table id="table-m" class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2" >
                                    <tr class="table-header">
                                        <th>تصویر</th>
                                        <th>نام</th>
                                        <th>سن</th>
                                        <th>مشخصات وسیله</th>
                                        <th>امتیاز</th>
                                        <th>وضعیت</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @foreach($motors as $motor)
                                        <tr>
                                            <td>
                                                @if($motor->photo)
                                                    <img class="img-fluid img-product-list"
                                                         src="{{asset('images/deliveries/'.$motor->photo->path)}}">
                                                @else
                                                    <img class="img-fluid img-product-list"
                                                         src="https://placehold.co/60x60">
                                                @endif
                                            </td>
                                            <td>{{$motor->name}} {{$motor->family}}</td>
                                            <td>{{$motor->age}}</td>
                                            <td>
                                                @php
                                                    echo $motor->vehicle_details;
                                                @endphp
                                            </td>
                                            <td>{{$motor->rate}}</td>
                                            <td>
                                                @if($motor->ready==0)
                                                    <i class="badge badge-danger">مشغول</i>
                                                @else
                                                    <i class="badge badge-success">آماده به کار</i>
                                                @endif
                                            </td>
                                            <td>
                                                <a class="text-decoration-none" data-toggle="modal"
                                                   data-target="#deleteModal-{{$motor->id}}"
                                                   data-whatever="@getbotstrap">
                                                    <i class="badge badge-success py-2"
                                                       style="cursor: pointer">درخواست</i>
                                                </a>
                                                {{--MODAL--}}
                                                <div class="modal fade" id="deleteModal-{{$motor->id}}"
                                                     tabindex="-1"
                                                     role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog ">
                                                        <div class="modal-content">
                                                            <div class=" modal-header text-center ">
                                                                <h5 class="modal-title text-warning"
                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>
                                                            </div>
                                                            {!! Form::open(['method'=>'POST','action' =>['AdminDeliveryController@saveReq',$motor->id]]) !!}
                                                            <div class="modal-body ">
                                                                <input type="text" name="name"
                                                                       placeholder="نام محموله"
                                                                       class="form-control my-2">
                                                                <input type="text" name="qty" placeholder="تعداد"
                                                                       class="form-control my-2">
                                                                <input type="text" name="origin"
                                                                       placeholder="آدرس مبدا"
                                                                       class="form-control my-2">
                                                                <input type="text" name="destination"
                                                                       placeholder="آدرس مقصد"
                                                                       class="form-control my-2">
                                                            </div>
                                                            <div class="modal-footer ">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">انصراف
                                                                </button>
                                                                <button class="btn btn-success" type="submit">ثبت
                                                                </button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="alert alert-warning mt-1 text-center">
                                <h4 class="text-warning">
                                    هنوز هیچ پیک موتوری ثبت نشده است.
                                </h4>
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane fade p-3" id="nav-2" role="tabpanel" aria-labelledby="2">
                        <div class="row">
                            {!! Form::select('province',$arr,null,['class'=>'form-control w-25','id'=>'province-c']) !!}
                            <select class="form-control w-25 mx-2" name="city" id="city-c">

                            </select>
                            <button class="btn btn-success" id="search-c">جستجو</button>
                        </div>
                        <div class="text-center bg-white m-0 w-100">
                            <h6 class="bg-primary text-white py-2">لیست وانت بار</h6>
                        </div>
                        @if(count($cars)>0)
                            <div class="box-body">
                                <table id="table-c" class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                    <tr class="table-header">
                                        <th>تصویر</th>
                                        <th>نام</th>
                                        <th>سن</th>
                                        <th>مشخصات وسیله</th>
                                        <th>امتیاز</th>
                                        <th>وضعیت</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @foreach($cars as $car)
                                        <tr>
                                            <td>
                                                @if($car->photo)
                                                    <img class="img-fluid img-product-list"
                                                         src="{{asset('images/deliveries/'.$car->photo->path)}}">
                                                @else
                                                    <img class="img-fluid img-product-list"
                                                         src="https://placehold.co/60x60">
                                                @endif
                                            </td>
                                            <td>{{$car->name}} {{$car->family}}</td>
                                            <td>{{$car->age}}</td>
                                            <td>
                                                @php
                                                    echo $car->vehicle_details;
                                                @endphp
                                            </td>
                                            <td>{{$car->rate}}</td>
                                            <td>
                                                @if($car->ready==0)
                                                    <i class="badge badge-danger">مشغول</i>
                                                @else
                                                    <i class="badge badge-success">آماده به کار</i>
                                                @endif
                                            </td>
                                            <td>
                                                <a class="text-decoration-none" data-toggle="modal"
                                                   data-target="#deleteModal-{{$car->id}}"
                                                   data-whatever="@getbotstrap">
                                                    <i class="badge badge-success py-2"
                                                       style="cursor: pointer">درخواست</i>
                                                </a>
                                                {{--MODAL--}}
                                                <div class="modal fade" id="deleteModal-{{$car->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog ">
                                                        <div class="modal-content">
                                                            <div class=" modal-header text-center ">
                                                                <h5 class="modal-title text-warning"
                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>
                                                            </div>
                                                            {!! Form::open(['method'=>'POST','action' =>['AdminDeliveryController@saveReq',$car->id]]) !!}
                                                            <div class="modal-body ">
                                                                <input type="text" name="name"
                                                                       placeholder="نام محموله"
                                                                       class="form-control my-2">
                                                                <input type="text" name="qty" placeholder="تعداد"
                                                                       class="form-control my-2">
                                                                <input type="text" name="origin"
                                                                       placeholder="آدرس مبدا"
                                                                       class="form-control my-2">
                                                                <input type="text" name="destination"
                                                                       placeholder="آدرس مقصد"
                                                                       class="form-control my-2">
                                                            </div>
                                                            <div class="modal-footer ">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">انصراف
                                                                </button>
                                                                <button class="btn btn-success" type="submit">ثبت
                                                                </button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="alert alert-warning mt-1 text-center">
                                <h4 class="text-warning">
                                    هنوز هیچ وانت باری ثبت نشده است.
                                </h4>
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane fade p-3" id="nav-3" role="tabpanel" aria-labelledby="3">
                        <div class="row">
                            {!! Form::select('province',$arr,null,['class'=>'form-control w-25','id'=>'province-t']) !!}
                            <select class="form-control w-25 mx-2" name="city" id="city-t">

                            </select>
                            <button class="btn btn-success" id="search-t">جستجو</button>
                        </div>
                        <div class="text-center bg-white m-0 w-100">
                            <h6 class="bg-warning text-white py-2">لیست کامیون ها</h6>
                        </div>
                        @if(count($trucks)>0)
                            <div class="box-body">
                                <table id="table-t"  class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                    <tr class="table-header">
                                        <th>تصویر</th>
                                        <th>نام</th>
                                        <th>سن</th>
                                        <th>مشخصات وسیله</th>
                                        <th>امتیاز</th>
                                        <th>وضعیت</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @foreach($trucks as $truck)
                                        <tr>
                                            <td>
                                                @if($truck->photo)
                                                    <img class="img-fluid img-product-list"
                                                         src="{{asset('images/deliveries/'.$truck->photo->path)}}">
                                                @else
                                                    <img class="img-fluid img-product-list"
                                                         src="https://placehold.co/60x60">
                                                @endif
                                            </td>
                                            <td>{{$truck->name}}</td>
                                            <td>{{$truck->age}}</td>
                                            <td>
                                                @php
                                                    echo $truck->vehicle_details;
                                                @endphp
                                            </td>
                                            <td>{{$truck->rate}}</td>
                                            <td>
                                                @if($truck->ready==0)
                                                    <i class="badge badge-danger">مشغول</i>
                                                @else
                                                    <i class="badge badge-success">آماده به کار</i>
                                                @endif
                                            </td>
                                            <td>
                                                <a class="text-decoration-none" data-toggle="modal"
                                                   data-target="#deleteModal-{{$truck->id}}"
                                                   data-whatever="@getbotstrap">
                                                    <i class="badge badge-success py-2"
                                                       style="cursor: pointer">درخواست</i>
                                                </a>
                                                {{--MODAL--}}
                                                <div class="modal fade" id="deleteModal-{{$truck->id}}"
                                                     tabindex="-1"
                                                     role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog ">
                                                        <div class="modal-content">
                                                            <div class=" modal-header text-center ">
                                                                <h5 class="modal-title text-warning"
                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>
                                                            </div>
                                                            {!! Form::open(['method'=>'POST','action' =>['AdminDeliveryController@saveReq',$truck->id]]) !!}
                                                            <div class="modal-body ">
                                                                <input type="text" name="name"
                                                                       placeholder="نام محموله"
                                                                       class="form-control my-2">
                                                                <input type="text" name="qty" placeholder="تعداد"
                                                                       class="form-control my-2">
                                                                <input type="text" name="origin"
                                                                       placeholder="آدرس مبدا"
                                                                       class="form-control my-2">
                                                                <input type="text" name="destination"
                                                                       placeholder="آدرس مقصد"
                                                                       class="form-control my-2">
                                                            </div>
                                                            <div class="modal-footer ">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">انصراف
                                                                </button>
                                                                <button class="btn btn-success" type="submit">ثبت
                                                                </button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="alert alert-warning mt-1 text-center">
                                <h4 class="text-warning">
                                    هنوز هیچ کامیونی ثبت نشده است.
                                </h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')

            $('#province').on("change", function () {
                var province = $('#province').val()
                console.log(province)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            document.getElementById('city').value = re['id']
                        })
                        console.log(document.getElementById('city'))
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $('#province-c').on("change", function () {
                var province = $('#province-c').val()
                console.log(province)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city-c').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city-c').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            document.getElementById('city-c').value = re['id']
                        })
                        console.log(document.getElementById('city-c'))
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });
            $('#province-t').on("change", function () {
                var province = $('#province-t').val()
                console.log(province)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city-t').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city-t').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            document.getElementById('city-t').value = re['id']
                        })
                        console.log(document.getElementById('city-t'))
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $('#city').on("change", function () {

                console.log(this.value)
            });

            $('#search-m').on("click", function () {
                var province_id = $('#province').val()
                var city_id = $('#city').val()
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'search-delivery-m',
                        province_id: province_id,
                        city_id: city_id
                    },
                    datatype: 'jsons',
                    success: function (response) {

                        if(response['data']!=""){
                            response['data'].forEach(function (re) {
                                var table = document.getElementById('table-m')
                                table.innerHTML=""
                                table.innerHTML=' <tr class="table-header">\n' +
                                    '                                        <th>تصویر</th>\n' +
                                    '                                        <th>نام</th>\n' +
                                    '                                        <th>سن</th>\n' +
                                    '                                        <th>مشخصات وسیله</th>\n' +
                                    '                                        <th>امتیاز</th>\n' +
                                    '                                        <th>وضعیت</th>\n' +
                                    '                                        <th>امکانات</th>\n' +
                                    '                                    </tr>'
                                var row = table.insertRow(-1)
                                var td1 = row.insertCell(0)
                                var td2 = row.insertCell(1)
                                var td3 = row.insertCell(2)
                                var td4 = row.insertCell(3)
                                var td5 = row.insertCell(4)
                                var td6 = row.insertCell(5)
                                var td7 = row.insertCell(6)
                                if (re['photo']){
                                    var path = '/images/deliveries/' + re['photo']['path']
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="'+path+'">'
                                }
                                else
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="https://placehold.co/60x60">'
                                td2.innerHTML = re['name'] + " " + re['family']
                                td3.innerHTML = re['age']
                                td4.innerHTML = re['vehicle_details']
                                td5.innerHTML = re['rate']
                                if (re['ready'] == 0)
                                    td6.innerHTML = '<i class="badge badge-danger">مشغول</i>'
                                else
                                    td6.innerHTML = ' <i class="badge badge-success">آماده به کار</i>'
                                var sag='{{route('saveReq',1)}}';
                                sag.replace('1',re['id'])
                                td7.innerHTML=' <a class="text-decoration-none" data-toggle="modal"\n' +
                                    '                                                   data-target="#deleteModal-'+re['id']+'"\n' +
                                    '                                                   data-whatever="@getbotstrap">\n' +
                                    '                                                    <i class="badge badge-success py-2"\n' +
                                    '                                                       style="cursor: pointer">درخواست</i>\n' +
                                    '                                                </a>' +
                                    '<div class="modal fade" id="deleteModal-'+re['id']+'"' +
                                    '                                                     tabindex="-1"' +
                                    '                                                     role="dialog" aria-labelledby="exampleModalLabel"' +
                                    '                                                     aria-hidden="true">' +
                                    '                                                    <div class="modal-dialog ">' +
                                    '                                                        <div class="modal-content">' +
                                    '                                                            <div class=" modal-header text-center ">\n' +
                                    '                                                                <h5 class="modal-title text-warning"\n' +
                                    '                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>\n' +
                                    '                                                            </div>' +
                                    '<form method="post" action="'+sag+'">' +


                                    '                                                            <div class="modal-body ">\n' +
                                    '                                                                <input type="text" name="name"\n' +
                                    '                                                                       placeholder="نام محموله"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="qty" placeholder="تعداد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="origin"\n' +
                                    '                                                                       placeholder="آدرس مبدا"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="destination"\n' +
                                    '                                                                       placeholder="آدرس مقصد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                            </div>\n' +
                                    '                                                            <div class="modal-footer ">\n' +
                                    '                                                                <button type="button" class="btn btn-danger"\n' +
                                    '                                                                        data-dismiss="modal">انصراف\n' +
                                    '                                                                </button>\n' +
                                    '                                                                <button class="btn btn-success" type="submit">ثبت\n' +
                                    '                                                                </button>\n' +
                                    '                                                            </div>\n' +
                                    '</form>' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                </div>'

                            })
                        }else {
                            swal('موردی یافت نشد.')
                        }

                    },
                    error: function (response) {
                        console.log(response)
                    }
                })
            })
            $('#search-c').on("click", function () {
                var province_id = $('#province-c').val()
                var city_id = $('#city-c').val()
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'search-delivery-c',
                        province_id: province_id,
                        city_id: city_id
                    },
                    datatype: 'jsons',
                    success: function (response) {
                        if(response['data']!=""){
                            response['data'].forEach(function (re) {
                                var table = document.getElementById('table-c')
                                table.innerHTML=""
                                table.innerHTML=' <tr class="table-header">\n' +
                                    '                                        <th>تصویر</th>\n' +
                                    '                                        <th>نام</th>\n' +
                                    '                                        <th>سن</th>\n' +
                                    '                                        <th>مشخصات وسیله</th>\n' +
                                    '                                        <th>امتیاز</th>\n' +
                                    '                                        <th>وضعیت</th>\n' +
                                    '                                        <th>امکانات</th>\n' +
                                    '                                    </tr>'
                                var row = table.insertRow(-1)
                                var td1 = row.insertCell(0)
                                var td2 = row.insertCell(1)
                                var td3 = row.insertCell(2)
                                var td4 = row.insertCell(3)
                                var td5 = row.insertCell(4)
                                var td6 = row.insertCell(5)
                                var td7 = row.insertCell(6)
                                if (re['photo']){
                                    var path = '/images/deliveries/' + re['photo']['path']
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="'+path+'">'
                                }
                                else
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="https://placehold.co/60x60">'
                                td2.innerHTML = re['name'] + " " + re['family']
                                td3.innerHTML = re['age']
                                td4.innerHTML = re['vehicle_details']
                                td5.innerHTML = re['rate']
                                if (re['ready'] == 0)
                                    td6.innerHTML = '<i class="badge badge-danger">مشغول</i>'
                                else
                                    td6.innerHTML = ' <i class="badge badge-success">آماده به کار</i>'
                                var sag='{{route('saveReq',1)}}';
                                sag.replace('1',re['id'])
                                td7.innerHTML=' <a class="text-decoration-none" data-toggle="modal"\n' +
                                    '                                                   data-target="#deleteModal-'+re['id']+'"\n' +
                                    '                                                   data-whatever="@getbotstrap">\n' +
                                    '                                                    <i class="badge badge-success py-2"\n' +
                                    '                                                       style="cursor: pointer">درخواست</i>\n' +
                                    '                                                </a>' +
                                    '<div class="modal fade" id="deleteModal-'+re['id']+'"' +
                                    '                                                     tabindex="-1"' +
                                    '                                                     role="dialog" aria-labelledby="exampleModalLabel"' +
                                    '                                                     aria-hidden="true">' +
                                    '                                                    <div class="modal-dialog ">' +
                                    '                                                        <div class="modal-content">' +
                                    '                                                            <div class=" modal-header text-center ">\n' +
                                    '                                                                <h5 class="modal-title text-warning"\n' +
                                    '                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>\n' +
                                    '                                                            </div>' +
                                    '<form method="post" action="'+sag+'">' +


                                    '                                                            <div class="modal-body ">\n' +
                                    '                                                                <input type="text" name="name"\n' +
                                    '                                                                       placeholder="نام محموله"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="qty" placeholder="تعداد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="origin"\n' +
                                    '                                                                       placeholder="آدرس مبدا"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="destination"\n' +
                                    '                                                                       placeholder="آدرس مقصد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                            </div>\n' +
                                    '                                                            <div class="modal-footer ">\n' +
                                    '                                                                <button type="button" class="btn btn-danger"\n' +
                                    '                                                                        data-dismiss="modal">انصراف\n' +
                                    '                                                                </button>\n' +
                                    '                                                                <button class="btn btn-success" type="submit">ثبت\n' +
                                    '                                                                </button>\n' +
                                    '                                                            </div>\n' +
                                    '</form>' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                </div>'

                            })
                        }else {
                            swal('موردی یافت نشد.')
                        }

                    },
                    error: function (response) {
                        console.log(response)
                    }
                })
            })
            $('#search-t').on("click", function () {
                var province_id = $('#province-t').val()
                var city_id = $('#city-t').val()
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'search-delivery-t',
                        province_id: province_id,
                        city_id: city_id
                    },
                    datatype: 'jsons',
                    success: function (response) {
                        if(response['data']!=""){
                            response['data'].forEach(function (re) {
                                var table = document.getElementById('table-t')
                                table.innerHTML=""
                                table.innerHTML=' <tr class="table-header">\n' +
                                    '                                        <th>تصویر</th>\n' +
                                    '                                        <th>نام</th>\n' +
                                    '                                        <th>سن</th>\n' +
                                    '                                        <th>مشخصات وسیله</th>\n' +
                                    '                                        <th>امتیاز</th>\n' +
                                    '                                        <th>وضعیت</th>\n' +
                                    '                                        <th>امکانات</th>\n' +
                                    '                                    </tr>'
                                var row = table.insertRow(-1)
                                var td1 = row.insertCell(0)
                                var td2 = row.insertCell(1)
                                var td3 = row.insertCell(2)
                                var td4 = row.insertCell(3)
                                var td5 = row.insertCell(4)
                                var td6 = row.insertCell(5)
                                var td7 = row.insertCell(6)
                                if (re['photo']){
                                    var path = '/images/deliveries/' + re['photo']['path']
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="'+path+'">'
                                }
                                else
                                    td1.innerHTML = '<img class="img-fluid img-product-list" src="https://placehold.co/60x60">'
                                td2.innerHTML = re['name'] + " " + re['family']
                                td3.innerHTML = re['age']
                                td4.innerHTML = re['vehicle_details']
                                td5.innerHTML = re['rate']
                                if (re['ready'] == 0)
                                    td6.innerHTML = '<i class="badge badge-danger">مشغول</i>'
                                else
                                    td6.innerHTML = ' <i class="badge badge-success">آماده به کار</i>'
                                var sag='{{route('saveReq',1)}}';
                                sag.replace('1',re['id'])
                                td7.innerHTML=' <a class="text-decoration-none" data-toggle="modal"\n' +
                                    '                                                   data-target="#deleteModal-'+re['id']+'"\n' +
                                    '                                                   data-whatever="@getbotstrap">\n' +
                                    '                                                    <i class="badge badge-success py-2"\n' +
                                    '                                                       style="cursor: pointer">درخواست</i>\n' +
                                    '                                                </a>' +
                                    '<div class="modal fade" id="deleteModal-'+re['id']+'"' +
                                    '                                                     tabindex="-1"' +
                                    '                                                     role="dialog" aria-labelledby="exampleModalLabel"' +
                                    '                                                     aria-hidden="true">' +
                                    '                                                    <div class="modal-dialog ">' +
                                    '                                                        <div class="modal-content">' +
                                    '                                                            <div class=" modal-header text-center ">\n' +
                                    '                                                                <h5 class="modal-title text-warning"\n' +
                                    '                                                                    id="exampleModalLabel">مشخصات محموله پستی</h5>\n' +
                                    '                                                            </div>' +
                                    '<form method="post" action="'+sag+'">' +


                                    '                                                            <div class="modal-body ">\n' +
                                    '                                                                <input type="text" name="name"\n' +
                                    '                                                                       placeholder="نام محموله"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="qty" placeholder="تعداد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="origin"\n' +
                                    '                                                                       placeholder="آدرس مبدا"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                                <input type="text" name="destination"\n' +
                                    '                                                                       placeholder="آدرس مقصد"\n' +
                                    '                                                                       class="form-control my-2">\n' +
                                    '                                                            </div>\n' +
                                    '                                                            <div class="modal-footer ">\n' +
                                    '                                                                <button type="button" class="btn btn-danger"\n' +
                                    '                                                                        data-dismiss="modal">انصراف\n' +
                                    '                                                                </button>\n' +
                                    '                                                                <button class="btn btn-success" type="submit">ثبت\n' +
                                    '                                                                </button>\n' +
                                    '                                                            </div>\n' +
                                    '</form>' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                </div>'

                            })
                        }else {
                            swal('موردی یافت نشد.')
                        }

                    },
                    error: function (response) {
                        console.log(response)
                    }
                })
            })
        })
    </script>
@endsection


