@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">جزئیات فاکتور</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body  border-teacher-index px-2">
                <div class="row w-100 py-3  text-center mx-2 px-5">
                    <div class="col-12 col-md-2">
                        @if($factor->customer->photo)
                            <img src="{{asset('images/'.$factor->customer->photo->path)}}" alt="profile"
                                 class="img-fluid rounded-circle img-prof-reg">
                        @else
                            <img src="{{asset('img/user-avatar.svg')}}" alt="profile"
                                 class="img-fluid rounded-circle img-prof-reg">
                        @endif

                    </div>
                    <div class="col-12 col-md-6 text-right ">
                        <h4 class="my-1">{{$factor->customer->name}} {{$factor->customer->family}}</h4>
                        <h6 class="my-2 text-muted">
                            {{$factor->customer->mobile}} <i class="fa fa-mobile-alt mx-3"></i>
                        </h6>
                        <h6 class="my-2 text-muted">
                            {{$factor->customer->phone}} <i class="fa fa-phone-square-alt mx-3"></i>
                        </h6>
                        <h6 class="text-muted my-1 text-right">
                            {{$factor->customer->address}}
                        </h6>
                    </div>
                    <div class="col-12 col-md-4">
                        <h5 class="text-left ">
                            تاریخ:
                            <span class="badge badge-light text-left">{{verta($factor->f_date)->formatdate()}}</span>
                        </h5>
                        <h5 class="text-left">
                            شماره فاکتور:
                            <span class="badge badge-light text-left" id="factor_id">{{$factor->id}}</span>
                        </h5>
                    </div>
                </div>
                <div class="row w-100 py-3 d-flex justify-content-center ">
                    <div class="col-12 ">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                            <tr class="table-header">
                                <th>ردیف</th>
                                <th>تصویر</th>
                                <th>محصول</th>
                                <th>تعداد</th>
                                <th>قیمت واحد (تومان)</th>
                                <th>قیمت کل (تومان)</th>
                            </tr>
                            @php($count=1)
                            @php($total=0)
                            @foreach($factor->factor_pro as $pro)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>
                                        @if($pro->product->photo==null)
                                            <img src="{{asset('img/default.jpg')}}" alt="Product"
                                                 class="img-fluid img-product-list">
                                        @else
                                            <img src="{{asset('images/products/'.$pro->product->photo->path)}}" alt="Product"
                                                 class="img-fluid img-product-list">
                                        @endif
                                    </td>
                                    <td>{{$pro->product->name}}</td>
                                    <td>{{$pro->qty}}</td>
                                    <td>{{$pro->price}}</td>
                                    <td>{{$pro->price*$pro->qty}}</td>
                                </tr>
                                @php($total+=($pro->price*$pro->qty))
                                @php($count++)
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="row w-100  d-flex  justify-content-center">
                    <div class="col-12 col-md-6 text-center">
                        <a class="btn btn-success text-decoration-none" data-toggle="modal"
                           data-target="#Modal" data-whatever="@getbotstrap">
                            بستن سفارش
                        </a>
                        <a class="btn btn-info text-decoration-none" href="{{route('deliveryReq')}}">
                            درخواست پیک
                        </a>
                        {{--MODAL--}}
                        <div class="modal fade" id="Modal" tabindex="-1"
                             role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <div class=" modal-header ">
                                        <h5 class="modal-title text-warning" id="exampleModalLabel">
                                            ثبت اطلاعات پیگیری</h5>
                                    </div>
                                    <div class="modal-body">
                                        <label for="tracking">شماره پیگیری دریافتی از پست یا شماره تلفن پیک  را وارد کنید</label>
                                        <input id="tracking" type="text" class="form-control" placeholder="پیگیری">
                                        <input id="delivery_mobile" type="text" class="form-control mt-3" placeholder="موبایل">
                                    </div>

                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">بستن
                                        </button>
                                        <button class="btn btn-success" type="submit" id="set-status">ثبت وضعیت</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 text-center">
                        <h3 class="text-center">
                            مبلغ کل فاکتور:
                            <span class="badge badge-primary p-2 ">{{number_format($total)}} تومان</span>
                        </h3>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')
            $('#set-status').click(function () {
                var tracking=document.getElementById("tracking").value
                var mobile=document.getElementById("delivery_mobile").value
                var factor=document.getElementById("factor_id").innerText
                if (tracking=="" && mobile==""){
                    swal('شماره پیگیری پست یا شماره تلفن پیک را وارد کنید')
                }else {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {// change data to this object
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'set-factor-status',
                            tracking: tracking,
                            mobile:mobile,
                            factor:factor
                        },
                        dataType: 'json',
                        success: function (response) {
                            window.location.href = "{{ route('onlineOrders')}}";
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }
            })

        });
    </script>
@endsection


