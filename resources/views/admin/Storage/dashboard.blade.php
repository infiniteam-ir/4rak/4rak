@extends('admin.layouts.newTheme.master')


@section('main')
    <div class="row px-2 my-3 d-flex flex-nowrap overflow-auto">
        <div class="col mx-1">
            <a href="{{route('storage.index')}}">
                <div class="btn btn-purple  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/storage-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white">مدیریت انبارها</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('storage.create')}}">
                <div class="btn btn-success h-100 text-center " style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/add-storage-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white"> افزودن انبار</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('ioProduct')}}">
                    <div class="btn btn-info  text-center h-100" style="border-radius: 10px">
                        <div class="p-4">
                            <img src="{{asset('img/io-white.svg')}}" alt="4rak" class="img-fluid rounded">
                            <h6 class="text-white"> ورود/خروج کالا</h6>
                        </div>

                    </div>
            </a>
        </div>
        <div class="col mx-1">
            <a href="{{route('storageReport')}}">
                    <div class="btn btn-warning  text-center h-100" style="border-radius: 10px">
                        <div class="p-4">
                            <img src="{{asset('img/report-white.svg')}}" alt="4rak" class="img-fluid rounded">
                            <h6 class="text-white"> گزارش انبار</h6>
                        </div>
                    </div>
            </a>
        </div>


        <div class="col-lg-3 col-md-6">
            <div class="card-box h-100">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">انبارهای شما</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$storages_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$storages_qty}} </h2>
                        <p class="text-muted"> انبار</p>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">نمودار موجودی کالاهای شما در هر انبار</h4>
                <canvas id="storages" width="500px" height="300" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            let storage_qty={{$storages_qty}}
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                // url: $('.ajax.d-none').attr('id'),
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-storage-detail-for-chart',
                },
                dataType: 'json',
                success: function (response) {
                    let colors=new Array();
                    for (let i=0;i<storage_qty;i++){
                        let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
                        colors.push(randomColor)
                    }
                    let max_qty=parseInt(response['max_qty'])
                    new Chart(document.getElementById("storages"), {
                        type: 'pie',
                        data: {
                            labels: response['storages'],
                            datasets: [
                                {
                                    backgroundColor: colors,
                                    data: response['products']
                                }
                            ]
                        },
                    });

                },
                error: function (response) {
                    console.log('error')
                }
            });
        })
    </script>
@endsection

