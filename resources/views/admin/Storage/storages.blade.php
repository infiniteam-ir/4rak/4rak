@extends('admin.layouts.newTheme.master')
@section('header')
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">مدیریت انبارها</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1 text-center">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <div class="nav  d-flex justify-content-center ">
                            <ul class=" d-flex justify-content-between list-unstyled w-100 user-list-menu p-2 checkout-ul ">
                                <li class="blog-list-item ">
                                     <a href="{{route('storage.create')}}" class="btn btn-outline-success">
                                         <i class="fa fa-plus"></i>
                                         افزودن
                                     </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if(count($storages)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2" id="table">
                            <tr class="table-header">
                                <th>نام</th>
                                <th>کاربر</th>
                                <th>فروشگاه</th>
                                <th>آدرس</th>
                                <th>امکانات</th>
                            </tr>

                            @foreach($storages as $storage )
                                <tr>
                                    <td>{{$storage->name}}</td>
                                    <td>{{$storage->user->name}} {{$storage->user->family}}</td>
                                    <td>{{$storage->shop->name}}</td>
                                    <td>{{$storage->address}}</td>
                                    <td>
                                        <div class="row w-100 justify-content-center">
                                            <div class="col-12 col-md-4 ">
                                                <a class="text-primary" href="{{route('storage.edit',$storage->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <a href="javascript:void(0)"  class="delete btn delete text-danger p-0" id="{{$storage->id}}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$storages->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            انباری جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table')
            $(document).on("click", '.delete', function () {
                var id = this.id
                var rowindex = $(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این انبار رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: id,
                                do: 'delete-storage',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                if (response == 'forbidden') {
                                    swal("خطا!", "این انبار دارای محصول است و شما مجاز به حذف آن نیستید.", "error")
                                } else if (response == 'success') {
                                    var table = document.getElementById('table')
                                    table.deleteRow(rowindex)
                                    swal("موفق", "انبار ارسال حذف شد.", "success")
                                }
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection
