@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">ورود/خروج کالا</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-1">
                    <h4 class="text-success text-center">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger text-center mt-1">
                    <h4 class="text-danger text-center">
                        {{Session::pull('error')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(count($storages)>0)
                <div class="container">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="import" data-toggle="tab" href="#nav-im"
                               role="tab"
                               aria-controls="nav-im" aria-selected="true">ورود کالا</a>
                            <a class="nav-item nav-link" id="export" data-toggle="tab" href="#nav-ex" role="tab"
                               aria-controls="nav-ex" aria-selected="false">خروج کالا</a>
                            <a class="nav-item nav-link" id="storage" data-toggle="tab" href="#nav-storage" role="tab"
                               aria-controls="nav-ex" aria-selected="false">انبار به انبار</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active p-3" id="nav-im" role="tabpanel"
                             aria-labelledby="import">
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-success text-white py-2">ورود کالا</h6>
                            </div>
                            <div class="box-body">
                                {!! Form::open(['method'=>'POST','action'=>'AdminStorageController@importProduct']) !!}
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                    <tr class="table-header">
                                        <th> عنوان کالا</th>
                                        <th>تعداد</th>
                                        <th>انبار</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="product_name" class="form-control" id="ajaxqty">
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}">
                                                        {{$product->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            {!! Form::number('qty',null,['class'=>'form-control','id'=>'qty']) !!}
                                        </td>
                                        <td>
                                            <select name="storage_name" class="form-control">
                                                @foreach($storages as $storage)
                                                    <option value="{{$storage->name}}">
                                                        {{$storage->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                                <button type="submit" class="btn btn-success">ثبت</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="tab-pane fade p-3 " id="nav-ex" role="tabpanel" aria-labelledby="export">
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-danger text-white py-2">خروج کالا</h6>
                            </div>
                            <div class="box-body ">
                                {!! Form::open(['method'=>'POST','action'=>'AdminStorageController@exportProduct']) !!}
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                    <tr class="table-header">
                                        <th>انبار</th>
                                        <th> عنوان کالا</th>
                                        <th>تعداد</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="storage_name" id="out_storage_id" class="form-control">
                                                <option class="form-control" value="none">
                                                    انتخاب کنید
                                                </option>
                                                @foreach($storages as $storage)
                                                    <option value="{{$storage->id}}">
                                                        {{$storage->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="product_name" id="products" class="form-control">
                                                <option value="">
                                                    ابتدا انبار را انتخاب کنید
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            {!! Form::number('qty',null,['class'=>'form-control','id'=>'pro_qty_storage']) !!}
                                        </td>
                                    </tr>
                                </table>
                                <button type="submit" class="btn btn-success">ثبت</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="tab-pane fade p-3 " id="nav-storage" role="tabpanel" aria-labelledby="storage">
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-primary text-white py-2">انتقال کالا از انبار به انبار</h6>
                            </div>

                            <div class="box-body ">
                                @if(count($storages)>1)
                                    {{--                                {!! Form::open(['method'=>'POST','action'=>'AdminStorageController@exportProduct']) !!}--}}
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                        <tr class="table-header">
                                            <th>انبار مبدا</th>
                                            <th>انبار مقصد</th>
                                            <th> کالا</th>
                                            <th>تعداد</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="origin" id="origin_id" class="form-control">
                                                    <option class="form-control" value="none">
                                                        انتخاب کنید
                                                    </option>
                                                    @foreach($storages as $storage)
                                                        <option value="{{$storage->id}}">
                                                            {{$storage->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select name="destination" id="destination_id" class="form-control">
                                                    <option class="form-control" value="none">
                                                        ابتدا انبار مبدا را انتخاب کنید
                                                    </option>

                                                </select>
                                            </td>
                                            <td>
                                                <select name="product_name" id="product-list" class="form-control">
                                                    <option value="none">
                                                        ابتدا انبار را انتخاب کنید
                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" id="qty-pivot" class="form-control">
                                            </td>
                                        </tr>
                                    </table>
                                    <button id="transfer" class="btn btn-success">ثبت</button>
                                    {{--                                {!! Form::close() !!}--}}

                                @else
                                    <div class="alert alert-danger mt-1 text-center">
                                        <h4 class="text-danger">
                                            شما تنها یک انبار دارید !
                                        </h4>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="box-body">
                    <div class="alert alert-danger mt-1 text-center">
                        <h4 class="text-danger">
                            شما هنوز انباری ثبت نکرده اید.
                        </h4>
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')
            var storage_id = $('#out_storage_id').val()
            $('#out_storage_id').on("change", function () {
                storage_id = this.value
                console.log(storage_id);
                var list = document.getElementById('products')
                if (storage_id != 'none') {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-product-from-storage',
                            id: storage_id,
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response == 0) {
                                list.innerHTML = ''
                                swal('محصولی در این انبار وجود ندارد!')
                            }
                            else {
                                response.forEach(function (re) {
                                    list.innerHTML += '<option class="form-control" value="' + re['id'] + '">' + re['name'] + '</option>'
                                })
                            }
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }

            })

            var getqty = document.querySelector('#ajaxqty')
            getqty.addEventListener("change", function () {
                var id = getqty.value
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-product',
                        id: id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#qty').val(response['quantity'])
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });

            });

            var pros_in_storage = document.querySelector('#products')
            pros_in_storage.addEventListener("change", function () {
                var id = pros_in_storage.value
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{route('webAjax')}}',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-products-in-storage',
                        id: id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#pro_qty_storage').val(response['quantity'])
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });

            });

            var origin_storage_id
            var dest_storage_id
            var product_id
            var qty=document.querySelector('#qty-pivot')

            var origin_storage = document.querySelector('#origin_id')
            var dest_storage = document.querySelector('#destination_id')
            var products = document.getElementById('product-list')

            origin_storage.addEventListener("change", function () {
                origin_storage_id = this.value
                if (origin_storage_id != 'none') {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-destination-storage',
                            id: origin_storage_id,
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            dest_storage.innerHTML = ""
                            dest_storage.innerHTML = '<option value="none" class="form-control">انتخاب کنید</option>'
                            response['storages'].forEach(function (re) {
                                dest_storage.innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            })
                            if (response['products'] != "") {
                                products.innerHTML='<option value="none" class="form-control">انتخاب کالا</option>'
                                response['products'].forEach(function (re) {
                                    products.innerHTML += '<option value="' + re['product']['id'] + '" class="form-control">' + re['product']['name'] + '</option>'
                                })
                            } else {
                                swal('کالایی در انبار مبدا وجود ندارد !')
                            }
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }

            })
            dest_storage.addEventListener("change", function () {
                dest_storage_id = this.value
            })

            products.addEventListener("change", function () {
                product_id=this.value
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-pro-from-storage-pivot',
                        id: origin_storage_id,
                        product_id:this.value,
                        storage_id:origin_storage_id
                    },
                    dataType: 'json',
                    success: function (response) {
                        qty.value=response['quantity']
                        qty.innerHTML=response['quantity']
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('#transfer').on("click",function () {

                if(origin_storage.value!= 'none' && dest_storage.value!='none' && products.value!=='none'&& qty.value!="") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'transfer-strorage-to-storage',
                            origin_storage_id: origin_storage_id,
                            product_id:products.value,
                            dest_storage_id:dest_storage_id,
                            qty:qty.value
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            swal('انتقال کالاها انجام شد.')
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }else{
                    swal('لطفا تمام فیلد ها را پر کنید')
                }
            })

        })
    </script>
@endsection


