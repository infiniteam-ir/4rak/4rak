@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">گزارش انبار</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1 text-center">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="box-body">
                    <div class="container">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                @php($flag=true)
                                @foreach($storages as $storage)
                                    @if($flag)
                                        <a class="nav-item nav-link active " id="my-{{$storage->id}}" data-toggle="tab"
                                           href="#nav-my-{{$storage->id}}"
                                           role="tab"
                                           aria-controls="nav-my-{{$storage->id}}"
                                           aria-selected="true">{{$storage->name}}</a>
                                    @else
                                        <a class="nav-item nav-link  " id="my-{{$storage->id}}" data-toggle="tab"
                                           href="#nav-my-{{$storage->id}}"
                                           role="tab"
                                           aria-controls="nav-my-{{$storage->id}}"
                                           aria-selected="true">{{$storage->name}}</a>
                                    @endif
                                    @php($flag=false)
                                @endforeach
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            @php($flag2=true)
                            @foreach($storages as $storage)
                                @if($flag2)
                                    <div class="tab-pane fade show active p-3" id="nav-my-{{$storage->id}}"
                                         role="tabpanel"
                                         aria-labelledby="my-{{$storage->id}}">
                                        <div class= "alert alert-warning  w-100" >
                                            <h6 class=" py-2">لیست کالاهای موجود در انبار: {{$storage->name}} </h6>
                                        </div>
                                        <div class="box-body">
                                            @if($storage->pivots)
                                                <table class="table table-sm table-info table-bordered table-responsive-md table-hover table-striped table-header   ">
                                                    <tr class="">
                                                        <th>نام محصول</th>
                                                        <th>تعداد</th>
                                                    </tr>
                                                    @foreach($storage->pivots as $pivot)
                                                        <tr>
                                                            <td>{{$pivot->product->name}}</td>
                                                            <td>{{$pivot->quantity}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            @else
                                                @dd($storage->pivots)
                                                <h6 class="alert alert-success  py-2">
                                                    انبار خالی است.
                                                </h6>
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <div class="tab-pane fade show  p-3" id="nav-my-{{$storage->id}}"
                                         role="tabpanel"
                                         aria-labelledby="my-{{$storage->id}}">
                                        <div class= "alert alert-warning  w-100" >
                                            <h6 class=" py-2">لیست کالاهای موجود در انبار: {{$storage->name}} </h6>
                                        </div>
                                        <div class="box-body">
                                            @if($storage->pivots)
                                                <table class="table table-sm table-info table-bordered table-responsive-md table-hover table-striped table-header   ">
                                                    <tr class="">
                                                        <th>نام محصول</th>
                                                        <th>تعداد</th>
                                                    </tr>
                                                    @foreach($storage->pivots as $pivot)
                                                        <tr>
                                                            <td>{{$pivot->product->name}}</td>
                                                            <td>{{$pivot->quantity}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            @else
                                                @dd($storage->pivots)
                                                <h6 class="alert alert-success  py-2">
                                                    انبار خالی است.
                                                </h6>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                @php($flag2=false)
                            @endforeach
                        </div>
                    </div>
                </div>

            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


