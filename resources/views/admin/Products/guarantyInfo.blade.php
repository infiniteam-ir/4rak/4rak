@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">اطلاعات گارانتی</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="box-body  border-teacher-index px-2">
                @if($guaranty!=null)
                    <div class="row w-100 py-3  text-center mx-2 px-5">
                        <div class="col-12 col-md-3">
                            @if($guaranty->product->photo)
                                <img src="{{asset('images/products/'.$guaranty->product->photo->path)}}"
                                     alt="image" class="img-fluid img-prof-reg rounded">
                            @else
                                <img src="{{asset('img/default.jpg')}}"
                                     alt="Flash Memory" class="img-fluid img-prof-reg rounded ">
                            @endif
                        </div>
                        <div class="col-12 col-md-8 ">
                            <h4 class="text-right">
                                <span class="text-muted text-light">محصول:</span>
                                {{$guaranty->product->name}}
                            </h4>
                            <h4 class=" mt-5 text-right">
                                <span class="text-muted text-light">دسته:</span>
                                {{$guaranty->product->category->title}}
                            </h4>
                            <h4 class="text-right">
                                <span class="text-muted text-light">قیمت:</span>
                                <span class="badge badge-success">
                                    @php
                                        $price=$guaranty->price/$guaranty->qty;
                                        echo number_format($price);
                                    @endphp
                                    تومان
                                </span>
                            </h4>
                        </div>
                    </div>
                    <hr class="bg-primary">
                    <div class="row w-100 py-3 ">
                        <div class="col-12 col-md-6">خریدار: <span class="text-bold">
                                {{$guaranty->factor->customer->name}} {{$guaranty->factor->customer->family}}
                            </span>
                        </div>
                        <div class="col-12 col-md-6">تاریخ فروش: <span class="text-bold">
                                {{$guaranty->factor->f_date}}
                            </span>
                        </div>
                    </div>
                    <div class="row w-100 py-3">
                        <div class="col-12 col-md-6">مدت گارانتی:
                            <span class=" badge badge-warning text-bold">{{$guaranty->product->guaranty->duration}}</span>
                            @switch($guaranty->product->guaranty->duration_unit)
                                @case('day')
                                <span class=" badge badge-warning text-bold">روز</span>
                                @break
                                @case('month')
                                <span class=" badge badge-warning text-bold">ماه</span>
                                @break
                                @case('year')
                                <span class=" badge badge-warning text-bold">سال</span>
                                @break
                            @endswitch
                        </div>
                        <div class="col-12 col-md-6">شروع از:
                            @if($guaranty->product->guaranty->start=='sell_date')
                                <span class=" badge badge-warning text-bold p-2">تاریخ فروش</span>
                            @else
                                <span class=" badge badge-warning text-bold p-2">تاریخ خرید</span>
                            @endif
                        </div>
                    </div>
                    <hr class="bg-primary">
                    <div class="row  py-3 d-flex  justify-content-center">
                        <div class="col-6">
                            <table class="table  table-bordered table-responsive-md">
                                <tr class="table-header">
                                    <th>فروشنده</th>
                                    <th>تاریخ خرید از فروشنده</th>
                                </tr>
                                @foreach($guaranty->product->in_products as $in_factor)
                                    <tr>
                                        <td>
                                            {{$in_factor->factor->provider->name}}
                                            {{$in_factor->factor->provider->family}}
                                        </td>
                                        <td>
                                            {{$in_factor->factor->f_date}}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="row w-100 py-3 d-flex  justify-content-center">
                        <div class="col-12 col-md-6">
                            <button class="btn btn-primary" id="close" data-toggle="modal" data-target="#accept"
                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                قبول گارانتی
                            </button>
                            <div class="modal fade" id="accept" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">انتخاب پذیرنده گارانتی</h5>
                                        </div>
                                        {!! Form::open(['method'=>'POST','action'=>'AdminProductController@accept_guaranty']) !!}
                                        <div class="modal-body">
                                            <h4>
                                                از لیست زیر شرکت پذیرنده گارانتی را انتخاب کنید.
                                            </h4>
                                            <select name="provider" id="" class="form-control my-3">
                                                @foreach($guaranty->product->in_products as $in_factor)
                                                    <option value="{{$in_factor->factor->provider_id}}">
                                                        {{$in_factor->factor->provider->name}}
                                                        {{$in_factor->factor->provider->family}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input name="id" type="text" class="d-none" value="{{$guaranty->id}}">

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن
                                            </button>
                                            {!! Form::submit('تایید',['class'=>'form-control btn btn-success']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @else
                    <div class="alert alert-danger m-3">
                        <h3 class="text-muted">
                            این کالا دارای گارانتی نمیباشد.
                        </h3>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection


