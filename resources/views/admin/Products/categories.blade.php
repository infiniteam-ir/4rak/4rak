@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">گروه های کالا</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
                @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        {{Session::pull('error')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <a href="{{route('category.create')}}" class="btn btn-outline-success rounded">
                            <i class="fa fa-plus"></i>
                            افزودن گروه کالا
                        </a>
                        <input type="text" name="cat_id" id="cat" class="cat d-none">
                        <div class="autocomplete" style="width:300px;">
                            {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'cats']) !!}
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2 "
                           id="table">
                        <tr class="table-header">
                            <th>تصویر</th>
                            <th>عنوان</th>
                            <th>امکانات</th>
                        </tr>
                        @foreach($categories as $cat )
                            <tr>
                                <td>
                                    @if($cat->photo)
                                        <img class="img-product-list" src="{{asset('images/cats/'.$cat->photo->path)}}">
                                    @else
                                        <img class="img-product-list" src="{{asset('img/default-2.svg')}}">
                                    @endif
                                </td>
                                <td>{{$cat->title}}</td>
                                <td>
                                    <div class="row w-100 justify-content-center">
                                        <div class="col-12 my-1 ">
                                            <a class=" btn btn-primary" href="{{route('category.edit',$cat->id)}}">
                                                <i class="fa fa-edit"></i>
                                                ویرایش
                                            </a>
                                        </div>
                                        <div class="col-12 my-1">
                                            <button id="{{$cat->id}}"  class="delete btn btn-danger ">
                                                <i class="fa fa-trash"></i>
                                                حذف
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="row d-flex justify-content-center">{{$categories->links()}}</div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection

@section('script')

    <script>

        $(document).ready(function () {
            var table = document.getElementById('table')
            $("#cats").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>تصویر</th>\n' +
                    '                            <th>نام گروه</th>\n' +
                    '                            <th>امکانات</th>\n' +
                    '                        </tr>'

                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            cat_name: $('#cats').val(),
                            do: 'search-category',
                        },
                        dataType: 'json',
                        success: function (response) {
                            data = response
                            // autocomplete(document.getElementById("cats"), data);
                            response['data'].forEach(function (re) {
                                var id=re['id']
                                var routee='{{route('category.edit','*')}}'
                                var pathedit=routee.replace('*',re['id'])

                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)

                                if (re['photo'] != null) {
                                    var path = '../images/cats/' + re['photo']['path']
                                    td0.innerHTML = '<img class="img-product-list" src="' + path + '">'
                                }
                                else
                                    td0.innerHTML = '<img class="img-product-list" src="{{asset('img/default-2.svg')}}">'
                                td1.innerHTML = re['title']
                                td2.innerHTML = '<div class="row w-100 justify-content-center">\n' +
                                    '                                        <div class="col-12 col-md-3 ">\n' +
                                    '                                            <a class=" btn btn-primary" href="'+pathedit+'">\n' +
                                    '                                                <i class="fa fa-edit"></i>\n' +
                                    '                                                ویرایش\n' +
                                    '                                            </a>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="col-12 col-md-3">\n' +
                                    '                                            <button type="button" id="'+id+'" class="delete btn btn-danger ">\n' +
                                    '                                                <i class="fa fa-trash"></i>\n' +
                                    '                                                حذف\n' +
                                    '                                            </button>\n' +
                                    '\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>'
                            })
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
            });


            $(document).on("click", '.delete',function () {
                var cat_id=this.id
                var rowindex=$(this).parent().parent().parent().parent().index()

                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این گروه رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                cat_id: cat_id,
                                do: 'delete-category',
                            },
                            dataType: 'json',
                            success: function (response) {
                                if(response=='forbidden'){
                                    swal("خطا!", "این گروه دارای محصول است و شما مجاز به حذف آن نیستید.", "error")
                                }else if(response=='success'){
                                    var table=document.getElementById('table')
                                    table.deleteRow(rowindex)
                                    swal("موفق", "گروه کالا حذف شد.", "success")
                                }
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>

@endsection
