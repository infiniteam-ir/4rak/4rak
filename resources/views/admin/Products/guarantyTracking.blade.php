@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">پیگیری گارانتی</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1 text-center">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        {{Session('error')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                    <div class="container">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="new" data-toggle="tab" href="#nav-new" role="tab"
                                   aria-controls="nav-new" aria-selected="true">
                                    جدید
                                </a>
                                <a class="nav-item nav-link" id="history" data-toggle="tab" href="#nav-history"
                                   role="tab"
                                   aria-controls="nav-factors" aria-selected="false">بایگانی</a>
                            </div>
                        </nav>
                        <div class="tab-content pt-0" id="nav-tabContent">
                            <div class="tab-pane fade show active p-3" id="nav-new" role="tabpanel" aria-labelledby="new">
                                <div class="box-body">
                                    <div class="row my-3 w-100">
                                        <div class="col-12 col-md-6">
                                            <h6 class="text-muted">جستجو با نام مشتری</h6>
                                            <input type="text" name="customer_id" id="customer" class="d-none">
                                            <div class="autocomplete" style="width:300px;">
                                                {!! Form::text('customer',null,['class'=>'form-control','id'=>'customers']) !!}
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <h6 class="text-muted">جستجو با نام محصول</h6>
                                            <input type="text" name="product_id" id="product" class="d-none">
                                            <div class="autocomplete" style="width:300px;">
                                                {!! Form::text('product',null,['class'=>'form-control','id'=>'products']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <table id="table-new" class="table table-bordered table-responsive-md table-hover table-striped table-header mt-4  ">
                                        <tr class="table-header">
                                            <th>مشتری</th>
                                            <th>فروشنده</th>
                                            <th>تاریخ دریافت</th>
                                            <th>تاریخ ارسال به فروشنده</th>
                                            <th>تاریخ دریافت از فروشنده</th>
                                            <th>تاریخ تحویل به مشتری</th>
                                        </tr>
                                        @foreach($guaranties as $guaranty )
                                            <tr>
                                                <td>{{$guaranty->customer->name}} {{$guaranty->customer->family}}</td>
                                                <td>{{$guaranty->provider->name}} {{$guaranty->provider->family}}</td>
                                                <td>{{$guaranty->created_at}}</td>
                                                <td id="send">
                                                    @if($guaranty->send_to_guaranty==null)
                                                        @php($step='send')
                                                        {{--<a class="btn btn-outline-success"
                                                           href="{{route('setGuarantyTracking',[$guaranty->id,$step])}}">ثبت وضعیت</a>--}}
                                                        <button class="btn btn-outline-success send" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->send_to_guaranty}}
                                                    @endif
                                                </td>
                                                <td id="receive">
                                                    @if($guaranty->receive_from_guaranty==null)
                                                        @php($step='receive')

                                                        <button class="btn btn-outline-success receive" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->receive_from_guaranty}}
                                                    @endif
                                                </td>
                                                <td id="deliver">
                                                    @if($guaranty->customer_delivery==null)
                                                        @php($step='deliver')
                                                        <button class="btn btn-outline-success deliver" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->customer_delivery}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <div class="row d-flex justify-content-center">{{$guaranties->links()}}</div>
                                </div>
                            </div>
                            <div class="tab-pane fade p-3" id="nav-history" role="tabpanel"
                                 aria-labelledby="history">
                                <div class="box-body">
                                    <div class="row my-3 w-100">
                                        <div class="col-12 col-md-6">
                                            <h6 class="text-muted">جستجو با نام مشتری</h6>
                                            <input type="text" name="customer_id" id="customer" class="d-none">
                                            <div class="autocomplete" style="width:300px;">
                                                {!! Form::text('customer',null,['class'=>'form-control','id'=>'customers']) !!}
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <h6 class="text-muted">جستجو با نام محصول</h6>
                                            <input type="text" name="product_id" id="product" class="d-none">
                                            <div class="autocomplete" style="width:300px;">
                                                {!! Form::text('product',null,['class'=>'form-control','id'=>'products']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <table id="table-archive" class="table table-bordered table-responsive-md table-hover table-striped table-header mt-4  ">
                                        <tr class="table-header">
                                            <th>مشتری</th>
                                            <th>فروشنده</th>
                                            <th>تاریخ دریافت</th>
                                            <th>تاریخ ارسال به فروشنده</th>
                                            <th>تاریخ دریافت از فروشنده</th>
                                            <th>تاریخ تحویل به مشتری</th>
                                        </tr>
                                        @foreach($done_guaranties as $guaranty )
                                            <tr>
                                                <td>{{$guaranty->customer->name}} {{$guaranty->customer->family}}</td>
                                                <td>{{$guaranty->provider->name}} {{$guaranty->provider->family}}</td>
                                                <td>{{$guaranty->created_at}}</td>
                                                <td >
                                                    @if($guaranty->send_to_guaranty==null)
                                                        @php($step='send')
                                                        {{--<a class="btn btn-outline-success"
                                                           href="{{route('setGuarantyTracking',[$guaranty->id,$step])}}">ثبت وضعیت</a>--}}
                                                        <button class="btn btn-outline-success send" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->send_to_guaranty}}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if($guaranty->receive_from_guaranty==null)
                                                        @php($step='receive')

                                                        <button class="btn btn-outline-success receive" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->receive_from_guaranty}}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if($guaranty->customer_delivery==null)
                                                        @php($step='deliver')
                                                        <button class="btn btn-outline-success deliver" id="{{$guaranty->id}}">ثبت وضعیت</button>
                                                    @else
                                                        {{$guaranty->customer_delivery}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <div class="row d-flex justify-content-center">{{$guaranties->links()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>

            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table-new')
            $("#customers").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>مشتری</th>\n' +
                    '                            <th>فروشنده</th>\n' +
                    '                            <th>تاریخ دریافت</th>\n' +
                    '                            <th>تاریخ ارسال به فروشنده</th>\n' +
                    '                            <th>تاریخ دریافت از فروشنده</th>\n' +
                    '                            <th>تاریخ تحویل به مشتری</th>\n' +
                    '                        </tr>'
                customer_name = $('#customers').val()
                product_name = $('#products').val()

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        customer_name: $('#customers').val(),
                        product_name: $('#products').val(),
                        do: 'guaranty-tracking',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        response.forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            var td2 = row.insertCell(2)
                            var td3 = row.insertCell(3)
                            var td4 = row.insertCell(4)
                            var td5 = row.insertCell(5)
                            if (re['customer']['family'] != null)
                                td0.innerHTML = re['customer']['name'] + " " + re['customer']['family']
                            else
                                td0.innerHTML = re['customer']['name']

                            if (re['provider']['family'] != null)
                                td1.innerHTML = re['provider']['name'] + " " + re['provider']['family']
                            else
                                td1.innerHTML = re['provider']['name']

                            td2.innerHTML = re['created_at']

                            if (re['send_to_guaranty'] == null) {
                                td3.innerHTML = '<button id="' + re['id'] + '" name="send" class="send btn btn-outline-success">ثبت وضعیت</a>'
                                $(".send").click(function () {
                                    var id = re['id']
                                    var step = 'send'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()

                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });
                                });
                            } else {
                                td3.innerHTML = re['send_to_guaranty']
                            }

                            if (re['receive_from_guaranty'] == null) {
                                td4.innerHTML = '<button id="' + re['id'] + '" name="receive" class="receive btn btn-outline-success">ثبت وضعیت</a>'
                                $(".receive").click(function () {
                                    var id = re['id']
                                    var step = 'receive'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()
                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });

                                });

                            } else {
                                td4.innerHTML = re['receive_from_guaranty']
                            }

                            if (re['customer_delivery'] == null) {
                                td5.innerHTML = '<button id="' + re['id'] + '" name="deliver" class="deliver btn btn-outline-success">ثبت وضعیت</a>'
                                $(".deliver").click(function () {
                                    var id = re['id']
                                    var step = 'deliver'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()

                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });

                                });

                            } else {
                                td5.innerHTML = re['customer_delivery']
                            }
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $("#products").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>مشتری</th>\n' +
                    '                            <th>فروشنده</th>\n' +
                    '                            <th>تاریخ دریافت</th>\n' +
                    '                            <th>تاریخ ارسال به فروشنده</th>\n' +
                    '                            <th>تاریخ دریافت از فروشنده</th>\n' +
                    '                            <th>تاریخ تحویل به مشتری</th>\n' +
                    '                        </tr>'
                customer_name = $('#customers').val()
                product_name = $('#products').val()

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        customer_name: $('#customers').val(),
                        product_name: $('#products').val(),
                        do: 'guaranty-tracking',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        response.forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            var td2 = row.insertCell(2)
                            var td3 = row.insertCell(3)
                            var td4 = row.insertCell(4)
                            var td5 = row.insertCell(5)
                            if (re['customer']['family'] != null)
                                td0.innerHTML = re['customer']['name'] + " " + re['customer']['family']
                            else
                                td0.innerHTML = re['customer']['name']

                            if (re['provider']['family'] != null)
                                td1.innerHTML = re['provider']['name'] + " " + re['provider']['family']
                            else
                                td1.innerHTML = re['provider']['name']

                            td2.innerHTML = re['created_at']

                            if (re['send_to_guaranty'] == null) {
                                td3.innerHTML = '<button id="' + re['id'] + '" name="send" class="send btn btn-outline-success">ثبت وضعیت</a>'
                                $(".send").click(function () {
                                    var id = re['id']
                                    var step = 'send'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()

                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });
                                });
                            } else {
                                td3.innerHTML = re['send_to_guaranty']
                            }

                            if (re['receive_from_guaranty'] == null) {
                                td4.innerHTML = '<button id="' + re['id'] + '" name="receive" class="receive btn btn-outline-success">ثبت وضعیت</a>'
                                $(".receive").click(function () {
                                    var id = re['id']
                                    var step = 'receive'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()
                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });

                                });

                            } else {
                                td4.innerHTML = re['receive_from_guaranty']
                            }

                            if (re['customer_delivery'] == null) {
                                td5.innerHTML = '<button id="' + re['id'] + '" name="deliver" class="deliver btn btn-outline-success">ثبت وضعیت</a>'
                                $(".deliver").click(function () {
                                    var id = re['id']
                                    var step = 'deliver'
                                    $.ajax({
                                        type: "POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: $('.ajax.d-none').attr('id'),
                                        data: {// change data to this object
                                            customer_name: $('#customers').val(),
                                            do: 'guaranty-tracking-set-step',
                                            id: id,
                                            step: step
                                        },
                                        dataType: 'json',
                                        success: function (response) {
                                            console.log(response)
                                            location.reload()

                                        },
                                        error: function (response) {
                                            console.log('error')
                                        }
                                    });

                                });

                            } else {
                                td5.innerHTML = re['customer_delivery']
                            }
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $(".btn.btn-outline-success.send").on("click",function () {
                var id=this.id
                var table = document.getElementById('table-new')
                var rows = $('tr', table);
                var rowindex=$(this).parent().parent().index()
                var currentRow=rows.eq(rowindex)
                var td1=currentRow.children("#send")
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        do: 'SetGuarantyTracking',
                        id:id,
                        step:'send',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        td1.text(response['send_to_guaranty'])
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            })

            $(".btn.btn-outline-success.receive").on("click",function () {
                var id=this.id
                var table = document.getElementById('table-new')
                var rows = $('tr', table);
                var rowindex=$(this).parent().parent().index()
                var currentRow=rows.eq(rowindex)
                var td2=currentRow.children("#receive")
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        do: 'SetGuarantyTracking',
                        id:id,
                        step:'receive',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        td2.text(response['receive_from_guaranty'])
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            })

            $(".btn.btn-outline-success.deliver").on("click",function () {
                var id=this.id
                var table = document.getElementById('table-new')
                var rows = $('tr', table);
                var rowindex=$(this).parent().parent().index()
                var currentRow=rows.eq(rowindex)
                var td1=currentRow.children("#send")
                var td2=currentRow.children("#receive")
                var td3=currentRow.children("#deliver")
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        do: 'SetGuarantyTracking',
                        id:id,
                        step:'deliver',
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        td1.text(response['send_to_guaranty'])
                        td2.text(response['receive_from_guaranty'])
                        td3.text(response['customer_delivery'])
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            })
        })


    </script>
@endsection


