@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">مدیریت کالا</h1>
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('deleted'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('deleted')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">

            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center my-2 p-0 ">
                        <input type="text" name="product_id" id="product_name" class="product_name d-none">
                        <div class="autocomplete my-2" style="width:300px;">
                            {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو با نام کالا','class'=>'form-control','id'=>'product_names']) !!}
                        </div>

                        <input type="text" name="barcode" id="barcode" class="barcode d-none">
                        <div class="autocomplete my-2" style="width:300px;">
                            {!! Form::text('barcode',null,['autocomplete'=>'off','placeholder'=>'جستجو با بارکد','class'=>'form-control','id'=>'barcodes']) !!}
                        </div>

                        <a href="{{route('newProduct')}}" class="btn btn-success">افزودن کالا</a>
                        <a href="{{route('product.create')}}" class="btn btn-primary">خرید کالا</a>
                        <a href="{{route('sell')}}" class="btn btn-warning">فروش کالا</a>
                    </div>

                </div>
                @if(count($products)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2" id="table">
                            <tr class="table-header text-center">
                                <th>تصویر</th>
                                <th>نام کالا</th>
                                <th> گروه کالا</th>
                                <th>قیمت واحد (تومان)</th>
                                <th>تعداد</th>
                                <th>امکانات</th>
                            </tr>
                            @foreach($products as $product )
                                <tr>
                                    <td>
                                        @if($product->photo)
                                            <img class="img-product-list rounded"
                                                 src="{{asset('images/products/'.$product->photo->path)}}">
                                        @else
                                            <img class="img-product-list" src="{{asset('img/default-2.svg')}}">
                                        @endif
                                    </td>
                                    <td>
                                        <a class="text-decoration-none" href="{{route('product.edit',$product->id)}}">
                                            {{$product->name}}
                                        </a>
                                    </td>
                                    <td>{{$product->category->title}}</td>
                                    <td>{{number_format($product->sell_price)}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td>
                                        <div class="row w-100 justify-content-center">
                                            <div class="col-12 col-md-4 ">
                                                <a class="text-primary" href="{{route('product.edit',$product->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </div>
                                            <div class="col-12 col-md-4">
{{--                                                {!! Form::open(['method'=>'DELETE','action'=>['AdminProductController@destroy',$product->id]])!!}--}}
                                                <button id="{{$product->id}}" class="delete btn text-danger p-0">
                                                    <i class="fa fa-trash"></i>
                                                </button>
{{--                                                {!! Form::close() !!}--}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$products->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            محصولی جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table')
            $("#product_names").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>تصویر</th>\n' +
                    '                            <th>نام کالا</th>\n' +
                    '                            <th>گروه کالا</th>\n' +
                    '                            <th>قیمت واحد(تومان)</th>\n' +
                    '                            <th>تعداد</th>\n' +
                    '                            <th>امکانات</th>\n' +
                    '                        </tr>'
                var product_name = $('#product_names').val()
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            product_name: $('#product_names').val(),
                            do: 'search-product-by-name',
                        },
                        dataType: 'json',
                        success: function (response) {
                            response['data'].forEach(function (re) {
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                var td5 = row.insertCell(5)
                                var id=re['id']
                                var route='{{route('product.edit','*')}}'
                               var routee=route.replace('*',id)

                                if (re['photo'] != null) {
                                    var path = '/images/products/' + re['photo']['path']
                                    td0.innerHTML = '<img class="img-product-list" src="' + path + '">'
                                }
                                else
                                    td0.innerHTML = '<img class="img-product-list" src="{{asset('img/default-2.svg')}}">'
                                td1.innerHTML = re['name']
                                td2.innerHTML = re['category']['title']
                                td3.innerHTML = re['sell_price']
                                td4.innerHTML = re['quantity']
                                td5.innerHTML = '<div class="row w-100 justify-content-center">\n' +
                                    '                                        <div class="col-12 col-md-3 ">\n' +
                                    '                                            <a class=" btn btn-primary" href="'+routee+'">\n' +
                                    '                                                <i class="fa fa-edit"></i>\n' +
                                    '                                                ویرایش\n' +
                                    '                                            </a>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="col-12 col-md-3">\n' +
                                    '                                            <button id='+re['id']+' class="delete btn btn-danger ">\n' +
                                    '                                                <i class="fa fa-trash"></i>\n' +
                                    '                                                حذف\n' +
                                    '                                            </button>\n' +
                                    '\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>'
                            })

                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });

            });

            $("#barcodes").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>تصویر</th>\n' +
                    '                            <th>نام کالا</th>\n' +
                    '                            <th>گروه کالا</th>\n' +
                    '                            <th>قیمت واحد(تومان)</th>\n' +
                    '                            <th>تعداد</th>\n' +
                    '                            <th>امکانات</th>\n' +
                    '                        </tr>'
                var barcode = $('#barcodes').val()
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            barcode: $('#barcodes').val(),
                            do: 'search-product-by-barcode',
                        },
                        dataType: 'json',
                        success: function (response) {
                            data = response
                            // autocompleteName(document.getElementById("product_names"), data);
                            console.log(response)
                            response['data'].forEach(function (re) {
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                var td5 = row.insertCell(5)
                                var route='{{route('product.edit','*')}}'
                               var routee=route.replace('*',re['id'])

                                if (re['photo'] != null) {
                                    var path = '/images/products/' + re['photo']['path']
                                    td0.innerHTML = '<img class="img-product-list" src="' + path + '">'
                                }
                                else
                                    td0.innerHTML = '<img class="img-product-list" src="{{asset('img/default-2.svg')}}">'
                                td1.innerHTML = re['name']
                                td2.innerHTML = re['category']['title']
                                td3.innerHTML = re['sell_price']
                                td4.innerHTML = re['quantity']
                                td5.innerHTML = '<div class="row w-100 justify-content-center">\n' +
                                    '                                        <div class="col-12 col-md-3 ">\n' +
                                    '                                            <a class=" btn btn-primary" href="'+routee+'">\n' +
                                    '                                                <i class="fa fa-edit"></i>\n' +
                                    '                                                ویرایش\n' +
                                    '                                            </a>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="col-12 col-md-3">\n' +
                                    '                                            <button id='+re['id']+' class="delete btn btn-danger ">\n' +
                                    '                                                <i class="fa fa-trash"></i>\n' +
                                    '                                                حذف\n' +
                                    '                                            </button>\n' +
                                    '\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>'
                            })
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });

            });

            $('.delete.btn').click(function () {
                var pro_id=this.id
                var rowindex=$(this).parent().parent().parent().parent().index()
                swal({
                    title: "مطمئنی ؟؟",
                    text: "میخوای این کالا رو پاک کنی؟؟",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "بله . پاکش کن!",
                    cancelButtonText: "نه  . بیخیال!",
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: $('.ajax.d-none').attr('id'),
                            data: {// change data to this object
                                id: pro_id,
                                do: 'delete-product',
                            },
                            dataType: 'json',
                            success: function (response) {
                                console.log(response)
                                if(response=='Access Denied'){
                                    swal("خطا!", " شما مجاز به انجام این عملیات نیستید.", "error")
                                }else if(response=='success'){
                                    var table=document.getElementById('table')
                                    table.deleteRow(rowindex)
                                    swal("موفق", " کالا حذف شد.", "success")
                                }
                            },
                            error: function (response) {
                                console.log('error')
                            }
                        });
                        swal("پاک شد!", "فایل با موفقیت پاک شد", "success");
                    }
                });
            });

        })
    </script>
    @endsection


