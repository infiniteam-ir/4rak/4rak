@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted"> افزودن کالا</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="container">
                    <div class="box-body">
                        {{--                                {!! Form::open(['method'=>'POST','action'=>'AdminProductController@store','files'=>true]) !!}--}}
                        <form action="" method="post" id="add-pro-form">
                            <div class="row px-4 d-flex pb-5">

                                <div class="row w-100 mt-3 justify-content-between align-items-center ">
                                    <div class="col-12 mt-3 col-md-4 text-right">
                                        <h6 class="text-muted">نام کالا:</h6>
                                        {{--                                            {!! Form::text('title',null,['class'=>'form-control','id'=>'p_name']) !!}--}}
                                        <input name="title" id="p_name" class="form-control" type="text"
                                               required="required"
                                               oninvalid="this.setCustomValidity('لطفا نام کالا را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                    <div class="col-12 mt-3 col-md-4 text-right">
                                        <h6 class="text-muted"> گروه کالا:</h6>
                                        <div class="autocomplete" style="width:300px;">
                                            <input type="text" name="cat_id" id="cat" class="cat d-none">
                                            {{--                                                {!! Form::text('cats',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'cats']) !!}--}}
                                            <input name="cats" id="cats" autocomplete="off" class="form-control"
                                                   type="text" required="required"
                                                   oninvalid="this.setCustomValidity('لطفا گروه کالا را وارد کنید')"
                                                   oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                </div>
                                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                    <div class="col-12 mt-3 col-md-3 text-right">
                                        <h6 class="text-muted"> تعداد:</h6>
                                        {{--                                            {!! Form::text('qty',null,['class'=>'form-control','id'=>'qty']) !!}--}}
                                        <input name="qty" id="qty" autocomplete="off" class="form-control"
                                               type="text" required="required"
                                               oninvalid="this.setCustomValidity('لطفا تعداد کالا را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                    <div class="col-12 mt-3 col-md-3 text-right">
                                        <h6 class="text-muted">قیمت خرید(تومان):</h6>
                                        {{--                                            {!! Form::text('buy_price',null,['class'=>'form-control','id'=>'buy_price']) !!}--}}
                                        <input name="buy_price" id="buy_price" autocomplete="off"
                                               class="form-control" type="text" required="required"
                                               oninvalid="this.setCustomValidity('لطفا قیمت خرید را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                    <div class="col-12 mt-3 col-md-3 text-right">
                                        <h6 class="text-muted">قیمت فروش(تومان):</h6>
                                        {{--                                            {!! Form::text('sell_price',null,['class'=>'form-control','id'=>'sell_price']) !!}--}}
                                        <input name="sell_price" id="sell_price" autocomplete="off"
                                               class="form-control" type="text" required="required"
                                               oninvalid="this.setCustomValidity('لطفا قیمت فروش را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                    <div class="col-12 mt-3 col-md-3 text-right">
                                        <h6 class="text-muted">قیمت عمده(تومان):</h6>
                                        {{--                                            {!! Form::text('whole_price',null,['class'=>'form-control','id'=>'whole_price']) !!}--}}
                                        <input name="whole_price" id="whole_price" autocomplete="off"
                                               class="form-control" type="text" required="required"
                                               oninvalid="this.setCustomValidity('لطفا قیمت فروش عمده را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>

                                </div>
                                <div class="row w-100 mt-3 ">
                                    <div class="col-12 mt-3 col-md-4 text-right">
                                        <h6 class="text-muted">گارانتی:</h6>
                                        <select name="guaranty" class="form-control" id="guaranty">
                                            <option value="false">ندارد</option>
                                            <option value="true">دارد</option>
                                        </select>
                                    </div>
                                    <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section">
                                        <h6 class="text-muted">مدت گارانتی:</h6>
                                        <div class="row w-100">
                                            <div class="col-12 col-md-6">
                                                {!! Form::text('duration',0,['class'=>'form-control','id'=>'guaranty_duration']) !!}
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <select name="duration_unit" class="form-control"
                                                        id="guaranty_unit">
                                                    <option value="day">روز</option>
                                                    <option value="month">ماه</option>
                                                    <option value="year">سال</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section">
                                        <h6 class="text-muted">شروع از:</h6>
                                        <select name="guaranty_start" class="form-control" id="guaranty_start">
                                            <option value="sell_date">فاکتور فروش</option>
                                            <option value="buy_date">فاکتور خرید</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row w-100 mt-3 justify-content-center align-items-start ">
                                    <div class="col-12 mt-3 col-md-4 text-right">
                                        <h6 class="text-muted">کلیدواژه ها:</h6>
                                        {!! Form::text('keyword',null,['class'=>'form-control','id'=>'keywords']) !!}
                                    </div>
                                    <div class="barcode col-12 mt-3 col-md-4 text-right">
                                        <h6 class="text-muted">بارکد:</h6>
                                        {!! Form::number('barcode',null,['class'=>'form-control','id'=>'barcode']) !!}
                                    </div>
                                </div>
                                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                    <div class="col-12 mt-3 col-md-8 text-right">
                                        <h6 class="text-muted"> توضیحات:</h6>
                                        {!! Form::textarea('desc',null,['class'=>'form-control','id'=>'desc']) !!}
                                        <script>
                                            CKEDITOR.replace('desc', {
                                                language: 'fa',
                                                // uiColor: '#9AB8F3'
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                    <div class="col-12 mt-3 col-md-8 text-right">
                                        <div class="row">
                                            <div class="d-none col-12 inp-upload mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">تصویر:</h6>
                                                <input type="file" class="filepond" name="image"
                                                       data-allow-reorder="true"
                                                       data-max-file-size="3MB"
                                                       accept="image/png, image/jpeg, image/gif"/>
                                            </div>
                                            <div class="d-none  col-12 inp-upload mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">فایل:</h6>
                                                <input type="file" class="filepond" name="file"
                                                       data-allow-reorder="true"
                                                       data-max-file-size="200MB"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="product_id d-none"></span>
                                <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center ">
                                    <div class="col-12 mt-3 col-md-3 text-center">
                                        <button type="submit" name="save" id="add" value="save"
                                                class="btn btn-success w-75">
                                            ثبت و افزودن تصویر/فایل
                                        </button>
                                    </div>
                                    <div class="col-12 mt-3 col-md-3 text-center">
                                        <button type="submit" name="save" id="add-new" value="save-new"
                                                class="btn btn-purple w-75">
                                            ثبت و جدید
                                        </button>
                                    </div>
                                    <div class="col-12 mt-3 col-md-3 text-center">
                                        <a class="btn btn-info w-50 " id="close" href="{{route('product.index')}}">بستن</a>
                                    </div>

                                    <div class="col-12 mt-3 col-md-3 text-center">
                                        <a class="btn btn-danger w-50 "
                                           href="{{route('product.index')}}">انصراف</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        {{--                                {!! Form::close() !!}--}}
                        <table class="table table-bordered table-responsive-md table-hover table-striped"
                               id="products_table">
                            <tr class="table-header">
                                <th>نام محصول</th>
                                <th>گروه کالا</th>
                                <th>قیمت خرید(واحد)</th>
                                <th>قیمت فروش(واحد)</th>
                                <th>تعداد</th>
                            </tr>

                        </table>
                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


@section('script')
{{--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
    <script src="{{asset('js/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            var table = document.getElementById('table-list');
            var valid = false;

            $('#barcode').focusout(function () {
                $('.barcode-error').remove()
                if ($('#barcode').val() !== "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'check-barcode-is-unique',
                            barcode: $('#barcode').val(),
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response == 'OK') {
                                valid = true;
                                $('.barcode-error').remove();
                            } else {
                                valid = false;
                                var element = '<span class="barcode-error text-danger mt-2">بارکد تکراری است</span>'
                                $('.barcode').append(element)
                            }

                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }

            })


            $('#add-pro-form').submit(function (e) {
                e.preventDefault();
                var tbl = document.getElementById('products_table')
                var barcode = $('#barcode').val()
                if (valid != false || barcode == "") {
                    var val = document.activeElement;
                    var desc=CKEDITOR.instances.desc.getData();
                    if (desc=="") {
                        swal('فیلد توضیحات نمیتواند خالی باشد !')
                    } else{
                        if (val.value != "" && val.value == 'save') {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: $('.ajax.d-none').attr('id'),
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr('content'),
                                    do: 'new-product',
                                    flag: 'save',
                                    p_name: $('#p_name').val(),
                                    category: $('#cats').val(),
                                    buy_price: $('#buy_price').val(),
                                    sell_price: $('#sell_price').val(),
                                    whole_price: $('#whole_price').val(),
                                    qty: $('#qty').val(),
                                    guaranty: $('#guaranty').val(),
                                    guaranty_unit: $('#guaranty_unit').val(),
                                    guaranty_duration: $('#guaranty_duration').val(),
                                    guaranty_start: $('#guaranty_start').val(),
                                    description: CKEDITOR.instances.desc.getData(),
                                    keywords: $('#keywords').val(),
                                    barcode: $('#barcode').val(),
                                },
                                dataType: 'json',
                                success: function (response) {
                                    $('.product_id').attr('id', response['product']['id'])
                                    FilePond.registerPlugin();
                                    var element = document.querySelector('meta[name="csrf-token"]');
                                    var csrf = element && element.getAttribute("content");
                                    var product_id = $('.product_id').attr('id')
                                    var addresssss = "{{ route('UploadProductFiles',1)}}";
                                    FilePond.setOptions({
                                        server: {
                                            url: addresssss,
                                            process: {
                                                headers: {
                                                    'X-CSRF-TOKEN': csrf,
                                                    'id': product_id,
                                                },
                                            },
                                            revert: {
                                                headers: {
                                                    'X-CSRF-TOKEN': csrf,
                                                    'id': product_id,
                                                },
                                            },
                                        },
                                        allowRevert: true,
                                        labelIdle: "فایلها را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
                                        labelFileProcessing: 'در حال آپلود',
                                        labelFileProcessingComplete: 'تکمیل شد',
                                        labelFileProcessingAborted: 'لغو شد',
                                        labelFileProcessingError: 'خطا حین آپلود',
                                        labelTapToCancel: 'برای لغو اینجا کلیک کنید',
                                        labelTapToUndo: 'برای بازگشت اینجا کلیک کنید',
                                        credits: false,
                                    });

                                    const inputElement = document.querySelector('input[name="image"]');
                                    const pond = FilePond.create(inputElement);
                                    const inputElement1 = document.querySelector('input[name="file"]');
                                    const pond1 = FilePond.create(inputElement1);

                                    $('.inp-upload').removeClass('d-none')

                                    var row = tbl.insertRow(-1);
                                    var td1 = row.insertCell(0)
                                    var td2 = row.insertCell(1)
                                    var td3 = row.insertCell(2)
                                    var td4 = row.insertCell(3)
                                    var td5 = row.insertCell(4)
                                    td1.innerHTML = response['product']['name']
                                    td2.innerHTML = response['pro_category']
                                    td3.innerHTML = response['product']['buy_price']
                                    td4.innerHTML = response['product']['sell_price']
                                    td5.innerHTML = response['product']['quantity']

                                    $('#add').addClass('d-none')

                                },
                                error: function (response) {
                                    console.log(response)
                                }
                            });
                        } else if (val.value != "" && val.value == 'save-new') {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: $('.ajax.d-none').attr('id'),
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr('content'),
                                    do: 'new-product',
                                    flag: 'savenew',
                                    p_name: $('#p_name').val(),
                                    category: $('#cats').val(),
                                    buy_price: $('#buy_price').val(),
                                    sell_price: $('#sell_price').val(),
                                    whole_price: $('#whole_price').val(),
                                    qty: $('#qty').val(),
                                    guaranty: $('#guaranty').val(),
                                    guaranty_unit: $('#guaranty_unit').val(),
                                    guaranty_duration: $('#guaranty_duration').val(),
                                    guaranty_start: $('#guaranty_start').val(),
                                    description: CKEDITOR.instances.desc.getData(),
                                    keywords: $('#keywords').val(),
                                    barcode: $('#barcode').val(),
                                },
                                dataType: 'json',
                                success: function (response) {
                                    if (response == 'new') {
                                        $('#add').removeClass('d-none')

                                        $('.inp-upload').addClass('d-none')
                                        document.getElementById('add-pro-form').scrollIntoView();
                                        $('#p_name').focus();
                                        $('#p_name').val("");
                                        $('#cats').val("");
                                        $('#buy_price').val("");
                                        $('#sell_price').val("");
                                        $('#whole_price').val("");
                                        $('#qty').val("");
                                        $('#barcode').val("");
                                        $('#keywords').val("")
                                        CKEDITOR.instances.desc.setData("")

                                    } else {
                                        $('.product_id').attr('id', response['product']['id'])

                                        FilePond.registerPlugin();
                                        var element = document.querySelector('meta[name="csrf-token"]');
                                        var csrf = element && element.getAttribute("content");
                                        var product_id = $('.product_id').attr('id')
                                        var addresssss = "{{ route('UploadProductFiles',1)}}";
                                        FilePond.setOptions({
                                            server: {
                                                url: addresssss,
                                                process: {
                                                    headers: {
                                                        'X-CSRF-TOKEN': csrf,
                                                        'id': product_id,
                                                    },
                                                },
                                                revert: {
                                                    headers: {
                                                        'X-CSRF-TOKEN': csrf,
                                                        'id': product_id,
                                                    },
                                                },
                                            },
                                            allowRevert: false,
                                            labelIdle: "فایلها را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
                                            labelFileProcessing: 'در حال آپلود',
                                            labelFileProcessingComplete: 'تکمیل شد',
                                            labelFileProcessingAborted: 'لغو شد',
                                            labelFileProcessingError: 'خطا حین آپلود',
                                            labelTapToCancel: 'برای لغو اینجا کلیک کنید',
                                            labelTapToUndo: 'برای بازگشت اینجا کلیک کنید',
                                            credits: false,
                                        });

                                        const inputElement = document.querySelector('input[name="image"]');
                                        const pond = FilePond.create(inputElement);
                                        const inputElement1 = document.querySelector('input[name="file"]');
                                        const pond1 = FilePond.create(inputElement1);

                                        $('.inp-upload').addClass('d-none')

                                        var row = tbl.insertRow(-1);
                                        var td1 = row.insertCell(0)
                                        var td2 = row.insertCell(1)
                                        var td3 = row.insertCell(2)
                                        var td4 = row.insertCell(3)
                                        var td5 = row.insertCell(4)
                                        td1.innerHTML = response['product']['name']
                                        td2.innerHTML = response['pro_category']
                                        td3.innerHTML = response['product']['buy_price']
                                        td4.innerHTML = response['product']['sell_price']
                                        td5.innerHTML = response['product']['quantity']
                                        document.getElementById('add-pro-form').scrollIntoView();
                                        $('#p_name').focus();
                                        $('#p_name').val("");
                                        $('#cats').val("");
                                        $('#buy_price').val("");
                                        $('#sell_price').val("");
                                        $('#whole_price').val("");
                                        $('#qty').val("");
                                        $('#barcode').val("");
                                        $('#keywords').val("")
                                        CKEDITOR.instances.desc.setData("")
                                    }
                                },
                                error: function (response) {
                                    console.log(response)
                                }
                            });
                        }
                    }


                } else {
                    $('#barcode').focus()
                    location.href = "#barcode";
                }
            });


            var cats = document.getElementById('cats')
            cats.addEventListener("input", function () {
                if ($('#cats').val() != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            key: $('#cats').val(),
                            do: 'search-cat'
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response != "")
                                autocompletecat(document.getElementById("cats"), response);
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                } else {
                    var existingElement = document.getElementById("catsautocomplete-list")
                    console.log(existingElement)
                    if (existingElement)
                        existingElement.remove()
                }
            });
            function autocompletecat(inp, arr) {
                var existingElement = document.getElementById("catsautocomplete-list")
                if (existingElement)
                    existingElement.remove()

                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;

                var a, b = null
                /*, val = this.value;
                                /!*close any already open lists of autocompleted values*!/
                                closeAllLists();
                                if (!val) {
                                    return false;
                                }*/
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary rounded bg-light py-2 px-1");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (var i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i]['title'] + "</strong>";
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['title'] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        cat = document.getElementById('cat')
                        cat.value = this.getElementsByTagName("input")[0].id
                        document.getElementById("cat").value = this.getElementsByTagName("input")[0].id
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }

                /*execute a function when someone writes in the text field:*/
                /*inp.addEventListener("input", function (e) {

                });*/

                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });

                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }

                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }

                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }

                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }

            $("#cat").on("keyup", function () {
                var data = null
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        key: $('#cat').val(),
                        do: 'search-cat'
                    },
                    dataType: 'json',
                    success: function (response) {
                        data = response
                        autocompleteCat(document.getElementById("cat"), data);
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });

            });


            $('#close').click(function () {
                swal('کالاها اضافه شد')
            })



            $('#guaranty').click(function (event) {
                var flag = this.value
                if (flag == "true") {
                    $('.guaranty-section').removeClass('d-none')
                } else {
                    $('.guaranty-section').addClass('d-none')
                }
            });
        })
    </script>

@endsection

