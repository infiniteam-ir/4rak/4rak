@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">فاکتور خرید</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="container">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="new" data-toggle="tab" href="#nav-new" role="tab"
                               aria-controls="nav-new" aria-selected="true">
                                @if(Route::CurrentRouteName()=='product.create')
                                    فاکتور جدید
                                @else
                                    ویرایش فاکتور
                                @endif
                            </a>
                            <a class="nav-item nav-link" id="factors" data-toggle="tab" href="#nav-factors"
                               role="tab"
                               aria-controls="nav-factors" aria-selected="false">لیست فاکتور ها</a>
                        </div>
                    </nav>
                    <div class="tab-content pt-0" id="nav-tabContent">
                        <div class="tab-pane fade show active p-3" id="nav-new" role="tabpanel" aria-labelledby="new">
                            <div class="box-body">
                                {{--                                {!! Form::open(['method'=>'POST','action'=>'AdminProductController@store','files'=>true]) !!}--}}
                                <form action="" method="post" id="add-pro-form">
                                    <div class="row px-4 d-flex pb-5">
                                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">شماره فاکتور: <sup class="text-danger">*</sup></h6>
                                                @if($factor==null)
                                                    <input id="f_num" class="form-control" type="number"
                                                           required="required"
                                                           oninvalid="this.setCustomValidity('لطفا شماره فاکتور را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                    {{--                                                {!! Form::number('f_num',null,['class'=>'form-control','id'=>'f_num']) !!}--}}
                                                @else
                                                    <label class="form-control">{{$factor->f_num}}</label>
                                                    <input id="f_num" value="{{$factor->f_num}}" class="form-control"
                                                           type="number" required="required"
                                                           oninvalid="this.setCustomValidity('لطفا شماره فاکتور را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                    {{--                                                {!! Form::text('f_num',$factor->f_num,['class'=>'form-control d-none','id'=>'f_num','required']) !!}--}}
                                                @endif
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted"> قیمت کل فاکتور:</h6>
                                                @if($factor==null)
                                                    {!! Form::text('total_price',0,['class'=>'form-control','disabled'=>'disabled','id'=>'total_price']) !!}
                                                @else
                                                    {!! Form::text('total_price',$factor->total_price,['class'=>'form-control','disabled'=>'disabled','id'=>'total_price']) !!}
                                                @endif
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted"> تاریخ: <sup class="text-danger">*</sup></h6>
                                                @if($factor==null)
                                                    <input name="f_date" id="f_date" class="form-control"
                                                           required="required"
                                                           oninvalid="this.setCustomValidity('لطفا تاریخ فاکتور را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                @else
                                                    {{--                                                {!! Form::text('f_date',$factor->f_date,['class'=>'form-control','id'=>'f_date']) !!}--}}
                                                    <input name="f_date" id="f_date" value="{{$factor->f_date}}"
                                                           class="form-control" required="required"
                                                           oninvalid="this.setCustomValidity('لطفا تاریخ فاکتور را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row w-100 mt-3 justify-content-between align-items-center ">
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">نام کالا: <sup class="text-danger">*</sup></h6>
                                                {{--                                            {!! Form::text('title',null,['class'=>'form-control','id'=>'p_name']) !!}--}}
                                                <input name="title" id="p_name" class="form-control" type="text"
                                                       required="required"
                                                       oninvalid="this.setCustomValidity('لطفا نام کالا را وارد کنید')"
                                                       oninput="setCustomValidity('')">
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted"> گروه کالا: <sup class="text-danger">*</sup></h6>
                                                <div class="autocomplete" style="width:300px;">
                                                    <input type="text" name="cat_id" id="cat" class="cat d-none">
                                                    {{--                                                {!! Form::text('cats',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'cats']) !!}--}}
                                                    <input name="cats" id="cats" autocomplete="off" class="form-control"
                                                           type="text" required="required"
                                                           oninvalid="this.setCustomValidity('لطفا گروه کالا را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">فروشنده: <sup class="text-danger">*</sup></h6>
                                                <input type="text" name="provider_id" id="provider"
                                                       class="provider d-none">
                                                @if($factor==null)
                                                    <div class="autocomplete" style="width:300px;">
                                                        <input name="provider_co" id="providers" autocomplete="off"
                                                               class="form-control" type="text" required="required"
                                                               oninvalid="this.setCustomValidity('لطفا فروشنده کالا را وارد کنید')"
                                                               oninput="setCustomValidity('')">
                                                        {{--                                                    {!! Form::text('provider_co',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'providers']) !!}--}}
                                                    </div>
                                                @else
                                                    <input name="provider_co"
                                                           value="{{$factor->provider->name." ".$factor->provider->family}}"
                                                           id="providers" autocomplete="off" class="form-control"
                                                           type="text" required="required"
                                                           oninvalid="this.setCustomValidity('لطفا فروشنده کالا را وارد کنید')"
                                                           oninput="setCustomValidity('')">
                                                    {{--                                                {!! Form::text('provider_co',$factor->provider->name." ".$factor->provider->family,['class'=>'form-control','id'=>'providers']) !!}--}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                            <div class="col-12 mt-3 col-md-3 text-right">
                                                <h6 class="text-muted"> تعداد: <sup class="text-danger">*</sup></h6>
                                                {{--                                            {!! Form::text('qty',null,['class'=>'form-control','id'=>'qty']) !!}--}}
                                                <input name="qty" id="qty" autocomplete="off" class="form-control"
                                                       type="text" required="required"
                                                       oninvalid="this.setCustomValidity('لطفا تعداد کالا را وارد کنید')"
                                                       oninput="setCustomValidity('')">
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-right">
                                                <h6 class="text-muted">قیمت خرید(تومان): <sup class="text-danger">*</sup></h6>
                                                {{--                                            {!! Form::text('buy_price',null,['class'=>'form-control','id'=>'buy_price']) !!}--}}
                                                <input name="buy_price" id="buy_price" autocomplete="off"
                                                       class="form-control" type="text" required="required"
                                                       oninvalid="this.setCustomValidity('لطفا قیمت خرید را وارد کنید')"
                                                       oninput="setCustomValidity('')">
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-right">
                                                <h6 class="text-muted">قیمت فروش(تومان): <sup class="text-danger">*</sup></h6>
                                                {{--                                            {!! Form::text('sell_price',null,['class'=>'form-control','id'=>'sell_price']) !!}--}}
                                                <input name="sell_price" id="sell_price" autocomplete="off"
                                                       class="form-control" type="text" required="required"
                                                       oninvalid="this.setCustomValidity('لطفا قیمت فروش را وارد کنید')"
                                                       oninput="setCustomValidity('')">
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-right">
                                                <h6 class="text-muted">قیمت عمده(تومان):</h6>
                                                <input name="whole_price" id="whole_price" autocomplete="off"
                                                       class="form-control" type="text">
                                            </div>

                                        </div>
                                        <div class="row w-100 mt-3 ">
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">گارانتی:</h6>
                                                <select name="guaranty" class="form-control" id="guaranty">
                                                    <option value="false">ندارد</option>
                                                    <option value="true">دارد</option>
                                                </select>
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section">
                                                <h6 class="text-muted">مدت گارانتی:</h6>
                                                <div class="row w-100">
                                                    <div class="col-12 col-md-6">
                                                        {!! Form::text('duration',0,['class'=>'form-control','id'=>'guaranty_duration']) !!}
                                                    </div>
                                                    <div class="col-12 col-md-6">
                                                        <select name="duration_unit" class="form-control"
                                                                id="guaranty_unit">
                                                            <option value="day">روز</option>
                                                            <option value="month">ماه</option>
                                                            <option value="year">سال</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section">
                                                <h6 class="text-muted">شروع از:</h6>
                                                <select name="guaranty_start" class="form-control" id="guaranty_start">
                                                    <option value="sell_date">فاکتور فروش</option>
                                                    <option value="buy_date">فاکتور خرید</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row w-100 mt-3 justify-content-center align-items-start ">
                                            <div class="col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">کلیدواژه ها:</h6>
                                                {!! Form::text('keyword',null,['class'=>'form-control','id'=>'keywords']) !!}
                                            </div>
                                            <div class="barcode col-12 mt-3 col-md-4 text-right">
                                                <h6 class="text-muted">بارکد:</h6>
                                                {!! Form::number('barcode',null,['class'=>'form-control','id'=>'barcode']) !!}
                                            </div>
                                        </div>
                                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                            <div class="col-12 mt-3 col-md-8 text-right">
                                                <h6 class="text-muted"> توضیحات: <sup class="text-danger">*</sup></h6>
{{--                                                {!! Form::textarea('desc',null,['class'=>'form-control','id'=>'desc']) !!}--}}
                                                <textarea class="form-control" id="desc" name="desc" required
                                                          oninvalid="this.setCustomValidity('لطفا  توضیحات را وارد کنید')"
                                                          oninput="setCustomValidity('')">
                                                </textarea>
                                                <script>
                                                    CKEDITOR.replace('desc', {
                                                        language: 'fa',
                                                        // uiColor: '#9AB8F3'
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                            <div class="col-12 mt-3 col-md-8 text-right">
                                                <div class="row">
                                                    <div class="d-none col-12 inp-upload mt-3 col-md-4 text-right">
                                                        <h6 class="text-muted">تصویر:</h6>
                                                        <input type="file" class="filepond" name="image"
                                                               data-allow-reorder="true"
                                                               data-max-file-size="3MB"
                                                               accept="image/png, image/jpeg, image/gif"/>
                                                    </div>
                                                    <div class="d-none  col-12 inp-upload mt-3 col-md-4 text-right">
                                                        <h6 class="text-muted">فایل:</h6>
                                                        <input type="file" class="filepond" name="file"
                                                               data-allow-reorder="true"
                                                               data-max-file-size="200MB"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="product_id d-none"></span>
                                        <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center ">
                                            <div class="col-12 mt-3 col-md-3 text-center">
                                                <button type="submit" name="save" id="add" value="save"
                                                        class="btn btn-success w-75">
                                                    ثبت و افزودن تصویر/فایل
                                                </button>
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-center">
                                                <button type="submit" name="save" id="add-new" value="save-new"
                                                        class="btn btn-purple w-75">
                                                    ثبت و جدید
                                                </button>
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-center">
                                                <button type="submit" name="save" id="purchase" value="purchase"
                                                        class="d-none btn btn-primary w-50">ذخیره فاکتور
                                                </button>
                                            </div>
                                            <div class="col-12 mt-3 col-md-3 text-center">
                                                <a class="btn btn-danger w-50 "
                                                   href="{{route('product.index')}}">انصراف</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{--                                {!! Form::close() !!}--}}
                                <table class="table table-bordered table-responsive-md table-hover table-striped"
                                       id="products_table">
                                    <tr class="table-header">
                                        <th>نام محصول</th>
                                        <th>گروه کالا</th>
                                        <th>قیمت خرید(واحد)</th>
                                        <th>قیمت فروش(واحد)</th>
                                        <th>تعداد</th>
                                    </tr>
                                    @if($factor!=null)
                                        @foreach($factor->in_products as $pro)
                                            <tr>
                                                <th>{{$pro->product->name}}</th>
                                                <th>{{$pro->product->category->title}}</th>
                                                <th>{{$pro->product->buy_price}}</th>
                                                <th>{{$pro->product->sell_price}}</th>
                                                <th>{{$pro->product->quantity}}</th>
                                            </tr>
                                        @endforeach
                                    @endif

                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-factors" role="tabpanel"
                             aria-labelledby="factors">
                            <div class=" text-center bg-white m-0 w-100">
                                <h6 class="bg-primary text-white py-2">لیست فاکتورهای خرید</h6>
                            </div>
                            <div class="box-body">
                                @if(count($buyFactors)>0)
                                    <table
                                        class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                        <tr class="table-header">
                                            <th>شماره فاکتور</th>
                                            <th>فروشنده</th>
                                            <th>تاریخ</th>
                                            <th>مبلغ</th>
                                            <th>امکانات</th>
                                        </tr>
                                        @foreach($buyFactors as $buyFactor)
                                            <tr>
                                                <td>{{$buyFactor->f_num}}</td>
                                                <td>{{$buyFactor->provider->name}} {{$buyFactor->provider->family}}</td>
                                                <td>{{$buyFactor->f_date}}</td>
                                                <td>{{number_format($buyFactor->total_price)}}</td>
                                                <td>
                                                    <button class="btn btn-primary my-1" id="close" data-toggle="modal"
                                                            data-target="#showfactorModal-{{$buyFactor->f_num}}"
                                                            tabindex="-1" role="dialog"
                                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        جزئیات
                                                    </button>
                                                    <div class="modal fade" id="showfactorModal-{{$buyFactor->f_num}}"
                                                         tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                        جزئیات و ویرایش فاکتور</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <a href="{{route('editbuyFactor',$buyFactor->id)}}"
                                                                       class="btn btn-sm btn-success mb-2">افزودن جنس به
                                                                        این فاکتور</a>
                                                                    <table class="tbl table table-sm"
                                                                           id="table-list-{{$buyFactor->f_num}}">
                                                                        <tr>
                                                                            <th>محصول</th>
                                                                            <th>تعداد</th>
                                                                            <th>قیمت واحد</th>
                                                                            <th>قیمت کل</th>
                                                                            <th>امکانات</th>
                                                                        </tr>
                                                                        @foreach($buyFactor->in_products as $pro)
                                                                            <tr class="iner-tr">
                                                                                <td>{{$pro->product->name}}</td>
                                                                                <td class="{{$pro->product->id}}"
                                                                                    id="p_qty">{{$pro->qty}}</td>
                                                                                <td id="p_price">{{$pro->price}}</td>
                                                                                <td id="p_total">{{$pro->price*$pro->qty}}</td>
                                                                                <td>
                                                                                    @if(count($buyFactor->in_products)>0)
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_delete btn btn-danger  p-1">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </a>
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_edit btn btn-primary p-1">
                                                                                            <i class="fa fa-edit"></i>
                                                                                        </a>
                                                                                    @else
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_edit btn btn-primary p-1">
                                                                                            <i class="fa fa-edit"></i>
                                                                                        </a>
                                                                                        <a class=" btn btn-dark disabled  p-1">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </a>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>

                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                                data-dismiss="modal">بستن
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($buyFactor->pay_status==='unpaid')
                                                    <a class=" btn btn-warning"
                                                       href="{{route('factorPurchase',$buyFactor->f_num)}}">
                                                        <i class="fa fa-edit"></i>
                                                        ثبت پرداخت
                                                    </a>
                                                    @else
                                                        <a class=" btn btn-secondary disabled"
                                                           href="#">
                                                            <i class="fa fa-edit"></i>
                                                            پرداخت شده
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما فاکتور خریدی ندارید.
                                    </h6>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


@section('script')
{{--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
    <script src="{{asset('js/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            var table = document.getElementById('table-list');
            var valid = false;

            $('#barcode').focusout(function () {
                $('.barcode-error').remove()
                if ($('#barcode').val() !== "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'check-barcode-is-unique',
                            barcode: $('#barcode').val(),
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response == 'OK') {
                                valid = true;
                                $('.barcode-error').remove();
                            } else {
                                valid = false;
                                var element = '<span class="barcode-error text-danger mt-2">بارکد تکراری است</span>'
                                $('.barcode').append(element)
                            }

                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }

            })

            $('#f_date').on('change', function () {
                if ($('#f_date').val().length > 0) {
                    $('#f_date').removeAttr("required");
                } else {
                    $('#f_date').attr("required")
                }
            });

            $('#add-pro-form').submit(function (e) {
                e.preventDefault();
                var tbl = document.getElementById('products_table')
                var barcode = $('#barcode').val()
                var desc=CKEDITOR.instances.desc.getData();
                if (desc==""){
                    swal('فیلد توضیحات نمیتواند خالی باشد !')
                }else{
                    if (valid != false || barcode == "") {
                        var val = document.activeElement;
                        if (val.value != "" && val.value == 'save') {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: $('.ajax.d-none').attr('id'),
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr('content'),
                                    do: 'new-input-factor',
                                    flag: 'save',
                                    factor_number: $('#f_num').val(),
                                    factor_date: $('#f_date').val(),
                                    provider_id: $('#provider').val(),
                                    provider_name: $('#providers').val(),
                                    p_name: $('#p_name').val(),
                                    category: $('#cats').val(),
                                    buy_price: $('#buy_price').val(),
                                    sell_price: $('#sell_price').val(),
                                    whole_price: $('#whole_price').val(),
                                    qty: $('#qty').val(),
                                    guaranty: $('#guaranty').val(),
                                    guaranty_unit: $('#guaranty_unit').val(),
                                    guaranty_duration: $('#guaranty_duration').val(),
                                    guaranty_start: $('#guaranty_start').val(),
                                    description: CKEDITOR.instances.desc.getData(),
                                    keywords: $('#keywords').val(),
                                    barcode: $('#barcode').val(),
                                },
                                dataType: 'json',
                                success: function (response) {
                                    console.log(response)
                                    $('#total_price').val(response['total_price'])
                                    $('#purchase').removeClass('d-none')
                                    $('.product_id').attr('id', response['product']['id'])


                                    FilePond.registerPlugin();
                                    var element = document.querySelector('meta[name="csrf-token"]');
                                    var csrf = element && element.getAttribute("content");
                                    var product_id = $('.product_id').attr('id')
                                    var addresssss = "{{ route('UploadProductFiles',1)}}";
                                    FilePond.setOptions({
                                        server: {
                                            url: addresssss,
                                            process: {
                                                headers: {
                                                    'X-CSRF-TOKEN': csrf,
                                                    'id': product_id,
                                                },
                                            },
                                            revert: {
                                                headers: {
                                                    'X-CSRF-TOKEN': csrf,
                                                    'id': product_id,
                                                },
                                            },
                                        },
                                        allowRevert: true,
                                        labelIdle: "فایلها را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
                                        labelFileProcessing: 'در حال آپلود',
                                        labelFileProcessingComplete: 'تکمیل شد',
                                        labelFileProcessingAborted: 'لغو شد',
                                        labelFileProcessingError: 'خطا حین آپلود',
                                        labelTapToCancel: 'برای لغو اینجا کلیک کنید',
                                        labelTapToUndo: 'برای بازگشت اینجا کلیک کنید',
                                        credits: false,
                                    });

                                    const inputElement = document.querySelector('input[name="image"]');
                                    const pond = FilePond.create(inputElement);
                                    const inputElement1 = document.querySelector('input[name="file"]');
                                    const pond1 = FilePond.create(inputElement1);

                                    $('.inp-upload').removeClass('d-none')



                                    var row = tbl.insertRow(-1);
                                    var td1 = row.insertCell(0)
                                    var td2 = row.insertCell(1)
                                    var td3 = row.insertCell(2)
                                    var td4 = row.insertCell(3)
                                    var td5 = row.insertCell(4)
                                    td1.innerHTML = response['product']['name']
                                    td2.innerHTML = response['pro_category']
                                    td3.innerHTML = response['product']['buy_price']
                                    td4.innerHTML = response['product']['sell_price']
                                    td5.innerHTML = response['product']['quantity']
                                    $('#provider').val(response['providerId'])
                                    $('#add').addClass('d-none')

                                },
                                error: function (response) {
                                    console.log(response)
                                }
                            });
                        } else if (val.value != "" && val.value == 'save-new') {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: $('.ajax.d-none').attr('id'),
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr('content'),
                                    do: 'new-input-factor',
                                    flag: 'savenew',
                                    factor_number: $('#f_num').val(),
                                    factor_date: $('#f_date').val(),
                                    provider_id: $('#provider').val(),
                                    provider_name: $('#providers').val(),
                                    p_name: $('#p_name').val(),
                                    category: $('#cats').val(),
                                    buy_price: $('#buy_price').val(),
                                    sell_price: $('#sell_price').val(),
                                    whole_price: $('#whole_price').val(),
                                    qty: $('#qty').val(),
                                    guaranty: $('#guaranty').val(),
                                    guaranty_unit: $('#guaranty_unit').val(),
                                    guaranty_duration: $('#guaranty_duration').val(),
                                    guaranty_start: $('#guaranty_start').val(),
                                    description: CKEDITOR.instances.desc.getData(),
                                    keywords: $('#keywords').val(),
                                    barcode: $('#barcode').val(),
                                },
                                dataType: 'json',
                                success: function (response) {
                                    console.log(response)

                                    $('#total_price').val(response['total_price'])
                                    $('#add').removeClass('d-none')

                                    if (response['status'] == 'new') {
                                        $('.inp-upload').addClass('d-none')
                                        document.getElementById('add-pro-form').scrollIntoView();
                                        $('#p_name').focus();
                                        $('#p_name').val("");
                                        $('#cats').val("");
                                        $('#buy_price').val("");
                                        $('#sell_price').val("");
                                        $('#whole_price').val("");
                                        $('#qty').val("");
                                        $('#barcode').val("");
                                        $('#keywords').val("")
                                        CKEDITOR.instances.desc.setData("")

                                    } else {
                                        $('#purchase').removeClass('d-none')
                                        $('.product_id').attr('id', response['product']['id'])

                                        FilePond.registerPlugin();
                                        var element = document.querySelector('meta[name="csrf-token"]');
                                        var csrf = element && element.getAttribute("content");
                                        var product_id = $('.product_id').attr('id')
                                        var addresssss = "{{ route('UploadProductFiles',1)}}";
                                        FilePond.setOptions({
                                            server: {
                                                url: addresssss,
                                                process: {
                                                    headers: {
                                                        'X-CSRF-TOKEN': csrf,
                                                        'id': product_id,
                                                    },
                                                },
                                                revert: {
                                                    headers: {
                                                        'X-CSRF-TOKEN': csrf,
                                                        'id': product_id,
                                                    },
                                                },
                                            },
                                            allowRevert: false,
                                            labelIdle: "فایلها را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
                                            labelFileProcessing: 'در حال آپلود',
                                            labelFileProcessingComplete: 'تکمیل شد',
                                            labelFileProcessingAborted: 'لغو شد',
                                            labelFileProcessingError: 'خطا حین آپلود',
                                            labelTapToCancel: 'برای لغو اینجا کلیک کنید',
                                            labelTapToUndo: 'برای بازگشت اینجا کلیک کنید',
                                            credits: false,
                                        });

                                        const inputElement = document.querySelector('input[name="image"]');
                                        const pond = FilePond.create(inputElement);
                                        const inputElement1 = document.querySelector('input[name="file"]');
                                        const pond1 = FilePond.create(inputElement1);

                                        $('.inp-upload').addClass('d-none')

                                        var row = tbl.insertRow(-1);
                                        var td1 = row.insertCell(0)
                                        var td2 = row.insertCell(1)
                                        var td3 = row.insertCell(2)
                                        var td4 = row.insertCell(3)
                                        var td5 = row.insertCell(4)
                                        td1.innerHTML = response['product']['name']
                                        td2.innerHTML = response['product']['cat_id']
                                        td3.innerHTML = response['product']['buy_price']
                                        td4.innerHTML = response['product']['sell_price']
                                        td5.innerHTML = response['product']['quantity']
                                        $('#provider').val(response['providerId'])
                                        document.getElementById('add-pro-form').scrollIntoView();
                                        $('#p_name').focus();
                                        $('#p_name').val("");
                                        $('#cats').val("");
                                        $('#buy_price').val("");
                                        $('#sell_price').val("");
                                        $('#whole_price').val("");
                                        $('#qty').val("");
                                        $('#barcode').val("");
                                        $('#keywords').val("")
                                        CKEDITOR.instances.desc.setData("")
                                    }
                                },
                                error: function (response) {
                                    console.log(response)
                                }
                            });
                        }

                    } else {
                        $('#barcode').focus()
                        location.href = "#barcode";
                    }
                }

            });


            function autocomplete(inp, arr) {
                var existingElement = document.getElementById("providersautocomplete-list")
                console.log(existingElement)
                if (existingElement)
                    existingElement.remove()


                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;

                var a, b = null
                /*, val = this.value;
                                /!*close any already open lists of autocompleted values*!/
                                closeAllLists();
                                if (!val) {
                                    return false;
                                }*/
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary rounded bg-light py-2 px-1");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (var i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i]['family'] != null) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + " " + arr[i]['family'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + " " + arr[i]['family'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            provider = document.getElementById('provider')
                            provider.value = this.getElementsByTagName("input")[0].id
                            console.log(provider)
                            document.getElementById("provider").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    } else {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            provider = document.getElementById('provider')
                            provider.value = this.getElementsByTagName("input")[0].id
                            console.log(provider)
                            document.getElementById("provider").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }


                /*execute a function when someone writes in the text field:*/
                /*inp.addEventListener("input", function (e) {

                });*/

                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });

                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }

                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }

                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }

                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }

            function autocompletecat(inp, arr) {
                var existingElement = document.getElementById("catsautocomplete-list")
                console.log(existingElement)
                if (existingElement)
                    existingElement.remove()

                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;

                var a, b = null
                /*, val = this.value;
                                /!*close any already open lists of autocompleted values*!/
                                closeAllLists();
                                if (!val) {
                                    return false;
                                }*/
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary rounded bg-light py-2 px-1");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (var i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i]['title'] + "</strong>";
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['title'] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        cat = document.getElementById('cat')
                        cat.value = this.getElementsByTagName("input")[0].id
                        document.getElementById("cat").value = this.getElementsByTagName("input")[0].id
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }

                /*execute a function when someone writes in the text field:*/
                /*inp.addEventListener("input", function (e) {

                });*/

                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });

                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }

                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }

                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }

                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }


            var inp = document.getElementById('providers')
            inp.addEventListener("input", function () {
                if ($('#providers').val() != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            key: $('#providers').val(),
                            do: 'search-provider'
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            if (response != "") {
                                autocomplete(document.getElementById("providers"), response);
                            } else {
                                $('#provider').val("")
                            }

                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                } else {
                    var existingElement = document.getElementById("providersautocomplete-list")
                    console.log(existingElement)
                    if (existingElement)
                        existingElement.remove()
                }
            });

            var cats = document.getElementById('cats')
            cats.addEventListener("input", function () {
                if ($('#cats').val() != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            key: $('#cats').val(),
                            do: 'search-cat'
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response != "")
                                autocompletecat(document.getElementById("cats"), response);
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                } else {
                    var existingElement = document.getElementById("catsautocomplete-list")
                    console.log(existingElement)
                    if (existingElement)
                        existingElement.remove()
                }
            });


            $("#cat").on("keyup", function () {
                var data = null
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        key: $('#cat').val(),
                        do: 'search-cat'
                    },
                    dataType: 'json',
                    success: function (response) {
                        data = response
                        autocompleteCat(document.getElementById("cat"), data);
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });

            });


            $('.p_edit').click(function () {
                var indexx = $(this).parent().parent().index()
                var modal = $('.modal.show').attr('id')
                var tableid = modal.replace(/showfactorModal-/g, "")
                var table = document.getElementById('table-list-' + tableid)
                var row = table.insertRow(indexx + 1)
                var td0 = row.insertCell(0)
                var td1 = row.insertCell(1)
                var td2 = row.insertCell(2)
                var td3 = row.insertCell(3)
                td0.innerHTML = '<input class="form-control" type="text" id="edit-qty" placeholder="تعداد">'
                td1.innerHTML = '<input class="form-control" type="text" id="edit-price" placeholder="قیمت">'
                td2.innerHTML = '<button type="button" class="btn btn-sm btn-success" id="update-row">ثبت</button>'
                td3.innerHTML = '<button type="button" class="btn btn-sm btn-secondary" id="cancel">انصراف</button>'
                var id = this.id

                $('#update-row').click(function () {
                    var upIndex = $(this).parent().parent().index()
                    var qty = document.getElementById('edit-qty').value
                    var price = document.getElementById('edit-price').value
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'update-buy-factor-pro',
                            id: id,
                            qty: qty,
                            price: price,
                        },
                        dataType: 'json',
                        success: function (response) {
                            var modal = $('.modal.show').attr('id')
                            var tableid = modal.replace(/showfactorModal-/g, "")
                            var table = document.getElementById('table-list-' + tableid)
                            // var tbl = $('.tbl')
                            var rows = $('tr', table);
                            var preRow = rows.eq(upIndex - 1);
                            var td1 = preRow.children("#p_qty")
                            var td2 = preRow.children("#p_price")
                            var td3 = preRow.children("#p_total")
                            td1.text(response['qty'])
                            td2.text(response['price'])
                            td3.text(response['qty'] * response['price'])
                            table.deleteRow(upIndex)
                            console.log(response)
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });

                })
                $('#cancel').click(function () {
                    var canIndex = $(this).parent().parent().index()
                    table.deleteRow(canIndex)
                })

            })

            $('.p_delete').click(function () {
                var modal = $('.modal.show').attr('id')
                var tableid = modal.replace(/showfactorModal-/g, "")
                var currenttable = document.getElementById('table-list-' + tableid)
                var pro_id = this.id
                var currentrow = $('.p_delete').parent().parent()
                var currentrowindex = $(this).parent().parent().index()
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'delete-buy-factor-pro',
                        id: pro_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        currenttable.deleteRow(currentrowindex)
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('#purchase').click(function () {
                var f_num = $('#f_num').val()
                window.location.replace("../factor/purchases/" + f_num);
            })

            $('#f_date').persianDatepicker({
                altField: '#f_date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,

            });

            $('#guaranty').click(function (event) {
                var flag = this.value
                if (flag == "true") {
                    $('.guaranty-section').removeClass('d-none')
                } else {
                    $('.guaranty-section').addClass('d-none')
                }
            });
        })
    </script>

@endsection

