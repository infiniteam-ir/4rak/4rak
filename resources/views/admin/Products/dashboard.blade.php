@extends('admin.layouts.newTheme.master')


@section('main')
    <div class="row px-2 my-3 d-flex flex-nowrap overflow-auto">
        <div class="col mx-1">
            <a href="{{route('product.index')}}">
                <div class="btn btn-purple  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/products-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white text-nowrap" >مدیریت کالا</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class=" col   mx-1">
            <a href="{{route('category.index')}}">
                <div class="btn btn-warning  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/categories-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white text-nowrap">مدیریت گروه کالا</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class=" col   mx-1">
            <a href="{{route('product.create')}}">
                <div class="btn btn-info  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/buy-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white text-nowrap">خرید کالا</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class=" col   mx-1">
            <a href="{{route('sell')}}">
                <div class="btn btn-pinterest  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/sell-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white text-nowrap">فروش کالا</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class=" col   mx-1">
            <a href="{{route('guaranty')}}">
                <div class="btn btn-success  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/guarantee-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white"> گارانتی</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class=" col   mx-1">
            <a href="{{route('service.index')}}">
                <div class="btn btn-primary  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/customer-service-white.svg')}}" alt="4rak" class="img-fluid rounded" >
                        <h6 class="text-white text-nowrap"> مدیریت خدمات</h6>
                    </div>
                </div>
            </a>
        </div>
    </div>
    @if(Session::has('error'))
        <div class="alert alert-danger mt-1 text-center">
            <h4 class="text-danger text-center">
                {{Session::pull('error')}}
            </h4>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">گروه های کالا</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$cat_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$cat_qty}} </h2>
                        <p class="text-muted">گروه کالا</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
{{--                        <i class="zmdi zmdi-more-vert"></i>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">موجودی کالاهای شما</h4>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#2891a0 "
                               data-bgColor="#aacdd2" value="{{$product_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$product_qty}} </h2>
                        <p class="text-muted"> کالا</p>
                    </div>
                </div>

            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
{{--                        <i class="zmdi zmdi-more-vert"></i>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">آمار</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                               data-bgColor="#FFE6BA" value="{{$total_online_sells[0]}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$total_online_sells[0]}} </h2>
                        <p class="text-muted"> فروش آنلاین امروز</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        {{--                        <i class="zmdi zmdi-more-vert"></i>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">آمار</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                               data-bgColor="#FFE6BA" value="{{$total_offline_sells[0]}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>
                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$total_offline_sells[0]}} </h2>
                        <p class="text-muted"> فروش آفلاین امروز</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        {{--                        <i class="zmdi zmdi-more-vert"></i>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">آمار فروش آنلاین هفتگی</h4>
                {{--                <div id="morris-line-example" style="height: 280px;"></div>--}}
                <canvas id="total-online-sell" class=""></canvas>
            </div>
        </div><!-- end col -->

<!--        <div class="col-lg-4">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">آمارها</h4>
                <div id="morris-bar-example" style="height: 280px;"></div>
            </div>
        </div>-->
        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
{{--                        <i class="zmdi zmdi-more-vert"></i>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">آمار فروش آفلاین هفتگی</h4>
{{--                <div id="morris-line-example" style="height: 280px;"></div>--}}
                <canvas id="total-sell" class=""></canvas>
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->


    <div class="row my-4 ">
        <div class="col-12">
            <div class="w-100 text-center  "><h6 class="text-muted mb-3">پرفروش ترین محصولات شما:</h6></div>
        </div>
        @foreach($most_sold_products as $pro)
        <div class="col-lg-3 col-md-6">
            <div class="card-box widget-user">
                <div>
                    @if($pro->photo)
                    <img src="{{asset('images/products/'.$pro->photo->path)}}" class="img-responsive p-2 img-circle rounded" alt="محصول">
                    @else
                        <img src="{{asset('img/default.jpg')}}" class="img-responsive img-circle rounded p-2" alt="محصول">
                    @endif
                    <div class="wid-u-info">
                        <h5 class="m-t-0 m-b-5 font-600">{{$pro->name}}</h5>
                        <p class="text-muted m-b-5 font-13"></p>
                        <small class="text-success"><b>{{$pro->sold_qty}} فروش</b></small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
        <!-- end col -->
    </div>
    <!-- end row -->


    <div class="row">
<!--        <div class="col-lg-4">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">جعبه دریافتی</h4>

                <div class="inbox-widget nicescroll" style="height: 315px;">
                    <a href="#">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg" class="img-circle" alt=""></div>
                            <p class="inbox-item-author">کاربر اول</p>
                            <p class="inbox-item-text">سلام دوست من این یک متن تستی است</p>
                            <p class="inbox-item-date">13:40 ب:ظ</p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/avatar-2.jpg" class="img-circle" alt=""></div>
                            <p class="inbox-item-author">کاربر دوم</p>
                            <p class="inbox-item-text">سلام دوست من این یک متن تستی است</p>
                            <p class="inbox-item-date">13:34 ب:ظ</p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/avatar-3.jpg" class="img-circle" alt=""></div>
                            <p class="inbox-item-author">کاربر سوم</p>
                            <p class="inbox-item-text">سلام دوست من این یک متن تستی است</p>
                            <p class="inbox-item-date">13:17 ق::ظ</p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/avatar-4.jpg" class="img-circle" alt=""></div>
                            <p class="inbox-item-author">کاربر چهارم</p>
                            <p class="inbox-item-text">سلام دوست من این یک متن تستی است</p>
                            <p class="inbox-item-date">12:20 ب:ظ</p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/avatar-5.jpg" class="img-circle" alt=""></div>
                            <p class="inbox-item-author">کاربر پنجم</p>
                            <p class="inbox-item-text">سلام دوست من این یک متن تستی است</p>
                            <p class="inbox-item-date">10:15 ق:ظ</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>-->
        <!-- end col -->

        <div class="col-lg-12">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0 m-b-30 text-warning"> محصولات رو به اتمام</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>نام محصول</th>
                            <th>گروه محصول</th>
                            <th>تعداد</th>

                        </tr>
                        </thead>
                        <tbody>
                        @php($row=0)
                        @foreach($low_qty_products as $pro)
                            @php($row++)
                        <tr>
                            <td>{{$row}}</td>
                            <td>{{$pro->name}}</td>
                            <td>{{$pro->category->title}}</td>
                            <td><span class="label label-danger rounded text-white">{{$pro->quantity}}</span></td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="row d-flex justify-content-center">{{$low_qty_products->links()}}</div>

            </div>

        </div><!-- end col -->

    </div>
    <!-- end row -->

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var chart1 = document.getElementById('total-sell');
            var chart2 = document.getElementById('total-online-sell');
            let start = new Date()
            let end = new Date();
            let max_off={{$max_offline_sell}};
            let max_on={{$max_online_sell}};

            start.setDate(start.getDate() - 6); // set to 'now' minus 7 days.
            start.setHours(0, 0, 0, 0); // set to midnight.
            let off_today={{$total_offline_sells[0]}};
            let off_today1={{$total_offline_sells[1]}};
            let off_today2={{$total_offline_sells[2]}};
            let off_today3={{$total_offline_sells[3]}};
            let off_today4={{$total_offline_sells[3]}};
            let off_today5={{$total_offline_sells[5]}};
            let off_today6={{$total_offline_sells[0]}};

            let on_today={{$total_online_sells[0]}};
            let on_today1={{$total_online_sells[1]}};
            let on_today2={{$total_online_sells[2]}};
            let on_today3={{$total_online_sells[3]}};
            let on_today4={{$total_online_sells[3]}};
            let on_today5={{$total_online_sells[5]}};
            let on_today6={{$total_online_sells[0]}};

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                // url: $('.ajax.d-none').attr('id'),
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'getLast7Days',
                },
                dataType: 'json',
                success: function (response) {
                    if (max_off>0){
                        let sell_Chart = new Chart(chart1 , {
                            type: 'line',
                            data: {
                                labels: response.reverse(),
                                datasets: [{
                                    label: '',
                                    backgroundColor:'#89b2ef',
                                    borderColor:'#266fd0',
                                    pointBackgroundColor:'#217f9b',
                                    lineTension:'.3',
                                    data: [off_today6,off_today5,off_today4,off_today3,off_today2,off_today1,off_today]
                                    // data: [10,30,15,50,34,12,10]

                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: max_off+(10*max_off/100),
                                            min: 0,
                                            stepSize: Math.ceil(max_off/10)
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        });
                    }else {
                        let sell_Chart = new Chart(chart1 , {
                            type: 'line',
                            data: {
                                labels: response.reverse(),
                                datasets: [{
                                    label: '',
                                    backgroundColor:'#89b2ef',
                                    borderColor:'#266fd0',
                                    pointBackgroundColor:'#217f9b',
                                    lineTension:'.3',
                                    data: [off_today6,off_today5,off_today4,off_today3,off_today2,off_today1,off_today]
                                    // data: [10,30,15,50,34,12,10]

                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: 10,
                                            min: 0,
                                            stepSize: 2
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        });
                    }

                    if (max_on>0){
                        let online_sell_Chart = new Chart(chart2 , {
                            type: 'line',
                            data: {
                                labels: response,
                                datasets: [{
                                    label: '',
                                    backgroundColor:'#3dd79a',
                                    borderColor:'#0c923e',
                                    pointBackgroundColor:'#167225',
                                    lineTension:'.3',
                                    data: [on_today6,on_today5,on_today4,on_today3,on_today2,on_today1,on_today]
                                    // data: [10,30,15,50,34,12,10]
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: max_on+(max_on*5/100),
                                            min: 0,
                                            stepSize: max_on/10
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        })
                    }else {
                        let online_sell_Chart = new Chart(chart2 , {
                            type: 'line',
                            data: {
                                labels: response,
                                datasets: [{
                                    label: '',
                                    backgroundColor:'#3dd79a',
                                    borderColor:'#0c923e',
                                    pointBackgroundColor:'#167225',
                                    lineTension:'.3',
                                    data: [on_today6,on_today5,on_today4,on_today3,on_today2,on_today1,on_today]
                                    // data: [10,30,15,50,34,12,10]
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            max: 10,
                                            min: 0,
                                            stepSize: 2
                                        }
                                    }],
                                    xAxes: [{
                                    }]
                                },
                                legend:false
                            }
                        })
                    }
                },
                error: function (response) {
                    console.log('error')
                }
            });

        })
    </script>
@endsection
