@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <div class="modal fade" id="closing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">بستن فاکتور</h5>
                </div>
                <div class="modal-body">
                    <h4>
                        کل مبلغ این فاکتور: <span class="text-bold" id="modal_total"></span> تومان
                    </h4>
                    <h4>
                        کل مبلغ دریافتی: <span class="text-bold" id="modal_paid"></span> تومان
                    </h4>
                    <h4>
                        <span class="text-success" id="modal_msg"></span>
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <a href="{{route('dashboard')}}" class="btn btn-success w-50">تایید</a>
                </div>
            </div>
        </div>
    </div>


    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <input class="d-none" id="customer_account" value="{{$factor->customer_id}}">
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">دریافت هزینه فاکتور</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-header mt-0 bg-white">
                <div class="col justify-content-center p-0 ">
                    <div class="nav  d-flex justify-content-center ">
                        <div class="alert alert-primary w-100">
                            <h3>
                                مبلغ کل فاکتور: <span id="total">{{$factor->total_price}}</span> تومان
                            </h3>
                            <h3>
                                دریافت شده تاکنون: <span id="paid">
                                    {{$paid}}
                                </span> تومان
                            </h3>
                            <span class="factor_id" id="{{$factor->f_num}}"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row p-4 d-flex pb-5">
                    <div class="row w-100 mt-3 justify-content-center align-items-center ">
                        <div class="col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">نوع دریافت:</h6>
                            <select name="acc_type" class="form-control" id="acc_type">
                                <option value="">انتخاب کنید</option>
                                <option value="cash">نقدی</option>
                                <option value="cheque">چک</option>
                            </select>
                        </div>
                        <div class="col-12 cash-section mt-3 col-md-4 text-right">
                            <h6 class="text-muted">حساب:</h6>
                            <select name="account" class="form-control" id="">
                                @foreach($accounts as $account)
                                    <option id="account_id" value="{{$account->id}}">
                                        {{$account->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">مبلغ:</h6>
                            {!! Form::text('price',null,['class'=>'form-control','id'=>'price']) !!}
                        </div>
                        <div class="cheque-section   row w-100 mt-3 justify-content-center align-items-center">
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">صاحب حساب:</h6>
                                {!! Form::text('owner',null,['class'=>'form-control','id'=>'owner']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">در وجه:</h6>
                                {!! Form::text('pay_to',null,['class'=>'form-control','id'=>'pay_to']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">سریال چک:</h6>
                                {!! Form::text('serial',null,['class'=>'form-control','id'=>'serial']) !!}
                            </div>

                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">تاریخ:</h6>
                                {!! Form::text('pay-date',null,['class'=>'form-control','id'=>'pay-date']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">توضیحات:</h6>
                                {!! Form::text('note',null,['class'=>'form-control','id'=>'new-note']) !!}
                            </div>
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-center">
                            <h6 class="text-muted"></h6>
                            {!! Form::submit('ثبت پرداخت',['class'=>'form-control btn btn-success w-50','id'=>'submit']) !!}
                            {{--                            <a class="btn btn-primary" href="{{route('savePayment',$factor->f_num)}}">بستن</a>--}}
                            <button class="btn btn-primary" id="close" data-toggle="modal" data-target="#closing"
                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                بستن فاکتور
                            </button>
                        </div>
                    </div>

                </div>
                <table id="table"
                       class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2">
                    <tr class="table-header">
                        <th>نوع درآمد</th>
                        <th>منبع</th>
                        <th>قیمت</th>
                        <th>تاریخ</th>
                    </tr>
                    @foreach($incomes as $income )
                        @php($paid+=$income->price)
                        <tr>
                            <td>{{$income->source_type}}</td>
                            <td>{{$income->source_id}}</td>
                            <td>{{$income->price}}</td>
                            <td>{{$income->created_at}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#pay-date').persianDatepicker({
                altField: '#pay-date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
            });
            var ajaxUrl = $('.ajax.d-none').attr('id')
            var factor_id = document.getElementsByClassName('factor_id')[0].id
            var table = document.getElementById('table');
            $('.cheque-section').addClass('d-none')

            $('#submit').click(function (event) {
                var customer_account = document.getElementById('customer_account').value
                var price = parseInt(document.getElementById('price').value)
                var account = document.getElementById('account_id').value
                var id = event.target.value;
                var paid = parseInt(document.getElementById('paid').innerHTML)
                var total = parseInt(document.getElementById('total').innerHTML)
                var sum = paid + price
                var pay_type = document.getElementById('acc_type').value
                if (pay_type == 'cash') {
                    if (sum <= total && price !=0) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: ajaxUrl,
                            data: {
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do: 'get-payment',
                                id: factor_id,
                                price: price,
                                account_id: account,
                                paid: paid,
                                customer_account: customer_account
                            },
                            dataType: 'json',
                            success: function (response) {
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                td0.innerHTML = response['source_type']
                                td1.innerHTML = response['source_id']
                                td2.innerHTML = parseInt(response['price']);
                                td3.innerHTML = response['created_at']
                                document.getElementById('paid').innerHTML = paid + parseInt(response['price'])
                            },
                            error: function (response) {
                                console.log(response)
                            }
                        });
                    } else {
                        alert('مقدار وارد شده از مانده حساب بیشتر است.')
                    }
                } else if (pay_type == 'cheque') {
                    if (sum <= total && price !=0) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: ajaxUrl,
                            data: {
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do: 'other-cheque-input',
                                id: factor_id,
                                amount: price,
                                account_id: account,
                                paid: paid,
                                customer_account: customer_account,
                                serial: document.getElementById('serial').value,
                                owner: document.getElementById('owner').value,
                                pay_date: document.getElementById('pay-date').value,
                                pay_to: document.getElementById('pay_to').value,
                                note: document.getElementById('new-note').value,
                            },
                            dataType: 'json',
                            success: function (response) {
                                var row = table.insertRow(-1);
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                td1.innerHTML = response['source_type']
                                td2.innerHTML = response['source_id']
                                td3.innerHTML = response['price']
                                td4.innerHTML = response['created_at']
                                document.getElementById('paid').innerHTML = paid + parseInt(response['price'])
                                console.log(response)
                                price = ""
                                document.getElementById('serial').value = ""
                                document.getElementById('owner').value = ""
                                document.getElementById('pay-date').value = ""
                                document.getElementById('pay_to').value = ""
                                document.getElementById('new-note').value =""

                            },

                            error: function (response) {
                                console.log(response)
                            }
                        });
                    } else {
                        alert('مقدار وارد شده از مانده حساب بیشتر است.')
                    }
                }
            });

            $('#close').click(function (event) {
                var total_price = parseInt(document.getElementById('total').innerHTML)
                var paid = parseInt(document.getElementById('paid').innerHTML)
                document.getElementById('modal_total').innerHTML = total_price
                document.getElementById('modal_paid').innerHTML = paid
                if (paid < total_price) {
                    document.getElementById('modal_msg').innerHTML = 'طلب شما ثبت شد.'
                } else {
                    document.getElementById('modal_msg').innerHTML = ''
                }
            })

            $('#acc_type').on("change", function () {
                var type = this.value
                if (type == 'cash') {
                    $('.cheque-section').addClass('d-none')
                    $('.cash-section').removeClass('d-none')
                }else if (type == 'cheque'){
                    $('.cash-section').addClass('d-none')
                    $('.cheque-section').removeClass('d-none')
                }
            })
        })
    </script>
@endsection


