@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <div class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-exclamation-triangle text-warning"></i> عدم داشتن تعداد کافی
                        <i class="fa fa-exclamation-triangle text-warning"></i></h5>
                </div>
                <div class="modal-body">
                    موجودی کالای انتخاب شده در فروشگاه کافی نیست، در لیست زیر موجودی این کالا در انبارها نشان داده شده
                    است.
                    <table class="table table-sm table-hover table-striped table-info table-responsive-md table-bordered mt-2"
                           id="modal-table">
                        <tr class="">
                            <th>نام انبار</th>
                            <th>تعداد موجود از این کالا</th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">فروش کالا</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow  text-center">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="container">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            @if(Route::CurrentRouteName()=='sell')
                            <a class="nav-item nav-link active" id="new" data-toggle="tab" href="#nav-new" role="tab"
                               aria-controls="nav-new" aria-selected="true">فاکتور جدید</a>
                            @else
                                <a class="nav-item nav-link active" id="new" data-toggle="tab" href="#nav-new" role="tab"
                                   aria-controls="nav-new" aria-selected="true">ویرایش فاکتور</a>
                                @endif

                            <a class="nav-item nav-link" id="factors" data-toggle="tab" href="#nav-factors"
                               role="tab"
                               aria-controls="nav-factors" aria-selected="false">لیست فاکتور ها</a>
                        </div>
                    </nav>
                    <div class="tab-content pt-0" id="nav-tabContent">
                        <div class="tab-pane fade show active p-3" id="nav-new" role="tabpanel" aria-labelledby="new">
                            <div class="box-body">
                                <form action="" method="post" id="sell-form" >
                                <div class="row px-4 d-flex pb-1">
                                    <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted">شماره فاکتور:</h6>
                                            @if(Route::CurrentRouteName()=='sell')
                                            <input type="text" name="f_num" value="{{$f_num}}" class="form-control" id="f_num" required oninvalid="this.setCustomValidity('لطفا شماره فاکتور را وارد کنید')" oninput="setCustomValidity('')">
                                                @else
                                                <input type="text" name="f_num" value="{{$f_num}}" class=" d-none form-control" id="f_num" required oninvalid="this.setCustomValidity('لطفا شماره فاکتور را وارد کنید')" oninput="setCustomValidity('')">
                                                <label class="form-control">{{$f_num}}</label>
                                            @endif
                                        </div>
                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted">تاریخ:</h6>
                                            @if(Route::CurrentRouteName()=='sell')
                                                @php($now=Verta::now()->formatdate())
{{--                                            {!! Form::text('f_date',null,['class'=>'form-control','id'=>'f_date']) !!}--}}
                                                <input type="text" name="f_date"  class="form-control" id="f_date" required oninvalid="this.setCustomValidity('لطفا تاریخ فاکتور را وارد کنید')" oninput="setCustomValidity('')">
                                            @else
{{--                                                {!! Form::text('f_date',$factor->f_date,['class'=>'form-control','id'=>'f_date']) !!}--}}
                                                <input type="text" name="f_date" value="{{$factor->f_date}}"  class="form-control" id="f_date" required oninvalid="this.setCustomValidity('لطفا تاریخ فاکتور را وارد کنید')" oninput="setCustomValidity('')">

                                            @endif

                                        </div>

                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted">خریدار:</h6>
                                            <div class="autocomplete" style="width:300px;">
                                                <input type="text" id="customer" class="customer_id d-none">
                                            @if(Route::CurrentRouteName()=='sell')
{{--                                                {!! Form::text('customer',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'customers']) !!}--}}
                                                    <input type="text" name="customer" autocomplete="off"  class="form-control" id="customers" required oninvalid="this.setCustomValidity('لطفا نام مشتری  را وارد کنید')" oninput="setCustomValidity('')">
                                                @else
{{--                                                    {!! Form::text('customer',$factor->customer->name." ".$factor->customer->family,['Autocomplete'=>'off','class'=>'form-control','id'=>'customers']) !!}--}}
                                                    <input type="text" name="customer" value="{{$factor->customer->name." ".$factor->customer->family}}" autocomplete="off"  class="form-control" id="customers" required oninvalid="this.setCustomValidity('لطفا نام مشتری را وارد کنید')" oninput="setCustomValidity('')">

                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row w-100 mt-3 justify-content-center align-items-center ">
                                        <span class="ajax d-none" id="{{route('ajax')}}"></span>
                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted">عنوان محصول:</h6>
                                            <select class="form-control" name="p_id" id="ajaxqty">
                                                <option value="">انتخاب محصول</option>
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}" id="pro" class="ajaxqty">
                                                        {{$product->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted"> تعداد:</h6>
{{--                                            {!! Form::text('qty',null,['class'=>'form-control','id'=>'qty']) !!}--}}
                                            <input type="number" name="qty" autocomplete="off"  class="form-control" id="qty" required oninvalid="this.setCustomValidity('لطفا تعداد محصول را وارد کنید')" oninput="setCustomValidity('')">
                                        </div>
                                        <div class="col-12 mt-3 col-md-4 text-right">
                                            <h6 class="text-muted">قیمت فروش(تومان):</h6>
{{--                                            {!! Form::text('sell_price',null,['class'=>'form-control' ,'id'=>'price']) !!}--}}
                                            <input type="number" name="ر" autocomplete="off"  class="form-control" id="price" required oninvalid="this.setCustomValidity('لطفا قیمت محصول را وارد کنید')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                    <div id="guaranty_section"
                                         class="d-none guaranty row w-100 mt-5 justify-content-center align-items-center ">
                                        <div class="col-12 mt-3 mx-2 col-md-5 alert d-flex justify-content-center  alert-success">
                                            <h6 class="text-muted ">این کالا <span id="duration"></span> <span
                                                        id="d-unit"></span>
                                                گارانتی از تاریخ <span id="start"></span> دارد.</h6>
                                        </div>
                                        <div class="col-12 mt-3 mx-2 col-md-5 d-inline-flex alert alert-success">
                                            <h6 class="text-muted">اعمال گارانتی:</h6>
                                            <select id="guaranty-accept" name="guaranty" class="form-control w-25 mx-3">
                                                <option value="1">بله</option>
                                                <option value="0">خیر</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row w-100 mt-2 mb-5 justify-content-center align-items-center ">
                                        <div class="col-12 mt-3 col-md-4 text-center">
                                            <button type="submit" class="btn btn-primary" id="add">افزودن</button>
                                            <a class="btn btn-danger" href="{{route('product.index')}}">انصراف</a>
                                            @if(Route::CurrentRouteName()=='sell')
                                            <a href="{{route('sellFactor',$f_num)}}" name="save"
                                               class="btn btn-success d-none" id="savebtn">ذخیره</a>
                                                @else
                                                <a href="{{route('sellFactor',$f_num)}}" name="save"
                                                   class="btn btn-success" id="savebtn">ذخیره</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <table id="table"
                                   class="table table-bordered table-responsive-md table-hover table-striped table-header">
                                <tr class="table-header">
                                    <th>نام محصول</th>
                                    <th>تعداد</th>
                                    <th>قیمت</th>
                                    <th>قیمت کل</th>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-factors" role="tabpanel"
                             aria-labelledby="factors">
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-primary text-white py-2">لیست فاکتورهای فروش</h6>
                            </div>
                            <div class="box-body">
                                @if(count($sellFactors)>0)
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                        <tr class="table-header">
                                            <th>شماره فاکتور</th>
                                            <th>خریدار</th>
                                            <th>تاریخ</th>
                                            <th>مبلغ</th>
                                            <th>امکانات</th>
                                        </tr>
                                        @foreach($sellFactors as $sellFactor)
                                            <tr>
                                                <td>{{$sellFactor->f_num}}</td>
                                                <td>{{$sellFactor->customer->name}} {{$sellFactor->customer->family}}</td>
                                                <td>{{$sellFactor->f_date}}</td>
                                                <td>{{number_format($sellFactor->total_price)}}</td>
                                                <td>
                                                    <button class="btn btn-primary" id="close" data-toggle="modal"
                                                            data-target="#showfactorModal-{{$sellFactor->id}}"
                                                            tabindex="-1" role="dialog"
                                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        جزئیات
                                                    </button>
                                                    <div class="modal fade" id="showfactorModal-{{$sellFactor->id}}"
                                                         tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                        جزئیات و ویرایش فاکتور</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <a href="{{route('editsellFactor',$sellFactor->id)}}"
                                                                       class="btn btn-sm btn-success mb-2">افزودن جنس به
                                                                        این فاکتور</a>
                                                                    <table class="table table-sm" id="table-list-{{$sellFactor->id}}">
                                                                        <tr>
                                                                            <th>محصول</th>
                                                                            <th>تعداد</th>
                                                                            <th>قیمت واحد</th>
                                                                            <th>قیمت کل</th>
                                                                            <th>امکانات</th>
                                                                        </tr>
                                                                        @foreach($sellFactor->out_products as $pro)
                                                                            <tr>
                                                                                <td>{{$pro->product->name}}</td>
                                                                                <td class="{{$pro->product->id}}"
                                                                                    id="p_qty">{{$pro->qty}}</td>
                                                                                <td id="p_price">{{$pro->price}}</td>
                                                                                <td id="p_total">{{$pro->price*$pro->qty}}</td>
                                                                                <td>
                                                                                    @if(count($sellFactor->out_products)>0)
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_delete btn btn-danger  p-1">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </a>
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_edit btn btn-primary p-1">
                                                                                            <i class="fa fa-edit"></i>
                                                                                        </a>
                                                                                    @else
                                                                                        <a id="{{$pro->id}}"
                                                                                           class="p_edit btn btn-primary p-1">
                                                                                            <i class="fa fa-edit"></i>
                                                                                        </a>
                                                                                        <a class=" btn btn-dark disabled  p-1">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </a>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                    {{--
                                                                                                                                        <table class="fulltable fulltable-editable table table-sm " id="test-table">
                                                                                                                                            <thead>
                                                                                                                                            <tr>
                                                                                                                                                <th fulltable-field-name="p_name">محصول</th>
                                                                                                                                                <th fulltable-field-name="p_qty">تعداد</th>
                                                                                                                                                <th fulltable-field-name="p_price">قیمت واحد</th>
                                                                                                                                                <th fulltable-field-name="total">قیمت کل</th>
                                                                                                                                            </tr>
                                                                                                                                            </thead>
                                                                                                                                            <tbody>
                                                                                                                                            @foreach($sellFactor->out_products as $pro)
                                                                                                                                                <td>{{$pro->product->name}}</td>
                                                                                                                                                <td>{{$pro->qty}}</td>
                                                                                                                                                <td>{{$pro->price}}</td>
                                                                                                                                                <td>{{$pro->price * $pro->qty}}</td>
                                                                                                                                            @endforeach
                                                                                                                                            </tbody>
                                                                                                                                        </table>
                                                                    --}}
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                                data-dismiss="modal">بستن
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما فاکتور خریدی ندارید.
                                    </h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')
            var guaranty = null;
            var f_num = document.getElementById('f_num').value
            var f_date = document.getElementById('f_date').value
            var table = document.getElementById('table');
            var product_id = document.getElementById("pro").value

          /*  $('.ajaxqty').change(function (event) {
                var id = event.target.value;
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-product',
                        id: id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        $('#qty').val(response['quantity'])
                        $('#price').val(response['sell_price'])
                        product_id = response['id']
                        guaranty = response['guaranty_id']
                        if (guaranty != null) {
                            $('#guaranty_section').removeClass('d-none')
                            $('#duration').text(response['guaranty']['duration'])
                            switch (response['guaranty']['duration_unit']) {
                                case 'year':
                                    $('#d-unit').text('سال')
                                    break;
                                case 'month':
                                    $('#d-unit').text('ماه')
                                    break;
                                case 'day':
                                    $('#d-unit').text('روز')
                                    break;
                            }

                            if (response['guaranty']['start'] == 'sell_date')
                                $('#start').text(' فروش')
                            else
                                $('#start').text(' خرید')

                        } else {
                            $('#guaranty_section').addClass('d-none')
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            });*/
            var getqty=document.querySelector('#ajaxqty')
            getqty.addEventListener("change",function () {
                var id=getqty.value
                if (id!=""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'get-product',
                            id: id,
                        },
                        dataType: 'json',
                        success: function (response) {
                            $('#qty').val(response['quantity'])
                            $('#price').val(response['sell_price'])
                            product_id = response['id']
                            guaranty = response['guaranty_id']
                            if (guaranty != null) {
                                $('#guaranty_section').removeClass('d-none')
                                $('#duration').text(response['guaranty']['duration'])
                                switch (response['guaranty']['duration_unit']) {
                                    case 'year':
                                        $('#d-unit').text('سال')
                                        break;
                                    case 'month':
                                        $('#d-unit').text('ماه')
                                        break;
                                    case 'day':
                                        $('#d-unit').text('روز')
                                        break;
                                }

                                if (response['guaranty']['start'] == 'sell_date')
                                    $('#start').text(' فروش')
                                else
                                    $('#start').text(' خرید')

                            } else {
                                $('#guaranty_section').addClass('d-none')
                            }
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });
                }
            });


            $("#pro").keypress(function(event) {
                if (event.keyCode == 13 ||event.which == 13) {
                    event.preventDefault();
                    alert('ss')
                }
            });

            var customers=document.getElementById("customers")
            customers.addEventListener("input", function () {
                if ($('#customers').val() != "") {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            key: $('#customers').val(),
                            do: 'search-customer'
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response != "")
                                autocomplete(document.getElementById("customers"), response);
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                } else {
                    var existingElement = document.getElementById("customersautocomplete-list")
                    console.log(existingElement)
                    if (existingElement)
                        existingElement.remove()
                }
            });

            function autocomplete(inp, arr) {
                var existingElement = document.getElementById("customersautocomplete-list")
                console.log(existingElement)
                if (existingElement)
                    existingElement.remove()

                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;

                var a, b = null
                /*, val = this.value;
                                /!*close any already open lists of autocompleted values*!/
                                closeAllLists();
                                if (!val) {
                                    return false;
                                }*/
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary rounded bg-light py-2 px-1");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (var i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i]['family'] != null) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + " " + arr[i]['family'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + " " + arr[i]['family'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            customer = document.getElementById('customer')
                            customer.value = this.getElementsByTagName("input")[0].id
                            document.getElementById("customer").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    } else {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            customer = document.getElementById('customer')
                            customer.value = this.getElementsByTagName("input")[0].id
                            document.getElementById("customer").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }


                /*execute a function when someone writes in the text field:*/
                /*inp.addEventListener("input", function (e) {

                });*/

                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });

                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }

                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }

                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }

                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }


           /* $('#add').click(function (event) {
                var id = event.target.value;
                var customer = $('.customer_id').val()
                var customer_name = document.getElementById("customers").value
                var qty = document.getElementById("qty").value
                var sell_price = document.getElementById("price").value
                var f_date = document.getElementById('f_date').value
                var guaranty_accept = document.getElementById('guaranty-accept').value

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'sell',
                        id: id,
                        f_num: f_num,
                        f_date: f_date,
                        customer_id: customer,
                        customer_name: customer_name,
                        product_id: product_id,
                        qty: qty,
                        sell_price: sell_price,
                        guaranty_accept: guaranty_accept,
                        guaranty_id: guaranty
                    },
                    dataType: 'json',
                    success: function (response) {
                        switch (response['status']) {
                            case 0:
                                swal({
                                    text: "تعداد انتخاب شده از تعداد کل موجودی شما از این کالا بیشتر است.",
                                });
                                break;
                            case 1:
                                var mymodal = document.getElementsByClassName("modal-body")
                                var modal_table = document.getElementById('modal-table')
                                response['content'].forEach(function (re) {
                                    console.log(mymodal)
                                    var row = modal_table.insertRow(-1)
                                    var td0 = row.insertCell(0)
                                    var td1 = row.insertCell(1)
                                    td0.innerHTML = re['storage']['name']
                                    td1.innerHTML = re['quantity']
                                    $('.modal').modal('show');
                                })

                                break;
                            case 2:
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                td0.innerHTML = response['content']['name']
                                td1.innerHTML = response['content']['qty']
                                td2.innerHTML = response['content']['price']
                                td3.innerHTML = response['content']['qty'] * response['content']['price']
                                $('.customer_id').val(response['content']['customer_id'])
                                $('#qty').val("")
                                $('#price').val("")
                                break;
                        }
                        $('#savebtn').removeClass('d-none')

                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            });*/
            $('#f_date').on('change', function () {
                if ($('#f_date').val().length>0) {
                    $('#f_date').removeAttr("required");
                }else {
                    $('#f_date').attr("required")
                }
            });
            $('#price').on('change', function () {
                if ($('#price').val().length>0) {
                    $('#price').removeAttr("required");
                }else {
                    $('#price').attr("required")
                }
            });
            $('#sell-form').submit(function (e) {
                e.preventDefault();
                var id = e.target.value;
                var customer = $('.customer_id').val()
                var customer_name = document.getElementById("customers").value
                var qty = document.getElementById("qty").value
                var sell_price = document.getElementById("price").value
                var f_date = document.getElementById('f_date').value
                var guaranty_accept = document.getElementById('guaranty-accept').value

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'sell',
                        id: id,
                        f_num: f_num,
                        f_date: f_date,
                        customer_id: customer,
                        customer_name: customer_name,
                        product_id: product_id,
                        qty: qty,
                        sell_price: sell_price,
                        guaranty_accept: guaranty_accept,
                        guaranty_id: guaranty
                    },
                    dataType: 'json',
                    success: function (response) {
                        switch (response['status']) {
                            case 0:
                                swal({
                                    text: "تعداد انتخاب شده از تعداد کل موجودی شما از این کالا بیشتر است.",
                                });
                                break;
                            case 1:
                                var mymodal = document.getElementsByClassName("modal-body")
                                var modal_table = document.getElementById('modal-table')
                                response['content'].forEach(function (re) {
                                    console.log(mymodal)
                                    var row = modal_table.insertRow(-1)
                                    var td0 = row.insertCell(0)
                                    var td1 = row.insertCell(1)
                                    td0.innerHTML = re['storage']['name']
                                    td1.innerHTML = re['quantity']
                                    $('.modal').modal('show');
                                })

                                break;
                            case 2:
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                td0.innerHTML = response['content']['name']
                                td1.innerHTML = response['content']['qty']
                                td2.innerHTML = response['content']['price']
                                td3.innerHTML = response['content']['qty'] * response['content']['price']
                                $('.customer_id').val(response['content']['customer_id'])
                                $('#qty').val("")
                                $('#price').val("")
                                break;
                        }
                        $('#savebtn').removeClass('d-none')

                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            });

            document.getElementById('f_num').addEventListener("input", function (ev) {
                id = this.value
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        key: $('#customers').val(),
                        do: 'output_factor_info',
                        id: id
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                        if (response != 'nist') {
                            document.getElementById('customer_name').value = response['customer']
                            document.getElementById('f_date').value = response['date']
                            response['subItem'].forEach(function (re) {
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                td0.innerHTML = re['name']
                                td1.innerHTML = re['qty']
                                td2.innerHTML = re['price']
                                td3.innerHTML = re['qty'] * re['price']
                            })

                        } else {
                            document.getElementById('customer_name').value = ""
                            document.getElementById('f_date').value = ""
                            $("#table tr").remove();
                            table.innerHTML = '  <tr class="table-header">\n' +
                                '                            <th>نام محصول</th>\n' +
                                '                            <th>تعداد</th>\n' +
                                '                            <th>قیمت</th>\n' +
                                '                            <th>قیمت کل</th>\n' +
                                '                        </tr>'
                        }
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('.p_edit').click(function () {
                var indexx = $(this).parent().parent().index()
                var modal = $('.modal.show').attr('id')
                var tableid = modal.replace(/showfactorModal-/g, "")
                var table = document.getElementById('table-list-' + tableid)
                var row = table.insertRow(indexx + 1)
                var td0 = row.insertCell(0)
                var td1 = row.insertCell(1)
                var td2 = row.insertCell(2)
                var td3 = row.insertCell(3)
                td0.innerHTML = '<input class="form-control" type="text" id="edit-qty" placeholder="تعداد">'
                td1.innerHTML = '<input class="form-control" type="text" id="edit-price" placeholder="قیمت">'
                td2.innerHTML = '<button type="button" class="btn btn-sm btn-success" id="update-row">ثبت</button>'
                td3.innerHTML = '<button type="button" class="btn btn-sm btn-secondary" id="cancel">انصراف</button>'
                var id = this.id

                $('#update-row').click(function () {
                    var upIndex = $(this).parent().parent().index()
                    var qty = document.getElementById('edit-qty').value
                    var price = document.getElementById('edit-price').value
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'update-sell-factor-pro',
                            id: id,
                            qty: qty,
                            price: price,
                        },
                        dataType: 'json',
                        success: function (response) {
                            var modal = $('.modal.show').attr('id')
                            var tableid = modal.replace(/showfactorModal-/g, "")
                            var table = document.getElementById('table-list-' + tableid)
                            // var tbl = $('.tbl')
                            var rows = $('tr', table);
                            var preRow = rows.eq(upIndex-1);
                            var td1 = preRow.children("#p_qty")
                            var td2 = preRow.children("#p_price")
                            var td3 = preRow.children("#p_total")
                            td1.text(response['qty'])
                            td2.text(response['price'])
                            td3.text(response['qty'] * response['price'])
                            table.deleteRow(upIndex)
                            console.log(response)
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });

                })
                $('#cancel').click(function () {
                    var canIndex = $(this).parent().parent().index()
                    table.deleteRow(canIndex)
                })

            })
            $('.p_delete').click(function () {
                var modal = $('.modal.show').attr('id')
                var tableid = modal.replace(/showfactorModal-/g, "")
                var currenttable = document.getElementById('table-list-' + tableid)
                var pro_id = this.id
                var currentrow = $('.p_delete').parent().parent()
                var currentrowindex = $(this).parent().parent().index()

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'delete-sell-factor-pro',
                        id: pro_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        currenttable.deleteRow(currentrowindex)
                        console.log(response)
                    },
                    error: function (response) {
                        console.log(response)
                    }
                });
            })

            $('#factor-add').click(function () {
                var table = document.getElementById('table-list')
                var row = table.insertRow(-1)
                var td0 = row.insertCell(0)
                var td1 = row.insertCell(1)
                var td2 = row.insertCell(2)
                var td3 = row.insertCell(3)
                var td4 = row.insertCell(4)
                td0.innerHTML = '<select class="form-control" id="name"></select>'
                td1.innerHTML = '<input class="form-control" type="text" id="qty" placeholder="تعداد">'
                td2.innerHTML = '<input class="form-control" type="text" id="price" placeholder="قیمت">'
                td3.innerHTML = '<button type="button" class="btn btn-sm btn-success" id="save-row">ثبت</button>'
                td4.innerHTML = '<button type="button" class="btn btn-sm btn-secondary" id="cancel">انصراف</button>'

                var id=this.id

                $('#save-row').click(function () {
                    var qty=document.getElementById('edit-qty').value
                    var price=document.getElementById('edit-price').value
                    console.log(qty)
                    console.log(price)
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ajaxUrl,
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do: 'update-factor-pro',
                            id:id,
                            qty:qty,
                            price:price,
                        },
                        dataType: 'json',
                        success: function (response) {
                            table.deleteRow(-1)
                            console.log(response)
                        },
                        error: function (response) {
                            console.log(response)
                        }
                    });

                })
                $('#cancel').click(function () {
                    table.deleteRow(-1)
                })
            })

            $('#f_date').persianDatepicker({
                altField: '#f_date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
            });
        })

    </script>
@endsection


