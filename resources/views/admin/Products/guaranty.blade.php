@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <span class="guaranty-route d-none" id="{{route('guarantyinfo',1)}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">گارانتی</h1>
            <h5 class="text-muted mt-0 ">
                برای جستجوی گارانتی، نام خریدار یا نام کالای فروخته شده را وارد کنید.
            </h5>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="box-body">
                    <div class="row p-4 d-flex pb-5">
                        <a class="btn btn-outline-primary" href="{{route('guarantyTracking')}}">پیگیری گارانتی</a>
                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                            <div class="col-12 mt-3 col-md-4 text-right">
                                <h6 class="text-muted">عنوان کالا:</h6>
                                <input type="text" name="product_id" id="product" class="product d-none">
                                <div class="autocomplete" style="width:300px;">
                                    {!! Form::text('title',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'products']) !!}
                                </div>
                            </div>
                            <div class="col-12 mt-3 col-md-4 text-right">
                                <h6 class="text-muted">خریدار:</h6>
                                <input type="text" name="customer_id" id="customer" class="product d-none">
                                <div class="autocomplete" style="width:300px;">
                                    {!! Form::text('customer',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'customers']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row w-100 mt-3 justify-content-center align-items-center ">
                            <div class="col-12 mt-3 col-md-10 text-center">
                                <div class="box-body">
                                    <table id="table"
                                           class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                        <tr class="table-header">
                                            <th>نام مشتری</th>
                                            <th>نام محصول</th>
                                            <th>تاریخ فروش</th>
                                            <th>قیمت(تومان)</th>
                                            <th>عملیات</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var table = document.getElementById('table')

            $("#products").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام خریدار</th>\n' +
                    '                            <th>نام محصول</th>\n' +
                    '                            <th>تاریخ</th>\n' +
                    '                            <th>قیمت </th>\n' +
                    '                            <th>عملیات </th>\n' +
                    '                        </tr>'
                var data = null
                customer_name = $('#customers').val()
                product_name = $('#products').val()
                if (customer_name=='' && product_name == '') {
                    $("#table tr").remove();
                    table.innerHTML = '  <tr class="table-header">\n' +
                        '                            <th>نام خریدار</th>\n' +
                        '                            <th>نام محصول</th>\n' +
                        '                            <th>تاریخ</th>\n' +
                        '                            <th>قیمت </th>\n' +
                        '                            <th>عملیات </th>\n' +
                        '                        </tr>'
                } else {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            customer_name: $('#customers').val(),
                            product_name: $('#products').val(),
                            do: 'search-product-guaranty',
                        },
                        dataType: 'json',
                        success: function (response) {
                            /*data = response['product']
                            autocomplete(document.getElementById("products"), data);*/
                            response.forEach(function (re) {
                                var route=$('.guaranty-route.d-none').attr('id')
                                var editedRoute=route.slice(0,-1)
                                editedRoute=editedRoute+re['id']
                                console.log(editedRoute)
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                td0.innerHTML = re['factor']['customer']['name']
                                td1.innerHTML = re['product']['name']
                                td2.innerHTML = re['factor']['f_date']
                                td3.innerHTML = re['price']
                                td4.innerHTML = '<a href="'+editedRoute+'" class="text-decoration-none">جزئیات</a>'
                            })
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                }
            });

            $("#customers").on("keyup", function () {
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام خریدار</th>\n' +
                    '                            <th>نام محصول</th>\n' +
                    '                            <th>تاریخ</th>\n' +
                    '                            <th>قیمت </th>\n' +
                    '                            <th>عملیات </th>\n' +
                    '                        </tr>'
                customer_name = $('#customers').val()
                product_name = $('#products').val()
                if (customer_name=='' && product_name == '') {
                    $("#table tr").remove();
                    table.innerHTML = '  <tr class="table-header">\n' +
                        '                            <th>نام خریدار</th>\n' +
                        '                            <th>نام محصول</th>\n' +
                        '                            <th>تاریخ</th>\n' +
                        '                            <th>قیمت </th>\n' +
                        '                            <th>عملیات </th>\n' +
                        '                        </tr>'
                } else {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            customer_name: $('#customers').val(),
                            product_name: $('#products').val(),
                            do: 'search-product-guaranty',
                        },
                        dataType: 'json',
                        success: function (response) {
                            // data = response['factor']['customer']
                            // autocomplete(document.getElementById("products"), data);
                            response.forEach(function (re) {
                                var route=$('.guaranty-route.d-none').attr('id')
                                var editedRoute=route.slice(0,-1)
                                editedRoute=editedRoute+re['id']
                                console.log(editedRoute)
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                td0.innerHTML = re['factor']['customer']['name']
                                td1.innerHTML = re['product']['name']
                                td2.innerHTML = re['factor']['f_date']
                                td3.innerHTML = re['price']
                                td4.innerHTML = '<a href="'+editedRoute+'" class="text-decoration-none">جزئیات</a>'
                            })
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                }


            });
        })
    </script>
@endsection

