@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('main')

    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <span class="provider_account d-none" id="{{$factor->provider_id}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">پرداخت هزینه فاکتور</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-header mt-0 bg-white">
                <div class="col justify-content-center p-0 ">
                    <div class="nav  d-flex justify-content-center ">
                        <div class="alert alert-primary w-100">
                            <h3>
                                مبلغ کل فاکتور: <span id="total">{{$factor->total_price}}</span> تومان
                            </h3>
                            <h3>
                                پرداخت شده تاکنون: <span id="paid">
                                    {{$paid}}
                                </span> تومان
                            </h3>
                            <span class="factor_id" id="{{$factor->f_num}}"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row p-4 d-flex pb-5">
                    <div class="row w-100 mt-3 justify-content-center align-items-center ">
                        <div class="col-12 mt-3 col-md-3 text-right">
                            <h6 class="text-muted">نوع حساب: </h6>
                            <select name="account" class="form-control" id="acc_type">
                                <option selected value="none">انتخاب کنید</option>
                                @foreach($acc_types as $type)
                                    @if($type->name!=\App\Customer::class && $type->name != \App\Provider::class)
                                    <option id="account_type_id" value="{{$type->id}}">
                                        {{$type->name}}
                                    </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="account d-none col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">حساب: <span id="acc-balance" class="d-none text-danger">(موجودی : <i id="balance"></i>)</span></h6>
                            <select class="form-control" name="account" id="account"></select>
                        </div>
                        <div class="cheque d-none col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">چک:</h6>
                            <select class="form-control" name="cheque" id="cheque">
                                <option selected value="">انتخاب کنید</option>
                                <option value="new">صدور چک جدید</option>

                            </select>
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-right" id="price-section">
                            <h6 class="text-muted">مبلغ:<sup class="text-danger">*</sup></h6>
                            {!! Form::text('price',null,['class'=>'form-control','id'=>'price']) !!}
                        </div>
                        <div class="other-cheque-section d-none col-12 mt-3 col-md-3 text-center">
                            <h6 class="text-muted">توضیحات:</h6>
                            {!! Form::text('note',null,['class'=>'form-control','id'=>'note']) !!}
                        </div>

                        <div class="cheque-section d-none  row w-100 mt-3 justify-content-center align-items-center">
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">بانک :<sup class="text-danger">*</sup></h6>
                                <select class="form-control" name="banks" id="banks"></select>
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">سریال چک:<sup class="text-danger">*</sup></h6>
                                {!! Form::text('price',null,['class'=>'form-control','id'=>'serial']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">در وجه:<sup class="text-danger">*</sup></h6>
                                {!! Form::text('price',null,['class'=>'form-control','id'=>'pay_to']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">تاریخ:<sup class="text-danger">*</sup></h6>
                                {!! Form::text('price',null,['class'=>'form-control','id'=>'pay-date']) !!}
                            </div>
                            <div class="col-12 mt-3 col-md-3 text-center">
                                <h6 class="text-muted">توضیحات:</h6>
                                {!! Form::text('price',null,['class'=>'form-control','id'=>'new-note']) !!}
                            </div>
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-center">
                            <h6 class="text-muted"></h6>
                            {!! Form::submit('ثبت پرداخت',['class'=>'form-control btn btn-success w-50','id'=>'submit']) !!}
                            <a class="btn btn-primary" href="{{route('savePurchase',$factor->f_num)}}">بستن</a>
                        </div>
                    </div>
                </div>
                <table id="table" class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2">
                    <tr class="table-header">
                        <th>مبلغ</th>
                        <th>تاریخ</th>
                    </tr>
                    @foreach($outgoes as $outgo )
                        @php($paid+=$outgo->price)
                        <tr>
                            <td>{{$outgo->price}}</td>
                            <td>{{verta($outgo->created_at)->formatDate()}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->
    <script>
        $(document).ready(function () {
            function separate(Number)
            {
                Number+= '';
                Number= Number.replace(',', '');
                x = Number.split('.');
                y = x[0];
                z= x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(y))
                    y= y.replace(rgx, '$1' + ',' + '$2');
                return y+ z;
            }

            $('#pay-date').persianDatepicker({
                altField: '#pay-date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
            });

            var ajaxUrl = $('.ajax.d-none').attr('id')
            var factor_id=document.getElementsByClassName('factor_id')[0].id
            var table=document.getElementById('table');
            var provider_account=document.getElementsByClassName('provider_account').id
            var acc_type=document.getElementById('acc_type')
            var price

            $('#submit').click(function(event) {
                var acc_type=document.getElementById('acc_type')
                 price=parseInt(document.getElementById('price').value)
                var id=event.target.value;
                var paid=parseInt(document.getElementById('paid').innerHTML)
                var total=parseInt(document.getElementById('total').innerHTML)
                var sum=paid+price
                if (price==""){
                    swal('لطفا مبلغ را وارد کنید')
                }
                if(acc_type.value=='3'){
                    var cheque_type=document.getElementById('cheque').value
                    if(cheque_type=='new'){
                        var bank=document.getElementById('banks').value
                        var serial=document.getElementById('serial').value
                        var pay_to=document.getElementById('pay_to').value
                        var note=document.getElementById('new-note').value
                        var date=document.getElementById('pay-date').value
                        var price=document.getElementById('price').value
                        if (price==""){
                            swal('لطفا مبلغ را وارد کنید')
                        }
                        if (serial==""){
                            swal('لطفا سریال چک را وارد کنید')
                        }
                        if (date==""){
                            swal('لطفا تاربخ چک را وارد کنید')
                        }
                        if (pay_to==""){
                            swal('لطفا مشخص کنید چک در وجه چه کسی است')
                        }
                        $.ajax({
                            type:"POST",
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:ajaxUrl,
                            data:{
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do:'set-new-cheque',
                                id:factor_id,
                                price:price,
                                account_id:account.value,
                                paid:paid,
                                provider_account:provider_account,
                                bank:bank,
                                serial:serial,
                                pay_to:pay_to,
                                note:note,
                                date:date,
                            },
                            dataType:'json',
                            success:function (response) {
                                if(response!=='ERROR'){
                                    var dateFormat = new Intl.DateTimeFormat("fa");
                                    var row=table.insertRow(-1);
                                    var td0=row.insertCell(0)
                                    var td1=row.insertCell(1)
                                    td0.innerHTML=response['price']
                                    var created=new Date(response['created_at']);
                                    td1.innerHTML=dateFormat.format(created)
                                    document.getElementById('paid').innerHTML=paid+parseInt(response['price'])
                                }else {
                                    swal('موجودی حساب شما کافی نیست!')
                                }
                            },
                            error:function (response) {
                                console.log('error')
                                console.log(response)
                            }
                        });

                    }else if(cheque_type!=""){
                        var cheque_id=cheque_type
                        $.ajax({
                            type:"POST",
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:ajaxUrl,
                            data:{
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do:'set-purchase-by-other-cheque',
                                id:factor_id,
                                account_id:account.value,
                                paid:paid,
                                cheque_id:cheque_id,
                                note:$('#note').value,
                                provider_account:provider_account,
                            },
                            dataType:'json',
                            success:function (response) {
                                console.log(response)

                            },
                            error:function (response) {
                                console.log('error')
                                console.log(response)
                            }
                        });
                    }
                }else {
                    if(sum<=total){
                        $.ajax({
                            type:"POST",
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:ajaxUrl,
                            data:{
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do:'set-purchase',
                                id:factor_id,
                                price:price,
                                account_id:$('#account').val(),
                                paid:paid,
                                provider_account:provider_account,
                            },
                            dataType:'json',
                            success:function (response) {
                                if(response!=='ERROR'){
                                    var dateFormat = new Intl.DateTimeFormat("fa");
                                    var row=table.insertRow(-1);
                                    var td0=row.insertCell(0)
                                    var td1=row.insertCell(1)
                                    td0.innerHTML=response['price']
                                    var created=new Date(response['created_at']);
                                    td1.innerHTML=dateFormat.format(created)
                                    document.getElementById('paid').innerHTML=paid+parseInt(response['price'])
                                    console.log(response)
                                }else {
                                    swal('موجودی حساب شما کافی نیست!')
                                }
                            },
                            error:function (response) {
                                console.log('error')
                                console.log(response)
                            }
                        });
                    }else{
                        swal('مقدار وارد شده از باقیمانده حساب بیشتر است.')
                        if (price==""){
                            swal('لطفا مبلغ را وارد کنید')
                        }
                    }
                }
            });

            var account=document.querySelector('#acc_type')
            account.addEventListener("change",function () {
               var id=this.value
                if(id!='none'){
                    $.ajax({
                        type:"POST",
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:ajaxUrl,
                        data:{
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do:'search-account',
                            id:id,
                        },
                        dataType:'json',
                        success:function (response) {
                            $('#balance').text("")
                            let amount=parseInt(response['amount'])
                            $('#balance').text(separate(amount))
                            if(response['amount']>0){
                                $('#acc-balance').removeClass('text-danger')
                                $('#acc-balance').addClass('text-success')
                            }else {

                                $('#acc-balance').addClass('text-danger')
                                $('#acc-balance').removeClass('text-success')
                            }
                            $('#acc-balance').removeClass('d-none')
                        },

                        error:function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }

            })

            var cheque=document.getElementById('cheque')
            acc_type.addEventListener("change",function () {
                var acc_type_id=this.value
                if(acc_type_id!="none"){
                    if  (acc_type_id==3){
                        $.ajax({
                            type:"POST",
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:ajaxUrl,
                            data:{
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do:'get-other-cheque',
                                id:acc_type_id,
                            },
                            dataType:'json',
                            success:function (response) {
                                console.log(response)
                                $('div.cheque').removeClass('d-none')
                                $('div.account').addClass('d-none')
                                cheque.innerHTML="";
                                cheque.innerHTML+='<option selected value="">انتخاب کنید</option>';
                                cheque.innerHTML+='<option value="new">صدور چک جدید</option>';
                                response['cheques'].forEach(function (re){
                                    cheque.innerHTML+='<option value="'+re['id']+'">مبلغ: '+re['amount']+' تاریخ:'+re['pay_date']+'</option>'
                                })
                                var bank=document.querySelector('#banks')
                                if (response['banks']=="" || response['banks']==null){
                                    swal('شما حسابی برای دسته چک ندارید.')
                                }else {
                                    bank.innerHTML="";
                                    response['banks'].forEach(function (re){
                                        bank.innerHTML+='<option value="'+re['id']+'">'+re['name']+'</option>'
                                    })
                                }
                            },

                            error:function (response) {
                                console.log('error')
                                console.log(response)
                            }
                        });
                    }else {
                        $.ajax({
                            type:"POST",
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:ajaxUrl,
                            data:{
                                _token: $('meta[name="csrf-token"]').attr('content'),
                                do:'get-account-by-type',
                                id:acc_type_id,
                            },
                            dataType:'json',
                            success:function (response) {
                                if(response!=""){
                                    $('div.account').removeClass('d-none')
                                    $('div.cheque-section').addClass('d-none')
                                    $('div.cheque').addClass('d-none')
                                    var account=document.getElementById('account')
                                    account.innerHTML=""
                                    account.innerHTML+='<option selected value="">انتخاب کنید</option>';
                                    response.forEach(function (re) {
                                        account.innerHTML+='<option id="" value="'+re['id']+'">'+re['name']+ '</option>'
                                    })
                                }else{
                                    $('#balance').text("0")
                                    swal('حسابی از این نوع ندارید')
                                }
                            },

                            error:function (response) {
                                console.log('error')
                                console.log(response)
                            }
                        });
                    }
                }

            })

            cheque.addEventListener("change",function () {
                var newcheque=this.value
                if(newcheque=='new'){
                    $('div.cheque-section').removeClass('d-none')
                    $('div.account').addClass('d-none')
                }else if(newcheque!=''){
                    $('#price-section').addClass('d-none')
                    $('.other-cheque-section').removeClass('d-none')
                    var cheque_id=newcheque
                    $.ajax({
                        type:"POST",
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:ajaxUrl,
                        data:{
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            do:'get-other-cheque-price',
                            id:cheque_id,
                        },
                        dataType:'json',
                        success:function (response) {
                            price=parseInt(response['amount'])
                        },

                        error:function (response) {
                            console.log('error')
                            console.log(response)
                        }
                    });
                }
            })

            var account_types=document.getElementById('account')
            account_types.addEventListener("change",function (){
                let acc_id=account_types.value;
                $.ajax({
                    type:"POST",
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:ajaxUrl,
                    data:{
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do:'search-account',
                        id:acc_id,
                    },
                    dataType:'json',
                    success:function (response) {
                        $('#balance').text("")
                        let amount=parseInt(response['amount'])
                        $('#balance').text(separate(amount))
                        if(response['amount']>0){
                            $('#acc-balance').removeClass('text-danger')
                            $('#acc-balance').addClass('text-success')
                        }else {

                            $('#acc-balance').addClass('text-danger')
                            $('#acc-balance').removeClass('text-success')
                        }
                        $('#acc-balance').removeClass('d-none')
                    },

                    error:function (response) {
                        console.log('error')
                        console.log(response)
                    }
                });
            })
        })
    </script>
@endsection


