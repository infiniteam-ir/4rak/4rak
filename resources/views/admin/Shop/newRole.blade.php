@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
        <div class="row p-4 d-flex pb-5">
            <div class="col-12 alert alert-light text-center ">
                <h1 class="text-muted">افزودن نقش </h1>
            </div>
            <div class="col-12 bg-white p-2 rounded box-shadow">
                <div class="box-body">
                    {!! Form::open(['method'=>'POST','action' =>'AdminShopController@saveRole']) !!}
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                        <tr class="table-header">
                            <th>عنوان</th>
                            <th>محصولات</th>
                            <th>حسابداری</th>
                            <th>کارکنان</th>
                            <th>انبارداری</th>
                            <th>خرید</th>
                            <th>فروش</th>
                            <th>مرجوعی</th>
                            <th>گارانتی</th>
                            <th>حمل و نقل</th>
                        </tr>
                        <tr>
                            <th>
                                <input type="text" name="name"  class="form-control"  required oninvalid="this.setCustomValidity('لطفا عنوان نقش را وارد کنید')" oninput="setCustomValidity('')">
                            </th>
                            <th>
                                {!! Form::checkbox('products',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('accountant',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('personnel',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('storage',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('buy',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('sell',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('reject',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('guaranty',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('delivery',1) !!}
                            </th>

                        </tr>

                    </table>
                    {!! Form::submit('ثبت',['class'=>'btn btn-success']) !!}
                    <a href="{{route('shopRoles')}}" class="btn btn-danger">انصراف</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>







@endsection


