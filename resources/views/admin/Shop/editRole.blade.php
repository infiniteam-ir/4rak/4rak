@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
        <div class="row p-4 d-flex pb-5">
            <div class="col-12 alert alert-light text-center ">
                <h1 class="text-muted">ویرایش نقش <span class="text-success">"{{$role->role_name}}"</span></h1>
            </div>
            <div class="col-12 bg-white p-2 rounded box-shadow">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    {{$error}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box-body">
                    {!! Form::model($role,['method'=>'PATCH','action' =>['AdminShopController@updateRole',$role->id]]) !!}
                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                        <tr class="table-header">
                            <th>عنوان</th>
                            <th>حسابداری</th>
                            <th>کارکنان</th>
                            <th>انبارداری</th>
                            <th>خرید</th>
                            <th>فروش</th>
                            <th>مرجوعی</th>
                            <th>گارانتی</th>
                            <th>حمل و نقل</th>
                        </tr>
                        <tr>
                            <th>
                                <input type="text" name="name" value="{{$role->role_name}}" class="form-control"  required oninvalid="this.setCustomValidity('لطفا عنوان نقش را وارد کنید')" oninput="setCustomValidity('')">
                            </th>
                            <th>
                                {!! Form::checkbox('accountant',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('personnel',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('storage',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('buy',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('sell',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('reject',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('guaranty',1) !!}
                            </th>
                            <th>
                                {!! Form::checkbox('delivery',1) !!}
                            </th>

                        </tr>

                    </table>
                    {!! Form::submit('ثبت',['class'=>'btn btn-success']) !!}
                    <a href="{{route('shopRoles')}}" class="btn btn-danger">انصراف</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>







@endsection


