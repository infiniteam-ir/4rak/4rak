@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
        <div class="row p-4">
            <div class="alert alert-light w-100">
                <h1 class="text-muted text-center ">خرید اشتراک</h1>
            </div>
        </div>
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(\Illuminate\Support\Facades\Session::has('error'))
        <div class="alert alert-danger">
            {{\Illuminate\Support\Facades\Session::pull('error')}}
        </div>
    @endif

    <div class="row w-100 p-4  mb-5 text-center justify-content-center align-items-start ">
            @foreach($pay_plans as $plan)
                <div class="row border rounded p-2 pb-4 w-100 bg-white">
                    <div class="col-12 mb-3 alert bg-color3 mx-1">
                        <h6>{{$plan->name}}</h6>
                        <h6 class="badge badge-purple text-white py-2">درصد کارمزد: <i class=" text-white p-2">{{$plan->wagePercent}} % </i></h6>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <form class="w-100" action="{{route('plan-purchase')}}" method="post">
                            @csrf
                        <div class="border border-danger rounded py-3 shadow">
                            <h5>ماهانه</h5>
                            <h5>{{number_format($plan->monthlyPrice)}}</h5>
                            <button  type="submit" name="duration" value="1"   class="btn btn-danger w-50">خرید</button>
                            <input class="d-none" type="hidden" name="plan_id" value="{{$plan->id}}">
                        </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <form class="w-100" action="{{route('plan-purchase')}}" method="post">
                            @csrf
                        <div class="border border-primary rounded py-3 shadow">
                            <h5>سه ماهه</h5>
                            <h5>{{number_format($plan->threeMonthPrice)}}</h5>
                            <button  type="submit" name="duration" value="3" class="btn btn-primary w-50">خرید</button>
                            <input class="d-none" type="hidden" name="plan_id" value="{{$plan->id}}">
                        </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <form class="w-100" action="{{route('plan-purchase')}}" method="post">
                            @csrf
                        <div class="border border-warning rounded py-3 shadow">
                            <h5>شش ماهه</h5>
                            <h5>{{number_format($plan->sixMonthPrice)}}</h5>
                            <button  type="submit" name="duration" value="6" class="btn btn-warning w-50">خرید</button>
                            <input class="d-none" type="hidden" name="plan_id" value="{{$plan->id}}">
                        </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <form class="w-100" action="{{route('plan-purchase')}}" method="post">
                            @csrf
                        <div class="border border-success rounded py-3 shadow">
                            <h5>یکساله</h5>
                            <h5>{{number_format($plan->yearlyPrice)}}</h5>
                            <button  type="submit" name="duration" value="12"  class="btn btn-success w-50">خرید</button>
                            <input class="d-none" type="hidden" name="plan_id" value="{{$plan->id}}">
                        </div>
                        </form>
                    </div>
                </div>
            @endforeach
    </div>
@endsection

