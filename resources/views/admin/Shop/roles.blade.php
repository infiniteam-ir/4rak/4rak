@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">اطلاعات نقش های فروشگاه شما</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="row w-100 justify-content-center">
                    <div class="alert alert-success ">
                        <h4 class="text-success">
                            {{Session::pull('success')}}
                        </h4>
                    </div>
                </div>
            @endif
            @if(Session::has('updated'))
                <div class="row w-100 justify-content-center">
                    <div class="alert alert-success ">
                        <h4 class="text-success">
                            {{Session::pull('updated')}}
                        </h4>
                    </div>
                </div>
            @endif
            @if(Session::has('forbidden'))
                <div class="row w-100 justify-content-center">
                    <div class="alert alert-danger ">
                        <h4 class="text-dark">
                            {{Session::pull('forbidden')}}
                        </h4>
                    </div>
                </div>
            @endif
            @if(Session::has('delete_role_fail'))
                <div class="row w-100 justify-content-center">
                    <div class="alert alert-danger ">
                        <h4 class="text-dark">
                            {{Session::pull('delete_role_fail')}}
                        </h4>
                    </div>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <div class="nav  d-flex justify-content-center ">
                            <ul class=" d-flex justify-content-between list-unstyled w-100 user-list-menu p-2 checkout-ul ">
                                <li class="blog-list-item ">
                                    <a href="{{route('createRole')}}" class="btn btn-outline-success">
                                        <i class="fa fa-plus"></i>
                                        افزودن نقش
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if(count($roles)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                            <tr class="table-header">
                                <th>ID</th>
                                <th>عنوان</th>
                                <th>حسابداری</th>
                                <th>کارکنان</th>
                                <th>انبارداری</th>
                                <th>خرید</th>
                                <th>فروش</th>
                                <th>مرجوعی</th>
                                <th>گارانتی</th>
                                <th>حمل و نقل</th>
                                <th>امکانات</th>
                            </tr>

                            @foreach($roles as $role)
                                <tr>
                                    <td>{{$role->id}}</td>
                                    <td><a class="text-decoration-none"
                                           href="{{route('editRoles',$role->id)}}">{{$role->role_name}}</a></td>
                                    <td>
                                        @switch($role->accountant)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->personnel)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->storage)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->buy)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->sell)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->reject)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->guaranty)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        @switch($role->delivery)
                                            @case(1)
                                            <i class="fa fa-check text-success"></i>
                                            @break
                                            @case(0)
                                            <i class="fa fa-times text-danger"></i>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <a style="cursor: pointer" class="text-decoration-none" data-toggle="modal"
                                           data-target="#deleteModal-{{$role->id}}" data-whatever="@getbotstrap">
                                            <i class="fa fa-trash-alt"></i>
                                        </a>

                                        {{--MODAL--}}
                                        <div class="modal fade" id="deleteModal-{{$role->id}}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog ">
                                                <div class="modal-content">
                                                    <div class=" modal-header ">
                                                        <h5 class="modal-title text-warning" id="exampleModalLabel">
                                                            هشدار!</h5>
                                                    </div>
                                                    {!! Form::open(['method'=>'DELETE','action' =>['AdminShopController@destroyRole',$role->id]]) !!}
                                                    <div class="modal-body ">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">آیا
                                                                مطمئنید میخواهید نقش <span
                                                                        class="badge badge-danger p-2 text-bold">{{$role->role_name}}</span>
                                                                را حذف کنید؟ </label>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer ">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">خیر
                                                        </button>
                                                        <button class="btn btn-danger" type="submit">بله</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$roles->links()}}</div>
                    </div>
                @else
                    <div class="col-12 alert alert-warning text-center ">
                        <h5 class="text-danger">شما هنوز نقشی تعریف نکرده اید.</h5>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>






@endsection


