@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    @if(Auth::user()->shop_id==null)
        <div class="row p-4">
            <div class="alert alert-light w-100">
                <h1 class="text-muted text-center ">ثبت اطلاعات فروشگاه </h1>
            </div>
        </div>
    @endif

    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Auth::user()->shop==null)
        @livewire('shop.shop-info',['provinces'=>$provinces,'guilds'=>$guilds])
    @else
        <div class="row  justify-content-center">
            <div class="alert alert-light w-100">
                <h1 class="text-muted text-center "> اطلاعات فروشگاه </h1>
                <h1 class="text-muted text-center "></h1>
            </div>
        </div>
        <div class="row ">
            <div class="col-12 mb-5 bg-white border rounded box-shadow">
                @if(Session::has('success'))
                    <div class="alert alert-success text-center mt-2 ">
                        <h4 class="text-success">
                            {{Session::pull('success')}}
                        </h4>
                    </div>
                @endif
                <div class="row mb-5 mt-2 border border-1-gainsboro rounded pb-5">
                    @if($header == null)
                        <div style="background: url('https://placehold.co/1100x200/png');background-repeat: no-repeat;background-size: 100% 100%" class="col-12 border border-1-gainsboro  rounded shop-info-header">
                   @else
                                <div style="background: url('{{asset('images/headers/'.$header->path)}}');background-repeat: no-repeat;background-size: 100% 100%" class="col-12 border border-1-gainsboro  rounded shop-info-header">
                      @endif
                                    <span class="m-3">
                            @if($logo == null)
                                            <img class="img-fluid img-shop-info border border-white"
                                                 src="https://placehold.co/200x200/png">
                                        @else
                                            <img class="img-fluid img-shop-info "
                                                 src="{{asset('images/Logos/'.$logo->path)}}">
                                        @endif
                        </span>
                                </div>

                                <div class="row w-100 mx-5">
                                    <div class="col-12 col-md-6 mt-3 ">
                                        <p><i class="fa fa-store"></i>
                                            نام فروشگاه:
                                            <span class="h4 text-success">
                                            {{$shop->name}}
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-store-alt"></i>
                                            نام مخصوص فروشگاه:
                                            <span class="h4 text-success">
                                            {{$shop->unique_name}}
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-map-marker-alt"></i>
                                            استان:
                                            <span class="h4 text-success">
                                            {{$shop->province->name}}
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-map-marker-alt"></i>
                                            شهر:
                                            <span class="h4 text-success">
                                            {{$shop->city->name}}
                                        </span>
                                        </p>

                                    </div>
                                    <div class="col-12 col-md-6 mt-3">
                                        <a href="{{route('EditShopInfo',$shop->id)}}"
                                           class="btn btn-outline-primary rounded float-left">
                                            <i class="fa fa-plus"></i>
                                            ویرایش
                                        </a>
                                        <p class="">
                                            <i class="fa fa-layer-group"></i>
                                            صنف:
                                            <span class="h4 text-success">
                                            {{$shop->guild->name}}
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-layer-group"></i>
                                            رسته:
                                            <span class="h4 text-success">
                                                @if($shop->subguild)
                                            {{$shop->subguild->name}}
                                                @else
                                                سایر
                                                @endif
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-map-marked-alt"></i>
                                            آدرس:
                                            <span class="h4 text-success">
                                            {{$shop->address}}
                                        </span>
                                        </p>
                                        <p>
                                            <i class="fa fa-info-circle"></i>
                                            درباره فروشگاه:
                                            <span class="h4 text-success">
                                            {{$shop->description}}
                                        </span>
                                        </p>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')

            $('#province').on("change", function () {
                var province = $('#province').val()
                console.log(province)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            // document.getElementById('city').value = re['id']
                        })
                        console.log(document.getElementById('city'))
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $('#city').on("change", function () {

                console.log(this.value)
            });

            $('#guild').on("change", function () {
                var guild = $('#guild').val()
                console.log(guild)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-sub-guilds',
                        guild: guild
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('sub_guild').innerHTML = ''
                        document.getElementById('sub_guild').innerHTML = '<option value="" class="form-control">انتخاب رسته</option>'

                        response.forEach(function (re) {
                            document.getElementById('sub_guild').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            // document.getElementById('city').value = re['id']
                        })
                        document.getElementById('sub_guild').innerHTML += '<option value="0" class="form-control">سایر</option>'
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            /*
                        $('#sub_guild').on("change", function () {
                            var value=this.value
                            var exist_element=$('.newsub')
                            if (value=='new'){
                                if (exist_element.length==0){
                                    $child='<input class="form-control mt-2 newsub" type="text" placeholder="نام رسته جدید را اینجا وارد کنید" name="new_sub_guild" wire:model.lazy="new_sub_guild">';
                                    var section=$('#subguild-section')
                                    section.append($child)
                                }
                            }else {
                                exist_element.remove();
                            }
                        });
            */

            /*$('.selected-plan ').on('click',function (){
                $(this).removeClass('btn-outline-primary')
                $('.btn.btn-primary').addClass('btn-outline-primary')
                $('.btn.btn-primary').removeClass('btn-primary')
                $(this).addClass('btn-primary')
                $('#plan').val($(this).attr('id'))
                console.log($('#plan').val())
            })*/

        })
    </script>
@endsection
