@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">گالری</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                {{--                {!! Form::open(['method'=>'POST','action'=>'AdminShopController@setGallery','files'=>true]) !!}--}}
                <div class="row p-4 d-flex pb-5">
                    <div class="row w-100 mt-3 justify-content-center align-items-center border py-3 border-1-gainsboro rounded">
                        <div class="col-12">
                            <h5> برای بخش گالری فروشگاه خود حداکثر 10 تصویر با حجم هر کدام حداکثر 6 مگابایت را در محل مشخص شده بارگذاری کنید.</h5>
                        </div>
                        <div class="col-12 col-md-2">
                            <img src="{{asset('img/picture.svg')}}" class="img-fluid ">
                        </div>
                        <div class="col-12 mt-3 col-md-10 text-right">
                            <div id="dropzone" class="bg-white">
                                <form action="{{route('setGallery')}}" enctype="multipart/form-data"
                                      class="dropzone bg-light" id="images">
                                    @csrf
                                    <div class="dz-message needsclick">
                                        <button type="button" class="dz-button">تصاویر را بکشید و اینجا رها کنید</button>
                                        <br/>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center  py-3 border border-1-gainsboro rounded">
                        <div class="col-12">
                            <h5> در بخش گالری فروشگاهتان میتوانید یک تیزر تبلیغاتی قرار دهید، این تیزر برای دیده شدن و معرفی شما به دیگران کمک میکند.</h5>
                            <h6 class="text-muted font-weight-light">تنها یک تیزر و با حجم حداکثر 30 مگابایت</h6>
                        </div>
                        <div class="col-12 col-md-2">
                            <img src="{{asset('img/movie.svg')}}" class="img-fluid ">
                        </div>
                        <div class="col-12 mt-3 col-md-10 text-right">
                            <div id="dropzone" class="bg-white ">
                                <form action="{{route('setGallery')}}" class="dropzone bg-light" id="teaser">
                                    @csrf
                                    <div class="dz-message needsclick">
                                        <button type="button" class="dz-button">فیلم را بکشید و اینجا رها کنید</button>
                                        <br/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{--{!! Form::close() !!}--}}
            </div>
        </div>
    </div>

@endsection
@section('filepond')
    <script>
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "Drop files here to upload";
        Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
        Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
        Dropzone.prototype.defaultOptions.dictFileTooBig = "حجم فایل بیش از حد مجاز است. ";
        Dropzone.prototype.defaultOptions.dictInvalidFileType = "پسوند فایل انتخاب شده غیر مجاز است.";
        Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with  code.";
        Dropzone.prototype.defaultOptions.dictCancelUpload = "لغو";
        Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "آیا مطمئنید؟";
        Dropzone.prototype.defaultOptions.dictRemoveFile = "حذف";
        Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "تعداد فایل ها بیش از حد مجاز است";


        Dropzone.options.teaser = {
            paramName: "teaser", // The name that will be used to transfer the file
            maxFilesize: 30, // MB
            maxFiles: 1,
            acceptedFiles: "video/*",
            addRemoveLinks: true,
            dictRemoveFile: "حذف",
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime();
                return time + file.name;
            },
            removedfile: function (file) {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('deleteGalleryFile') }}',
                    data: {filename: name},
                    success: function (data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
        };
        Dropzone.options.images = {
            paramName: "images", // The name that will be used to transfer the file
            maxFilesize: 6, // MB
            maxFiles: 10,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            removedfile: function (file) {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: '{{ route('deleteGalleryImg') }}',
                    data: {filename: name},
                    success: function (data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime();
                return time + file.name;
            },


        };


    </script>
@endsection


