@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row  justify-content-center">
        <div class="alert alert-light w-100">
            <h1 class="text-muted text-center ">ویرایش اطلاعات فروشگاه </h1>
        </div>
    </div>

    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

       <div class="row p-4 d-flex pb-1 bg-white justify-content-between">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{Session('error')}}
            </div>
        @endif
            <div class="col-12 col-md-6 border border-1-gainsboro rounded ">
                @livewire('shop.info-edit',['shop'=>$shop,'provinces'=>$provinces,'cities'=>$cities])
           </div>
            <div class="col-12 col-md-5 border border-1-gainsboro rounded">
                <div class="row w-100 mt-3  justify-content-center align-items-center ">
                    <div class="col-12 mt-3 ">
                        <h6 class="text-muted">لوگو:</h6>
                        <input type="file" class="filepond" name="logo_img"  data-allow-reorder="true"
                               data-max-file-size="3MB" accept="image/png, image/jpeg, image/gif"/>
                    </div>

                    <div class="col-12 mt-3 ">
                        <h6 class="text-muted">تصویر هدر: <p class="text-success">اندازه مناسب 1124px * 300px میباشد</p></h6>

                        <input type="file" class="filepond" name="header_img" accept="image/png, image/jpeg, image/gif"/>
                    </div>

                    <div class="col-12 mt-3 ">
                        <h6 class="text-muted">مجوز:</h6>
                        <input type="file" class="filepond" name="license_img" accept="image/png, image/jpeg, image/gif"/>
                    </div>
                </div>
            </div>

    </div>



@endsection
@section('filepond')
    <script>
        FilePond.registerPlugin();
        var element = document.querySelector('meta[name="csrf-token"]');
        var csrf = element && element.getAttribute("content");
        FilePond.setOptions({
            server: {
                url: "{{ route('imageup')}}",
                process: {
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },
                }
            },
            credits: false,
            labelIdle:"فایلها را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
            labelFileProcessing:'در حال آپلود',
            labelFileProcessingComplete:'تکمیل شد',
            labelFileProcessingAborted:'لغو شد',
            labelFileProcessingError:'خطا حین آپلود',
            labelTapToCancel:'برای لغو اینجا کلیک کنید',
            labelTapToUndo:'برای بازگشت اینجا کلیک کنید',
        });

        const inputElement2 = document.querySelector('input[name="logo_img"]');
        const pond2 = FilePond.create( inputElement2);
        const inputElement = document.querySelector('input[name="header_img"]');
        const pond = FilePond.create( inputElement);
        const inputElement1 = document.querySelector('input[name="license_img"]');
        const pond1 = FilePond.create( inputElement1);
    </script>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var ajaxUrl = $('.ajax.d-none').attr('id')

            $('#province').on("change", function () {
                var province = $('#province').val()
                // console.log(province)
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: ajaxUrl,
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        do: 'get-cities',
                        province: province
                    },
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('city').innerHTML = ''
                        response.forEach(function (re) {
                            document.getElementById('city').innerHTML += '<option value="' + re['id'] + '" class="form-control">' + re['name'] + '</option>'
                            document.getElementById('city').value = re['id']
                        })
                        console.log(document.getElementById('city'))
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $('#city').on("change", function () {

                console.log(this.value)
            });
        })
    </script>
@endsection
