@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}" >

@endsection
@section('main')
<div class="row p-4 d-flex pb-5">
    @if(Session::has('error'))
        <div class="alert alert-danger">
            {{Session('error')}}
        </div>
    @endif
        @if(Session::has('success'))
            <div class="alert alert-success text-center">
                {{Session::pull('success')}}
            </div>
        @endif
    <div class="row w-100 mt-3 mb-5 justify-content-center align-items-center bg-white py-5 px-2 rounded ">
        <div class="col-12 mt-3 col-md-4">
            <h6 class="text-muted">لوگو:</h6>
            <input type="file" class="filepond" name="logo_img"  data-allow-reorder="true"
                   data-max-file-size="3MB" accept="image/png, image/jpeg, image/gif"/>
        </div>

        <div class="col-12 mt-3 col-md-4">
            <h6 class="text-muted">تصویر هدر:</h6>
            <input type="file" class="filepond" name="header_img" accept="image/png, image/jpeg, image/gif"/>
        </div>

        <div class="col-12 mt-3 col-md-4">
            <h6 class="text-muted">مجوز:</h6>
            <input type="file" class="filepond" name="license_img" accept="image/png, image/jpeg, image/gif"/>
        </div>

        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4">
                {!! Form::open(['method'=>'POST','action'=>'AdminShopController@imageup'] ) !!}
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success mt-3']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('filepond')
    <script>
        FilePond.registerPlugin();
        var element = document.querySelector('meta[name="csrf-token"]');
        var csrf = element && element.getAttribute("content");
        FilePond.setOptions({
            server: {
                url: "{{ route('imageup')}}",
                process: {
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    },

                }

            },
            labelIdle:"فایل را بکشید و اینجا رها کنید یا <span class=\"filepond--label-action\">انتخاب کنید</span>",
            labelFileProcessing:'در حال آپلود',
            labelFileProcessingComplete:'تکمیل شد',
            labelFileProcessingAborted:'لغو شد',
            labelFileProcessingError:'خطا حین آپلود',
            labelTapToCancel:'برای لغو اینجا کلیک کنید',
            labelTapToUndo:'برای بازگشت اینجا کلیک کنید',
        });

        const inputElement2 = document.querySelector('input[name="logo_img"]');
        const pond2 = FilePond.create( inputElement2);
        const inputElement = document.querySelector('input[name="header_img"]');
        const pond = FilePond.create( inputElement);
        const inputElement1 = document.querySelector('input[name="license_img"]');
        const pond1 = FilePond.create( inputElement1);
    </script>
@endsection
