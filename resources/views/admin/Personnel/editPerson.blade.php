@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4">
        <div class="alert alert-light w-100 text-center">
            <h3 class="text-muted">فرم ویرایش کاربر {{$person->name}} {{$person->family}}</h3>
        </div>
    </div>

    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

        <div class="row px-4 d-flex pb-5">
            <div class="col-12 bg-white rounded">
                @livewire('personnel.edit-person',['person'=>$person])
            </div>
        </div>


@endsection

