@extends('admin.layouts.newTheme.master')


@section('main')
    <div class="row px-2 my-3 d-flex flex-nowrap overflow-auto">
        <div class="col mx-1">
            <a href="{{route('personnel.index')}}">
                <div class="btn btn-purple  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/users-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white">مدیریت کارکنان</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('personnel.create')}}">
                <div class="btn btn-warning  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/add-user-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white"> افزودن کاربر</h6>
                    </div>
                </div>
            </a>

        </div>
        <div class="col mx-1">
            <a href="{{route('shopRoles')}}">
                <div class="btn btn-info  text-center h-100" style="border-radius: 10px">
                    <div class="py-4 px-3">
                        <img src="{{asset('img/role-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white"> مدیریت نقش ها</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col mx-1">
            <a href="{{route('createRole')}}">
                <div class="btn btn-success  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/add-role-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white">افزودن نقش</h6>
                    </div>
                </div>
            </a>
        </div>
        <div class="col mx-1">
            <a href="{{route('userLogs')}}">
                <div class="btn btn-pinterest  text-center h-100" style="border-radius: 10px">
                    <div class="p-4">
                        <img src="{{asset('img/logs-white.svg')}}" alt="4rak" class="img-fluid rounded">
                        <h6 class="text-white"> بررسی کارکرد</h6>
                    </div>
                </div>
            </a>
        </div>

    </div>
    <!-- end row -->

    <div class="row my-3 justify-content-between">
        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">کارکنان فروشگاه</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                               data-bgColor="#F9B9B9" value="{{$personnel_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$personnel_qty}}</h2>
                        <p class="text-muted"> نفر</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-4 col-md-6">
            <div class="card-box">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>

                <h4 class="header-title m-t-0 m-b-30">نقش های فروشگاه</h4>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#2891a0 "
                               data-bgColor="#aacdd2" value="{{$roles_qty}}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true
                               data-thickness=".15"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{$roles_qty}} </h2>
                        <p class="text-muted"> نقش</p>
                    </div>
                </div>

            </div>
        </div><!-- end col -->

    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">آمار نقش های کارکنان</h4>
                <canvas id="personnel-roles" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

        <!-- end col -->

        <div class="col-lg-6">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0">کارکرد هفتگی کل کارکنان بر اساس ساعت</h4>
                <canvas id="work-time" class=""></canvas>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->

    <div class="row justify-content-center">
        <div class="col-lg-10">
            <div class="card-box ">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">فعال</a></li>
                        <li><a href="#">متن اول</a></li>
                        <li><a href="#">متن دوم</a></li>
                        <li class="divider"></li>
                        <li><a href="#">متن پاورقی</a></li>
                    </ul>
                </div>
                <h4 class="header-title m-t-0"> کارکرد ماهانه کارکنان بر اساس ساعت</h4>
                <canvas id="monthly-work" class=""></canvas>
            </div>
        </div>
        <!-- end col -->

    </div>



@endsection
@section('script')
    <script>
        $(document).ready(function () {
            let chart1 = document.getElementById('personnel-roles');
            let chart2 = document.getElementById('work-time');
            let chart3 = document.getElementById('monthly-work');



            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-personnel-roles-chart',
                },
                dataType: 'json',
                success: function (response) {
                    let qty={{$roles_qty}};
                    let colors=new Array();
                    for (let i=0;i<qty;i++){
                        let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
                        colors.push(randomColor)
                    }
                    let personnel_roles = new Chart(chart1, {
                        type: 'pie',
                        data: {
                            labels: response['roles'],
                            datasets: [{
                                label: 'Chart 1',
                                backgroundColor: colors,
                                data: response['role_qty']
                            }]
                        },
                        options: {}
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'get-personnel-work-time-chart',
                },
                dataType: 'json',
                success: function (response) {
                    let work_time = new Chart(chart2 , {
                        type: 'line',
                        data: {
                            labels: response['days'],
                            datasets: [{
                                label: '',
                                backgroundColor:'#3dd79a',
                                borderColor:'#0c923e',
                                pointBackgroundColor:'#167225',
                                lineTension:'.3',
                                data: response['work']
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: response['max']+2,
                                        min: 0,
                                        stepSize: 1
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    })
                },
                error: function (response) {
                    console.log('error')
                }
            });

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                url: '{{route('webAjax')}}',
                data: {// change data to this object
                    do: 'monthly-personnel-work-chart',
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                    let personnel=new Array();
                    let labels;
                    let data;
                    Object.keys(response['personnel']).forEach(function (key){
                        let randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
                        labels=Object.keys(response['personnel'][key]);
                       data=Object.values(response['personnel'][key])
                        let obj1= {
                            label:key,
                            backgroundColor:'transparent',
                            borderColor:randomColor,
                            pointBackgroundColor:randomColor,
                            lineTension:'.2',
                            data:Object.values(data)
                        };
                        personnel.push(obj1)
                    });

                    let monthly_chart = new Chart(chart3, {
                        type: 'line',
                        data: {
                            labels: labels,
                            datasets: personnel
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        max: 25,
                                        min: 0,
                                        stepSize: 1
                                    }
                                }],
                                xAxes: [{
                                }]
                            },
                            legend:false
                        }
                    });
                },
                error: function (response) {
                    console.log('error')
                }
            });

        })
    </script>
@endsection
