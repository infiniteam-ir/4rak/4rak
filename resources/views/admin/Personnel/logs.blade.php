@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">ورود و خروج کاربران فروشگاه</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1 text-center">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id != null)
                <div class="mt-0 bg-white">
                    <div class="row justify-content-center p-0 ">
                        <div class="col-12">
                            <h5 class=" alert alert-success w-100 text-center">برای مشاهده گزارش کامل فیلدهای تاریخ را
                                خالی بگذارید.</h5>
                        </div>
                        <div class="col-12 col-md-4">
                            <h6 class="text-muted">نام کاربر را وارد کنید:</h6>
                            <div class="autocomplete" style="width:300px;">
                                <input type="text" name="user" id="user" class="user d-none">
                                {!! Form::text('user',null,['class'=>'form-control','id'=>'users']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <h6 class="text-muted">از تاریخ:</h6>

                            {!! Form::text('start',null,['class'=>'form-control ','id'=>'start-date']) !!}
                        </div>
                        <div class="col-12 col-md-5 clearfix">
                            <h6 class="text-muted">تا تاریخ:</h6>
                            <span class="float-right">{!! Form::text('end',null,['class'=>'form-control ','id'=>'end-date']) !!}</span>
                            <span class="float-left">
                                  <button id="search" class="btn btn-outline-success">جستجو</button>
                            </span>
                        </div>
                    </div>
                </div>
                @if(count($logins)>0)

                    <div class="box-body">
                        <table id="table"
                               class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                            <tr class="table-header">
                                <th>کاربر</th>
                                <th>تاریخ</th>
                                <th>ساعت ورود</th>
                                <th> ساعت خروج</th>
                                <th>مدت زمان فعال بودن</th>
                            </tr>
                            {{--   @for($i=0;$i<count($logouts);$i++ )
                                   @php
                                       $login=verta($logins[$i]->login_time);
                                       $logout=verta($logouts[$i]->logout_time);
                                   @endphp
                                   <tr>
                                       <td>{{$logouts[$i]->user->name}} {{$logouts[$i]->user->family}}</td>
                                       <td>{{$logout->formatDate()}}</td>
                                       <td>{{$login->formattime()}}</td>
                                       <td>{{$logout->formattime()}}</td>
                                       <td>{{$login->diffMinutes($logout)}} دقیقه</td>
                                   </tr>
                               @endfor--}}
                        </table>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            رکوردی جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif

        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {

            var users = document.querySelector("#users")
            users.addEventListener("input", function () {
                if (users.value != ""){
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            user_name: $('#users').val(),
                            do: 'search-user',
                        },
                        dataType: 'json',
                        success: function (response) {
                            data = response;
                            if (response != "")
                                autocomplete(document.getElementById("users"), data);
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                }else {
                    var existingElement = document.getElementById("usersautocomplete-list")
                    console.log(existingElement)
                    if (existingElement)
                        existingElement.remove()
                }

            })

            function autocomplete(inp, arr) {
                var existingElement = document.getElementById("usersautocomplete-list")
                console.log(existingElement)
                if (existingElement)
                    existingElement.remove()

                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;

                var a, b = null
                /*, val = this.value;
                                /!*close any already open lists of autocompleted values*!/
                                closeAllLists();
                                if (!val) {
                                    return false;
                                }*/
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items border border-primary rounded bg-light py-2 px-1");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (var i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i]['family'] != null) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + " " + arr[i]['family'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + " " + arr[i]['family'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            user = document.getElementById('user')
                            user.value = this.getElementsByTagName("input")[0].id
                            document.getElementById("user").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    } else {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i]['name'] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input id='" + arr[i]['id'] + "' type='hidden' value='" + arr[i]['name'] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            user = document.getElementById('user')
                            user.value = this.getElementsByTagName("input")[0].id
                            document.getElementById("user").value = this.getElementsByTagName("input")[0].id
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }


                /*execute a function when someone writes in the text field:*/
                /*inp.addEventListener("input", function (e) {

                });*/

                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x) x[currentFocus].click();
                        }
                    }
                });

                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x) return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }

                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }

                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }

                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }


            $('#search').on("click", function () {
                user_id = $("#user").val()
                console.log(user_id)
                $("#table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>کاربر</th>\n' +
                    '                            <th>تاریخ</th>\n' +
                    '                            <th>ساعت ورود</th>\n' +
                    '                            <th>ساعت خروج </th>\n' +
                    '                            <th>مدت زمان فعال بودن </th>\n' +
                    '                        </tr>'
                user_name = $('#users').val()
                if (user_name == '') {
                    $("#table tr").remove();
                    table.innerHTML = '  <tr class="table-header">\n' +
                        '                            <th>کاربر</th>\n' +
                        '                            <th>تاریخ</th>\n' +
                        '                            <th>ساعت ورود</th>\n' +
                        '                            <th>ساعت خروج </th>\n' +
                        '                            <th>مدت زمان فعال بودن </th>\n' +
                        '                        </tr>'
                } else {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('.ajax.d-none').attr('id'),
                        data: {// change data to this object
                            do: 'search-user-logs',
                            id: user_id,
                            start: $('#start-date').val(),
                            end: $('#end-date').val(),
                        },
                        dataType: 'json',
                        success: function (response) {
                            console.log(response)
                            var table = document.getElementById('table')
                            var i;
                            for (i = 0; i < response['logouts'].length; i++) {
                                var row = table.insertRow(-1);
                                var td0 = row.insertCell(0)
                                var td1 = row.insertCell(1)
                                var td2 = row.insertCell(2)
                                var td3 = row.insertCell(3)
                                var td4 = row.insertCell(4)
                                var dateFormat = new Intl.DateTimeFormat("fa");
                                const fmt = new Intl.DateTimeFormat("fa", {
                                    hour: "numeric",
                                    minute: "numeric"
                                });
                                const persianNum = new Intl.NumberFormat("fa")
                                // console.log(fmt.format(date));
                                logout_time = Date.parse(response['logouts'][i]['logout_time'])
                                login_time = Date.parse(response['logins'][i]['login_time'])
                                login_time = fmt.format(login_time)
                                logout_time_d = dateFormat.format(logout_time)
                                logout_time = fmt.format(logout_time)

                                td0.innerHTML = response['logouts'][i]['user']['name'] + " " + response['logouts'][i]['user']['family']
                                td1.innerHTML = logout_time_d
                                td2.innerHTML = login_time
                                td3.innerHTML = logout_time

                                td4.innerHTML = persianNum.format(response['diff'][i]) + " دقیقه"
                            }
                        },
                        error: function (response) {
                            console.log('error')
                        }
                    });
                }
            })

            $('#start-date').persianDatepicker({
                altField: '#start-date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
            });
            $('#end-date').persianDatepicker({
                altField: '#end-date',
                altFormat: "YYYY/MM/DD",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
            });
        })
    </script>

@endsection


