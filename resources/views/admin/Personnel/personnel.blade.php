@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">کارکنان فروشگاه</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <div class="nav  d-flex justify-content-center ">
                            <ul class=" d-flex justify-content-between list-unstyled w-100 user-list-menu p-2 checkout-ul ">
                                <li class="blog-list-item ">
                                    <a href="{{route('personnel.create')}}" class="btn btn-outline-success">
                                        <i class="fa fa-plus"></i>
                                        افزودن
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if(count($personnel)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                            <tr class="table-header">
                                <th>نام و نام خانوادگی</th>
                                <th>تلفن</th>
                                <th>نقش</th>
                            </tr>

                            @foreach($personnel as $person )
                                <tr>
                                    <td>
                                        <a class="text-decoration-none" href="{{route('personnel.edit',$person->id)}}">
                                            {{$person->name}} {{$person->family}}
                                        </a>
                                    </td>
                                    <td>{{$person->mobile}}</td>
                                    <td>
                                        <a class="text-decoration-none" data-toggle="modal"
                                           data-target="#RoleModal-{{$person->id}}" data-whatever="@getbotstrap">
                                            @if($person->shop_role_id)
                                                {{$person->shoprole->roles->role_name}}
                                            @else
                                                تعیین نشده
                                            @endif
                                        </a>

                                        {{--MODAL--}}
                                        <div class="modal fade" id="RoleModal-{{$person->id}}" tabindex="-1"
                                             role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog ">
                                                <div class="modal-content">
                                                    <div class=" modal-header ">
                                                        <h5 class="modal-title text-warning" id="exampleModalLabel">
                                                            تعیین
                                                            نقش </h5>
                                                    </div>
                                                    {!! Form::open(['POST'=>'DELETE','action' =>['AdminPersonnelController@setUserRole',$person->id]]) !!}
                                                    <div class="modal-body ">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">نقش کاربر
                                                                را
                                                                از لیست انتخاب کنید: </label>
                                                            {{--{!! Form::select('role',[$arrayRole],null,['class'=>'form-control']) !!}--}}
                                                            <select name="role" class="form-control">
                                                                @foreach($roles as $role)
                                                                    <option value="{{$role->roles->role_name}}">
                                                                        {{$role->roles->role_name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer ">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">انصراف
                                                        </button>
                                                        <button class="btn btn-success" type="submit">ثبت</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$personnel->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            پرسنلی جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection


