@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">ویرایش حساب</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                {!! Form::model($account,['method'=>'PATCH','action'=>['AdminAccountantController@updateAccount',$account->id]]) !!}
                <div class="row p-4 d-flex pb-5">
                    <div class="row w-100 mt-3 justify-content-center align-items-center ">
                        <div class="col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">نام حساب:</h6>
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-right">
                            <h6 class="text-muted">مقدار:</h6>
                            {!! Form::text('amount',null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center ">
                        <div class="col-12 mt-3 col-md-4 text-center">
                            {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                        </div>
                        <div class="col-12 mt-3 col-md-4 text-center">
                            <a class="btn btn-danger w-50 " href="{{route('accounts')}}">انصراف</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection


