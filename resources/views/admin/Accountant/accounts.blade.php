@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">حسابها</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('msg'))
                <div class="alert alert-success text-center mt-1">
                    <h4 class="text-success">
                        {{Session::pull('msg')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 mx-3 mb-5">
                        <a href="{{route('accounts.create')}}" class="btn btn-outline-success">
                            <i class="fa fa-plus"></i>
                            افزودن حساب
                        </a>
                    </div>
                </div>
                <div class="container">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="my" data-toggle="tab" href="#nav-my" role="tab"
                               aria-controls="nav-my" aria-selected="true">حساب های من</a>

                            <a class="nav-item nav-link" id="providers" data-toggle="tab" href="#nav-providers"
                               role="tab"
                               aria-controls="nav-providers" aria-selected="false">حساب تامین کنندگان</a>

                            <a class="nav-item nav-link" id="customers" data-toggle="tab" href="#nav-customers"
                               role="tab"
                               aria-controls="nav-customer" aria-selected="false">حساب مشتریان</a>

                            <a class="nav-item nav-link" id="debt" data-toggle="tab" href="#nav-debt" role="tab"
                               aria-controls="nav-debt" aria-selected="false">بدهی ها</a>

                            <a class="nav-item nav-link" id="credit" data-toggle="tab" href="#nav-credit" role="tab"
                               aria-controls="nav-credit" aria-selected="false">طلب ها</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active p-3" id="nav-my" role="tabpanel" aria-labelledby="my">
                            <div class="autocomplete" style="width:300px;">
                                {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'my-accounts']) !!}
                            </div>
                            <div class="bg-white text-center m-0 w-100">
                                <h6 class="bg-success text-white py-2">حساب های من
                                </h6>
                            </div>
                            <div class="box-body">
                                @if(count($my_accs)>0)
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                                           id="table1">
                                        <tr class="table-header">
                                            <th>نام حساب</th>
                                            <th>تراز</th>
                                        </tr>
                                        @foreach($my_accs as $my_acc)
                                            <tr>
                                                <td>{{$my_acc->name}}</td>
                                                <td>
                                                    @if($my_acc->amount<0)
                                                        <span class="text-bold text-danger">{{number_format($my_acc->amount*-1)}}</span>
                                                    @else
                                                        <span class="text-bold">{{number_format($my_acc->amount)}}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما حسابی ثبت نکرده اید
                                    </h6>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-providers" role="tabpanel"
                             aria-labelledby="providers">
                            <div class="autocomplete" style="width:300px;">
                                {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'provider-accounts']) !!}
                            </div>
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-primary text-white py-2">حساب تامین کنندگان</h6>
                                <p class="bg-light text-danger">حساب با رنگ قرمز به معنای بدهی این تامین کننده به شما
                                    است.</p>
                            </div>
                            <div class="box-body">
                                @if(count($providers_acc)>0)
                                    <table id="provider-table"
                                           class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                        <tr class="table-header">
                                            <th>نام</th>
                                            <th>تراز</th>
                                        </tr>
                                        @foreach($providers_acc as $provider_acc)
                                            <tr>
                                                <td>{{$provider_acc->name}}</td>
                                                <td>
                                                    @if($provider_acc->amount<0)
                                                        <span class="text-bold text-danger">{{number_format($provider_acc->amount*-1)}}</span>
                                                    @else
                                                        <span class="text-bold">{{number_format($provider_acc->amount)}}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما تامین کننده ای ندارید.
                                    </h6>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-customers" role="tabpanel"
                             aria-labelledby="customers">
                            <div class="autocomplete" style="width:300px;">
                                {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'customer-accounts']) !!}
                            </div>
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-warning text-white py-2">حساب مشتری ها</h6>
                                <p class="bg-light text-danger">حساب با رنگ قرمز به معنای بدهی این مشتری به شما است.</p>

                            </div>
                            <div class="box-body">
                                @if(count($customers_acc)>0)
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                                           id="customer-table">
                                        <tr class="table-header">
                                            <th>نام</th>
                                            <th>تراز</th>
                                        </tr>
                                        @foreach($customers_acc as $customer_acc)
                                            <tr>
                                                <td>{{$customer_acc->name}}</td>
                                                <td>
                                                    @if($customer_acc->amount<0)
                                                        <span class="text-bold text-danger">{{number_format($customer_acc->amount*-1)}}</span>
                                                    @else
                                                        <span class="text-bold">{{number_format($customer_acc->amount)}}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما مشتری ای ندارید.
                                    </h6>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-debt" role="tabpanel" aria-labelledby="debt">
                            <div class="autocomplete" style="width:300px;">
                                {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'debts']) !!}
                            </div>
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-danger text-white py-2">بدهی ها</h6>
                            </div>
                            <div class="box-body">
                                @if(count($debts)>0)
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                                           id="debt-table">
                                        <tr class="table-header">
                                            <th>طرف حساب</th>
                                            <th>مبلغ</th>
                                        </tr>
                                        @foreach($debts as $debt)
                                            <tr>
                                                <td>{{$debt->name}}</td>
                                                <td>
                                                    <span class="text-bold text-danger">{{number_format($debt->amount)}}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما بدهکار نیستید
                                    </h6>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade p-3" id="nav-credit" role="tabpanel" aria-labelledby="credit">
                            <div class="autocomplete" style="width:300px;">
                                {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو...','class'=>'form-control','id'=>'credits']) !!}
                            </div>
                            <div class="text-center bg-white m-0 w-100">
                                <h6 class="bg-success text-white py-2">طلب ها</h6>
                            </div>
                            <div class="box-body">
                                @if(count($credits)>0)
                                    <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                                           id="credit-table">
                                        <tr class="table-header">
                                            <th>طرف حساب</th>
                                            <th>مبلغ</th>
                                        </tr>
                                        @foreach($credits as $credit)
                                            <tr>
                                                <td>{{$credit->name}}</td>
                                                <td>
                                                    <span class="text-bold text-success">{{number_format($credit->amount*-1)}}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <h6 class="alert alert-success  py-2">
                                        شما طلبی ندارید
                                    </h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("#my-accounts").on("keyup", function () {
                var table = document.getElementById('table1')

                $("#table1 tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام حساب</th>\n' +
                    '                            <th>تراز</th>\n' +
                    '                        </tr>'

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        acc_name: $('#my-accounts').val(),
                        do: 'search-my-accounts',
                    },
                    dataType: 'json',
                    success: function (response) {
                        response['data'].forEach(function (re) {
                            var row = table1.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            td0.innerHTML = re['name']
                            if (re['amount'] < 0) {
                                td1.innerHTML = '<span class="text-bold text-danger">' + re['amount'] * -1 + '</span>\n'
                            } else {
                                td1.innerHTML = '<span class="text-bold">' + re['amount'] + '</span>\n'
                            }
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $("#provider-accounts").on("keyup", function () {
                var table = document.getElementById('provider-table')

                $("#provider-table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام حساب</th>\n' +
                    '                            <th>تراز</th>\n' +
                    '                        </tr>'

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        acc_name: $('#provider-accounts').val(),
                        do: 'search-provider-accounts',
                    },
                    dataType: 'json',
                    success: function (response) {
                        response['data'].forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            td0.innerHTML = re['name']
                            if (re['amount'] < 0) {
                                td1.innerHTML = '<span class="text-bold text-danger">' + re['amount'] * -1 + '</span>\n'
                            } else {
                                td1.innerHTML = '<span class="text-bold">' + re['amount'] + '</span>\n'
                            }
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $("#customer-accounts").on("keyup", function () {
                var table = document.getElementById('customer-table')
                $("#customer-table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام حساب</th>\n' +
                    '                            <th>تراز</th>\n' +
                    '                        </tr>'
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        acc_name: $('#customer-accounts').val(),
                        do: 'search-customer-accounts',
                    },
                    dataType: 'json',
                    success: function (response) {
                        response['data'].forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            td0.innerHTML = re['name']
                            if (re['amount'] < 0) {
                                td1.innerHTML = '<span class="text-bold text-danger">' + re['amount'] * -1 + '</span>\n'
                            } else {
                                td1.innerHTML = '<span class="text-bold">' + re['amount'] + '</span>\n'
                            }
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $("#debts").on("keyup", function () {
                var table = $('#debt-table')
                $("#debt-table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام حساب</th>\n' +
                    '                            <th>تراز</th>\n' +
                    '                        </tr>'
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        acc_name: $('#debts').val(),
                        do: 'search-debts',
                    },
                    dataType: 'json',
                    success: function (response) {
                        response['data'].forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            td0.innerHTML = re['name']
                            td1.innerHTML = re['amount']
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });

            $("#credits").on("keyup", function () {
                var table = document.getElementById('credit-table')
                $("#credit-table tr").remove();
                table.innerHTML = '  <tr class="table-header">\n' +
                    '                            <th>نام حساب</th>\n' +
                    '                            <th>تراز</th>\n' +
                    '                        </tr>'
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $('.ajax.d-none').attr('id'),
                    data: {// change data to this object
                        acc_name: $('#credits').val(),
                        do: 'search-credits',
                    },
                    dataType: 'json',
                    success: function (response) {
                        response['data'].forEach(function (re) {
                            var row = table.insertRow(-1);
                            var td0 = row.insertCell(0)
                            var td1 = row.insertCell(1)
                            td0.innerHTML = re['name']
                            td1.innerHTML = re['amount']
                        })
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });
        })
    </script>
@endsection

