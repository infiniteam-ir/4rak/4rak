@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">حسابداری کل</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row w-100">
                <div class="col-12 col-md-6  justify-content-center text-center">
                    <div class="box-header bg-white m-0 w-100">
                        <h6 class="bg-danger text-white py-2">هزینه ها</h6>
                    </div>
                    <div class="box-body">
                        @if(count($outgoes)>0)
                            <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                <tr class="table-header">
                                    <th>ردیف</th>
                                    <th>عنوان</th>
                                    <th>مبلغ(تومان)</th>
                                    <th>تاریخ</th>
                                </tr>
                                @php($total=0)
                                @foreach($outgoes as $outgo)
                                    <tr>
                                        <td>{{$outgo->id}}</td>
                                        <td>
                                            @switch($outgo->target_type)
                                                @case('App\Outgo_Bill')
                                                {{$outgo->bill->name}}
                                                @break
                                                @case('App\Outgo_Salary')
                                                    حقوق  {{$outgo->salary->user->name}} {{$outgo->salary->user->family}}
                                                @break
                                                @case('App\Outgo_Other')
                                                {{$outgo->other->target}}
                                                @break
                                                @case('App\Outgo_Charak_Service')
                                                {{$outgo->Charak_Service->target}}
                                                @break
                                                @case('فاکتور خرید')
                                                {{$outgo->target_type}}
                                                @break
                                                @case('App\Input_Factor')
                                                فاکتور خرید
                                                @break
                                                @endswitch
                                        </td>
                                        <td>{{number_format($outgo->price)}}</td>
                                        <td>{{$outgo->created_at}}</td>
                                    </tr>
                                    @php($total+=$outgo->price)
                                @endforeach
                            </table>
                            <span class="total d-none" id="{{$total}}"></span>
                            <h6 class="alert alert-danger  py-2">
                                جمع کل: {{number_format($total)}}
                            </h6>
                        @else
                            <h6 class="alert alert-danger  py-2">
                                شما تاکنون هزینه ای نداشته اید.
                            </h6>
                        @endif
                        <div class="row d-flex justify-content-center"></div>
                    </div>
                </div>
                {{--<span class="d-none d-md-block" style="border-left: 1px solid grey"></span>--}}
                <div class="col-12 col-md-6  justify-content-center text-center">
                    <div class="box-header bg-white m-0 w-100">
                        <h6 class="bg-success text-white py-2">درآمدها</h6>
                    </div>
                    <div class="box-body">
                        @if(count($incomes)>0)
                            <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                <tr class="table-header">
                                    <th>ردیف</th>
                                    <th>عنوان</th>
                                    <th>مبلغ(تومان)</th>
                                    <th>تاریخ</th>
                                </tr>
                                @php($totalearn=0)
                                @foreach($incomes as $income)
                                    <tr>
                                        <td>{{$income->id}}</td>
                                        <td>{{$income->source_type}}</td>
                                        <td>{{number_format($income->price)}}</td>
                                        <td>{{$income->created_at}}</td>
                                    </tr>
                                    @php($totalearn+=$income->price)
                                @endforeach
                            </table>
                            <span id="{{$totalearn}}" class="totaleran d-none"></span>
                            <h6 class="alert alert-success  py-2">
                                جمع کل: {{number_format($totalearn)}}
                            </h6>
                        @else
                            <h6 class="alert alert-success  py-2">
                                شما تاکنون درامدی نداشته اید.
                            </h6>
                        @endif
                        <div class="row d-flex justify-content-center"></div>
                    </div>
                </div>
            </div>
            <div class="row w-100">
                <div class="col-12">
                    <canvas id="chart1" class="p-5"></canvas>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var chart1 = document.getElementById('chart1');
            var income = $('.totaleran.d-none').attr('id')
            var outgo = $('.total.d-none').attr('id')

            var chart = new Chart(chart1, {
                type: 'pie',
                data: {
                    labels: ['درآمد', 'هزینه'],
                    datasets: [{
                        label: 'Chart 1',
                        backgroundColor: ['#4fff8d', '#ff7b73'],
                        data: [income, outgo]
                    }]
                },
                options: {}
            });
        })
    </script>
@endsection


