@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">افزودن حساب</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="box-body">
                @livewire('accountant.add-acc',['banks'=>$banks])
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            var acc_type=document.getElementById('type')
            acc_type.addEventListener("change",function () {
                var acc_type_id=this.value
                switch (acc_type_id){
                    case '2':
                        $('section#bank-section').removeClass('d-none')
                        $('section#cheque-section').addClass('d-none')
                        break;
                    case '1':
                        $('section#bank-section').addClass('d-none')
                        $('section#cheque-section').addClass('d-none')
                        break;
                    case '3':
                        $('section#bank-section').addClass('d-none');
                        $('section#cheque-section').removeClass('d-none');
                        break;
                }
            });

            var banks=document.getElementById('cheque_bank')
            banks.addEventListener("change",function (){
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{route('webAjax')}}',
                    data: {// change data to this object
                        do: 'search-banks-by-id',
                        id:banks.value
                    },
                    dataType: 'json',
                    success: function (response) {
                        var amount=$('#amount')
                        amount.addClass('disabled')
                        amount.val(response['amount'])
                    },
                    error: function (response) {
                        console.log('error')
                    }
                });
            });


        })
    </script>
@endsection
