@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">ثبت هزینه جدید</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('msg'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('msg')}}
                    </h4>
                </div>
            @endif
            @if(Auth::user()->shop_id!= null)
                <div class="row w-100">
                    <div class="container">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="bill" data-toggle="tab" href="#nav-bill"
                                   role="tab"
                                   aria-controls="nav-bill" aria-selected="true">قبض</a>

                                <a class="nav-item nav-link" id="employee" data-toggle="tab" href="#nav-employee"
                                   role="tab"
                                   aria-controls="nav-employee" aria-selected="false">حقوق کارکنان</a>

                                <a class="nav-item nav-link" id="charak" data-toggle="tab" href="#nav-charak"
                                   role="tab"
                                   aria-controls="nav-charak" aria-selected="false">هزینه خدمات چارک</a>

                                <a class="nav-item nav-link" id="other" data-toggle="tab" href="#nav-other" role="tab"
                                   aria-controls="nav-other" aria-selected="false">سایر</a>

                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active p-3" id="nav-bill" role="tabpanel"
                                 aria-labelledby="bill">
                                <div class="text-center bg-white m-0 w-100">
                                    <h6 class="bg-success text-white py-2">قبض</h6>
                                </div>
                                <div class="">
                                    @livewire('accountant.outgo.set-bill',['accounts'=>$accounts])
                                </div>
                            </div>
                            <div class="tab-pane fade p-3" id="nav-employee" role="tabpanel"
                                 aria-labelledby="employee">
                                <div class="text-center bg-white m-0 w-100">
                                    <h6 class="bg-primary text-white py-2">حقوق کارکنان</h6>
                                </div>
                                <div class="">
{{--                                    @livewire('accountant.outgo.set-salary',['accounts'=>$accounts,'users'=>$users])--}}
                                    <div>
                                            {!! Form::open(['method'=>'POST','action'=>'AdminAccountantController@saveOutgo'] ) !!}
                                        <form>
                                            <div class="row  d-flex pb-5 justify-content-center">
                                                <div class="col-12 col-md-8 ">
                                                    <div class="row w-100  justify-content-center align-items-center ">
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted">نام کارمند: </h6>
                                                            <select class="form-control" name="user" id="" wire:model.lazy="user">
                                                                <option value="none">انتخاب کنید...</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}">
                                                                        {{$user->name}} {{$user->family}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            @error('user') <span class="text-danger mt-2">{{$message}}</span> @enderror
                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted"><sup class="text-danger">*</sup> مبلغ پرداختی:</h6>
                                                            {{--                    {!! Form::text('price',null,['class'=>'form-control']) !!}--}}
                                                            <input type="text" name="price" class="form-control" wire:model.lazy="price">
                                                            @error('price') <span class="text-danger mt-2">{{$message}}</span> @enderror

                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6" >
                                                            <h6 class="text-muted mb-0"><sup class="text-danger">*</sup> از تاریخ:</h6>
{{--                                                            <p class="mt-0">(مثال: 1400/06/01)</p>--}}
                                                            <div >
                                                                <input type="text" name="start" autocomplete="off" class="form-control"   wire:model.lazy="start" id="start" >
                                                            </div>
                                                            @error('start') <span class="text-danger mt-2">{{$message}}</span> @enderror
                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6" >
                                                            <h6 class="text-muted mb-0"><sup class="text-danger">*</sup> تا تاریخ:</h6>
{{--                                                            <p class="mt-0">(مثال: 1400/06/31)</p>--}}

                                                            <div >
                                                                <input type="text" name="end" autocomplete="off" class="form-control" wire:model.lazy="end" id="end">
                                                            </div>
                                                            @error('end') <span class="text-danger mt-2">{{$message}}</span> @enderror
                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted"><sup class="text-danger">*</sup> نحوه محاسبه: <span>(ساعتی ،روزانه، ماهانه و ...)</span> </h6>

                                                            {{--                    {!! Form::text('calc_method',null,['class'=>'form-control']) !!}--}}
                                                            <input type="text" name="calc_method" class="form-control" wire:model.lazy="calc_method">
                                                            @error('calc_method') <span class="text-danger mt-2">{{$message}}</span> @enderror

                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted"><sup class="text-danger">*</sup> میزان کارکرد:</h6>
                                                            <input type="text" name="total_work" class="form-control" wire:model.lazy="total_work">
                                                            {{--                    {!! Form::number('total_work',null,['class'=>'form-control']) !!}--}}
                                                            @error('total_work') <span class="text-danger mt-2">{{$message}}</span> @enderror

                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted">کد پیگیری:</h6>
                                                            {{--                    {!! Form::text('tracking_id',null,['class'=>'form-control']) !!}--}}
                                                            <input type="text" name="tracking_id" class="form-control" wire:model.lazy="tracking_id">
                                                            @error('tracking_id') <span class="text-danger mt-2">{{$message}}</span> @enderror

                                                        </div>
                                                        <div class="col-12 mt-3 col-md-6">
                                                            <h6 class="text-muted"><sup class="text-danger">*</sup> حساب:</h6>
                                                            <select class="form-control" name="source" id="" wire:model.lazy="source">
                                                                <option value="none">انتخاب کنید...</option>
                                                                @foreach($accounts as $account)
                                                                    <option value="{{$account->id}}">
                                                                        {{$account->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            @error('source') <span class="text-danger mt-2">{{$message}}</span> @enderror
                                                            <input name="type" type="hidden" value="employee">

                                                        </div>
                                                    </div>

                                                    <div class="row w-100 mt-3 mb-5 justify-content-center align-items-center ">
                                                        <div class="col-12 mt-3 col-md-4 text-center">
                                                            {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                            {!! Form::close() !!}
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane fade p-3" id="nav-charak" role="tabpanel"
                                 aria-labelledby="charak">
                                <div class="text-center bg-white m-0 w-100">
                                    <h6 class="bg-warning text-white py-2">خدمات چارک</h6>
                                </div>
                                <div class="">
                                    @livewire('accountant.outgo.set-charak-services',['accounts'=>$accounts])
                                </div>
                            </div>
                            <div class="tab-pane fade p-3" id="nav-other" role="tabpanel" aria-labelledby="other">
                                <div class="text-center bg-white m-0 w-100">
                                    <h6 class="bg-danger text-white py-2"> سایر هزینه ها</h6>
                                </div>
                                <div class="">
                                    @livewire('accountant.outgo.set-other',['accounts'=>$accounts])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $('#start').persianDatepicker({
            altField: '#start',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
        $('#end').persianDatepicker({
            altField: '#end',
            altFormat: "YYYY/MM/DD",
            observer: true,
            format: 'YYYY/MM/DD',
            initialValue: false,
            initialValueType: 'persian',
            autoClose: true,

        });
    </script>

@endsection


