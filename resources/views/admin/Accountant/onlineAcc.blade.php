@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('main')

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">حسابداری آنلاین</h1>
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">
            @if(Session::has('success'))
                <div class="alert alert-success mt-1">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="container">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="buy" data-toggle="tab" href="#nav-buy" role="tab"
                           aria-controls="nav-buy" aria-selected="true">فاکتور خرید</a>
                        <a class="nav-item nav-link" id="sell" data-toggle="tab" href="#nav-sell" role="tab"
                           aria-controls="nav-sell" aria-selected="false">فاکتور فروش</a>
                        <a class="nav-item nav-link" id="spent" data-toggle="tab" href="#nav-spent" role="tab"
                           aria-controls="nav-spent" aria-selected="false">هزینه ها</a>
                        <a class="nav-item nav-link" id="profit" data-toggle="tab" href="#nav-profit" role="tab"
                           aria-controls="nav-profit" aria-selected="false">درآمد ها</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active p-3" id="nav-buy" role="tabpanel" aria-labelledby="buy">
                        <div class="box-header bg-white m-0 w-100">
                            <h6 class="bg-success text-white py-2">خریدها</h6>
                        </div>
                        <div class="box-body">
                            @if(count($buy_factors)>0)
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2  ">
                                    <tr class="table-header">
                                        <th>شماره فاکتور</th>
                                        <th>فروشنده</th>
                                        <th>مبلغ(تومان)</th>
                                        <th>تاریخ</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @php($total_buy=0)
                                    @foreach($buy_factors as $buy_factor)
                                        <tr>
                                            <td>{{$buy_factor->f_num}}</td>
                                            <td>{{$buy_factor->seller}}</td>
                                            <td>{{$buy_factor->total_price}}</td>
                                            <td>{{$buy_factor->f_date}}</td>
                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        @php($total_buy+=$buy_factor->total_price)
                                    @endforeach
                                </table>
                                <h6 class="alert alert-success  py-2">
                                    جمع کل: {{$total_buy}}
                                </h6>
                            @else
                                <h6 class="alert alert-success  py-2">
                                    شما فاکتور خریدی نداشته اید
                                </h6>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade p-3" id="nav-sell" role="tabpanel" aria-labelledby="sell">
                        <div class="box-header bg-white m-0 w-100">
                            <h6 class="bg-primary text-white py-2">فروش ها</h6>
                        </div>
                        <div class="box-body">
                            @if(count($sell_factors)>0)
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2">
                                    <tr class="table-header">
                                        <th>شماره فاکتور</th>
                                        <th>خریدار</th>
                                        <th>مبلغ(تومان)</th>
                                        <th>تاریخ</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @php($total_sell=0)
                                    @foreach($sell_factors as $sell_factor)
                                        <tr>
                                            <td>{{$sell_factor->id}}</td>
                                            <td>{{$sell_factor->customer->name}} {{$sell_factor->customer->family}}</td>
                                            <td>{{$sell_factor->total_price}}</td>
                                            <td>{{$sell_factor->f_date}}</td>
                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        @php($total_sell+=$sell_factor->total_price)
                                    @endforeach
                                </table>
                                <h6 class="alert alert-success  py-2">
                                    جمع کل: {{$total_sell}}
                                </h6>
                            @else
                                <h6 class="alert alert-success  py-2">
                                    شما فاکتور فروشی ندارید.
                                </h6>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade p-3" id="nav-spent" role="tabpanel" aria-labelledby="spent">
                        <div class="box-header bg-white m-0 w-100">
                            <h6 class="bg-warning text-white py-2">هزینه ها</h6>
                        </div>
                        <div class="box-body">
                            @if(count($outgoes)>0)
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2">
                                    <tr class="table-header">
                                        <th>عنوان</th>
                                        <th>مبلغ(تومان)</th>
                                        <th>تاریخ</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @php($total_out=0)
                                    @foreach($outgoes as $outgo)
                                        <tr>
                                            <td>
                                                @switch($outgo->target_type)
                                                    @case('App\Outgo_Bill')
                                                    {{$outgo->bill->name}}
                                                    @break
                                                    @case('App\Outgo_Salary')
                                                    حقوق  {{$outgo->salary->user->name}} {{$outgo->salary->user->family}}
                                                    @break
                                                    @case('App\Outgo_Other')
                                                    {{$outgo->other->target}}
                                                    @break
                                                    @case('App\Outgo_Charak_Service')
                                                    {{$outgo->Charak_Service->target}}
                                                    @break
                                                    @case('فاکتور خرید')
                                                    {{$outgo->target_type}}
                                                    @break
                                                @endswitch
                                            </td>
                                            <td>{{$outgo->price}}</td>
                                            <td>{{$outgo->created_at}}</td>
                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        @php($total_out+=$outgo->price)
                                    @endforeach
                                </table>
                                <h6 class="alert alert-success  py-2">
                                    جمع کل: {{$total_out}}
                                </h6>
                            @else
                                <h6 class="alert alert-success  py-2">
                                    هزینه ای ثبت نشده است
                                </h6>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade p-3" id="nav-profit" role="tabpanel" aria-labelledby="profit">
                        <div class="box-header bg-white m-0 w-100">
                            <h6 class="bg-secondary text-white py-2">درآمد ها</h6>
                        </div>
                        <div class="box-body">
                            @if(count($incomes)>0)
                                <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2">
                                    <tr class="table-header">
                                        <th>عنوان</th>
                                        <th>مبلغ(تومان)</th>
                                        <th>تاریخ</th>
                                        <th>امکانات</th>
                                    </tr>
                                    @php($total_in=0)
                                    @foreach($incomes as $income)
                                        <tr>
                                            <td>{{$income->source_type}}</td>
                                            <td>{{$income->price}}</td>
                                            <td>{{$income->created_at}}</td>
                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        @php($total_in+=$income->price)
                                    @endforeach
                                </table>
                                <h6 class="alert alert-success  py-2">
                                    جمع کل: {{$total_in}}
                                </h6>
                            @else
                                <h6 class="alert alert-success  py-2">
                                    درآمدی ثبت نشده است
                                </h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')

@endsection


