@extends('admin.layouts.newTheme.master')
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('main')
    <span class="ajax d-none" id="{{route('webAjax')}}"></span>

    <div class="row p-4 d-flex pb-5">
        <div class="col-12 alert alert-light text-center ">
            <h1 class="text-muted">فاکتورهای پرداخت نشده</h1>
            @if(Session::has('success'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('success')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('deleted'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('deleted')}}
                    </h4>
                </div>
            @endif
            @if(Session::has('saved'))
                <div class="alert alert-success text-center mt-2 ">
                    <h4 class="text-success">
                        {{Session::pull('saved')}}
                    </h4>
                </div>
            @endif
        </div>
        <div class="col-12 bg-white p-2 rounded box-shadow">

            @if(Auth::user()->shop_id!= null)
<!--                <div class=" mt-0 bg-white">
                    <div class="col justify-content-center p-0 ">
                        <input type="text" name="service_id" id="service_name" class="service_name d-none">
                        <div class="autocomplete" style="width:300px;">
                            {!! Form::text('name',null,['autocomplete'=>'off','placeholder'=>'جستجو با عنوان ','class'=>'form-control','id'=>'service_names']) !!}
                        </div>
                    </div>
                </div>-->
                @if(count($factors)>0)
                    <div class="box-body">
                        <table class="table table-bordered table-responsive-md table-hover table-striped table-header mt-2"
                               id="table">
                            <tr class="table-header text-center">
                                <th>شماره فاکتور</th>
                                <th>فروشنده</th>
                                <th>تاریخ</th>
                                <th>مبلغ (تومان)</th>
                                <th>امکانات</th>
                            </tr>
                            @foreach($factors as $factor )
                                <tr>
                                    <td>{{$factor->f_num}}</td>
                                    <td>{{$factor->provider->name.' '.$factor->provider->family}}</td>
                                    <td>{{verta($factor->f_date)->formatDate()}}</td>
                                    <td>{{number_format($factor->total_price)}}</td>
                                    <td>
                                        <div class="row w-100 justify-content-center">
                                            <div class="col-12 col-md-6 ">
                                                <a class=" btn btn-success"
                                                   href="{{route('factorPurchase',$factor->f_num)}}">
                                                    <i class="fa fa-edit"></i>
                                                    ثبت پرداخت
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="row d-flex justify-content-center">{{$factors->links()}}</div>
                    </div>
                @else
                    <div class="alert alert-warning mt-1 text-center">
                        <h4 class="text-warning">
                            فاکتوری جهت نمایش وجود ندارد
                        </h4>
                    </div>
                @endif
            @else
                <div class="alert alert-danger mt-1 text-center">
                    <h4 class="text-danger">
                        شما هنوز فروشگاهی ثبت نکرده اید.
                    </h4>
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
    <!-- Sweet Alert js -->
    <script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
    <!-- Sweet Alert js -->


@endsection


