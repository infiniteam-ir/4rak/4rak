
<nav class="navbar navbar-dark bg-color1 navbar-expand-lg p-0 main-shadow" >
    <div class="row w-100">
        <div class="col-4 col-lg-2 px-0">
            <div class="d-flex align-items-center justify-content-center bg-color1 p-2" data-toggle="collapse" data-target="#sidebar">
                <a class="nav-link color4 charak-logo" href="#">پنل مدیریت</a>
                <span class="fa fa-bars color4 mx-2" ></span>
            </div>
        </div>
        <div class="col-8 col-lg-10 d-flex  align-items-center">
            <div class="navbar-collapse d-flex  align-items-center">
                <ul class="navbar-nav d-inline-block  mr-5 color3">
                    {{--<li class=" my-nav-item">
                        <a class="my-nav-link active" href="#">لینک</a>
                    </li>
                    <li class=" my-nav-item">
                        <a class="my-nav-link" href="#">لینک</a>
                    </li>
                    <li class="my-nav-item">
                        <a class="my-nav-link" href="#">لینک</a>
                    </li>--}}
                </ul>
                <div class="mr-auto p-2 float-left">
                    {{--<a class="btn-sm bg-color2 text-decoration-none text-light" href="#">dokme</a>
                    <a class="btn-sm bg-color2 text-decoration-none text-light" href="#">dokme</a>
                    <a class="btn-sm bg-color2 text-decoration-none text-light" href="#">dokme</a>--}}
                </div>
                <div class="mr-auto p-2 float-left d-flex">
                    <a href="{{route('Profile.index')}}" class="btn text-white p-0" >
                        <i class="fa fa-user "></i>
                    </a>
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button class="btn text-white p-0 mx-4" type="submit">
                            <i class="fa fa-power-off "></i>
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</nav>
