<div class="col-12 col-lg-2   bg-color2 color4 box-shadow-2 collapse show mr-0 p-0 " id="sidebar">
    <div class="text-center pt-2">
        <a class="text-decoration-none" href="{{route('store')}}">
            <img class="img-fluid" src="{{asset('img/4rak-logo.png')}}">
        </a>
    </div>
    <a href="#" class="nav-link  text-secondary py-2 pr-0  text-bold text-center collapsed mt-3"
       style="font-size: large;background-color:#ffd4c3 " data-toggle="collapse"
       data-target="#user_menu">
        پنل شخصی
        <i class="fa fa-caret-left float-left text-secondary"></i>
    </a>
    <div class="collapse px-2" id="user_menu">
        <ul class="nav flex-column pr-1 pl-0 side mr-0">
            @if(Auth::user()->shop_id != null)
                <li class="nav-item  ">
                    <a class="nav-link " href="{{route('singleshop',Auth::user()->shop_id)}}">
                        <i class="fa fa-store coloryellow"></i>
                        <span class="mr-2 coloryellow">فروشگاه شما </span>
                    </a>
                </li>
            @else
                <li class="nav-item  ">
                    <a class="nav-link " href="{{route('shopInfo')}}">
                        <i class="fa fa-store coloryellow"></i>
                        <span class="mr-2 coloryellow">ثبت فروشگاه</span>
                    </a>
                </li>
            @endif
            @if(Route::CurrentRouteName()=='storage.index')
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('buyHistory')}}">
                        <i class="fa fa-boxes coloryellow"></i>
                        <span class="mr-2 coloryellow">سوابق خرید</span>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('buyHistory')}}">
                        <i class="fa fa-boxes coloryellow"></i>
                        <span class="mr-2 coloryellow">سوابق خرید</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>

    @if(Auth::user()->shop_id!=null)
        <label class="nav-link py-2 pr-0 color4  text-bold bg-secondary collapsed mt-3 text-center"
               style="font-size: large">پنل کاری</label>
    @endif
    <div class="px-2">
        {{--SHOP--}}
        @if(Auth::user()->role->name=='مالک')
            <a href="#" class="nav-link py-2 pr-0 color3  text-bold collapsed" data-toggle="collapse"
               data-target="#shopmenu">
                فروشگاه
                <i class="fa fa-caret-left float-left green-light"></i>
            </a>
            <div class="collapse" id="shopmenu">
                <ul class="nav flex-column pr-1 pl-0 side mr-0">

                    @if(Route::CurrentRouteName()=='shopInfo'|| Route::CurrentRouteName()=='EditShopInfo')
                        <li class="nav-item active ">
                            <a class="nav-link " href="{{route('shopInfo')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">اطلاعات فروشگاه</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link " href="{{route('shopInfo')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">اطلاعات فروشگاه</span>
                            </a>
                        </li>
                    @endif
                    @if(Route::CurrentRouteName()=='gallery')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('gallery')}}">
                                <i class="fa fa-images coloryellow"></i>
                                <span class="mr-2 coloryellow">گالری</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('gallery')}}">
                                <i class="fa fa-images coloryellow"></i>
                                <span class="mr-2 coloryellow">گالری</span>
                            </a>
                        </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fa fa-cog coloryellow"></i>
                            <span class="mr-2 coloryellow">تنظیمات</span>
                        </a>
                    </li>

                </ul>
            </div>
        @endif


        {{--PRODUCT MANAGE--}}
        @if(Auth::user()->shop_id!=null)
            @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && (Auth::user()->shoprole->roles->products==1 || Auth::user()->shoprole->roles->sell==1 || Auth::user()->shoprole->roles->buy==1) || Auth::user()->shoprole->roles->guaranty==1))
                <a href="#" class="nav-link py-2 pr-0  text-bold collapsed color3 " data-toggle="collapse"
                   data-target="#productmenu">
                    مدیریت کالا
                    <i class="fa fa-caret-left float-left green-light"></i>
                </a>
            @endif
            <div class="collapse" id="productmenu">
                <ul class="nav flex-column pr-1 pl-0 side mr-0">
                    @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->products==1))
                        @if(Route::CurrentRouteName()=='category.index' || Route::CurrentRouteName()=='category.create' || Route::CurrentRouteName()=='category.edit')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('category.index')}}">
                                    <i class="fas fa-layer-group coloryellow"></i>
                                    <span class="mr-2 coloryellow">مدیریت گروه</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('category.index')}}">
                                    <i class="fas fa-layer-group coloryellow"></i>
                                    <span class="mr-2 coloryellow">مدیریت گروه</span>
                                </a>
                            </li>
                        @endif

                        @if(Route::CurrentRouteName()=='product.index' || Route::CurrentRouteName()=='product.edit')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('product.index')}}">
                                    <i class="fa fa-box-open coloryellow"></i>
                                    <span class="mr-2 coloryellow">مدیریت کالاها</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('product.index')}}">
                                    <i class="fa fa-box-open coloryellow"></i>
                                    <span class="mr-2 coloryellow">مدیریت کالاها</span>
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->buy==1) )
                        @if(Route::CurrentRouteName()=='product.create')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('product.create')}}">
                                    <i class="fa fa-money-check-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">خرید کالا</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('product.create')}}">
                                    <i class="fa fa-money-check-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">خرید کالا</span>
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(Auth::user()->role->name=='مدیر' ||  (Auth::user()->shoprole && Auth::user()->shoprole->roles->sell==1) )
                        @if(Route::CurrentRouteName()=='sell')
                            <li class="nav-item active">
                                <a class="nav-link " href="{{route('sell')}}">
                                    <i class="fa fa-money-bill-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">فروش کالا</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item ">
                                <a class="nav-link " href="{{route('sell')}}">
                                    <i class="fa fa-money-bill-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">فروش کالا</span>
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->guaranty==1))
                        @if(Route::CurrentRouteName()=='guaranty' || Route::CurrentRouteName()=='guarantyinfo')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('guaranty')}}">
                                    <i class="fa fa-shield-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">گارانتی</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('guaranty')}}">
                                    <i class="fa fa-shield-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">گارانتی</span>
                                </a>
                            </li>
                        @endif
                        @if(Route::CurrentRouteName()=='guarantyTracking')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('guarantyTracking')}}">
                                    <i class="fa fa-shield-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">پیگیری گارانتی</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item ">
                                <a class="nav-link" href="{{route('guarantyTracking')}}">
                                    <i class="fa fa-shield-alt coloryellow"></i>
                                    <span class="mr-2 coloryellow">پیگیری گارانتی</span>
                                </a>
                            </li>
                        @endif
                    @endif

                </ul>
            </div>
        @endif

        {{--ACCOUNTANT--}}
        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->accountant==1))
            <a href="#" class="nav-link py-2 pr-0  text-bold collapsed color3 " data-toggle="collapse"
               data-target="#accmenu">
                حسابداری
                <i class="fa fa-caret-left float-left green-light"></i>
            </a>
            <div class="collapse" id="accmenu">
                <ul class="nav flex-column pr-1 pl-0 side mr-0">
                    @if(Route::CurrentRouteName()=='accounts')
                        <li class="nav-item active ">
                            <a class="nav-link" href="{{route('accounts')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">حسابها</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" href="{{route('accounts')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">حسابها</span>
                            </a>
                        </li>
                    @endif
                    @if(Route::CurrentRouteName()=='accountant')
                        <li class="nav-item active ">
                            <a class="nav-link" href="{{route('accountant')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">حسابداری کل</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" href="{{route('accountant')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">حسابداری کل</span>
                            </a>
                        </li>
                    @endif

                    @if(Route::CurrentRouteName()=='onlineAcc')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('onlineAcc')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">فروش آنلاین</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('onlineAcc')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">فروش آنلاین</span>
                            </a>
                        </li>
                    @endif
                    @if(Route::CurrentRouteName()=='offlineAcc')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('offlineAcc')}}">
                                <i class="fa fa-store-alt coloryellow"></i>
                                <span class="mr-2 coloryellow">فروش آفلاین</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('offlineAcc')}}">
                                <i class="fa fa-store-alt coloryellow"></i>
                                <span class="mr-2 coloryellow">فروش آفلاین</span>
                            </a>
                        </li>
                    @endif


                </ul>
            </div>
        @endif

        {{--STORAGE--}}
        @if(Auth::user()->role->name=='مدیر'  || (Auth::user()->shoprole && Auth::user()->shoprole->roles->storage==1))
            <a href="#" class="nav-link py-2 pr-0  text-bold collapsed color3 " data-toggle="collapse"
               data-target="#strmenu">
                انبار داری
                <i class="fa fa-caret-left float-left green-light"></i>
            </a>
            <div class="collapse" id="strmenu">
                <ul class="nav flex-column pr-1 pl-0 side mr-0">
                    @if(Route::CurrentRouteName()=='storage.index' || Route::CurrentRouteName()=='storage.create' || Route::CurrentRouteName()=='storage.edit')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('storage.index')}}">
                                <i class="fa fa-boxes coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت انبارها</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('storage.index')}}">
                                <i class="fa fa-boxes coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت انبارها</span>
                            </a>
                        </li>
                    @endif

                    @if(Route::CurrentRouteName()=='ioProduct')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('ioProduct')}}">
                                <i class="fa fa-exchange-alt coloryellow"></i>
                                <span class="mr-2 coloryellow">ورود/خروج کالا</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('ioProduct')}}">
                                <i class="fa fa-exchange-alt coloryellow"></i>
                                <span class="mr-2 coloryellow">ورود/خروج کالا</span>
                            </a>
                        </li>
                    @endif

                    @if(Route::CurrentRouteName()=='storageReport')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('storageReport')}}">
                                <i class="fa fa-chart-line coloryellow"></i>
                                <span class="mr-2 coloryellow">گزارش انبار</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('storageReport')}}">
                                <i class="fa fa-chart-line coloryellow"></i>
                                <span class="mr-2 coloryellow">گزارش انبار</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        @endif

        {{--PERSONEL--}}
        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->personnel==1))
            <a href="#" class=" nav-link py-2 pr-0  text-bold collapsed color3 " data-toggle="collapse"
               data-target="#personmenu">
                نیروی انسانی
                <i class="fa fa-caret-left float-left green-light"></i>
            </a>
            <div class="collapse" id="personmenu">
                <ul class="nav flex-column pr-1 pl-0 side mr-0">
                    @if(Route::CurrentRouteName()=='personnel.index'|| Route::CurrentRouteName()=='personnel.create' || Route::CurrentRouteName()=='personnel.edit')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('personnel.index')}}">
                                <i class="fa fa-users-cog coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت کارکنان</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('personnel.index')}}">
                                <i class="fa fa-users-cog coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت کارکنان</span>
                            </a>
                        </li>
                    @endif

                    @if(Route::CurrentRouteName()=='shopRoles')
                        <li class="nav-item active">
                            <a class="nav-link " href="{{route('shopRoles')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت نقش ها</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link " href="{{route('shopRoles')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">مدیریت نقش ها</span>
                            </a>
                        </li>
                    @endif

                    @if(Route::CurrentRouteName()=='userLogs')
                        <li class="nav-item active">
                            <a class="nav-link " href="{{route('userLogs')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">بررسی کارکرد</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link " href="{{route('userLogs')}}">
                                <i class="fa fa-info-circle coloryellow"></i>
                                <span class="mr-2 coloryellow">بررسی کارکرد</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        @endif

        {{--DELIVERY--}}
        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->delivery==1))
            <a href="#" class="nav-link py-2 pr-0  text-bold collapsed color3 " data-toggle="collapse"
               data-target="#deliverymenu">
                حمل و نقل
                <i class="fa fa-caret-left float-left green-light"></i>
            </a>
            <div class="collapse" id="deliverymenu">
                <ul class="nav flex-column side pr-1 pl-0 mr-0">
                    @if(Route::CurrentRouteName()=='deliveryReq')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('deliveryReq')}}">
                                <i class="fa fa-truckcoloryellow"></i>
                                <span class="mr-2 coloryellow">درخواست پیک</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" href="{{route('deliveryReq')}}">
                                <i class="fa fa-truckcoloryellow"></i>
                                <span class="mr-2 coloryellow">درخواست پیک</span>
                            </a>
                        </li>
                    @endif
                    @if(Route::CurrentRouteName()=='delivery.create')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('delivery.create')}}">
                                <i class="fa fa-truckcoloryellow"></i>
                                <span class="mr-2 coloryellow">پیک جدید</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" href="{{route('delivery.create')}}">
                                <i class="fa fa-truckcoloryellow"></i>
                                <span class="mr-2 coloryellow">پیک جدید</span>
                            </a>
                        </li>
                    @endif
                    @if(Route::CurrentRouteName()=='onlineOrders'|| Route::CurrentRouteName()=='onlineOrdersInfo')
                        <li class="nav-item active ">
                            <a class="nav-link" href="{{route('onlineOrders')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">سفارشات آنلاین</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" href="{{route('onlineOrders')}}">
                                <i class="fa fa-globe coloryellow"></i>
                                <span class="mr-2 coloryellow">سفارشات آنلاین</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        @endif
    </div>

</div>
