<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    @yield('header')
    <title>چارک | پلتفرم جامع تجارت</title>

    <!--Morris Chart CSS -->
    {{--    <link rel="stylesheet" href="{{asset('newTheme/assets/plugins/morris/morris.css')}}">--}}

<!-- App css -->
    <link href="{{asset('css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('newTheme/assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone/basic.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('datepicker/dist/css/persian-datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/fontiran.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <link href="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet"
          type="text/css"/>

    {{--    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">--}}
    <link href="{{asset('css/filepond.css')}}" rel="stylesheet">

{{--    <link rel="stylesheet" href="{{asset('css/datepick/bootstrap-theme.min.css')}}" />--}}
{{--    <link rel="stylesheet" href="{{asset('css/datepick/jquery.Bootstrap-PersianDateTimePicker.css')}}" />--}}

    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>


    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('newTheme/assets/js/modernizr.min.js')}}"></script>
    <livewire:styles/>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{route('store')}}" class="logo ">
                <img src="{{asset('img/4rak-logo.png')}}" class="img-fluid my-2" width="125px" alt="چارک">
            </a>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <!-- Page title -->
                <ul class="nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">داشبورد</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
                <ul class="nav  navbar-right">
                    <li>
                        <!-- Notification -->
                        <!--                        <div class="notification-box">
                                                    <ul class="list-inline m-b-0">
                                                        <li>
                                                            <a href="javascript:void(0)" class="px-2 right-bar-toggle">
                                                                <i class="zmdi zmdi-notifications-none"></i>
                                                            </a>
                                                            <div class="noti-dot">
                                                                <span class="dot"></span>
                                                                <span class="pulse"></span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>-->
                        <!-- End Notification bar -->
                    </li>
                    <!--                    <li class="hidden-xs">
                                            <form role="search" class="app-search">
                                                <input type="text" placeholder="به دنبال چه می گردی ؟؟؟"
                                                       class="form-control">
                                                <a href=""><i class="fa fa-search"></i></a>
                                            </form>
                                        </li>-->
                </ul>

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Right Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                <div class="user-img">
                    @if(Auth::user()->photo)
                        <img src="{{asset('images/profile/'.Auth::user()->photo->path)}}" alt="user-img"
                             title="Mat Helme" width="100px" height="100px"
                             class="img-circle img-thumbnail img-responsive rounded-circle"
                             style="border: none;max-height: 100px;max-width: 100px">
                    @else
                        <img src="{{asset('img/user-avatar.svg')}}" alt="user-img" title="Mat Helme"
                             class="img-circle img-thumbnail img-responsive" style="border: none">
                    @endif
                    <div class="user-status offline "><i class="zmdi zmdi-dot-circle statuss text-success"></i></div>
                </div>
                <h5><a href="{{route('Profile.index')}}">{{Auth::user()->name}} {{Auth::user()->family}}</a></h5>
                <ul class="list-unstyled d-inline-flex ">
                    <li class="mx-2">
                        <a class="text-secondary" href="{{route('Profile.index')}}">
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    <li class="mx-2">
                        <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <button class="btn p-0 text-secondary" type="submit">
                                <i class="fa fa-power-off "></i>
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">

                <!-- Admin panel -->
                @if(Auth::user()->role_id==1)
                    <ul>
                        <li class="has_sub">
                            <a id="menItemadmin" href="javascript:void(0);" class="waves-effect">
                                <i class="fa fa-user-cog"></i>
                                <span> پنل ادمین </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="list-unstyled">
                                <li class="nav-item ">
                                    <a class="nav-link " href="{{route('config')}}">
                                        <i class="fa fa-cog  mx-0"></i>
                                        <span class=" ">راه اندازی اولیه</span>
                                    </a>
                                </li>
                                @if(Route::currentRouteName()=='main-shop-setting')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('main-shop-setting')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">تنظیمات فروشگاه اصلی</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('main-shop-setting')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">تنظیمات فروشگاه اصلی</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='blog-setting')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('blog-setting')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">تنظیمات وبلاگ</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('blog-setting')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">تنظیمات وبلاگ</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='plans'||Route::currentRouteName()=='new_pay_plan'||Route::currentRouteName()=='edit_pay_plan')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('plans')}}">
                                            <i class="fa fa-cog mx-0 "></i>
                                            <span class=" ">تنظیمات فروش و پلن</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('plans')}}">
                                            <i class="fa fa-cog mx-0 "></i>
                                            <span class=" ">تنظیمات فروش و پلن</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='delivery_methods'||Route::currentRouteName()=='new_delivery_method'||Route::currentRouteName()=='edit_delivery_method')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('delivery_methods')}}">
                                            <i class="fa fa-cog mx-0 "></i>
                                            <span class=" ">بخش ارسال مرسولات پستی</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('delivery_methods')}}">
                                            <i class="fa fa-cog mx-0 "></i>
                                            <span class=" ">بخش ارسال مرسولات پستی</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='checkoutReq')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('checkoutReq')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">درخواست های تسویه</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('checkoutReq')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">درخواست های تسویه</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='guilds-setting'||Route::currentRouteName()=='add-guild'||Route::currentRouteName()=='edit_guild'
                                        ||Route::currentRouteName()=='sub_guilds'||Route::currentRouteName()=='add-subguild'||Route::currentRouteName()=='edit-subguild')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('guilds-setting')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">تنظیمات صنف و رسته</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('guilds-setting')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">تنظیمات صنف و رسته</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='all-comments')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('all-comments')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">نظرات</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('all-comments')}}">
                                            <i class="fa fa-cog mx-0"></i>
                                            <span class=" ">نظرات</span>
                                        </a>
                                    </li>
                                @endif
                                @if(Route::currentRouteName()=='pro-categories' || Route::currentRouteName()=='add-pro-category' ||Route::currentRouteName()=='edit-pro-category')
                                    <li class="nav-item active">
                                        <a class="nav-link " href="{{route('pro-categories')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">مدیریت گروه محصول</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{route('pro-categories')}}">
                                            <i class="fa fa-cog  mx-0"></i>
                                            <span class=" ">مدیریت گروه محصول</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                @endif
            <!-- End Admin panel -->


                <!-- user panel -->
                <ul>
                    <li class="has_sub">
                        <a id="menItem" href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-alt"></i>
                            <span> پنل شخصی </span> <span class="menu-arrow"></span>
                        </a>
                        <ul class="list-unstyled">
                            @if(Route::currentRouteName()=='Profile.index'||Route::currentRouteName()=='Profile.edit')
                                <li class="nav-item active ">
                                    <a class="nav-link " href="{{route('Profile.index')}}">
                                        <i class="fa fa-user "></i>
                                        <span class="mr-2 ">پروفایل</span>
                                    </a>
                                </li>
                            @else
                                <li class="nav-item  ">
                                    <a class="nav-link " href="{{route('Profile.index')}}">
                                        <i class="fa fa-user "></i>
                                        <span class="mr-2 ">پروفایل</span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->shop_id == null)
                                @if(Route::CurrentRouteName()=='shopInfo')
                                    <li class="nav-item active  ">
                                        <a class="nav-link " href="{{route('shopInfo')}}">
                                            <i class="fa fa-store "></i>
                                            <span class="mr-2 ">ثبت فروشگاه</span>
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item  ">
                                        <a class="nav-link " href="{{route('shopInfo')}}">
                                            <i class="fa fa-store "></i>
                                            <span class="mr-2 ">ثبت فروشگاه</span>
                                        </a>
                                    </li>
                                @endif
                            @endif
                            @if(Route::CurrentRouteName()=='buyHistory')
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('buyHistory')}}">
                                        <i class="fa fa-boxes "></i>
                                        <span class="mr-2 ">سوابق خرید</span>
                                    </a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('buyHistory')}}">
                                        <i class="fa fa-boxes "></i>
                                        <span class="mr-2 ">سوابق خرید</span>
                                    </a>
                                </li>
                            @endif
                        </ul>

                    </li>
                </ul>
                <!-- end User panel -->


                <!-- shop panel -->
                @if(Auth::user()->shop_id!=null)
                    <ul>
                        <li class="text-muted menu-title">پنل کاری</li>
                        @if(Route::currentRouteName()=='dashboard')
                            <li>
                                <a href="{{route('dashboard')}}" class="waves-effect active">
                                    <i class="zmdi zmdi-view-dashboard"></i> <span> داشبورد </span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{route('dashboard')}}" class="waves-effect ">
                                    <i class="zmdi zmdi-view-dashboard"></i> <span> داشبورد </span>
                                </a>
                            </li>
                        @endif

                        @if(Auth::user()->role->name=='مالک' || Auth::user()->role->name=='ادمین'|| Auth::user()->role->name=='مدیر')
                            <li class="has_sub">
                                <a id="menItemBlog" href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-book"></i>
                                    <span> مدیریت مجله </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='article-category'||Route::currentRouteName()=='article-category-edit'||Route::currentRouteName()=='article-category-new')
                                        <li class="active"><a href="{{route('article-category')}}">مدیریت گروه مطالب</a>
                                        </li>
                                    @else
                                        <li><a href="{{route('article-category')}}">مدیریت گروه مطالب</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='article.index'||Route::currentRouteName()=='article.edit'||Route::currentRouteName()=='article.create')
                                        <li class="active"><a href="{{route('article.index')}}">مدیریت مطالب</a></li>
                                    @else
                                        <li><a href="{{route('article.index')}}">مدیریت مطالب</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='article-comments'||Route::currentRouteName()=='comment.edit')
                                        <li class="active"><a href="{{route('article-comments')}}">مدیریت نظرات</a></li>
                                    @else
                                        <li><a href="{{route('article-comments')}}">مدیریت نظرات</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif


                        @if(Auth::user()->shop_id!=null)
                            <li class="has_sub">
                                @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && (Auth::user()->shoprole->roles->products==1 || Auth::user()->shoprole->roles->sell==1 || Auth::user()->shoprole->roles->buy==1) || Auth::user()->shoprole->roles->guaranty==1))
                                    <a id="menItemService" href="javascript:void(0);" class="waves-effect">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span> مدیریت کالا و خدمات </span>
                                        <span class="menu-arrow"></span>
                                    </a>
                                @endif
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='productDashboard')
                                        <li class="active"><a href="{{route('productDashboard')}}"> داشبورد</a></li>
                                    @else
                                        <li><a href="{{route('productDashboard')}}"> داشبورد</a></li>
                                    @endif
                                    @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->products==1))
                                        @if(Route::currentRouteName()=='category.index'||Route::currentRouteName()=='category.edit'||Route::currentRouteName()=='category.create')
                                            <li class="active">
                                                <a href="{{route('category.index')}}">مدیریت گروه کالا</a>
                                            </li>
                                        @else
                                            <li><a href="{{route('category.index')}}">مدیریت گروه کالا</a></li>
                                        @endif
                                        @if(Route::currentRouteName()=='product.index'||Route::currentRouteName()=='product.edit')
                                            <li class="active"><a href="{{route('product.index')}}">مدیریت کالا</a></li>
                                        @else
                                            <li><a href="{{route('product.index')}}">مدیریت کالا</a></li>
                                        @endif
                                            @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->buy==1) )
                                                @if(Route::currentRouteName()=='product.create')
                                                    <li class="active"><a href="{{route('product.create')}}">خرید کالا</a></li>
                                                @else
                                                    <li><a href="{{route('product.create')}}">خرید کالا</a></li>
                                                @endif
                                                @if(Route::currentRouteName()=='newProduct')
                                                    <li class="active"><a href="{{route('newProduct')}}">افزودن کالا</a></li>
                                                @else
                                                    <li><a href="{{route('newProduct')}}">افزودن کالا</a></li>
                                                @endif
                                            @endif
                                            @if(Auth::user()->role->name=='مدیر' ||  (Auth::user()->shoprole && Auth::user()->shoprole->roles->sell==1) )
                                                @if(Route::currentRouteName()=='sell')
                                                    <li class="active"><a href="{{route('sell')}}">فروش کالا</a></li>
                                                @else
                                                    <li><a href="{{route('sell')}}">فروش کالا</a></li>
                                                @endif
                                            @endif

                                            @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->products==1))
                                                @if(Route::currentRouteName()=='service.index'||Route::currentRouteName()=='service.create'||Route::currentRouteName()=='service.edit')
                                                    <li class="active"><a href="{{route('service.index')}}">مدیریت خدمات</a>
                                                    </li>
                                                @else
                                                    <li><a href="{{route('service.index')}}">مدیریت خدمات</a></li>
                                                @endif
                                            @endif
                                    @endif


                                    @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->guaranty==1))
                                        @if(Route::currentRouteName()=='guaranty')
                                            <li class="active"><a href="{{route('guaranty')}}">گارانتی</a></li>
                                        @else
                                            <li><a href="{{route('guaranty')}}">گارانتی</a></li>
                                        @endif
                                        @if(Route::currentRouteName()=='guarantyTracking'||Route::currentRouteName()=='guarantyinfo')
                                            <li class="active"><a href="{{route('guarantyTracking')}}">پیگیری
                                                    گارانتی</a></li>
                                        @else
                                            <li><a href="{{route('guarantyTracking')}}">پیگیری گارانتی</a></li>
                                        @endif
                                    @endif
                                        @if(Auth::user()->role->name=='مدیر' ||Auth::user()->role->name=='مالک' )
                                            @if(Route::currentRouteName()=='product-comments')
                                                <li class="active"><a href="{{route('product-comments')}}">مدیریت نظرات</a>
                                                </li>
                                            @else
                                                <li><a href="{{route('product-comments')}}">مدیریت نظرات</a></li>
                                            @endif
                                        @endif
                                </ul>
                            </li>
                        @endif


                        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->accountant==1))
                            <li class="has_sub">
                                <a id="menItemAccountant" href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-dollar"></i>
                                    <span> حسابداری </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='accountantDashboard')
                                        <li class="active"><a href="{{route('accountantDashboard')}}"> داشبورد</a></li>
                                    @else
                                        <li><a href="{{route('accountantDashboard')}}"> داشبورد</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='accounts'||Route::currentRouteName()=='accounts.create'||Route::currentRouteName()=='accounts.edit')
                                        <li class="active"><a href="{{route('accounts')}}">حسابها</a></li>
                                    @else
                                        <li><a href="{{route('accounts')}}">حسابها</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='accountant')
                                        <li class="active"><a href="{{route('accountant')}}">حسابداری کل</a></li>
                                    @else
                                        <li><a href="{{route('accountant')}}">حسابداری کل</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='onlineAcc')
                                        <li class="active"><a href="{{route('onlineAcc')}}">کارکرد آنلاین</a></li>
                                    @else
                                        <li><a href="{{route('onlineAcc')}}">کارکرد آنلاین</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='offlineAcc'||Route::currentRouteName()=='newOutgo')
                                        <li class="active"><a href="{{route('offlineAcc')}}">کارکرد آفلاین</a></li>
                                    @else
                                        <li><a href="{{route('offlineAcc')}}">کارکرد آفلاین</a></li>
                                    @endif
                                        @if(Route::currentRouteName()=='unpaidFactors')
                                            <li class="active"><a href="{{route('unpaidFactors')}}"> فاکتورهای پرداخت نشده</a></li>
                                        @else
                                            <li><a href="{{route('unpaidFactors')}}">فاکتورهای پرداخت نشده</a></li>
                                        @endif
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->name=='مدیر'  || (Auth::user()->shoprole && Auth::user()->shoprole->roles->storage==1))
                            <li class="has_sub">
                                <a id="menItemStorage" href="javascript:void(0);" class="waves-effect ">
                                    <i class="fa fa-database"></i>
                                    <span> انبار داری </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='storageDashboard')
                                        <li class="active"><a href="{{route('storageDashboard')}}">داشبورد</a></li>
                                    @else
                                        <li><a href="{{route('storageDashboard')}}">داشبورد</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='storage.index'||Route::currentRouteName()=='storage.create'||Route::currentRouteName()=='storage.edit')
                                        <li class="active"><a href="{{route('storage.index')}}">مدیریت انبار</a></li>
                                    @else
                                        <li><a href="{{route('storage.index')}}">مدیریت انبار</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='ioProduct')
                                        <li class="active"><a href="{{route('ioProduct')}}">ورود/خروج کالا</a></li>
                                    @else
                                        <li><a href="{{route('ioProduct')}}">ورود/خروج کالا</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='storageReport')
                                        <li class="active"><a href="{{route('storageReport')}}">گزارش انبار</a></li>
                                    @else
                                        <li><a href="{{route('storageReport')}}">گزارش انبار</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->personnel==1))
                            <li class="has_sub">
                                <a id="menItemPersonnel" href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-users"></i>
                                    <span> نیروی انسانی </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='personnelDashboard')
                                        <li class="active"><a href="{{route('personnelDashboard')}}">داشبورد</a></li>
                                    @else
                                        <li><a href="{{route('personnelDashboard')}}">داشبورد</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='personnel.index'||Route::currentRouteName()=='personnel.create'||Route::currentRouteName()=='personnel.edit')
                                        <li class="active"><a href="{{route('personnel.index')}}">مدیریت کارکنان</a>
                                        </li>
                                    @else
                                        <li><a href="{{route('personnel.index')}}">مدیریت کارکنان</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='shopRoles'||Route::currentRouteName()=='createRole'||Route::currentRouteName()=='editRoles')
                                        <li class="active"><a href="{{route('shopRoles')}}">مدیریت نقش ها</a></li>
                                    @else
                                        <li><a href="{{route('shopRoles')}}">مدیریت نقش ها</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='userLogs')
                                        <li class="active"><a href="{{route('userLogs')}}">بررسی کارکرد</a></li>
                                    @else
                                        <li><a href="{{route('userLogs')}}">بررسی کارکرد</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->name=='مدیر' || (Auth::user()->shoprole && Auth::user()->shoprole->roles->delivery==1))
                            <li class="has_sub">
                                <a id="menItemDelivery" href="javascript:void(0);" class="waves-effect ">
                                    <i class="fa fa-truck"></i>
                                    <span>حمل و نقل </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    @if(Route::currentRouteName()=='deliveryDashboard')
                                        <li class="active"><a href="{{route('deliveryDashboard')}}">داشبورد</a></li>
                                    @else
                                        <li><a href="{{route('deliveryDashboard')}}">داشبورد</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='deliveryReq')
                                        <li class="active"><a href="{{route('deliveryReq')}}">درخواست پیک</a></li>
                                    @else
                                        <li><a href="{{route('deliveryReq')}}">درخواست پیک</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='delivery.create')
                                        <li class="active"><a href="{{route('delivery.create')}}">پیک جدید</a></li>
                                    @else
                                        <li><a href="{{route('delivery.create')}}">پیک جدید</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='onlineOrders')
                                        <li class="active"><a href="{{route('onlineOrders')}}">سفارشات آنلاین</a></li>
                                    @else
                                        <li><a href="{{route('onlineOrders')}}">سفارشات آنلاین</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->role->name=='مالک' || Auth::user()->role->name=='ادمین'|| Auth::user()->role->name=='مدیر')
                            <li class="has_sub">
                                <a id="menItemShop" href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-store"></i>
                                    <span> فروشگاه </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="list-unstyled">
                                    <li class="nav-item ">
                                        <a class="nav-link " target="_blank" href="{{route('singleshop',Auth::user()->shop->unique_name)}}">
                                            <span class="mr-2 ">بازدید از فروشگاه {{Auth::user()->shop->name}}</span>
                                        </a>
                                    </li>
                                    @if(Route::currentRouteName()=='shopInfo'||Route::currentRouteName()=='EditShopInfo')
                                        <li class=" active"><a class="" href="{{route('shopInfo')}}">اطلاعات فروشگاه</a>
                                        </li>
                                    @else
                                        <li><a href="{{route('shopInfo')}}">اطلاعات فروشگاه</a></li>
                                    @endif
                                    @if(Route::currentRouteName()=='gallery')
                                        <li class="active"><a href="{{route('gallery')}}">گالری</a></li>
                                    @else
                                        <li><a href="{{route('gallery')}}">گالری</a></li>
                                    @endif
                                    {{--                                    <li><a href="#">تنظیمات</a></li>--}}
                                </ul>
                            </li>
                        @endif


                    </ul>
            @endif

            <!-- end shop panel -->


                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Right Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12 ">
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                @yield('main')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->

    </div>
    <!-- content -->

    <footer class="footer">
        حرانت ©
    </footer>

</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Left Sidebar -->
<!--<div class="side-bar right-bar">
    <a href="javascript:void(0);" class="right-bar-toggle">
        <i class="zmdi zmdi-close-circle-o"></i>
    </a>
    <h4 class="">اعلانات</h4>
    <div class="notification-list nicescroll">
        <ul class="list-group list-no-border user-list">
            <li class="list-group-item">
                <a href="#" class="user-list-item">
                    <div class="avatar">
                        <img src="" alt="">
                    </div>
                    <div class="user-desc">
                        <span class="name">کاربر شماره یک</span>
                        <span class="desc">متن کاربر شماره یک</span>
                        <span class="time">2 ساعت قبل</span>
                    </div>
                </a>
            </li>
            <li class="list-group-item">
                <a href="#" class="user-list-item">
                    <div class="icon bg-info">
                        <i class="zmdi zmdi-account"></i>
                    </div>
                    <div class="user-desc">
                        <span class="name">ثبت نام جدید</span>
                        <span class="desc">کاربری جدید در سایت ثبت نام کرده است</span>
                        <span class="time">5 ساعت قبل</span>
                    </div>
                </a>
            </li>
            <li class="list-group-item">
                <a href="#" class="user-list-item">
                    <div class="icon bg-pink">
                        <i class="zmdi zmdi-comment"></i>
                    </div>
                    <div class="user-desc">
                        <span class="name">پیام جدید</span>
                        <span class="desc">متن پیام جدید از کاریی جدید</span>
                        <span class="time">1 روز قبل</span>
                    </div>
                </a>
            </li>
            <li class="list-group-item active">
                <a href="#" class="user-list-item">
                    <div class="avatar">
                        <img src="" alt="">
                    </div>
                    <div class="user-desc">
                        <span class="name">کاربر شماره 2</span>
                        <span class="desc">با سلام من یک متن کاملا آزمایشی هستم</span>
                        <span class="time">2 روز قبل</span>
                    </div>
                </a>
            </li>
            <li class="list-group-item active">
                <a href="#" class="user-list-item">
                    <div class="icon bg-warning">
                        <i class="zmdi zmdi-settings"></i>
                    </div>
                    <div class="user-desc">
                        <span class="name">تنظیمات</span>
                        <span class="desc">تنظیمات جدید برای دسترسی و راحتی شما موجود است</span>
                        <span class="time">1 روز قبل</span>
                    </div>
                </a>
            </li>

        </ul>
    </div>
</div>-->

<!-- /Left-bar -->

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('newTheme/assets/js/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap-rtl.min.js')}}"></script>
<script src="{{asset('newTheme/assets/js/detect.js')}}"></script>
<script src="{{asset('newTheme/assets/js/fastclick.js')}}"></script>
<script src="{{asset('newTheme/assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('newTheme/assets/js/waves.js')}}"></script>
<script src="{{asset('newTheme/assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('newTheme/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('newTheme/assets/js/jquery.scrollTo.min.js')}}"></script>
{{--<script src="js/bootstrap.min.js" type="text/javascript"></script>--}}

{{--<script src="{{asset('js/datepick/jalaali.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('js/datepick/jquery.Bootstrap-PersianDateTimePicker.js')}}" type="text/javascript"></script>--}}


<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="newTheme/assets/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="{{asset('newTheme/assets/plugins/jquery-knob/jquery.knob.js')}}"></script>

<!--Morris Chart-->
{{--<script src="{{asset('newTheme/assets/plugins/morris/morris.min.js')}}"></script>--}}
{{--<script src="{{asset('newTheme/assets/plugins/raphael/raphael-min.js')}}"></script>--}}

<!-- Dashboard init -->
{{--<script src="{{asset('newTheme/assets/pages/jquery.dashboard.js')}}"></script>--}}

<!-- App js -->
<script src="{{asset('newTheme/assets/js/jquery.core.js/')}}"></script>
<script src="{{asset('newTheme/assets/js/jquery.app.js/')}}"></script>

<script src="{{asset('js/myJS.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="{{asset('js/Chart.min.js')}}"></script>
<script src="{{asset('js/my-charts.js')}}"></script>
<script src="{{asset('js/dropzone/dropzone.js')}}"></script>
<script src="{{asset('js/dropzone/dropzone-amd-module.js')}}"></script>
<script src="{{asset('datepicker/assets/persian-date.min.js')}}"></script>
<script src="{{asset('datepicker/dist/js/persian-datepicker.js')}}"></script>
<!-- Sweet Alert js -->
<script src="{{asset('newTheme/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
<script src="{{asset('newTheme/assets/pages/jquery.sweet-alert.init.js')}}"></script>
<!-- Sweet Alert js -->

{{--<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>--}}
<script src="{{asset('js/filepond.min.js')}}"></script>
<!-- include FilePond plugins -->
{{--<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>--}}
<script src="{{asset('js/filepond-plugin-image-preview.min.js')}}"></script>
<!-- include FilePond jQuery adapter -->
<script>
    window.addEventListener("load", () => {
        hasNetwork(navigator.onLine);
        window.addEventListener("online", () => {
            // Set hasNetwork to online when they change to online.
            hasNetwork(true);
        });
        window.addEventListener("offline", () => {
            // Set hasNetwork to offline when they change to offline.
            hasNetwork(false);
        });
    });

    function hasNetwork(online) {
        const element = document.querySelector(".statuss");
        // Update the DOM to reflect the current status
        if (online) {
            element.classList.add("text-success");
        } else {
            element.classList.remove("text-success");
        }
    }
</script>

@yield('filepond')
@yield('script')
@if(Route::currentRouteName()=='Profile.index' ||Route::currentRouteName()=='Profile.edit' ||Route::currentRouteName()=='buyHistory')
    <script>
        $("#menItem").trigger("click");
    </script>
@endif
@if(Auth::user()->shop_id == null && Route::currentRouteName()=='shopInfo' )
    <script>
        $("#menItem").trigger("click");
    </script>
@elseif(Route::currentRouteName()=='shopInfo'||Route::currentRouteName()=='EditShopInfo'||Route::currentRouteName()=='gallery')
    <script>
        $("#menItemShop").trigger("click");
    </script>
@endif
@if(Route::currentRouteName()=='main-shop-setting'|| Route::currentRouteName()=='all-comments'||Route::currentRouteName()=='blog-setting' ||Route::currentRouteName()=='plans'||Route::currentRouteName()=='delivery_methods'||Route::currentRouteName()=='edit_delivery_method'
||Route::currentRouteName()=='checkoutReq'||Route::currentRouteName()=='guilds-setting'||Route::currentRouteName()=='new_pay_plan' ||Route::currentRouteName()=='edit_pay_plan'||Route::currentRouteName()=='new_delivery_method'
||Route::currentRouteName()=='add-guild'||Route::currentRouteName()=='edit_guild'||Route::currentRouteName()=='sub_guilds'||Route::currentRouteName()=='add-subguild'||Route::currentRouteName()=='edit-subguild' ||Route::currentRouteName()=='pro-categories'||Route::currentRouteName()=='add-pro-category'||Route::currentRouteName()=='edit-pro-category')
    <script>
        $("#menItemadmin").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='article.index'||Route::currentRouteName()=='article-comments' ||Route::currentRouteName()=='article.edit' ||Route::currentRouteName()=='article.create'||Route::currentRouteName()=='article-category'||Route::currentRouteName()=='article-category-edit'||Route::currentRouteName()=='article-category-new')
    <script>
        $("#menItemBlog").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='productDashboard' ||Route::currentRouteName()=='category.index' ||Route::currentRouteName()=='category.edit'||Route::currentRouteName()=='category.create'||Route::currentRouteName()=='product.index'
||Route::currentRouteName()=='product.edit'||Route::currentRouteName()=='product.create'||Route::currentRouteName()=='factorPurchase' ||Route::currentRouteName()=='savePurchase'||Route::currentRouteName()=='savePayment'
||Route::currentRouteName()=='sell'||Route::currentRouteName()=='service.index'||Route::currentRouteName()=='service.create'||Route::currentRouteName()=='service.edit'||Route::currentRouteName()=='guaranty'
||Route::currentRouteName()=='guarantyTracking'||Route::currentRouteName()=='guarantyinfo'||Route::currentRouteName()=='editbuyFactor' ||Route::currentRouteName()=='editsellFactor'||Route::currentRouteName()=='editFactorProduct'
||Route::currentRouteName()=='sellFactor'||Route::currentRouteName()=='product-comments'||Route::currentRouteName()=='newProduct')
    <script>
        $("#menItemService").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='accountant' ||Route::currentRouteName()=='accountantDashboard' ||Route::currentRouteName()=='offlineAcc'||Route::currentRouteName()=='onlineAcc'||Route::currentRouteName()=='accounts'
||Route::currentRouteName()=='accounts.create'||Route::currentRouteName()=='accounts.edit'||Route::currentRouteName()=='newOutgo'||Route::currentRouteName()=='unpaidFactors')
    <script>
        $("#menItemAccountant").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='storageDashboard' ||Route::currentRouteName()=='ioProduct' ||Route::currentRouteName()=='storageReport'||Route::currentRouteName()=='storage.index'||Route::currentRouteName()=='storage.create'||Route::currentRouteName()=='storage.edit')
    <script>
        $("#menItemStorage").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='personnelDashboard' ||Route::currentRouteName()=='userLogs' ||Route::currentRouteName()=='personnel.index'||Route::currentRouteName()=='personnel.create'||Route::currentRouteName()=='personnel.edit'
||Route::currentRouteName()=='shopRoles'||Route::currentRouteName()=='createRole'||Route::currentRouteName()=='editRoles')
    <script>
        $("#menItemPersonnel").trigger("click");
    </script>
@endif

@if(Route::currentRouteName()=='deliveryDashboard' ||Route::currentRouteName()=='deliveryReq' ||Route::currentRouteName()=='delivery.index'||Route::currentRouteName()=='delivery.create'||Route::currentRouteName()=='delivery.edit'
||Route::currentRouteName()=='onlineOrders'||Route::currentRouteName()=='onlineOrdersInfo')
    <script>
        $("#menItemDelivery").trigger("click");
    </script>
@endif

<livewire:scripts/>

</body>
</html>
