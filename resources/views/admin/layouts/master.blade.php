<!DOCTYPE html>
<html lang="en" class="bg-color3">
<head>
    <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
    <meta charset="UTF-8">

    @yield('header')
    <title>پنل مدیریت</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/uikit-rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/uikit.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/fontiran.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone/basic.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('datepicker/dist/css/persian-datepicker.css')}}"/>
    <link rel="stylesheet" href="{{asset('js/jQuery-fullTable/jquery.fulltable.css')}}">

    <script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>


    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>

{{--    <script src="{{asset('css/Chart.min.css')}}"></script>--}}
</head>
<body class="bg-color3">

<div class="container-fluid">

    <!--CONTENT-->
    <div class="row">

        <!--SIDEBAR-->
    @include('admin.layouts.sidebar')
    <!--END SIDEBAR-->

        <div class="col-12 col-lg-10 mr-lg-auto mb-5 p-0" id="main">
            <!--NAVBAR-->
        @include('admin.layouts.header')
        <!--END NAVBAR-->


            <!--MAIN-->
            <div class="container mt-5 bg-color4 rounded main-shadow">
                @yield('main')
            </div>
            <!--END MAIN-->


        <!--FOOTER-->

        @include('admin.layouts.footer')
        <!--END FOOTER-->

        </div>

    </div>
    <!--END CONTENT-->




</div>


{{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}

<script src="{{asset('js/jQuery-fullTable/jquery.fulltable.js')}}"></script>
{{--<script src="{{asset('js/filepond.min.js')}}"></script>--}}
{{--<script src="{{asset('js/filepond.jquery.js')}}"></script>--}}
{{--<script src="{{asset('js/filepond.js')}}"></script>--}}
<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
{{--<script src="{{asset('js/jquery.form.js')}}"></script>--}}
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/myJS.js')}}"></script>
<script src="{{asset('js/Chart.min.js')}}"></script>
<script src="{{asset('js/my-charts.js')}}"></script>
<script src="{{asset('js/dropzone/dropzone.js')}}"></script>
<script src="{{asset('js/dropzone/dropzone-amd-module.js')}}"></script>
<script src="{{asset('datepicker/assets/persian-date.min.js')}}"></script>
<script src="{{asset('datepicker/dist/js/persian-datepicker.js')}}"></script>

<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
<!-- include FilePond plugins -->
<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
<!-- include FilePond jQuery adapter -->
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>

@yield('filepond')
@yield('script')
</body>
</html>
