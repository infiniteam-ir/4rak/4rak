<div>
    <form wire:submit.prevent="update_plan">
        <div class="row p-4 d-flex pb-5">
            <div class="row w-100 mt-3 justify-content-center align-items-center ">
                <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                    <div class="col-12 mt-3">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان:</h6>
                        <input type="text" name="name" class="form-control" wire:model.lazy="name">
                        @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> هزینه ماهانه:</h6>
                        <input type="text" name="monthly" class="form-control" wire:model.lazy="monthly">
                        @error('monthly') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> هزینه سه ماه:</h6>
                        <input type="text" name="three_month" class="form-control" wire:model.lazy="three_month">
                        @error('three_month') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> هزینه شش ماه:</h6>
                        <input type="text" name="six_month" class="form-control" wire:model.lazy="six_month">
                        @error('six_month') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> هزینه سالانه:</h6>
                        <input type="text" name="yearly" class="form-control" wire:model.lazy="yearly">
                        @error('yearly') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"> <sup class="text-danger">*</sup> درصد کارمزد:</h6>
                        <input type="text" name="wage" class="form-control" wire:model.lazy="wage">
                        @error('wage') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>

                    <div class="col-12 mt-5">
                        {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success my-2 w-50']) !!}
                        <a class="btn btn-danger my-2 w-50 " href="{{route('plans')}}">انصراف</a>
                    </div>
                </div>

                <div class="col-12 col-md-5 p-5">
                    <img src="{{asset('img/financial.svg')}}" class="img-fluid" alt="">
                </div>
            </div>

        </div>
    </form>
</div>

