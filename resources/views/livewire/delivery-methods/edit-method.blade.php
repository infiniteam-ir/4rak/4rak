<div>
    <form wire:submit.prevent="edit_method">
        <div class="row p-4 d-flex pb-5">
            <div class="row w-100 mt-3 justify-content-center align-items-center ">
                <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                    <div class="col-12 mt-3">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان:</h6>
                        <input type="text" name="title" class="form-control" wire:model.lazy="title">
                        @error('title') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> هزینه ارسال:</h6>
                        <input type="text" name="price" class="form-control" wire:model.lazy="price">
                        @error('price') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> توضیحات:</h6>
                        <input type="text" name="desc" class="form-control" wire:model.lazy="desc">
                        @error('desc') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted">تصویر:</h6>
                        <input type="file" name="photo" class="form-control" wire:model.lazy="photo">
                        @error('photo') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    <div class="col-12 mt-5">
                        {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success my-2 w-50']) !!}
                        <a class="btn btn-danger my-2 w-50 " href="{{route('delivery_methods')}}">انصراف</a>
                    </div>
                </div>


                <div class="col-12 col-md-5 p-5">
                    <img src="{{asset('img/food-delivery.svg')}}" class="img-fluid" alt="">
                </div>
            </div>

        </div>
    </form>
</div>

