<div>
{{--    {!! Form::model($product,['method'=>'PATCH','action'=>['AdminProductController@update',$product->id],'files'=>true]) !!}--}}
    <form wire:submit.prevent="update_product">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">نام:</h6>
                {!! Form::text('title',$product->name,['class'=>'form-control','wire:model.lazy="title"']) !!}
                @error('title') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted"> گروه کالا:</h6>
                <select name="cat" class="form-control" wire:model.lazy="cat">
                    <option value="{{$product->cat_id}}">{{$product->category->title}}</option>
                    @foreach($cats as $cat)
                        <option value={{$cat->id}}>
                            {{$cat->title}}
                        </option>
                    @endforeach
                </select>
                @error('cat') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">کلیدواژه ها:</h6>
                {!! Form::text('keyword',$product->meta_keywords,['class'=>'form-control','wire:model.lazy="keyword"']) !!}
                @error('keyword') <span class="text-danger">{{$message}}</span>@enderror
            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted"> تعداد:</h6>
                {!! Form::text('qty',$product->quantity,['class'=>'form-control','wire:model.lazy="qty"']) !!}
                @error('qty') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت خرید(تومان):</h6>
                {!! Form::text('buy_price',$product->buy_price,['class'=>'form-control','wire:model.lazy="buy_price"']) !!}
                @error('buy_price') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت فروش(تومان):</h6>
                {!! Form::text('sell_price',$product->sell_price,['class'=>'form-control','wire:model.lazy="sell_price"']) !!}
                @error('sell_price') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت عمده(تومان):</h6>
                {!! Form::text('whole_price',$product->whole_price,['class'=>'form-control','wire:model.lazy="whole_price"']) !!}
                @error('whole_price') <span class="text-danger">{{$message}}</span>@enderror
            </div>
        </div>

        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">تصویر:</h6>
                {!! Form::file('image',['class'=>'form-control','wire:model.lazy="image"']) !!}
                @error('image') <span class="text-danger">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">فایل:</h6>
                {!! Form::file('file',['class'=>'form-control','wire:model.lazy="file"']) !!}
                @error('file') <span class="text-danger">{{$message}}</span>@enderror
            </div>

        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-8 text-right" wire:ignore>
                <h6 class="text-muted"> توضیحات:</h6>
                {!! Form::textarea('desc',$product->description,['class'=>'form-control','id'=>'desc','wire:model.lazy="desc"']) !!}
                @error('desc') <span class="text-danger">{{$message}}</span>@enderror
                <script>
                    CKEDITOR.replace( 'desc', {
                        language: 'fa',
                        // uiColor: '#9AB8F3'
                    });
                </script>
            </div>
        </div>

        <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 text-center">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('product.index')}}">انصراف</a>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
