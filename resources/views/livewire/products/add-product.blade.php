<div>
    <form wire:submit.prevent="add_pro">
    <div class="row px-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">شماره فاکتور:</h6>
                @if($factor==null)
                    {!! Form::text('f_num',null,['class'=>'form-control','id'=>'f_num','wire:model.lazy="f_num"']) !!}
                    @error('f_num') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @else
                    <label class="form-control">{{$factor->f_num}}</label>
                    {!! Form::text('f_num',$factor->f_num,['class'=>'form-control d-none','id'=>'f_num','wire:model.lazy="f_num"']) !!}
                    @error('f_num') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @endif
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted"> قیمت کل فاکتور:</h6>
                @if($factor==null)
                    {!! Form::text('total_price',0,['class'=>'form-control','disabled'=>'disabled','wire:model.lazy="total_price"']) !!}
                    @error('total_price') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @else
                    {!! Form::text('total_price',$factor->total_price,['class'=>'form-control','disabled'=>'disabled','wire:model.lazy="total_price"']) !!}
                    @error('total_price') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @endif
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted"> تاریخ:</h6>
                @if($factor==null)
                    {!! Form::text('f_date',null,['class'=>'form-control','id'=>'f_date','wire:model.lazy="f_date"']) !!}
                    @error('f_date') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @else
                    {!! Form::text('f_date',$factor->f_date,['class'=>'form-control','id'=>'f_date','wire:model.lazy="f_date"']) !!}
                    @error('f_date') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @endif
            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-between align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">نام کالا:</h6>
                {!! Form::text('title',null,['class'=>'form-control','id'=>'p_name','wire:model.lazy="title"']) !!}
                @error('title') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted"> گروه کالا:</h6>
                <div class="autocomplete" style="width:300px;">
                    <input type="text" name="cat_id" id="cat" class="cat d-none">
                    {!! Form::text('cat',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'cats','wire:model.lazy="cat"']) !!}
                    @error('cat') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">فروشنده:</h6>
                <input type="text" name="provider_id" id="provider" class="provider d-none">
                @if($factor==null)
                    <div class="autocomplete" style="width:300px;">
                        {!! Form::text('provider_co',null,['Autocomplete'=>'off','class'=>'form-control','id'=>'providers','wire:model.lazy="provider_co"']) !!}
                        @error('provider_co') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                @else
                    {!! Form::text('provider_co',$factor->provider->name." ".$factor->provider->family,['class'=>'form-control','id'=>'providers','wire:model.lazy="provider_co"']) !!}
                    @error('provider_co') <span class="text-danger mt-3">{{$message}}</span>@enderror
                @endif
            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted"> تعداد:</h6>
                {!! Form::text('qty',null,['class'=>'form-control','id'=>'qty','wire:model.lazy="qty"']) !!}
                @error('qty') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت خرید(تومان):</h6>
                {!! Form::text('buy_price',null,['class'=>'form-control','id'=>'buy_price','wire:model.lazy="buy_price"']) !!}
                @error('buy_price') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت فروش(تومان):</h6>
                {!! Form::text('sell_price',null,['class'=>'form-control','id'=>'sell_price','wire:model.lazy="sell_price"']) !!}
                @error('sell_price') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-3 text-right">
                <h6 class="text-muted">قیمت عمده(تومان):</h6>
                {!! Form::text('whole_price',null,['class'=>'form-control','id'=>'whole_price','wire:model.lazy="whole_price"']) !!}
                @error('whole_price') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>

        </div>
        <div class="row w-100 mt-3 " wire:ignore>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">گارانتی:</h6>
                <select name="guaranty" class="form-control" id="guaranty" wire:model.lazy="guaranty">
                    <option value="false">ندارد</option>
                    <option value="true">دارد</option>
                </select>
                @error('guaranty') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section" wire:ignore>
                <h6 class="text-muted">مدت گارانتی:</h6>
                <div class="row w-100">
                    <div class="col-12 col-md-6">
                        {!! Form::text('duration',0,['class'=>'form-control','id'=>'guaranty_duration','wire:model.lazy="duration"']) !!}
                        @error('duration') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                    <div class="col-12 col-md-6">
                        <select name="duration_unit" class="form-control" wire:model.lazy="duration_unit">
                                id="guaranty_unit">
                            <option value="day">روز</option>
                            <option value="month">ماه</option>
                            <option value="year">سال</option>
                        </select>
                        @error('duration_unit') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3 col-md-4 text-right d-none guaranty-section">
                <h6 class="text-muted">شروع از:</h6>
                <select name="guaranty_start" class="form-control" id="guaranty_start" wire:model.lazy="guaranty_start">
                    <option value="sell_date">فاکتور فروش</option>
                    <option value="buy_date">فاکتور خرید</option>
                </select>
                @error('guaranty_start') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
        </div>

        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">کلیدواژه ها:</h6>
                {!! Form::text('keyword',null,['class'=>'form-control','id'=>'keywords','wire:model.lazy="keyword"']) !!}
                @error('keyword') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">بارکد:</h6>
                {!! Form::number('barcode',null,['class'=>'form-control','id'=>'barcode','wire:model.lazy="barcode"']) !!}
                @error('barcode') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-8 text-right">
                <h6 class="text-muted"> توضیحات:</h6>
                {!! Form::textarea('desc',null,['class'=>'form-control','id'=>'desc','wire:model.lazy="desc"']) !!}
                @error('desc') <span class="text-danger mt-3">{{$message}}</span>@enderror
                <script>
                    CKEDITOR.replace('desc', {
                        language: 'fa',
                        // uiColor: '#9AB8F3'
                    });
                </script>
            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-8 text-right">
                <div class="d-none col-12 inp-upload mt-3 col-md-4 text-right">
                    <h6 class="text-muted">تصویر:</h6>
                    <input type="file" class="filepond" name="image"
                           data-allow-reorder="true"
                           data-max-file-size="3MB"
                           accept="image/png, image/jpeg, image/gif"/>
                </div>
                <div class="d-none  col-12 inp-upload mt-3 col-md-4 text-right">
                    <h6 class="text-muted">فایل:</h6>
                    <input type="file" class="filepond" name="file"
                           data-allow-reorder="true"
                           data-max-file-size="200MB"/>
                </div>
            </div>
        </div>
        <span class="product_id d-none"></span>
        <div class="row w-100 mt-5 mb-5 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-center">
                <button type="submit" name="save" id="add" value="save"
                        class="btn btn-success w-50">ثبت
                </button>
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <button type="submit" name="save" id="purchase" value="save"
                        class="d-none btn btn-primary w-50">ذخیره فاکتور
                </button>
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('product.index')}}">انصراف</a>

            </div>
        </div>
    </div>
    </form>
    <table class="table table-bordered table-responsive-md table-hover table-striped"
           id="products_table">
        <tr class="table-header">
            <th>نام محصول</th>
            <th>گروه کالا</th>
            <th>قیمت خرید(واحد)</th>
            <th>قیمت فروش(واحد)</th>
            <th>تعداد</th>
        </tr>
        @if($factor!=null)
            @foreach($factor->in_products as $pro)
                <tr>
                    <th>{{$pro->product->name}}</th>
                    <th>{{$pro->product->category->title}}</th>
                    <th>{{$pro->product->buy_price}}</th>
                    <th>{{$pro->product->sell_price}}</th>
                    <th>{{$pro->product->quantity}}</th>
                </tr>
            @endforeach
        @endif
    </table>
</div>
