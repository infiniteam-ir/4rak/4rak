<div>
    {{--    {!! Form::open(['method'=>'POST','action'=>'AdminShopController@store','files'=>true,'id'=>'uploadForm'] ) !!}--}}
    <form wire:submit.prevent="save_info">
        <div class="row p-4 d-flex pb-5">
            <div class="row w-100 justify-content-center align-items-start">
                <div class="col-12 mt-3 col-md-3">
                    <h6 class="text-muted">استان:</h6>
{{--                    {!! Form::select('province',$arr, null,['placeholder'=>'انتخاب استان','class'=>'form-control','id'=>'province','wire:model.lazy'=>'province'])!!}--}}
                    <select class="form-control" name="province" id="province" wire:model.lazy="province">
                        <option value="">انتخاب استان</option>
                        @foreach($provinces as $province)
                            <option value="{{$province->id}}">{{$province->name}}</option>
                        @endforeach
                    </select>
                    @error('province')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3 col-md-3" >
                    <h6 class="text-muted">شهر:</h6>
                    <div wire:ignore>
                        <select class="form-control" name="city" id="city" wire:model.lazy="city"></select>
                    </div>
                    @error('city')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3 col-md-3">
                    <h6 class="text-muted">نام فروشگاه:</h6>
                    {!! Form::text('name',null,['class'=>'form-control','wire:model.lazy'=>'name']) !!}
                    @error('name')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
            </div>
            <div class="row w-100 mt-3 justify-content-center align-items-start ">
                <div class="col-12 mt-3 col-md-3">
                    <h6 class="text-muted">صنف:</h6>
{{--                    {!! Form::select('guild',$guilds,null,['placeholder'=>'انتخاب صنف','class'=>'form-control','wire:model.lazy'=>'guild']) !!}--}}
                    <select class="form-control" name="guild" id="guild" wire:model.lazy="guild">
                        <option value="">انتخاب صنف</option>
                        @foreach($guilds as $guild)
                            <option value="{{$guild->id}}">{{$guild->name}}</option>
                        @endforeach
                    </select>
                    @error('guild')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div id="subguild-section" class="col-12 mt-3 col-md-3" >
                    <h6 class="text-muted">رسته:</h6>
{{--                    {!! Form::text('sub_guild',null,['class'=>'form-control','wire:model.lazy'=>'sub_guild']) !!}--}}
                    <div wire:ignore>
                        <select name="sub_guild" class="form-control" id="sub_guild" wire:model.lazy="sub_guild">
                            <option value="">انتخاب رسته</option>
                        </select>
                    </div>
                    @error('sub_guild')<span class="text-danger mt-3">{{$message}}</span>@enderror
                    @error('new_sub_guild')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3 col-md-3">
                    <h6 class="text-muted">نام مخصوص فروشگاه:</h6>
                    {!! Form::text('unique_name',null,['class'=>'form-control','wire:model.lazy'=>'unique_name']) !!}
                    @error('unique_name')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
            </div>
            <div class="row w-100 mt-3 justify-content-center align-items-start ">
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted">آدرس:</h6>
                    {!! Form::textarea('address',null,['rows'=>1,'class'=>'form-control','wire:model.lazy'=>'address']) !!}
                    @error('address')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted">توضیحات:</h6>
                    {!! Form::textarea('description',null,['rows'=>1,'class'=>'form-control','wire:model.lazy'=>'description']) !!}
                    @error('description')<span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
            </div>
            <div class="row w-100 mt-3 mb-5 justify-content-center align-items-start ">
                <div class="col-12 mt-3 col-md-4">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success']) !!}
                </div>
            </div>
        </div>
    </form>
    {{--    {!! Form::close() !!}--}}
</div>

