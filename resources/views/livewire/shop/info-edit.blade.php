<div>
{{--    {!! Form::model($shop,['method'=>'PATCH','action'=>['AdminShopController@UpdateInfo',$shop->id],'files'=>true] ) !!}--}}
    <form wire:submit.prevent="update_info">
    <div class="row p-4 d-flex pb-5 bg-white">
        <div class="row w-100 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">استان:</h6>
{{--                {!! Form::select('province',$provinces,null,['placeholder'=>$shop->province->name,'class'=>'form-control','id'=>'province']) !!}--}}
                <select name="province" id="province" class="form-control" wire:model.lazy="province">
                    <option  value="{{$shop->province->id}}">{{$shop->province->name}}</option>
                @foreach($provinces as $province)
                        <option value="{{$province->id}}">{{$province->name}}</option>
                    @endforeach
                    @error('province') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </select>

            </div>
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">شهر:</h6>
{{--                {!! Form::select('city',$cities,null,['placeholder'=>$shop->city->name,'class'=>'form-control','id'=>'city']) !!}--}}
                <select name="city" id="city" class="form-control" wire:model.lazy="city">
                    <option value="{{$shop->city->id}}">{{$shop->city->name}}</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                    @error('city') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </select>
            </div>
        </div>

        <div class="row w-100 mt-3 justify-content-center align-items-top " >
            <div class="col-12 mt-3 col-md-6" >
                <h6 class="text-muted">نام فروشگاه:</h6>
{{--                {!! Form::text('name',$shop->name,['class'=>'form-control','wire:model.lazy="name"']) !!}--}}
                <input type="text" name="name" class="form-control"  wire:model.lazy="name">
                @error('name') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">نام مخصوص صفحه فروشگاه:</h6>
                <label for="" class="form-control disabled text-muted">{{$shop->unique_name}}</label>
            </div>

        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">صنف:</h6>
                <label for="" class="form-control disabled text-muted">{{$shop->guild->name}}</label>

            </div>
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">رسته:</h6>
                @if($shop->subguild_id==0)
                <label for="" class="form-control disabled text-muted">سایر</label>
                @else
                    <label for="" class="form-control disabled text-muted">{{$shop->subguild->name}}</label>
                @endif

            </div>
        </div>
        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">آدرس:</h6>
                {!! Form::text('address',$shop->address,['class'=>'form-control','wire:model.lazy="address"']) !!}
                @error('address') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>

            <div class="col-12 mt-3 col-md-6">
                <h6 class="text-muted">توضیحات:</h6>
                {!! Form::text('description',$shop->description,['class'=>'form-control','wire:model.lazy="description"']) !!}
                @error('description') <span class="text-danger mt-3">{{$message}}</span>@enderror
            </div>
        </div>
        <div class="row w-100 mt-3  justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 w-100">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 w-100">
                <a href="{{route('shopInfo')}}" class="btn btn-danger w-100">انصراف</a>
            </div>

        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
