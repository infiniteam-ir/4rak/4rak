<form wire:submit.prevent="edit_profile">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">نام: <sup class="text-danger">*</sup></h6>
                    <input type="text" name="name" class="form-control" wire:model.lazy="name">
                    @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"> نام خانوادگی: <sup class="text-danger">*</sup> </h6>
                    <input type="text" name="family" class="form-control" wire:model.lazy="family">
                    @error('family') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">موبایل: <sup class="text-danger">*</sup></h6>
                    <input type="text" name="mobile" class="form-control" wire:model.lazy="mobile">
                    @error('mobile') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">ایمیل: <sup class="text-danger">*</sup></h6>
                    <input type="email" name="email" class="form-control" wire:model.lazy="email">
                    @error('email') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">استان: <sup class="text-danger">*</sup></h6>
                    <select name="province" id="province" class="form-control" wire:model.lazy="province">
                        @if(Auth::user()->info)
                            <option value="{{Auth::user()->info->province_id}}">{{Auth::user()->info->province->name}}</option>
                        @else
                            <option value="">انتخاب کنید ...</option>
                        @endif
                        @foreach($provinces as $province)
                            <option value="{{$province->id}}">{{$province->name}}</option>
                        @endforeach
                    </select>
                    @error('province') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">شهر: <sup class="text-danger">*</sup></h6>
                    <div wire:ignore>
                        <select name="city" id="city" class="form-control" wire:model.lazy="city">
                            @if(Auth::user()->info)
                                <option value="{{Auth::user()->info->city->id}}">{{Auth::user()->info->city->name}}</option>
                            @endif
                        </select>
                    </div>
                    @error('city') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">آدرس: <sup class="text-danger">*</sup></h6>
                    <input type="text" name="address" class="form-control" wire:model.lazy="address">
                    @error('address') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">تلفن ثابت: <sup class="text-danger">*</sup></h6>
                    <input type="text" name="tell" class="form-control" wire:model.lazy="tell">
                    @error('tell') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">آواتار:</h6>
                    <input type="file" name="avatar" class="form-control" wire:model.lazy="avatar">
                    @error('avatar') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-5">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success my-2 w-50']) !!}
                    <a class="btn btn-danger my-2 w-50 " href="{{route('Profile.index')}}">انصراف</a>
                </div>
            </div>


            <div class="col-12 col-md-5 p-5">
                <img src="{{asset('img/profile-edit.svg')}}" class="img-fluid" alt="">
            </div>
        </div>

    </div>
</form>
