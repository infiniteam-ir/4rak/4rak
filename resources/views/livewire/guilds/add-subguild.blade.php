<div>
    <form wire:submit.prevent="add_subguild">
        <div class="row p-4 d-flex pb-5">
            <div class="row w-100 mt-3 justify-content-center align-items-start ">
                <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان:</h6>
                        {!! Form::text('title',null,['class'=>'form-control','wire:model.lazy="title"']) !!}
                        @error('title') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted">تصویر:</h6>
                        {!! Form::file('image',['class'=>'form-control','wire:model.lazy="image"']) !!}
                        @error('image') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                    <div class="col-12 mt-3  ">
                        <h6 class="text-muted">توضیحات:</h6>
                        <textarea class="form-control" name="desc" id="" cols="30" rows="5"wire:model.lazy="desc" ></textarea>
                        @error('desc') <span class="text-danger mt-3">{{$message}}</span>@enderror
                    </div>
                </div>
                <div class="col-12 col-md-5 p-5">
                    <img src="{{asset('img/guild.svg')}}" class="img-fluid" alt="">
                </div>
            </div>

            <div class="row w-100 mt-2 mb-5 justify-content-center align-items-center ">
                <div class="col-12 col-md-4 text-left">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                </div>
                <div class="col-12  col-md-4 text-right">
                    <a class="btn btn-danger w-50 " href="{{route('guilds-setting')}}">انصراف</a>
                </div>
            </div>
        </div>
    </form>
</div>
