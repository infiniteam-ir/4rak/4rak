<form wire:submit.prevent="save" class="comment-form">
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ Session::pull('message') }}
            </div>
        @endif
    </div>
    <div class="row comment-form-header">
        <div class="col-lg">
            <div class="comment-notes">
                <h3>دیگران را با نوشتن نظرات خود، برای انتخاب این محصول راهنمایی کنید.</h3>
                <div>
                    <p>لطفا پیش از ارسال نظر، خلاصه قوانین زیر را مطالعه کنید:</p>
                    <p>فارسی بنویسید و از کیبورد فارسی استفاده کنید. بهتر است از فضای خالی (Space) بیش‌از‌حدِ معمول، شکلک یا ایموجی استفاده نکنید و از کشیدن حروف یا کلمات با صفحه‌کلید بپرهیزید.</p><p>نظرات خود را براساس تجربه و استفاده‌ی عملی و با دقت به نکات فنی ارسال کنید؛ بدون تعصب به محصول خاص، مزایا و معایب را بازگو کنید و بهتر است از ارسال نظرات چندکلمه‌‌ای خودداری کنید.</p><p>بهتر است در نظرات خود از تمرکز روی عناصر متغیر مثل قیمت، پرهیز کنید.</p><p>به کاربران و سایر اشخاص احترام بگذارید. پیام‌هایی که شامل محتوای توهین‌آمیز و کلمات نامناسب باشند، حذف می‌شوند.</p>
                </div>
            </div>
        </div>
        <!--                                                    <div class="col-lg">
                                                                <p class="comment-form-slider">
                                                                    <span class="cfs-title">ارزش خرید به نسبت قیمت</span>
                                                                    <span class="cb-nouislider" data-name="cb_feature[c95d8a41f87fb7cdfb8450e7efc02428]"></span>
                                                                </p>
                                                                <p class="comment-form-slider">
                                                                    <span class="cfs-title">عملکرد سخت‌افزاری</span>
                                                                    <span class="cb-nouislider" data-name="cb_feature[2c5a2cfc31d2c498e8b662a4866f731e]"></span>
                                                                </p>
                                                                <p class="comment-form-slider"><span class="cfs-title">امکانات و قابلیت‌ها</span>
                                                                    <span class="cb-nouislider" data-name="cb_feature[b6f5f5ad400031dc65436d1dc6fcfdb9]"></span>
                                                                </p>
                                                                <p class="comment-form-slider">
                                                                    <span class="cfs-title">طراحی و کیفیت ساخت</span>
                                                                    <span class="cb-nouislider" data-name="cb_feature[1e26456844c4fd8face4ac475a275f2c]"></span>
                                                                </p>
                                                            </div>-->
    </div>
    <p class="comment-form-comment mt-4">
        <label for="comment">متن دیدگاه:</label>
        <textarea id="comment" name="content" class="form-control" placeholder="" cols="45" rows="8"  wire:model.lazy="content"></textarea>
        @error('content') <span class="text-danger mt-3">{{$message}}</span> @enderror
    </p>
    <div class="comment-form-rating">
        <label for="rating">امتیاز شما:</label>
        <div class="rating">
            <input type="radio" value="5" name="rating" checked id="star1" wire:model.lazy="rating"><label for="star1"></label>
            <input type="radio" value="4" name="rating" id="star2" wire:model.lazy="rating"><label for="star2"></label>
            <input type="radio" value="3" name="rating" id="star3" wire:model.lazy="rating"><label for="star3"></label>
            <input type="radio" value="2" name="rating" id="star4" wire:mode.lazyl="rating"><label for="star4"></label>
            <input type="radio" value="1" name="rating" id="star5" wire:model.lazy="rating"><label for="star5"></label>
            @error('rate') <span class="text-danger mt-3">{{$message}}</span> @enderror
        </div>
<!--        <select name="rating" id="rating" aria-required="true" required wire:model="rating">
            <option value="">رای دهید</option>
            <option value="5" selected>عالی</option>
            <option value="4">خوب</option>
            <option value="3">متوسط</option>
            <option value="2">نه خیلی بد</option>
            <option value="1">خیلی بد</option>
        </select>
        @error('rating') <span class="text-danger mt-2">{{$message}}</span> @enderror-->
    </div>

    <style>
        .rating{
            position: absolute;
            transform: translate(-50%,-50%);
            display: flex;
        }
        .rating input{
            display: none;
        }
        .rating label{
            display: block;
            cursor: pointer;
            width: 50px;
            /*background: #ccc;*/
        }
        .rating label:before{
            content: '\f005';
            font-family: FontAwesome;
            position: relative;
            display: block;
            font-size:30px ;
            color: #f3eaea;
        }
        .rating label:after{
            content: '\f005';
            font-family: FontAwesome;
            position: absolute;
            display: block;
            font-size:30px ;
            color: #e6ee0b;
            top: 0;
            opacity: 0;
            transition:.5s ;
            text-shadow:0 2px 5px rgba(0,0,0,.5) ;
        }
        .rating label:hover:after,
        .rating label:hover ~ label:after,
        .rating input:checked ~ label:after
        {
            opacity: 1;
        }

    </style>
    <div class="row mt-4">
        <div class="col-lg">
            <div class="comment-form-chip plus-cfc">
                <label>نقاط قوت: </label>
                <input type="text" name="positives" class="form-control" wire:model.lazy="positives">
                @error('positives') <span class="text-danger mt-3">{{$message}}</span> @enderror
            </div>
        </div>
        <div class="col-lg">
            <div class="comment-form-chip nega-cfc">
                <label>نقاط ضعف: </label>
                <input type="text" name="negatives" class="form-control" wire:model.lazy="negatives">
                @error('negatives') <span class="text-danger mt-3">{{$message}}</span> @enderror
            </div>
        </div>
    </div>
    <style>
        #sliderr{
            -webkit-appearance: none;
            color: #0b2e13;
            box-shadow: inset 0 3px 18px rgb(22, 108, 210);
            outline: none;
            height: 8px;
            border-radius: 5px;

        }
        #sliderr::-webkit-slider-thumb{
            -webkit-appearance: none;
            width: 48px;
            height: 48px;
            cursor: pointer;
            z-index: 3;
            position: relative;
        }
        #selectorr{
            height: 104px;
            width: 48px ;
            position: absolute;
            bottom: -20px;
            left: 50%;
            transform: translateX(-50%);
            z-index: 2;
        }
        .selectbtnn{
            height: 48px;
            width: 48px;
            background-image: {{asset('img/default.jpg')}};
            background-size: cover;
            background-position: center;
            border-radius: 50%;
            position: absolute;
            bottom: 0;
        }
    </style>

    <div class="col-lg mt-2">
        <div class="" wire:ignore>
            <label for="customRange2" class="form-label">ارزش خرید به نسبت قیمت</label>
            <input id="sliderr"  style="direction: ltr" name="value_per_price" type="range" min="0" max="5" value="5" onchange="showval(this.value)" >
            <span id="valBox"  class="text-success p-2" style="border-radius: 50%">5</span>
        </div>
    </div>
    <div class="col-lg mt-2">
        <div class="" wire:ignore>
            <label for="customRange2" class="form-label">طراحی و کیفیت </label>
            <input id="sliderr"  style="direction: ltr" name="quality" type="range" min="0" max="5" value="5" onchange="showval2(this.value)" >
            <span id="valBox2"  class="text-success p-2" style="border-radius: 50%">5</span>
        </div>
    </div>
    <script>
        function showval(newval){
            document.getElementById("valBox").innerHTML=newval;
        }
        function showval2(newval){
            document.getElementById("valBox2").innerHTML=newval;
        }
    </script>

    <p class="form-submit">
        <button name="submit" type="submit"  class="btn btn-primary" >ثبت دیدگاه</button>
        <input type='hidden' name='comment_post_ID' value='' id='comment_post_ID' />
        <input type='hidden' name='comment_parent' id='comment_parent' value='' />
    </p>
</form>
