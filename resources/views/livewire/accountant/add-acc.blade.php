<div>
    <form wire:submit.prevent="add_acc">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 justify-content-center align-items-center ">
            <div class="col-12  col-md-4 text-right">
                <h6 class="text-muted"><sup class="text-danger">*</sup> نوع حساب:</h6>
                <select name="type" class="form-control" id="type" wire:model.lazy="type">
                    <option value="none">انتخاب کنید</option>
                    <option value="3">دسته چک</option>
                    <option value="2">بانک</option>
                    <option value="1">محلی</option>
                </select>
                @error('type') <span class="text-danger mt-2">{{$message}}</span> @enderror

                <h6 class="text-muted"><sup class="text-danger">*</sup> نام حساب: (جهت نمایش)</h6>
                <input type="text" name="name" class="form-control" wire:model.lazy="name">
                @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror

                <section class="d-none" id="bank-section" wire:ignore>
                    <h6 class="text-muted"><sup class="text-danger">*</sup> نام بانک:</h6>
                    <input type="text" name="bank" class="form-control" wire:model.lazy="bank">
                    @error('bank') <span class="text-danger mt-2">{{$message}}</span> @enderror

                    <h6 class="text-muted"><sup class="text-danger">*</sup>  نام صاحب حساب:</h6>
                    <input type="text" name="owner_name" class="form-control" wire:model.lazy="owner_name">
                    @error('owner_name') <span class="text-danger mt-2">{{$message}}</span> @enderror

                    <h6 class="text-muted"><sup class="text-danger">*</sup>  نام خانوادگی صاحب حساب:</h6>
                    <input type="text" name="owner_family" class="form-control" wire:model.lazy="owner_family">
                    @error('owner_family') <span class="text-danger mt-2">{{$message}}</span> @enderror


                    <h6 class="text-muted"><sup class="text-danger">*</sup>  نوع : </h6>
                    <select name="acc_type" class="form-control" id="acc_type" wire:model.lazy="acc_type">
                        <option value="none">انتخاب کنید</option>
                        <option value="1">جاری</option>
                        <option value="2">پس انداز</option>
                        <option value="3">قرض الحسنه</option>
                    </select>
                    @error('acc_type') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </section>

                <section class="d-none" id="cheque-section" wire:ignore>
                    <h6 class="text-muted"><sup class="text-danger">*</sup>  بانک: </h6>
{{--                    <input type="text"  name="cheque_bank" id="cheque_bank" class="form-control" wire:model.lazy="cheque_bank">--}}
                    <select name="cheque_bank" id="cheque_bank" class="form-control" wire:model.lazy="cheque_bank" >
                        <option value="none">انتخاب کنید</option>
                        @foreach($banks as $bank)
                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                        @endforeach
                    </select>
                    @error('cheque_bank') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </section>

                <h6 class="text-muted mb-0"><sup class="text-danger">*</sup> موجودی:</h6>
                <p class="text-muted mt-0">(در صورت ایجاد دسته چک، این فیلد را خالی بگذارید)</p>
                <div wire:ignore>
                    <input type="text" value="0" name="amount" id="amount" class="form-control" wire:model.lazy="amount">
                </div>
                @error('amount') <span class="text-danger mt-2">{{$message}}</span> @enderror

            </div>
            <div class="col-12 col-md-4 p-4 text-right">
                <img src="{{asset('img/accountant.svg')}}" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row w-100  mb-5 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 text-center">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('accounts')}}">انصراف</a>
            </div>
        </div>
    </div>
    </form>
</div>
