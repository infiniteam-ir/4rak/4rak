<div>
{{--    {!! Form::open(['method'=>'POST','action'=>'AdminAccountantController@saveOutgo'] ) !!}--}}
    <form wire:submit.prevent="set_charak_services">
    <div class="row  d-flex pb-5 justify-content-center">
        <div class="col-12 col-md-8 ">
            <div class="row w-100  justify-content-center align-items-center ">
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان سرویس: </h6>
{{--                    {!! Form::text('name',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="name" class="form-control" wire:model.lazy="name">
                    @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> مبلغ:</h6>
{{--                    {!! Form::text('price',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="price" class="form-control" wire:model.lazy="price">
                    @error('price') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> کد پیگیری:</h6>
{{--                    {!! Form::text('tracking_id',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="tracking_id" class="form-control" wire:model.lazy="tracking_id">
                    @error('tracking_id') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> حساب:</h6>
                    <select class="form-control" name="source" id="" wire:model.lazy="source">
                        <option value="none">انتخاب کنید...</option>
                        @foreach($accounts as $account)
                            <option value="{{$account->id}}">
                                {{$account->name}}
                            </option>
                        @endforeach
                    </select>
                    @error('source') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
            </div>
            <div class="row w-100 mt-3 mb-5 justify-content-center align-items-center ">
                <div class="col-12 mt-3 col-md-4 text-center">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                </div>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
