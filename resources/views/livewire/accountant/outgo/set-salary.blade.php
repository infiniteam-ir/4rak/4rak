<div>
{{--    {!! Form::open(['method'=>'POST','action'=>'AdminAccountantController@saveOutgo'] ) !!}--}}
    <form wire:submit.prevent="set_salary">
    <div class="row  d-flex pb-5 justify-content-center">
        <div class="col-12 col-md-8 ">
            <div class="row w-100  justify-content-center align-items-center ">
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted">نام کارمند: </h6>
                    <select class="form-control" name="user" id="" wire:model.lazy="user">
                        <option value="none">انتخاب کنید...</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">
                                {{$user->name}} {{$user->family}}
                            </option>
                        @endforeach
                    </select>
                    @error('user') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> مبلغ پرداختی:</h6>
{{--                    {!! Form::text('price',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="price" class="form-control" wire:model.lazy="price">
                    @error('price') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3 col-md-6" >
                    <h6 class="text-muted mb-0"><sup class="text-danger">*</sup> از تاریخ:</h6>
                    <p class="mt-0">(مثال: 1400/06/01)</p>
                    <div >
                        <input type="text" name="start" autocomplete="off" class="form-control"   wire:model.lazy="start" id="start" >
                    </div>
                    @error('start') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6" >
                    <h6 class="text-muted mb-0"><sup class="text-danger">*</sup> تا تاریخ:</h6>
                    <p class="mt-0">(مثال: 1400/06/31)</p>

                    <div >
                        <input type="text" name="end" autocomplete="off" class="form-control" wire:model.lazy="end" id="end">
                    </div>
                    @error('end') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> نحوه محاسبه: <span>(ساعتی ،روزانه، ماهانه و ...)</span> </h6>

{{--                    {!! Form::text('calc_method',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="calc_method" class="form-control" wire:model.lazy="calc_method">
                    @error('calc_method') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> میزان کارکرد:</h6>
                    <input type="text" name="total_work" class="form-control" wire:model.lazy="total_work">
{{--                    {!! Form::number('total_work',null,['class'=>'form-control']) !!}--}}
                    @error('total_work') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted">کد پیگیری:</h6>
{{--                    {!! Form::text('tracking_id',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="tracking_id" class="form-control" wire:model.lazy="tracking_id">
                    @error('tracking_id') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3 col-md-6">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> حساب:</h6>
                    <select class="form-control" name="source" id="" wire:model.lazy="source">
                        <option value="none">انتخاب کنید...</option>
                        @foreach($accounts as $account)
                            <option value="{{$account->id}}">
                                {{$account->name}}
                            </option>
                        @endforeach
                    </select>
                    @error('source') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
            </div>

            <div class="row w-100 mt-3 mb-5 justify-content-center align-items-center ">
                <div class="col-12 mt-3 col-md-4 text-center">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                </div>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
