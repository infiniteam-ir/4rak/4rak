<div>
{{--    {!! Form::model($storage,['method'=>'PATCH','action'=>['AdminStorageController@update',$storage->id]]) !!}--}}
    <form wire:submit.prevent="edit_storage">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">نام:</h6>
{{--                {!! Form::text('name',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="name" class="form-control" wire:model.lazy="name">
                @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4 text-right">
                <h6 class="text-muted">آدرس:</h6>
{{--                {!! Form::text('address',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="address" class="form-control" wire:model.lazy="address">
                @error('address') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
        </div>
        <div class="row w-100 mt-5 mb-5 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 text-center">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('storage.index')}}">انصراف</a>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
