<div>
{{--    {!! Form::open(['method'=>'POST','action'=>'AdminStorageController@store']) !!}--}}
    <form wire:submit.prevent="set_storage">
    <div class="row p-4 d-flex pb-5">
        <div class="col-12 col-md-6">
            <div class="row w-100 mt-3 p-5 justify-content-center align-items-center ">
                <div class="col-12 mt-3  text-right">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> نام:</h6>
{{--                    {!! Form::text('name',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="name" class="form-control" wire:model.lazy="name">
                    @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3  text-right">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> آدرس:</h6>
{{--                    {!! Form::text('address',null,['class'=>'form-control']) !!}--}}
                    <input type="text" name="address" class="form-control" wire:model.lazy="address">
                    @error('address') <span class="text-danger mt-2">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3 col-md-4 text-center">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
                </div>
                <div class="col-12 mt-3 col-md-4 text-center">
                    <a class="btn btn-danger w-50 " href="{{route('storage.index')}}">انصراف</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 p-5">
            <img class="img-fluid" src="{{asset('img/warehouse.svg')}}">
        </div>

    </div>
{{--    {!! Form::close() !!}--}}
    </form>
</div>
