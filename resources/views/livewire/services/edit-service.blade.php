<form wire:submit.prevent="edit_service">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-center ">
            <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">عنوان:</h6>
                    <input type="text" name="title" class="form-control" wire:model.lazy="title">
                    @error('title') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">  نوع خدمت: (مثال: پروژه ای، متری، ساعتی و ...)</h6>
                    <input type="text" name="type" class="form-control" wire:model.lazy="type">
                    @error('type') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">هزینه بر اساس نوع خدمت:</h6>
                    <input type="text" name="price" class="form-control" wire:model.lazy="price">
                    @error('price') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">شماره تماس:</h6>
                    <input type="text" name="call" class="form-control" wire:model.lazy="call">
                    @error('call') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">توضیحات:</h6>
                    <input type="text" name="desc" class="form-control" wire:model.lazy="desc">
                    @error('desc') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">تصویر:</h6>
                    <input type="file" name="image" class="form-control" wire:model.lazy="image">
                    @error('image') <span class="text-danger mt-2">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-5">
                    {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success my-2 w-50']) !!}
                    <a class="btn btn-danger my-2 w-50 " href="{{route('service.index')}}">انصراف</a>
                </div>
            </div>


            <div class="col-12 col-md-5 p-5">
                <img src="{{asset('img/customer-service.svg')}}" class="img-fluid" alt="">
            </div>
        </div>

    </div>
</form>
