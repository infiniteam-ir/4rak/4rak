<div>
{{--    {!! Form::model($cat,['method'=>'PATCH','action'=>['AdminCategoryController@update',$cat->id],'files'=>true]) !!}--}}
    <form wire:submit.prevent="update_cat">
    <div class="row p-4 d-flex justify-content-center pb-5">
        <div class="row w-100  justify-content-center align-items-center ">
            <div class="col-12 col-md-7 p-5 border border-1-gainsboro rounded">
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان:</h6>
                    {!! Form::text('title',$cat->title,['class'=>'form-control','wire:model.lazy="title"']) !!}
                    @error('title') <span class="text-danger">{{$message}}</span> @enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted">تصویر:</h6>
                    {!! Form::file('image',['class'=>'form-control','wire:model.lazy="image"']) !!}
                    @error('image') <span class="text-danger">{{$message}}</span> @enderror

                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"> گروه والد:</h6>
                    <select name="parent" class="form-control" wire:model.lazy="parent">
                        <option value="0">
                            ندارد
                        </option>
                        @foreach($cats as $cat)
                            <option value={{$cat->title}}>
                                {{$cat->title}}
                            </option>
                        @endforeach
                    </select>
                    @error('parent') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>
            <div class="col-12 col-md-5 p-5">
                @if($cat->photo)
                    <img src="{{asset('/images/cats/'.$cat->photo->path)}}" class="img-fluid" alt="">
                @else
                    <img src="{{asset('img/categories.svg')}}" class="img-fluid" alt="">
                @endif
            </div>
        </div>

        <div class="row w-100 mt-2 mb-5 justify-content-center align-items-center ">
            <div class="col-12 col-md-4 text-center my-2 text-lg-left">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12  col-md-4 text-center my-2 text-lg-right">
                <a class="btn btn-danger w-50 " href="{{route('category.index')}}">انصراف</a>
            </div>
        </div>
    </div>
{{--    {!! Form::close() !!}--}}
    </form>
</div>
