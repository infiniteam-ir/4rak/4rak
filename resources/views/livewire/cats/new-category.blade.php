<div>
{{--    {!! Form::open(['method'=>'POST','action'=>'AdminCategoryController@store','files'=>true]) !!}--}}
    <form wire:submit.prevent="save_new_cat" >
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 col-md-7 border border-1-gainsboro rounded p-5">
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> عنوان:</h6>
                    {!! Form::text('title',null,['class'=>'form-control','wire:model.lazy="title"']) !!}
                    @error('title') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"><sup class="text-danger">*</sup> تصویر:</h6>
                    {!! Form::file('image',['class'=>'form-control','wire:model.lazy="image"']) !!}
                    @error('image') <span class="text-danger mt-3">{{$message}}</span>@enderror
                </div>
                <div class="col-12 mt-3  ">
                    <h6 class="text-muted"> گروه والد:</h6>
                    <select name="parent" class="form-control" wire:model.lazy="parent">
                        @php
                            function kids($cats,$parent,$level){
                            $tree=' |_ ';
                            for ($i=1;$i <$level;$i++){
                                $tree="  ".$tree;
                            }
                                foreach ($cats as $cat){
                                if ($cat->parent_id==$parent->id){
                                echo '<option value='.$cat->id.'>
                                                '.$tree.$cat->title.'
                                            </option>';
                                     kids($cats,$cat,$level+1);
                                }
                                }
                                }
                        @endphp
                        <option value="0">
                            ندارد
                        </option>
                        @foreach($cats as $cat)
                            @if($cat->parent_id==null)
                                <option value={{$cat->id}}>
                                    {{$cat->title}}
                                </option>
                                @php(kids($cats,$cat,1))
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-5 p-5">
                <img src="{{asset('img/categories.svg')}}" class="img-fluid" alt="">
            </div>
        </div>

        <div class="row w-100 mt-2 mb-5 justify-content-center align-items-center ">
            <div class="col-12 col-md-4 my-2">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12  col-md-4 my-2 ">
                <a class="btn btn-danger w-50 " href="{{route('category.index')}}">انصراف</a>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
