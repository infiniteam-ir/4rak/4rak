<div>
    <form wire:submit.prevent="save_comment" >
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ Session::pull('message') }}
                </div>
            @endif
        </div>
        <p class="comment-form-comment mt-4">
            <label for="comment">متن دیدگاه:</label>
            <textarea id="comment" name="content" class="form-control" placeholder="" cols="45" rows="8"  wire:model.lazy="content"></textarea>
            @error('content') <span class="text-danger mt-3">{{$message}}</span> @enderror
        </p>

        <div class="row mt-4">
            <div class="col-lg">
                <div class="comment-form-chip plus-cfc">
                    <label>نقاط قوت: </label>
                    <input type="text" name="positives" class="form-control" wire:model.lazy="positives">
                    @error('positives') <span class="text-danger mt-3">{{$message}}</span> @enderror
                </div>
            </div>
            <div class="col-lg">
                <div class="comment-form-chip nega-cfc">
                    <label>نقاط ضعف: </label>
                    <input type="text" name="negatives" class="form-control" wire:model.lazy="negatives">
                    @error('negatives') <span class="text-danger mt-3">{{$message}}</span> @enderror
                </div>
            </div>
        </div>

        <p class="form-submit">
            <button type="submit"  class="btn btn-primary" >ثبت دیدگاه</button>
        </p>
    </form>
</div>
