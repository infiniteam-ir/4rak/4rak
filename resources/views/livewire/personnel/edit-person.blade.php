<div>
    <form wire:submit.prevent="edit_person">
        <div class="row w-100  justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> نام:</h6>
                {{--            {!! Form::text('name',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="name" class="form-control" wire:model.lazy="name">
                @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> نام خانوادگی</h6>
                {{--            {!! Form::text('family',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="family" class="form-control" wire:model.lazy="family">
                @error('family') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> موبایل:</h6>
                {{--            {!! Form::text('mobile',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="mobile" class="form-control" wire:model.lazy="mobile">
                @error('mobile') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
        </div>

        <div class="row w-100 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> ایمیل:</h6>
                {{--            {!! Form::email('email',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="email" class="form-control" wire:model.lazy="email">
                @error('email') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted">کلمه عبور:</h6>
                {{--            {!! Form::password('password',['class'=>'form-control']) !!}--}}
                <input type="password" name="password" class="form-control" wire:model.lazy="password">
                @error('password') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>

        </div>

        <div class="row w-100 mt-3 mb-5 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4 text-center">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('personnel.index')}}">انصراف</a>
            </div>
        </div>
    </form>
</div>
