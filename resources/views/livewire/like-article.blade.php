<div>
    {{-- Nothing in the world is as soft and yielding as water. --}}
    <li class="my-2">
        <i class="fas fa-heart" style="cursor: pointer" wire:click="like({{$article->id}})"></i>
        <span>{{$article->rate}} پسندیده شده</span>
    </li>
</div>
