<div>
{{--    {!! Form::open(['method'=>'POST','action'=>'AdminDeliveryController@store','files'=>true] ) !!}--}}
    <form wire:submit.prevent="set_delivery">
    <div class="row p-4 d-flex pb-5">
        <div class="row w-100 mt-3 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> نام:</h6>
{{--                {!! Form::text('name',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="name" class="form-control" wire:model.lazy="name">
                @error('name') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> نام خانوادگی</h6>
{{--                {!! Form::text('family',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="family" class="form-control" wire:model.lazy="family">
                @error('family') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> موبایل:</h6>
{{--                {!! Form::text('mobile',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="mobile" class="form-control" wire:model.lazy="mobile">
                @error('mobile') <span class="text-danger mt-2">{{$message}}</span> @enderror

            </div>
        </div>
        <div class="row w-100 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> استان:</h6>
{{--                {!! Form::select('province',$arr, null,['class'=>'form-control','id'=>'province']) !!}--}}
                <select name="province" class="form-control" id="province" wire:model.lazy="province">
                    <option value="none">انتخاب کنید</option>
                    @foreach($provinces as $province)
                        <option value="{{$province->id}}">{{$province->name}}</option>
                    @endforeach
                </select>
                @error('province') <span class="text-danger mt-2">{{$message}}</span> @enderror

            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> شهر:</h6>
                <div wire:ignore>
                    <select class="form-control" name="city" id="city" wire:model.lazy="city"></select>
                </div>
                @error('city') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
        </div>
        <div class="row w-100 justify-content-center align-items-start ">
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted">تصویر:</h6>
{{--                {!! Form::file('avatar',['class'=>'form-control']) !!}--}}
                <input type="file" name="avatar" class="form-control" wire:model.lazy="avatar">
                @error('avatar') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> سن:</h6>
{{--                {!! Form::text('age',null,['class'=>'form-control']) !!}--}}
                <input type="text" name="age" class="form-control" wire:model.lazy="age">
                @error('age') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>
            <div class="col-12 mt-3 col-md-4">
                <h6 class="text-muted"><sup class="text-danger">*</sup> وسیله:</h6>
                <select class="form-control" name="vehicle" wire:model.lazy="vehicle">
                    <option value="">انتخاب کنید</option>
                    <option value="موتور">موتور</option>
                    <option value="وانت">وانت</option>
                    <option value="کامیون">کامیون</option>
                </select>
                @error('vehicle') <span class="text-danger mt-2">{{$message}}</span> @enderror
            </div>

        </div>
        <div class="row w-100 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-7" >
                <h6 class="text-muted"><sup class="text-danger">*</sup> مشخصات وسیله:</h6>
{{--                {!! Form::textarea('detail',null,['class'=>'form-control','id'=>'detail']) !!}--}}
                <div wire:ignore>
                    <textarea name="detail" class="form-control" id="detail" cols="30" rows="10" wire:model.lazy="detail"></textarea>
                </div>
                @error('detail') <span class="text-danger mt-2">{{$message}}</span> @enderror

            </div>
<!--            <script>
                CKEDITOR.replace('detail', {
                    language: 'fa',
                    // uiColor: '#9AB8F3'
                });
            </script>-->
        </div>

        <div class="row w-100 mt-3 mb-5 justify-content-center align-items-center ">
            <div class="col-12 mt-3 col-md-4 text-center">
                {!! Form::submit('ذخیره',['class'=>'form-control btn btn-success w-50']) !!}
            </div>
            <div class="col-12 mt-3 col-md-4 text-center">
                <a class="btn btn-danger w-50 " href="{{route('deliveryReq')}}">انصراف</a>
            </div>
        </div>
    </div>
    </form>
{{--    {!! Form::close() !!}--}}
</div>
