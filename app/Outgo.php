<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outgo extends Model
{
    public function account()
    {
        return $this->hasOne(Account::class,'source_id');
    }

    public function factor()
    {
        return $this->belongsTo(Input_Factor::class);
    }

    public function bill()
    {
        return $this->belongsTo(Outgo_Bill::class,'target_id');
    }

    public function charak_service()
    {
        return $this->belongsTo(Outgo_Charak_Service::class,'target_id');
    }

    public function salary()
    {
        return $this->belongsTo(Outgo_Salary::class,'target_id');
    }

    public function other()
    {
        return $this->belongsTo(Outgo_Other::class,'target_id');
    }
}
