<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    public function factor()
    {
        return $this->hasMany(Output_factor::class,'source_id','f_num');
    }

    public function online_factor()
    {
        return $this->hasMany(online_output_factor::class,'source_id','f_num');
    }
}
