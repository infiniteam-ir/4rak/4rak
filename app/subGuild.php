<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subGuild extends Model
{
    public function guild()
    {
        return $this->belongsTo(Guild::class,'parent_id');
    }
}
