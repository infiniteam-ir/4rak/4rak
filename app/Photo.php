<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
   /* public function user()
    {
        return $this->hasMany(User::class);
    }*/

   /* public function shop()
    {
        return $this->hasMany(Shop::class);
    }*/
    public function groupable()
    {
        return $this->morphTo();
    }
}
