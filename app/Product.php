<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'cat_id','id');
    }

    public function photo()
    {
        return $this->morphOne(Photo::class,'groupable');

    }
    public function photos()
    {
        return $this->morphMany(Photo::class,'groupable');

    }


    public function file()
    {
        return $this->belongsTo(File::class);

    }

    public function pivot()
    {
        return $this->hasOne(StoragePivot::class);
    }

    public function guaranty()
    {
        return $this->hasOne(Guaranty::class);
    }

    public function out_products()
    {
        return $this->hasMany(Output_factor_pro::class);
    }

    public function in_products()
    {
        return $this->hasMany(Input_factor_pro::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
