<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Input_Factor_Pro extends Model
{
    public function factor()
    {
        return $this->belongsTo(Input_factor::class,'factor_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
