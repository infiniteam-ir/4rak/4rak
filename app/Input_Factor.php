<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Input_Factor extends Model
{
    public function in_products()
    {
        return $this->hasMany(Input_factor_pro::class,'factor_id','id');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function outgoes()
    {
        return $this->hasMany(Outgo::class,'target_id','f_num');
    }
}
