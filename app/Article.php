<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function category()
    {
        return $this->belongsTo(Article_category::class,'cat_id','id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class,'author_id','id');
    }


    public function comments()
    {
        return $this->morphMany(Comment::class,'groupable');
    }

}
