<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public function factors()
    {
        return $this->hasMany(online_output_factor::class);
    }
}
