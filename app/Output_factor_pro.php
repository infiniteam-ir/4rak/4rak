<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Output_factor_pro extends Model
{
    public function factor()
    {
        return $this->belongsTo(Output_factor::class,'factor_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


}
