<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'family', 'mobile', 'sex', 'role_id', 'province', 'city', 'last_login_at',
        'last_login_ip', 'last_logout_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function city()
    {
        return $this->hasOne(City::class);
    }

    public function province()
    {
        return $this->hasOne(Province::class);
    }

    public function shop()
    {
        return $this->hasOne(Shop::class);
    }

    public function file()
    {
        return $this->hasMany(File::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function shoprole()
    {
        return $this->belongsTo(ShopRole::class, 'shop_role_id', 'id');
    }

    public function logins()
    {
        return $this->hasMany(Login_log::class);
    }
    public function logouts()
    {
        return $this->hasMany(Logout_log::class);
    }

    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

}
