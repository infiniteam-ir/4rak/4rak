<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class myCheque extends Model
{
    public function account()
    {
        return $this->belongsTo(Account::class,'belong_to_acc');
    }
}
