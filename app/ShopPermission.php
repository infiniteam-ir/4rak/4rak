<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopPermission extends Model
{
    public function shoprole()
    {
        return $this->hasOne(ShopRole::class,'shop_permission_id','id');
    }
}
