<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logout_log extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
