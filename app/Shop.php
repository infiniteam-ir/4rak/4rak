<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function file()
    {
        return $this->hasMany(File::class);
    }

    public function shoprole()
    {
        return $this->hasMany(ShopRole::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function logo()
    {
        return $this->belongsTo(Photo::class,'logo_id');
    }

    public function header()
    {
        return $this->belongsTo(Photo::class,'header_id');
    }

    public function guild()
    {
        return $this->belongsTo(Guild::class);
    }
    public function subguild()
    {
        return $this->belongsTo(subGuild::class);
    }


}
