<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guild extends Model
{
    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function subguild()
    {
        return $this->hasMany(subGuild::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
