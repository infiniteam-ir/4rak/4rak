<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function outgo()
    {
        return $this->belongsTo(Outgo::class);
    }

    public function acc_type()
    {
        return $this->belongsTo(Account_type::class);
    }

}
