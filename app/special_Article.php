<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class special_Article extends Model
{
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
