<?php

namespace App;


class Cart
{
    public $items=null;
    public  $totalQty=0;
    public  $totalPrice=0;

    public function __construct($oldCart)
    {
        if ($oldCart){
            $this->items=$oldCart->items;
            $this->totalQty=$oldCart->totalQty;
            $this->totalPrice=$oldCart->totalPrice;
        }
    }

    public function add($item, $id)
    {
        $storedItem=['Qty'=>0,'Price'=>$item->sell_price,'Item'=>$item];
        if ($this->items){
            if (array_key_exists($id,$this->items)){
                $storedItem=$this->items[$id];
            }
        }
        $storedItem['Qty']++;
        $storedItem['Price']=$item->sell_price*$storedItem['Qty'];
        $this->items[$id]=$storedItem;
        $this->totalQty++;
        $this->totalPrice+=$item->sell_price;

    }

    public function remove($item,$id)
    {

        $storedItem=['Qty'=>0,'Price'=>$item->sell_price,'Item'=>$item];
        if ($this->items){
            if (array_key_exists($id,$this->items)){
                $storedItem=$this->items[$id];

            }
        }

        $storedItem['Qty']--;
        if ($item->discount){
            $storedItem['Price']=$item->discount*$storedItem['Qty'];
            $this->totalPrice-= $item->discount;
        }else{
            $storedItem['Price']=$item->price*$storedItem['Qty'];
            $this->totalPrice-= $item->sell_price;
        }
        $this->items[$id]=$storedItem;
        $this->totalQty--;

    }




}
