<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_method extends Model
{
    public function factors()
    {
        return $this->hasMany(online_output_factor::class);
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
