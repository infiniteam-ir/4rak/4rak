<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoragePivot extends Model
{
    public function storage()
    {
        return $this->belongsTo(Storage::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
