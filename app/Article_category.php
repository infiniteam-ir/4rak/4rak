<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article_category extends Model
{
    public function articles()
    {
        return $this->belongsTo(Article::class,'id','cat_id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
