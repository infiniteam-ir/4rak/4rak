<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function photo()
    {
        return $this->morphOne(Photo::class,'groupable');
    }

    public function products()
    {
        return $this->belongsTo(Product::class,'id','cat_id');
    }


}
