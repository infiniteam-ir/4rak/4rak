<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
