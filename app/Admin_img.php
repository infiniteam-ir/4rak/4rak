<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin_img extends Model
{
    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
