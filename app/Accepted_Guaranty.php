<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accepted_Guaranty extends Model
{
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function output_factor()
    {
        return $this->belongsTo(Output_factor_pro::class);
    }
}
