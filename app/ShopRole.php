<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopRole extends Model
{
    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function roles()
    {
        return $this->belongsTo(ShopPermission::class,'shop_permission_id','id');
    }



}
