<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public function factor()
    {
        return $this->hasMany(Input_Factor::class,'provider_id');
    }
}
