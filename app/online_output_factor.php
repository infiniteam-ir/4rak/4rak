<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class online_output_factor extends Model
{

    public function factor_pro()
    {
        return $this->hasMany(online_output_pro::class,'factor_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function incomes()
    {
        return $this->belongsTo(Income::class,'id','source_id');
    }

    public function delivery_method()
    {
        return $this->belongsTo(Delivery_method::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
