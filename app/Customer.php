<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function output_factor()
    {
        return $this->hasMany(Output_factor::class,'customer_id');
    }
    public function online_output_factor()
    {
        return $this->hasMany(online_output_factor::class,'customer_id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
