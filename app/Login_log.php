<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login_log extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
