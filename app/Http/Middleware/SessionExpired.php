<?php

namespace App\Http\Middleware;

use App\Logout_log;
use Closure;
use Illuminate\Session\Store;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SessionExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        // If user is not logged in...
        if (!Auth::check()) {
            return $next($request);
        }
        $now = Carbon::now();
        $user = Auth::user();
        $logout = Logout_log::where([['shop_id', $user->shop_id], ['user_id', Auth::id()]])->orderby('created_at')->get()->last();
        if ($logout) {
            $logout->logout_time = $now->addHour();
            $logout->save();
        }


        /* if (!Session::has('last-seen')) {
             Session::put('last-seen', Carbon::now());
         }*/

        /*  $now = Carbon::now();
          $absence = $now->diffInMinutes(Session('last-seen'));*/

        // If user has been inactivity longer than the allowed inactivity period
        /* if ($absence >= 10) {
             if (Auth::user()->shop_id){
                 $log=new Logout_log();
                 $log->user_id=Auth::id();
                 $log->shop_id=Auth::user()->shop_id;
                 $log->logout_time=Carbon::now();
                 $log->save();
             }
             Auth::guard()->logout();

             $request->session()->invalidate();

             return $next($request);
         }
         Session::put('last-seen', Carbon::now());*/

        return $next($request);
    }
}
