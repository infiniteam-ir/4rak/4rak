<?php

namespace App\Http\Middleware;

use App\MobileCodes;
use Closure;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class verifyMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Illuminate\Support\Facades\Route::CurrentRouteName()=='verifyMobile' || \Illuminate\Support\Facades\Route::CurrentRouteName()=='change_mobile'
            || \Illuminate\Support\Facades\Route::CurrentRouteName()=='update_mobile'){
            return $next($request);
        }else{
            if (Auth::check() && Auth::user()->mobile_verified_at == null){
                $code=MobileCodes::where([['user_id',Auth::id()],['created_at','>',now()->subMinute(2)]])->first();
                if ($code==null){
                    $codes=MobileCodes::where([['user_id',Auth::id()],['created_at','<',now()->subMinute(4)]])->delete();
                    $mobile_code=new MobileCodes();
                    $mobile_code->user_id=Auth::id();
                    $rand=rand(10000,99999);
                    $mobile_code->code=$rand;
                    Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                        'receptor' => Auth::user()->mobile,
                        'token' => str_replace(' ','-',trim(Auth::user()->name)),
                        'token2' => str_replace(' ','-',trim(Auth::user()->family)),
                        'token3' => $rand,
                        'template' => '4rakMobileVerif',
                    ]);
                    $mobile_code->save();
                    return response()->view('auth.verifyMobile');
                }else{
                    return response()->view('auth.verifyMobile');
                }
            }else{
                return $next($request);
            }
        }


    }
}
