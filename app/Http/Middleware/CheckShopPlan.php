<?php

namespace App\Http\Middleware;

use App\ShopPlanPivot;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckShopPlan
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $shop = Auth::user()->shop;
            if($shop != null){
                $shop_plans = ShopPlanPivot::where([['shop_id', $shop->id], ['status', 'active']])->get();
                if (count($shop_plans) > 0) {
                    foreach ($shop_plans as $plan) {
                        if (Carbon::parse($plan->exp_date)->lt(Carbon::today()))
                            return redirect()->to(route('buy_plan'));
                    }
                } else {
                    return redirect()->to(route('buy_plan'));
                }
            }else{
                return redirect(route('shopInfo'));
            }
        }
        return $next($request);
    }
}
