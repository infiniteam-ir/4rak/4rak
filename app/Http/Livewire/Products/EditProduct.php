<?php

namespace App\Http\Livewire\Products;

use App\Category;
use App\File;
use App\Photo;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditProduct extends Component
{
    use WithFileUploads;
    public $cats;
    public $product;

    public $title;
    public $cat;
    public $keyword;
    public $qty;
    public $buy_price;
    public $sell_price;
    public $whole_price;
    public $image;
    public $file;
    public $desc;

    protected $rules= [
        'title'=>'required|min:3',
        'cat'=>'required',
        'qty'=>'required|integer',
        'buy_price'=>'required|integer',
        'sell_price'=>'required|integer',
//        'whole_price'=>'required|integer',
        'desc'=>'required',
    ];

    protected $messages = [
        'title.required' => 'نام محصول نمیتواند خالی باشد',
        'title.min' => 'نام محصول نباید کمتر از 3 حرف باشد',
        'cat.required' => 'گروه کالا نمیتواند خالی باشد',
        'qty.required' => 'تعداد محصول را وارد کنید',
        'qty.integer' => 'تعداد محصول را به صورت عدد وارد کنید',
        'buy_price.required' => 'قیمت خرید را وارد کنید',
        'buy_price.integer' => 'قیمت خرید را  به صورت عدد وارد کنید',
        'sell_price.required' => 'قیمت فروش را وارد کنید',
        'sell_price.integer' => 'قیمت فروش را  به صورت عدد وارد کنید',
        'whole_price.required' => 'قیمت فروش عمده را وارد کنید',
        'whole_price.integer' => 'قیمت فروش عمده را  به صورت عدد وارد کنید',
        'desc.required' => 'توضیحات محصول را وارد کنید',
    ];

    public function mount()
    {
        $this->title=$this->product->name;
        $this->cat=$this->product->cat_id;
        $this->keyword=$this->product->meta_keywords;
        $this->qty=$this->product->quantity;
        $this->buy_price=$this->product->buy_price;
        $this->sell_price=$this->product->sell_price;
        $this->whole_price=$this->product->whole_price;
        $this->desc=$this->product->description;
    }

    public function updated($title)
    {
        $this->validateOnly($title);
    }

    public function update_product()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {

            $product = Product::find($this->product->id);
            $cat = Category::where('id', $this->cat)->first();
            $product->cat_id = $cat->id;
            $product->name = $this->title;
            $product->sell_price = $this->sell_price;
            $product->buy_price = $this->buy_price;
            $product->whole_price = $this->whole_price;
            $product->quantity = $this->qty;
            $product->description = $this->desc;
            $product->meta_keywords = $this->keyword;
            if ($this->image!=null) {
                if ($product->photo_id != null) {
                    $old = Photo::where('id', $product->photo_id)->first();
                    unlink(public_path() . '/images/products/' . $old->path);
                    $old->delete();
                }
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/products';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $product->id;
                $photo->groupable_type = Product::class;
                $photo->save();
                $product->photo_id = $photo->id;
            }
            if ($this->file!=null) {
                if ($product->file_id != null) {
                    $old = File::where('id', $product->file_id)->first();
                    unlink(public_path() . '/files/' . $old->path);
                    $old->delete();
                }
                $name = time() . $this->file->getClientOriginalName();
                $dir='files/products';
                $this->image->storeAS($dir,$name);
                $fileRecord = new File();
                $fileRecord->name = $this->file->getClientOriginalName();
                $fileRecord->path = $name;
                $fileRecord->user_id = Auth::id();
                $fileRecord->save();
                $product->file_id = $fileRecord->id;
            }
            $product->save();
           Session::put('success','تغییرات ثبت شد.');
            return redirect(route('product.index'));
        } else {
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.products.edit-product');
    }
}
