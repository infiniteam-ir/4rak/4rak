<?php

namespace App\Http\Livewire\Products;

use App\Account;
use App\Category;
use App\Guaranty;
use App\Input_Factor;
use App\Input_Factor_Pro;
use App\Product;
use App\Provider;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AddProduct extends Component
{
    public $cats;
    public $factor;

    public $title;
    public $f_num;
    public $total_price;
    public $f_date;
    public $cat;
    public $provider_co;
    public $qty;
    public $buy_price;
    public $sell_price;
    public $whole_price;
    public $guaranty;
    public $duration;
    public $duration_unit;
    public $guaranty_start;
    public $keyword;
    public $barcode;
    public $desc;

    protected $rules= [
        'f_num'=>'required|integer',
        'f_date'=>'required|date',
        'title'=>'required|min:3',
        'cat'=>'required',
        'provider_co'=>'required',
        'qty'=>'required|integer',
        'buy_price'=>'required|integer',
        'sell_price'=>'required|integer',
        'whole_price'=>'required|integer',
        'barcode'=>'unique:products',
    ];



    protected $messages = [
        'f_num.required' => 'شماره فاکتور را وارد کنید',
        'f_date.required' => 'تاریخ فاکتور را وارد کنید',
        'f_num.integer' => 'شماره فاکتور را  به صورت عدد وارد کنید',
        'f_date.date' => 'شماره فاکتور را  به صورت صحیح وارد کنید',
        'title.min' => 'نام کالا نباید کمتر از 3 حرف باشد',
        'title.required' => 'نام کالا را وارد کنید',
        'cat.required' => 'گروه کالا نمیتواند خالی باشد',
        'provider_co.required' => 'نام تامین کندده این کالا را وارد کنید',
        'qty.required' => 'تعداد محصول را وارد کنید',
        'qty.integer' => 'تعداد محصول را به صورت عدد وارد کنید',
        'buy_price.required' => 'قیمت خرید را وارد کنید',
        'buy_price.integer' => 'قیمت خرید را  به صورت عدد وارد کنید',
        'sell_price.required' => 'قیمت فروش را وارد کنید',
        'sell_price.integer' => 'قیمت فروش را  به صورت عدد وارد کنید',
        'whole_price.required' => 'قیمت فروش عمده را وارد کنید',
        'whole_price.integer' => 'قیمت فروش عمده را  به صورت عدد وارد کنید',
        'barcode.unique' => 'بارکد تکراری است',
    ];

    public function updated($f_num,$value)
    {
        $this->validateOnly($f_num);
    }

    public function add_pro()
    {
        $this->validate();

        $product = Product::where([['shop_id', Auth::user()->shop_id], ['name', $this->title]])->first();
        $factor = Input_Factor::where('f_num', $this->f_num)->with('provider')->first();
        $provider = Provider::where([['belong_to', Auth::user()->shop_id], ['id', $this->provider_co]])->first();
        if ($provider == null) {
            $provider = new Provider();
            $provider->belong_to = Auth::user()->shop_id;
            $provider->name = $this->provider_co;
            $provider->address = '-';
            $provider->save();
            $providerAccount = new Account();
            $providerAccount->name = $provider->name;
            $providerAccount->owner_type = Provider::class;
            $providerAccount->owner_id = $provider->id;
            $providerAccount->amount = 0;
            $providerAccount->shop_id = Auth::user()->shop_id;
            $providerAccount->save();
        }

        if ($factor == null) {
            $factor = new Input_Factor();
            $factor->shop_id = Auth::user()->shop_id;
            $factor->f_num = $this->f_num;
            $factor->type = "offline";
            $factor->provider_id = $provider->id;
            $factor->total_price = 0;
            if (strpos($this->f_date, '/') !== false) {
                $orderdate = explode('/', $this->f_date);
                function engNum($string)
                {
                    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                    $output = str_replace($persian, $english, $string);
                    return $output;
                }

                $month = (int)engNum($orderdate[1]);
                $day = (int)engNum($orderdate[2]);
                $year = (int)engNum($orderdate[0]);
                $jalaliDate = Verta::getGregorian($year, $month, $day);
                $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
            } else {
                $milDate = Carbon::instance(Verta::parse($this->f_date)->datetime());
            }
            $factor_date = $milDate;
            $factor->f_date = $factor_date;
            $factor->save();

            if ($product == null) {
                $product = new Product();
                $product->shop_id = Auth::user()->shop_id;
                $product->user_id = Auth::id();
                $cat = Category::where('title', $this->cat)->first();
                $product->cat_id = $cat->id;
                $product->name = $this->title;
                $product->slug = make_slug($this->title);
                $product->status = 'تایید نشده';
                $product->provider_co = $provider->name . " " . $provider->family;
                $product->sell_price = $this->sell_price;
                $product->buy_price = $this->buy_price;
                $product->whole_price = $this->whole_price;
                $product->quantity = $this->qty;
                $product->description = $this->description;
                $product->meta_keywords = $this->keywords;
                $product->barcode = $this->barcode;
            } else {
                $product->quantity += $this->qty;
            }
            $product->save();
            if ($this->guaranty == "true") {
                $guaranty = new Guaranty();
                $guaranty->product_id = $product->id;
                $guaranty->duration = $this->guaranty_duration;
                $guaranty->duration_unit = $this->guaranty_unit;
                $guaranty->start = $this->guaranty_start;
                $guaranty->save();
            }

            $factor->total_price += $product->buy_price * $product->quantity;
            $factor->save();
            $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
            $providerAccount->amount += $factor->total_price;
            $providerAccount->save();

            $factor_pro = new Input_Factor_Pro();
            $factor_pro->factor_id = $factor->id;
            $factor_pro->shop_id = Auth::user()->shop_id;
            $factor_pro->product_id = $product->id;
            $factor_pro->qty = $this->qty;
            $factor_pro->price = $product->buy_price;
            $factor_pro->save();

        } else {
            if ($product == null) {
                $product = new Product();
                $product->shop_id = Auth::user()->shop_id;
                $product->user_id = Auth::id();
                $cat = Category::where('title', $this->cat)->first();
                $product->cat_id = $cat->id;
                $product->name = $this->title;
                $product->slug = make_slug($this->title);
                $product->status = 'تایید نشده';
                $product->provider_co = $provider->name . " " . $provider->family;
                $product->sell_price = $this->sell_price;
                $product->buy_price = $this->buy_price;
                $product->whole_price = $this->whole_price;
                $product->quantity = $this->qty;
                $product->description = $this->description;
                $product->meta_keywords = $this->keywords;
                $product->barcode = $this->barcode;
            } else {
                $product->quantity += $this->qty;
            }
            $product->save();

            if ($this->guaranty == "true") {
                $guaranty = new Guaranty();
                $guaranty->product_id = $product->id;
                $guaranty->duration = $this->guaranty_duration;
                $guaranty->duration_unit = $this->guaranty_unit;
                $guaranty->start = $this->guaranty_start;
                $guaranty->save();
                $product->guaranty_id = $guaranty->id;
                $product->save();
            }

            $factor->total_price += $product->buy_price * $product->quantity;
            $factor->save();

            $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
            $providerAccount->amount += $factor->total_price;
            $providerAccount->save();

            $factor_pro = new Input_Factor_Pro();
            $factor_pro->factor_id = $factor->id;
            $factor_pro->shop_id = Auth::user()->shop_id;
            $factor_pro->product_id = $product->id;
            $factor_pro->qty = $product->quantity;
            $factor_pro->price = $product->buy_price;
            $factor_pro->save();
        }
        return response()->json(['product' => $product, 'providerId' => $provider->id, 'factorId' => $factor->f_num]);
    }


    public function render()
    {
        return view('livewire.products.add-product');
    }
}
