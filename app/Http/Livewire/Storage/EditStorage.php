<?php

namespace App\Http\Livewire\Storage;

use App\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use function Symfony\Component\Translation\t;

class EditStorage extends Component
{
    public $storage;
    public $name;
    public $address;
    protected $rules=[
        'name'=>'required',
        'address'=>'required'
    ];
    protected $messages=[
        'name.required'=>'لطفا نام انبار را وارد کنید',
        'address.required'=>'لطفا آدرس انبار را وارد کنید'
    ];

    public function mount()
    {
        $this->name=$this->storage->name;
        $this->address=$this->storage->address;
    }

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function edit_storage()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $storage = Storage::where('id', $this->storage->id)->first();
            $storage->name = $this->name;
            $storage->address = $this->address;
            $storage->save();
            Session::put('success', 'تغییرات ثبت شد');
            return redirect(route('storage.index'));
        }else{
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.storage.edit-storage');
    }
}
