<?php

namespace App\Http\Livewire\Storage;

use App\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class SetStorage extends Component
{
    public $name;
    public $address;

    protected $rules=[
        'name'=>'required',
        'address'=>'required'
    ];
    protected $messages=[
        'name.required'=>'لطفا نام انبار را وارد کنید',
        'address.required'=>'لطفا آدرس انبار را وارد کنید'
    ];

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function set_storage()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $storage = new Storage();
            $storage->name = $this->name;
            $storage->address = $this->address;
            $storage->shop_id = Auth::user()->shop_id;
            $storage->user_id = Auth::id();
            $storage->save();
            Session::put('success', 'انبار جدید ثبت شد');
            return redirect(route('storage.index'));
        }else{
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.storage.set-storage');
    }
}
