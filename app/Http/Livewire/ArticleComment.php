<?php

namespace App\Http\Livewire;

use App\Article;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ArticleComment extends Component
{
    public $post;
    public $content;
    public $positives;
    public $negatives;
    public $post_id=null;

    protected $listeners = [
        'destroyComment' => 'destroy',
        'publishComment' => 'publish',
    ];

    public function mount($post_id=null)
    {
        $this->post_id = $post_id;
    }

    protected $rules = [
        'content' => 'required|min:5',
    ];
    protected $messages = [
        'content.required' => 'لطفا متن دیدگاه را بنویسید',
        'content.min' => 'متن دیدگاه نباید کمتر از 5 حرف باشد',
    ];


    public function updated($content)
    {
        $this->validateOnly($content);
    }

    public function save_comment()
    {
        $this->validate();
        $comment = new \App\Comment();
        $article=Article::find($this->post_id);
        $comment->shop_id=$article->shop_id;
        $comment->groupable_type = Article::class;
        $comment->groupable_id = $this->post_id;
        $comment->user_id = Auth::id();
        $comment->content = $this->content;
        if ($this->positives != null)
            $comment->positive_points = $this->positives;
        if ($this->negatives != null)
            $comment->weak_points = $this->negatives;
        $comment->save();
        $this->reset();
        $this->dispatchBrowserEvent('success');
        return redirect(request()->header('Referer'));
    }

    public function render()
    {
        return view('livewire.article-comment');
    }
}
