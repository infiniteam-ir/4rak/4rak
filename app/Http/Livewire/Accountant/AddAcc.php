<?php

namespace App\Http\Livewire\Accountant;

use App\Account;
use App\BankAccInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use function Symfony\Component\Translation\t;

class AddAcc extends Component
{
    public $banks;
    public $name;
    public $bank;
    public $owner_name;
    public $owner_family;
    public $acc_type;
    public $amount;
    public $type;
    public $cheque_bank;

    protected $rules=[
        'type'=>'required|integer',
        'name'=>'required',
        'amount'=>'required_if:type,==,1,2',
        'owner_name'=>'required_if:type,==,2',
        'bank'=>'required_if:type,==,2',
        'owner_family'=>'required_if:type,==,2',
        'acc_type'=>'required_if:type,==,2',

    ];
    protected $messages=[
        'name.required'=>'نام حساب را وارد کنید',
        'bank.required'=>'نام بانک را انتخاب کنید',
        'owner_name.required'=>'نام صاحب حساب را وارد کنید',
        'owner_family.required'=>'نام خانوادگی صاحب حساب را وارد کنید',
        'acc_type.required'=>'نوع حساب را انتخاب کنید',
        'amount.integer'=>'موجودی حساب را به صورت عدد وارد کنید',
        'amount.required'=>'موجودی حساب را وارد کنید',
        'type.integer'=>'نوع حساب را انتخاب کنید',
        'type.required'=>'نوع حساب را انتخاب کنید',
    ];

    public function updated($type)
    {
        $this->validateOnly($type);
     }

    public function add_acc()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $account = new Account();
            $account->shop_id = Auth::user()->shop_id;
            $account->name = $this->name;
            $account->owner_type = 'self';
            $account->type_id=$this->type;
            if ($this->type=='3'){
                $bank=Account::find($this->cheque_bank);
                $account->owner_id=$bank->id;
                $account->amount=$bank->amount;
                $account->save();
            }else{
                $account->amount = $this->amount;
            }
            $account->save();
            if ($this->type=='2'){
                $bank_info=new BankAccInfo();
                $bank_info->account_id=$account->id;
                $bank_info->owner_name=$this->owner_name;
                $bank_info->owner_family=$this->owner_family;
                $bank_info->acc_type=$this->acc_type;
                $bank_info->bank=$this->bank;
                $bank_info->save();
            }

            Session::put('success','حساب جدید ایجاد شد.');
            return redirect(route('accounts'));
        } else {
            return 403;
        }
    }


    public function render()
    {
        return view('livewire.accountant.add-acc');
    }
}
