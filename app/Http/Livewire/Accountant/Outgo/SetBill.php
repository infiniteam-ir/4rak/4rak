<?php

namespace App\Http\Livewire\Accountant\Outgo;

use App\Account;
use App\Outgo;
use App\Outgo_Bill;
use App\Outgo_Charak_Service;
use App\Outgo_Other;
use App\Outgo_Salary;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SetBill extends Component
{
    public $accounts;

    public $name;
    public $price;
    public $bill_id;
    public $payment_code;
    public $tracking_id;
    public $source;

    protected $rules=[
        'price'=>'required|integer',
        'name'=>'required',
        'bill_id'=>'required|integer',
        'payment_code'=>'required|integer',
        'tracking_id'=>'required|integer',
        'source'=>'required|integer',
    ];
    protected $messages=[
        'name.required'=>'نام قبض را وارد کنید',
        'price.required'=>'مقدار قبض را وارد کنید',
        'price.integer'=>'مقدار قبض را به صورت عدد وارد کنید',
        'bill_id.required'=>'شناسه قبض را وارد کنید',
        'bill_id.integer'=>'شناسه قبض را به صورت عدد وارد کنید',
        'payment_code.required'=>'شناه پرداخت را وارد کنید',
        'payment_code.integer'=>'شناسه پرداخت را به صورت عدد وارد کنید',
        'acc_type.required'=>'نوع حساب را انتخاب کنید',
        'tracking_id.integer'=>'کد پیگیری را به صورت عدد وارد کنید',
        'tracking_id.required'=>'کد پیگیری را وارد کنید',
        'source.required'=>' حساب را انتخاب کنید',
        'source.integer'=>' حساب را انتخاب کنید',
    ];

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function set_bill()
    {
        $this->validate();

        $outgo=new Outgo();
        $outgo->shop_id=Auth::user()->shop_id;
        $outgo->user_id=Auth::id();
        $outgo->source_id=$this->source;
        $outgo->source_type=Account::class;
        $outgo->price=$this->price;

        $bill=new Outgo_Bill();
        $bill->price=$this->price;
        $bill->name=$this->name;
        $bill->bill_id=$this->bill_id;
        $bill->payment_code=$this->payment_code;
        $bill->tracking_id=$this->tracking_id;
        $bill->save();

        $outgo->type='offline';
        $outgo->target_type=Outgo_Bill::class;
        $outgo->target_id=$bill->id;
        $outgo->save();

        $account=Account::where([['shop_id',Auth::user()->shop_id],['owner_type','self'],['id',$this->source]])->first();
        $account->amount-=$this->price;
        $account->save();
        return redirect(route('offlineAcc'));
    }

    public function render()
    {
        return view('livewire.accountant.outgo.set-bill');
    }
}
