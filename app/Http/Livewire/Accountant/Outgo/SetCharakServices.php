<?php

namespace App\Http\Livewire\Accountant\Outgo;

use App\Account;
use App\Outgo;
use App\Outgo_Charak_Service;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SetCharakServices extends Component
{
    public $accounts;

    public $name;
    public $price;
    public $tracking_id;
    public $source;

    protected $rules=[
        'price'=>'required|integer',
        'name'=>'required',
        'tracking_id'=>'required|integer',
        'source'=>'required|integer',
    ];
    protected $messages=[
        'name.required'=>'نام سرویس را وارد کنید',
        'price.required'=>'مقدار  را وارد کنید',
        'price.integer'=>'مقدار  را به صورت عدد وارد کنید',
        'tracking_id.integer'=>'کد پیگیری را به صورت عدد وارد کنید',
        'tracking_id.required'=>'کد پیگیری را وارد کنید',
        'source.required'=>' حساب را انتخاب کنید',
        'source.integer'=>' حساب را انتخاب کنید',
    ];
    public function updated($name)
    {
        $this->validateOnly($name);
    }


    public function set_charak_services()
    {
        $this->validate();
        $outgo=new Outgo();
        $outgo->shop_id=Auth::user()->shop_id;
        $outgo->user_id=Auth::id();
        $outgo->source_id=$this->source;
        $outgo->price=$this->price;

        $charak=new Outgo_Charak_Service();
        $charak->price=$this->price;
        $charak->target=$this->name;
        $charak->tracking_id=$this->tracking_id;
        $charak->save();

        $outgo->target_type=Outgo_Charak_Service::class;
        $outgo->target_id=$charak->id;
        $outgo->type='online';
        $outgo->source_type=Account::class;
        $outgo->save();

        $account=Account::where([['shop_id',Auth::user()->shop_id],['owner_type','self'],['id',$this->source]])->first();
        $account->amount-=$this->price;
        $account->save();
        return redirect(route('offlineAcc'));
    }



    public function render()
    {
        return view('livewire.accountant.outgo.set-charak-services');
    }
}
