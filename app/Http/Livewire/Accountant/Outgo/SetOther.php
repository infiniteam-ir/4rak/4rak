<?php

namespace App\Http\Livewire\Accountant\Outgo;

use App\Account;
use App\Outgo;
use App\Outgo_Charak_Service;
use App\Outgo_Other;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SetOther extends Component
{
    public $accounts;

    public $name;
    public $price;
    public $tracking_id;
    public $source;

    protected $rules=[
        'price'=>'required|integer',
        'name'=>'required',
        'tracking_id'=>'required|integer',
        'source'=>'required|integer',
    ];
    protected $messages=[
        'name.required'=>'نام هزینه را وارد کنید',
        'price.required'=>'مقدار هزینه را وارد کنید',
        'price.integer'=>'مقدار هزینه را به صورت عدد وارد کنید',
        'tracking_id.integer'=>'کد پیگیری را به صورت عدد وارد کنید',
        'tracking_id.required'=>'کد پیگیری را وارد کنید',
        'source.required'=>' حساب را انتخاب کنید',
        'source.integer'=>' حساب را انتخاب کنید',
    ];
    public function updated($name)
    {
        $this->validateOnly($name);
    }


    public function set_other()
    {
        $this->validate();
        $outgo=new Outgo();
        $outgo->shop_id=Auth::user()->shop_id;
        $outgo->user_id=Auth::id();
        $outgo->source_id=$this->source;
        $outgo->price=$this->price;

        $other=new Outgo_Other();
        $other->price=$this->price;
        $other->target=$this->name;
        if ($this->tracking_id)
            $other->tracking_id=$this->tracking_id;
        $other->save();
        $outgo->target_type=Outgo_Other::class;
        $outgo->target_id=$other->id;
        $outgo->type='offline';
        $outgo->source_type=Account::class;
        $outgo->save();

        $account=Account::where([['shop_id',Auth::user()->shop_id],['owner_type','self'],['id',$this->source]])->first();
        $account->amount-=$this->price;
        $account->save();
        return redirect(route('offlineAcc'));
    }

    public function render()
    {
        return view('livewire.accountant.outgo.set-other');
    }
}
