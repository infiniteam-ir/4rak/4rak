<?php

namespace App\Http\Livewire\Accountant\Outgo;

use App\Account;
use App\Outgo;
use App\Outgo_Bill;
use App\Outgo_Salary;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SetSalary extends Component
{
    public $accounts;
    public $users;

    public $user;
    public $price;
    public $start;
    public $end;
    public $calc_method;
    public $total_work;
    public $tracking_id;
    public $source;

    protected $rules=[
        'price'=>'required|integer',
        'user'=>'required|integer',
        'start'=>'required',
        'end'=>'required',
        'calc_method'=>'required',
        'total_work'=>'required|integer',
        'source'=>'required|integer',
    ];
    protected $messages=[
        'user.required'=>'نام کارمند را انتخاب کنید',
        'user.integer'=>'نام کارمند را انتخاب کنید',
        'price.required'=>'مقدار حقوق را وارد کنید',
        'price.integer'=>'مقدار حقوق را به صورت عدد وارد کنید',
        'start.required'=>'تاریخ شروع را وارد کنید',
        'end.required'=>'تاریخ پایان را وارد کنید',
        'calc_method.required'=>'نوع محاسبه را انتخاب کنید',
        'total_work.required'=>'میزان کارکرد را وارد کنید',
        'total_work.integer'=>'میزان کارکرد را به صورت عدد وارد کنید',
        'source.required'=>' حساب را انتخاب کنید',
        'source.integer'=>' حساب را انتخاب کنید',
    ];

    public function updated($price)
    {
        $this->validateOnly($price);
    }

    public function set_salary()
    {
        $this->validate();

        $outgo=new Outgo();
        $outgo->shop_id=Auth::user()->shop_id;
        $outgo->user_id=Auth::id();
        $outgo->source_id=$this->source;
        $outgo->source_type=Account::class;
        $outgo->price=$this->price;

        $salary=new Outgo_Salary();
        $salary->employee_id=$this->user;
        $salary->price=$this->price;
        $salary->calc_method=$this->calc_method;
        $salary->total_work=$this->total_work;
        if ($this->tracking_id!=null)
            $salary->tracking_id=$this->tracking_id;

        if (strpos($this->start, '/') !== false) {
            $orderdate = explode('/', $this->start);
            function engNum($string)
            {
                $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                $output = str_replace($persian, $english, $string);
                return $output;
            }
            $month = (int)engNum($orderdate[1]);
            $day = (int)engNum($orderdate[2]);
            $year = (int)engNum($orderdate[0]);
            $jalaliDate = Verta::getGregorian($year, $month, $day);
            $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
        } else {
            $milDate = Carbon::instance(Verta::parse($this->start)->datetime());
        }
        $salary->start=$milDate;
        $milDate=null;
        if (strpos($this->end, '/') !== false) {
            $orderdate = explode('/', $this->end);
            function engNum1($string)
            {
                $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                $output = str_replace($persian, $english, $string);
                return $output;
            }
            $month = (int)engNum1($orderdate[1]);
            $day = (int)engNum1($orderdate[2]);
            $year = (int)engNum1($orderdate[0]);
            $jalaliDate = Verta::getGregorian($year, $month, $day);
            $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
        } else {
            $milDate = Carbon::instance(Verta::parse($this->end)->datetime());
        }
        $salary->end=$milDate;
        $salary->save();
        $outgo->type='offline';
        $outgo->target_type=Outgo_Salary::class;
        $outgo->target_id=$salary->id;
        $outgo->save();
        return redirect(route('offlineAcc'));
    }

    public function render()
    {
        return view('livewire.accountant.outgo.set-salary');
    }
}
