<?php

namespace App\Http\Livewire\Services;

use App\Photo;
use App\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditService extends Component
{
    use WithFileUploads;
    public $service;
    public $title;
    public $type;
    public $price;
    public $call;
    public $desc;
    public $image;
    protected $rules=[
        'title'=>'required',
        'type'=>'required',
        'desc'=>'required',
        'price'=>'required|integer',
        'image'=>'nullable|image|max:10240',
    ];
    protected $messages=[
        'title.required'=>'عنوان خدمت را وارد کنید',
        'type.required'=>'نوع خدمت را وارد کنید',
        'price.required'=>'هزینه خدمت را وارد کنید',
        'desc.required'=>'توضیح مختصری در مورد خدمت بنویسید',
        'price.integer'=>'هزینه را به صورت عدد وارد کنید',
        'image.image'=>'نوع فایل غیر مجاز است',
        'image.max'=>'حجم تصویر نباید از 10 مگابایت بیشتر باشد',
    ];

    public function mount()
    {
        $this->title=$this->service->title;
        $this->type=$this->service->type;
        $this->price=$this->service->price;
        $this->call=$this->service->call;
        $this->desc=$this->service->description;
    }

    public function updated($title, $value)
    {
        $this->validateOnly($title);
    }

    public function edit_service()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $service=Service::where([['shop_id',Auth::user()->shop_id],['id',$this->service->id]])->first();
            $service->title=$this->title;
            $service->type=$this->type;
            $service->price=$this->price;
            $service->call=$this->call;
            $service->description=$this->desc;
            if ($this->image!=null){
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/services';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $service->id;
                $photo->groupable_type = Service::class;
                $photo->save();
                $service->photo_id = $photo->id;
            }
            $service->save();
            Session::put('success','تغییرات ذخیره شد');
            return redirect(route('service.index'));
        }else{
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.services.edit-service');
    }
}
