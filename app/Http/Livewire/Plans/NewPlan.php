<?php

namespace App\Http\Livewire\Plans;

use App\Delivery_method;
use App\Photo;
use App\plan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class NewPlan extends Component
{
    public $name;
    public $monthly;
    public $three_month;
    public $six_month;
    public $yearly;
    public $wage;

    protected $rules = [
        'name' => 'required',
        'monthly' => 'required|integer',
        'yearly' => 'required|integer',
        'three_month' => 'required|integer',
        'six_month' => 'required|integer',
        'wage' => 'required',
    ];
    protected $messages = [
        'title.required' => 'لطفا عنوان را وارد کنید',
        'monthly.required' => 'لطفا هزینه  ماهانه را وارد کنید',
        'monthly.integer' => 'لطفا هزینه ماهانه را به صورت عدد وارد کنید',
        'three_month.required' => 'لطفا هزینه  سه ماهه را وارد کنید',
        'three_month.integer' => 'لطفا هزینه سه ماهه را به صورت عدد وارد کنید',
        'six_month.required' => 'لطفا هزینه  شش ماهه را وارد کنید',
        'six_month.integer' => 'لطفا هزینه شش ماهه را به صورت عدد وارد کنید',
        'wage.required' => 'لطفا درصد کارمزد را وارد کنید',

    ];

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function new_plan()
    {
        $this->validate();
        if (Auth::user()->role_id == 1) {
            $plan = new plan();
            $plan->name = $this->name;
            $plan->monthlyPrice = $this->monthly;
            $plan->threeMonthPrice = $this->three_month;
            $plan->sixMonthPrice = $this->six_month;
            $plan->yearlyPrice = $this->yearly;
            $plan->wagePercent = $this->wage;
            $plan->save();
            Session::put('success', 'پلن پرداختی ذخیره شد');
            return $this->redirect(route('plans'));
        }
    }

    public function render()
    {
        return view('livewire.plans.new-plan');
    }
}
