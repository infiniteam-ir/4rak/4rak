<?php

namespace App\Http\Livewire\Profile;

use App\Photo;
use App\User;
use App\UserInfo;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditProfile extends Component
{
    use WithFileUploads;

    public $provinces;
    public $user;

    public $name;
    public $family;
    public $mobile;
    public $email;
    public $province;
    public $city;
    public $tell;
    public $address;
    public $avatar;
    public $current_mobile;

    protected $rules=[
        'name'=>'required',
        'family'=>'required',
        'mobile'=>'required',
        'email'=>'required|email',
        'province'=>'required',
        'city'=>'required',
        'tell'=>'required',
        'address'=>'required',
        'avatar'=>'nullable|image|max:10240',
    ];
    protected $messages=[
        'name.required'=>'لطفا نام را وارد کنید',
        'family.required'=>'لطفا نام خانوادگی را وارد کنید',
        'mobile.required'=>'لطفا شماره موبایل را وارد کنید',
        'mobile.integer'=>'لطفا شماره موبایل را به صورت عدد  وارد کنید',
        'email.required'=>'لطفا ایمیل را وارد کنید',
        'email.email'=>'لطفا ایمیل صحیح وارد کنید',
        'province.required'=>'لطفا استان را انتخاب کنید',
        'city.required'=>'لطفا شهر را انتخاب کنید',
        'tell.required'=>'لطفا تلفن را وارد کنید',
        'tell.integer'=>'لطفا تلفن را به صورت عدد وارد کنید',
        'address.required'=>'لطفا آدرس را وارد کنید',
        'avatar.image'=>'فرمت فایل انتخاب شده صحیح نیست',
        'avatar.max'=>'حجم تصویر نباید بیشتر از 10 مگابایت باشد',
    ];

    public function mount()
    {
        $this->name=$this->user->name;
        $this->family=$this->user->family;
        $this->mobile=$this->user->mobile;
        $this->email=$this->user->email;
        $current_mobile=$this->user->mobile;
        if($this->user->info){
            $this->province=$this->user->info->province_id;
            $this->city=$this->user->info->city_id;
            $this->tell=$this->user->info->phone;
            $this->address=$this->user->info->address;
        }
    }

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function edit_profile()
    {
        $this->validate();

        $user=User::where('id',$this->user->id)->first();
        $user->name=$this->name;
        $user->family=$this->family;
        $user->email=$this->email;
        $user->mobile=$this->mobile;
        if ($avatar=$this->avatar){
            $name=time() . $avatar->getClientOriginalName();
            $dir='images/profile';
            $this->avatar->storeAS($dir,$name);
            $photo=new Photo();
            $photo->name=$avatar->getClientOriginalName();
            $photo->path=$name;
            $photo->groupable_type=User::class;
            $photo->groupable_id=$user->id;
            $photo->save();
            $user->photo_id=$photo->id;
        }

        $user->save();

        if ($user->user_info_id==null){
            $info=new UserInfo();
            $info->province_id=$this->province;
            $info->city_id=$this->city;
            $info->phone=$this->tell;
            $info->address=$this->address;
            $info->user_id=$this->user->id;
            $info->save();
            $user->user_info_id=$info->id;
            $user->save();
        }else{
            $info=UserInfo::where('user_id',$this->user->id)->first();
            $info->province_id=$this->province;
            $info->city_id=$this->city;
            $info->phone=$this->tell;
            $info->address=$this->address;
            $info->user_id=$this->user->id;
            $info->save();
        }

        if ($user->mobile != $this->current_mobile){
            $user->mobile_verified_at=null;
            $user->save();
        }
        return redirect(route('Profile.index'));
    }
    public function render()
    {
        return view('livewire.profile.edit-profile');
    }
}
