<?php

namespace App\Http\Livewire\DeliveryMethods;

use App\Category;
use App\Delivery_method;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class SetMethod extends Component
{
    use WithFileUploads;

    public $title;
    public $price;
    public $photo;
    public $desc;

    protected $rules = [
        'title' => 'required',
        'price' => 'required|integer',
        'photo' => 'nullable|image|max:10240',
        'desc' => 'required',
    ];
    protected $messages = [
        'title.required' => 'لطفا عنوان را وارد کنید',
        'price.required' => 'لطفا هزینه را وارد کنید',
        'price.integer' => 'لطفا هزینه را به صورت عدد وارد کنید',
        'desc.required' => 'لطفا توضیحات را وارد کنید',
        'photo.image' => 'فرمت تصویر صحیح نیست',
        'photo.max' => 'حجم تصویر نباید بیشتر از 10 مگابایت باشد',

    ];
    public function updated($title)
    {
        $this->validateOnly($title);
    }

    public function set_method()
    {
        $this->validate();

        if (Auth::user()->role_id==1){
            $method=new Delivery_method();
            $method->method=$this->title;
            $method->price=$this->price;
            $method->description=$this->desc;
            $method->save();
            if ($this->photo!=null){
                $name = time() . $this->photo->getClientOriginalName();
                $dir='images/delivery';
                $this->photo->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->photo->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $method->id;
                $photo->groupable_type = Delivery_method::class;
                $photo->save();
                $method->photo_id = $photo->id;
                $method->save();
            }
            Session::put('success','روش ارسال ذخیره شد');
            return $this->redirect(route('delivery_methods'));
        }
    }

    public function render()
    {
        return view('livewire.delivery-methods.set-method');
    }
}
