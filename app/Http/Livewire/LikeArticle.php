<?php

namespace App\Http\Livewire;

use App\Article;
use Livewire\Component;

class LikeArticle extends Component
{
    public $article;

    public function like($id)
    {
        $article=Article::find($id);
        $article->increment('rate');
    }
    public function render()
    {
        return view('livewire.like-article');
    }
}
