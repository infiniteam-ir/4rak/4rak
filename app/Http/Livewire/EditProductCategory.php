<?php

namespace App\Http\Livewire;

use App\Category;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditProductCategory extends Component
{
    use WithFileUploads;
    public $cat;
    public $cats;

    public $title;
    public $image;
    public $parent;

    public function mount()
    {
        $this->title=$this->cat->title;
        $this->parent=$this->cat->parent_id;
    }

    protected $rules= [
        'title'=>'required|min:2',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان گروه کالا را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
    ];

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function update_cat()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'ادمین') {

            $cat = Category::find($this->cat->id);
            $cat->title =$this->title;
            if ($this->image!=null) {
                if ($cat->photo_id != null) {
                    $old = Photo::where('id', $cat->photo_id)->first();
                    unlink(public_path() . '/images/cats/' . $old->path);
                    $old->delete();
                }
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/cats';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $cat->id;
                $photo->groupable_type = Category::class;
                $photo->save();
                $cat->photo_id = $photo->id;
            }
            if ($this->parent != 0) {
                $parent = Category::where('id', $this->parent)->first();
                $cat->parent_id = $parent->id;
            }
            $cat->save();
            Session::put('success','تغییرات ثبت شد');
            return redirect(route('pro-categories'));
        } else {
            return 403;
        }
    }
    public function render()
    {
        return view('livewire.edit-product-category');
    }
}
