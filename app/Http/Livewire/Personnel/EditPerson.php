<?php

namespace App\Http\Livewire\Personnel;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class EditPerson extends Component
{
    public $person;

    public $name;
    public $family;
    public $email;
    public $password;
    public $mobile;

    protected $rules = [
        'name' => 'required',
        'family' => 'required',
        'email' => 'required|email',
        'mobile' => 'required',
    ];
    protected $messages = [
        'name.required' => 'لطفا نام کارمند را وارد کنید',
        'family.required' => 'لطفا نام خانوادگی کارمند را وارد کنید',
        'email.required' => 'لطفا ایمیل کارمند را وارد کنید',
        'email.email' => 'لطفا ایمیل را به صورت صحیح  وارد کنید',
        'mobile.required' => 'لطفا موبایل کارمند را وارد کنید',
    ];

    public function mount()
    {
        $this->name = $this->person->name;
        $this->family = $this->person->family;
        $this->mobile = $this->person->mobile;
        $this->email = $this->person->email;
    }

    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function edit_person()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {
            $person = User::find($this->person->id);
            $person->name = $this->name;
            $person->family = $this->family;
            $person->mobile = $this->mobile;
            $person->email = $this->email;
            if ($this->password != null)
                $person->password = Hash::make($this->password);
            $person->save();
            Session::put('success', 'تغییرات ذخیره شد');
            return redirect(route('personnel.index'));
        } else {
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.personnel.edit-person');
    }
}
