<?php

namespace App\Http\Livewire\Personnel;

use App\ShopPermission;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class SetPerson extends Component
{
    public $roles;

    public $name;
    public $family;
    public $role;
    public $email;
    public $password;
    public $mobile;

    protected $rules=[
        'name'=>'required',
        'family'=>'required',
        'role'=>'required',
        'email'=>'required|email',
        'password'=>'required',
        'mobile'=>'required',
    ];
    protected $messages=[
        'name.required'=>'لطفا نام کارمند را وارد کنید',
        'family.required'=>'لطفا نام خانوادگی کارمند را وارد کنید',
        'role.required'=>'لطفا نقش کارمند را وارد کنید',
        'email.required'=>'لطفا ایمیل کارمند را وارد کنید',
        'email.email'=>'لطفا ایمیل را به صورت صحیح  وارد کنید',
        'password.required'=>'لطفا کلمه عبور را وارد کنید',
        'mobile.required'=>'لطفا موبایل کارمند را وارد کنید',
    ];


    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function set_person()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $role = ShopPermission::where([['shop_id', Auth::user()->shop_id], ['role_name', $this->role]])->with('shoprole')->first();
            $user = new User();
            $user->name = $this->name;
            $user->family = $this->family;
            $user->mobile = $this->mobile;
            $user->email = $this->email;
            $user->password = Hash::make($this->password);
            $user->role_id = 3; /* کاربر*/
            $user->shop_id = Auth::user()->shop_id;
            $user->shop_role_id = $role->shoprole->id;
            $user->save();
            Session::put('success', 'کاربر جدید ثبت شد');
            return redirect(route('personnel.index'));
        } else {
            return 403;
        }
    }
    public function render()
    {
        return view('livewire.personnel.set-person');
    }
}
