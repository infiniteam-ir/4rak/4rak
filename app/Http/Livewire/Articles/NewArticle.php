<?php

namespace App\Http\Livewire\Articles;

use App\Article;
use App\Category;
use App\Photo;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;
use function App\Http\Livewire\Products\engNum;

class NewArticle extends Component
{
    use WithFileUploads;
    public $cats;

    public $title;
    public $cat;
    public $image;
    public $content;
    public $keyword;

    protected $rules= [
        'title'=>'required|min:3',
        'cat'=>'required',
        'image'=>'nullable|image|max:20480',
        'content'=>'required|min:5',
    ];



    protected $messages = [
        'title.min' => 'عنوان مطلب نباید کمتر از 3 حرف باشد',
        'content.min' => 'متن  نباید کمتر از 5 حرف باشد',
        'content.required' => 'متن را وارد کنید',
        'title.required' => 'عنوان مطلب را وارد کنید',
        'cat.required' => 'دسته بندی نمیتواند خالی باشد',
        'cat.integer' => 'دسته بندی نمیتواند خالی باشد',
        'image.image' => 'پسوند فایل انتخاب شده صحیح نیست',
        'image.max' => 'حجم تصویر نباید از 20 مگابایت بیشتر باشد',

    ];

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function new_article()
    {
        $this->validate();
        $article=new Article();
        $article->shop_id=Auth::user()->shop_id;
        $article->title=$this->title;
        $article->slug=make_slug($this->title);
        $article->cat_id=$this->cat;
        $article->author_id=Auth::id();
        $article->content=$this->content;
        $article->keywords=$this->keyword;
        $article->save();
        if ($this->image != null) {
            $name = time() . $this->image->getClientOriginalName();
            $dir='images/articles/';
            $this->image->storeAS($dir,$name);
            $photo = new Photo();
            $photo->name = $this->image->getClientOriginalName();
            $photo->path = $name;
            $photo->groupable_id = $article->id;
            $photo->groupable_type = Article::class;
            $photo->save();
            $article->photo_id = $photo->id;
            $article->save();
        }
        Session::put('saved','مطلب جدید اضافه شد.');
        return redirect(route('article.index'));

    }
    public function render()
    {
        return view('livewire.articles.new-article');
    }
}
