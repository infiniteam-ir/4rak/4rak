<?php

namespace App\Http\Livewire\Articles;

use App\Article_category;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditArticleCat extends Component
{
    use WithFileUploads;

    public $cats;
    public $cat;

    public $title;
    public $image;
    public $parent;

    protected $rules= [
        'title'=>'required|min:2',
        'image'=>'nullable|image|max:10240',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان گروه  را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
        'image.image' => 'نوع تصویر مجاز نیست',
        'image.max' => 'حجم تصویر نباید از 10 مگابایت بیشتر باشد.',
    ];

    public function mount()
    {
        $this->title=$this->cat->title;
        $this->parent=$this->cat->parent_id;
    }

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function cat_edit()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $cat = Article_category::find($this->cat->id);
            $cat->title = $this->title;
            $cat->save();
            if ($this->image != null) {
                if ($this->cat->photo_id){
                    $oldpic=Photo::where([['groupable_id',$this->cat->photo_id],['groupable_type',Article_category::class]])->first();
                    unlink(public_path().'images/articles/cats/'.$oldpic->path);
                    $oldpic->delete();
                }

                $name = time() . $this->image->getClientOriginalName();
                $dir='images/articles/cats/';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $cat->id;
                $photo->groupable_type = Article_category::class;
                $photo->save();
                $cat->photo_id = $photo->id;
            }
            if ($this->parent != 0 || $this->parent != null) {
                $parent = Article_category::where('title', $this->parent)->first();
                $cat->parent_id = $parent->id;
            }
            $cat->save();
            Session::put('saved','تغییرات ذخیره شد');
            return redirect(route('article-category'));
        } else {
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.articles.edit-article-cat');
    }
}
