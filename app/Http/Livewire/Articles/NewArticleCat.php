<?php

namespace App\Http\Livewire\Articles;

use App\Article_category;
use App\Category;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewArticleCat extends Component
{
    use WithFileUploads;

    public $cats;

    public $title;
    public $image;
    public $parent;

    protected $rules= [
        'title'=>'required|min:2',
        'image'=>'nullable|image|max:10240',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان گروه  را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
        'image.image' => 'نوع تصویر مجاز نیست',
        'image.max' => 'حجم تصویر نباید از 10 مگابایت بیشتر باشد.',
    ];

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function new_cat()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {
            $cat = new Article_category();
            $cat->title = $this->title;
            $cat->shop_id = Auth::user()->shop_id;
            $cat->save();
            if ($this->image != null) {
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/articles/cats/';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $cat->id;
                $photo->groupable_type = Article_category::class;
                $photo->save();
                $cat->photo_id = $photo->id;
            }
            if ($this->parent != 0 || $this->parent != null) {
                $parent = Article_category::where('title', $this->parent)->first();
                $cat->parent_id = $parent->id;
            }
            $cat->save();
            Session::put('saved','گروه مطلب اضافه شد.');
            return redirect(route('article-category'));
        } else {
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.articles.new-article-cat');
    }
}
