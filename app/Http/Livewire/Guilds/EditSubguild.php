<?php

namespace App\Http\Livewire\Guilds;

use App\Guild;
use App\Photo;
use App\subGuild;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditSubguild extends Component
{
    use WithFileUploads;
    public $subguild;

    public $title;
    public $image;
    public $desc;

    protected $rules= [
        'title'=>'required|min:2',
        'image'=>'nullable|image|max:10240',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان صنف را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
        'image.image' => 'نوع تصویر مجاز نیست',
        'image.max' => 'حجم تصویر نباید از 10 مگابایت بیشتر باشد.',
    ];

    public function mount()
    {
        $this->title=$this->subguild->name;
        $this->desc=$this->subguild->description;

    }

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function edit_subguild()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'ادمین') {
            $subguild = subGuild::find($this->subguild->id);
            $subguild->name = $this->title;
            $subguild->description=$this->desc;
            $subguild->save();
            if ($this->image != null) {
                $oldpic=Photo::find($this->$subguild->photo_id);
                unlink(public_path().'images/guilds/'.$oldpic->path);
                $oldpic->delete();

                $name = time() . $this->image->getClientOriginalName();
                $dir='images/guilds';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $subguild->id;
                $photo->groupable_type = subGuild::class;
                $photo->save();
                $subguild->photo_id = $photo->id;
                $subguild->save();
            }
            Session::put('saved','تغییرات ثبت شد.');
            return redirect(route('guilds-setting'));
        } else {
            return 403;
        }
    }
    public function render()
    {
        return view('livewire.guilds.edit-subguild');
    }
}
