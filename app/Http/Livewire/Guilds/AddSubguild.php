<?php

namespace App\Http\Livewire\Guilds;

use App\Category;
use App\Guild;
use App\Photo;
use App\subGuild;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddSubguild extends Component
{
    use WithFileUploads;
    public $guild;

    public $title;
    public $image;
    public $desc;

    protected $rules= [
        'title'=>'required|min:2',
        'image'=>'nullable|image|max:10240',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان صنف را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
        'image.image' => 'نوع تصویر مجاز نیست',
        'image.max' => 'حجم تصویر نباید از 10 مگابایت بیشتر باشد.',
    ];

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function add_subguild()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'ادمین') {
            $subguild = new subGuild();
            $subguild->name = $this->title;
            $subguild->description=$this->desc;
            $subguild->parent_id=$this->guild->id;
            $subguild->save();
            if ($this->image != null) {
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/guilds';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $subguild->id;
                $photo->groupable_type = subGuild::class;
                $photo->save();
                $subguild->photo_id = $photo->id;
                $subguild->save();
            }

            Session::put('saved','رسته جدید اضافه شد.');
            return redirect(route('guilds-setting'));
        } else {
            return 403;
        }
    }
    public function render()
    {
        return view('livewire.guilds.add-subguild');
    }
}
