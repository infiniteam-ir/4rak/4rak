<?php

namespace App\Http\Livewire\Guilds;

use App\Category;
use App\Guild;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddGuild extends Component
{
    use WithFileUploads;
    public $guilds;

    public $title;
    public $image;
    public $parent;
    public $desc;

    protected $rules= [
        'title'=>'required|min:2',
        'image'=>'nullable|image|max:10240',
    ];

    protected $messages = [
        'title.required' => 'لطفا عنوان صنف را وارد کنید',
        'title.min' => 'عنوان نباید کمتر از 2 حرف باشد',
        'image.image' => 'نوع تصویر مجاز نیست',
        'image.max' => 'حجم تصویر نباید از 10 مگابایت بیشتر باشد.',
    ];

    public function updated($title,$value)
    {
        $this->validateOnly($title);
    }

    public function add_guild()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'ادمین') {
            $guild = new Guild();
            $guild->name = $this->title;
            $guild->description=$this->desc;
            $guild->save();
            if ($this->image != null) {
                $name = time() . $this->image->getClientOriginalName();
                $dir='images/guilds';
                $this->image->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->image->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $guild->id;
                $photo->groupable_type = Guild::class;
                $photo->save();
                $guild->photo_id = $photo->id;
            }
            if ($this->parent != 0 || $this->parent != null) {
                $parent = Guild::where('id', $this->parent)->first();
                $guild->parent_id = $parent->id;
            }
            $guild->save();
            Session::put('saved','صنف جدید اضافه شد.');
            return redirect(route('guilds-setting'));
        } else {
            return 403;
        }
    }
    public function render()
    {
        return view('livewire.guilds.add-guild');
    }
}
