<?php

namespace App\Http\Livewire\Shop;

use App\Shop;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use function Psy\sh;

class InfoEdit extends Component
{
    public $provinces;
    public $cities;
    public $shop;

    public $province;
    public $city;
    public $name;
  /*  public $guild;
    public $guild_id;
    public $sub_guild;
    public $sub_guild_id;*/
    public $address;
    public $unique_name;
    public $description;

    public function mount()
    {
        $this->name=$this->shop->name;
        $this->unique_name=$this->shop->unique_name;
    /*    $this->guild=$this->shop->guild->name;
        $this->guild_id=$this->shop->guild->id;
        $this->sub_guild=$this->shop->subguild->name;
        $this->sub_guild_id=$this->shop->subguild->id;*/
        $this->address=$this->shop->address;
        $this->description=$this->shop->description;
        $this->province=$this->shop->province->id;
        $this->city=$this->shop->city->id;
    }


    protected $rules= [
        'province'=>'required',
        'city'=>'required',
        'name'=>'required',
        'address'=>'required',
        'description'=>'required',
    ];
    protected $messages = [
        'province.required' => 'لطفا استان را انتخاب کنید',
        'city.required' => 'لطفا شهر را انتخاب کنید',
        'name.required' => 'لطفا نام فروشگاهتان را وارد کنید',
//        'guild.required' => 'لطفا صنف را انتخاب کنید',
//        'sub_guild.required' => 'لطفا رسته را انتخاب کنید',
//        'new_sub_guild.required' => 'لطفا نام رسته را وارد کنید',
        'address.required' => 'لطفا آدرس فروشگاهتان را وارد کنید',
        'description.required' => 'لطفا توضیحات مختصری در مورد فروشگاهتان بنویسید.',
    ];

    /* public function mount($arr)
     {
         $this->$arr=$arr;
     }*/
    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function update_info ()
    {
        $this->validate();
        $shopp=Shop::where('id',$this->shop->id)->first();
        $shopp->name = $this->name;
        $shopp->city_id=$this->city;
        $shopp->province_id=$this->province;
        $shopp->address = $this->address;
        $shopp->description = $this->description;
        $shopp->save();
        Session::put('success','تغییرات ثبت شد');
        return redirect(route('shopInfo'));
    }
    public function render()
    {
        return view('livewire.shop.info-edit');
    }
}
