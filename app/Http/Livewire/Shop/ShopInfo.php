<?php

namespace App\Http\Livewire\Shop;

use App\Account;
use App\plan;
use App\Role;
use App\Shop;
use App\ShopPermission;
use App\ShopPlanPivot;
use App\ShopRole;
use App\subGuild;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class ShopInfo extends Component
{
    public $provinces;
    public $guilds;

    public $province;
    public $city;
    public $name;
    public $guild;
    public $sub_guild;
    public $address;
    public $unique_name;
    public $description;


    protected $rules= [
        'province'=>'required',
        'city'=>'required',
        'name'=>'required',
        'unique_name'=>'required|unique:shops',
        'guild'=>'required',
        'sub_guild'=>'required|different:none',
        'address'=>'required',
        'description'=>'required',
    ];
    protected $messages = [
        'province.required' => 'لطفا استان را انتخاب کنید',
        'city.required' => 'لطفا شهر را انتخاب کنید',
        'name.required' => 'لطفا نام فروشگاهتان را وارد کنید',
        'unique_name.required' => 'لطفا نام مخصوص فروشگاهتان را وارد کنید، فروشگاه شما در چارک با این نام شناخته میشود.',
        'unique_name.unique' => 'این نام قبلا انتخاب شده است.',
        'guild.required' => 'لطفا صنف را انتخاب کنید',
        'sub_guild.required' => 'لطفا رسته را انتخاب کنید',
        'sub_guild.different' => 'لطفا رسته را انتخاب کنید',
        'address.required' => 'لطفا آدرس فروشگاهتان را وارد کنید',
        'description.required' => 'لطفا توضیحات مختصری در مورد فروشگاهتان بنویسید.',
    ];

   /* public function mount($arr)
    {
        $this->$arr=$arr;
    }*/
    public function updated($name)
    {
        $this->validateOnly($name);
    }
    public function save_info()
    {
        $this->validate();
        $shop=new Shop();
        $shop->user_id=Auth::id();
        $shop->name=$this->name;
        $shop->province_id=$this->province;
        $shop->city_id=$this->city;
        $shop->subguild_id=$this->sub_guild;
        $shop->guild_id=$this->guild;
        $shop->address=$this->address;
        $shop->unique_name=$this->unique_name;
        $shop->description=$this->description;
        $shop->wallet=0;
        $shop->save();

        /*$plan=plan::find($this->plan);
        $shop_plan=new ShopPlanPivot();
        $shop_plan->shop_id=$shop->id;
        $shop_plan->plan_id=$plan->id;
        $shop_plan->start_date=Carbon::now();
        switch ($this->duration){
            case'monthly'.$plan->id:
                $shop_plan->price=$plan->monthlyPrice;
                $shop_plan->exp_date=Carbon::parse($shop_plan->start_date)->addMonth(1);
                break;
            case'threemonth'.$plan->id:
                $shop_plan->price=$plan->threeMonthPrice;
                $shop_plan->exp_date=Carbon::parse($shop_plan->start_date)->addMonth(3);
                break;
            case'sixmonth'.$plan->id:
                $shop_plan->price=$plan->sixMonthPrice;
                $shop_plan->exp_date=Carbon::parse($shop_plan->start_date)->addMonth(6);
                break;
            case'yearly'.$plan->id:
                $shop_plan->price=$plan->yearlyPrice;
                $shop_plan->exp_date=Carbon::parse($shop_plan->start_date)->addYear(1);
                break;
        }
        $shop_plan->save();
        $shop->pay_plan_id=$shop_plan->id;
        $shop->save()*/;


        $account=New Account();
        $account->shop_id=$shop->id;
        $account->type_id=1;
        $account->name='صندوق';
        $account->owner_type='self';
        $account->owner_id=0;
        $account->amount=0;
        $account->save();
        $user=Auth::user();
        $user->shop_id=$shop->id;

        $shop_permission=new ShopPermission();
        $shop_permission->shop_id=$shop->id;
        $shop_permission->role_name='مالک';
        $shop_permission->products=1;
        $shop_permission->accountant=1;
        $shop_permission->personnel=1;
        $shop_permission->storage=1;
        $shop_permission->buy=1;
        $shop_permission->sell=1;
        $shop_permission->reject=1;
        $shop_permission->guaranty=1;
        $shop_permission->delivery=1;
        $shop_permission->save();

        $shop_role=new ShopRole();
        $shop_role->shop_id=$shop->id;
        $shop_role->shop_permission_id=$shop_permission->id;
        $shop_role->save();

        $user->shop_role_id=$shop_role->id;
        $role=Role::where('name','مالک')->first();
        $user->role_id=$role->id;
        $user->save();
        Session::put('success', 'اطلاعات فروشگاه ثبت شد.');
        return redirect(route('buy_plan'));
    }

    public function render()
    {
        return view('livewire.shop.shop-info');
    }
}
