<?php

namespace App\Http\Livewire;

use App\Product;
use App\Rate;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Comment extends Component
{
    public $content;
    public $positives;
    public $negatives;
    public $rating;
    public $value_per_price;
    public $quality;
    public $pro_id;


    public function mount($pro_id)
    {
        $this->pro_id=$pro_id;
    }

    protected $rules= [
        'content'=>'required|min:5',
        'positives'=>'required',
        'negatives'=>'required',
        'rating'=>'required',
    ];
    protected $messages = [
        'content.required' => 'لطفا متن دیدگاه را بنویسید',
        'content.min' => 'متن دیدگاه نباید کمتر از 5 حرف باشد',
        'positives.required' => 'لطفا نکات مثبت محصول را وارد کنید',
        'negatives.required' => 'لطفا نکات منفی محصول را وارد کنید',
        'rating.required' => 'لطفا امتیاز محصول را انتخاب کنید',
    ];


    public function updated($content)
    {
        $this->validateOnly($content);
    }

    public function save()
    {
        $this->validate();
        $comment=new \App\Comment();
        $comment->groupable_type=Product::class;
        $comment->groupable_id=$this->pro_id;
        $product=Product::find($this->pro_id);
        $comment->shop_id=$product->shop_id;
        $comment->user_id=Auth::id();
        $comment->content=$this->content;
        $comment->positive_points=$this->positives;
        $comment->weak_points=$this->negatives;
        $comment->value_per_price=$this->value_per_price;
        $comment->quality=$this->quality;
        $comment->save();

        $rate=new Rate();
        $rate->user_id=Auth::id();
        $rate->groupable_type=Product::class;
        $rate->groupable_id=$this->pro_id;
        $rate->score=$this->rating;
        $rate->save();

        $pro=Product::find($this->pro_id);
        $rates=Rate::where([['groupable_type',Product::class],['groupable_id',$this->pro_id]])->get();
        $sum=0;
        foreach ($rates as $rate)
            $sum+=$rate->score;
        $pro->rate=$sum/count($rates);
        $pro->save();
        $this->reset();
        $this->dispatchBrowserEvent('success');
        return redirect(request()->header('Referer'));
    }


    public function render()
    {
        return view('livewire.comment');
    }

}
