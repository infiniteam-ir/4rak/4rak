<?php

namespace App\Http\Livewire\Delivery;

use App\Delivery;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;
use function Symfony\Component\Translation\t;

class SetDelivery extends Component
{
    use WithFileUploads;

    public $provinces;
    public $name;
    public $family;
    public $mobile;
    public $province;
    public $city;
    public $avatar;
    public $age;
    public $vehicle;
    public $detail;

    protected $rules = [
        'name' => 'required',
        'family' => 'required',
        'age' => 'required|integer',
        'mobile' => 'required',
        'province' => 'required',
        'city' => 'required',
        'avatar' => 'nullable|image|max:10240',
        'vehicle' => 'required',
        'detail' => 'required',
    ];
    protected $messages = [
        'name.required' => 'لطفا نام را وارد کنید',
        'family.required' => 'لطفا نام خانوادگی را وارد کنید',
        'age.required' => 'لطفا سن را وارد کنید',
        'age.integer' => 'لطفا سن را به صورت عدد وارد کنید',
        'mobile.required' => 'لطفا موبایل را وارد کنید',
        'province.required' => 'لطفا استان را انتخاب کنید',
        'city.required' => 'لطفا شهر را انتخاب کنید',
        'vehicle.required' => 'لطفا وسیله را انتخاب کنید',
        'detail.required' => 'لطفا توضیحات را وارد کنید',
        'avatar.required' => 'لطفا آواتار را انتخاب کنید',
        'avatar.image' => 'فرمت آواتار صحیح نیست',
        'avatar.max' => 'حجم تصویر آواتار نباید بیشتر از 10 مگابایت باشد',

    ];
    public function updated($name)
    {
        $this->validateOnly($name);
    }

    public function set_delivery()
    {
        $this->validate();
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->delivery == 1) {
            $delivery = new Delivery();
            $delivery->name = $this->name;
            $delivery->family = $this->family;
            $delivery->mobile = $this->mobile;
            $delivery->age = $this->age;
            $delivery->vehicle = $this->vehicle;
            $delivery->vehicle_details = $this->detail;
            $delivery->province_id = $this->province;
            $delivery->city_id = $this->city;
            $delivery->save();
            if ($this->avatar!=null) {
                $name = time() . $this->avatar->getClientOriginalName();
                $dir='images/deliveries';
                $this->avatar->storeAS($dir,$name);
                $photo = new Photo();
                $photo->name = $this->avatar->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $delivery->id;
                $photo->groupable_type = Delivery::class;
                $photo->save();
                $delivery->photo_id = $photo->id;
                $delivery->save();
            }
            Session::put('success', 'پیک جدید ثبت شد');
            return redirect(route('deliveryReq'));
        } else {
            return 403;
        }
    }

    public function render()
    {
        return view('livewire.delivery.set-delivery');
    }
}
