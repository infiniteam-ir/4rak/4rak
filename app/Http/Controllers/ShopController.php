<?php

namespace App\Http\Controllers;

use App\Admin_img;
use App\Article;
use App\Article_category;
use App\Cart;
use App\Category;
use App\City;
use App\Comment;
use App\Customer;
use App\Customer_factor;
use App\Delivery_method;
use App\File;
use App\Guild;
use App\Income;
use App\online_output_factor;
use App\online_output_pro;
use App\Photo;
use App\Product;
use App\Province;
use App\Service;
use App\Shop;
use App\ShopPermission;
use App\special_Article;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use PharIo\Manifest\RequiresElementTest;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class ShopController extends Controller
{
    public function index()
    {
        return view('front.index');
    }

    public function store()
    {
        $products = Product::with('category', 'photo')->get();
        $categories = Category::where([['shop_id', 0], ['parent_id', null]])->get();
        $top_5_products = Product::where('trash', 0)->orderby('id', 'desc')->take(5)->with('photo')->get();
        $top_4_cats = Category::where('shop_id', 0)->take(4)->get();
        $articles = Article::with('category', 'photo')->orderBy('created_at', 'desc')->take(5)->get();
        $guilds = Guild::where('parent_id', null)->with('photo')->get();
        $slider_imgs = Admin_img::where('type', 'slider')->with('photo')->get();
        $top_banners = Admin_img::where([['type', 'banner'], ['position', 'top']])->with('photo')->get();
        $bot_banners = Admin_img::where([['type', 'banner'], ['position', 'bot']])->with('photo')->get();
        $side_banner = Admin_img::where([['type', 'banner'], ['position', 'side']])->with('photo')->first();
        return view('front.newTheme.main-shop', compact(['products', 'categories', 'top_5_products', 'top_4_cats', 'articles', 'guilds', 'slider_imgs', 'top_banners', 'bot_banners', 'side_banner']));
    }

    public function allShops()
    {
        $provinces = Province::all();
        $arrayCity = array();
        $cities = City::all();
        $arrayProvince = array();
        foreach ($cities as $city) {
            array_push($arrayCity, $city->name);
        }
        $arr = array();
        foreach ($provinces as $province) {
            $arr[$province->id] = $province->name;
        }
        $guilds = Guild::all();
        $arr_guilds = array();
        foreach ($guilds as $guild) {
            $arr_guilds[$guild->id] = $guild->name;
        }
        $shops = Shop::with('logo', 'guild', 'subguild')->get();
        $categories = Category::all();
        return view('front.newTheme.all-shops', compact(['shops', 'categories', 'arr', 'arrayCity', 'arr_guilds']));
    }

    public function singleshop($unique_name)
    {
        $shop = Shop::with('photo')->where('unique_name', $unique_name)->first();
        $products = Product::with('category', 'shop','photo')->where('shop_id', $shop->id)->paginate(9);
        $header = Photo::where('id', $shop->header_id)->first();
        $logo = Photo::where('id', $shop->logo_id)->first();
        $services = Service::where('shop_id', $shop->id)->get();
        $categories = Category::with('products')->where('shop_id', 0)->orWhere('shop_id', $shop->id)->get();
//        dd($categories);
        return view('front.newTheme.single-shop2', compact(['shop', 'products', 'header', 'logo', 'categories', 'services']));

    }

    public function get_products_by_cat($name, $id, $title)
    {
        $shop = Shop::with('photo')->where('unique_name', $name)->first();
        $products = Product::with('category', 'shop')->where([['shop_id', $shop->id], ['cat_id', $id]])->paginate(9);
        $header = Photo::where('id', $shop->header_id)->first();
        $logo = Photo::where('id', $shop->logo_id)->first();
        $services = Service::where('shop_id', $shop->id)->get();
        $categories = Category::with('products')->where('shop_id', 0)->orWhere('shop_id', $shop->id)->get();
        /*$products=Product::with('shop')->whereHas('shop',function($query)use ($name){
            $query->where('unique_name',$name);
        })->where('cat_id',$id)->paginate(9);*/

        return view('front.newTheme.single-shop2', compact(['shop', 'products', 'header', 'logo', 'categories', 'services']));
    }

    public function shop_services($name)
    {
        $shop = Shop::where('unique_name', $name)->first();
        $header = Photo::where('id', $shop->header_id)->first();
        $logo = Photo::where('id', $shop->logo_id)->first();
        $categories = Category::with('products')->where('shop_id', 0)->orWhere('shop_id', $shop->id)->get();
        $services = Service::where('shop_id', $shop->id)->paginate(9);
        return view('front.newTheme.single-shop-services', compact(['shop', 'header', 'logo', 'services', 'categories']));
    }

    public function product($slug, $id)
    {

        $product = Product::with('category', 'shop', 'guaranty', 'photos', 'photo')->where('id', $id)
            ->whereHas('photos', function ($query) {
                $query->where('groupable_type', Product::class);
            })
            ->first();
        if (!$product)
            $product = Product::with('category', 'shop', 'guaranty', 'photos', 'photo')->where('id', $id)->first();
        $categories = Category::all();
        $logo = Photo::where('id', $product->shop->logo_id)->first();
        $related_products = Product::where('cat_id', $product->cat_id)->get();
        $comments = Comment::where([['groupable_type', Product::class], ['groupable_id', $id]])->with('user')->get();
        return view('front.newTheme.product', compact(['product', 'categories', 'logo', 'related_products', 'comments']));
    }

    public function service($id)
    {
        $categories = Category::all();
        $service = Service::where('id', $id)->with('shop')->first();
        return view('front.newTheme.service', compact(['service', 'categories']));
    }

    public function shop_gallery($name)
    {
        $categories = Category::all();
        $shop = Shop::where('unique_name', $name)->first();
        $photos = Photo::where([['groupable_id', $shop->id], ['groupable_type', Shop::class]])->get();
        $video = File::where([['groupable_id', $shop->id], ['groupable_type', Shop::class]])->first();
        return view('front.newTheme.single-shop-gallery', compact(['photos', 'video', 'shop', 'categories']));
    }

    public function shop_blog($name)
    {
        $categories = Category::all();
        $shop = Shop::where('unique_name', $name)->first();
        $articles = Article::where('shop_id', $shop->id)->with('photo', 'author', 'category')->paginate(8);
        $article_cats = Article_category::where('shop_id', 0)->orwhere('shop_id', $shop->id)->get();

        return view('front.newTheme.single-shop-blog', compact(['articles', 'shop', 'categories', 'article_cats']));
    }

    public function blog()
    {
        $categories = Article_category::where('shop_id', 0)->get();
        $articles = Article::paginate(2);
//        $slider_articles = Article::where('special', 1)->get();
        $slider_articles = Article::take(5);
        $top_right_article = special_Article::where('position', 'blog_top_right')->with('article')->first();
        $top_mid_articles = special_Article::where('position', 'blog_top_mid')->with('article')->get();
        $top_left_articles = special_Article::where('position', 'blog_top_left')->with('article')->get();
        $comments = Comment::where('groupable_type', Article::class)->with('user', 'groupable')->orderby('id', 'desc')->take(6)->get();
        return view('front.newTheme.blog', compact(['categories', 'comments', 'articles', 'slider_articles', 'top_right_article', 'top_mid_articles', 'top_left_articles']));
    }

    public function article($id)
    {
        $article = Article::with('photo', 'author', 'category')->find($id);
        $categories = Article_category::all();
        $comments = Comment::where('groupable_type', Article::class)->with('user', 'groupable')->orderby('id', 'desc')->take(6)->get();
        $article_comments = Comment::where([['groupable_type', Article::class], ['groupable_id', $id]])->with('user', 'groupable')->get();
        return view('front.newTheme.article', compact(['categories', 'article', 'comments', 'article_comments']));
    }

    public function shop_article($name, $id)
    {
        $shop = Shop::where('unique_name', $name)->first();
        $article = Article::with('photo', 'author', 'category')->find($id);
        $categories = Category::all();
        $comments = Comment::where([['shop_id', Auth::user()->shop_id], ['groupable_type', Article::class]])->with('user', 'groupable')->orderby('id', 'desc')->take(6)->get();
        $article_comments = Comment::where([['groupable_type', Article::class], ['groupable_id', $id]])->with('user', 'groupable')->get();
        return view('front.newTheme.single-shop-article', compact(['categories', 'article', 'comments', 'article_comments', 'shop']));
    }

    public function addToCart(Request $request, $id)
    {
//        $product = Product::find($id);
        /*  $oldCart = Session::has('cart') ? Session::get('cart') : null;
          $cart = new Cart($oldCart);
          $cart->add($product, $product->id);
          $request->session()->put('cart', $cart);*/
//        return redirect(route('singleshop', $product->shop_id));
        $product = Product::find($id);
        if (Session::has('cart')) {
            $card = Session::get('cart');
            $card->items = array_push($card->items, $product);
            $card->totalQty += $request->qty;
            $card->totalPrice += $product->sell_price * $request->qty;
            $request->session()->push('cart', $card);
            dd($card);
//            Session::put('cart' ,json_encode($card));
            return redirect(route('singleProduct', [$product->slug, $product->id]));
        } else {
            $card = array("items" => array($product), "totalPrice" => $product->sell_price * $request->qty, "totalQty" => $request->qty);
            $request->session()->push('cart', $card);
//            dd(Session::all());
            dd($card);
            return redirect(route('singleProduct', [$product->slug, $product->id]));
        }

    }

    public function ShoppingCart()
    {
        $provinces=Province::all();
        $categories = Category::all();
        $delivery_methods = Delivery_method::all();
        if (!Session::has('cart')) {
            return view('front.newTheme.shopping-cart', ['products' => null, 'categories' => $categories]);
        }
        $cart = Session::get('cart');
        return view('front.newTheme.shopping-cart', ['provinces'=>$provinces,'products' => $cart['items'], 'totalPrice' => $cart['totalPrice'], 'categories' => $categories, 'delivery_methods' => $delivery_methods]);

    }

    public function deleteCart(Request $request)
    {
        $request->session()->forget('cart');
        return redirect(route('store'));
    }

    public function bill(Request $request)
    {
        return view('front.bill');
    }

    public function payment()
    {
        $customer_factor = new Customer_factor();
        $customer_factor->customer_id = Auth::id();
        $cart = Session::get('cart');
        $customer_factor->total = $cart['totalPrice'];
        $customer_factor->save();

        foreach ($cart['items'] as $product) {
            $f_products = new online_output_pro();
            $f_products->shop_id = $product->shop_id;
            $f_products->customer_factor_id = $customer_factor->id;
            $f_products->product_id = $product->id;
            $f_products->qty = $product->quantity;
            $f_products->price = $product->sell_price;;
            $f_products->save();
        }
        $amount = (integer)$customer_factor->total;

        $data = [
            'merchant_id' => '62697a96-bee0-4d87-85c0-f8db52150966',
            'callback_url' => route('paymentResponse', $customer_factor->id),
            'description' => "فاکتور خرید از چارک",
//            'amount' => 1000
            'amount' => $amount
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.zarinpal.com/pg/v4/payment/request.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: application/json",
                "content-type: application/json",
            ),
        ));
        $curlResp = curl_exec($curl);
        if (json_decode($curlResp)->data && json_decode($curlResp)->data->code == 100) {
            $customer_factor->transaction_code = json_decode($curlResp)->data->authority;
            $customer_factor->save();
            return redirect("https://www.zarinpal.com/pg/StartPay/" . json_decode($curlResp)->data->authority);
        } elseif(str_contains($curlResp,'The amount must be at least 1000.')){
                Session::put('limited','درگاه پرداخت: حداقل خرید 1000 تومان میباشد');
                return redirect(route('ShoppingCart'));
        }else
            return 404;
    }

    public function paymentResponse($customer_factor_id)
    {
        $cart = Session::get('cart');
        $customer_factor = Customer_factor::findorfail($customer_factor_id);
        $amount = (integer)$customer_factor->total;
        $status = $_GET['Status'];
        if ($_GET['Authority'] == $customer_factor->transaction_code && Auth::id() == $customer_factor->customer_id) {
            if ($status == 'NOK') {
                Session::put('Error', 'عملیات توسط کاربر لغو شد.');
                Session::remove('cart');
                return redirect(route('store'));

            } elseif ($status == 'OK') {

                // Make Post Fields Array
                $data = [
                    'merchant_id' => '62697a96-bee0-4d87-85c0-f8db52150966',
                    'authority' => $customer_factor->transaction_code,
                    'amount' => $amount
//                    'amount' => 1000
                ];

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.zarinpal.com/pg/v4/payment/verify.json",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array(
                        // Set here requred headers
                        "accept: application/json",
                        "content-type: application/json",
                    ),
                ));

                $response = json_decode(curl_exec($curl));

                if ($response->data->code == 100 && $response->data->message == "Paid") {

                    $customer_factor->status = 'پرداخت شده';
                    $customer_factor->save();
                    $factor_pros = online_output_pro::where('customer_factor_id', $customer_factor->id)->get();
                    $shops = array();

                    foreach ($factor_pros as $pro) {
                        $product = Product::where('id', $pro->product->id)->first();
                        $product->sold_qty++;
                        $product->save();
                        if ($product->guaranty_id){
                            $pro->guaranty_id = $product->guaranty_id;
                           $pro->save();
                        }
                        if (!in_array($pro->shop_id, $shops)) {
                            array_push($shops, $pro->shop_id);
                        }
                    }
                    $counter = count($shops);
                    for ($i = 0; $i < $counter; $i++) {
                        $customer = Customer::where([['mobile', Auth::user()->mobile], ['shop_id', $shops[$i]]])->first();
                        if ($customer == null) {
                            $customer = new Customer();
                            $customer->shop_id = $shops[$i];
                            $customer->name = Auth::user()->name;
                            $customer->family = Auth::user()->family;
                            $customer->mobile = Auth::user()->mobile;
                            $customer->address = Auth::user()->info->province->name . "" . Auth::user()->info->city->name . "" . Auth::user()->info->address;
                            $customer->phone = Auth::user()->info->phone;
                            $customer->save();
                        }
                        $factor = new online_output_factor();
                        $factor->shop_id = $shops[$i];
                        $factor->customer_id = $customer->id;
                        $factor->delivery_method_id = $cart['deliveryMethod'];
                        $factor->f_date = Carbon::now();
                        foreach ($factor_pros as $pro) {
                            if ($pro->shop_id == $shops[$i]) {
                                $factor->total_price += $pro->price * $pro->qty;
                            }
                            $factor->save();
                            $pro->factor_id = $factor->id;
                            $pro->save();
                        }
                        $shop = Shop::where('id', $shops[$i])->first();
                        $shop->wallet += $factor->total_price;
                        $shop->save();
                        $factor->save();
                        $income = new Income();
                        $income->type = 'online';
                        $income->shop_id = $shops[$i];
                        $income->source_type = 'فروش محصول';
                        $income->target_type = 'wallet';
                        $income->source_id = $factor->id;
                        $income->price = $factor->total_price;
                        $income->save();
                        $shop_permissions = ShopPermission::where([['shop_id', $factor->shop_id], ['sell', true]])->with('shoprole', 'shoprole.user')->get();
                        foreach ($shop_permissions as $shop_permission) {
                            if ($shop_permission->shoprole){
                                    if (count($shop_permission->shoprole->user)>0){
                                        foreach ($shop_permission->shoprole->user as $user) {
                                            Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                                                'receptor' => $user->mobile,
                                                'token' => $factor->id,
                                                'token2' => $factor->total_price . 'تومان',
                                                'token3' => verta($factor->f_date)->format('Y/n/j'),
                                                'template' => '4rakNewBuySeller',
                                            ]);
                                        }
                                    }
                            }

                        }
                    }

                    Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                        'receptor' => Auth::user()->mobile,
                        'token' => $customer_factor->id,
                        'token2' => $customer_factor->total . 'تومان',
                        'token3' =>verta($customer_factor->created_at)->format('Y/n/j'),
                        'template' => '4rakNewBuyUser',
                    ]);

                    Session()->forget('cart');
                    Session::put('success', 'پرداخت انجام شد.');
                    return redirect(route('buyHistory'));

                } else {
                    Session::put('Error', ' پرداخت نامعتبر است');
                    return redirect(route('store'));
                }

            }

        } else {
            return redirect(404);
        }

    }

    public function guilds()
    {
        $categories = Category::all();
        $provinces = Province::all();
        $arrayCity = array();
        $cities = City::all();
        $arrayProvince = array();
        foreach ($cities as $city) {
            array_push($arrayCity, $city->name);
        }
        $arr = array();
        foreach ($provinces as $province) {
            $arr[$province->id] = $province->name;
        }
        $guilds = Guild::with('shops', 'photo')->whereHas('shops')->get();
        return view('front.newTheme.guilds', compact(['categories', 'arr', 'arrayCity', 'guilds']));
    }

    /* public function categories($title, $id)
     {
         $categories = Category::with('products')->get();
         $products = Product::where([['cat_id', $id], ['trash', 0]])->with('photo')->paginate(9);
         $provinces = Province::all();
         $arrayCity = array();
         $cities = City::all();
         $arrayProvince = array();
         foreach ($cities as $city) {
             array_push($arrayCity, $city->name);
         }
         $arr = array();
         foreach ($provinces as $province) {
             $arr[$province->id] = $province->name;
         }
         return view('front.newTheme.cats', compact(['categories', 'arr', 'arrayCity', 'products']));
     }*/
    public function blog_categories($cat, $id)
    {
        $catt = $cat;
        if ($cat == "همه")
            $cat = "";
        $posts = Article::with('photo', 'category')
            ->where('cat_id', $id)
            ->paginate(9);

        $categories = Article_category::with('articles')->get();
        return view('front.newTheme.article-cats', compact(['categories', 'posts', 'catt']));
    }

    public function getProductPaginate($province, $city, $cat)
    {
        $provincee = $province;
        $cityy = $city;
        $catt = $cat;
        if ($province == "همه")
            $province = "";
        if ($city == "همه")
            $city = "";
        if ($cat == "همه")
            $cat = "";

        $products = Product::with('photo', 'shop.province', 'shop.city', 'category')
            ->whereHas('shop.province', function ($query) use ($province) {
                $query->where('name', 'LIKE', '%' . $province . '%');
            })
            ->whereHas('shop.city', function ($query) use ($city) {
                $query->where('name', 'LIKE', '%' . $city . '%');
            })
            ->whereHas('category', function ($query) use ($cat) {
                $query->where('title', 'LIKE', '%' . $cat . '%');
            })
            ->paginate(9);

        $categories = Category::with('products')->get();
        $provinces = Province::all();
        return view('front.newTheme.cats', compact(['categories', 'provinces', 'products', 'provincee', 'cityy', 'catt']));
    }

}
