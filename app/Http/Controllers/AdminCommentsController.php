<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function article_comments()
    {
        $comments=Comment::where([['groupable_type',Article::class],['shop_id',Auth::user()->shop_id]])->paginate(20);
        return view('admin.Blog.comments',compact('comments'));
    }
    public function product_comments()
    {
        $comments=Comment::where([['groupable_type',Product::class],['shop_id',Auth::user()->shop_id]])->paginate(20);
        return view('admin.Products.comments',compact('comments'));
    }
    public function all_comments()
    {
        if (Auth::user()->role->name=='مدیر'||Auth::user()->role->name=='ادمین'){
            $comments=Comment::paginate(20);
            return view('admin.admin.comments',compact('comments'));
        }else{
            return 404;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment=new Comment();
        $comment->groupable_type=Product::class;
        $comment->groupable_id=$request->input('comment_post_ID');
        $comment->reply_to=$request->input('comment_parent');
        $comment->user_id=Auth::id();
        $comment->content=$request->input('content');
        $comment->positive_points=$request->input('positives');
        $comment->weak_points=$request->input('negatives');
        $comment->value_per_price=$request->input('value_per_price');
        $comment->quality=$request->input('quality');
        $comment->save();
        dd($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
