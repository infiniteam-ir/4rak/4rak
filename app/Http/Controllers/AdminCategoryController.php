<?php

namespace App\Http\Controllers;

use App\Category;
use App\Photo;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' ||Auth::user()->role->name == 'ادمین' ) {
            $categories = Category::where('shop_id', Auth::user()->shop_id)->paginate(10);
            return view('admin.Products.categories', compact('categories'));
        } else {
            return 403;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {
            $cats = Category::where('shop_id', Auth::user()->shop_id)->orwhere('shop_id',0)->get();
            return view('admin.Products.newCategory', compact('cats'));
        } else {
            return 403;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {
            $cat = new Category();
            $cat->title = $request->input('title');
            $cat->shop_id = Auth::user()->shop_id;
            $cat->save();
            if ($file = $request->File('image')) {
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $cat->id;
                $photo->groupable_type = Category::class;
                $photo->save();
                $cat->photo_id = $photo->id;
            }
            if ($request->parent != '0') {
                $parent = Category::where('title', $request->parent)->first();
                $cat->parent_id = $parent->id;
            }
            $cat->save();
            $request->session()->put('saved','گروه کالا اضافه شد.');
            return redirect(route('category.index'));
        } else {
            return 403;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {

            $cats = Category::where('shop_id', Auth::user()->shop_id)->get();
            $cat = Category::find($id);
            return view('admin.Products.editCategory', compact(['cats', 'cat']));
        } else {
            return 403;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {

            $cat = Category::find($id);
            $cat->title = $request->input('title');
            if ($file = $request->File('image')) {
                if ($cat->photo_id != null) {
                    $old = Photo::where('id', $cat->photo_id)->first();
                    unlink(public_path() . '/images/' . $old->path);
                    $old->delete();
                }
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $cat->id;
                $photo->groupable_type = Category::class;
                $photo->save();
                $cat->photo_id = $photo->id;
            }
            if ($request->parent != '0') {
                $parent = Category::where('title', $request->parent)->first();
                $cat->parent_id = $parent->id;
            }
            $cat->save();
            $request->session()->put('success','تغییرات ثبت شد');
            return redirect(route('category.index'));
        } else {
            return 403;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $cat = Category::with('products')->where('id', $id)->first();
            if ($cat->products != null) {
                Session::flash('error', 'این گروه دارای محصول است و شما قادر به حذف آن نیستید');
                return redirect(route('category.index'));
            } else {
                $childern = Category::where('parent_id', $id)->get();
                foreach ($childern as $child) {
                    $child->parent_id = null;
                    $child->save();
                }
                $cat->delete();
                Session::put('success','تغییرات ثبت شد');
                return redirect(route('category.index'));
            }
        } else {
            return 403;
        }


    }
}
