<?php

namespace App\Http\Controllers;

use App\Product;
use App\Storage;
use App\StoragePivot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminStorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {

            $storages = Storage::where('shop_id',Auth::user()->shop_id)->with('user', 'shop')->paginate(10);
            return view('admin.Storage.storages', compact('storages'));
        }else{
            return 403;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1)
            return view('admin.Storage.newStorage');
        else
            return 403;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $storage = new Storage();
            $storage->name = $request->input('name');
            $storage->address = $request->input('address');
            $storage->shop_id = Auth::user()->shop_id;
            $storage->user_id = Auth::id();
            $storage->save();
            Session::flash('success', 'انبار جدید ثبت شد');
            return redirect(route('storage.index'));
        }else{
            return 403;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $storage = Storage::where('id', $id)->first();
            return view('admin.Storage.editStorage', compact('storage'));
        }else{
            return 403;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $storage = Storage::where('id', $id)->first();
            $storage->name = $request->name;
            $storage->address = $request->address;
            $storage->save();
            Session::flash('success', 'تغییرات ثبت شد');
            return redirect(route('storage.index'));
        }else{
            return 403;
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ioProduct()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $products = Product::where([['shop_id', Auth::user()->shop_id],['trash',0]])->get();
            $storages = Storage::where('shop_id', Auth::user()->shop_id)->get();
            return view('admin.Storage.ioProduct', compact(['products', 'storages']));
        }else{
            return 403;
        }
    }

    public function importProduct(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $product = Product::where([['id', $request->product_name], ['shop_id', Auth::user()->shop_id]])->first();
            $storage = Storage::where([['name', $request->storage_name], ['shop_id', Auth::user()->shop_id]])->first();
            $existingPivot = StoragePivot::where([['product_id', $product->id], ['storage_id', $storage->id]])->first();
            if ($product->quantity < $request->input('qty')){
                Session::put('error', 'مقدار انتخاب شده از مقدار موجودی کالا بیشتر است.');
            }else{
                if ($existingPivot == null) {
                    $pivot = new StoragePivot();
                    $pivot->product_id = $product->id;
                    $pivot->shop_id = Auth::user()->shop_id;
                    $pivot->storage_id = $storage->id;
                    $pivot->quantity = $request->input('qty');
                    $pivot->save();
                    $product->storage_id = $storage->id;
                    $product->quantity-=$request->input('qty');
                    $product->save();

                } else {
                    $existingPivot->quantity += $request->input('qty');
                    $existingPivot->save();
                    $product->quantity-=$request->input('qty');
                    $product->save();
                }
                Session::put('success', 'کالاها به انبار اضافه شد');
            }
            return redirect(route('ioProduct'));
        }else{
            return 403;
        }
    }


    public function exportProduct(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {
            $product = Product::where([['id', $request->product_name], ['shop_id', Auth::user()->shop_id]])->first();
            $storage = Storage::where([['id', $request->storage_name], ['shop_id', Auth::user()->shop_id]])->first();
            $existingPivot = StoragePivot::where([['product_id', $product->id], ['storage_id', $storage->id]])->first();

            if ($existingPivot != null) {
                if ($existingPivot->quantity > $request->input('qty')) {
                    $existingPivot->quantity -= $request->input('qty');
                    $existingPivot->save();
                    $product->quantity+=$request->input('qty');
                    $product->save();
                    Session::put('success', 'کالاها از انبار خارج شد');
                } else {
                    Session::put('error', 'تعداد کالای انتخاب شده از تعداد موجود بیشتر است');
                }
            } else {
                Session::put('error', 'کالا در انبار موجود نمیباشد');
            }
            return redirect(route('ioProduct'));
        }else{
            return 403;
        }
    }

    public function storageReport()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage==1) {

            $products = StoragePivot::where('shop_id', Auth::user()->shop_id)->with('product', 'storage')->paginate(10);
            $storages = Storage::where('shop_id', Auth::user()->shop_id)->with('pivots', 'pivots.product')->get();
            return view('admin.Storage.report', compact(['products', 'storages']));
        }else{
            return 403;
        }

    }

    public function dashboard()
    {
        $storages_qty=Storage::where('shop_id',Auth::user()->shop_id)->count();
        return view('admin.Storage.dashboard',compact(['storages_qty']));
    }
}
