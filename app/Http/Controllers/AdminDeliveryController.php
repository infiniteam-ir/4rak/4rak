<?php

namespace App\Http\Controllers;

use App\City;
use App\Delivery;
use App\online_output_factor;
use App\Photo;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        return view('admin.Delivery.new', compact('provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->delivery == 1) {
            $delivery = new Delivery();
            $delivery->name = $request->input('name');
            $delivery->family = $request->input('family');
            $delivery->mobile = $request->input('mobile');
            $delivery->age = $request->input('age');
            $delivery->vehicle = $request->input('vehicle');
            $delivery->vehicle_details = $request->input('detail');
            $delivery->province_id = $request->input('province');
            $delivery->city_id = $request->input('city');
            $delivery->save();
            if ($file = $request->file('avatar')) {
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $delivery->id;
                $photo->groupable_type = Delivery::class;
                $photo->save();
                $delivery->photo_id = $photo->id;
                $delivery->save();
            }
            Session::put('success', 'پیک جدید ثبت شد');
            return redirect(route('deliveryReq'));
        } else {
            return 403;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deliveryReq()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->delivery == 1) {
            $cities = City::all();
            $provinces = Province::all();
            $arrayCity = array();
            foreach ($cities as $city) {
                array_push($arrayCity, $city->name);
            }
            $arr = array();
            foreach ($provinces as $province) {
                $arr[$province->id] = $province->name;
            }

            $motors = Delivery::with(['photo', 'province', 'city'])->where('vehicle', 'موتور')->paginate(10);
            $cars = Delivery::with('photo')->where('vehicle', 'وانت')->paginate(10);
            $trucks = Delivery::with('photo')->where('vehicle', 'کامیون')->paginate(10);
            return view('admin.Delivery.request', compact(['motors', 'cars', 'trucks', 'arrayCity', 'arr']));
        } else {
            return 403;
        }
    }

    public function saveReq(Request $request, $id)
    {
        dd($request);
    }

    public function onlineOrders()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->delivery == 1) {
            $factors = online_output_factor::where('shop_id', Auth::user()->shop_id)->whereNull('send_date')->with('factor_pro', 'customer')->get();
            return view('admin.delivery.onlineOrders', compact('factors'));
        } else
            return 403;
    }

    public function onlineOrdersInfo($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->delivery == 1) {
            $factor = online_output_factor::where([['shop_id', Auth::user()->shop_id], ['id', $id]])->with('factor_pro.product.photo', 'customer', 'customer.photo')->first();
            return view('admin.delivery.onlineOrderInfo', compact('factor'));
        } else
            return 403;
    }

    public function dashboard()
    {
        $motor_qty = Delivery::where([['province_id', Auth::user()->shop->province_id], ['vehicle', 'موتور']])->count();
        $truck_qty = Delivery::where([['province_id', Auth::user()->shop->province_id], ['vehicle', '<>', 'موتور']])->count();

        return view('admin.Delivery.dashboard', compact(['motor_qty', 'truck_qty']));
    }
}
