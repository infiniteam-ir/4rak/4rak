<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services=Service::where('shop_id',Auth::user()->shop_id)->with('photo')->paginate(10);
        return view('admin.Service.services',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Service.newService');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $service=new Service();
            $service->shop_id=Auth::user()->shop_id;
            $service->title=$request->input('title');
            $service->type=$request->input('type');
            $service->price=$request->input('price');
            $service->call=$request->input('call');
            $service->status='active';
            $service->save();
            Session::put('success','خدمت جدید اضافه شد');
            return redirect(route('service.index'));
        }else{
            return 403;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service=Service::where([['shop_id',Auth::user()->shop_id],['id',$id]])->first();
        return view('admin.Service.editService',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $service=Service::where([['shop_id',Auth::user()->shop_id],['id',$id]])->first();
            $service->title=$request->input('title');
            $service->type=$request->input('type');
            $service->price=$request->input('price');
            $service->call=$request->input('call');
            $service->status=$request->input('status');
            $service->save();
            Session::put('success','تغییرات ذخیره شد');
            return redirect(route('service.index'));
        }else{
            return 403;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
