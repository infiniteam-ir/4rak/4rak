<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:55'],
            'family' => ['required', 'string', 'max:155'],
            'mobile' => ['required', 'string', 'max:11','min:11','unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8','confirmed'],
        ],[
            'name.required'=>'نوشتن نام الزامیست',
            'family.required'=>'نوشتن نام خانوادگی الزامیست',
            'mobile.required'=>'وارد کردن شماره همراه الزامیست',
            'mobile.max'=>'شماره همراه بیشتر از 11 رقم نمیتواند باشد',
            'email.required'=>'نوشتن ایمیل الزامیست',
            'email.unique'=>'ایمیل وارد شده قبلا ثبت شده',
            'email.email'=>'لطفا یک ایمیل معتبر وارد کنید',
            'password.required'=>'رمز عبور نمیتواند خالی باشد',
            'password.confirmed'=>'رمز عبور و تکرار آن مطابقت ندارد',
            'password_confirmation.required'=>'تکرار رمز عبور نمیتواند خالی باشد',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'family' => $data['family'],
            'mobile' => $data['mobile'],
            'role_id' => 3,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
