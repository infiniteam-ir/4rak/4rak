<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Login_log;
use App\Logout_log;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function username()
    {
        return 'mobile';
    }
    public function login(Request $request)
    {

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'password' => [__('اطلاعات وارد شده صحیح نمیباشد.')],
        ]);
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ],['mobile.required'=>'لطفا شماره موبایل خود را وارد کنید',
            'password.required'=>'لطفا رمز عبور خود را وارد کنید'
        ]);
    }



    public function logout(Request $request)
    {
        Auth::user()->update([
            'last_logout_at' => Carbon::now()->toDateTimeString(),
        ]);
        if (Auth::user()->shop_id!=null){
            $user = Auth::user();
            $logout = Logout_log::where([['shop_id', $user->shop_id], ['user_id', Auth::id()]])->orderby('created_at')->get()->last();
            if ($logout) {
                $logout->logout_time = Carbon::now();
                $logout->save();
            }
        }
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => \Illuminate\Support\Facades\Request::ip(),
        ]);
        if (Auth::user()->shop_id != null){
            $log=new Login_log();
            $log->user_id=Auth::id();
            $log->shop_id=Auth::user()->shop_id;
            $log->login_time=Carbon::now();
            $log->save();
            $out=new Logout_log();
            $out->user_id=Auth::id();
            $out->shop_id=Auth::user()->shop_id;
            $out->logout_time=Carbon::now();
            $out->logout_time->addHour();
            $out->save();

        }
    }
}
