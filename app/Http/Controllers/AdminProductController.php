<?php

namespace App\Http\Controllers;

use App\Accepted_Guaranty;
use App\Account;
use App\Account_type;
use App\Category;
use App\Customer;
use App\Guaranty;
use App\Http\Middleware\SessionExpired;
use App\Income;
use App\Input_Factor;
use App\Input_Factor_Pro;
use App\online_output_pro;
use App\Outgo;
use App\Output_factor;
use App\Output_factor_pro;
use App\Photo;
use App\Product;
use App\Provider;
use App\Shop;
use App\File;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\String\AbstractUnicodeString;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک'|| Auth::user()->role->name == 'ادمین') {

            $products = Product::where([['shop_id', Auth::user()->shop_id], ['trash', 0], ['quantity', '>', 0]])->with('user', 'category', 'photo')->paginate(10);
            return view('admin.Products.products', compact('products'));
        } else {
            return 403;
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {
            $accounts=Account::where([['shop_id',Auth::user()->shop_id],['owner_type','self']])->get();
            if (count($accounts)>0){
                $factor = null;
                $cats = Category::where('shop_id', Auth::user()->shop_id)->get();
                $buyFactors = Input_Factor::where('shop_id', Auth::user()->shop_id)->with('in_products.product', 'provider')->get();
                return view('admin.Products.newProduct', compact(['cats', 'factor', 'buyFactors']));
            }else{
                Session::put('error','شما حسابی برای پرداخت هزینه فاکتور ندارید، ابتدا از منوی حسابداری یا حساب ایجاد کنید.');
                return redirect(route('productDashboard'));
            }
        } else {
            return 403;
        }
    }
    public function new_product()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {
            $cats = Category::where('shop_id', Auth::user()->shop_id)->get();
            return view('admin.Products.addProduct', compact('cats'));
        } else {
            return 403;
        }
    }

    public function editbuyFactor($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {
            $factor = Input_Factor::where([['id', $id], ['shop_id', Auth::user()->shop_id]])->with('in_products', 'in_products.product', 'provider')->first();
            $cats = Category::where('shop_id', Auth::user()->shop_id)->get();
            $buyFactors = Input_Factor::where('shop_id', Auth::user()->shop_id)->with('in_products', 'provider')->get();
            return view('admin.Products.newProduct', compact(['cats', 'factor', 'buyFactors']));
        } else {
            return 403;
        }
    }

    public function editsellFactor($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {
            $factor = Output_Factor::where([['id', $id], ['shop_id', Auth::user()->shop_id]])->with('out_products', 'out_products.product', 'customer')->first();
            $cats = Category::where('shop_id', Auth::user()->shop_id)->get();
            $sellFactors = Output_Factor::where('shop_id', Auth::user()->shop_id)->with('out_products', 'customer')->get();
            $f_num=$factor->f_num;
            $products = Product::where([['shop_id', Auth::user()->shop_id], ['quantity', '>', 0]])->get();
            return view('admin.Products.sellProduct', compact(['cats', 'factor', 'sellFactors','f_num','products']));
        } else {
            return 403;
        }
    }

    public function editFactorProduct($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {

        } else {
            return 403;
        }
    }

    public function UploadProductFiles(Request $request, $id)
    {

        if ($file = $request->file('image')) {
            $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->header('id')]])->first();
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('images/products', $path);
            $image = new Photo();
            $image->groupable_id = $product->id;
            $image->groupable_type = Product::class;
            $image->path = $path;
            $image->name = $name;
            $image->save();
        }
        if ($file = $request->file('file')) {
            $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->header('id')]])->first();
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('files', $path);
                $newfile = new File();
            $newfile->groupable_id = $product->id;
            $newfile->groupable_type = Product::class;
            $newfile->path = $path;
            $newfile->name = $name;
            $newfile->save();
            $product->file_id=$newfile->id;
            $product->save();
        }

    }

    public function DelUploadProductFiles(Request $request, $id)
    {
        return $request->header();
        $filename = $request->get('filename');
        $photo = Photo::where([['name', $filename], ['groupable_id', $request->header('id')]])->first();
        unlink(public_path() . '/images/products/' . $photo->path);
        $photo->delete();
        return response()->json(['success' => $filename]);

    }

    public function deleteUploaded(Request $request)
    {
        if ($file = $request->file('image')) {
            $filename = $request->get('filename');
            Photo::where('name', $filename)->delete();
            $path = public_path() . '/images/products/' . $filename;
            if (file_exists($path)) {
                unlink($path);
            }
            return response()->json(['success' => $filename]);
        }
        if ($file = $request->file('file')) {
            $filename = $request->get('filename');
            File::where('name', $filename)->delete();
            $path = public_path() . '/files/' . $filename;
            if (file_exists($path)) {
                unlink($path);
            }
            return response()->json(['success' => $filename]);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1) {

            $factor = Input_Factor::where('f_num', $request->f_num)->with('provider')->first();
            $provider = Provider::where([['belong_to', Auth::user()->shop_id], ['id', $request->provider_id]])->first();

            if ($provider == null) {
                $provider = new Provider();
                $provider->belong_to = Auth::user()->shop_id;
                $provider->name = $request->input('provider_co');
                $provider->address = '-';
                $provider->save();
                $providerAccount = new Account();
                $providerAccount->name = $provider->name;
                $providerAccount->owner_type = Provider::class;
                $providerAccount->owner_id = $provider->id;
                $providerAccount->amount = 0;
                $providerAccount->shop_id = Auth::user()->shop_id;
                $providerAccount->save();
            }

            if ($factor == null) {

                $factor = new Input_Factor();
                $factor->shop_id = Auth::user()->shop_id;
                $factor->f_num = $request->input('f_num');
                $factor->type = "offline";
                $factor->provider_id = $provider->id;
                $factor->total_price = 0;
                $factor->f_date = $request->input('f_date');
                $factor->save();


                $product = new Product();
                $product->shop_id = Auth::user()->shop_id;
                $product->user_id = Auth::id();
                $cat = Category::where('title', $request->cat)->first();
                $product->cat_id = $cat->id;
                $product->name = $request->input('title');
                if ($request->input('slug')) {
                    $product->slug = make_slug($request->input('slug'));
                } else {
                    $product->slug = make_slug($request->input('title'));
                }
                $product->status = 'تایید نشده';

                $product->provider_co = $provider->name . " " . $provider->family;

                $product->sell_price = $request->input('sell_price');
                $product->buy_price = $request->input('buy_price');
                $product->whole_price = $request->input('whole_price');
                $product->quantity = $request->input('qty');
                $product->description = $request->input('desc');
                $product->meta_description = $request->input('meta_desc');
                $product->meta_keywords = $request->input('keyword');
                $product->save();
                if ($file = $request->file('image')) {
                    $name = time() . $file->getClientOriginalName();
                    $file->move('images', $name);
                    $photo = new Photo();
                    $photo->name = $file->getClientOriginalName();
                    $photo->path = $name;
                    $photo->groupable_id = $product->id;
                    $photo->groupable_type = Product::class;
                    $photo->save();
                    $product->photo_id = $photo->id;
                }
                if ($file = $request->file('file')) {
                    $name = time() . $file->getClientOriginalName();
                    $file->move('files', $name);
                    $fileRecord = new File();
                    $fileRecord->name = $file->getClientOriginalName();
                    $fileRecord->path = $name;
                    $fileRecord->groupable_id = $product->id;
                    $fileRecord->groupable_type = Product::class;
//                $fileRecord->save();
//                $product->file_id=$fileRecord->id;
                }
                $product->save();
                if ($request->guaranty == true) {
                    $guaranty = new Guaranty();
                    $guaranty->product_id = $product->id;
                    $guaranty->duration = $request->duration;
                    $guaranty->duration_unit = $request->duration_unit;
                    $guaranty->start = $request->guaranty_start;
                    $guaranty->save();
                }

                $factor->total_price += $product->buy_price * $product->quantity;
                $factor->save();
                $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
                $providerAccount->amount -= $factor->total_price;
                $providerAccount->save();

                $factor_pro = new Input_Factor_Pro();
                $factor_pro->f_num = $factor->f_num;
                $factor_pro->shop_id = Auth::user()->shop_id;
                $factor_pro->product_id = $product->id;
                $factor_pro->qty = $request->qty;
                $factor_pro->price = $product->buy_price;
                $factor_pro->save();

            } else {
                $product = new Product();
                $product->shop_id = Auth::user()->shop_id;
                $product->user_id = Auth::id();
                $cat = Category::where('title', $request->cat)->first();
                $product->cat_id = $cat->id;
                $product->name = $request->input('title');
                if ($request->input('slug')) {
                    $product->slug = make_slug($request->input('slug'));
                } else {
                    $product->slug = make_slug($request->input('title'));
                }
                $product->status = 'تایید نشده';

                $product->provider_co = $provider->name . " " . $provider->family;
                $product->sell_price = $request->input('sell_price');
                $product->buy_price = $request->input('buy_price');
                $product->whole_price = $request->input('whole_price');
                $product->quantity = $request->input('qty');
                $product->description = $request->input('desc');
                $product->meta_description = $request->input('meta_desc');
                $product->meta_keywords = $request->input('keyword');
                $product->save();
                if ($file = $request->file('image')) {
                    $name = time() . $file->getClientOriginalName();
                    $file->move('images', $name);
                    $photo = new Photo();
                    $photo->name = $file->getClientOriginalName();
                    $photo->path = $name;
                    $photo->groupable_id = $product->id;
                    $photo->groupable_type = Product::class;
                    $photo->save();
                    $product->photo_id = $photo->id;
                }
                if ($file = $request->file('file')) {
                    $name = time() . $file->getClientOriginalName();
                    $file->move('files', $name);
                    $fileRecoed = new File();
                    $fileRecoed->name = $file->getClientOriginalName();
                    $fileRecoed->path = $name;
                    $fileRecoed->user_id = Auth::id();
                    $fileRecoed->save();
                    $product->file_id = $fileRecoed->id;
                }
                $product->save();
                if ($request->guaranty == true) {
                    $guaranty = new Guaranty();
                    $guaranty->product_id = $product->id;
                    $guaranty->duration = $request->duration;
                    $guaranty->duration_unit = $request->duration_unit;
                    $guaranty->start = $request->guaranty_start;
                    $guaranty->save();
                    $product->guaranty_id = $guaranty->id;
                    $product->save();

                }

                $factor->total_price += $product->buy_price * $product->quantity;
                $factor->save();

                $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
                $providerAccount->amount -= $factor->total_price;
                $providerAccount->save();

                $factor_pro = new Input_Factor_Pro();
                $factor_pro->f_num = $factor->f_num;
                $factor_pro->shop_id = Auth::user()->shop_id;
                $factor_pro->product_id = $product->id;
                $factor_pro->qty = $product->quantity;
                $factor_pro->price = $product->buy_price;
                $factor_pro->save();
            }
            if ($request->save == 'save') {

                return redirect(route('factorPurchase', $factor->f_num));
            } elseif ($request->savenew == 'savenew') {
                $f_num = $factor->f_num;
                return redirect(route('savenew', $f_num));
            }
        } else {
            return 403;
        }
    }

    public function factorPurchase($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'ادمین' ||Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1 || Auth::user()->role->name == 'ادمین') {
            $factor = Input_Factor::where([['shop_id',Auth::user()->shop_id],['f_num', $id]])->first();
            $outgoes = Outgo::where([['shop_id',Auth::user()->shop_id],['target_id', $factor->id]])->get();
            $accounts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self']])->get();
            $acc_types=Account_type::all();
            $paid = 0;
            foreach ($outgoes as $outgo) {
                $paid += $outgo->price;
            }
            return view('admin.Products.saveBuyFactor', compact(['accounts', 'factor', 'outgoes', 'paid','acc_types']));
        } else {
            return 403;
        }

    }

    public function savePurchase($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->buy == 1 || Auth::user()->role->name == 'ادمین') {
            $factor = Input_Factor::where('f_num', $id)->with('provider', 'outgoes')->first();
             $paid=0;

             foreach ($factor->outgoes as $outgo){
                 $paid+=$outgo->price;
             }
             if ($paid==$factor->total_price)
             {
                 $factor->pay_status='paid';
                 $factor->save();
             }

            $providerAccount = Account::where('owner_id', $factor->provider->id)->first();
            if ($providerAccount->amount < 0)
                Session::put('msg', 'بدهی شما ثبت شد');
            return redirect(route('unpaidFactors'));
        } else {
            return 403;
        }
    }

    public function savePayment($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->sell == 1) {

            $factor = Output_factor::where('f_num', $id)->with('customer', 'incomes')->first();
            $customerAccount = Account::where('owner_id', $factor->customer->id)->first();
            $paid = 0;
            foreach ($factor->incomes as $income) {
                $paid += $income->price;
            }
            if ($paid < $factor->total_price)
                Session::flash('msg', 'طلب شما ثبت شد');
            return redirect(route('accounts'));
        } else {
            return 403;
        }
    }

    public function sell()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->sell == 1) {
            if (Auth::user()->shop_id != null) {
                $products = Product::where([['shop_id', Auth::user()->shop_id],['trash',0], ['quantity', '>', 0]])->get();
                $factor = Output_factor::where('shop_id', Auth::user()->shop_id)->get()->last();
                if ($factor == null)
                    $f_num = 100;
                else
                    $f_num = $factor->f_num + 1;
            } else {
                $products = null;
                $f_num = 0;
            }
            $sellFactors = Output_factor::where('shop_id', Auth::user()->shop_id)->with('customer')->get();
            return view('admin.Products.sellProduct', compact(['products', 'f_num', 'sellFactors']));
        } else {
            return 403;
        }
    }

    public function sellFactor($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->sell == 1) {
            $factor = Output_factor::where([['f_num', $id], ['shop_id', Auth::user()->shop_id]])->first();
            $incomes = Income::where([['source_id', $factor->f_num], ['shop_id', Auth::user()->shop_id]])->get();
            $accounts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self']])->get();
            $paid = 0;
            foreach ($incomes as $income) {
                $paid += $income->price;
            }
            return view('admin.Products.saveSellFactor', compact(['accounts', 'factor', 'incomes', 'paid']));
        } else {
            return 403;
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->role->name == 'ادمین') {

            $product = Product::where('id', $id)->with('category', 'file', 'photo')->first();
            $cats = Category::where('shop_id', Auth::user()->shop_id)->orwhere('shop_id', 0)->get();
            return view('admin.Products.editProduct', compact(['product', 'cats']));
        } else {
            return 403;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {

            $product = Product::find($id);
            $cat = Category::where('title', $request->cat)->first();
            $product->cat_id = $cat->id;
            $product->name = $request->input('title');
            if ($request->input('slug')) {
                $product->slug = make_slug($request->input('slug'));
            } else {
                $product->slug = make_slug($request->input('title'));
            }
            $product->provider_co = $request->input('provider_co');
            $product->sell_price = $request->input('sell_price');
            $product->buy_price = $request->input('buy_price');
            $product->whole_price = $request->input('whole_price');
            $product->quantity = $request->input('qty');
            $product->description = $request->input('desc');
            $product->meta_description = $request->input('meta_desc');
            $product->meta_keywords = $request->input('keyword');
            if ($file = $request->file('image')) {
                if ($product->photo_id != null) {
                    $old = Photo::where('id', $product->photo_id)->first();
                    unlink(public_path() . '/images/' . $old->path);
                    $old->delete();
                }
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->groupable_id = $product->id;
                $photo->groupable_type = Product::class;
                $photo->save();
                $product->photo_id = $photo->id;
            }
            if ($file = $request->file('file')) {
                if ($product->file_id != null) {
                    $old = File::where('id', $product->file_id)->first();
                    unlink(public_path() . '/files/' . $old->path);
                    $old->delete();
                }
                $name = time() . $file->getClientOriginalName();
                $file->move('files', $name);
                $fileRecord = new File();
                $fileRecord->name = $file->getClientOriginalName();
                $fileRecord->path = $name;
                $fileRecord->user_id = Auth::id();
                $fileRecord->save();
                $product->file_id = $fileRecord->id;
            }
            $product->save();
            $request->session()->put('success','تغییرات ثبت شد.');
            return redirect(route('product.index'));
        } else {
            return 403;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
            $product = Product::find($id);
            $product->trash = 1;
            $product->save();
            Session::flash('deleted', 'کالا حذف شد');

            return redirect(route('product.index'));
        } else {
            return 403;
        }
    }


    public function guaranty()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->guaranty == 1) {

            $guaranties = null;
            return view('admin.Products.guaranty', compact('guaranties'));
        } else {
            return 403;
        }
    }

    public function guarantyinfo($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->guaranty == 1) {
            $guaranty = Output_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $id], ['guaranty_id', '<>', null]])
                ->with('product', 'product.in_products.factor.provider', 'product.guaranty', 'product.out_products.factor.customer', 'factor')->first();;
            return view('admin.Products.guarantyInfo', compact('guaranty'));
        } else {
            return 403;
        }
    }

    public function accept_guaranty(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->guaranty == 1) {

            $guaranty = Output_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id], ['guaranty_id', '<>', null]])
                ->with('product', 'product.in_products.factor.provider', 'product.guaranty', 'product.out_products.factor.customer', 'factor')->first();;
            $accept = new Accepted_Guaranty();
            $accept->customer_id = $guaranty->factor->customer_id;
            $accept->shop_id = Auth::user()->shop_id;
            $accept->output_factor_id = $guaranty->id;
            $accept->provider_id = $request->provider;
            $accept->save();
            $request->session()->put('success','کالا برای ارسال به گارانتی ثبت شد.');
            return redirect(route('guaranty'));
        } else {
            return 403;
        }
    }

    public function guarantyTracking()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->guaranty == 1) {

            $guaranties = Accepted_Guaranty::where([['shop_id', Auth::user()->shop_id], ['customer_delivery', null]])
                ->with('customer', 'provider')->paginate(20);
            $done_guaranties = Accepted_Guaranty::where([['shop_id', Auth::user()->shop_id],['customer_delivery','<>', null]])
                ->with('customer', 'provider')->paginate(20);

            return view('admin.Products.guarantyTracking', compact(['guaranties','done_guaranties']));
        } else {
            return 403;
        }

    }

    public function SetGuarantyTracking($id, $step)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->guaranty == 1) {

            $guaranty = Accepted_Guaranty::where([['shop_id', Auth::user()->shop_id], ['id', $id]])->first();
            switch ($step) {
                case 'send':
                    $guaranty->send_to_guaranty = Carbon::now();
                    break;
                case 'receive':
                    $guaranty->receive_from_guaranty = Carbon::now();
                    break;
                case 'deliver':
                    $guaranty->customer_delivery = Carbon::now();
                    break;
            }
            $guaranty->save();
            Session::flash('success', 'وضعیت گارانتی ثبت شد');
            return redirect(route('guarantyTracking'));
        } else {
            return 403;
        }
    }
    public function dashboard()
    {
        $cats=Category::where('shop_id',Auth::user()->shop_id)->get();
        $cat_qty=count($cats);
        $products=Product::where('shop_id',Auth::user()->shop_id)->get();
        $product_qty=count($products);
        $today=Carbon::now()->toDateString();
        $lastweek=Carbon::now()->subDay(6)->toDateString();
        $sells=Output_factor_pro::whereHas('factor', function ($query) use ($today,$lastweek) {
            $query->wherebetween('f_date', [$lastweek, $today])->where('shop_id',Auth::user()->shop_id);
        })->with('factor')->get();
        $total_offline_sells=array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0);
        $today1=Carbon::now()->subDay(1)->toDateString();
        $today2=Carbon::now()->subDay(2)->toDateString();
        $today3=Carbon::now()->subDay(3)->toDateString();
        $today4=Carbon::now()->subDay(4)->toDateString();
        $today5=Carbon::now()->subDay(5)->toDateString();
        $today6=Carbon::now()->subDay(6)->toDateString();
        $max_offline_sell=0;
        $max_online_sell=0;
        foreach ($sells as $sell){
            switch ($sell->factor->f_date){
                case $today:
                    $total_offline_sells[0]++;
                    break;
                case $today1:
                    $total_offline_sells[1]++;
                    break;
                case $today2:
                    $total_offline_sells[2]++;
                    break;
                case $today3:
                    $total_offline_sells[3]++;
                    break;
                case $today4:
                    $total_offline_sells[4]++;
                    break;
                case $today5:
                    $total_offline_sells[5]++;
                    break;
                case $today6:
                    $total_offline_sells[6]++;
                    break;
            }
        }

        $counter=count($total_offline_sells);
        for ($i=0;$i<$counter;$i++){
            if ($total_offline_sells[$i]>$max_offline_sell)
                $max_offline_sell=$total_offline_sells[$i];
        }

        $online_sells=online_output_pro::whereHas('factor', function ($query) use ($today,$lastweek) {
            $query->wherebetween('f_date', [$lastweek, $today])->where('shop_id',Auth::user()->shop_id);
        })->with('factor')->get();
        $total_online_sells=array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0);
        foreach ($online_sells as $sell){
            switch ($sell->factor->f_date){
                case $today:
                    $total_online_sells[0]++;
                    break;
                case $today1:
                    $total_online_sells[1]++;
                    break;
                case $today2:
                    $total_online_sells[2]++;
                    break;
                case $today3:
                    $total_online_sells[3]++;
                    break;
                case $today4:
                    $total_online_sells[4]++;
                    break;
                case $today5:
                    $total_online_sells[5]++;
                    break;
                case $today6:
                    $total_online_sells[6]++;
                    break;
            }
        }
        $counter=count($total_online_sells);
        for ($i=0;$i<$counter;$i++){
            if ($total_online_sells[$i]>$max_online_sell)
                $max_online_sell=$total_online_sells[$i];
        }
        $most_sold_products=Product::where('shop_id',Auth::user()->shop_id)->orderby('sold_qty','desc')->take(4)->get();
        $low_qty_products=Product::where([['shop_id',Auth::user()->shop_id],['quantity','<',3]])->with('category')->paginate(5);

        return view('admin.Products.dashboard',compact(['low_qty_products','most_sold_products','cat_qty','product_qty','total_offline_sells','total_online_sells','max_offline_sell','max_online_sell']));
    }


}

