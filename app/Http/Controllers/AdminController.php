<?php

namespace App\Http\Controllers;

use App\Accepted_Guaranty;
use App\Account;
use App\Account_type;
use App\Admin_img;
use App\Article;
use App\Article_category;
use App\Cart;
use App\Category;
use App\Checkout;
use App\City;
use App\Comment;
use App\Customer;
use App\Customer_factor;
use App\Delivery;
use App\Delivery_method;
use App\Discount;
use App\Favorite_product;
use App\Guaranty;
use App\Guild;
use App\Income;
use App\Input_Factor;
use App\Input_Factor_Pro;
use App\Login_log;
use App\Logout_log;
use App\MobileCodes;
use App\myCheque;
use App\online_output_factor;
use App\online_output_pro;
use App\otherCheque;
use App\Outgo;
use App\Outgo_Bill;
use App\Outgo_Charak_Service;
use App\Outgo_Other;
use App\Outgo_Salary;
use App\Output_factor;
use App\Output_factor_pro;
use App\Photo;
use App\plan;
use App\Product;
use App\Provider;
use App\Province;
use App\Role;
use App\Service;
use App\Shop;
use App\ShopPermission;
use App\special_Article;
use App\Storage;
use App\StoragePivot;
use App\subGuild;
use App\User;
use App\UserInfo;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Type\Integer;
use Symfony\Component\String\AbstractUnicodeString;

class AdminController extends Controller
{
    public function index()
    {
        if (Auth::user()->shop_id) {
            $wallet = Auth::user()->shop->wallet;
            $pending_money=Checkout::where([['shop_id',Auth::user()->shop_id],['status','pending']])->sum('total_price');
            $customers_qty = Customer::where('shop_id', Auth::user()->shop_id)->count();
            $personnel = User::where('shop_id', Auth::user()->shop_id)->count();
            $products = Product::where('shop_id', Auth::user()->shop_id)->get();
            $product_qty = count($products);
            $today = \Carbon\Carbon::now()->toDateString();
            $lastweek = Carbon::now()->subDay(6)->toDateString();
            $sells = Output_factor_pro::whereHas('factor', function ($query) use ($today, $lastweek) {
                $query->wherebetween('f_date', [$lastweek, $today])->where('shop_id', Auth::user()->shop_id);
            })->with('factor')->get();
            $total_offline_sells = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0);
            $today1 = Carbon::now()->subDay(1)->toDateString();
            $today2 = Carbon::now()->subDay(2)->toDateString();
            $today3 = Carbon::now()->subDay(3)->toDateString();
            $today4 = Carbon::now()->subDay(4)->toDateString();
            $today5 = Carbon::now()->subDay(5)->toDateString();
            $today6 = Carbon::now()->subDay(6)->toDateString();

            foreach ($sells as $sell) {
                switch ($sell->factor->f_date) {
                    case $today:
                        $total_offline_sells[0]++;
                        break;
                    case $today1:
                        $total_offline_sells[1]++;
                        break;
                    case $today2:
                        $total_offline_sells[2]++;
                        break;
                    case $today3:
                        $total_offline_sells[3]++;
                        break;
                    case $today4:
                        $total_offline_sells[4]++;
                        break;
                    case $today5:
                        $total_offline_sells[5]++;
                        break;
                    case $today6:
                        $total_offline_sells[6]++;
                        break;
                }
            }

            $online_sells = online_output_pro::whereHas('factor', function ($query) use ($today, $lastweek) {
                $query->wherebetween('f_date', [$lastweek, $today])->where('shop_id', Auth::user()->shop_id);
            })->with('factor')->get();
            $total_online_sells = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0);
            foreach ($online_sells as $sell) {
                switch ($sell->factor->f_date) {
                    case $today:
                        $total_online_sells[0]++;
                        break;
                    case $today1:
                        $total_online_sells[1]++;
                        break;
                    case $today2:
                        $total_online_sells[2]++;
                        break;
                    case $today3:
                        $total_online_sells[3]++;
                        break;
                    case $today4:
                        $total_online_sells[4]++;
                        break;
                    case $today5:
                        $total_online_sells[5]++;
                        break;
                    case $today6:
                        $total_online_sells[6]++;
                        break;
                }
            }

            $debts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '>', 0]])
                ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '>', 0]])
                ->sum('amount');

            $credits = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '<', 0]])
                ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '<', 0]])
                ->sum('amount');
            $incomes = Income::where('shop_id', Auth::user()->shop_id)->sum('price');
            $outgoes = Outgo::where('shop_id', Auth::user()->shop_id)->sum('price');
            $weekly_incomes = Income::where('shop_id', Auth::user()->shop_id)->wherebetween('created_at', [$lastweek, $today])->get();
            $weekly_outgoes = Outgo::where('shop_id', Auth::user()->shop_id)->wherebetween('created_at', [$lastweek, $today])->get();
            $total_weekly_earn = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0);
            $total_weekly_outgo = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0);

            foreach ($weekly_incomes as $income) {
                $d = Carbon::parse($income->created_at)->toDateString();
                switch ($d) {
                    case $today:
                        $total_weekly_earn[0] += $income->price;
                        break;
                    case $today1:
                        $total_weekly_earn[1] += $income->price;
                        break;
                    case $today2:
                        $total_weekly_earn[2] += $income->price;
                        break;
                    case $today3:
                        $total_weekly_earn[3] += $income->price;
                        break;
                    case $today4:
                        $total_weekly_earn[4] += $income->price;
                        break;
                    case $today5:
                        $total_weekly_earn[5] += $income->price;
                        break;
                    case $today6:
                        $total_weekly_earn[6] += $income->price;
                        break;
                }
            }
            $max_income = max($total_weekly_earn);

            foreach ($weekly_outgoes as $outgo) {
                $dd = Carbon::parse($outgo->created_at)->toDateString();
                switch ($dd) {
                    case $today:
                        $total_weekly_outgo[0] += $outgo->price;
                        break;
                    case $today1:
                        $total_weekly_outgo[1] += $outgo->price;
                        break;
                    case $today2:
                        $total_weekly_outgo[2] += $outgo->price;
                        break;
                    case $today3:
                        $total_weekly_outgo[3] += $outgo->price;
                        break;
                    case $today4:
                        $total_weekly_outgo[4] += $outgo->price;
                        break;
                    case $today5:
                        $total_weekly_outgo[5] += $outgo->price;
                        break;
                    case $today6:
                        $total_weekly_outgo[6] += $outgo->price;
                        break;
                }
            }
            $max_outgo = max($total_weekly_outgo);
            if ($max_income > $max_outgo)
                $max_weekly_in_out = $max_income;
            else
                $max_weekly_in_out = $max_outgo;

            return view('admin.dashboard', compact(['personnel','pending_money', 'wallet', 'credits', 'debts', 'product_qty', 'customers_qty', 'total_offline_sells', 'total_online_sells', 'incomes', 'outgoes', 'max_weekly_in_out', 'total_weekly_outgo', 'total_weekly_earn']));

        } else {
            return view('admin.Profile.profile');
        }

    }

    public function ajax(Request $request)
    {
        switch ($request->do) {
            case 'get-product':
                $product = Product::where([['shop_id',Auth::user()->shop_id],['id', $request->id]])->first();
                return response()->json($product, 200);
                break;

        }
    }

    public function frontAjax(Request $request)
    {
        switch ($request->do) {
            case 'del-card-pro':
                $cart = Session::has('cart') ? Session::get('cart') : null;
                $items = $cart['items'];
                foreach ($items as $key => $item) {
                    if ($item->id == $request->id) {
                        unset($items[$key]);
                        $cart['totalQty'] -= $item->quantity;
                        $cart['totalPrice'] -= $item->sell_price * $item->quantity;
                    }
                }
                $cart['items'] = $items;

                $request->session()->put('cart', $cart);
//                return  $cart['items'];
                return response($cart,200);
                break;

            case 'inc-card-pro':
                $cart = Session::has('cart') ? Session::get('cart') : null;
                foreach ($cart['items'] as $item) {
                    if ($item->id == $request->id) {
                        $item->quantity++;
                        $cart['totalQty']++;
                        $cart['totalPrice'] += $item->sell_price;
                    }
                }

                $request->session()->put('cart', $cart);
//                return  $cart['items'];
                return $cart;
                break;

            case 'dec-card-pro':
                $cart = Session::has('cart') ? Session::get('cart') : null;
                foreach ($cart['items'] as $item) {
                    if ($item->id == $request->id)
                        if ($item->quantity > 0) {
                            $item->quantity--;
                            $cart['totalQty']--;
                            $cart['totalPrice'] -= $item->sell_price;
                        }
                }

                $request->session()->put('cart', $cart);
//                return  $cart['items'];
                return $cart;
                break;

            case'add-to-cart':
                $product = Product::find($request->id);
                $product->quantity = $request->qty;
                if (Session::has('cart')) {
                    $card = Session::get('cart');
                    foreach ($card['items'] as $item) {
                        if ($item->id == $request->id) {
                            $item->quantity += $request->qty;
                            $card['totalQty'] += $request->qty;
                            $card['totalPrice'] += $product->sell_price * $request->qty;
                            $request->session()->put('cart', $card);
                            return $card;
                        }
                    }
                    array_push($card['items'], $product);
                    $card['totalQty'] += $request->qty;
                    $card['totalPrice'] += $product->sell_price * $request->qty;
                    $request->session()->put('cart', $card);
                    return $card;
                } else {
                    $card = array("items" => array($product), "totalPrice" => $product->sell_price * $request->qty, "totalQty" => $request->qty, "deliveryPrice" => 0, "deliveryMethod" => 0, "discount" => 0);
                    Session::put('cart', $card);
                    return $card;
                }
                break;

            case'get-category-childs':
                $cats = Category::where([['shop_id', '0'], ['parent_id', $request->menu_id]])->get();
                if (count($cats) > 0) {

                    $a = null;
                    $data_tab = 1;
                    foreach ($cats as $cat) {
                        $a = $a . '<li>
                            <a id="' . $cat->id . '" data-tab="' . $data_tab . '" data-query="" href="' . route('advanceCategory', ['همه', 'همه', $cat->title]) . '">' . $cat->title . '</a>
                         </li>';
                        $data_tab++;
                    }
                    $data = '<ul class="sub-menu is-mega-menu-con is-product-mega-menu container exclude">
                            <li class="tabs">
                                <ul>' . $a . '

                                </ul>
                            </li>
                            <li class="contents">
                            </li>
                       </ul>';
                    $res = array('data' => $data, 'status' => true, 'type' => 'product');
                    return $res;
                } else {
                    return null;
                }
                break;

            case'get-cat-products':
                $products = Product::where([['cat_id', $request->cat_id], ['trash', 0]])->with('photo')->take(5)->get();
                if (count($products) > 0) {
                    $a = "";
                    foreach ($products as $product) {
                        if ($product->photo)
                            $path = asset('images/products/' . $product->photo->path);
                        else
                            $path = asset('img/default.jpg');

                        $a = ' <article class="product-item">
                    <a href="' . route('singleProduct', [$product->slug, $product->id]) . '" >
                    <figure class="optimized-1-1">
                        <img src="' . $path . '" alt="">
                    </figure>
                    <h2 class="title">' . $product->name . '</h2>
                    <h6 class="price">
                    <span class="woocommerce-Price-amount amount">
                    <bdi>
                    <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;' . $product->sell_price . '</bdi>
                    </span>
                    </h6>
                    </a>
                    </article>';
                    }
                    $content = "<div class=\"owl-carousel tab-" . $request->tab . " owl-hidden\" data-owl-options='{\"items\":4,\"rtl\":true,\"dots\":false,\"nav\":true}'>" . $a . '</div>';
                    $data = array('classAttr' => $request->tab, 'htmlContent' => $content);
                    $res = array('data' => $data, 'status' => true);

                } else {
                    $a = '<h5 class="mx-5 bg-light p-2 text-muted rounded">در این دسته هنوز کالایی وجود ندارد</h5>';
                    $content = "<div class=\"owl-carousel tab-" . $request->tab . " owl-hidden\" data-owl-options='{\"items\":2,\"rtl\":true,\"dots\":false,\"nav\":true}'>" . $a . '</div>';
                    $data = array('classAttr' => $request->tab, 'htmlContent' => $content);
                    $res = array('data' => $data, 'status' => true);
                }
                return $res;
                break;

            case'get-products-for-carousel':
                $products = Product::where([['trash', 0], ['cat_id', $request->cat_id]])->take(5)->get();
                if (count($products) > 0) {
                    $path = null;
                    $content = null;
                    foreach ($products as $product) {
                        if ($product->photo)
                            $path = asset('images/products/' . $product->photo->path);
                        else
                            $path = asset('img/product-placeholder.png');
                        $content .= '
      <div id=\"car-item-6ff671a-0\">
          <article class="w-p-item product-type-variable product-item-style-1">
          <a href="' . route('singleProduct', [$product->slug, $product->id]) . '">
          <figure class=\"thumbnail\">
              <img src="' . $path . '"   alt="' . $product->name . '">
          </figure>
          </a>
          <div class=\"info\">
              <a href="' . route('singleProduct', [$product->slug, $product->id]) . '">
                  <h6 class="title ">' . $product->name . '</h6>
              </a>
              <div class=\"price\">
              <span class="woocommerce-Price-amount">
                  <bdi>
                      <span class=\"woocommerce-Price-currencySymbol\">تومان</span>
                      &nbsp;' . number_format($product->sell_price) . '
                  </bdi>
                  </span>
          </div>
          </div>
          </article>
      </div>';
                    }
                    $data = array('archive' => "", 'content' => $content);
                    $res = array('status' => true, 'data' => $data);
                } else {
                    $content = '
      <div id=\"car-item-6ff671a-0\">
          <article class="w-p-item product-type-variable product-item-style-1">
          <a href="">
          <figure class=\"thumbnail\">
              <img src=""   alt="">
          </figure>
          </a>
          <div class=\"info\">
              <a href="">
                  <h3 class="title ">بدون محصول !</h3>
              </a>
          </div>
          </article>
      </div>';
                    $data = array('archive' => "", 'content' => $content);
                    $res = array('status' => true, 'data' => $data);
                }
                return $res;
                break;

            case'product_quick_view':
                $product = Product::where('id', $request->id)->first();
                $path = null;
                if ($product->photo)
                    $path = asset('images/products/' . $product->photo->path);
                else
                    $path = asset('newFront/images/product-placeholder.png');

                $data = ' <div class="cb-qv-modal-content p-5">
                <div class="row align-items-center">
                <div class="col-lg-5 thumbnail mb-5 mb-lg-0">
                <div id="cbQVModalCarousel" class="carousel dark" data-ride="carousel">
                <div class="carousel-inner">
                <div class="carousel-item active">
                <img class="d-block w-100" src="' . $path . '">
                </div>
                </div>

                </div>
                </div>
                <div class="col-lg info">
                <h2 class="ribbs">
                <div class="product-single-ribbons">
                <div class="ribbons">
                <div>
                <span style="background-color: #81d742" title="2.8" class="item">
                <i class="fal fa-percent"></i>
                <span> ' . $product->rate . '</span>
                </span>
                </div>
                </div>
                </div>
                </h2>
                <h2 class="title">' . $product->name . '</h2>
                <h6 class="sub-title"></h6>
                <div class="price">
                <ins><span class="woocommerce-Price-amount amount"><bdi>
                <span class="woocommerce-Price-currencySymbol">تومان</span>&nbsp;' . number_format($product->sell_price) . '</bdi>
                </span>
                </ins>
                </div>
                <div class="add-to-cart text-left">
                <a class="cb-add-to-cart btn btn-primary" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" data-original-title="افزودن به سبد خرید" data-offset="99999" data-id="' . $product->id . '">افزودن به سبد خرید</a>
                <a class="btn btn-transparent" href="' . route('singleProduct', [$product->slug, $product->id]) . '"> مشاهده محصول  </a>
                </div>
                </div>
                </div>
                </div>';
                $res = array('status' => true, 'data' => $data);
                return $res;
                break;

            case'get-shops-by-province':
                $shops = Shop::where([['province_id', $request->province], ['city_id', $request->city]])->with('logo', 'subguild', 'guild')->get();
                return $shops;
                break;

            case'get-shops-by-guild-city':
                $shops = Shop::where([['guild_id', $request->id], ['city_id', $request->city_id]])->with('logo', 'subguild', 'guild')->get();
                $subguilds = subGuild::where('parent_id', $request->id)->get();
                $data = array('shops' => $shops, 'subguilds' => $subguilds, 'subguild' => count($subguilds));
                return $data;
                break;

            case'get-shops-by-guild':
                $shops = Shop::where('guild_id', $request->id)->with('logo', 'subguild', 'guild')->get();
                return $shops;
                break;

            case'get-shops-by-subguild':
                $shops = Shop::where([['subguild_id', $request->id], ['city_id', $request->city_id]])->with('logo', 'subguild')->get();
                return $shops;
                break;

            case 'get-cities':
                $cities = City::where('province', $request->province_id)->get();
                return $cities;
                break;

            case'get-products-by-cat':
                $products = Product::where('cat_id', $request->id)->with('category')->paginate(18);
                return $products;
                break;

            case'get-articles-by-cat':
                $posts = Article::where('cat_id', $request->id)->with('category', 'photo')->paginate(18);
                return $posts;
                break;

            case'get-products-by-cat-id':
                $products = Product::where([['shop_id', $request->shop_id], ['cat_id', $request->cat_id]])->paginate(18);
                return $products;
                break;

            case'get-guilds-by-province':
                $id = $request->province_id;
                $cities = City::where('province', $request->province_id)->get();
                $guilds = Guild::with('shops')->whereHas('shops', function ($query) use ($id) {
                    $query->where('province_id', $id);
                })->get();
                $shops = Shop::where('province_id', $id)->get();
                $data = array('cities' => $cities, 'guilds' => count($guilds), 'shops' => count($shops));
                return $data;
                break;

            case'get-guilds-by-city':
                $id = $request->city_id;
                $guilds = Guild::with('shops')->whereHas('shops', function ($query) use ($id) {
                    $query->where('city_id', $id);
                })->get();
                $shops = Shop::where('city_id', $id)->get();
                $data = array('guild' => $guilds, 'guilds' => count($guilds), 'shops' => count($shops));
                return $data;
                break;

            case'get-products-fot-category-page':
                return redirect(route('advanceCategory', [$request->province, $request->city, $request->cat]));

            /*$province=$request->province;
            $city=$request->city;
            $cat=$request->cat;
            if ($request->province=="همه")
                $province="";
            if ($request->city=="همه")
                $city="";
            if ($request->cat=="همه")
                $cat="";
            $products=Product::with('photo','shop.province','shop.city','category')
               ->whereHas('shop.province', function ($query) use ($province) {
                    $query->where('name', 'LIKE', '%' . $province . '%');
                })
                ->whereHas('shop.city', function ($query) use ($city) {
                    $query->where('name', 'LIKE', '%' . $city . '%');
                })
                ->whereHas('category', function ($query) use ($cat) {
                    $query->where('title', 'LIKE', '%' . $cat . '%');
                })
                ->paginate(9);
            return $products;*/

            case'search-products':
                $qty = null;
                if ($request->stuck == '')
                    $qty = -1;
                else
                    $qty = 0;

                if ($request->cat == '') {
                    $products = Product::where([['trash', 0], ['name', 'LIKE', '%' . $request->s . '%'],
                        ['quantity', '>', $qty],
                    ])->with('photo', 'category')->get();
                } else {
                    $products = Product::where([['trash', 0], ['name', 'LIKE', '%' . $request->s . '%'],
                        ['cat_id', $request->cat],
                        ['quantity', '>', $qty],
                    ])->with('photo', 'category')->get();
                }

                $pros = array();
                $path = null;
                foreach ($products as $product) {
                    if ($product->photo)
                        $path = asset('images/products/' . $product->photo->path);
                    else
                        $path = asset('img/product-placeholder.png');

                    $arr = array('id' => $product->id, 'title' => $product->name, 'price' => number_format($product->sell_price) . ' ' . 'تومان', 'url' => route('singleProduct', [$product->slug, $product->id]), 'thumb' => $path);
                    array_push($pros, $arr);
                }
                $res = array('status' => true, 'data' => $pros);
                return $res;
                break;

            case 'like-product':
                if (Auth::user()) {
                    $faved = new Favorite_product();
                    $faved->user_id = Auth::id();
                    $faved->product_id = $request->id;
                    $faved->save();
                }
                if (Session::has('favorite')) {
                    $favorites = Session::get('favorite');
                    array_push($favorites, $request->id);
                    Session::put('favorite', $favorites);
                } else {
                    $favorites = array($request->id);
                    Session::put('favorite', $favorites);
                }
                $data = array('status' => true, 'status_code' => 1, 'data' => "محصول مورد نظر به علاقه مندی ها اضافه شد");
                return $data;
                break;

            case'unlike-product':
                if (Auth::user()) {
                    $faved = Favorite_product::where([['user_id', Auth::id()], ['product_id', $request->id]])->first();
                    $faved->delete();
                }
                $favorites = Session::get('favorite');
                $index = array_search($request->id, $favorites);
                foreach ($favorites as $item) {
                    if ($item == $request->id) {
                        unset($favorites[$index]);
                    }
                }
                Session::put('favorite', $favorites);
                $data = array('status' => true, 'status_code' => 2, 'data' => "محصول مورد نظر از علاقه مندی ها حذف شد");
                return $data;
                break;
        }
    }

    public function webAjax(Request $request)
    {
        switch ($request->do) {
            case 'get-products-in-storage':
                $products_in_storage = StoragePivot::where([['shop_id',Auth::user()->shop_id],['product_id', $request->id]])->first();
                return response()->json($products_in_storage, 200);
                break;
            case'checkout-request':
                $wallet = Auth::user()->shop->wallet;
                if ($wallet > 0) {
                    $chekout = new Checkout();
                    $chekout->shop_id = Auth::user()->shop_id;
                    $chekout->total_price = $wallet;
                    $chekout->card_number=$request->card;
                    $chekout->card_owner=$request->owner;
                    $chekout->save();
                    Auth::user()->shop->wallet = 0;
                    Auth::user()->shop->save();
                    $pending_money=Checkout::where([['shop_id',Auth::user()->shop_id],['status','pending']])->sum('total_price');

                    return response(json_encode(['status'=>'done','pending'=>$pending_money]));
                } else {
                    return response(json_encode(['status'=>'fail']));
                }
                break;

            case 'search-provider':
                $req = $request->key;
                if ($req) {
                    $providers = Provider::where('belong_to', Auth::user()->shop_id)->where(function ($query) use ($req) {
                        $query->where('name', 'LIKE', '%' . $req . '%')
                            ->orWhere('family', 'LIKE', '%' . $req . '%');
                    })->get();
                    $resp = array();
                    foreach ($providers as $provider) {
                        $arr = array('id' => $provider->id, 'name' => $provider->name, 'family' => $provider->family);
                        array_push($resp, $arr);
                    }
                } else {
                    $resp = "";
                }
                return json_encode($resp);
                break;

            case 'search-cat':
                $req = $request->key;
                $cats = Category::where([['shop_id', Auth::user()->shop_id], ['title', 'LIKE', '%' . $req . '%']])->
                orwhere([['shop_id', 0], ['title', 'LIKE', '%' . $req . '%']])->get();
                if ($cats != null || count($cats) > 0) {
                    $resp = array();
                    foreach ($cats as $cat) {
                        $arr = array('id' => $cat->id, 'title' => $cat->title);
                        array_push($resp, $arr);
                    }
                    return json_encode($resp);
                } else {
                    return response()->json('NotFound');
                }
                break;

            case 'set-purchase':
                $factor = Input_Factor::where([['shop_id',Auth::user()->shop_id],['f_num', $request->id]])->with('provider')->first();
                $providerAcc = Account::where([['shop_id',Auth::user()->shop_id],['owner_id', $factor->provider_id]])->first();
                $account = Account::where([['shop_id', Auth::user()->shop_id], ['id', $request->account_id]])->first();
                if ($account->amount < $request->price) {
                    return response()->json('ERROR');
                } else {
                    $sum = $request->paid + $request->price;
                    if ($sum <= $factor->total_price) {
                        $outgo = new Outgo();
                        $outgo->type = 'offline';
                        $outgo->shop_id = Auth::user()->shop_id;
                        $outgo->target_type = Input_Factor::class;
                        $outgo->target_id = $factor->f_num;
                        $outgo->source_id = $request->account_id;
                        $outgo->source_type = Account::class;
                        $outgo->user_id = Auth::id();
                        $outgo->price = $request->price;
                        $outgo->save();
                        $account->amount -= $outgo->price;
                        $account->save();
                        $providerAcc->amount =$providerAcc->amount - $outgo->price;
                        $providerAcc->save();
                        if ($factor->total_price===$sum){
                            $factor->pay_status='paid';
                            $factor->save();
                        }
                        return $outgo;
                    } else {
                        return response()->json('Overflow ERRRRRROOOOOR');
                    }
                }
                break;

            case 'sell':
                $exist_product = StoragePivot::where('product_id', $request->product_id)->with('storage')->get();
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->product_id]])->first();
                $products_in_shop = $product->quantity;
                $products_in_storage = null;
                if (count($exist_product) > 0) {
                    $products_in_storage = 0;
                    foreach ($exist_product as $item) {
                        $products_in_storage += $item->quantity;
                    }
                }
                if ($request->qty > $product->quantity + $products_in_storage) {
                    $resp = array('status' => 0, 'content' => null);
                    return json_encode($resp);
                }

//                $products_in_shop = $product->quantity - $products_in_storage;
                if ($request->qty > $products_in_shop) {
                    $resp = array('status' => 1, 'content' => $exist_product);
                    return json_encode($resp);
                } else {
                    $sell_factor = Output_factor::where([['f_num', $request->f_num], ['shop_id', Auth::user()->shop_id]])->first();
                    $customer = Customer::where('id', $request->customer_id)->first();
                    if ($customer == null) {
                        $customer = new Customer();
                        $customer->name = $request->customer_name;
                        $customer->address = "-";
                        $customer->shop_id = Auth::user()->shop_id;
                        $customer->save();
                        $customerAcc = new Account();
                        $customerAcc->shop_id = $customer->shop_id;
                        $customerAcc->name = $customer->name;
                        $type = Account_type::where('name', Customer::class)->first();
                        $customerAcc->owner_type = Customer::class;
                        $customerAcc->owner_id = $customer->id;
                        $customerAcc->type_id = $type->id;
                        $customerAcc->amount = 0;
                        $customerAcc->save();
                    } else {
                        $customerAcc = Account::where('owner_id', $customer->id)->first();
                    }
                    if ($sell_factor == null) {
                        $sell_factor = new Output_factor();
                        $sell_factor->shop_id = Auth::user()->shop_id;
                        $sell_factor->type = 'offline';
                        $sell_factor->total_price = 0;
                        $sell_factor->f_num = $request->f_num;
                        if (strpos($request->f_date, '/') !== false) {
                            $orderdate = explode('/', $request->f_date);
                            function engNum($string)
                            {
                                $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                                $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                                $output = str_replace($persian, $english, $string);
                                return $output;
                            }

                            $month = (int)engNum($orderdate[1]);
                            $day = (int)engNum($orderdate[2]);
                            $year = (int)engNum($orderdate[0]);
                            $jalaliDate = Verta::getGregorian($year, $month, $day);
                            $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                        } else {
                            $milDate = Carbon::instance(Verta::parse($request->factor_date)->datetime());
                        }
                        $sell_factor->f_date = $milDate;
                        $sell_factor->customer_id = $customer->id;
                        $sell_factor->save();

                    }
                    $factor_pro = new Output_factor_pro();
                    $factor_pro->shop_id = Auth::user()->shop_id;
                    $factor_pro->factor_id = $sell_factor->id;
                    $factor_pro->product_id = $product->id;
                    $factor_pro->price = $request->sell_price;
                    $factor_pro->qty = $request->qty;

                    if ($request->guaranty_accept == true)
                        $factor_pro->guaranty_id = $request->guaranty_id;

                    $factor_pro->save();
                    $sell_factor->total_price += ($factor_pro->price * $factor_pro->qty);
                    $sell_factor->save();
                    $customerAcc->amount -= $factor_pro->price * $factor_pro->qty;
                    $customerAcc->save();
                    $product->quantity -= $request->qty;
                    $product->sold_qty++;
                    $product->save();

                    $arr = array('id' => $factor_pro->id, 'customer_id' => $customer->id, 'name' => $factor_pro->product->name, 'qty' => $factor_pro->qty, 'price' => $factor_pro->price);
                    $resp = array('status' => 2, 'content' => $arr);
                    return json_encode($resp);
                }
                break;

            case 'search-customer':
                $req = $request->key;
                $customers = Customer::where('shop_id', Auth::user()->shop_id)->
                where(function ($query) use ($req) {
                    $query->where('name', 'LIKE', '%' . $req . '%')
                        ->orWhere('family', 'LIKE', '%' . $req . '%');
                })->get();
                $resp = array();
                foreach ($customers as $customer) {
                    $arr = array('id' => $customer->id, 'name' => $customer->name, 'family' => $customer->family);
                    array_push($resp, $arr);
                }
//                return response()->json(array('msg' => $resp), 200);
                return json_encode($resp);
                break;

            case 'get-product':
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->id], ['trash', 0]])->with('guaranty')->first();
                return response()->json($product, 200);
                break;

            case 'output_factor_info':
                $output_factor = Output_factor::where('f_num', $request->id)->with('customer')->first();
                if ($output_factor != null) {
                    $factor_pros = Output_factor_pro::where('f_num', $output_factor->f_num)->get();
                    $resp = array();
                    foreach ($factor_pros as $factor_pro) {
                        $arr = array('name' => $factor_pro->product->name, 'price' => $factor_pro->price, 'qty' => $factor_pro->qty);
                        array_push($resp, $arr);
                    }
                    return response()->json(array('subItem' => $resp, 'date' => $output_factor->f_date, 'customer' => $output_factor->customer->name), 200);
//                    return json_encode($resp);
                }
                return response()->json('nist');

                break;

            case 'get-payment':
                $factor = Output_factor::where([['f_num', $request->id], ['shop_id', Auth::user()->shop_id]])->first();
                $customer = Customer::where('id', $request->customer_account)->first();
                $account = Account::where([['id', $request->account_id], ['shop_id', Auth::user()->shop_id]])->first();
                $customerAcc = Account::where([['owner_id', $customer->id], ['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class]])->first();
                $sum = $request->paid + $request->price;
                if ($sum <= $factor->total_price) {
                    $income = new Income();
                    $income->type = 'offline';
                    $income->shop_id = Auth::user()->shop_id;
                    $income->user_id = Auth::id();
                    $income->source_type = 'فروش محصول';
                    $income->source_id = $factor->f_num;
                    $income->target_id = $account->id;
                    $income->target_type = Account::class;
                    $income->price = $request->price;
                    $income->save();

                    $account->amount += $request->price;
                    $account->save();
                    $customerAcc->amount += $request->price;
                    $customerAcc->save();
                    return $income;
                } else {
                    return response()->json('overflow EERROORR');
                }
                break;

            case 'search-product-guaranty':
                $customer_name = '';
                $product_name = '';
                if ($request->customer_name != null)
                    $customer_name = $request->customer_name;
                if ($request->product_name != null)
                    $product_name = $request->product_name;

                $responses = Output_factor_pro::with('product', 'factor.customer')->where('shop_id',Auth::user()->shop_id)->whereHas('product', function ($query) use ($product_name) {
                    $query->where('name', 'LIKE', '%' . $product_name . '%');
                })->WhereHas('factor.customer', function ($query) use ($customer_name) {
                    $query->where('name', 'LIKE', '%' . $customer_name . '%')
                        ->orwhere('family', 'LIKE', '%' . $customer_name . '%');
                })->get();
                return $responses;
                break;

            case 'guaranty-tracking':
                $customer_name = '';
                $product_name = '';
                if ($request->customer_name != null)
                    $customer_name = $request->customer_name;
                if ($request->product_name != null)
                    $product_name = $request->product_name;

                $customers = Accepted_Guaranty::with('customer', 'provider', 'output_factor.product')->whereHas('output_factor.product', function ($query) use ($product_name) {
                    $query->where('name', 'LIKE', '%' . $product_name . '%');
                })->WhereHas('customer', function ($query) use ($customer_name) {
                    $query->where('name', 'LIKE', '%' . $customer_name . '%')
                        ->orwhere('family', 'LIKE', '%' . $customer_name . '%');
                })->get();

                /* $customers = Accepted_Guaranty::with('customer','provider','output_factor')->whereHas('customer', function ($query) use ($customer_name) {
                     $query->where('name', 'LIKE', '%' . $customer_name . '%')
                         ->orwhere('family', 'LIKE', '%' . $customer_name . '%');
                 })->get();*/
                return $customers;
                break;

            case 'guaranty-tracking-set-step':
                $guaranty = Accepted_Guaranty::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                switch ($request->step) {
                    case 'send':
                        $guaranty->send_to_guaranty = Carbon::now();
                        break;
                    case 'receive':
                        $guaranty->receive_from_guaranty = Carbon::now();
                        break;
                    case 'deliver':
                        $guaranty->customer_delivery = Carbon::now();
                        break;
                }
                $guaranty->save();
                return $guaranty;
                break;

            case 'get-cities':
                $cities = City::where('province', $request->province)->get();
                return $cities;
                break;

            case 'get-product-from-storage':
                $products = Product::where([['shop_id', Auth::user()->shop_id], ['storage_id', $request->id]])->get();
                if (count($products) > 0)
                    return $products;
                else
                    return 0;
                break;

            case 'set-logout':
                $log = new Logout_log();
                $log->user_id = Auth::id();
                $log->shop_id = Auth::user()->shop_id;
                $log->logout_time = Carbon::now();
                $log->save();
                return $log;
                break;

            case'search-user':
                $user_name = '';
                if ($request->user_name != null)
                    $user_name = $request->user_name;
                $users = User::where('shop_id', Auth::user()->shop_id)->where(function ($query) use ($user_name) {
                    $query->where('name', 'LIKE', '%' . $user_name . '%')
                        ->orWhere('family', 'LIKE', '%' . $user_name . '%');
                })->get();
                return $users;
                break;

            case'search-user-logs':
                $start = null;
                $end = null;
                if ($request->start != "" && $request->end != "") {

                    if (strpos($request->start, '/') !== false) {
                        $orderdate = explode('/', $request->start);
                        function engNum($string)
                        {
                            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                            $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                            $output = str_replace($persian, $english, $string);
                            return $output;
                        }

                        $month = (int)engNum($orderdate[1]);
                        $day = (int)engNum($orderdate[2]);
                        $year = (int)engNum($orderdate[0]);
                        $jalaliDate = Verta::getGregorian($year, $month, $day);
                        $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                    } else {
                        $milDate = Carbon::instance(Verta::parse($request->start_date)->datetime());
                    }
                    $start = $milDate;
                    $milDate = null;
                    if (strpos($request->end, '/') !== false) {
                        $orderdate = explode('/', $request->end);
                        function engNum1($string)
                        {
                            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                            $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                            $output = str_replace($persian, $english, $string);
                            return $output;
                        }

                        $month = (int)engNum1($orderdate[1]);
                        $day = (int)engNum1($orderdate[2]);
                        $year = (int)engNum1($orderdate[0]);
                        $jalaliDate = Verta::getGregorian($year, $month, $day);
                        $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                    } else {
                        $milDate = Carbon::instance(Verta::parse($request->start_date)->datetime());
                    }
                    $end = $milDate;
                }
                if ($start == null || $end == null) {
                    $logins = Login_log::where([['shop_id', Auth::user()->shop_id], ['user_id', $request->id]])->with('user')->get();
                    $logouts = Logout_log::where([['shop_id', Auth::user()->shop_id], ['user_id', $request->id]])->with('user')->get();
                } else {
                    $logins = Login_log::where([['shop_id', Auth::user()->shop_id], ['user_id', $request->id]])
                        ->wherebetween('login_time', [$start, $end])
                        ->with('user')->get();
                    $logouts = Logout_log::where([['shop_id', Auth::user()->shop_id], ['user_id', $request->id]])
                        ->wherebetween('logout_time', [$start, $end])
                        ->with('user')->get();
                }

                $diffs = array();
                $counter = count($logouts);
                for ($i = 0; $i < $counter; $i++) {
                    $in = $logins[$i]->login_time;
                    $out = $logouts[$i]->logout_time;
                    $in = verta($in);
                    $out = verta($out);
                    $diff = $in->diffMinutes($out);
                    array_push($diffs, $diff);
                }
                $logs = array('logins' => $logins, 'logouts' => $logouts, 'diff' => $diffs);
                return $logs;
                break;

            case'set-user-info':
                $info = new UserInfo();
                $info->user_id = Auth::id();
                $info->phone = $request->phone;
                $info->province_id = $request->province;
                $info->city_id = $request->city;
                $info->address = $request->address;
                $info->save();
                Auth::user()->user_info_id = $info->id;
                Auth::user()->save();
                return $info;
                break;

            case 'set-factor-status':
                $factor = online_output_factor::where('id', $request->factor)->with('customer')->first();
                $factor->send_date = Carbon::now();
                if (!empty($request->tracking)) {
                    $factor->tracking = $request->tracking;
                    Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                        'receptor' => $factor->customer->mobile,
                        'token' => \verta(now())->format('Y/n/j-H:i'),
                        'token2' => Auth::user()->shop->name,
                        'token3' => $request->tracking,
                        'template' => '4rakOrderTrackingCode',
                    ]);
                } elseif (!empty($request->mobile)) {
                    $factor->tracking = $request->mobile;
                    Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                        'receptor' => $factor->customer->mobile,
                        'token' => \verta(now())->format('Y/n/j-H:i'),
                        'token2' => Auth::user()->shop->name,
                        'token3' => $request->mobile,
                        'template' => '4rakOrderPeykPhone',
                    ]);
                }
                $factor->save();
                return $factor;
                break;

            case'update-buy-factor':
                $factor_pro = Input_Factor_Pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->with('factor')->first();
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->product_id]])->first();
                $factor = Input_Factor::where([['shop_id', Auth::user()->shop_id], ['f_num', $factor_pro->factor->f_num]])->with('in_products')->first();
                switch ($request->type) {
                    case'p_name':
                        $product->name = $request->data;
                        $product->save();
                        break;
                    case'p_qty':
                        $factor_pro->qty = $request->data;
                        $factor_pro->save();
                        $factor->total_price = 0;
                        foreach ($factor->in_products as $pro) {
                            $factor->total_price += $factor_pro->qty * $factor_pro->price;
                        }
                        $factor->save();
                        break;
                    case'p_price':
                        $factor_pro->price = $request->data;
                        $factor_pro->save();
                        $factor->total_price = 0;
                        foreach ($factor->in_products as $pro) {
                            $factor->total_price += $factor_pro->qty * $factor_pro->price;
                        }
                        $factor->save();
                        break;
                    case'p_total':

                        break;
                }
                return $factor;
                break;

            case'update-sell-factor':
                $factor_pro = Output_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->with('factor')->first();
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['id', $request->product_id]])->first();
                $factor = Output_factor::where([['shop_id', Auth::user()->shop_id], ['id', $factor_pro->factor->id]])->with('out_products')->first();
                switch ($request->type) {
                    case'p_name':
                        $product->name = $request->data;
                        $product->save();
                        break;
                    case'p_qty':
                        $factor_pro->qty = $request->data;
                        $factor->total_price = 0;
                        foreach ($factor->out_products as $pro) {
                            $factor->total_price += $pro->qty * $pro->price;
                        }
                        $factor_pro->save();
                        $factor->save();
                        break;
                    case'p_price':
                        $factor_pro->price = $request->data;
                        $factor_pro->save();
                        $factor->total_price = 0;
                        foreach ($factor->out_products as $pro) {
                            $factor->total_price += $pro->qty * $pro->price;
                        }
                        $factor->save();
                        break;
                    case'p_total':

                        break;
                }
                return $factor;
                break;

            case'update-sell-factor-pro':
                $factor_pro = Output_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                $factor = Output_Factor::where([['shop_id', Auth::user()->shop_id], ['id', $factor_pro->factor_id]])->first();
                if ($request->qty != "")
                    $factor_pro->qty = (int)$request->qty;
                if ($request->price != "")
                    $factor_pro->price = (int)$request->price;
                $factor_pro->save();
                $factor->total_price = 0;
                foreach ($factor->out_products as $pro) {
                    $factor->total_price += $pro->price * $pro->qty;
                }
                $factor->save();
                return $factor_pro;
                break;

            case'update-buy-factor-pro':
                $factor_pro = Input_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                $factor = Input_Factor::where([['shop_id', Auth::user()->shop_id], ['id', $factor_pro->factor_id]])->first();
                if ($request->qty != "")
                    $factor_pro->qty = (int)$request->qty;
                if ($request->price != "")
                    $factor_pro->price = (int)$request->price;
                $factor_pro->save();
                $factor->total_price = 0;
                foreach ($factor->in_products as $pro) {
                    $factor->total_price += $pro->price * $pro->qty;
                }
                $factor->save();
                return $factor_pro;
                break;

            case'new-product':
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['name', $request->p_name]])->first();
                if ($request->flag == 'savenew') {
                    if ($product == null) {
                        $product = new Product();
                        $product->shop_id = Auth::user()->shop_id;
                        $product->user_id = Auth::id();
                        $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                        if ($cat == null || $cat == "") {
                            $cat = new Category();
                            $cat->shop_id = Auth::user()->shop_id;
                            $cat->title = $request->category;
                            $cat->save();
                        }
                        $product->cat_id = $cat->id;
                        $product->name = $request->p_name;
                        $product->slug = make_slug($request->p_name);
                        $product->status = 'تایید نشده';
                        $product->provider_co ="";
                        $product->sell_price = $request->sell_price;
                        $product->buy_price = $request->buy_price;
                        $product->whole_price = $request->whole_price;
                        $product->quantity = $request->qty;
                        $product->description = $request->description;
                        $product->meta_keywords = $request->keywords;
                        $product->barcode = $request->barcode;
                        $product->save();
                        if ($request->guaranty == "true") {
                            $guaranty = new Guaranty();
                            $guaranty->product_id = $product->id;
                            $guaranty->duration = $request->guaranty_duration;
                            $guaranty->duration_unit = $request->guaranty_unit;
                            $guaranty->start = $request->guaranty_start;
                            $guaranty->save();
                        }
                        $pro_category=Category::find($product->cat_id);
                        return response()->json(['product' => $product,'pro_category'=>$pro_category->title]);
                    } else {
                        return response()->json('new');
                    }
                } else if ($request->flag == 'save') {
                    if ($product == null) {
                        $product = new Product();
                        $product->shop_id = Auth::user()->shop_id;
                        $product->user_id = Auth::id();
                        $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                        if ($cat == null || $cat == "") {
                            $cat = new Category();
                            $cat->shop_id = Auth::user()->shop_id;
                            $cat->title = $request->category;
                            $cat->save();
                        }
                        $product->cat_id = $cat->id;
                        $product->name = $request->p_name;
                        $product->slug = make_slug($request->p_name);
                        $product->status = 'تایید نشده';
                        $product->provider_co ="";
                        $product->sell_price = $request->sell_price;
                        $product->buy_price = $request->buy_price;
                        $product->whole_price = $request->whole_price;
                        $product->quantity = $request->qty;
                        $product->description = $request->description;
                        $product->meta_keywords = $request->keywords;
                        $product->barcode = $request->barcode;
                    } else {
                        $product->quantity += $request->qty;
                    }
                    $product->save();
                    if ($request->guaranty == "true") {
                        $guaranty = new Guaranty();
                        $guaranty->product_id = $product->id;
                        $guaranty->duration = $request->guaranty_duration;
                        $guaranty->duration_unit = $request->guaranty_unit;
                        $guaranty->start = $request->guaranty_start;
                        $guaranty->save();
                    }
                    $pro_category=Category::find($product->cat_id);
                    return response()->json(['product' => $product,'pro_category'=>$pro_category->title]);
                }
                break;

            case'new-input-factor':
                $product = Product::where([['shop_id', Auth::user()->shop_id], ['name', $request->p_name]])->first();
                $factor = Input_Factor::where([['f_num', $request->factor_number],['shop_id',Auth::user()->shop_id]])->with('provider')->first();

                if ($request->flag == 'savenew') {
                    if ($product == null) {
                        $factor = Input_Factor::where([['f_num', $request->factor_number],['shop_id',Auth::user()->shop_id]])->with('provider')->first();
                        $provider = Provider::where([['belong_to', Auth::user()->shop_id], ['id', $request->provider_id]])->first();
                        if ($provider == null) {
                            $provider = new Provider();
                            $provider->belong_to = Auth::user()->shop_id;
                            $provider->name = $request->provider_name;
                            $provider->address = '-';
                            $provider->save();
                            $providerAccount = new Account();
                            $providerAccount->name = $provider->name;
                            $providerAccount->owner_type = Provider::class;
                            $providerAccount->owner_id = $provider->id;
                            $type = Account_type::where('name', Provider::class)->first();
                            $providerAccount->type_id = $type->id;
                            $providerAccount->amount = 0;
                            $providerAccount->shop_id = Auth::user()->shop_id;
                            $providerAccount->save();
                        }

                        if ($factor == null) {
                            $factor = new Input_Factor();
                            $factor->shop_id = Auth::user()->shop_id;
                            $factor->f_num = $request->factor_number;
                            $factor->type = "offline";
                            $factor->provider_id = $provider->id;
                            $factor->total_price = 0;
                            if (strpos($request->factor_date, '/') !== false) {
                                $orderdate = explode('/', $request->factor_date);
                                function engNum($string)
                                {
                                    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                                    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                                    $output = str_replace($persian, $english, $string);
                                    return $output;
                                }

                                $month = (int)engNum($orderdate[1]);
                                $day = (int)engNum($orderdate[2]);
                                $year = (int)engNum($orderdate[0]);
                                $jalaliDate = Verta::getGregorian($year, $month, $day);
                                $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                            } else {
                                $milDate = Carbon::instance(Verta::parse($request->factor_date)->datetime());
                            }
                            $factor_date = $milDate;
                            $factor->f_date = $factor_date;
                            $factor->save();

                            if ($product == null) {
                                $product = new Product();
                                $product->shop_id = Auth::user()->shop_id;
                                $product->user_id = Auth::id();
                                $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                                if ($cat == null || $cat == "") {
                                    $cat = new Category();
                                    $cat->shop_id = Auth::user()->shop_id;
                                    $cat->title = $request->category;
                                    $cat->save();
                                }
                                $product->cat_id = $cat->id;
                                $product->name = $request->p_name;
                                $product->slug = make_slug($request->p_name);
                                $product->status = 'تایید نشده';
                                $product->provider_co = $provider->name . " " . $provider->family;
                                $product->sell_price = $request->sell_price;
                                $product->buy_price = $request->buy_price;
                                $product->whole_price = $request->whole_price;
                                $product->quantity = $request->qty;
                                $product->description = $request->description;
                                $product->meta_keywords = $request->keywords;
                                $product->barcode = $request->barcode;
                            } else {
                                $product->quantity += $request->qty;
                            }
                            $product->save();
                            if ($request->guaranty == "true") {
                                $guaranty = new Guaranty();
                                $guaranty->product_id = $product->id;
                                $guaranty->duration = $request->guaranty_duration;
                                $guaranty->duration_unit = $request->guaranty_unit;
                                $guaranty->start = $request->guaranty_start;
                                $guaranty->save();
                            }

                            $factor->total_price += $product->buy_price * $product->quantity;
                            $factor->save();
                            $providerAccount = Account::where([['shop_id', Auth::user()->shop_id], ['owner_id', $factor->provider_id]])->first();
                            $providerAccount->amount += $factor->total_price;
                            $providerAccount->save();

                            $factor_pro = new Input_Factor_Pro();
                            $factor_pro->factor_id = $factor->id;
                            $factor_pro->shop_id = Auth::user()->shop_id;
                            $factor_pro->product_id = $product->id;
                            $factor_pro->qty = $request->qty;
                            $factor_pro->price = $product->buy_price;
                            $factor_pro->save();

                        } else {
                            if ($product == null) {
                                $product = new Product();
                                $product->shop_id = Auth::user()->shop_id;
                                $product->user_id = Auth::id();
                                $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                                if ($cat == null || $cat == "") {
                                    $cat = new Category();
                                    $cat->shop_id = Auth::user()->shop_id;
                                    $cat->title = $request->category;
                                    $cat->save();
                                }
                                $product->cat_id = $cat->id;
                                $product->name = $request->p_name;
                                $product->slug = make_slug($request->p_name);
                                $product->status = 'تایید نشده';
                                $product->provider_co = $provider->name . " " . $provider->family;
                                $product->sell_price = $request->sell_price;
                                $product->buy_price = $request->buy_price;
                                $product->whole_price = $request->whole_price;
                                $product->quantity = $request->qty;
                                $product->description = $request->description;
                                $product->meta_keywords = $request->keywords;
                                $product->barcode = $request->barcode;
                            } else {
                                $product->quantity += $request->qty;
                            }
                            $product->save();

                            if ($request->guaranty == "true") {
                                $guaranty = new Guaranty();
                                $guaranty->product_id = $product->id;
                                $guaranty->duration = $request->guaranty_duration;
                                $guaranty->duration_unit = $request->guaranty_unit;
                                $guaranty->start = $request->guaranty_start;
                                $guaranty->save();
                                $product->guaranty_id = $guaranty->id;
                                $product->save();
                            }

                            $factor->total_price += $product->buy_price * $product->quantity;
                            $factor->save();

                            $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
                            $providerAccount->amount += $factor->total_price;
                            $providerAccount->save();

                            $factor_pro = new Input_Factor_Pro();
                            $factor_pro->factor_id = $factor->id;
                            $factor_pro->shop_id = Auth::user()->shop_id;
                            $factor_pro->product_id = $product->id;
                            $factor_pro->qty = $product->quantity;
                            $factor_pro->price = $product->buy_price;
                            $factor_pro->save();
                        }
                        return response()->json(['status'=>'null','product' => $product, 'providerId' => $provider->id, 'factorId' => $factor->f_num, 'total_price'=>$factor->total_price]);
                    } else {
                        return response()->json(['status'=>'new','total_price'=>$factor->total_price]);

                    }

                } else if ($request->flag == 'save') {
                    $provider = Provider::where([['belong_to', Auth::user()->shop_id], ['id', $request->provider_id]])->first();
                    if ($provider == null) {
                        $provider = new Provider();
                        $provider->belong_to = Auth::user()->shop_id;
                        $provider->name = $request->provider_name;
                        $provider->address = '-';
                        $provider->save();
                        $providerAccount = new Account();
                        $providerAccount->name = $provider->name;
                        $providerAccount->owner_type = Provider::class;
                        $providerAccount->owner_id = $provider->id;
                        $type = Account_type::where('name', Provider::class)->first();
                        $providerAccount->type_id = $type->id;
                        $providerAccount->amount = 0;
                        $providerAccount->shop_id = Auth::user()->shop_id;
                        $providerAccount->save();
                    }

                    if ($factor == null) {
                        $factor = new Input_Factor();
                        $factor->shop_id = Auth::user()->shop_id;
                        $factor->f_num = $request->factor_number;
                        $factor->type = "offline";
                        $factor->provider_id = $provider->id;
                        $factor->total_price = 0;
                        if (strpos($request->factor_date, '/') !== false) {
                            $orderdate = explode('/', $request->factor_date);
                            function engNum($string)
                            {
                                $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                                $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                                $output = str_replace($persian, $english, $string);
                                return $output;
                            }

                            $month = (int)engNum($orderdate[1]);
                            $day = (int)engNum($orderdate[2]);
                            $year = (int)engNum($orderdate[0]);
                            $jalaliDate = Verta::getGregorian($year, $month, $day);
                            $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                        } else {
                            $milDate = Carbon::instance(Verta::parse($request->factor_date)->datetime());
                        }
                        $factor_date = $milDate;
                        $factor->f_date = $factor_date;
                        $factor->save();

                        if ($product == null) {
                            $product = new Product();
                            $product->shop_id = Auth::user()->shop_id;
                            $product->user_id = Auth::id();
                            $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                            if ($cat == null || $cat == "") {
                                $cat = new Category();
                                $cat->shop_id = Auth::user()->shop_id;
                                $cat->title = $request->category;
                                $cat->save();
                            }
                            $product->cat_id = $cat->id;
                            $product->name = $request->p_name;
                            $product->slug = make_slug($request->p_name);
                            $product->status = 'تایید نشده';
                            $product->provider_co = $provider->name . " " . $provider->family;
                            $product->sell_price = $request->sell_price;
                            $product->buy_price = $request->buy_price;
                            $product->whole_price = $request->whole_price;
                            $product->quantity = $request->qty;
                            $product->description = $request->description;
                            $product->meta_keywords = $request->keywords;
                            $product->barcode = $request->barcode;
                        } else {
                            $product->quantity += $request->qty;
                        }
                        $product->save();
                        if ($request->guaranty == "true") {
                            $guaranty = new Guaranty();
                            $guaranty->product_id = $product->id;
                            $guaranty->duration = $request->guaranty_duration;
                            $guaranty->duration_unit = $request->guaranty_unit;
                            $guaranty->start = $request->guaranty_start;
                            $guaranty->save();
                        }

                        $factor->total_price += $product->buy_price * $product->quantity;
                        $factor->save();
                        $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
                        $providerAccount->amount += $factor->total_price;
                        $providerAccount->save();

                        $factor_pro = new Input_Factor_Pro();
                        $factor_pro->factor_id = $factor->id;
                        $factor_pro->shop_id = Auth::user()->shop_id;
                        $factor_pro->product_id = $product->id;
                        $factor_pro->qty = $request->qty;
                        $factor_pro->price = $product->buy_price;
                        $factor_pro->save();

                    } else {
                        if ($product == null) {
                            $product = new Product();
                            $product->shop_id = Auth::user()->shop_id;
                            $product->user_id = Auth::id();
                            $cat = Category::where([['title', $request->category], ['shop_id', Auth::user()->shop_id]])->orwhere([['title', $request->category], ['shop_id', 0]])->first();
                            if ($cat == null || $cat == "") {
                                $cat = new Category();
                                $cat->shop_id = Auth::user()->shop_id;
                                $cat->title = $request->category;
                                $cat->save();
                            }
                            $product->cat_id = $cat->id;
                            $product->name = $request->p_name;
                            $product->slug = make_slug($request->p_name);
                            $product->status = 'تایید نشده';
                            $product->provider_co = $provider->name . " " . $provider->family;
                            $product->sell_price = $request->sell_price;
                            $product->buy_price = $request->buy_price;
                            $product->whole_price = $request->whole_price;
                            $product->quantity = $request->qty;
                            $product->description = $request->description;
                            $product->meta_keywords = $request->keywords;
                            $product->barcode = $request->barcode;
                        } else {
                            $product->quantity += $request->qty;
                        }
                        $product->save();

                        if ($request->guaranty == "true") {
                            $guaranty = new Guaranty();
                            $guaranty->product_id = $product->id;
                            $guaranty->duration = $request->guaranty_duration;
                            $guaranty->duration_unit = $request->guaranty_unit;
                            $guaranty->start = $request->guaranty_start;
                            $guaranty->save();
                            $product->guaranty_id = $guaranty->id;
                            $product->save();
                        }

                        $factor->total_price += $product->buy_price * $product->quantity;
                        $factor->save();

                        $providerAccount = Account::where('owner_id', $factor->provider_id)->first();
                        $providerAccount->amount += $factor->total_price;
                        $providerAccount->save();

                        $factor_pro = new Input_Factor_Pro();
                        $factor_pro->factor_id = $factor->id;
                        $factor_pro->shop_id = Auth::user()->shop_id;
                        $factor_pro->product_id = $product->id;
                        $factor_pro->qty = $product->quantity;
                        $factor_pro->price = $product->buy_price;
                        $factor_pro->save();
                    }
                    $pro_category=Category::find($product->cat_id);
                    return response()->json(['product' => $product,'pro_category'=>$pro_category->title, 'providerId' => $provider->id, 'factorId' => $factor->f_num,'total_price'=>$factor->total_price]);
                }
                break;

            case'delete-buy-factor-pro':
                $factor_pro = Input_Factor_Pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                $product=Product::where([['shop_id', Auth::user()->shop_id], ['id', $factor_pro->product_id]])->first();
                if($product->quantity > 0){
                    $product->quantity-=$factor_pro->qty;
                    $product->save();
                }
                $factor_pro->delete();
                return response()->json('deleted');
                break;

            case'delete-sell-factor-pro':
                $factor_pro = Output_factor_pro::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->with('factor')->first();
                $factor = $factor_pro->factor;
                $factor->total_price -= $factor_pro->price;
                $factor->save();
                $factor_pro->delete();
                return response()->json('deleted');
                break;

            case'search-category':
                $cat_name = '';
                if ($request->cat_name != null)
                    $cat_name = $request->cat_name;
                $cats = Category::where('shop_id', Auth::user()->shop_id)
                    ->where('title', 'LIKE', '%' . $cat_name . '%')->with('photo')
                    ->paginate(10);
                return $cats;
                break;

            case'search-product-by-name':
                $pro_name = '';
                if ($request->product_name != null) {
                    $pro_name = $request->product_name;
                    $pros = Product::where([['shop_id', Auth::user()->shop_id], ['trash', '0']])
                        ->where('name', 'LIKE', '%' . $pro_name . '%')->with('photo', 'category')
                        ->paginate(10);
                } else {
                    $pros = Product::where([['shop_id', Auth::user()->shop_id], ['trash', '0']])
                        ->with('photo', 'category')
                        ->paginate(10);
                }
                return $pros;
                break;

            case'search-product-by-barcode':
                $barcode = '';
                if ($request->barcode != null) {
                    $barcode = $request->barcode;
                    $pros = Product::where([['shop_id', Auth::user()->shop_id], ['trash', '0']])
                        ->where('barcode', 'LIKE', '%' . $barcode . '%')->with('photo', 'category')
                        ->paginate(10);
                } else {
                    $pros = Product::where([['shop_id', Auth::user()->shop_id], ['trash', '0']])
                        ->with('photo', 'category')
                        ->paginate(10);
                }
                return $pros;
                break;

            case'SetGuarantyTracking':
                $guaranty = Accepted_Guaranty::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                switch ($request->step) {
                    case 'send':
                        $guaranty->send_to_guaranty = Carbon::now();
                        break;
                    case 'receive':
                        $guaranty->receive_from_guaranty = Carbon::now();
                        break;
                    case 'deliver':
                        $guaranty->customer_delivery = Carbon::now();
                        if ($guaranty->receive_from_guaranty == null)
                            $guaranty->receive_from_guaranty = Carbon::now();
                        if ($guaranty->send_to_guaranty == null)
                            $guaranty->send_to_guaranty = Carbon::now();
                        break;
                }
                $guaranty->save();
                return $guaranty;
                break;

            case 'delete-category':
                $cat = Category::with('products')->where('id', $request->cat_id)->first();
                if ($cat->products != null) {
                    return response()->json('forbidden');
                } else {
                    $childern = Category::where('parent_id', $request->cat_id)->get();
                    foreach ($childern as $child) {
                        $child->parent_id = null;
                        $child->save();
                    }
                    $cat->delete();
                }
                return response()->json('success');
                break;

            case 'delete-article-category':
                $cat = Article_category::with('articles')->where('id', $request->id)->first();
                if ($cat->articles != null) {
                    return response()->json('forbidden');
                } else {
                    $childern = Article_category::where('parent_id', $request->id)->get();
                    foreach ($childern as $child) {
                        $child->parent_id = null;
                        $child->save();
                    }
                    $cat->delete();
                }
                return response()->json('success');
                break;

            case'delete-guild' :
                $shop = Shop::where('guild_id', $request->id)->first();
                $subguild = subGuild::where('parent_id', $request->id)->first();
                if ($shop != null || $subguild != null) {
                    return response()->json('forbidden');
                } else {
                    $guild = Guild::find($request->id);
                    $guild->delete();
                }
                return response()->json('success');
                break;

            case'delete-subguild':
                $shop = Shop::where('subguild_id', $request->id)->first();
                if ($shop != null) {
                    return response()->json('forbidden');
                } else {
                    $subguild = subGuild::find($request->id);
                    $subguild->delete();
                }
                return response()->json('success');
                break;

            case'delete-article':
                $article = Article::find($request->id);
                if ($article->photo_id) {
                    $photo = Photo::find($article->photo_id)->first();
                    unlink(public_path() . 'images/articles/' . $photo->path);
                    $photo->delete();
                }
                $article->delete();
                return response()->json('success');


            case 'delete-storage':
                $storages = StoragePivot::where([['storage_id', $request->id], ['quantity', '!=', 0]])->get();
                if (count($storages) > 0) {
                    return response()->json('forbidden');
                } else {
                    $storage = Storage::where('id', $request->id)->first();
                    $storage->delete();
                }
                return response()->json('success');
                break;

            case'delete-delivery-method':
                $method = Delivery_method::where('id', $request->id)->with('photo')->first();
                if ($method->photo) {
                    $photo = Photo::where([['groupable_type', Delivery_method::class], ['groupable_id', $method->id]])->first();
                    unlink(public_path() . '/images/delivery/' . $photo->path);
                    $photo->delete();
                }
                $method->delete();
                return response()->json('success');
                break;

            case'delete-pay-plan':
                $plan = plan::find($request->id);
                $plan->deleted = true;
                $plan->save();
                return response(json_encode('success'));
                break;

            case 'delete-product':
                if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک') {
                    $product = Product::find($request->id);
                    $product->trash = 1;
                    $product->save();
                    return response()->json('success');
                    break;
                } else {
                    return response()->json('Access Denied');
                    break;
                }

            case'search-my-accounts':
                $my_acc = '';
                if ($request->acc_name != null) {
                    $my_acc = $request->acc_name;
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self']])
                        ->where('name', 'LIKE', '%' . $my_acc . '%')
                        ->paginate(10);
                } else {
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self']])->paginate(10);
                }
                return $accs;
                break;

            case'search-provider-accounts':
                $p_acc = '';
                if ($request->acc_name != null) {
                    $p_acc = $request->acc_name;
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'App\Provider']])
                        ->where('name', 'LIKE', '%' . $p_acc . '%')
                        ->paginate(10);
                } else {
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'App\Provider']])->paginate(10);
                }
                return $accs;
                break;

            case'search-customer-accounts':
                $customer_acc = '';
                if ($request->acc_name != null) {
                    $customer_acc = $request->acc_name;
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'App\Customer']])
                        ->where('name', 'LIKE', '%' . $customer_acc . '%')
                        ->paginate(10);
                } else {
                    $accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'App\Customer']])->paginate(10);
                }
                return $accs;
                break;

            case'search-debts':
                if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
                    $debt = '';
                    if ($request->acc_name != null) {
                        $debt = $request->acc_name;
                        $debts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '>', 0]], ['name', 'LIKE', '%' . $debt . '%'])
                            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '>', 0], ['name', 'LIKE', '%' . $debt . '%']])
                            ->paginate(10);
                    } else {
                        $debts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '>', 0]])
                            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '>', 0]])->paginate(10);
                    }
                    return $debts;

                } else {
                    return response('Access Denied');
                }

            case'search-credits':
                if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
                    $credit = '';
                    if ($request->acc_name != null) {
                        $credit = $request->acc_name;
                        $credits = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '<', 0], ['name', 'LIKE', '%' . $credit . '%']])
                            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '<', 0], ['name', 'LIKE', '%' . $credit . '%']])
                            ->paginate(10);
                    } else {
                        $credits = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class], ['amount', '<', 0]])
                            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class], ['amount', '<', 0]])->paginate(10);
                    }
                    return $credits;

                } else {
                    return response('Access Denied');
                }

            case'search-account':
                $account = Account::where([['shop_id', Auth::user()->shop_id], ['id', $request->id],['owner_type','self']])->first();
                return $account;
                break;

            case'search-banks':
                $bank_type=Account_type::where('name','بانک')->first();
                $banks = Account::where([['shop_id', Auth::user()->shop_id], ['type_id', $bank_type->id],['owner_type','self']])->get();
                $res=array('banks'=>$banks);
                return response()->json($res);
                break;

            case 'search-banks-by-id' :
                $bank = Account::where([['shop_id', Auth::user()->shop_id], ['id',$request->id],['owner_type','self']])->first();
                return response()->json($bank);
                break;

            case'get-destination-storage':
                $storages = Storage::where([['shop_id', Auth::user()->shop_id], ['id', '<>', $request->id]])->get();
                $products = StoragePivot::where([['shop_id', Auth::user()->shop_id], ['storage_id', $request->id]])
                    ->with('product', 'storage')
                    ->get();
                $arr = array('storages' => $storages, 'products' => $products);
                return $arr;
                break;

            case'get-pro-from-storage-pivot':
                if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->storage == 1) {
                    $product = StoragePivot::where([['shop_id', Auth::user()->shop_id], ['storage_id', $request->storage_id], ['product_id', $request->product_id]])
                        ->first();
                    return $product;
                } else {
                    return false;
                }
                break;

            case'transfer-strorage-to-storage':
                $destination = StoragePivot::where([['shop_id', Auth::user()->shop_id], ['storage_id', $request->dest_storage_id], ['product_id', $request->product_id]])->first();
                $origin = StoragePivot::where([['shop_id', Auth::user()->shop_id], ['storage_id', $request->origin_storage_id], ['product_id', $request->product_id]])->first();
                if ($destination == null) {
                    $destination = new StoragePivot();
                    $destination->shop_id = Auth::user()->shop_id;
                    $destination->product_id = $request->product_id;
                    $destination->quantity = $request->qty;
                    $destination->storage_id = $request->dest_storage_id;
                } else {
                    $destination->quantity += $request->qty;
                }
                $destination->save();
                $origin->quantity -= $request->qty;
                $origin->save();
                if ($origin->quantity == 0)
                    $origin->delete();
                return response()->json('success');
                break;

            case'search-delivery-m':
                $motors = Delivery::with(['photo', 'province', 'city'])->where([['vehicle', 'موتور'], ['province_id', $request->province_id], ['city_id', $request->city_id]])->paginate(10);
                return $motors;
                break;

            case'search-delivery-c':
                $cars = Delivery::with(['photo', 'province', 'city'])->where([['vehicle', 'وانت'], ['province_id', $request->province_id], ['city_id', $request->city_id]])->paginate(10);
                return $cars;
                break;

            case'search-delivery-t':
                $cars = Delivery::with(['photo', 'province', 'city'])->where([['vehicle', 'کامیون'], ['province_id', $request->province_id], ['city_id', $request->city_id]])->paginate(10);
                return $cars;
                break;

            case'get-account-by-type':
                $accounts = Account::where([['shop_id', Auth::user()->shop_id], ['type_id', $request->id]])->get();
                return $accounts;
                break;

            case'get-other-cheque':
                $cheques = otherCheque::where([['shop_id', Auth::user()->shop_id], ['pay_to_type', null]])->get();
                $acc_type = Account_type::where('name', 'چک')->first();
                $banks = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self'], ['type_id', $acc_type->id]])->get();
                $resp = array('cheques' => $cheques, 'banks' => $banks);
                return $resp;
                break;

            case'set-purchase-by-other-cheque':
                $factor = Input_Factor::where('f_num', $request->id)->with('provider')->first();
                $providerAcc = Account::where([['shop_id',Auth::user()->shop_id],['owner_id', $factor->provider_id]])->first();
                $cheque = otherCheque::where([['shop_id', Auth::user()->shop_id], ['id', $request->cheque_id]])->first();
                $sum = $request->paid + $cheque->amount;
                if ($sum <= $factor->total_price) {
                    $outgo = new Outgo();
                    $outgo->type = 'offline';
                    $outgo->shop_id = Auth::user()->shop_id;
                    $outgo->target_type = Input_Factor::class;
                    $outgo->target_id = $factor->id;
                    $outgo->source_id = $cheque->id;
                    $outgo->source_type = otherCheque::class;
                    $outgo->user_id = Auth::id();
                    $outgo->price = $cheque->amount;
                    $outgo->save();
                    $providerAcc->amount -= $outgo->price;
                    $providerAcc->save();
                    $cheque->pay_to_type = Provider::class;
                    $cheque->pay_to_id = $factor->provider_id;
                    $cheque->note = $request->note;
                    $cheque->save();
                    return $outgo;
                } else {
                    return response()->json('Overflow ERRRRRROOOOOR');
                }
                break;

            case 'get-other-cheque-price':
                $cheque = otherCheque::where([['shop_id', Auth::user()->shop_id], ['id', $request->id]])->first();
                return $cheque;
                break;

            case'set-new-cheque':
                $factor = Input_Factor::where([['f_num', $request->id],['shop_id',Auth::user()->shop_id]])->with('provider')->first();
                $providerAcc = Account::where([['shop_id',Auth::user()->shop_id],['owner_id', $factor->provider_id]])->first();
                $bank_cheque=Account::where([['shop_id',Auth::user()->shop_id],['id',$request->bank]])->first();
                $bank=Account::where([['shop_id',Auth::user()->shop_id],['id',$bank_cheque->owner_id],['owner_type','self']])->first();

                $sum = $request->paid + $request->price;
                if ($sum <= $factor->total_price) {
                    $cheque = new myCheque();
                    $cheque->belong_to_acc = $request->bank;
                    $cheque->serial = $request->serial;
                    $cheque->pay_to = $request->pay_to;
                    $cheque->provider_id = $factor->provider_id;
                    $cheque->amount = $request->price;
                    if (strpos($request->date, '/') !== false) {
                        $orderdate = explode('/', $request->date);
                        function engNum($string)
                        {
                            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                            $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                            $output = str_replace($persian, $english, $string);
                            return $output;
                        }

                        $month = (int)engNum($orderdate[1]);
                        $day = (int)engNum($orderdate[2]);
                        $year = (int)engNum($orderdate[0]);
                        $jalaliDate = Verta::getGregorian($year, $month, $day);
                        $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                    } else {
                        $milDate = Carbon::instance(Verta::parse($request->date)->datetime());
                    }
                    $cheque_date = $milDate;
                    $cheque->pay_date = $cheque_date;
                    $cheque->note = $request->note;
                    $cheque->shop_id = Auth::user()->shop_id;
                    $cheque->save();
                    $outgo = new Outgo();
                    $outgo->type = 'offline';
                    $outgo->shop_id = Auth::user()->shop_id;
                    $outgo->target_type = Input_Factor::class;
                    $outgo->target_id = $factor->id;
                    $outgo->source_id = $cheque->id;
                    $outgo->source_type = myCheque::class;
                    $outgo->user_id = Auth::id();
                    $outgo->price = $request->price;
                    $outgo->save();
                    /*$providerAcc->amount -= $outgo->price;
                    $providerAcc->save();*/
                    $bank_cheque->amount -= $outgo->price;
                    $bank_cheque->save();
                    $bank->amount -= $outgo->price;
                    $bank->save();
                    return $outgo;
                } else {
                    return response()->json('Overflow ERRRRRROOOOOR');
                }
                break;

            case 'other-cheque-input':
                $factor = Output_factor::where([['f_num', $request->id], ['shop_id', Auth::user()->shop_id]])->first();
                $customer = Customer::where('id', $request->customer_account)->first();
                $customerAcc = Account::where('owner_id', $customer->id)->first();
                $sum = $request->paid + $request->amount;
                if ($sum <= $factor->total_price) {
                    $cheque = new otherCheque();
                    $cheque->shop_id = Auth::user()->shop_id;
                    $cheque->owner = $request->owner;
                    $cheque->owner_type = Customer::class;
                    $cheque->owner_id = $customer->id;
                    $cheque->pay_to = $request->pay_to;
                    if (strpos($request->pay_date, '/') !== false) {
                        $orderdate = explode('/', $request->pay_date);
                        function engNum($string)
                        {
                            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                            $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                            $output = str_replace($persian, $english, $string);
                            return $output;
                        }

                        $month = (int)engNum($orderdate[1]);
                        $day = (int)engNum($orderdate[2]);
                        $year = (int)engNum($orderdate[0]);
                        $jalaliDate = Verta::getGregorian($year, $month, $day);
                        $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                    } else {
                        $milDate = Carbon::instance(Verta::parse($request->pay_date)->datetime());
                    }
                    $cheque->pay_date = $milDate;
                    $cheque->amount = $request->amount;
                    $cheque->serial = $request->serial;
                    $cheque->note = $request->note;
                    $cheque->save();

                    $income = new Income();
                    $income->type = 'offline';
                    $income->shop_id = Auth::user()->shop_id;
                    $income->user_id = Auth::id();
                    $income->source_type = 'فروش محصول';
                    $income->source_id = $factor->f_num;
                    $income->target_type = otherCheque::class;
                    $income->target_id = $cheque->id;
                    $income->price = $request->amount;
                    $income->save();
                    $customerAcc->amount += $request->amount;
                    $customerAcc->save();
                    return $income;
                } else {
                    return response()->json('overflow EERROORR');
                }
                break;

            case'delete-service':
                $service = Service::where([['shop_id', Auth::user()->shop_id], ['id', $request->service_id]])->first();
                $service->delete();
                return response()->json('success');
                break;

            case'search-service':
                $service_name = '';
                if ($request->service_name != null)
                    $service_name = $request->service_name;
                $services = Service::where('shop_id', Auth::user()->shop_id)
                    ->where('title', 'LIKE', '%' . $service_name . '%')
                    ->paginate(10);
                return $services;
                break;

            case'add-delivery-price-to-cart':
                $cart = Session::get('cart');
                $cart['totalPrice'] -= $cart['deliveryPrice'];
//                $cart['deliveryPrice'] = 0;
                $cart['deliveryPrice'] = $request->price;
                $cart['totalPrice'] += $cart['deliveryPrice'];
                $cart['deliveryMethod'] = $request->id;
                Session::put('cart', $cart);
                return $cart;
                break;

            case'check-pre-payment-requirement':
                $cart = Session::get('cart');
                if (Auth::user()->user_info_id == null) {
                    return response()->json('null_info');
                }
                if ($cart['deliveryPrice'] == 0) {
                    return response()->json('delivery_null');
                }
                return 'true';

            case'set-discount':
                $off = Discount::where('name', $request->dis)->first();
                $status = true;
                $dis = 0;
                $date = $off->end;
                $expdate = Verta($date);
                $now = Verta::now();
                $diff = $now->gt($expdate);
                switch ($off->groupable_type) {
                    case online_output_factor::class :
                        if ($diff == true) {
                            $status = false;
                        } else {
                            $cart = Session::get('cart');
                            $cart['totalPrice'] -= $cart['deliveryPrice'];
                            if ($off->is_percent == true) {
                                $dis = $off->value * $cart['totalPrice'] / 100;
                                $cart['totalPrice'] -= $dis;
                                $cart['totalPrice'] += $cart['deliveryPrice'];
                                $cart['discount'] = $dis;
                                Session::put('cart', $cart);
                            } else {
                                $dis = $off->value;
                                $cart['totalPrice'] -= $dis;
                                $cart['totalPrice'] += $cart['deliveryPrice'];
                                $cart['discount'] = $dis;
                                Session::put('cart', $cart);
                            }
                        }
                        break;
                }
                $res = array("status" => $status, 'discount' => $dis, "cart" => Session::get('cart'));
                return $res;

            case'get-sub-guilds':
                $sub_guilds = subGuild::where('parent_id', $request->guild)->get();
                return $sub_guilds;
                break;

            case'check-barcode-is-unique':
                $product = Product::where('barcode', $request->barcode)->first();
                if ($product == null)
                    return response()->json('OK');
                else
                    return response()->json('NOK');

            case'get-favorites':
                $favorites = Favorite_product::where('user_id', Auth::id())->with('product')->get();
                return $favorites;

            case'delete-favorite':
                $favorites = Session::get('favorite');
                foreach ($favorites as $key => $item) {
                    if ($item == $request->pro_id) {
                        unset($favorites[$key]);
                    }
                }
                Session::put('favorite', $favorites);
                $favorite = Favorite_product::where([['user_id', Auth::id()], ['id', $request->id]])->first();
                $favorite->delete();
                return response()->json('deleted');

            case 'getLast7Days':
                $days = array();
                for ($i = 0; $i < 7; $i++) {
                    $day = \Hekmatinasser\Verta\Verta::now()->subDay($i)->format('%B %d');
                    array_push($days, $day);
                }
                $days = json_encode($days);
                return response($days);

            default:
                return response('bilakh', 404);

            case'get-income-outgo-detail-chart':
                $income_source_types = Income::where('shop_id', Auth::user()->shop_id)->get()->groupBy('source_type');
                $income_types = array_keys($income_source_types->toArray());
                $main = array();
                foreach ($income_types as $type) {
                    $main[$type] = 0;
                }
                foreach ($income_source_types as $group) {
                    foreach ($group as $item) {
                        $main[$item->source_type] += $item->price;
                    }
                }
                $outgo_source_types = Outgo::where('shop_id', Auth::user()->shop_id)->get()->groupBy('target_type');
                $outgo_types = array_keys($outgo_source_types->toArray());
                $main_outgo = array();
                foreach ($outgo_types as $type) {
                    $main_outgo[$type] = 0;
                }
                foreach ($outgo_source_types as $group) {
                    foreach ($group as $item) {
                        $main_outgo[$item->target_type] += $item->price;
                    }
                }
                foreach (array_keys($main_outgo) as $outgo) {
                    switch ($outgo) {
                        case Outgo_Bill::class:
                            $main_outgo['قبض'] = $main_outgo[Outgo_Bill::class];
                            unset($main_outgo[Outgo_Bill::class]);
                            break;

                        case Outgo_Charak_Service::class:
                            $main_outgo['خدمات چارک'] = $main_outgo[Outgo_Charak_Service::class];
                            unset($main_outgo[Outgo_Charak_Service::class]);
                            break;

                        case Outgo_Salary::class:
                            $main_outgo['حقوق کارکنان'] = $main_outgo[Outgo_Salary::class];
                            unset($main_outgo[Outgo_Salary::class]);
                            break;

                        case Outgo_Other::class:
                            $main_outgo['سایر'] = $main_outgo[Outgo_Other::class];
                            unset($main_outgo[Outgo_Other::class]);
                            break;

                        case Input_Factor::class:
                            $main_outgo['فاکتور خرید'] = $main_outgo[Input_Factor::class];
                            unset($main_outgo[Input_Factor::class]);
                            break;
                    }
                }
                return response(json_encode(array('incomes' => array_keys($main), 'in-prices' => array_values($main), 'outgoes' => array_keys($main_outgo), 'out-prices' => array_values($main_outgo))));

            case'get-storage-detail-for-chart':
                $storages = Storage::where('shop_id', Auth::user()->shop_id)->get();
                $storage_qty = count($storages);
                $product_in_storage = StoragePivot::where('shop_id', Auth::user()->shop_id)->with('storage')->get();
                $pros_in_storages = $product_in_storage->groupBy('storage_id');
                $my_storages = array_keys($pros_in_storages->toArray());
                $main_storages = array();
                foreach ($my_storages as $type) {
                    $main_storages[$type] = 0;
                }
                foreach ($pros_in_storages as $group) {
                    foreach ($group as $item) {
                        $main_storages[$item->storage_id] += $item->quantity;
                    }
                }
                foreach (array_keys($main_storages) as $st) {
                    foreach ($storages as $storage) {
                        if ($storage->id == $st) {
                            $main_storages[$storage->name] = $main_storages[$st];
                            unset($main_storages[$st]);
                        }
                    }
                }
                return response(json_encode(array('storage_qty' => $storage_qty, 'storages' => array_keys($main_storages), 'products' => array_values($main_storages))));
                break;

            case'get-personnel-roles-chart':
                $all_shop_roles = ShopPermission::where('shop_id', Auth::user()->shop_id)->get();
                $role_groups = $all_shop_roles->groupBy('role_name');
                $role_names = array_keys($role_groups->toArray());
                $roles = array();
                foreach ($role_names as $role) {
                    $roles[$role] = 0;
                }
                foreach ($all_shop_roles as $role) {
                    if ($role->role_name == $roles[$role->role_name]) {
                        $roles[$role->role_name]++;
                    }
                }
                return response(json_encode(array('roles' => array_keys($roles), 'role_qty' => array_values($roles))));

            case'get-personnel-work-time-chart':
                $today = \Carbon\Carbon::today();
                $last7days = Carbon::today()->subDay(6);
                $logins = Login_log::where('shop_id', Auth::user()->shop_id)
                    ->wherebetween('login_time', [$last7days, $today])
                    ->with('user')->get();
                $logouts = Logout_log::where('shop_id', Auth::user()->shop_id)
                    ->wherebetween('logout_time', [$last7days, $today])
                    ->with('user')->get();
                $logs = array();
                while ($last7days->lte($today)) {
                    $logs[\verta($last7days)->format('%d %B')] = 0;
                    $last7days->addDay(1);
                }
                $counter = count($logouts);
                $tpd = array();
                for ($i = 0; $i < $counter; $i++) {
                    $in = $logins[$i]->login_time;
                    $out = $logouts[$i]->logout_time;
                    $in = verta($in);
                    $out = verta($out);
                    $diff = $in->diffMinutes($out);
                    if (array_key_exists($in->format('%d %B'), $tpd))
                        $tpd[$in->format('%d %B')] += $diff;
                    else
                        $tpd[$in->format('%d %B')] = $diff;
                }
                foreach (array_keys($tpd) as $key) {
                    $logs[$key] = $tpd[$key];
                }
                $max = 0;

                foreach (array_keys($logs) as $log) {
                    if ($logs[$log] != 0) {
                        $logs[$log] /= 60;
                    }
                    if ($logs[$log] > $max) {
                        $max = $logs[$log];
                    }
                }

                return response(json_encode(array('max' => $max, 'days' => array_keys($logs), 'work' => array_values($logs))));
                break;

            case'monthly-personnel-work-chart':
                $today = \Carbon\Carbon::today();
                $lastMonth = Carbon::today()->subDay(30);
                $logins = Login_log::where('shop_id', Auth::user()->shop_id)
                    ->wherebetween('login_time', [$lastMonth, $today])
                    ->with('user')->get();
                $grouped_logins = $logins->groupBy('user_id');
                $logouts = Logout_log::where('shop_id', Auth::user()->shop_id)
                    ->wherebetween('logout_time', [$lastMonth, $today])
                    ->with('user')->get();
                $grouped_logouts = $logouts->groupBy('user_id');
                $login_users = array_keys($grouped_logins->toArray());
                $logout_users = array_keys($grouped_logouts->toArray());
                $personel = User::where('shop_id', Auth::user()->shop_id)->get();
                $person = array();
                $labels = array();
                while ($lastMonth->lte($today)) {
                    array_push($labels, \verta($lastMonth)->format('%d %B'));
                    $lastMonth->addDay(1);
                }
                foreach ($personel as $p) {
                    $lastMonth = Carbon::today()->subDay(30);
                    while ($lastMonth->lte($today)) {
                        $person[$p->id][\verta($lastMonth)->format('%d %B')] = 0;
                        $lastMonth->addDay(1);
                    }
                }
                $counter = count($logouts);
                $tpd = array();
                foreach ($grouped_logouts as $group) {
                    for ($i = 0; $i < $counter; $i++) {
                        $in = $logins[$i]->login_time;
                        $out = $logouts[$i]->logout_time;
                        $in = verta($in);
                        $out = verta($out);
                        $diff = $in->diffMinutes($out);
                        if (array_key_exists($in->format('%d %B'), $person[$logins[$i]->user_id]))
                            $person[$logins[$i]->user_id][$in->format('%d %B')] += $diff;
                        else
                            $person[$logins[$i]->user_id][$in->format('%d %B')] = $diff;
                    }
                }

                foreach (array_keys($person) as $p) {
                    foreach (array_keys($person[$p]) as $d) {
                        if ($person[$p][$d] != 0) {
                            $person[$p][$d] /= 60;
                        }
                    }
                }

                foreach ($personel as $p) {
                    $person[$p->name . " " . $p->family] = $person[$p->id];
                    unset($person[$p->id]);
                }
                return response(json_encode(array('labels' => $labels, 'personnel' => $person)));
                break;

            case'get-last-7days-sent-orders':
                $today = \Carbon\Carbon::today();
                $last7days = Carbon::today()->subDay(6);
                $sent_orders = online_output_factor::where([['shop_id', Auth::user()->shop_id], ['send_date', '<>', null]])->wherebetween('send_date', [$last7days, $today])->get();
                $sent = array();
                $max = 0;
                while ($last7days->lte($today)) {
                    $sent[\verta($last7days)->format('%d %B')] = 0;
                    $last7days->addDay(1);
                }
                foreach ($sent_orders as $order) {
                    $sent[\verta($order->send_date)->format('%d %B')]++;
                    if ($sent[\verta($order->send_date)->format('%d %B')] > $max)
                        $max = $sent[\verta($order->send_date)->format('%d %B')];
                }
                return response(json_encode(array('max' => $max, 'days' => array_keys($sent), 'sent' => array_values($sent))));

            case'get-last-7days--online-orders':
                $today = \Carbon\Carbon::today();
                $last7days = Carbon::today()->subDay(6);
                $online_orders = online_output_factor::where([['shop_id', Auth::user()->shop_id], ['send_date', null]])->wherebetween('created_at', [$last7days, $today])->get();
                $orders = array();
                $max = 0;
                while ($last7days->lte($today)) {
                    $orders[\verta($last7days)->format('%d %B')] = 0;
                    $last7days->addDay(1);
                }
                foreach ($online_orders as $order) {
                    $orders[\verta($order->created_at)->format('%d %B')]++;
                    if ($orders[\verta($order->created_at)->format('%d %B')] > $max)
                        $max = $orders[\verta($order->created_at)->format('%d %B')];
                }
                return response(json_encode(array('max' => $max, 'days' => array_keys($orders), 'orders' => array_values($orders))));

            case'delete-comment':
                $comment = Comment::find($request->id);
                $comment->delete();
                return response(json_encode('success'));
        }
    }

    public function main_shop_setting()
    {
        $slider_imgs = Admin_img::where('type', 'slider')->get();
        $top_banners = Admin_img::where([['type', 'banner'], ['position', 'top']])->get();
        $bot_banners = Admin_img::where([['type', 'banner'], ['position', 'bot']])->get();
        $side_banner = Admin_img::where([['type', 'banner'], ['position', 'side']])->first();
        return view('admin.Admin.main-shop-setting', compact('slider_imgs', 'top_banners', 'bot_banners', 'side_banner'));
    }

    public function save_slider_imgs(Request $request)
    {
        if ($file = $request->file('img')) {
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('images/slider', $path);
            $slider = new Admin_img();
            $slider->type = 'slider';
            $slider->note = $request->note;
            $slider->url = $request->url;
            $slider->save();
            $image = new Photo();
            $image->groupable_id = $slider->id;
            $image->groupable_type = Admin_img::class;
            $image->path = $path;
            $image->name = $name;
            $image->save();
            $slider->photo_id = $image->id;
            $slider->save();
            return redirect(route('main-shop-setting'));
        }
    }

    public function delete_slider_imgs($id)
    {
        $slider_img = Admin_img::find($id);
        $img = Photo::where([['groupable_type', Admin_img::class], ['groupable_id', $slider_img->id]])->first();
        unlink(public_path() . '/images/slider/' . $img->path);
        $slider_img->delete();
        $img->delete();
        return redirect(route('main-shop-setting'));
    }

    public function save_banner_imgs(Request $request)
    {
        switch ($request->type) {
            case'top':
                if ($file = $request->file('img')) {
                    $path = time() . $file->getClientOriginalName();
                    $name = $file->getClientOriginalName();
                    $file->move('images/banner', $path);
                    $banner = new Admin_img();
                    $banner->type = 'banner';
                    $banner->position = 'top';
                    $banner->url = $request->url;
                    $banner->save();
                    $image = new Photo();
                    $image->groupable_id = $banner->id;
                    $image->groupable_type = Admin_img::class;
                    $image->path = $path;
                    $image->name = $name;
                    $image->save();
                    $banner->photo_id = $image->id;
                    $banner->save();
                    return redirect(route('main-shop-setting'));
                }
                break;
            case'bot':
                if ($file = $request->file('img')) {
                    $path = time() . $file->getClientOriginalName();
                    $name = $file->getClientOriginalName();
                    $file->move('images/banner', $path);
                    $banner = new Admin_img();
                    $banner->type = 'banner';
                    $banner->position = 'bot';
                    $banner->url = $request->url;
                    $banner->save();
                    $image = new Photo();
                    $image->groupable_id = $banner->id;
                    $image->groupable_type = Admin_img::class;
                    $image->path = $path;
                    $image->name = $name;
                    $image->save();
                    $banner->photo_id = $image->id;
                    $banner->save();
                    return redirect(route('main-shop-setting'));
                }
                break;
            case'side':
                if ($file = $request->file('img')) {
                    $path = time() . $file->getClientOriginalName();
                    $name = $file->getClientOriginalName();
                    $file->move('images/banner', $path);
                    $banner = new Admin_img();
                    $banner->type = 'banner';
                    $banner->position = 'side';
                    $banner->url = $request->url;
                    $banner->save();
                    $image = new Photo();
                    $image->groupable_id = $banner->id;
                    $image->groupable_type = Admin_img::class;
                    $image->path = $path;
                    $image->name = $name;
                    $image->save();
                    $banner->photo_id = $image->id;
                    $banner->save();
                    return redirect(route('main-shop-setting'));
                }
                break;
        }

    }

    public function delete_banner_imgs($id)
    {
        $banner_img = Admin_img::find($id);
        $img = Photo::where([['groupable_type', Admin_img::class], ['groupable_id', $banner_img->id]])->first();
        unlink(public_path() . '/images/banner/' . $img->path);
        $banner_img->delete();
        $img->delete();
        return redirect(route('main-shop-setting'));
    }

    public function blog_setting()
    {
        $top_right_article = special_Article::where('position', 'blog_top_right')->with('article')->first();
        $top_mid_articles = special_Article::where('position', 'blog_top_mid')->with('article')->get();
        $top_left_articles = special_Article::where('position', 'blog_top_left')->with('article')->get();
        $articles = Article::all();
        return view('admin.Admin.blog-setting', compact(['top_left_articles', 'top_mid_articles', 'top_right_article', 'articles']));
    }

    public function blog_setting_set_article(Request $request)
    {
        switch ($request->position) {
            case'right':
                if ($request->old)
                $article = special_Article::where('position', 'blog_top_right')->update(['article_id' => $request->new_article]);
                else{
                    $article=new special_Article();
                    $article->article_id=$request->new_article;
                    $article->position='blog_top_right';
                    $article->save();
                }
                return redirect(route('blog-setting'));
                break;
            case'mid':
                if ($request->old)
                $article = special_Article::where([['position', 'blog_top_mid'], ['id', $request->old]])->update(['article_id' => $request->new_article]);
                else{
                    $article=new special_Article();
                    $article->article_id=$request->new_article;
                    $article->position='blog_top_mid';
                    $article->save();
                }
                return redirect(route('blog-setting'));
                break;
            case'left':
                if ($request->old)
                $article = special_Article::where([['position', 'blog_top_left'], ['id', $request->old]])->update(['article_id' => $request->new_article]);
               else{
                   $article=new special_Article();
                   $article->article_id=$request->new_article;
                   $article->position='blog_top_left';
                   $article->save();
               }
                return redirect(route('blog-setting'));
                break;

        }
    }

    public function delivery()
    {
        $methods = Delivery_method::with('photo')->get();
        return view('admin.Admin.delivery_methods', compact('methods'));
    }

    public function add_delivery_method()
    {
        return view('admin.Admin.new_delivery_method');
    }

    public function edit_delivery_method($id)
    {
        $method = Delivery_method::find($id);
        return view('admin.Admin.edit_delivery_method', compact('method'));
    }

    public function pay_plans()
    {
        $plans = plan::where('deleted', false)->get();
        return view('admin.Admin.pay-plans', compact('plans'));
    }

    public function new_pay_plan()
    {
        return view('admin.Admin.new_pay_plan');
    }

    public function edit_pay_plan($id)
    {
        $plan = plan::find($id);
        return view('admin.Admin.edit_pay_plan', compact('plan'));
    }

    public function checkoutReq()
    {
        $checkouts = Checkout::where('status', 'pending')->paginate(10);
        return view('admin.Admin.checkouts', compact('checkouts'));

    }

    public function payCheckout(Request $request, $id)
    {
        $checkout = Checkout::Find($id);
        $checkout->status = 'paid';
        $checkout->tracking_id = $request->tracking_id;
        $checkout->save();
        return redirect(route('checkoutReq'));
    }

    public function guilds()
    {
        $guilds = Guild::paginate(20);
        return view('admin.Admin.guilds', compact('guilds'));
    }

    public function add_guild()
    {
        $guilds = Guild::all();
        return view('admin.Admin.new_guild', compact('guilds'));
    }

    public function edit_guild($id)
    {
        $guild = Guild::find($id);
        $guilds = Guild::all();
        return view('admin.Admin.edit_guild', compact(['guild', 'guilds']));
    }

    public function sub_guilds($id)
    {
        $current_guild = Guild::find($id);
        $sub_guilds = subGuild::where('parent_id', $id)->paginate(20);
        $guilds = Guild::all();
        return view('admin.Admin.sub-guilds', compact(['sub_guilds', 'guilds', 'current_guild']));
    }

    public function add_subguild($id)
    {
        $guild = Guild::find($id);
        return view('admin.Admin.new_subguild', compact('guild'));
    }

    public function edit_subguild($id)
    {
        $subguild = subGuild::find($id);
        return view('admin.Admin.edit_subguild', compact(['subguild']));
    }

    public function add_pro_cat()
    {
        $cats = Category::where('shop_id', 0)->get();
        return view('admin.Admin.newCategory', compact('cats'));
    }

    public function edit_pro_cat($id)
    {
        $cats = Category::where('shop_id', 0)->get();
        $cat = Category::find($id);
        return view('admin.Admin.editCategory', compact(['cats', 'cat']));
    }

    public function pro_cats()
    {
        $categories = Category::where('shop_id', 0)->with('photo')->paginate(20);
        return view('admin.Admin.categories', compact('categories'));
    }

    public function verifyMobile(Request $request)
    {
        $code = MobileCodes::where([['user_id', Auth::id()], ['code', $request->code], ['created_at', '>', now()->subMinute(3)]])->first();
        if ($code == null) {
            Session::put('error', 'کد وارد شده اشتباه است یا منقضی شده، لطفا دوباره تلاش کنید.');
            return view('auth.verifyMobile');
        } else {
            $user = User::find(Auth::id());
            $user->mobile_verified_at = now();
            $user->save();
            return redirect(route('dashboard'));
        }

    }

    public function change_mobile()
    {
        return view('auth.changeMobile');
    }

    public function update_mobile(Request $request)
    {

        $validatedData = $request->validate([
            'mobile' => 'required|max:11|min:11',
        ], [
            'mobile.required' => 'لطفا شماره جدید را وارد کنید',
            'mobile.max' => ' شماره موبایل را  به صورت صحیح وارد کنید',
            'mobile.min' => ' شماره موبایل را  به صورت صحیح وارد کنید',
        ]);

        $user = Auth::user();
        $user->mobile = $request->mobile;
        $user->save();
        $codes = MobileCodes::where('user_id', Auth::id())->delete();
        $mobile_code = new MobileCodes();
        $mobile_code->user_id = Auth::id();
        $rand = rand(10000, 99999);
        $mobile_code->code = $rand;
        Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
            'receptor' => Auth::user()->mobile,
            'token' => str_replace(' ', '-', trim(Auth::user()->name)),
            'token2' => str_replace(' ', '-', trim(Auth::user()->family)),
            'token3' => $rand,
            'template' => '4rakMobileVerif',
        ]);
        $mobile_code->save();
        return response()->view('auth.verifyMobile');

    }

    public function change_password()
    {
        return view('auth.passwords.changePass');
    }

    public function verify_user_mobile(Request $request)
    {
        $validatedData = $request->validate([
            'mobile' => 'required|max:11|min:11',
        ], [
            'mobile.required' => 'لطفا شماره را وارد کنید',
            'mobile.max' => ' شماره موبایل را  به صورت صحیح وارد کنید',
            'mobile.min' => ' شماره موبایل را  به صورت صحیح وارد کنید',
        ]);

        $user = User::where('mobile', $request->mobile)->first();
        if ($user == null) {
            Session::put('error', 'شماره موبایل وارد شده صحیح نیست');
            return view('auth.passwords.changePass');
        } else {
            $mobile_code = new MobileCodes();
            $mobile_code->user_id = $user->id;
            $rand = rand(10000, 99999);
            $mobile_code->code = $rand;
            Http::get('https://api.kavenegar.com/v1/547373475546676B74494371434C464F6C3032574942746E7672426C4D4C436F/verify/lookup.json', [
                'receptor' => $user->mobile,
                'token' => str_replace(' ', '-', trim($user->name)),
                'token2' => str_replace(' ', '-', trim($user->family)),
                'token3' => $rand,
                'template' => '4rakMobileVerif',
            ]);
            $mobile_code->save();
            $id = $user->id;
            return view('auth.passwords.verifyCode', compact('id'));
        }


    }

    public function verify_code(Request $request)
    {
        $code = MobileCodes::where([['user_id', $request->id], ['code', $request->code], ['created_at', '>', now()->subMinute(3)]])->first();
        if ($code == null) {
            Session::put('error', 'کد وارد شده اشتباه است یا منقضی شده، لطفا دوباره تلاش کنید.');
            return view('auth.passwords.changePass');
        } else {
            Auth::loginUsingId($request->id);
            return redirect(route('show-change-pass'));
        }
    }

    public function show_change_pass()
    {
        return view('auth.passwords.updatePass');
    }

    public function update_pass(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required|confirmed|min:8',
        ], [
            'password.required' => 'رمز عبور نمیتواند خالی باشد',
            'password.confirmed' => 'رمز عبور و تکرار آن مطابقت ندارد',
            'password.min' => 'رمز عبور باید حداقل 8 کاراکتر باشد',
            'password_confirmation.required' => 'تکرار رمز عبور نمیتواند خالی باشد',
        ]);
        $user = Auth::user();
        $user->password = Hash::make($request['password']);
        $user->save();
        return redirect(route('dashboard'));

    }

    public function config()
    {
        if (Auth::user()->role_id==1){
            $roles = Role::all();
            if (count($roles) == 0) {
                $admin = new Role();
                $admin->name = 'ادمین';
                $admin->save();
                $owner = new Role();
                $owner->name = 'مالک';
                $owner->save();
                $user = new Role();
                $user->name = 'کاربر';
                $user->save();
            } else {
                $admin_flag = 0;
                $owner_flag = 0;
                $user_flag = 0;
                foreach ($roles as $role) {
                    if ($role->name == 'ادمین')
                        $admin_flag = 1;
                    if ($role->name == 'مالک')
                        $owner_flag = 1;
                    if ($role->name == 'کاربر')
                        $user_flag = 1;
                }
                if ($admin_flag==0){
                    $admin = new Role();
                    $admin->name = 'ادمین';
                    $admin->save();
                }
                if ($owner_flag==0){
                    $owner = new Role();
                    $owner->name = 'مالک';
                    $owner->save();
                }
                if ($user_flag==0){
                    $user = new Role();
                    $user->name = 'کاربر';
                    $user->save();
                }
            }

            $acc_types=Account_type::all();
            if (count($acc_types) == 0) {
                $local = new Account_type();
                $local->name = 'محلی';
                $local->save();

                $bank = new Account_type();
                $bank->name = 'بانک';
                $bank->save();

                $cheque = new Account_type();
                $cheque->name = 'چک';
                $cheque->save();

                $customer = new Account_type();
                $customer->name = Customer::class;
                $customer->save();

                $provider = new Account_type();
                $provider->name = Provider::class;
                $provider->save();
            } else {
                $local_flag = 0;
                $bank_flag = 0;
                $cheque_flag = 0;
                $customer_flag = 0;
                $provider_flag = 0;
                foreach ($acc_types as $type) {
                    if ($type->name == 'محلی')
                        $local_flag = 1;
                    if ($type->name == 'بانک')
                        $bank_flag = 1;
                    if ($type->name == 'چک')
                        $cheque_flag = 1;
                    if ($type->name ==Customer::class)
                        $customer_flag = 1;
                    if ($type->name == Provider::class)
                        $provider_flag = 1;
                }
                if ($local_flag==0){
                    $local = new Account_type();
                    $local->name = 'محلی';
                    $local->save();
                }
                if ($bank_flag==0){
                    $bank = new Account_type();
                    $bank->name = 'بانک';
                    $bank->save();
                }
                if ($cheque_flag==0){
                    $cheque = new Account_type();
                    $cheque->name = 'چک';
                    $cheque->save();
                }
                if ($customer_flag==0){
                    $customer = new Account_type();
                    $customer->name = Customer::class;
                    $customer->save();
                }
                if ($provider_flag==0){
                    $provider = new Account_type();
                    $provider->name = Provider::class;
                    $provider->save();
                }
            }

            $plans=plan::all();
            if (count($plans)==0){
                $plan=new plan();
                $plan->name='پایه';
                $plan->monthlyPrice = 0;
                $plan->threeMonthPrice = 172500;
                $plan->sixMonthPrice = 330000;
                $plan->yearlyPrice = 600000;
                $plan->wagePercent = 10;
                $plan->save();
            }

            $guilds=Guild::all();
            if (count($guilds)==0){
                $guild=new Guild();
                $guild->name='لوازم الکترونیکی';
                $guild->save();

                $sub=new subGuild();
                $sub->name='موبایل';
                $sub->parent_id=$guild->id;
                $sub->save();

                $sub=new subGuild();
                $sub->name='کامپیوتری';
                $sub->parent_id=$guild->id;
                $sub->save();

                $sub=new subGuild();
                $sub->name='لوازم خانگی';
                $sub->parent_id=$guild->id;
                $sub->save();

                $guild=new Guild();
                $guild->name='پوشاک';
                $guild->save();

                $guild=new Guild();
                $guild->name='مواد غذایی';
                $guild->save();

                $guild=new Guild();
                $guild->name='آرایشی بهداشتی';
                $guild->save();

                $guild=new Guild();
                $guild->name='تجهیزات پزشکی';
                $guild->save();

            }

            $delivery_methods=Delivery_method::all();
            if (count($delivery_methods)==0){
                $delivery_method=new Delivery_method();
                $delivery_method->method='پست معمولی';
                $delivery_method->price=20000;
                $delivery_method->description='تحویل از سه تا پنج روز کاری';
                $delivery_method->save();

                $delivery_method=new Delivery_method();
                $delivery_method->method='پست پیشتاز';
                $delivery_method->price=40000;
                $delivery_method->description='تحویل از یک تا سه روز کاری';
                $delivery_method->save();
            }


            Session::put('config','تنظیمات اولیه با موفقیت انجام شد');
            return redirect(route('dashboard'));

        }else{
            return redirect(403);
        }

    }

}
