<?php

namespace App\Http\Controllers;

use App\Account;
use App\City;
use App\File;
use App\Guild;
use App\Http\Requests\shopInfoReq;
use App\Photo;
use App\plan;
use App\Plan_Factor;
use App\Province;
use App\Role;
use App\Shop;
use App\ShopPermission;
use App\ShopPlanPivot;
use App\ShopRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class AdminShopController extends Controller
{

    public function shopInfo()
    {
        $shop = Shop::where('user_id', Auth::id())->with(['province','city'])->first();
        $logo=null;
        $header=null;
        $license=null;
        if ($shop!= null){
            $logo=Photo::where('id',$shop->logo_id)->first();
            $header=Photo::where('id',$shop->header_id)->first();
            $license=Photo::where('id',$shop->license_id)->first();
        }
        $provinces = Province::all();
        $guilds=Guild::all();
        return view('admin.Shop.info', compact(['provinces', 'shop','logo','header','license','guilds']));
    }


    public function imageup(Request $request)
    {
        $shop = Shop::where('user_id', Auth::id())->first();
        if ($file = $request->file('header_img')) {
            if ($shop->header_id != null) {
                $oldHeader = Photo::where('id', $shop->header_id)->first();
                unlink(public_path() . '/images/headers/' . $oldHeader->path);
                $oldHeader->delete();
                $shop->header_id=null;
                $shop->save();
            }
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('images/headers', $path);
            $header=new Photo();
            $header->groupable_id=$shop->id;
            $header->groupable_type=Shop::class;
            $header->path=$path;
            $header->name=$name;
            $header->save();
            $shop->header_id=$header->id;
            $shop->save();
        }

        if ($file = $request->file('logo_img')) {
            if ($shop->logo_id != null) {
                $oldlogo = Photo::where('id', $shop->logo_id)->first();
                unlink(public_path() . '/images/logos/' . $oldlogo->path);
                $oldlogo->delete();
                $shop->logo_id=null;
                $shop->save();
            }
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('images/logos', $path);
            $logo=new Photo();
            $logo->groupable_id=$shop->id;
            $logo->groupable_type=Shop::class;
            $logo->path=$path;
            $logo->name=$name;
            $logo->save();
            $shop->logo_id=$logo->id;
            $shop->save();
        }

        if ($file = $request->file('license_img')) {
            if ($shop->license_id != null) {
                $oldLicense = Photo::where('id', $shop->license_id)->first();
                unlink(public_path() . '/images/licenses/' . $oldLicense->path);
                $oldLicense->delete();
                $shop->license_id=null;
                $shop->save();
            }
            $path = time() . $file->getClientOriginalName();
            $name = $file->getClientOriginalName();
            $file->move('images/licenses', $path);
            $license=new Photo();
            $license->groupable_id=$shop->id;
            $license->groupable_type=Shop::class;
            $license->path=$path;
            $license->name=$name;
            $license->save();
            $shop->license_id=$license->id;
            $shop->save();
        }

        if ($shop->license_id!=null && $shop->license_id!=null &&$shop->license_id!=null){
            return redirect(route('shopInfo'));
        }else{
            Session::flash('error','لطفا همه فایل ها را آپلود کنید');
            return view('admin.Shop.uploadShopFiles');
        }
    }


    public function store(shopInfoReq $request)
    {
        $shop = new Shop();
        $shop->name = $request->input('name');
        $shop->city_id=$request->input('city');
        $shop->province_id=$request->input('province');
        $shop->class = $request->input('class');
        $shop->guild = $request->input('guild');
        $shop->address = $request->input('address');
        $shop->unique_name = $request->input('shopName');
        $shop->description = $request->input('description');
        $shop->user_id = Auth::id();



        if ($file = $request->file('headerImg')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = new Photo();
            $photo->name = $file->getClientOriginalName();
            $photo->path = $name;
            $photo->groupable_id = $shop->id;
            $photo->groupable_type = Shop::class;
            $photo->save();
            $shop->header_id = $photo->id;
        }
        if ($file = $request->file('logoImg')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = new Photo();
            $photo->name = $file->getClientOriginalName();
            $photo->path = $name;
            $photo->groupable_id = $shop->id;
            $photo->groupable_type = Shop::class;
            $photo->save();
            $shop->logo_id = $photo->id;
        }
        if ($file = $request->file('licenseImg')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('files', $name);
            $photo = new File();
            $photo->name = $file->getClientOriginalName();
            $photo->path = $name;
            $photo->groupable_id = $shop->id;
            $photo->groupable_type = Shop::class;
            $photo->save();
            $shop->license_id = $photo->id;
        }
        $shop->save();

        $account=New Account();
        $account->shop_id=$shop->id;
        $account->name='صندوق';
        $account->owner_type='self';
        $account->owner_id=0;
        $account->amount=0;
        $account->save();
        $user=Auth::user();
        $user->shop_id=$shop->id;

        $shop_permission=new ShopPermission();
        $shop_permission->shop_id=$shop->id;
        $shop_permission->role_name='مالک';
        $shop_permission->products=1;
        $shop_permission->accountant=1;
        $shop_permission->personnel=1;
        $shop_permission->storage=1;
        $shop_permission->buy=1;
        $shop_permission->sell=1;
        $shop_permission->reject=1;
        $shop_permission->guaranty=1;
        $shop_permission->delivery=1;
        $shop_permission->save();
        $shop_role=new ShopRole();
        $shop_role->shop_id=$shop->id;
        $shop_role->shop_permission_id=$shop_permission->id;
        $shop_role->save();

        $user->shop_role_id=$shop_role->id;
        $user->role_id=2;
        $user->save();

        Session::flash('success', 'اطلاعات فروشگاه ثبت شد.');
        return redirect(route('ShopuploadForm'));
    }

    public function ShopuploadForm()
    {
        return view('admin.Shop.uploadShopFiles');
    }

    public function ShopInfoEdit(Request $request, $id)
    {
        $shop=Shop::find($id);
        $cities = City::all();
        $provinces = Province::all();
        return view('admin.Shop.infoEdit',compact(['shop','provinces','cities']));
    }

    public function UpdateInfo(Request $request,$id)
    {
        $shop=Shop::Find($id);
        $shop->name = $request->input('name');

        $shop->city_id=$request->input('city');
        $shop->province_id=$request->input('province');

        $shop->guild_id = $request->input('guild');
        $shop->subguild_id = $request->input('subguild');
        $shop->address = $request->input('address');
        $shop->unique_name = $request->input('shopName');
        $shop->description = $request->input('description');
        $shop->save();
        $request->session()->put('success','تغییرات ثبت شد');
        return redirect(route('shopInfo'));

    }

    public function shopRoles()
    {
        $roles=ShopPermission::where('shop_id',Auth::user()->shop_id)->paginate();
        return view('admin.Shop.roles',compact('roles'));

    }

    public function destroyRole($id)
    {
        $role=ShopPermission::where('id',$id)->with('shoprole.user')->first();
        if ($role->shop_id==Auth::user()->shop_id){
            if (count($role->shoprole->user)==0){
                $shoprole=ShopRole::where([['shop_permission_id',$role->id],['shop_id',Auth::user()->shop_id]])->first();
                $role->delete();
                $shoprole->delete();
                Session::flash('success','تغییرات ذخیره شد');
                return redirect(route('shopRoles'));

            }else{
                Session::flash('delete_role_fail','کاربرانی با این نقش در فروشگاه شما وجود دارند،ابتدا نقش آنها را تغییر داده و سپس اقدام به حذف این نقش کنید.');
                return redirect(route('shopRoles'));
            }

        }else{
            Session::flash('forbidden','شما مجاز به انجام این عملیات نیستید');
            return redirect(route('shopRoles'));
        }
    }

    public function createRole()
    {
        return view('admin.Shop.newRole');
    }

    public function saveRole(Request $request)
    {
        $role=new ShopPermission();
        $role->role_name=$request->input('name');
        $role->shop_id=Auth::user()->shop_id;

        if ($request->accountant)
            $role->accountant=1;

        if ($request->products)
            $role->products=1;

        if ($request->personnel)
            $role->personnel=1;

        if ($request->storage)
            $role->storage=1;

        if ($request->buy)
            $role->buy=1;

        if ($request->sell)
            $role->sell=1;

        if ($request->reject)
            $role->reject=1;

        if ($request->guaranty)
            $role->guaranty=1;

        if ($request->delivery)
            $role->delivery=1;

        $role->save();

        $shopRole=new ShopRole();
        $shopRole->shop_id=$role->shop_id;
        $shopRole->shop_permission_id=$role->id;
        $shopRole->save();

        Session::flash('success','نقش جدید اضافه شد');
        return redirect(route('shopRoles'));
    }

    public function editRoles($id)
    {
        $role=ShopPermission::where('id',$id)->first();
        return view('admin.Shop.editRole',compact('role'));
    }

    public function updateRole(Request $request,$id)
    {
        $role=ShopPermission::where([['id',$id],['shop_id',Auth::user()->shop_id]])->first();

        $role->role_name=$request->input('name');

        if ($request->accountant)
            $role->accountant=1;
        else
            $role->accountant=0;

        if ($request->personnel)
            $role->personnel=1;
        else
            $role->personnel=0;


        if ($request->storage)
            $role->storage=1;
        else
            $role->storage=0;

        if ($request->buy)
            $role->buy=1;
        else
            $role->buy=0;

        if ($request->sell)
            $role->sell=1;
        else
            $role->sell=0;

        if ($request->reject)
            $role->reject=1;
        else
            $role->reject=0;


        if ($request->guaranty)
            $role->guaranty=1;
        else
            $role->guaranty=0;

        if ($request->delivery)
            $role->delivery=1;
        else
            $role->delivery=0;


        $role->save();
        Session::flash('updated','تغییرات ثبت شد');
        return redirect(route('shopRoles'));


    }

    public function gallery()
    {
        return view('admin.Shop.gallery');
    }

    public function setGallery(Request $request)
    {
        if ($image = $request->file('images')){
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('images/gallery'),$imageName);

            $imageUpload = new Photo();
            $imageUpload->groupable_id = Auth::user()->shop_id;
            $imageUpload->groupable_type = Shop::class;
            $imageUpload->name = $imageName;
            $imageUpload->path = $imageName;
            $imageUpload->save();
            return response()->json(['success'=>$imageName]);
        }
        if ($file = $request->file('teaser')){
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('files/gallery'),$fileName);

            $fileUpload = new File();
            $fileUpload->groupable_id = Auth::user()->shop_id;
            $fileUpload->groupable_type = Shop::class;
            $fileUpload->name = $fileName;
            $fileUpload->path = $fileName;
            $fileUpload->save();
            return response()->json(['success'=>$fileName]);
        }

    }

    public function deleteGalleryImg(Request $request)
    {
        $filename =  $request->get('filename');
        Photo::where('name',$filename)->delete();
        $path=public_path().'/images/gallery/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return response()->json(['success'=>$filename]);
    }

    public function deleteGalleryFile(Request $request)
    {
        $filename =  $request->get('filename');
        File::where('name',$filename)->delete();
        $path=public_path().'/files/gallery/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return response()->json(['success'=>$filename]);
    }

    public function buy_plan()
    {
        $pay_plans=plan::where('deleted',false)->get();
        return view('admin.Shop.plan',compact('pay_plans'));
    }

    public function plan_purchase(Request $request)
    {
        $plan=plan::find($request->plan_id);
        $price=null;
        switch ($request->duration){
            case "1":
                $price=$plan->monthlyPrice;
                break;
            case "3":
                $price=$plan->threeMonthPrice;
                break;
            case "6":
                $price=$plan->sixMonthPrice;
                break;
            case "12":
                $price=$plan->yearlyPrice;
                break;
        }
        $factor=new Plan_Factor();
        $factor->plan_id=$plan->id;
        $factor->shop_id=Auth::user()->shop_id;
        $factor->price=$price;
        $factor->month=$request->duration;
        $factor->save();
        if ($price>0){
            $price=(integer)$price;
            $data = [
                'merchant_id' => '62697a96-bee0-4d87-85c0-f8db52150966',
                'callback_url' => route('buy_plan_result',$factor->id),
                'description'=>" خرید اشتراک چارک",
                'amount' => $price
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.zarinpal.com/pg/v4/payment/request.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: application/json",
                    "content-type: application/json",
                ),
            ));
            $curlResp = curl_exec($curl);
            if (json_decode($curlResp)->data->code==100){
                $factor->transaction_code = json_decode($curlResp)->data->authority;
                $factor->save();
                return redirect("https://www.zarinpal.com/pg/StartPay/".json_decode($curlResp)->data->authority);
            }else
                return 404;
        }else{
            $free_plans=ShopPlanPivot::where([['shop_id',$factor->shop_id],['price',0]])->get();
            if (count($free_plans)>0){
                Session::put('error', 'شما قبلا یکبار از پلن رایگان استفاده کرده اید ');
                return redirect(route('buy_plan'));
            }else{
                $plan_pivot=new ShopPlanPivot();
                $plan_pivot->shop_id=$factor->shop_id;
                $plan_pivot->plan_id=$factor->plan_id;
                $plan_pivot->price=$factor->price;
                $current_plans=ShopPlanPivot::where([['shop_id',$factor->shop_id],['status','active'],['exp_date','>',now()]])->get();
                $max=now();
                foreach ($current_plans as $cp){
                    if ($cp->exp_date>$max)
                        $max=$cp->exp_date;
                }
                $plan_pivot->start_date=$max;
                $plan_pivot->exp_date=$max->addMonth($factor->month);
                $plan_pivot->status='active';
                $plan_pivot->save();

                $factor->status = 'پرداخت شده';
                $factor->pay_date = now();
                $factor->plan_pivot_id = $plan_pivot->id;
                $factor->save();

                Session::put('success', 'اطلاعات با موفقیت ثبت شد ');
                return redirect(route('ShopuploadForm'));
            }

        }



    }

    public function buy_plan_result($id)
    {
        $plan_factor = Plan_Factor::findorfail($id);
        $amount = (integer)$plan_factor->price;
        $status = $_GET['Status'];
        if ($_GET['Authority'] == $plan_factor->transaction_code && Auth::user()->shop_id == $plan_factor->shop_id) {
            if ($status == 'NOK') {
                Session::put('Error', 'عملیات توسط کاربر لغو شد.');
                return redirect(route('shopInfo'));

            } elseif ($status == 'OK') {

                // Make Post Fields Array
                $data = [
                    'merchant_id' => '62697a96-bee0-4d87-85c0-f8db52150966',
                    'authority' => $plan_factor->transaction_code,
                    'amount' => $amount
                ];

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.zarinpal.com/pg/v4/payment/verify.json",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array(
                        // Set here requred headers
                        "accept: application/json",
                        "content-type: application/json",
                    ),
                ));

                $response = json_decode(curl_exec($curl));

                if ($response->data->code==100 && $response->data->message=="Paid"){
                    $plan_pivot=new ShopPlanPivot();
                    $plan_pivot->shop_id=$plan_factor->shop_id;
                    $plan_pivot->plan_id=$plan_factor->plan_id;
                    $plan_pivot->price=$plan_factor->price;
                    $current_plans=ShopPlanPivot::where([['shop_id',$plan_factor->shop_id],['status','active'],['exp_date','>',now()]])->get();
                    $max=now();
                    foreach ($current_plans as $cp){
                        if ($cp->exp_date>$max)
                            $max=$cp->exp_date;
                    }
                    $plan_pivot->start_date=$max;
                    $plan_pivot->exp_date=$max->addMonth($plan_factor->month);
                    $plan_pivot->status='active';
                    $plan_pivot->save();

                    $plan_factor->status = 'پرداخت شده';
                    $plan_factor->pay_date = now();
                    $plan_factor->plan_pivot_id = $plan_pivot->id;
                    $plan_factor->save();

                    Session::put('success', 'پرداخت انجام شد.');
                    return redirect(route('ShopuploadForm'));

                }else{
                    Session::put('Error', ' پرداخت نامعتبر است');
                    return redirect(route('shopInfo'));
                }
            }

        } else {
            return redirect(404);
        }
    }


}
