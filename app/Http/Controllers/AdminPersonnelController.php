<?php

namespace App\Http\Controllers;

use App\City;
use App\Login_log;
use App\Logout_log;
use App\Province;
use App\Shop;
use App\ShopPermission;
use App\ShopRole;
use App\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminPersonnelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            if (Auth::user()->shop_id != null) {
                $user = User::with('shop')->where('id', Auth::id())->first();
                $personnel = User::where('shop_id', $user->shop_id)->with('shoprole.roles')->paginate(10);
                $roles = ShopRole::where('shop_id', Auth::user()->shop_id)->with('roles')->get();
            } else {
                $personnel = null;
                $roles = null;
            }
            return view('admin.Personnel.personnel', compact(['personnel', 'roles']));
        } else {
            return 403;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $provinces = Province::all();
            $cities = City::all();
            $roles = ShopPermission::where('shop_id', Auth::user()->shop_id)->get();
            return view('admin.Personnel.newPerson', compact(['provinces', 'cities', 'roles']));
        } else {
            return 403;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $role = ShopPermission::where([['shop_id', Auth::user()->shop_id], ['role_name', $request->role]])->with('shoprole')->first();
            $user = new User();
            $user->name = $request->input('name');
            $user->family = $request->input('family');
            $user->mobile = $request->input('mobile');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->role_id = 1;
            $user->shop_id = Auth::user()->shop_id;
            $user->shop_role_id = $role->shoprole->id;
            $user->save();
            Session::flash('success', 'کاربر جدید ثبت شد');
            return redirect(route('personnel.index'));
        } else {
            return 403;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $person = User::where('id', $id)->first();
            return view('admin.Personnel.editPerson', compact('person'));
        } else {
            return 403;
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $person = User::find($id);
            $person->name = $request->input('name');
            $person->family = $request->input('family');
            $person->mobile = $request->input('mobile');
            $person->email = $request->input('email');
            $person->password = Hash::make($request->input('password'));
            $person->save();
            Session::flash('success', 'تغییرات ذخیره شد');
            return redirect(route('personnel.index'));
        } else {
            return 403;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setUserRole(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $user = User::Find($id);
            $permission = ShopPermission::where([['role_name', $request->role], ['shop_id', Auth::user()->shop_id]])->with('shoprole')->first();
            $user->shop_role_id = $permission->shoprole->id;
            $user->save();

            Session::flash('success', 'تغییرات ثبت شد');
            return redirect(route('personnel.index'));
        } else {
            return 403;
        }

    }

    public function logs()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->personnel == 1) {

            $logins = Login_log::where('shop_id', Auth::user()->shop_id)->with('user')->get();
            $logouts = Logout_log::where('shop_id', Auth::user()->shop_id)->with('user')->get();
            $logs = $logins->merge($logouts);
            return view('admin.Personnel.logs', compact(['logins', 'logouts', 'logs']));
        } else {
            return 403;
        }

    }

    public function dashboard()
    {
        $personnel_qty = User::where('shop_id', Auth::user()->shop_id)->count();
        $roles_qty = ShopRole::where('shop_id', Auth::user()->shop_id)->count();

        return view('admin.Personnel.dashboard', compact(['personnel_qty', 'roles_qty']));

    }
}
