<?php

namespace App\Http\Controllers;

use App\City;
use App\Customer_factor;
use App\Photo;
use App\Province;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        return view('admin.Profile.profile',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provinces=Province::all();
        $cities=City::all();
        $user=User::find($id);
        $info=UserInfo::where('user_id',$id)->with('province','city')->first();
        return view('admin.Profile.profileEdit',compact(['user','info','provinces','cities']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name=$request->name;
        $user->family=$request->family;
        if ($avatar=$request->file('avatar')){
            $name=time() . $avatar->getClientOriginalName();
            $avatar->move(public_path('images/profile'),$name);
            $photo=new Photo();
            $photo->name=$avatar->getClientOriginalName();
            $photo->path=$name;
            $photo->groupable_type=User::class;
            $photo->groupable_id=$user->id;
            $photo->save();
            $user->photo_id=$photo->id;
        }
        $user->save();

        if ($user->user_info_id==null){
            $info=new UserInfo();
            $info->province=$request->province;
            $info->city=$request->city;
            $info->phone=$request->phone;
            $info->address=$request->address;
            $info->user_id=$id;
            $info->save();
            $user->user_info_id=$info->id;
            $user->save();
        }else{
            $info=UserInfo::where('user_id',$id)->first();
            $info->province=$request->province;
            $info->city=$request->city;
            $info->phone=$request->phone;
            $info->address=$request->address;
            $info->user_id=$id;
            $info->save();
            $user->name=$request->name;
            $user->family=$request->family;
            $user->save();
        }
        return redirect(route('Profile.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buyHistory()
    {
        $customer_factors=Customer_factor::where('customer_id',Auth::id())->with('factor_pro.factor','factor_pro.shop')->get();
        return view('admin.Profile.buyHistory',compact('customer_factors'));
    }

    public function buyFactorDetail($id)
    {
        $customer_factor=Customer_factor::find($id);

    }
}
