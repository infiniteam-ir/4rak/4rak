<?php

namespace App\Http\Controllers;

use App\Article;
use App\Article_category;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::where('shop_id',Auth::user()->shop_id)->paginate(20);
        return view('admin.Blog.articles',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats=Article_category::where('shop_id',Auth::user()->shop_id)->get();
        if (count($cats)>0){
            return view('admin.Blog.new-article',compact('cats'));
        }else{
            Session::put('forbidden','ابتدا یک دسته بندی برای مطالب ایجاد کنید.');
            return redirect(route('article.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article=Article::find($id);
        $cats=Article_category::where('shop_id',Auth::user()->shop_id)->get();
        return view('admin.Blog.article-edit',compact(['article','cats']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function category()
    {
        $cats=Article_category::where('shop_id',Auth::user()->shop_id)->with('photo')->paginate(20);
        return view('admin.Blog.article-categories',compact('cats'));
    }

    public function category_edit($id)
    {
        $cat=Article_category::find($id);
        $cats = Article_category::where('shop_id', Auth::user()->shop_id)->get();
        return view('admin.Blog.category-edit',compact(['cat','cats']));
    }

    public function category_new()
    {
        $cats = Article_category::where('shop_id', Auth::user()->shop_id)->get();
        return view('admin.Blog.new-article-category',compact('cats'));
    }
}
