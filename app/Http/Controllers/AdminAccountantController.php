<?php

namespace App\Http\Controllers;

use App\Account;
use App\Account_type;
use App\BankAccInfo;
use App\Checkout;
use App\Customer;
use App\Income;
use App\Input_Factor;
use App\myCheque;
use App\online_output_factor;
use App\otherCheque;
use App\Outgo;
use App\Outgo_Bill;
use App\Outgo_Charak_Service;
use App\Outgo_Other;
use App\Outgo_Salary;
use App\Output_factor;
use App\Provider;
use App\Shop;
use App\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\False_;

class AdminAccountantController extends Controller
{
    public function accountant()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $outgoes = Outgo::where('shop_id', Auth::user()->shop_id)->with('salary','bill','charak_service','other')->get();
            $incomes = Income::where('shop_id', Auth::user()->shop_id)->get();
            return view('admin.Accountant.accountant', compact(['incomes', 'outgoes']));
        } else {
            return 403;
        }
    }

    public function accounts()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $my_accs = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', 'self']])->paginate(10);
            $providers_acc = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Provider::class]])->paginate(10);
            $customers_acc = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type', Customer::class]])->paginate(10);
            $debts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type',Provider::class], ['amount', '>', 0]])
                ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type',Customer::class], ['amount', '>', 0]])
                ->paginate(10);
            $credits = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type',Provider::class], ['amount', '<', 0]])
                ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type',Customer::class], ['amount', '<', 0]])
                ->paginate(10);
            return view('admin.Accountant.accounts', compact(['my_accs', 'providers_acc', 'customers_acc', 'debts', 'credits']));
        } else {
            return 403;
        }
    }

    public function createAccount()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $acc_type=Account_type::where('name','بانک')->first();
            $banks=Account::where([['shop_id',Auth::user()->shop_id],['type_id',$acc_type->id],['owner_type','self']])->get();
            return view('admin.Accountant.newAccount',compact('banks'));
        }else{
            return 403;
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $account = new Account();
            $account->shop_id = Auth::user()->shop_id;
            $account->name = $request->input('name');
            $account->owner_type = 'self';
            $account->amount = $request->input('amount');
            $account->type_id=$request->input('type');
            $account->save();
            if ($request->type=='2'){
                $bank_info=new BankAccInfo();
                $bank_info->account_id=$account->id;
                $bank_info->owner_name=$request->input('owner_name');
                $bank_info->owner_family=$request->input('owner_family');
                $bank_info->acc_type=$request->input('acc_type');
                $bank_info->bank=$request->input('bank');
                $bank_info->save();
            }
            $request->session()->put('success','حساب جدید ایجاد شد.');
            return redirect(route('accounts'));
        } else {
            return 403;
        }
    }

    public function editAccount($id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {

            $account = Account::find($id);
            return view('admin.Accountant.editAccount', compact('account'));
        } else {
            return 403;
        }
    }

    public function updateAccount(Request $request, $id)
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $account = Account::find($id);
            $account->name = $request->input('name');
            $account->amount = $request->input('amount');
            $account->save();
            return redirect(route('accounts'));
        } else {
            return 403;
        }
    }

    public function offlineAcc()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $sell_factors = Output_factor::where([['shop_id', Auth::user()->shop_id], ['type', 'offline']])->with('customer')->get();
            $buy_factors = Input_Factor::where([['shop_id', Auth::user()->shop_id], ['type', 'offline']])->get();
            $incomes = Income::where([['shop_id', Auth::user()->shop_id], ['type', 'offline']])->get();
            $outgoes = Outgo::where([['shop_id', Auth::user()->shop_id], ['type', 'offline']])->with('salary','bill','charak_service','other')->get();
            return view('admin.Accountant.offlineAcc', compact(['sell_factors', 'buy_factors', 'incomes', 'outgoes']));
        } else {
            return 403;
        }
    }


    public function onlineAcc()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $sell_factors = online_output_factor::where('shop_id', Auth::user()->shop_id)->with('customer')->get();
            $buy_factors = Input_Factor::where([['shop_id', Auth::user()->shop_id], ['type', 'online']])->get();
            $incomes = Income::where([['shop_id', Auth::user()->shop_id], ['type', 'online']])->get();
            $outgoes = Outgo::where([['shop_id', Auth::user()->shop_id], ['type', 'online']])->with('salary','bill','charak_service','other')->get();
            return view('admin.Accountant.onlineAcc',compact(['sell_factors','buy_factors','incomes','outgoes']));
        } else {
            return 403;
        }
    }

    public function newOutgo(Request $request)
    {
        $users=User::where('shop_id',Auth::user()->shop_id)->get();
        $accounts=Account::where([['owner_type','self'],['shop_id',Auth::user()->shop_id]])->get();
        return view('admin.Accountant.newOutgo',compact(['users','accounts']));
    }

    public function saveOutgo(Request $request)
    {
        $outgo=new Outgo();
        $outgo->shop_id=Auth::user()->shop_id;
        $outgo->user_id=Auth::id();
        $outgo->source_id=$request->source;
        $outgo->source_type=Account::class;
        $outgo->price=$request->price;
        $outgo->type='offline';

        switch ($request->type){
            case'bill':
                $bill=new Outgo_Bill();
                $bill->price=$request->price;
                $bill->name=$request->name;
                $bill->bill_id=$request->bill_id;
                $bill->payment_code=$request->payment_code;
                $bill->tracking_id=$request->tracking_id;
                $bill->save();
                $outgo->type='offline';
                $outgo->target_type=Outgo_Bill::class;
                $outgo->target_id=$bill->id;
                break;

            case'employee':
                $salary=new Outgo_Salary();
                $salary->employee_id=$request->user;
                $salary->price=$request->price;
                $salary->calc_method=$request->calc_method;
                $salary->total_work=$request->total_work;
                if ($request->tracking_id)
                    $salary->tracking_id=$request->tracking_id;

                if (strpos($request->start, '/') !== false) {
                    $orderdate = explode('/', $request->start);
                    function engNum($string)
                    {
                        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                        $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                        $output = str_replace($persian, $english, $string);
                        return $output;
                    }
                    $month = (int)engNum($orderdate[1]);
                    $day = (int)engNum($orderdate[2]);
                    $year = (int)engNum($orderdate[0]);
                    $jalaliDate = Verta::getGregorian($year, $month, $day);
                    $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                } else {
                    $milDate = Carbon::instance(Verta::parse($request->start)->datetime());
                }
                $salary->start=$milDate;
                $milDate=null;
                if (strpos($request->end, '/') !== false) {
                    $orderdate = explode('/', $request->end);
                    function engNum1($string)
                    {
                        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                        $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

                        $output = str_replace($persian, $english, $string);
                        return $output;
                    }
                    $month = (int)engNum1($orderdate[1]);
                    $day = (int)engNum1($orderdate[2]);
                    $year = (int)engNum1($orderdate[0]);
                    $jalaliDate = Verta::getGregorian($year, $month, $day);
                    $milDate = Carbon::create($jalaliDate[0], $jalaliDate[1], $jalaliDate[2], 0, 0, 0, 'Asia/Tehran');
                } else {
                    $milDate = Carbon::instance(Verta::parse($request->end)->datetime());
                }
                $salary->end=$milDate;
                $salary->save();
                $outgo->target_type=Outgo_Salary::class;
                $outgo->target_id=$salary->id;
                break;

            case'charak':
                $charak=new Outgo_Charak_Service();
                $charak->price=$request->price;
                $charak->target=$request->name;
                if ($request->tracking_id)
                    $charak->tracking_id=$request->tracking_id;
                $charak->save();
                $outgo->target_type=Outgo_Charak_Service::class;
                $outgo->target_id=$charak->id;
                $outgo->type='online';
                break;

            case'other':
                $other=new Outgo_Other();
                $other->price=$request->price;
                $other->target=$request->name;
                if ($request->tracking_id)
                    $other->tracking_id=$request->tracking_id;
                $other->save();
                $outgo->target_type=Outgo_Other::class;
                $outgo->target_id=$other->id;
                $outgo->type='offline';
                break;
        }
        $outgo->save();
        $account=Account::where([['shop_id',Auth::user()->shop_id],['owner_type','self'],['id',$request->source]])->first();
        $account->amount-=$request->price;
        $account->save();
        return redirect(route('offlineAcc'));
    }

    public function dashboard()
    {
        $customers_qty=Customer::where('shop_id',Auth::user()->shop_id)->count();
        $providers_qty=Provider::where('belong_to',Auth::user()->shop_id)->count();
        $pending_money=Checkout::where([['shop_id',Auth::user()->shop_id],['status','pending']])->sum('total_price');
        $debts = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type',Provider::class], ['amount', '>', 0]])
            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type',Customer::class], ['amount', '>', 0]])
            ->sum('amount');

        $credits = Account::where([['shop_id', Auth::user()->shop_id], ['owner_type',Provider::class], ['amount', '<', 0]])
            ->orwhere([['shop_id', Auth::user()->shop_id], ['owner_type',Customer::class], ['amount', '<', 0]])
            ->sum('amount');
        $incomes=Income::where('shop_id',Auth::user()->shop_id)->sum('price');
        $outgoes=Outgo::where('shop_id',Auth::user()->shop_id)->sum('price');

        $today= \Carbon\Carbon::now();
        $lastweek= \Carbon\Carbon::now()->subDay(6)->toDateString();
        $weekly_incomes=Income::where('shop_id',Auth::user()->shop_id)->wherebetween('created_at',[$lastweek, $today])->get();
        $weekly_outgoes=Outgo::where('shop_id',Auth::user()->shop_id)->wherebetween('created_at',[$lastweek, $today])->get();
        $total_weekly_earn=array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0);
        $total_weekly_outgo=array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0);
        $today1=Carbon::now()->subDay(1)->toDateString();
        $today2=Carbon::now()->subDay(2)->toDateString();
        $today3=Carbon::now()->subDay(3)->toDateString();
        $today4=Carbon::now()->subDay(4)->toDateString();
        $today5=Carbon::now()->subDay(5)->toDateString();
        $today6=Carbon::now()->subDay(6)->toDateString();

        foreach ($weekly_incomes as $income) {
            $d = Carbon::parse($income->created_at)->toDateString();
            switch ($d) {
                case $today:
                    $total_weekly_earn[0] += $income->price;
                    break;
                case $today1:
                    $total_weekly_earn[1] += $income->price;
                    break;
                case $today2:
                    $total_weekly_earn[2] += $income->price;
                    break;
                case $today3:
                    $total_weekly_earn[3] += $income->price;
                    break;
                case $today4:
                    $total_weekly_earn[4] += $income->price;
                    break;
                case $today5:
                    $total_weekly_earn[5] += $income->price;
                    break;
                case $today6:
                    $total_weekly_earn[6] += $income->price;
                    break;
            }
        }
        $max_income = max($total_weekly_earn);

        foreach ($weekly_outgoes as $outgo) {
            $dd = Carbon::parse($outgo->created_at)->toDateString();
            switch ($dd) {
                case $today:
                    $total_weekly_outgo[0] += $outgo->price;
                    break;
                case $today1:
                    $total_weekly_outgo[1] += $outgo->price;
                    break;
                case $today2:
                    $total_weekly_outgo[2] += $outgo->price;
                    break;
                case $today3:
                    $total_weekly_outgo[3] += $outgo->price;
                    break;
                case $today4:
                    $total_weekly_outgo[4] += $outgo->price;
                    break;
                case $today5:
                    $total_weekly_outgo[5] += $outgo->price;
                    break;
                case $today6:
                    $total_weekly_outgo[6] += $outgo->price;
                    break;
            }
        }
        $max_outgo = max($total_weekly_outgo);

        if ($max_income>$max_outgo)
            $max_weekly_in_out=$max_income;
        else
            $max_weekly_in_out=$max_outgo;

        $this_month=Carbon::now()->format('m');
        $my_cheques=myCheque::where('shop_id',Auth::user()->shop_id)->whereMonth('created_at',$this_month)->with('account')->paginate(5);
        $other_cheques=otherCheque::where('shop_id',Auth::user()->shop_id)->whereMonth('created_at',$this_month)->paginate(5);
        $this_month_cheques=myCheque::where('shop_id',Auth::user()->shop_id)->whereMonth('pay_date',$this_month)->with('account')->paginate(5);
        $wallet=Auth::user()->shop->wallet;

        return view('admin.Accountant.dashboard',compact(['this_month_cheques','wallet','pending_money','my_cheques','other_cheques','customers_qty','providers_qty','debts','credits','incomes','outgoes','max_weekly_in_out','total_weekly_outgo','total_weekly_earn']));
    }

    public function unpaid_factors()
    {
        if (Auth::user()->role->name == 'مدیر' || Auth::user()->role->name == 'مالک' || Auth::user()->shoprole->roles->accountant == 1) {
            $factors = Input_Factor::where([['shop_id', Auth::user()->shop_id],['pay_status','unpaid']])->paginate(20);
            return view('admin.Accountant.unpaid_factors',compact('factors'));
        } else {
            return 403;
        }
    }
}
