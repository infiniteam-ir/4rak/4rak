<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class shopInfoReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city'=>'required',
            'province'=>'required',
            'name'=>'required',
            'class'=>'required',
            'guild'=>'required',
            'address'=>'required',
            'shopName'=>'required',
            'description'=>'required',
            /*'headerImg'=>'required',
            'logoImg'=>'required',
            'licenseImg'=>'required',*/

        ];
    }

    public function messages()
    {
        return[

        ];
    }
}
