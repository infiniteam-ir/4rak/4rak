<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Output_factor extends Model
{
    public function out_products()
    {
        return $this->hasMany(Output_factor_pro::class,'factor_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function incomes()
    {
        return $this->belongsTo(Income::class,'f_num','source_id');
    }
}
