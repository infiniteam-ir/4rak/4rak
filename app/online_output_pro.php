<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class online_output_pro extends Model
{
    public function factor()
    {
        return $this->belongsTo(online_output_factor::class);
    }

    public function customer_factor()
    {
        return $this->belongsTo(Customer_factor::class,'customer_factor_id','id');

    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
