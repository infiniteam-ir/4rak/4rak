<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileCodes extends Model
{
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
