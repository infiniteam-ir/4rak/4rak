<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Middleware\SessionExpired;
use App\Http\Middleware\CheckShopPlan;
use App\Http\Middleware\verifyMobile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('config','AdminController@config')->name('config');
Route::get('about', 'ShopController@index')->name('about');
Route::post('frontAjax','AdminController@frontAjax')->name('frontAjax');
Route::get('/', 'ShopController@store')->name('store');
Route::get('allShops', 'ShopController@allShops')->name('allShops');
Route::get('guilds', 'ShopController@guilds')->name('guilds');
//Route::get('categories/{title}/{id}', 'ShopController@categories')->name('categories');
Route::get('AdvanceCategorie/{province}/{city}/{cat}', 'ShopController@getProductPaginate')->name('advanceCategory');
Route::get('blogCategorie/{cat}/{id}', 'ShopController@blog_categories')->name('blogCategory');
//Route::get('paginatePros/{province}/{city}/{cat}', 'ShopController@getProductPaginate')->name('paginatePro');
Route::get('blog', 'ShopController@blog')->name('blog');
Route::get('blog/article/{id}', 'ShopController@article')->name('article');
Route::get('shop/{name}/cat/{id}/{title}', 'ShopController@get_products_by_cat')->name('cat.products');
Route::get('shop/{name}/services', 'ShopController@shop_services')->name('services');
Route::get('shop/{name}/gallery', 'ShopController@shop_gallery')->name('shop-gallery');
Route::get('shop/{name}/blog', 'ShopController@shop_blog')->name('shop-blog');
Route::get('shop/{name}/blog/{id}', 'ShopController@shop_article')->name('shop-article');
Route::get('shop/services/{id}', 'ShopController@service')->name('show_service');


Route::get('shop/{unique_name}', 'ShopController@singleshop')->name('singleshop');
Route::get('shop/cart/{id}', 'ShopController@addToCart')->name('addToCart');
Route::get('shop/product/{slug}/{id}', 'ShopController@product')->name('singleProduct');
Route::get('shoppingCart', 'ShopController@ShoppingCart')->name('ShoppingCart')->middleware('auth');
Route::post('bill', 'ShopController@bill')->name('bill');
Route::post('deleteCart', 'ShopController@deleteCart')->name('deleteCart');
Route::get('prePay','ShopController@payment')->name('payment');
Route::get('payment/response/{customer_factor_id}','ShopController@paymentResponse')->name('paymentResponse');
Route::get('change-password', 'AdminController@change_password')->name('change_password');
Route::post('verify-user-mobile','AdminController@verify_user_mobile')->name('verify_user_mobile');
Route::post('verify-code','AdminController@verify_code')->name('verify-code');
Route::post('update-pass','AdminController@update_pass')->name('update-pass');
Route::get('show-change-pass', 'AdminController@show_change_pass')->name('show-change-pass');





Route::prefix('admin')->middleware('auth', 'sessionExpired','verifyMobile')->group(function () {
    Route::get('info', 'AdminShopController@shopInfo')->name('shopInfo');
    Route::get('buy-plan', 'AdminShopController@buy_plan')->name('buy_plan');
    Route::get('buy-plan-result/{id}', 'AdminShopController@buy_plan_result')->name('buy_plan_result');
    Route::post('plan-purchase','AdminShopController@plan_purchase')->name('plan-purchase');
    Route::post('verifyMobile','AdminController@verifyMobile')->name('verifyMobile');
    Route::get('change-mobile', 'AdminController@change_mobile')->name('change_mobile');
    Route::post('update-mobile','AdminController@update_mobile')->name('update_mobile');





    Route::prefix('shop')->middleware('CheckShopPlan')->group(function (){
        Route::post('info/upload', 'AdminShopController@imageup')->name('imageup');
        Route::post('storeShopInfo', 'AdminShopController@store')->name('storeShopInfo');
        Route::get('ShopuploadForm', 'AdminShopController@ShopuploadForm')->name('ShopuploadForm');
        Route::get('edit/{id}', 'AdminShopController@ShopInfoEdit')->name('EditShopInfo');
        Route::patch('/{id}', 'AdminShopController@UpdateInfo')->name('UpdateInfo');
        Route::get('roles', 'AdminShopController@shopRoles')->name('shopRoles');
        Route::delete('shopDeleteRole/{id}', 'AdminShopController@destroyRole')->name('shopDeleteRole');
        Route::get('createRole', 'AdminShopController@createRole')->name('createRole');
        Route::post('saveRole', 'AdminShopController@saveRole')->name('saveRole');
        Route::get('roles/edit/{id}', 'AdminShopController@editRoles')->name('editRoles');
        Route::patch('updateRole/{id}', 'AdminShopController@updateRole')->name('updateRole');
        Route::get('gallery', 'AdminShopController@gallery')->name('gallery');
        Route::post('setGallery', 'AdminShopController@setGallery')->name('setGallery');
        Route::post('deleteGalleryImg', 'AdminShopController@deleteGalleryImg')->name('deleteGalleryImg');
        Route::post('deleteGalleryFile', 'AdminShopController@deleteGalleryFile')->name('deleteGalleryFile');
        Route::resource('article','AdminArticleController');
        Route::get('article-category','AdminArticleController@category')->name('article-category');
        Route::get('article-category/edit/{id}','AdminArticleController@category_edit')->name('article-category-edit');
        Route::get('article-category/new','AdminArticleController@category_new')->name('article-category-new');

        Route::get('article-comments','AdminCommentsController@article_comments')->name('article-comments');
        Route::get('product-comments','AdminCommentsController@product_comments')->name('product-comments');
        Route::get('all-comments','AdminCommentsController@all_comments')->name('all-comments');


        Route::get('personnel/dashboard','AdminPersonnelController@dashboard')->name('personnelDashboard');
        Route::resource('personnel', 'AdminPersonnelController');
        Route::post('personnel/setUserRole/{id}', 'AdminPersonnelController@setUserRole')->name('setUserRole');
        Route::get('logs', 'AdminPersonnelController@logs')->name('userLogs');


        Route::resource('category', 'AdminCategoryController');

        Route::resource('product', 'AdminProductController');
        Route::get('newProduct', 'AdminProductController@new_product')->name('newProduct');

        Route::get('products/dashboard','AdminProductController@dashboard')->name('productDashboard');
        Route::get('factor/purchases/{id}', 'AdminProductController@factorPurchase')->name('factorPurchase');
        Route::get('factor/purchases/close/{id}', 'AdminProductController@savePurchase')->name('savePurchase');
        Route::get('factor/payment/close/{id}', 'AdminProductController@savePayment')->name('savePayment');
        Route::get('sell', 'AdminProductController@sell')->name('sell');
        Route::get('guaranty', 'AdminProductController@guaranty')->name('guaranty');
        Route::get('guarantyTracking', 'AdminProductController@guarantyTracking')->name('guarantyTracking');
        Route::get('guarantyTracking/set/{id}/{step}', 'AdminProductController@SetGuarantyTracking')->name('setGuarantyTracking');
        Route::get('guarantyinfo/{id}/', 'AdminProductController@guarantyinfo')->name('guarantyinfo');
        Route::post('accept-guaranty', 'AdminProductController@accept_guaranty')->name('accept_guaranty');
        Route::get('editbuyFactor/{id}', 'AdminProductController@editbuyFactor')->name('editbuyFactor');
        Route::get('editsellFactor/{id}', 'AdminProductController@editsellFactor')->name('editsellFactor');
        Route::get('editFactorProduct/{id}', 'AdminProductController@editFactorProduct')->name('editFactorProduct');
        Route::get('sellFactor/{id}', 'AdminProductController@sellFactor')->name('sellFactor');
        Route::post('searchGuaranty', 'AdminProductController@searchGuaranty')->name('searchGuaranty');
        Route::post('UploadProductFiles/{id}', 'AdminProductController@UploadProductFiles')->name('UploadProductFiles');
        Route::delete('UploadProductFiles/{id}', 'AdminProductController@DelUploadProductFiles')->name('DelUploadProductFiles');
        Route::post('deleteUploaded', 'AdminProductController@deleteUploaded')->name('deleteUploaded');

        Route::get('accountant', 'AdminAccountantController@accountant')->name('accountant');
        Route::get('accountant/dashboard','AdminAccountantController@dashboard')->name('accountantDashboard');
        Route::get('offlineAcc', 'AdminAccountantController@offlineAcc')->name('offlineAcc');
        Route::get('onlineAcc', 'AdminAccountantController@onlineAcc')->name('onlineAcc');
        Route::get('Accounts', 'AdminAccountantController@Accounts')->name('accounts');
        Route::get('Accounts/create', 'AdminAccountantController@createAccount')->name('accounts.create');
        Route::get('Accounts/edit/{id}', 'AdminAccountantController@editAccount')->name('accounts.edit');
        Route::post('Accounts/store', 'AdminAccountantController@store')->name('accounts.store');
        Route::patch('Accounts/update/{id}', 'AdminAccountantController@updateAccount')->name('accounts.update');
        Route::get('Accounts/newOutgo', 'AdminAccountantController@newOutgo')->name('newOutgo');
        Route::post('Accounts/saveOutgo', 'AdminAccountantController@saveOutgo')->name('saveOutgo');
        Route::get('unpaidFactors', 'AdminAccountantController@unpaid_factors')->name('unpaidFactors');


        Route::get('storage/dashboard','AdminStorageController@dashboard')->name('storageDashboard');
        Route::get('ioProduct', 'AdminStorageController@ioProduct')->name('ioProduct');
        Route::post('importProduct', 'AdminStorageController@importProduct')->name('importProduct');
        Route::post('exportProduct', 'AdminStorageController@exportProduct')->name('exportProduct');
        Route::get('storageReport', 'AdminStorageController@storageReport')->name('storageReport');
        Route::resource('storage', 'AdminStorageController');

        Route::get('delivery/dashboard','AdminDeliveryController@dashboard')->name('deliveryDashboard');
        Route::resource('delivery', 'AdminDeliveryController');
        Route::get('deliveryReq', 'AdminDeliveryController@deliveryReq')->name('deliveryReq');
        Route::post('saveReq/{id}', 'AdminDeliveryController@saveReq')->name('saveReq');
        Route::get('onlineOrders', 'AdminDeliveryController@onlineOrders')->name('onlineOrders');
        Route::get('onlineOrders/factor/{id}', 'AdminDeliveryController@onlineOrdersInfo')->name('onlineOrdersInfo');

        Route::resource('service', 'AdminServiceController');

        Route::resource('comment', 'AdminCommentsController');
    });

    Route::get('setting/main-shop','AdminController@main_shop_setting')->name('main-shop-setting');
    Route::post('setting/main-shop/slider/save','AdminController@save_slider_imgs')->name('save_slider_imgs');
    Route::get('setting/main-shop/s/delete/{id}','AdminController@delete_slider_imgs')->name('delete_slider_imgs');
    Route::post('setting/main-shop/banner/save','AdminController@save_banner_imgs')->name('save_banner_imgs');
    Route::get('setting/main-shop/b/delete/{id}','AdminController@delete_banner_imgs')->name('delete_banner_imgs');
    Route::get('setting/blog','AdminController@blog_setting')->name('blog-setting');
    Route::post('setting/blog/set','AdminController@blog_setting_set_article')->name('set-blog-top-article');
    Route::get('setting/delivery-methods','AdminController@delivery')->name('delivery_methods');
    Route::get('setting/delivery-methods/new','AdminController@add_delivery_method')->name('new_delivery_method');
    Route::get('setting/delivery-methods/edit/{id}','AdminController@edit_delivery_method')->name('edit_delivery_method');
    Route::get('setting/pay-plans','AdminController@pay_plans')->name('plans');
    Route::get('setting/pay-plans/new','AdminController@new_pay_plan')->name('new_pay_plan');
    Route::get('setting/pay-plans/edit/{id}','AdminController@edit_pay_plan')->name('edit_pay_plan');
    Route::get('setting/checkout-requests','AdminController@checkoutReq')->name('checkoutReq');
    Route::post('setting/pay-checkout/{id}','AdminController@payCheckout')->name('payCheckout');
    Route::get('setting/guilds','AdminController@guilds')->name('guilds-setting');
    Route::get('setting/guilds/new','AdminController@add_guild')->name('add-guild');
    Route::get('setting/guilds/edit/{id}','AdminController@edit_guild')->name('edit_guild');
    Route::get('setting/subguilds/{id}','AdminController@sub_guilds')->name('sub_guilds');
    Route::get('setting/subguilds/new/{id}','AdminController@add_subguild')->name('add-subguild');
    Route::get('setting/subguilds/edit/{id}','AdminController@edit_subguild')->name('edit-subguild');
    Route::get('setting/product-cat','AdminController@pro_cats')->name('pro-categories');
    Route::get('setting/product-cat/new','AdminController@add_pro_cat')->name('add-pro-category');
    Route::get('setting/product-cat/edit/{id}','AdminController@edit_pro_cat')->name('edit-pro-category');


    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::resource('Profile', 'ProfileController');
    Route::get('shopping-history','ProfileController@buyHistory')->name('buyHistory');
    Route::get('shopping-history/detail/{id}','ProfileController@buyFactorDetail')->name('buyFactorDetail');
    Route::post('ajax', 'AdminController@ajax')->name('ajax');
    Route::post('/webAjax', 'AdminController@webAjax')->name('webAjax');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


