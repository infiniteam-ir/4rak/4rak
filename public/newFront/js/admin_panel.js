/* c o d e r b o y */
jQuery(function($){
    "use strict";
    $('#page_template').change(function(){
        $('#fw-options-box-magical_box_options').hide();
        $('#fw-options-box-default_page_options').hide();
        $('#fw-options-box-aof_page_options').hide();

        if($(this).val() === "magical-box.php"){
            $('#fw-options-box-magical_box_options').slideDown();
        }else if($(this).val() === "amazing-offer.php"){
            $('#fw-options-box-aof_page_options').slideDown();
        }else if($(this).val() === "default"){
            $('#fw-options-box-default_page_options').slideDown();
        }
    });
    $('#page_template').change();
    $(document).on('click','.cb_addable_image .add-btn',function(){
        var thisitem = $(this);
        var image_frame;
        if(image_frame){
            image_frame.open();
        }
        // Define image_frame as wp.media object
        image_frame = wp.media({
            title: 'Select Media',
            multiple : false,
            library : {
                type : 'image',
            }
        });

        image_frame.on('close',function() {
            var selection =  image_frame.state().get('selection');
            selection.each(function(attachment) {
                var inp_name = thisitem.attr("data-name");
                thisitem.parent().find('.image-items').append('<div class="img-item"><img src="'+attachment.changed.url+'" alt=""><i>X</i><input name="'+inp_name+'[]" value="'+attachment['id']+'" type="hidden"></div>');

            });
        });

        image_frame.on('open',function() {
            var selection =  image_frame.state().get('selection');
        });

        image_frame.open();

        $('#variable_regular_price_0').trigger('change');


    });
    $(document).on('click','.cb_addable_image .img-item i',function(){
        $(this).parent().remove();
        $('#variable_regular_price_0').trigger('change');
    });
});
