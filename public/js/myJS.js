$(document).ready(function () {

    resizeTemplate()
    $(window).resize(resizeTemplate)

    function resizeTemplate() {
        if($(window).width() <= 992) {
            $('#sidebar').collapse('hide');
        } else {
            $('#sidebar').collapse('show');
        }
    }

    $('#sidebar').on('hide.bs.collapse',function (e) {
        if (e.target == this){
            $('#main').removeClass('col-lg-10')
            $('#navbar').removeClass('myNav')
            $('#footer').removeClass('myNav')
            $('#footer').addClass('w-100')

        }


    })
    $('#sidebar').on('show.bs.collapse',function (e) {
        if (e.target == this) {
            $('#main').addClass('col-lg-10')
            $('#navbar').addClass('myNav')
            $('#footer').addClass('myNav')
        }
    })


        $('#sidebar').niceScroll({
            cursorwidth:12,
            cursoropacitymin:0.4,
            cursorcolor:'#322f3d',
            cursorborder:'none',
            cursorborderradius:8,
            autohidemode:'leave',
            railalign: 'left',
            emulatetouch:false

        });

    $('li.nav-item.active').parent().parent().addClass('show');





});