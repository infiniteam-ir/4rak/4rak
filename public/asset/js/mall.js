$("document").ready(function () {

});
/*PopOver*/
$(function () {
    $('.example-popover').popover({
        container: 'body'
    })
})
$("#popover").popover({trigger: "hover"});
$(function () {
    $('[data-toggle="popover"]').popover()
})
$(".pop").popover({trigger: "manual", html: true, animation: false})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
    var _this = this;
    setTimeout(function () {
        if (!$(".popover:hover").length) {
            $(_this).popover("hide");
        }
    }, 300);
});

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openNav() {
    $('#layer').removeClass('d-none')
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
   /* document.getElementById("layer").style.marginLeft = "250px";*/

}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    /*document.getElementById("layer").style.marginLeft = "0";*/
    $('#layer').addClass('d-none')
}