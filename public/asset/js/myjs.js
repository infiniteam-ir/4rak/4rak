$("document").ready(function () {
    $("#sidebarMenu").niceScroll()
    /*Section Off and ... #rightUl*/
    if (window.matchMedia("(max-width: 960px)").matches) {
        var util = UIkit.util;
        var btn = util.$('.uk-button');
        var modalEl = util.$('#modal-example');
        util.on(btn, 'click', function () {
            UIkit.modal(modalEl).show();
        })
    } else {
        // Notification
        // UIkit.notification('Window is less than 900px');
    };

});

function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

function w3_toggle() {
    if (document.getElementById("mySidebar").style.display == "block") {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    } else {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

}

var swiper = new Swiper('.blog-slider', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    mousewheel: {
        invert: false,
    },
    // autoHeight: true,
    pagination: {
        el: '.blog-slider__pagination',
        clickable: true,
    }
});