<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->string('groupable_type');
            $table->foreignId('groupable_id');
            $table->foreignId('reply_to')->nullable();
            $table->foreignId('user_id');
            $table->text('content');
            $table->text('positive_points')->nullable();
            $table->text('weak_points')->nullable();
            $table->string('value_per_price')->nullable();
            $table->string('quality')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
