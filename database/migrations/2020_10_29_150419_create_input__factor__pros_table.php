<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputFactorProsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input__factor__pros', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('factor_id');
            $table->foreignId('product_id');
            $table->unsignedInteger('qty');
            $table->unsignedBigInteger('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input__factor__pros');
    }
}
