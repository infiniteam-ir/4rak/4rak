<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_cheques', function (Blueprint $table) {
            $table->id();
            $table->foreignId('belong_to_acc');
            $table->foreignId('shop_id');
            $table->string('serial');
            $table->string('pay_to');
            $table->foreignId('provider_id');
            $table->unsignedDouble('amount');
            $table->date('pay_date');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_cheques');
    }
}
