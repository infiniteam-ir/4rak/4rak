<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutgoSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgo__salaries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id');
            $table->unsignedDouble('price');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('calc_method');
            $table->unsignedInteger('total_work');
            $table->unsignedBigInteger('tracking_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgo__salaries');
    }
}
