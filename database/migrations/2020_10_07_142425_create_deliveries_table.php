<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->string('name');
            $table->string('family');
            $table->string('mobile');
            $table->boolean('status')->default(0);
            $table->boolean('ready')->default(0);
            $table->integer('age');
            $table->string('vehicle');
            $table->text('vehicle_details');
            $table->foreignId('file_id')->nullable();
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->integer('rate')->default(0);
            $table->integer('xp')->default(0);
            $table->id();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
