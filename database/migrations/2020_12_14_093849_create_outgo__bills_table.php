<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutgoBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgo__bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedDouble('price');
            $table->string('name');
            $table->unsignedDouble('bill_id');
            $table->unsignedDouble('payment_code');
            $table->unsignedDouble('tracking_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgo__bills');
    }
}
