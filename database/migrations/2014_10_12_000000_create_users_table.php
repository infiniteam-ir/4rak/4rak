<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('family');
            $table->string('mobile');
            $table->boolean('sex')->nullable();
            $table->foreignId('role_id');
            $table->foreignId('user_info_id')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->foreignId('shop_id')->nullable();
            $table->foreignId('shop_role_id')->nullable();
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('file_id')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
