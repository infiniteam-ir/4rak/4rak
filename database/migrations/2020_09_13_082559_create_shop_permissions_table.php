<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_permissions', function (Blueprint $table) {
            $table->id();
            $table->boolean('products')->default(0);
            $table->boolean('accountant')->default(0);
            $table->boolean('personnel')->default(0);
            $table->boolean('storage')->default(0);
            $table->boolean('buy')->default(0);
            $table->boolean('sell')->default(0);
            $table->boolean('reject')->default(0);
            $table->boolean('guaranty')->default(0);
            $table->boolean('delivery')->default(0);
            $table->foreignId('shop_id');
            $table->string('role_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_permissions');
    }
}
