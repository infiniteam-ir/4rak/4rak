<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_cheques', function (Blueprint $table) {
            $table->id();
            $table->string('owner')->nullable();
            $table->string('owner_type')->nullable();
            $table->foreignId('owner_id')->nullable();
            $table->string('pay_to')->nullable();
            $table->string('pay_to_type')->nullable();
            $table->foreignId('pay_to_id')->nullable();
            $table->foreignId('shop_id');
            $table->string('serial');
            $table->unsignedDouble('amount');
            $table->date('pay_date');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_cheques');
    }
}
