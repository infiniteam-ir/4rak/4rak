<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('pay_plan_id')->nullable();
            $table->foreignId('province_id');
            $table->foreignId('city_id');
            $table->foreignId('guild_id');
            $table->foreignId('subguild_id');
            $table->string('address');
            $table->string('unique_name');
            $table->text('description');
            $table->unsignedDouble('wallet')->default(0);
            $table->foreignId('header_id')->nullable();
            $table->foreignId('logo_id')->nullable();
            $table->foreignId('license_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->integer('rate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
