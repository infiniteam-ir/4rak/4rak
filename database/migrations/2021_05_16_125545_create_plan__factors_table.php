<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan__factors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('plan_id');
            $table->foreignId('shop_id');
            $table->foreignId('plan_pivot_id')->nullable();
            $table->string('status')->default('unpaid');
            $table->unsignedInteger('month');
            $table->date('pay_date')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('transaction_code')->nullable();
            $table->unsignedDouble('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan__factors');
    }
}
