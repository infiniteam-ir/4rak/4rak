<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->default(0);
            $table->string('title');
            $table->string('slug');
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('cat_id');
            $table->foreignId('author_id');
            $table->text('content');
            $table->text('meta_description')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('rate')->default(0);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
