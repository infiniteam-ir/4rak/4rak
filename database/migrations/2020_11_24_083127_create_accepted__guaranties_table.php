<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcceptedGuarantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accepted__guaranties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id');
            $table->foreignId('shop_id');
            $table->foreignId('output_factor_id');
            $table->foreignId('provider_id');
            $table->date('send_to_guaranty')->nullable();
            $table->date('receive_from_guaranty')->nullable();
            $table->date('customer_delivery')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accepted__guaranties');
    }
}
