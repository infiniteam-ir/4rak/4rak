<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('cat_id');
            $table->foreignId('photo_id')->nullable();
            $table->foreignId('file_id')->nullable();
            $table->foreignId('user_id');
            $table->foreignId('storage_id')->nullable();
            $table->foreignId('guaranty_id')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('status');
            $table->string('provider_co');
            $table->unsignedBigInteger('buy_price');
            $table->unsignedBigInteger('sell_price');
            $table->unsignedBigInteger('whole_price')->nullable();
            $table->unsignedBigInteger('quantity');
            $table->text('description');
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->boolean('trash')->default(false);
            $table->tinyInteger('rate')->default(0);
            $table->Integer('like')->default(0);
            $table->string('barcode')->nullable();
            $table->mediumInteger('sold_qty')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
