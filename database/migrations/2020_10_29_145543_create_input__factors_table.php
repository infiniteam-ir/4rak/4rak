<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input__factors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->unsignedBigInteger('f_num');
            $table->string('type');
            $table->foreignId('provider_id');
            $table->unsignedBigInteger('total_price');
            $table->date('f_date');
            $table->string('pay_status')->default('unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input__factors');
    }
}
