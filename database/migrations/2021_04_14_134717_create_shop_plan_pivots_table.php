<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopPlanPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_plan_pivots', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('plan_id');
            $table->unsignedMediumInteger('price');
            $table->date('start_date');
            $table->date('exp_date');
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_plan_pivots');
    }
}
