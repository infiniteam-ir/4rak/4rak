<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineOutputProsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_output_pros', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('factor_id')->nullable();
            $table->foreignId('customer_factor_id');
            $table->foreignId('product_id');
            $table->unsignedInteger('qty');
            $table->unsignedBigInteger('price');
            $table->foreignId('guaranty_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_output_pros');
    }
}
