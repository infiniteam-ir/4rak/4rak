<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineOutputFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_output_factors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('customer_id');
            $table->unsignedBigInteger('total_price');
            $table->date('f_date');
            $table->date('send_date')->nullable();
            $table->string('tracking')->nullable();
            $table->foreignId('delivery_method_id');
            $table->text('note')->nullable();
            $table->unsignedMediumInteger('discount')->nullable();
            $table->foreignId('discount_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_output_factors');
    }
}
