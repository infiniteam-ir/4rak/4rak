<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutgosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgos', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->foreignId('shop_id');
            $table->foreignId('source_id');
            $table->string('source_type');
            $table->foreignId('user_id');
            $table->string('target_type');
            $table->foreignId('target_id');
            $table->unsignedBigInteger('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgos');
    }
}
